/** \addtogroup lib_ROM 
 * @{
 *  ambslp_settings class declaration
 * 
 *  \file ambslp/ambslp_settings.h
 *  \brief settings for the ambslp; same parameters that can be 
 *         extracted from OKS
 * 
 *  The ambslp_settings class collects the parameters needed to 
 *  define a brand new \a ambslp_card object.
 *  The collected variables are those that can be reached from OKS.
 *
 *
**/

/**
 *  \struct daq::ftk::ambslp_settings
 *  \brief ambslp_settings class
 *
 *  a collection of variables that are used to initialize 
 *  a new \a ambslp_card 
 *  object, and a method to copy the stored values into a cloned object
**/

/**
 * 
 *  \var int daq::ftk::ambslp_settings::crate 
 *  \brief an integer which identifies the crate in which the board is situated
 * 
**/

/**
 *  \var int daq::ftk::ambslp_settings::slot 
 *  \brief an integer which identifies the slot in which the card is situated
 *
**/

/**
 *
 *  \fn void daq::ftk::ambslp_settings::cloneInto( ambslp_settings* dest )
 *  \brief a method which fills the passed argument with the stored values
 * 
 *  Copy the values for the stored variables into the passed object; 
 *  this method is not responsible for the creation of the cloned object.
 * 
 *  \param[out] dest pointer to a new daq::ftk::ambslp_settings object to be
 *              filled with the values copied by this instance
 *  \return void; in case of problems, exceptions are thrown
 *  \throws daq::ftk::Issue_ambslp
 * @}
**/


#ifndef AMBSLP_SETTINGS_H
#define AMBSLP_SETTINGS_H

namespace daq {
  namespace ftk {

    struct ambslp_settings{
      int crate;
      int slot;
      void cloneInto( ambslp_settings* dest );
    };

  }
}


#endif  //  AMBSLP_SETTINGS_H
