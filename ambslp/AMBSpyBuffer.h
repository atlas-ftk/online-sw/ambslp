#ifndef AMB_SPY_BUFFER
#define AMB_SPY_BUFFER

/*
 *  Functions used by AMB for spy buffer access using VME block transfer
 */

#include <string>
#include <vector>

//Including the base class
#include "ftkvme/vme_spy_buffer.h"



//#define DEBUG_BLOCK
#define AMB_SPY_POINTER_MASK      0x3fff
#define AMB_SPY_DIMENSION_MASK    0xffff
#define AMB_SPY_OVERFLOW_OFFSET   14
#define AMB_SPY_FREEZE_OFFSET     15
#define AMB_SPY_DIMENSION_OFFSET  16

namespace daq { 
  namespace ftk {

    class AMBSpyBuffer : public daq::ftk::vme_spyBuffer{
    public:
      
      AMBSpyBuffer(unsigned int status_addr, 
		    unsigned int base_addr,
		    VMEInterface * vmei);
      virtual ~AMBSpyBuffer();

      //reimplementation of base classes (AMB uses specific SpyStatus definition so far)
      //FIXME SpyStatus is inconsistent with the common scheme. 
      //To be discussed in FTKHWD-513
      virtual uint32_t getSpyFreeze()    {return (m_spyStatus >> AMB_SPY_FREEZE_OFFSET) & 0x1; };
      virtual bool     getSpyOverflow()  {return (m_spyStatus >> AMB_SPY_OVERFLOW_OFFSET) & 0x1; };
      virtual uint32_t getSpyPointer()   {return  m_spyStatus  & AMB_SPY_POINTER_MASK; };
      virtual uint32_t getSpyDimension() {return m_userSpyDimension > 0  ? m_userSpyDimension : 
                                                                          (m_spyStatus >> AMB_SPY_DIMENSION_OFFSET) & AMB_SPY_DIMENSION_MASK; };

    };

  } // namespace ftk
} // namespace daq

#endif
