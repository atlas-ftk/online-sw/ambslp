#ifndef AMBSLP_AMCHIP_H
#define AMBSLP_AMCHIP_H

#include "ambslp/ambslp.h"
#include "ambslp/AMChip.h"
#include "ambslp/ambslp_jtag_func.h"
#include "ambslp/ambslp_vme_jtag_func.h"

bool ambslp_amchip_SERDES_setup(int, int, int, const char*, const char*, const char*, float);
int expand_DC_bits(int, int);
bool pattern2vmedata(uint32_t *, std::vector<std::vector<uint32_t>>&  , bool*, bool);
bool pattern2vmedata_v1(uint32_t *, std::vector<std::vector<uint32_t>>& , bool *, bool);
int read_link_errors(int, int, int, const char*, const char*);
std::pair<int, int> get_serdes_number(const char*);
int pilot_error_count(int, const char*, const char*, bool);
bool test_pattern(int, int, int, int, int, std::vector<std::vector<uint32_t>>&, bool *, bool *, uint32_t *, bool);
bool test_pattern_renaming(int, int, int, int, int, std::vector<std::vector<uint32_t>>&, bool *, uint32_t *, bool);
long unsigned ambslp_amchip_selftest(VMEInterface*, unsigned, unsigned);

// renaming
bool patternIsProperlyReconstructed( int reco_add, int address );
bool patternHasTriggered( int reco_add );
bool patternIsValid( int reco_add );
bool patternAddressIsMatched( int reco_add, int address );

void resetTAP_and_gotoIdle( int slot, int active_columns );
void loadPatterns( int slot, int chipno, int active_columns, int chainlength, const uint32_t *indata,uint32_t *outdata);
void readAddressOfFiredPattern( int slot, int chipno, int active_columns, int chainlength,  const uint32_t *indata, uint32_t *outdata );
//
// federico: don't understand whether this is the appropriate place.
//    wainting for complains
namespace daq { 
  namespace ftk { 
    enum AMchipType { NONE=0, AM05=5, AM06=6 };
    
    //This function is not used from any other codes; judged as obsolete; commented out for a while... TK-11022017
    //void ambslp_amchip_idle_cfg(VMEInterface*, int, int, int, int);
    bool ambslp_amchip_BER(int slot, int chainlength, int chipnum);
    bool ambslp_amchip_read_errDout32(int slot, int chainlength, int chipnum);
    bool ambslp_amchip_enable_bank(int slot, int chainlength, int chipnum, int npatt, int offset);
    bool ambslp_amchip_idlecfg(int slot, int chainlength, int chipnum);                                                                                                               
    bool ambslp_amchip_test_mode(int slot, int chainlength, int chipnum, bool is_duplicate=false); 
    int ambslp_amchip_chip_number(int chain, int chip);
    bool ambslp_amchip_polling_stat_reg_bus(int slot, int chainlength, int chipnum, bool prbsEnabled, bool verbose); 
    bool ambslp_amchip_prbsgen_inout(int slot, int lambver);
    bool ambslp_amchip_enable_bank_DUPLICATE(int slot, int chainlength, int chipnum);
    bool ambslp_amchip_write_test_bank(int slot, int chainlength, int chipnum, int npatt, int offset); 

    bool ambslp_amchip_serdes_config (int slot, int chainlength, int chipnum, const char* columns );
    bool ambslp_amchip_8b_10b_all_buses(int slot, int chainlength, int chipnum ); 

    bool ambslp_amchip_prbsgen_inout_all_buses(int slot, int chainlength, int chipnum); 

    bool ambslp_amchip_polling_stat_reg_pattin(int slot,int chainlength,int chipnum,const char* columns,const char* serdes,bool prbsEnabled); 
    bool ambslp_amchip_prbs_error_count_all_buses(int slot, int chainlength, int chipnum, const char* policy, bool verbose); 
    bool ambslp_amchip_prbs_error_count(int slot, int chainlength, int chipnum, const char* columns, const char* policy, const char* serdes, bool verbose); 
  }
}//namespace

#endif
