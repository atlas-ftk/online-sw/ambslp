////////////////////////////////////////
//		Francesco Cervigni
//		2010/20111
////////////////////////////////////////

#ifndef FTK_AMB_SLP_H
#define FTK_AMB_SLP_H

#include <algorithm>
#include <assert.h>
#include <ctime>
#include <fstream>
#include <inttypes.h>
#include <iostream>
#include <list>
#include <math.h> 
#include <memory> //for shared pointers
#include <pwd.h>
#include <set> 
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <stdexcept>
#include <sys/types.h>
#include <time.h>
#include <utility> //for pairs
#include <vector>

#include "ftkcommon/core.h"
#include "ftkcommon/exceptions.h"
#include "ftkvme/VMEManager.h"
#include "rcc_error/rcc_error.h" //until ftkvme-01-00-07 will be ready
#include "vme_rcc/vme_rcc.h" //until ftkvme-01-00-07 will be ready
#include "cmem_rcc/cmem_rcc.h" //until ftkvme-01-00-07 will be ready
#include "ambslp/ambslp_vme_regs.h"
#include "ambslp/exceptions.h"

/***** global definitions ****/
#define MAXCOLUMNS          32
#define NLAYERS              8
#define MAXOUTLINK_PERLAMB   4
#define MAXLAMB_PER_BOARD    4
#define MAXVMECOL_PER_LAMB   8
#define MAXCHIP_PER_VMECOL   2
#define MAXCHIP_PER_LAMB    (MAXVMECOL_PER_LAMB*MAXCHIP_PER_VMECOL)
#define MAXAMCHIPS          (MAXCHIP_PER_LAMB*MAXLAMB_PER_BOARD)
#define NBITLAYERS          18

#ifndef NBITS_CAMLAYER
#define NBITS_CAMLAYER 18
#endif

#ifndef NPATTS
#define NPATTS 2048
#endif

#ifndef MAXDATAWORDS
#define MAXDATAWORDS 1000
#endif







namespace daq 
{
  namespace ftk
  {

    struct lamb {
      static long unsigned int lambcfg[MAXLAMB_PER_BOARD];
    };

    /// extern  // = {0x10100101, 0, 0, 0};
    extern unsigned int tms,tdi,init, program, tdo;

//-------------
// need to remove the external all around


//
    //bool ambslp_init	(int slot, int loop, int amb_lamb, int verbose_level=0);

    //
///    bool ambslp_status( int slot, Json::Value & root, int verbose_level=0);

    //
///    bool ambslp_touch_reg(int slot);

    //  
///    bool ambslp_config_reg(int slot, int FixPolarity, int BoardVersion, Json::Value & root, unsigned int dc);

    //  
///    bool ambslp_reset_spy(int slot, int hit, int road, Json::Value & root );
    
    //
///    bool ambslp_amchip_8b_10b(int slot, int lambver);

    //
///    bool ambslp_amchip_8b_10b_all_buses(int slot, int chainlength, int chipnum, Json::Value & root);

    //
//    bool ambslp_amchip_8b_10b_pattout_only(int slot, int chainlength, int chipnum, const char* columns, Json::Value & root);

    //
//    bool ambslp_amchip_8b_10b_pattin_pattout(int slot, int chainlength, int chipnum, const char* columns, Json::Value & root);

    //
///    bool ambslp_amchip_jpatt_cfg(int slot, int chainlength, int chipnum, const char* columns, int thr, int req_lay, int dis_xoram, int dis_top2, int tmode, int dis_pflow, int drv_str, int dcbits, int cmode, bool is_lamb_v1, Json::Value & root);

    //
///    bool ambslp_amchip_init_evt(int slot, int chainlength, int chipnum, Json::Value & root);

    //
///    bool ambslp_amchip_disable_bank(int slot, int chainlength, int chipnum, int npatt, int offset, Json::Value & root);

    //
///    bool ambslp_amchip_enable_bank(int slot, int chainlength, int chipnum, int npatt, int offset, Json::Value & root);

    //
///    bool ambslp_amchip_write_test_bank(int slot, int chainlength, int chipnum, int npatt, int offset, Json::Value & root);

    //
///    bool ambslp_writepatterns(int slot, int chainlength, int chipnum, int npatt, int offset, int type, std::string pattfname, std::string dumpfname, int use_random_bank, bool verbose, bool SSOffset, bool lamb_v1);
    
    //
///    bool ambslp_amchip_writepatterns_dummy_online(int slot, int chainlength, int chipnum, int npatt, int offset, int type, std::string pattfname, std::string dumpfname, int use_random_bank, bool verbose, bool SSOffset, bool lamb_v1);

    //
    //int ambslp_read_IO_spy(int slot, int input, int statusm, Json::Value & root);

    //
    //bool ambslp_read_reg0( int slot, int cont, int verbose, Json::Value & root);

    //
    //bool ambslp_set_config_register(int slot, unsigned int PCB_version, unsigned int enableDVmajority, 
    //unsigned int enableRoadOutput, Json::Value & root );
   
    // here the declarations of functions coming from the splitting of the mains

    // in old ambslp_init.cxx
    bool ambslp_single_AMB_init( int slot, int loop, int verbose_level); 
    bool ambslp_fifo_init( int slot, int loop, int verbose_level); 
    bool ambslp_single_LAMB_init( int slot, int loop, int verbose_level); 
    bool ambslp_conf_init( int slot, int loop, int verbose_level); 
    bool ambslp_init( int slot, int loop, int amb_lamb, int verbose_level);
  


    //----------------------------------


    // Global variables
    // TODO: do we need them?
////    extern unsigned long lambcfg[MAXLAMB_PER_BOARD];
////    extern unsigned int  tms,tdi,init, program, tdo;
    const  unsigned int  lambFullMask = 0xFFFFFFFF;
    const  unsigned int  lambTopMask  = 0xF0F00F0F;
    const  unsigned int  lambBotMask  = ~lambTopMask;

 		
    //	bool amb_configure_lamb(int slot, Json::Value & root   ) ;
		
		
    //int amb_dump_ospy	(int slot, int statusm,  Json::Value & root  );
   
    //TODO: needed here?
    //    bool amb_expected_road( int sort, int thr, int laymap , int road_limit, int eebit, 
    //                           const char * pattfname, const char *hitfname, Json::Value & root  );		
		
    //TODO: needed here?
    //    bool amb_expected_road_DC( int sort, int thr, int laymap , int road_limit, int eebit, 
    //    //                           int DCmax, const char * pattfname, const char *hitfname, Json::Value & root  );		
		
    //TODO: needed here?
    //    int amb_road_diff	(int ignore_bits, int laymap, int skip64, const char * rif_fname,
    //                      const  char * meas_fname,  const char * kill_fname, Json::Value & root   );
		
    //TODO: needed here?
    //    int amb_patt_gen(int nPattPerChip, int mode, int randomseed, int seed, const char * pattfname, 
    //               const char * infname, Json::Value & root );

    //TODO: needed here?
    //		int amb_patt_gen_DC(int nPattPerChip, int mode, int randomseed, int seed, const char * pattfname, 
    //                     const char * infname, Json::Value & root );
		
    //TODO: needed here?
    //    int amb_gen_hits(int nevents, int nroads, int n5roads, int n4roads, int nnoise, int seed, int randomseed, 
    //                     int eebit, const  char * pattfname, Json::Value & root,  bool stratix=false) ; 
		
    //TODO: needed here?
    //		int amb_gen_hits_DC(int nevents, int nroads, int n5roads, int n4roads, int nnoise, int seed, int randomseed, 
    //                    int eebit, int DCmax, double DCprob, const  char * pattfname, Json::Value & root,  bool stratix=false) ; 

  }//namespace ftk
}//namespace daq

#endif
