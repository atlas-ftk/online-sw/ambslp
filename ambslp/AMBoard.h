#ifndef AMBOARD_H
#define AMBOARD_H

/** Author: A. Annnovi, N. Biesuz, G. Volpi
Date: 2016/01/25
*/

#include "ftkcommon/patternbank_lib.h"
#include "ftkcommon/dal/PatternBankConf.h"
#include "ftkcommon/Utils.h"
#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include "ftkcommon/FtkEMonDataOut.h" //Emon

#include "ambslp/AMBSpyBuffer.h"

#include "ambslp/ambslp.h"
#include "ambslp/AMChip.h"
#include "ambslp/PhaseTest.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/ambslp_jtag_func.h"
#include "ambslp/ambslp_vme_jtag_func.h"
#include "ambslp/dal/ambslpNamed.h"
#include "ambslp/dal/AMBStandaloneTest.h"
#include "ProcessingUnit/dal/ReadoutModule_PU.h"
#include "ambslp/dal/CHIP_config.h"

#include <dal/OnlineSegment.h>
#include <dal/ResourceSet.h>
#include <dal/Segment.h>
#include <dal/Partition.h>
#include <dal/seg-config.h>
#include <dal/util.h>
#include "DFdal/ReadoutConfiguration.h"

#include "ftkdqm/EventFragmentAMBRoad.h"
#include "ftkdqm/EventFragmentAMBHit.h"

#include <memory>

namespace daq {
  namespace ftk {

    class AMBoard {

      public:

        typedef enum { DEBUG=0, INFO=1, WARNING=2, ERROR=3 } VerbosityLevel;

        typedef enum { AMBvNONE=0, AMBv3, AMBv4, AMBv5} BoardVer;

        //        /** enumerate the type of LAMB supperted. LAMBv2 is ok for all version >1 */
        //        typedef enum { LAMBvNONE=0, LAMBv1=1, LAMBv2=2} LAMBVer;


      private:

        //AMB data members read from OKS in the Setup function
        unsigned int               m_hitfwver;       /**< hit firmware version */
        unsigned int               m_roadfwver;      /**< road firmware version */
        unsigned int               m_vmefwver;       /**< vme firmware version */
        unsigned int               m_ctrlfwver;      /**< ctrl firmware version */
        unsigned int               m_lambfwver;      /**< lamb firmware version */
        std::string                m_patternBankFileName;    /**< The Pattern Bank to be laoded    */
        unsigned int               m_NPattPerChip;   /**< Number of patterns to loaded in each chip */
        unsigned int               m_banktype;       /**< pattern bank type: 0 for root format, 1 for ascii format */
        bool                       m_ssoffset;       /**< superstrip offset to avoid SSID 0 which would always match */
        bool                       m_ForceWrite;     /**<When this flag is set skips the checksum check and force the load of a new bank */
        unsigned int               m_ExpAMBChecksum; /**< This is the value of the bank that is expected to be loaded */
        unsigned int               m_lambmask ;
        unsigned int               m_initThr ;       // initialization threshold, used during high-level chip procedures
        unsigned int               m_matchThr;       // matching threshold, used during high level chip procedures
        unsigned int               m_DummyHit;       // Dummy hit word, used to control the minimum power consumption
        unsigned int               m_FreezeSeverityError; //!< flag that control the freeze of the SBs in case of errors
        unsigned int 		   m_SlowLinkMask;	 // Slow loink mask register to be set			
        unsigned int               m_nAM06LinkErrors;  // Number of AM06 link errors
        unsigned int               m_nHITLinkErrors;  // Number of HIT FPGA link errors
        unsigned int               m_nROADLinkErrors;  // Number of ROAD FPGA link errors
        unsigned int               m_nAM06Align;  // Number of AM06 alignments
        unsigned int               m_nHITAlign;  // Number of HIT FPGA alignments
        unsigned int               m_nROADAlign;  // Number of ROAD FPGA alignments
        unsigned int               m_nPattCheckInterval;  // Number of Patterns to be checked
				double                     m_PattProbTolerance; // Maximum tolerance (inpercentage) of problematic patterns in AM06

        unsigned int               m_NumberOfPatternsToCheck; /**< Number of patterns to be checked per chip */
        bool                       m_skip_load_and_check;     /**< Flag to skip pattern-loading-&-check if NumberOfPatternsToCheck == 0 */

        bool                       m_useblock;       /**< Use block transfer mode for AMchip Jtag */
        bool                       m_useLaymap ;
        bool                       m_dualFace ;
        std::string                m_dumpFolder ;
        bool                       m_checkBank ;
        int                        m_bypassBus ;
        int                        m_pcbVersion ;
        bool                       m_dvMajority ;
        bool                       m_DoStandaloneTest;
        bool                       m_testConfLoop ;
        bool                       m_CanPublish;
        bool                       m_CanDoPhase;
        std::string                m_testFilePath ;
        //end

        // EMon
        daq::ftk::FtkEMonDataOut *m_ftkemonDataOut;  ///< Store pointer to EMonDataOut module

        // AM board HW parameters read from HW itself
        unsigned int    m_LAMB_mask;

        static unsigned int LinkIDToLinkWMEKMap[];
        static unsigned int LinkIDToLinkWMEFIFOMap[];

        // AMchip DC/DC related constants and parameters
        unsigned int m_DCDC_val_Vout; // Set the voltage for the chip core, 1.15 V => 0x02e30000;

        // data members assigned during construction or by the user
        int          m_slot;    // slot number, -1 means no slot assigned
        std::string  m_name;    // Card name

        // data members derived during the lifecycle
        VMEInterface *m_vme;       // VME interface pointer

        BoardVer m_AMBVersion;     // AMBoard version

        //        LAMBVer m_LAMBVersion;                // LAMB version, is not actually available through VME or registers

        int m_chainlength;                    // size of the JTAG chain for the chips

        unsigned int m_NMaxChips;             // total number of AM chips handled by the board
        unsigned int m_NMaxLAMBs;              // number of LAMB cards that the board can hold
        unsigned int m_NColumns;             // total number of JTAG chain for chip configuration available
        unsigned int m_NChipsPerLAMB;         // total number of chips that is possible to install within the LAMB

        unsigned long int m_LAMBPresence;               // bitmask representing the installed LAMBs, according the presence mask

        unsigned long int m_DCDCstatus;                 // DCDC status

        unsigned long int m_LAMBMap;                    // bitmask representing the installed LAMBs

        unsigned long int m_HITFWVersion;   // firmware version of the HIT  FPGA
        unsigned long int m_ROADFWVersion;  // firmware version of the ROAD FPGA
        unsigned long int m_CTRLFWVersion;  // firmware version of the CTRL FPGA
        unsigned long int m_VMEFWVersion;   // firmware version of the VME  FPGA
        unsigned int      m_LAMBFWVersion;  // firmware version on the LAMB FPGAs

        unsigned long int m_AMChipMap;      // bitmask representing the installed chips, in the most common case can be derived but LAMBMap

        std::vector< std::shared_ptr< AMBSpyBuffer > > m_InputSpyBuffer;
        std::vector< std::shared_ptr< AMBSpyBuffer > > m_OutputSpyBuffer;

        AMChip *m_AMChip;                               // pointer to AMChip: this will store all information about used AMChips

        bool m_AddTestPattern;

        bool m_OverrideSpecialPattern; // if true the special pattern, in the bank, are overwritten to ensure the SS values

        unsigned int m_WCSS; // WC value

        FTKPatternBank m_PatternBank; // object describing a patterns bank

        VerbosityLevel m_VerbosityLevel;

        PhaseTest m_PhaseTest;

     private:

        void readConfig();

        void configure();

        int getJtagChainInfo(int &chip_mask, int &columns_mask);

        void read_AM_idcodes();                         // read the id code of AM chips on AMBs. LAMBs are identified by 4 enabled bits in the chip map

        void write_single_pattern(int slot, int chainlength, int chipnum, int active_columns, int patt_number,uint32_t *vme_data, bool write_pattern=true);

        void cfg2vmedata(uint32_t *, int, int, int, int, int, int, int, int, int, int);

        bool amchip_SERDES_setup(int chainlength, int chipnum, unsigned int active_columns, const char* serdes, const char* operation_mode, float frequency);

				/**@fn unsigned int getGeographicalAddress(unsigned int chainNumber, unsigned int chipNumber)
				* 
				* @brief Returns the AMchip geographical address given the AMchip JTAG chain number and the chip position in the JTAG chain
				* 
				* @param chainNumber The AMchip JTAG chain number, also referred as the column number
				* @param chipNumber The AMchip JTAG pisition in the JTAG chain ()
				*/
				unsigned int getGeographicalAddress(unsigned int chainNumber, unsigned int chipNumber)
				{
					if(chainNumber >= 32){
						string message ("Got invalid chain number: ");
						message += chainNumber;
						daq::ftk::ftkException issue(ERS_HERE, name_ftk(), message);
						throw issue;
					}
					if(chipNumber >= 2){
						string message ("Got invalid chip number: ");
						message += chipNumber;
						daq::ftk::ftkException issue(ERS_HERE, name_ftk(), message);
						throw issue;
					}
					unsigned int lamb= chainNumber/8; //lamb number is thechain index divided as integer by the number of chain per lamb
					unsigned int link = (chainNumber%8)/2; //the link number is the link index inside the lamb divided by the number of JTAG chains belonging to the same link
					unsigned int chip = ((chainNumber%2)*2)+chipNumber; //the chip number
					// building the geographical address for AM06
					// This geographical position value is what appears in bits 22-17 in the AMB output format
					// See https://twiki.cern.ch/twiki/bin/view/Atlas/FastTrackerHardwareDocumentation#AMB%20output%20data%20format
					return ((lamb<<4)+(link<<2)+chip); 
				}

        void runPhaseTest(uint32_t [64], bool isTest=false);

        void runPatternTest(uint32_t [64], bool isTest=false);

				void check_FPGA_FW_version(std::string FPGA_name);


			public: // to be reconsidered if need to be public or not

				bool ambslp_config_reg(int, unsigned int dc=0xff); // configures basic AMB registers

			public:
        /// automatically called by every ERS_LOG to print the RM name
        inline std::string name_ftk() { return m_name; }

        /// make spybuffers available to emon;
        void shipSpyBuffer(std::vector< std::shared_ptr< AMBSpyBuffer > > & vecSpyBuffers, bool isHit=true);

        //Methods used to return some data_members read from RC_setup. This is required because they have 
        //been moved from RM to the AMB library, and a methods that can be called by RC is required 
        bool        get_DoStandaloneTest()  {return m_DoStandaloneTest;}
        bool        get_testConfLoop()      {return m_testConfLoop;}
        std::string get_testFilePath()      {return m_testFilePath;}

        bool getDoStandaloneTest() const {return m_DoStandaloneTest;}
        void setDoStandaloneTest(bool doStandaloneTest) {m_DoStandaloneTest = doStandaloneTest;}

        bool getTestConfLoop() const {return m_testConfLoop;}
        void setTestConfLoop(bool testConfLoop) {m_testConfLoop = testConfLoop;}

        std::string getTestFilePath() const {return m_testFilePath;}
        void setTestFilePath(std::string testFilePath) {m_testFilePath = testFilePath;}

        explicit AMBoard(int slot, VerbosityLevel verb=WARNING);                                                   //Class constructor: /* TODO This does not need to be explicit, you F****** M*****!

        BoardVer getAMBVersion() const { return m_AMBVersion; } // return the version of the AMB, read from VME

        //        LAMBVer getLAMBVersion() const { return m_LAMBVersion; }

        unsigned int getNMaxChips() const { return m_NMaxChips; }
        unsigned int getNMaxLAMBs() const { return m_NMaxLAMBs; }
        unsigned int getNColumns() const { return m_NColumns; }
        unsigned int getNChipsPerLAMB() const { return m_NChipsPerLAMB; }

        inline void setVerbosity(VerbosityLevel level);                               //Set the verbosity

        inline VerbosityLevel getVerbosity() const;                                   /*Get the verbosity level: using this method prevents
                                                                                        accidental change of m_VerbosityLevel */

        VMEInterface *getVME() { return m_vme; }                                      //Get the vme interface

        const AMChip *getAMChip() const { return m_AMChip; }                          //Get the amchips of the board ()

        int getSlot() const { return m_slot; }                                        //Get the crate slot that you are using

        unsigned long int getLAMBMap() const { return m_LAMBMap; }                    //Get the LAMB map, a mask of the plugged LAMB cards

        unsigned long int getAMChipMap() const { return m_AMChipMap; }                //Get amchip map, a mask of chips per jtag chain

        void configureDummyHit();
        void unconfigDummyHit();

        std::shared_ptr< AMBSpyBuffer > getInputSpyBuffer(unsigned int i) const { return m_InputSpyBuffer[i]; }
        std::shared_ptr< AMBSpyBuffer > getOutputSpyBuffer(unsigned int i) const { return m_OutputSpyBuffer[i]; }

        int getInputSpyBufferSize() const {return m_InputSpyBuffer.size(); }
        int getOutputSpyBufferSize() const {return m_OutputSpyBuffer.size(); }

        const unsigned long int& getLAMBPresence() const { return m_LAMBPresence; }   //Map of the LAMB presence pins

        void setAddTestPattern(bool flag=true) { m_AddTestPattern = flag; } // set the flag for the use of a test pattern
        bool getAddTestPattern() const { return m_AddTestPattern; } // return the flag related to the presence of a test pattern

        void setOverrideSpecialPattern(bool flag=true) { m_OverrideSpecialPattern = flag; }
        bool getOverrideSpecialPattern() const { return m_OverrideSpecialPattern; }

        void setWCSSValue(unsigned int ss) { m_WCSS = ss; } // Set the WC value for the pattern bank
        unsigned int getWCSSValue() const { return m_WCSS; } // get the WC value

        void setInitThreshold(unsigned int thr) { m_initThr = thr; }
        unsigned int getInitThreshold() const { return m_initThr; }

        void setMatchThreshold(unsigned int thr) { m_matchThr = thr; }
        unsigned int getMatchThreshold() const { return m_matchThr; }

        void setDummyHit(unsigned int dh) { m_DummyHit = dh; }
        unsigned int getDummyHit() const { return m_DummyHit; }

        /** Set the interest in freezing the spy-buffers in case of specific errors.
         * Each bit is associated to an error conditions:
         *    * Bit 0: errors in HIT, loss-of-synch
         *    * Bit 1: error in road
         *    * Bit 2: error from  AUX
         */
        /* void setFreezeSeverityError(unsigned int flag) { m_FreezeSeverityError = flag; } */
        /* unsigned int getFreezeSeverityError() const { return m_FreezeSeverityError; } */

        bool init(int level, int loop=1); //Send an init signal the board, the result depends from the level

        bool check_HIT_GTP_aligned(unsigned int ROAD_active_links);
        unsigned int check_ROAD_GTP_aligned(unsigned int ROAD_active_links);
        int reset_gtp(int max_tries, bool force, int mode=0x3); // reconfigure the GTPs for HIT and/or ROAD
        bool amchip8b10b_up_and_check(int part=0x3, int max_tries=10, int chainlength=2, int chipnum=3, bool do_config=true);
        bool amchip8b10b_count_errDout32(std::vector<std::vector<unsigned int>>& errorDout32_count, std::vector<std::vector<unsigned int>>& rx_stream_error, std::vector<std::vector<unsigned int>>& tx_stream_error, int part=0x3, int chainlength=2, int chipnum=3, int time=100000, int serDesMode=0x0);
        bool amchip8b10b(int part=0x3, unsigned int chipmask = 0xffffffff, unsigned int active_columns = 0xffffffff); //

        bool amchip8b10b_smart(int nTries = 25, bool isTest = false); 

        int read_link_errors(int, int, const char*, const char*);

        bool pilot_error_count(const char*, const char*, bool);

        bool DTEST_select(const char*, const char*, bool);

        uint64_t read_chip_DTEST();

        bool amchip_jpatt_cfg(int, const char*, int, int, int, int, int, int, int, int);

        bool amchip_reg_healthcheck(int chipnum=0x3, int chainlength=2, int reg_write=CONFIG_REG, int reg_read=CONFIG_REGrd, int reg_length=CONFIG_LENGTH, int n_checks=10);

        bool amchip_init_evt(int chipnum);

        const FTKPatternBank& getPatternBank() const { return m_PatternBank; }

        int readPatternBankDCconfig(int type, std::string pattfname, std::vector<uint32_t> &ndc);

        int readPatternBank(int type, std::string pattfname, bool SSOffset, std::vector<uint32_t> &ndc);

        int prepareRandomPatternBank(bool SSOffset);

        int prepareSequentialBank(int maxSS, bool SSOffset);

        bool writepatterns(int npatts, int offset, bool verbose, bool test_written_pattern=true, int test_interval=512, bool write_pattern=true);

        bool checkPatternBankChecksum(unsigned int val);

        bool disable_bank(int chainlength, int chipmask, int nPattPerChip);   //Disable pattern bank


        void feed_hit_init(); // initialize the feed of hits
        void feed_hit_end(bool loop=false); // initialize the feed of hits
        void feed_hit(const char *file, int link); // load the HIT input fifo of a specific link, 2 columns file format
        void feed_hit(const char *file); // load the HIT input FIFO of all the link, 12 columns with 36 bit format
        void feed_hit(const unsigned long int *data, const unsigned int ndata, int link); // write a stream of data in a specific link

        void getNMissingTestMatches(uint32_t [64]); // check the counters of the missing matches for test patterns

        // Run Control related procedures
        void RC_setup(const dal::ReadoutModule_PU*); 		// full setup procedure as required by RC
        void RC_configure(); 					// full configure procedure as required by RC
        void RC_connect(); 					// full connect procedure as required by the RC
        void RC_publishFull(std::vector<EventFragmentCollection*> &Road, std::vector<EventFragmentCollection*> &Hit);       // publishFullStat procedure (i  t miss only the publication in IS and OH)
        void Configure_AMchips(); 				// Sets the configuration for the AM chips
        bool HL_writebank(int npatts, bool firstTime=true, bool test_written_pattern=true, int test_interval=512, bool write_pattern=true);   // sequence of standard step used to write a bank
        void RC_prepareForRun(); 				// set the board in a confition that is able to receive data
        void RC_stop(); 					// sequence to stop data taking
        void RC_unconfigure(); 					// sequence to unconfigure the board
        void RC_checkLinks();                                   //sequence to check the link status 
        void HL_checkpatterns(int npatts, int interval=512);    // sequence for checking patterns written in the AMchips
        void cleanup(); 					// function to clean remnant patterns, reset FIFOs, reset spybuffers
        void idle_mode(); 					// AMB in idle mode and ignore/disable HOLD
        bool Empty_fifo();					// Function to check if the AMB FIFOs has been cleaned (if are empty)

        // DC/DC converter methods
        void DCDC_setVLimits(); 				// set the Voltage limits for the DC/DC converters (now LAMB's DCDCs only)
        void DCDC_setVOutputs(); 				// set the output voltages for the DC/DC converters
        std::vector<float> DCDC_read(bool printLog=true); 	// read values from DC/DCs

        // Spy buffer functions
        bool freezeSpyBuffers();
        bool unfreezeSpyBuffers();

	bool spys_freezed;

        void readInputSpyBuffers();
        void readOutputSpyBuffers();

        bool reset_spy(u_int addr, std::string success_message, std::string fail_message);  // This can probably stay private [tbc]
        bool reset_spy_hit();
        bool reset_spy_road();
        bool resetSpyBuffers(int hit, int road);

        void print_ISPY(u_int address);
        void print_OSPY(u_int address);
        bool inputSpyBuffers( int method, int keepFreeze);
        bool outputSpyBuffers(int method, int keepFreeze);



        int testExtraLines(int slot, bool verb); // Tests the extra Lines, DTEST

        void read_CRC(); // read the CRC values from the chips
    };

    /** @fn SetVerbosity
     * @param level The lowest level of verbosity of the output, AMBoard::DEBUG, AMBoard::INFO, AMBoard::WARNING, AMBoard::ERROR;
     * @brief This method stes the verbosity level for the AMBoard class instance. Single functions may override this value.
     */
    inline void AMBoard :: setVerbosity( AMBoard::VerbosityLevel level = AMBoard::WARNING ){ m_VerbosityLevel = level; }

    /** @fn getVerbosity
     * @return It returns an enum type VerbosityLevel, lower values refer to higher verbosity;
     * @brief Returns the verbosity level set for the AMBoard class instance
     */
    inline AMBoard::VerbosityLevel AMBoard :: getVerbosity() const { return m_VerbosityLevel; }    
  } // namespace daq
} // namespace ftk

#endif // AMBOARD_H
