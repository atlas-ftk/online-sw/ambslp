/** @author Nicolo' Vladi Biesuz <nicolo.vladi.biesuz@cern.ch>, Alberto Annovi <alberto.annovi@cern.ch>
 * @file This file contains the declaration of AMchip structures, Each structure represents a specific AMChip type (AMChip (generic), AMchip05, AMChip06)
 */


#ifndef ambslp_AMChip_H
#define ambslp_AMChip_H

#include "ambslp/ambslp.h"

/** This a struct with the parameters of a generic AMChip
 */
class AMChip {
 public:
  AMChip();
  /*! \brief Access the global AMChip instance
   */
  static AMChip *global();
  static std::pair<unsigned int, unsigned int> get_serdes_number(const char*);
  static unsigned int get_operation_mode_number(const char*);
  inline bool operator == (AMChip&);

  std::string getNAME() const { return m_NAME; }
  unsigned getIDCODE_VAL() const { return m_IDCODE_VAL; }
  unsigned getMAXPATT_PERCHIP() const { return m_MAXPATT_PERCHIP; }
  unsigned getADDR_SIZE() const { return m_ADDR_SIZE; }
  unsigned getSELFTEST_REG_SIZE()const { return m_SELFTEST_REG_SIZE; }

 protected:
  std::string m_NAME;
  unsigned m_IDCODE_VAL;
  unsigned m_MAXPATT_PERCHIP;
  unsigned m_ADDR_SIZE;
  unsigned m_SELFTEST_REG_SIZE;
};


class AMChip05: public AMChip
{
 public:
  AMChip05();
  /*! \brief Access the global AMChip instance
   */
  static AMChip05 *global();
};


class AMChip06 : public AMChip
{
 public:
  AMChip06();
  /*! \brief Access the global AMChip instance
   */
  static AMChip06 *global();
};


/** operator == for AMChips, it checks the AMChips are of the same type by looking at their NAME
 * @brief true if the AMChip have the same type else false
 */
bool AMChip :: operator==(AMChip& other) {
  if(strcmp( (this->m_NAME).c_str(), (other.m_NAME).c_str())==0) return true;
  else return false;
}


#endif
