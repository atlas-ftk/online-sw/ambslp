/********************************************************/
/*                                                      */
/********************************************************/

#ifndef AMBSLP_TEST_H
#define AMBSLP_TEST_H 

#include <string>

//#include "ipc/partition.h"
//#include "ipc/core.h"

#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_mainboard.h"

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/OnlineSegment.h>
#include <dal/ResourceSet.h>
#include <dal/Segment.h>
#include <dal/Partition.h>

#include <dal/seg-config.h>
#include <dal/util.h>

#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"

#include "ambslp/dal/ReadoutModule_Ambslp.h"
#include "ambslp/dal/CHIP_config.h"
#include "ambslp/dal/ambslpNamed.h"

#include "ambslp/ambslp_vme_regs.h"

//#include "ambslp/DataFlowReaderAMB.h"
//#include "ambslp/StatusRegisterAMBFactory.h"
//#include "ambslp/EventFragmentAMBRoad.h"
//#include "ftkcommon/EventFragmentCollection.h"
//#include "ftkcommon/SourceIDSpyBuffer.h"

// EMon
//#include "ftkcommon/FtkEMonDataOut.h"
#include <memory> //for shared pointers
#include <utility> //for pairs

namespace daq {
namespace ftk {

  class ambslp_test
  {
  public:  // Constructor and destructor

    /// Constructor (NB: executed at CONFIGURE transition)
    ambslp_test(uint _slot);
 
    /// Destructor (NB: executed at UNCONFIGURE transition)
    //virtual ~ambslp_test();

  public: // overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin
    
    /// Set internal variables (NB: executed at CONFIGURE transition)
    void setup(const dal::ReadoutModule_Ambslp*);
    
    /// Reset internal statistics
    //virtual void clearInfo();
   
    /// Get the list of channels connected to this ReadoutModule
    //virtual const std::vector<ROS::DataChannel *> *channels(); 

    /// Set the Run Parameters
    //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);
    
    /// Get values of statistical counters
    // virtual DFCountedPointer<Config> getInfo();

    /// Get access to statistics stored in ISInfo class
    // virtual ISInfo* getISInfo();
  
  public: // overloaded methods inherited from RunControl::Controllable

    /// RC configure transition
    void configure();
    
    /// RC connect transition: 
    void connect();

    /// RC start of run transition
    void prepareForRun();
   

    /// RC stop transition
    void stopDC();
    // void stopROIB();
    // void stopHLT();
    // void stopRecording(); 
    // void stopGathering(); 
    // void stopArchiving(); 
    
    /// RC unconfigure transition 
    void unconfigure();
    
    /// RC disconnect transition: 
    void disconnect();

    /// RC subtransition (IF NEEDED)
    // void subTransition(const daq::rc::SubTransitionCmd& cmd);
    
    // Method called periodically by RC
    //void publish(void);
    
    // Method called periodically with a longer interval by RC
    // void publishFullStats();

    /// RC HW re-synchronization request
    void resynch();


  private:
    //// Dump a JSon based SpyBuffer on file (TODO: get rid of it!)
    //// void dumpSpybufferOnFile(Json::Value &info);
    
    /// Example method that read a spybuffer, dumps it in *data and returns the size 
    uint32_t readSpyBufferExample(uint32_t* &data);

    /// Read spyBuffer
    //void readSpyBuffer(std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > >& spyBuffers);

    /// make spybuffers available to emon;
    //void shipSpyBuffer(std::vector< std::shared_ptr< AMBSpyBuffer > > & vecSpyBuffers);

  private:

    //std::vector<ROS::DataChannel *>  m_dataChannels;   
    //DFCountedPointer<ROS::Config> 	 m_configuration;	/**< Configuration Object, Map Wrapper */
    //std::string 					           m_isServerName;	/**< IS Server	*/
    //IPCPartition 					           m_ipcpartition;	/**< Partition	*/
    uint32_t    					           m_runNumber;			/**< Run Number	*/
    bool                             m_dryRun;        ///< Bypass VME calls
    //bool                             m_localLoop;        ///< Do local loop
    //::ftk::ambslpNamed              *m_ambslpNamed;   ///< Access IS via schema
    //OHRootProvider                  *m_ohProvider;    ///< Histogram provider
    //OHRawProvider<>                 *m_ohRawProvider;    ///< Raw histogram provider
    //DataFlowReaderAMB               *m_dfReaderAmb;   ///< DataFlowReaderAMB

    // EMon
    //daq::ftk::FtkEMonDataOut                *m_ftkemonDataOut;  ///< Store pointer to EMonDataOut module
    
    // AM BOARD Specific Parameters
    std::string     m_name;				/**< Card name	*/
    unsigned int    m_slot;				/**< Slot	*/
    std::string	    m_patternBank;			/**< The Pattern Bank to be laoded		*/
    unsigned int    m_NPattPerChip; /**< Number of patterns to loaded in each chip */
    unsigned int    m_banktype;         /**< pattern bank type: 0 for root format, 1 for ascii format */
    bool            m_ssoffset;            /**< superstrip offset to avoid SSID 0 which would always match */
    bool            m_ForceWrite; /**< When this flag is set skips the checksum check and force the load of a new bank */
    unsigned int    m_mask ;	
    unsigned int    m_initThr ;
    unsigned int    m_thr ;
    bool            m_useLaymap ;
    bool            m_dualFace ;
    std::string     m_dumpFolder ;
    bool            m_checkBank ;
    int             m_bypassBus ;
    int             m_pcbVersion ;
    bool            m_dvMajority ;
    // Histograms
    //TH1F           *m_hlast_lvl1id;
    //TH1F           *m_hnroads;
    //TH1F           *m_hgeoaddress;

    // AM board HW parameters read from HW itself
    unsigned int    m_LAMB_mask;

    // AMBoard class instance
    std::unique_ptr<AMBoard> m_AMB;

    //StatusRegisterAMBFactory* m_statusRegisterAMBFactory;
    void reset_weak_config();
  };

  /* inline const std::vector<ROS::DataChannel *> *ambslp_test::channels() */
  /* { */
  /*   return &m_dataChannels; */
  /* } */

} // namespace ftk
} // namespace daq
#endif // AMBSLP_TEST_H
 
