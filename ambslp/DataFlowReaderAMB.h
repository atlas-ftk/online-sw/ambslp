#ifndef DATAFLOWREADERAMB_H
#define DATAFLOWREADERAMB_H

#include "ftkcommon/DataFlowReader.h"
#include "ambslp/dal/FtkDataFlowSummaryAMBNamed.h"
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_vme_regs.h"

#include <string>
#include <vector>


// Namespaces
namespace daq {
namespace ftk {

/*! \brief  AMB implementation of DataFlowReader
*/
class DataFlowReaderAMB : public DataFlowReader
{
public:
    /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
     *  \param ohRawProvider Pointer to initialized  OHRawProvider object
     *  \param forceIS Force the code to use IS as the source of information (default is 0)
     */
  DataFlowReaderAMB(std::shared_ptr<OHRawProvider<>> ohRawProvider, 
		bool forceIS = false);

  void init(const string& deviceName,
	    const std::string& partitionName = "FTK",
	    const std::string& isServerName = "DF");

    /*! \brief Get fraction of BUSY from downstream
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    //void getBusyFraction(std::vector<float>& srv, uint32_t option = 0);

    /*! \brief Get fraction of Input FIFO BUSY 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    void getFifoInBusyFraction(std::vector<float>& srv, uint32_t option = 0);
    /*! \brief Get fraction of Output FIFO BUSY 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    //void getFifoOutBusyFraction(std::vector<float>& srv, uint32_t option = 0);
    /*! \brief Get fraction of Input FIFO Empty 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    void getFifoInEmptyFraction(std::vector<float>& srv, uint32_t option = 0);
    /*! \brief Get fraction of Output FIFO Empty 
     *
     *  \param option The type of the average, depends on particular implementation
     *
     */
    //void getFifoOutEmptyFraction(std::vector<float>& srv, uint32_t option = 0);


    /*! \brief Get current Input FIFO BUSY 
     */
    void getFifoInBusy(std::vector<int64_t>& srv);
    /*! \brief Get current Output FIFO BUSY 
     */
    void getFifoOutBusy(std::vector<int64_t>& srv);
    /*! \brief Get current Input FIFO Empty 
     */
    void getFifoInEmpty(std::vector<int64_t>& srv);
    /*! \brief Get current Output FIFO Empty      
     */
    void getFifoOutEmpty(std::vector<int64_t>& srv);

    /*! \brief Get L1ID seen
     */
    void getL1id(std::vector<int64_t>& srv);

    /*! \brief Get data errors on each Input channel
     */
    //void getDataErrors(std::vector<int64_t>& srv);

    /*! \brief Get event rate on each Input channel
     */
    //void getEventRate(std::vector<int64_t>& srv);

    /*! \brief Get status of input links (0: ON, 1: OFF, 2: Errors seen)
     */
    void getLinkInStatus(std::vector<int64_t>& srv);

    /*! \brief Get status of output links (0: ON, 1: OFF, 2: Errors seen)
     */
    void getLinkOutStatus(std::vector<int64_t>& srv);

    /*! \brief Get event rate on each Input channel
     */
    void getFpgaTemperature(std::vector<int64_t>& srv);

    /*! \brief Get the number of events processed in the HIT FPGA                                                                                         
     */                                                                                                                                                   
    void getNEventsProcessed(std::vector<int64_t>& srv); 
    
    /*! \brief Get PLL lock from the HIT FPGA
     */
    void getAmbPllLockIn(std::vector<int64_t>& srv); 

    /*! \brief Get PLL ref lock from the HIT FPGA
     */
    void getAmbPllRefLockIn(std::vector<int64_t>& srv); 

    /*! \brief Get PLL lock from the Road FPGA
     */
    void getAmbPllLockOut(std::vector<int64_t>& srv); 

    /*! \brief Get PLL ref lock from the Road FPGA
     */
    void getAmbPllRefLockOut(std::vector<int64_t>& srv); 

    /*! \brief Get general status of the Road FPGA: TMODE (0), PLL lock (1-4), PLL REF LOCK (5-8)
     */
    void getAmbStatusOut(std::vector<int64_t>& srv);

    /*! \brief Get FSM states from HIT FPGA
     */
    void getAmbFsmStateIn(std::vector<int64_t>& srv);

    /*! \brief Get Lost of Sync error counter from HIT FPGA
     */
    void getAmbLostSyncCntIn(std::vector<int64_t>& srv);

    /*! \brief Get AUX hold status from ROAD FPGA
     */
    void getAmbAuxHoldOut(std::vector<int64_t>& srv);

    /*! \brief Get missing pattern X counters from ROAD FPGA
     */
    void getAmbMissingPatternXOut(std::vector<int64_t>& srv);

    /*! \brief Get missing pattern X counters from ROAD FPGA
     */
    void getAmbRx8b10bDisparityOut(std::vector<int64_t>& srv);

    /*! \brief Get freeze status
     */
    void getAmbFreezeStatus(std::vector<int64_t>& srv);

    /*! \brief Publish extra distributions to IS as histograms
     *  \param option To be defined (default is 0)
     */
    void publishExtraHistos(uint32_t option = 0);  



private:
    std::vector<std::string> getISVectorNames();
    std::shared_ptr<FtkDataFlowSummaryAMBNamed> m_theAMBSummary;

private:

};

} // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERAMB_H */
