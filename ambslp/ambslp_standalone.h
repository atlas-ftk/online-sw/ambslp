
#ifndef AMBSLP_STANDALONE_H
#define AMBSLP_STANDALONE_H


#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/OnlineSegment.h>
#include <dal/ResourceSet.h>
#include <dal/Segment.h>
#include <dal/Partition.h>

#include <dal/seg-config.h>
#include <dal/util.h>

#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"

#include "ambslp/dal/ReadoutModule_Ambslp.h"

#include "ambslp/ambslp.h"



#endif  //  AMBSLP_STANDALONE_H

