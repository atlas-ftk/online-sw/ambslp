/*           AMBSLP mainboard library                                          */
/*           Written for AMBSLP FTK collaboration                              */
/*           Starting date: 2015-07-03                                         */
/*           Authors: Annovi, Biesuz, Kubota, Volpi                            */

#ifndef AMBSLP_MAINBOARD_H
#define AMBSLP_MAINBOARD_H

// IS includes
#include <is/info.h>
#include <is/inforeceiver.h>
#include <is/infodynany.h>
#include <ipc/core.h>

#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/StatusRegister/StatusRegisterISVector.h"
#include "ftkcommon/ReadSRFromISVectors.h"

#include "ftkdqm/EventFragmentAMBRoad.h"
#include "ftkdqm/EventFragmentAMBHit.h"
#include "ftkdqm/AMBRoadObjectEvent.h"
#include "ftkdqm/AMBHitObjectEvent.h"

#include "ambslp/AMBoard.h"

namespace daq {
  namespace ftk {
    const int AMB_INPUT_CHANNELS = 12;

    u_int readregister(u_int regname, int amc, bool isISmode=false,
		       std::vector< std::vector<unsigned int> > statregc  = std::vector< std::vector<unsigned int> >(),
		       std::vector< std::vector<unsigned int> > metainfoc = std::vector< std::vector<unsigned int> >());
    bool ambslp_status( int slot, int verbose_level, std::string is_part_name="");
    bool ambslp_configure_lamb(int slot);
    int  ambslp_feed_hit( int slot, int loopfifovme, const char* hitfname, int verbose=0 );
    int  ambftk_feed_road(int slot, int loopFifoVme, const char* roadfname );
    int  ambslp_road_diff( int ignore_bits, int laymap, int skip64, const char * rif_fname, const char * meas_fname, const char * kill_fname );
    bool ambslp_test_VME(VMEInterface *vme, int loop);

    // to be cheched: is still used?
    int ambslp_dump_ospy( int slot, int /* statusm */);

    // spy_buffer functions moved to AMBoard (AMBoard_spybuffers.cxx)
    /* bool ambslp_reset_spy(int slot, int hit, int road ); */
    /* int ambslp_inp_spy(int slot, int method, int keepFreeze); */
    /* int ambslp_out_spy(int slot, int method, int keepFreeze); */

  } //namespcae ftk
} //namespcae vmelib

#endif

