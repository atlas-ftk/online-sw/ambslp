

#ifndef AMB_HARDWARE_STATUS_API_H
#define AMB_HARDWARE_STATUS_API_H

//
// this header describes the API to ambslp boostrap autoconfiguration
// the reason is, we do not want to expose all the classes, methods 
// and so on which defines low-level behaviour of the bootstrap
//
//


#include "ambstatus/status.h"
#include "ambstatus/optionHandle.h"
#include "ambstatus/configuration.h"
#include "ambstatus/configuration_functions.h"



//----------------------------------------------------

#endif  //  AMB_HARDWARE_STATUS_API_H
