/** \addtogroup lib_ROM
 * @{
 *  ambslp_card class declaration
 * 
 *  \file ambslp/ambslp_card.h
 *  \brief ambslp abstraction for holding options, 
 *         settings and emulate the Readout Module behaviour.
 * 
 *  The ambslp_card object defines a placeholder for the am board. 
 * 
 *  This class is for example used by the Readout Module or by standalone
 *  programs, by letting the client to specify settings for the board.
 * 
 *  In order to be initialized, an ambslp_card needs a valid pointer to 
 *  an existing \a ambslp_settings class instance. This implies:
 *    - many different instances of the same ambslp_card are allowed
 *    - a unique connection to a given crate-slot pair is assured by the 
 *      \a VMEManager
 * 
 *  The ambslp_card allows to emulate the Readout Module sequence of operations
 *  by means of
 *    - \a setup()
 *    - \a configure()
 *    - \a connect()
 *    - \a prepareToRun()
 * 
 *  The same sequence can be called by standalone programs.
 *
 *
 *  \class daq::ftk::ambslp_card
 *  \brief ambslp_card class
 * 
 *  for a given ambslp_setting valid pointer, a new ambslp_card instance
 *  can be created.
 * 
 *  The ambslp_card internally discovers the hardware proprierties 
 *  and configurations. 
 * 
 *  In order to allow the replication/checkup of the configuration 
 *  (both settings and hardware status), a \a cloneSetup method can be used
 *  
 *  The returned objects are copies, cannot be used to alter 
 *  the internal status of the current ambslp_card; they can however 
 *  be used for using ambslp library functions which require 
 *  specific inputs.
 * 
 * 

 *  \fn daq::ftk::ambslp_card::ambslp_card( ambslp_settings* settings )
 *  \brief constructor
 *  
 *  \param settings pointer to a valid ambslp_settings struct 
 * 
 *  acquire the ambslp_settings fields, and extrapolate 
 *  the hardware configuration.
 * 
 *  in case of problems, exceptions are thrown.
 *  \throws daq::ftk::Issue_ambslp
 * 

 *  \fn virtual daq::ftk::ambslp_card::~ambslp_card()
 *  \brief descructor
 * 
 *  \a ambslp_card class desctructor
 * 
 *  in case of problems, exceptions are thrown.
 * 

 *  \fn bool daq::ftk::ambslp_card::isConnected()
 *  \brief ask the \a VMEInterface on the vme connection state
 * 
 *  function to ask the status of the vme; delegates to the \a VMEInterface
 *  \return true/false depending on the vme connection to the crate/slot 
 *          specified in the \a settings used for the constructor; 
 *  in case of problems, exceptions are thrown.
 *  \throws daq::ftk::Issue_ambslp
 * 

 *  \fn void daq::ftk::ambslp_card::setup()
 *  \brief emulate the \a setup() method in Readout Module in Run Control
 * 
 *  function which is responsible to setup the ambslp_card
 * 
 *  uses internal status from \a ambslp_settings and \a ambslp_hw 
 *  for setting up the board.
 *  \todo currently only wait 2 sec; to be implemented
 *  \todo throw exceptions
 *  \return void
 * 

 *  \fn void daq::ftk::ambslp_card::configure()
 *  \brief emulate the \a configure() method in Readout Module in Run Control
 * 
 *  function which is responsible to configure the ambslp_card
 * 
 *  uses internal status from \a ambslp_settings and \a ambslp_hw 
 *  for configuring the board.
 *  \todo currently only wait 2 sec; to be implemented
 *  \todo throw exceptions
 *  \return void
 * 

 *  \fn void daq::ftk::ambslp_card::connect()
 *  \brief emulate the \a connect() method in Readout Module in Run Control
 * 
 *  function which is responsible to connect the ambslp_card
 * 
 *  uses internal status from \a ambslp_settings and \a ambslp_hw 
 *  for connecting the board.
 *  \todo currently only wait 2 sec; to be implemented
 *  \todo throw exceptions
 *  \return void
 * 

 *  \fn void daq::ftk::ambslp_card::prepareForRun()
 *  \brief emulate the \a prepareForRun() method in Readout Module in Run Control
 * 
 *  function which is responsible to running the ambslp_card
 * 
 *  uses internal status from \a ambslp_settings and \a ambslp_hw 
 *  for running the board.
 *  \todo currently only wait 2 sec; to be implemented
 *  \todo throw exceptions
 *  \return void
 * 

 *  \fn void daq::ftk::ambslp_card::cloneSetup( ambslp_settings* settings, ambslp_hw* hardware )
 *  \brief clone the \a ambslp_settings and the \a ambslp_hw objects 
 *         from this class instance
 * 
 *  interrogates the internal status to retrieve the \a ambslp_settings 
 *  and the \a ambslp_hw objects; then fills the function arguments 
 *  with the settings.
 * 
 *  This function is not responsible for the creating the settings and the
 *  hardware objects. 
 *  Internally, this method delegates to the \a cloneSetup methods of 
 *  daq::ftk::ambslp_settings and daq::ftk::ambslp_hw
 * 
 *  \param[out] settings pointer to a new daq::ftk::ambslp_settings object
 *              to be filled with the values for 
 *              this daq::ftk::ambslp_card object
 *  \param[out] hardware pointer to a new daq::ftk::ambslp_hardware object
 *              to be filled with the values for 
 *              this daq::ftk::ambslp_card object
 *  \return void; in case of problems, exceptions are thrown.
 *  \throws daq::ftk::Issue_ambslp
 * 

 * 
 * @}
**/

#ifndef AMBSLP_CARD_H
#define AMBSLP_CARD_H

#include <memory>

class  VMEInterface;


namespace daq {
  namespace ftk {


class  ambslp_hw;
struct ambslp_settings;



class ambslp_card {

     public:
      ambslp_card( ambslp_settings* settings );
      virtual ~ambslp_card();

      bool isConnected();
      void cloneSetup( ambslp_settings* settings, ambslp_hw* hardware );

      void setup();
      void configure();
      void connect();
      void prepareForRun();

     private:
      std::unique_ptr<ambslp_settings> m_settings;
      VMEInterface    *m_vme;
      std::unique_ptr<ambslp_hw>       m_hardware;
    };

  }
}


#endif   //  AMBSLP_CARD_H

