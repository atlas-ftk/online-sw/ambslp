
#ifndef FTK_AMBSLP_EXCEPTIONS_H
#define FTK_AMBSLP_EXCEPTIONS_H

#include "ftkcommon/exceptions.h"


namespace daq {


// AMB_generic_issue
//


  ERS_DECLARE_ISSUE( ftk,
    Issue_ambslp,
    "generic issue for ambslp: " << parameter,
    ((std::string)parameter)
  )


  ERS_DECLARE_ISSUE( ftk,
    AMB_standalone_issue,
    "generic issue in standalone for ambslp: " << parameter,
    ((std::string)parameter)
  )


  ERS_DECLARE_ISSUE( ftk,
    CannotConnectToAMB,
    "connection to AMBSLP via VME failed: " << parameter,
    ((std::string)parameter )
  )


  ERS_DECLARE_ISSUE( ftk,
    CannotDisconnectFromAMB,
    "disconnect from AMBSLP via VME failed: " << parameter,
    ((std::string)parameter)
  )

  ERS_DECLARE_ISSUE( ftk,
    ChecksumFailedAMB,
    "checksum control failed from AMBSLP, expected checksum value: " << std::hex << std::setfill('0') << std::setw(8) << expected ,
    ((unsigned int)expected)
  )

  ERS_DECLARE_ISSUE( ftk,
    BadPatternWrittenAMB,
    "bad patterns are written in AM chips: " << parameter,
    ((std::string)parameter)
  )

}  // namespace


#endif  //  FTK_AMBSLP_EXCEPTIONS_H
