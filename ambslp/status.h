
#ifndef AMB_HARDWARE_STATUS_H
#define AMB_HARDWARE_STATUS_H

#include "ambslp/configuration.h"
#include <ctime>

namespace daq {
  namespace ftk {

// store some meta-information about some subject:
//    - is the subject configured?
//    - is it the latest status set?
//    - what time was it configured?
    
    struct StateInfo{
      bool isConfigured;
      bool isLatestStatus;
      struct tm latestConfiguration;
    };

// store the AMB information
    struct Board {
      int slot;
      bool activated_DCDC_up;
      bool activated_DCDC_down;
      StateInfo stateInfo;
    };

// store LAMB information
    struct Lamb {
      int total_numJTagChains;
      int total_JTagChainLength;
      int usable_numJTagChains;
      int usable_JTagChainLength;
      int connected_ROAD_GTP_links;
      int activated_ROAD_GTP_links;
      StateInfo stateInfo;
    };

// store values as they are read from the VME
    struct VMERegisters {
      int connected_LAMBS_mask;
      int enabled_board_DCDC;
      int ROAD_GTP_links_mask;
      StateInfo stateInfo;
    };


// preparing for storing the ATCA registers
    struct ATCARegisters {
      int justADummyTest;
    };

// collect the above information in a single struct
    struct AMBStatus {    
      bool isFullyConfigured;
      ConfigurationStep startFromHere;
      Board board;
      Lamb lamb[4];
      VMERegisters vmeRegisters;
      ATCARegisters atcaRegisters;
    };

  }  // namespace ftk
}  //  namespace daq


#endif  //  AMB_HARDWARE_STATUS_H


