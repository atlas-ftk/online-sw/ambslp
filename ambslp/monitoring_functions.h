
#ifndef AMBSLP_MONITORING_FUNCTIONS_H
#define AMBSLP_MONITORING_FUNCTIONS_H


class VMEInterface;


namespace daq {
  namespace ftk {


    struct HWTemperature{ };


    struct LambTemperature : public HWTemperature{
      unsigned int leftSensor;
      unsigned int rightSensor;
    };



    class TemperatureMonitor{
     public:
      TemperatureMonitor();
      virtual ~TemperatureMonitor();
      HWTemperature * readTemperature( VMEInterface *vme );

     private:
      virtual HWTemperature * read( VMEInterface *vme ) = 0;
    };


    class LambTemperatureMonitor : public TemperatureMonitor{

     public:
      LambTemperatureMonitor();
      virtual ~LambTemperatureMonitor();

     private:
      LambTemperature lambTemperature[4];
      virtual HWTemperature * read( VMEInterface * vme );
    };

  }  // ftk
}  // daq


#endif  //  AMBSLP_MONITORING_FUNCTIONS_H
