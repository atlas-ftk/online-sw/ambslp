/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_AMBSLP_H
#define READOUT_MODULE_AMBSLP_H 


#include "RunControl/Common/OnlineServices.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "is/info.h"
#include "is/infoT.h"
#include "ipc/partition.h"
#include "ipc/core.h"
#include "ROSCore/ReadoutModule.h"
#include "oh/OHRootProvider.h"
#include "oh/OHRawProvider.h"

#include "ftkcommon/dal/PatternBankConf.h"
#include "ftkcommon/Utils.h"
#include "ftkcommon/EventFragmentCollection.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include "ftkcommon/FtkEMonDataOut.h" //Emon

#include "ftkdqm/EventFragmentAMBRoad.h"
#include "ftkdqm/EventFragmentAMBHit.h"

#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_mainboard.h"
#include "ambslp/dal/ambslpNamed.h"
#include "ambslp/dal/AMBStandaloneTest.h"
#include "ambslp/dal/ReadoutModule_Ambslp.h"
#include "ambslp/dal/CHIP_config.h"
#include "ambslp/DataFlowReaderAMB.h"
#include "ambslp/StatusRegisterAMBFactory.h"

namespace daq {
namespace ftk {

  class ReadoutModule_Ambslp : public ROS::ReadoutModule
  {
  public:  // Constructor and destructor

    /// Constructor (NB: executed at CONFIGURE transition)
    ReadoutModule_Ambslp();
 
    /// Destructor (NB: executed at UNCONFIGURE transition)
    virtual ~ReadoutModule_Ambslp() noexcept;

  public: // overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin
    
    /// Set internal variables (NB: executed at CONFIGURE transition)
    virtual void setup(DFCountedPointer<ROS::Config> configuration) override;
    
    /// Reset internal statistics
    virtual void clearInfo() override;
   
    /// Get the list of channels connected to this ReadoutModule
    virtual const std::vector<ROS::DataChannel *> *channels() override; 

    /// Set the Run Parameters
    //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);
    
    /// Get values of statistical counters
    // virtual DFCountedPointer<Config> getInfo();

    /// Get access to statistics stored in ISInfo class
    // virtual ISInfo* getISInfo();
  
  public: // overloaded methods inherited from RunControl::Controllable

    /// RC configure transition
    virtual void configure(const daq::rc::TransitionCmd& cmd) override;
    
    /// RC connect transition: 
    virtual void connect(const daq::rc::TransitionCmd& cmd) override;

    /// RC start of run transition
    virtual void prepareForRun(const daq::rc::TransitionCmd& cmd) override;
   

    /// RC stop transition
    //virtual void stopDC(const daq::rc::TransitionCmd& cmd) override;
    // virtual void stopROIB(const daq::rc::TransitionCmd& cmd) override;
    // virtual void stopHLT(const daq::rc::TransitionCmd& cmd) override;
     virtual void stopRecording(const daq::rc::TransitionCmd& cmd) override; 
    // virtual void stopGathering(const daq::rc::TransitionCmd& cmd) override; 
    // virtual void stopArchiving(const daq::rc::TransitionCmd& cmd) override; 
    
    /// RC unconfigure transition 
    virtual void unconfigure(const daq::rc::TransitionCmd& cmd) override;
    
    /// RC disconnect transition: 
    virtual void disconnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC subtransition (IF NEEDED)
    // virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override;
    
    // Method called periodically by RC
    virtual void publish(void) override;
    
    // Method called periodically with a longer interval by RC
    virtual void publishFullStats();

    /// RC HW re-synchronization request
    virtual void resynch(const daq::rc::ResynchCmd& cmd);

    /// Method called in each ERS_LOG to print the RM name
    inline std::string name_ftk() { return m_name; }

  private:
    //// Dump a JSon based SpyBuffer on file (TODO: get rid of it!)
    //// void dumpSpybufferOnFile(Json::Value &info);
    

    /// make spybuffers available to emon;
    void shipSpyBuffer(std::vector< std::shared_ptr< AMBSpyBuffer > > & vecSpyBuffers, bool isHit=true);

  private:

    std::unique_ptr<ambslpNamed>     m_ambslpNamed;   ///< Access IS via schema
    std::vector<ROS::DataChannel *>  m_dataChannels;   
    DFCountedPointer<ROS::Config> 	 m_configuration;	/**< Configuration Object, Map Wrapper */
    std::string 					           m_isServerName;	/**< IS Server	*/
    IPCPartition 					           m_ipcpartition;	/**< Partition	*/
    uint32_t    					           m_runNumber;			/**< Run Number	*/
    bool                             m_dryRun;        ///< Bypass VME calls
    //bool                             m_localLoop;        ///< Do local loop
    std::unique_ptr<OHRootProvider>  m_ohProvider;    ///< Histogram provider
    std::shared_ptr<OHRawProvider<>> m_ohRawProvider;    ///< Raw histogram provider
    std::unique_ptr<DataFlowReaderAMB> m_dfReaderAmb;   ///< DataFlowReaderAMB

    // AM BOARD Specific Parameters
    std::string     m_name;				/**< Card name	*/
    unsigned int    m_slot;				/**< Slot	*/
    bool 						m_CanPublish;

    // Histograms
    std::unique_ptr<TH1F>     m_hlast_lvl1id;
    std::unique_ptr<TH1F>     m_hnroads;
    std::unique_ptr<TH1F>     m_hgeoaddress;
    std::unique_ptr<TH1F>     m_hnhits;

    // AMBoard class instance
    std::unique_ptr<AMBoard> m_AMB;

    std::unique_ptr<StatusRegisterAMBFactory> m_statusRegisterAMBFactory;
    void reset_weak_config();
  };

  inline const std::vector<ROS::DataChannel *> *ReadoutModule_Ambslp::channels()
  {
    return &m_dataChannels;
  }

} // namespace ftk
} // namespace daq
#endif // READOUT_MODULE_AMBSLP_H
 
