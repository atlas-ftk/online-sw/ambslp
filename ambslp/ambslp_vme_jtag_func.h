#ifndef VME_JTAG_FUNC_H
#define VME_JTAG_FUNC_H
/*         VME_JTAG_func.h
 * ============================================================================ 
 * functions for VME 2 JTAG interfaacre
 * mainly used to download patterns into the AMchip05
 * Alberto Annovi
 * November 2014            
 * ============================================================================
 */

#include "ambslp/ambslp.h"
#include "assert.h"

//#define MAXPATT_PERCHIP 2048 /* total number of pattern per AMchip05 */
//#define ADDR_SIZE 16
#define DATA_SIZE 145
#define REC_ADDR_SIZE 26
#define CONFIG_LENGTH  97
#define CONFIG_SIZE 97
#define SERDES_SEL_SIZE 7
#define SERDES_REG_SIZE 32
#define SERDES_STAT_SIZE 32
#define IDLE_CFG_SIZE 32
#define SELFTEST_SIZE 46	
#define CRC_REG_SIZE 32
#define IDCODE_REG_SIZE 32
#define ERRDOUT32_OFFSET 8
#define RX_STREAM_ERROR_OFFSET 24
#define TX_STREAM_ERROR_OFFSET 28

/* =============== Jtag instrucion codes */
#define IDCODE		0x01
#define JPATT_ADDR	0xC4
#define JPATT_DATA	0xC5
#define CONFIG_REG	0xC6
#define SERDES_SEL	0XC9
#define SERDES_REG	0XCA
#define IDLE_CFG	0XCB
#define SELFTEST	0XCD
#define WR_INCop	0xD4
#define NEXT_REC_ADDR   0xD5
#define INIT_EVop	0xD6
#define JPATT_ADDRrd	0xE4
#define JPATT_DATArd	0xE5
#define CONFIG_REGrd	0xE6
#define REC_ADDR	0xE8
#define SERDES_STAT	0XEA
#define CRC_REG		0xED
#define ADDRESS_REG	0xE8
#define BYPASS		0xFF
//#define SEL_BANKop     0xD5
//#define CHK_PATTop     0xD7
//#define TCKop          0xD8
//#define PATT_FLOW_FSM  0xE7

//#define EXTEST         0x00
//#define INTEST         0x02
//#define SAMPLE_PRELOAD 0x82
//
//
/* define code with mnemonic names */
//#define SAMPLE             SAMPLE_PRELOAD
#define LDCFG              CONFIG_REG
#define RDCFG              CONFIG_REGrd
#define LDADDR             JPATT_ADDR
#define RDADDR             JPATT_ADDRrd
#define LDDATA             JPATT_DATA
#define RDDATA             JPATT_DATArd
#define OPWRINC            WR_INCop
//#define OPSEL              SEL_BANKop
#define OPINIT             INIT_EVop
//#define OPCHKP             CHK_PATTop
//#define OPTCK              TCKop
#define RDBADDR            ADDRESS_REG
//#define RDFSM              PATT_FLOW_FSM

#define AM05_IDCODE 0x50004071
#define AM06_IDCODE 0x60003071

/* =========================================================== */
extern int poffset[MAXLAMB_PER_BOARD*MAXVMECOL_PER_LAMB][MAXCHIP_PER_VMECOL];
extern int roadaddr[MAXAMCHIPS];
extern int roadaddr_new[MAXAMCHIPS];

int getgeoadd(/*int jtagcol,*/ int chipnum);


int patt2addr(int pattnum);



/* ========================================================================== */


struct vme_jtag {
  unsigned long active_columns;
  unsigned long chainlength;
};

void getVmeJtag( vme_jtag* info );

//unsigned long getActiveColumns(/*unsigned long lambcfg[MAXLAMB_PER_BOARD]*/);

/* ========================================================================== */
int getroadaddr(int lamb, int jtagcol, int chipnum, int pattnum);

/* ========================================================================== */
void AMcfg(/*unsigned long lambcfg[MAXLAMB_PER_BOARD],*/ int nPattPerChip);

/* ========================================================================== */
int getgeoadd(int jtagcol, int chipnum);

/* ========================================================================== */
int getChainLength(/*unsigned long lambcfg[MAXLAMB_PER_BOARD]*/);
/* ========================================================================== */
int getNchips(/*unsigned long lambcfg[MAXLAMB_PER_BOARD]*/);

#endif /* VME_JTAG_FUNC_H */
