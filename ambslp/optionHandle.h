
#ifndef OPTION_HANDLE_H
#define OPTION_HANDLE_H



namespace daq {
  namespace ftk {


    struct Option { };

    
    struct OptionHandle {

      OptionHandle( );
      virtual ~OptionHandle();
      struct Option *option;

    };



    struct Option_VMEReader : public Option {
      int slot;
    };



    struct Option_BeginState : public Option {
      int slot;
    };

  }  // ftk namespace
}  // daq namespace


#endif  // OPTION_HANDLE_H

