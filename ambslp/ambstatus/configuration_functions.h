
#ifndef AMB_HARDWARE_STATUS_CONFIG_FUNCTIONS_H
#define AMB_HARDWARE_STATUS_CONFIG_FUNCTIONS_H

class daq::ftk::AMBConfiguration;

namespace daq{
  namespace ftk{

    AMBConfiguration * get_AMBConfiguration( int slot );

  }
};

#endif  //  AMB_HARDWARE_STATUS_CONFIG_FUNCTIONS_H
