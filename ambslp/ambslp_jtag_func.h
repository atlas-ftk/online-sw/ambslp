#ifndef AMBSLP_JTAG_FUNC_H
#define AMBSLP_JTAG_FUNC_H

#include "ambslp/ambslp.h"
//class VMEInterface;

int GotoTestReset(int slot);
int GotoTestReset( VMEInterface *vme );

int GotoIdlefromReset(int slot, int column);
int GotoIdlefromReset(VMEInterface *vme, int column);

int GotoShiftIRfromReset(int slot, int column);
int GotoShiftIRfromReset(VMEInterface *vme,int column);

int GotoShiftIRfromIdle(int slot, int column);
int GotoShiftIRfromIdle(VMEInterface *vme, int column);

int GotoIdlefromExit1(int slot, int column);
int GotoIdlefromExit1(VMEInterface *vme, int column);

int GotoSelectDRfromExit1(int slot, int column);
int GotoSelectDRfromExit1( VMEInterface *vme, int column);

int GotoShiftDRfromIdle(int slot, int column);
int GotoShiftDRfromIdle(VMEInterface *vme, int column);

int StayIdle(int slot);
int StayIdle(VMEInterface *vme);

int AMshift(int slot, u_int columns, u_int nwords, const uint32_t *indata, uint32_t *outdata, int exit );
int AMshift(VMEInterface *vme, u_int columns, u_int nwords,   const uint32_t *indata, uint32_t   *outdata, int myexit );

int ConfigureScam(int slot, int chipno, int column, int scaminstruction, int gotoruntest);
int ConfigureScam(VMEInterface *vme, int chipno, int column, int scaminstruction, int gotoruntest);

int char2int(char input);
int hex2bin(const char* src, unsigned char* target);

int ConfigureRegister(int slot,  int chipno, int active_columns, int scaminstruction, const bool *reg_data, int reg_length, uint32_t *outdata, bool compare_in_out=false);
int ConfigureRegister(VMEInterface *vme,  int chipno, int active_columns, int scaminstruction,  const bool *reg_data, int reg_length, uint32_t * outdata, bool compare_in_out=false);
int ConfigureRegister(int slot,  int chipno, int active_columns, int scaminstruction, const uint32_t *vme_data, int data_size,  uint32_t *outdata, bool compare_in_out=false);
int ConfigureRegister(VMEInterface *vme,  int chipno, int active_columns, int scaminstruction, const uint32_t *vme_data, int data_size, uint32_t  *outdata, bool compare_in_out=false);

void PrintRegisterContent(int chipno, int reglength, const uint32_t *regcontent);

#endif // AMSLP_JTAG_FUNC_H

