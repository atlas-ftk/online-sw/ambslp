/** @file PhaseTest.h 
 *  @auth Nicolo' Vladi Biesuz nicolo.vladi.biesuz@cern.ch
 *  
 *  This file contains the definitions of:
 *  -type   CutWindow
 *  -struct PhaseCut
 *  -struct PhaseTest
 */
#ifndef PHASETEST_h
#define PHASETEST_h

#include <utility>      // std::pair
#include <ftkcommon/exceptions.h>

using namespace std;

namespace daq{
  namespace ftk{

    typedef pair<float, float> CutWindow; /**< definition of the CutWindow type: it is a pair of float values representing the window boundaries */

    /** @struct PhaseCut
     * This is a structure used to store the cut values for a single AMchip. It also implements the basic functionality needed to verify a measured phase value
     */
    struct PhaseCut{

      CutWindow m_LowerCut; /**< Lower cut window */

      CutWindow m_UpperCut; /**< Upper cut window */      

      PhaseCut(){
        m_LowerCut = make_pair(0.00, 0.20);
        m_UpperCut = make_pair(0.80, 1.00);
      };

      /**@fn SetCut
       *
       * @param ll_cut lower bound of the lower phase window to be excluded
       * @param lh_cut upper bound of the upper phase window to be excluded
       * @param hl_cut lower bound of the lower phase window to be excluded
       * @param hh_cut upper bound of the upper phase window to be excluded
       * 
       * This is a constructor for the structure Phase cut 
       */
      void SetCut(float ll_cut, float lh_cut, float hl_cut, float hh_cut ){
        m_LowerCut = make_pair(ll_cut, lh_cut);
        m_UpperCut = make_pair(hl_cut, hh_cut);
      }

      /**@fn SetCut
       * 
       * @param l_cut the lower phase window to be excluded
       * @param h_cut the upper phase window to be excluded
       * 
       * This is a constructor for the structure Phase cut 
       */
      void SetCut(CutWindow l_cut, CutWindow h_cut){
        m_LowerCut = l_cut;
        m_UpperCut = h_cut;
      }

      /**@fn passCut
       *
       * @param phase the measured phase
       * @return true if the chip configuration is correct, false if the chip needs reconfiguring.
       * 
       * This method checks the phase value passed as input to be outside the two windows specified by m_LowerCut and m_UpperCut
       */
      bool passCut(float phase){
        if      ( (phase >= m_LowerCut.first) and (phase <= m_LowerCut.second) ) return false;
        else if ( (phase >= m_UpperCut.first) and (phase <= m_UpperCut.second) ) return false;
        else return true;
      };
    };

    /** @struct PhaseTest
     * Implements all the functionalities needed to check the phase of AMchips after the connfiguration.
     */
    struct PhaseTest{

      PhaseCut* m_ChipConfig; /**< This is the member containing the excluded phases for each of the chips. */

      /**@fn PhaseTest
       *
       * The default constructor.
       */
      PhaseTest(): m_ChipConfig(nullptr){};
      
      /**@fn ~PhaseTest
       *
       * The default destructor.
       */
      ~PhaseTest(){};
      
      /**@fn Initialize
       *
       * It initialize the phase cuts vector: IT MUST BE CALLED BEFORE PERFORMING THE PHASE TEST
       */

      void Initialize();

      /**@fn Delete
       *
       * It deletes the phase cuts vector: IT MUST BE CALLED IN ORDER NOT TO CAUSE A MEMORY LEAK
       */
      void Delete();

      /**@fn GetChipConfig
       *
       * @param ChipNumber is the chip numbeing. It ranges from 0-15, meaning that chip 0 (1, 2, ...) of each LAMB is considered as haveing the same configuration
       * @return a pair of pair of floats withe the two excluded phase regions
       * 
       * This method returns the two phase windows for which the chip 'ChipNumber' is considered as badly configured. That The chip must NOT have ret.first.first < phase < ret.first.second and ret.second.first < phase < ret.second.second
       */
      pair<CutWindow, CutWindow> GetChipConfig( unsigned int ChipNumber);


      /**@fn SetChipConfig
       * 
       * @param ChipNumber is the chip numbeing. It ranges from 0-15, meaning that chip 0 (1, 2, ...) of each LAMB is considered as haveing the same configuration
       * @param ll_cut lower bound of the lower phase window to be excluded
       * @param lh_cut upper bound of the upper phase window to be excluded
       * @param hl_cut lower bound of the lower phase window to be excluded
       * @param hh_cut upper bound of the upper phase window to be excluded
       *
       * This method allows to set the cut window for a chip given its ChipNumber
       */
      inline void SetChipConfig( unsigned int ChipNumber, float ll_cut, float lh_cut, float hl_cut, float hh_cut ) {
        m_ChipConfig[ChipNumber].m_LowerCut = make_pair(ll_cut, lh_cut);
        m_ChipConfig[ChipNumber].m_UpperCut = make_pair(hl_cut, hh_cut);
      };

      /**@fn SetChipConfig
       * 
       * @param ChipNumber is the chip numbeing. It ranges from 0-15, meaning that chip 0 (1, 2, ...) of each LAMB is considered as haveing the same configuration
       * @param l_cut the lower phase window to be excluded
       * @param h_cut the upper phase window to be excluded
       *
       * This method allows to set the cut window for a chip given its ChipNumber
       */
      inline void SetChipConfig( unsigned int ChipNumber, CutWindow l_cut, CutWindow h_cut) {
        m_ChipConfig[ChipNumber].m_LowerCut = l_cut;
        m_ChipConfig[ChipNumber].m_UpperCut = h_cut;
      };

      /**@fn IsGood
       *
       * @param ChipNumber is the chip numbeing. It ranges from 0-15, meaning that chip 0 (1, 2, ...) of each LAMB is considered as haveing the same configuration
       * @param Phase the phase measured for thes chip
       * @return true if the chip configuration is correct, false if the chip needs reconfiguring.
       */
      inline bool IsGood( unsigned int ChipNumber, float Phase){
        bool returnValue(false);
        
        if (m_ChipConfig==nullptr){
	  throw daq::ftk::FTKIssue(ERS_HERE, name_ftk(), std::string("Chip configuration set to nullptr! Did you called Initialize()? "));
        }
        
        try{
          returnValue = m_ChipConfig[ChipNumber].passCut(Phase);
        }catch(std::exception & e ){
	  throw daq::ftk::FTKIssue(ERS_HERE, name_ftk(), std::string("Failed to retrieve chip configuration! \t")+ e.what());
        }
        return returnValue;
      }

    };
  }
}



#endif
