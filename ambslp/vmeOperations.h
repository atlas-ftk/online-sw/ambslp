

#ifndef VME_OPERATIONS_AMB_H
#define VME_OPERATIONS_AMB_H

class VMEInterface;

//  operation_OrThrow functions throw 
//  daq::ftk::Issue_ambslp exceptions if the vme operation has failed

VMEInterface * connectToVME_OrThrow( int slot );
int readBoard_OrThrow( VMEInterface *vme, int address );
void writeToBoard_OrThrow( VMEInterface *vme, int address, int value );

#endif  //  VME_OPERATIONS_AMB_H
