#ifndef AM_VME_REGS_H
#define AM_VME_REGS_H

#define CHECK_ERROR(x) if (x != VME_SUCCESS) \
    { \
      VME_ErrorPrint(x); \
      return (x); \
    }


//Those values are the values to be written in register 
//   INIT (0X00000004) to reset respectively ambslp, lamb, all fifos 
//   on the ambslp, all config registers in ambslp
//
#define RESET_AMB            1
#define RESET_LAMB           2
#define RESET_FIFO           4
#define RESET_CONF           8



// control va ancora sistemato

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ AUX CARD REGISTER - TX ++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define FIRMWARE_VERS_TX    		0x01000000
#define TMODE_TX                	0x0100003c
#define RESET_TX			0x01000048
#define SET_SWITCH_TX			0x01000020
#define AUXTXCHIARISK           	0x01000060
#define SETTXCHAIRK             	0x01000070
#define AUXTXRAM			0x010A0000

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ AUX CARD REGISTER - RX ++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define FIRMWARE_VERS_RX        	0x04000000
#define RESET_RX	        	0x04000048
#define TMODE_RX                	0x0400003c
#define SET_CHAIN_RX			0x04000020
#define AUXRXLINK0			0x04010000
#define AUXRXLINK1              	0x04020000
#define AUXRXLINK2             		0x04030000
#define AUXRXLINK3              	0x04040000
#define AUXRXLINK4             	 	0x04050000
#define AUXRXLINK5             	 	0x04060000
#define AUXRXLINK6            	  	0x04070000
#define AUXRXLINK7            	  	0x04080000
#define AUXRXLINK8			0x04090000
#define AUXRXLINK9              	0x040A0000
#define AUXRXLINK10            		0x040B0000
#define AUXRXLINK11             	0x040C0000
#define AUXRXLINK12            	 	0x040D0000
#define AUXRXLINK13            	 	0x040E0000
#define AUXRXLINK14           	  	0x040F0000
#define AUXRXLINK15           	  	0x04100000
#define AUXERRRXSEL			0x04000060
#define SETRX1CHIAN			0x04000060
#define SETRX2CHIAN        	     	0x04000070
#define AUXERRRX1   			0x04000110
#define AUXERRRX2         	      	0x04000150
#define HOLD_STATUS                     0x00000130
#define AUX_DISABLE_HOLDS               0x000000B0
#define FREEZE_LINES                    0x00000110
#define FORCE_FREEZE                    0x00000010
#define AUX_TMODE                       0x0000003C

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ VME INTERFACE REGISTER ++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define STATUS_REGISTER_VME			0x00000000
#define INIT_AMB          			0x00000004
#define TMODE_VME		        	0x00000008
#define ENABLE_TCK      	    		0x0000000C
#define AMB_VME_BANKCHECKSUM			0x00000010
#define TMS_FPGA				0x00000014
#define TDI_FPGA				0x00000018
#define TDO_FPGA				0x0000001C
//#define TMS					    0x00000020 /**< TMS signal for amchips: this address can be used only in single write mode */
//#define TDI					    0x00000024 /**< TDI signal for amchips: this address can be used only in single write mode */
//#define	TDO					    0x00000028 /**< TDO signal for amchips: this address can be used only in single write mode */
#define TMS   					0x001A0000 /**< TMS signal for amchips: this address can be used only in block transfer mode */
#define TDI 	  				0x001A1000 /**< TDI signal for amchips: this address can be used only in block transfer mode */
#define	TDO 		  			0x001A2000 /**< TDI signal for amchips: this address can be used only in block transfer mode */
#define TRST			  		0x0000002C

#define AMB_MEM_TDITMS_FPGA	0x190000 /**< TDI-TMS memory address for BT, in order to program the AMB's FPGAs */
#define AMB_MEM_TDO_FPGA	0x194000
#define AMB_MEM_TDITMS_LAMB	0x198000 /**< TDI-TMS memory address for BT, in order to program the LAMB'sFPGAs */
#define AMB_MEM_TDO_LAMB	0x19c000

// LAMB temperature measurements
#define AMB_VME_LAMB_TEMP_REAR			0x00000030
#define AMB_VME_LAMB_TEMP_FRONT			0x00000034

// LAMB indirect access
#define AMB_VME_LAMB_INDIRECT_ADDR		0x00000038
#define AMB_VME_LAMB_INDIRECT_DATA		0x0000003C

// Values for LAMB indirect access
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_TESTREG               0x00000000   /**< test R/W reg                                                                 */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_FW_VER                0x01010101   /**< FW version on all LAMBs                                                      */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_DTEST_LO              0x02020202   /**< DTest for chips 0-7 on all LAMBs                                             */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_DTEST_HI              0x03030303   /**< DTest for chips 8-15 on all LAMBs                                            */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_HOLD_LO               0x04040404   /**< HOLD for chips 0-7 (2 not reported) on all LAMBs                             */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_HOLD_HI               0x05050505   /**< HOLD for chips 8-15 (2 not reported) onall LAMBs                             */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_NO_ROAD               0x06060606   /**< FORCE\DISABLE No More Roads signal (DTEST)                                   */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_REF_CLOCK_LOW         0x07070707   /**< Reference counter of slow sampling clock 24 bits in total, bits 7 downto 0   */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_REF_CLOCK_MED         0x08080808   /**< Reference counter of slow sampling clock 24 bits in total, bits 15 downto 8  */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_REF_CLOCK_HIGH        0x09090909   /**< Reference counter of slow sampling clock 24 bits in total, bits 23 downto 16 */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_CLOCK_CONTROL         0x0a0a0a0a   /**< Clock phase measurement control                                              */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_PHASE_PLL_ULOCK_1     0x49494949   /**< PLL UNLOCK count for phase measurements                                      */
#define AMB_VME_LAMB_INDIRECT_ADDRVAL_PHASE_PLL_ULOCK_2     0x4A4A4A4A   /**< PLL UNLOCK count for phase measurements                                      */

#define LAMB_PHASE_RESET          0x01   /**< Reset conunter used in phase measurement                                      */
#define LAMB_PHASE_ENABLE         0x06   /**< Enable conunter used in phase measurement                                     */
#define LAMB_PHASE_DISABLE        0x00   /**< Disable conunter used in phase measurement                                    */

// registers for access to the JTAG chains of the 4 BOUSCAs on the LAMBs
#define AMB_VME_TDITMS_LAMB			0x00000058 // TDI is bit 0, TMS is bit 1
#define AMB_VME_TDO_LAMB			0x0000005C

//lamb temperature I2C sensors
//
// temperature sensors are placed on the left-upper and on the
//   right-upper part of the lambs.
// The reason is, the cooling fans are above the boards, and 
//   the sensors monitor the parts of the lambs at higher temperature.
//
// Please refer to the "monitoring_functions.cxx" file to understand 
//   whether the sensor1 is on the left or on the right side of the lambs
//
#define LAMB_TEMP_SENS1                         0x00000030
#define LAMB_TEMP_SENS2                         0x00000034
#define AMB_VME_ENABLE_VME_FPGA_PROG		0x00000040
#define AMB_VME_FW_VERSION			0x0000004C
// Please add all registers before this line and update the value of AMB_VME_LAST_REGISTER
#define AMB_VME_LAST_REGISTER                   0x00000050 // address of next free register

// AMB VME chip registers for FLASH RAM
#define AMB_VME_FLASH_RAM_COMMAND               0x00000040 // address value to be changed!!!

// AMB VME AMchip JTAG registers for block transfer
#define AMB_VME_NADDS_AMCHIP                         0x400
#define AMB_VME_TMS_AMCHIP                      0x001A0000
#define AMB_VME_TDI_AMCHIP                      0x001A1000
#define AMB_VME_TDO_AMCHIP                      0x001A2000

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ CONTROL REGISTER ++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define CONTROL_STATUS_REGISTER			0x00006000
#define CONTROL_VME_FREEZE_BIT                  20
#define CONTROL_LAMB_FREEZE_BIT                 21
#define CONTROL_ROAD_FREEZE_BIT                 22
#define CONTROL_HIT_FREEZE_BIT                  23
#define CONTROL_AUX_FREEZE_BIT                  24
#define AMB_CONTROL_STATUS_BOARD_VERSION_1STBIT 0
#define AMB_CONTROL_STATUS_FSM_STATE_1STBIT 8
#define AMB_CONTROL_STATUS_LAMB_PRESENCE_1STBIT 16
#define AMB_CONTROL_STATUS_FREEZE_STATUS_1STBIT 16
#define AMB_CONTROL_FW_VERSION			0x00006004
#define FREEZE					0x00006010
// Please add all registers before this line and update the value of AMB_CONTROL_LAST_REGISTER
#define AMB_CONTROL_LAST_REGISTER               0x00006014 // address of next free register
// set the level of error that causes a freese
#define AMB_CONTROL_ERROR_SEVERITY 0x00006008
// set the semaphore for the DCS server to run
#define AMB_CONTROL_DCS_RUN 0x00006024
 // used to reset a freeze, specific register
#define AMB_CONTROL_FREEZE_RESET 0x0000601C
// used for reading the FREEZE form AUX status
#define FREEZE_FROM_AUX 0x00006020


//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ HIT REGISTER ++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
// Global constants of HIT FPGA
#define NCHANNELS_HIT_INPUT 12
#define NTILES_RX_HIT 16
#define NTILES_TX_HIT 16
#define NSCTBLOCKS_HIT 4
#define NPIXBLOCKS_HIT 4
// Global constants of HIT FPGA end
#define FIFOSCTLINK0   			0x00002000
#define KSCTLINK0     			0x00002004
#define FIFOSCTLINK1		    	0x00002008
#define KSCTLINK1     			0x0000200C
#define FIFOSCTLINK2		    	0x00002010
#define KSCTLINK2     			0x00002014
#define FIFOSCTLINK3 		    	0x00002018
#define KSCTLINK3     			0x0000201C
#define FIFOPIXLINK0 		    	0x00002020
#define KPIXLINK0     			0x00002024
#define FIFOPIXLINK1 		    	0x00002028
#define KPIXLINK1     			0x0000202C
#define FIFOPIXLINK2 		    	0x00002030
#define KPIXLINK2     			0x00002034
#define FIFOPIXLINK3 		    	0x00002038
#define KPIXLINK3     			0x0000203C
#define FIFOPIXLINK4 	   	 	0x00002040
#define KPIXLINK4     			0x00002044
#define FIFOPIXLINK5 		    	0x00002048
#define KPIXLINK5     			0x0000204C
#define FIFOPIXLINK6 		    	0x00002050
#define KPIXLINK6     			0x00002054
#define FIFOPIXLINK7 		    	0x00002058
#define KPIXLINK7     			0x0000205C
#define SPYSCTSTATUS0		    	0x00002060
#define SPYSCTSTATUS1    		0x00002064
#define SPYSCTSTATUS2    		0x00002068
#define SPYSCTSTATUS3    		0x0000206C
#define SPYPIXSTATUS0    		0x00002070
#define SPYPIXSTATUS1    		0x00002074
#define SPYPIXSTATUS2    		0x00002078
#define SPYPIXSTATUS3    		0x0000207C
#define SPYPIXSTATUS4    		0x00002080
#define SPYPIXSTATUS5    		0x00002084
#define SPYPIXSTATUS6    		0x00002088
#define SPYPIXSTATUS7		    	0x0000208C
#define HOLD_FLAG_HIT		    	0x00002090
#define HIT_STATUS_REGISTER	    	0x00002094
// constants relating to HIT_STATUS_REGISTER
#define HIT_STATUS_REG_START_PLLLOCK_FAILURE 0
#define HIT_STATUS_REG_START_PLLREFLOCK_FAILURE 4
// constants relating to HIT_STATUS_REGISTER end
#define CONTROL_REGISTER_HIT	    	0x00002098
#define HITALIGNGTP		    	0x0000209C
#define HITRESETDONEGTP	   	 	0x000020A0
#define EMPTY_FLAG_HIT   	 	0x000020A4
#define AMB_HIT_FW_VERSION 	       	0x000020A8
#define CONFIG_POL_HIT   	 	0x000020AC
#define LOSS_OF_SYNC0   	 	0x000020B0
#define LOSS_OF_SYNC1   	 	0x000020B4
#define LOSS_OF_SYNC2   	 	0x000020B8
#define AMB_HIT_PIX_FSM		    	0x000020BC
#define AMB_HIT_SCT_FSM	   	 	0x000020C0
#define ERROR_PIX		    	0x000020C4
#define PRBSCHECKER113		    	0x000020C8
#define PRBSCHECKER116		    	0x000020CC
#define PRBSCHECKER213		    	0x000020D0
#define PRBSCHECKER216		    	0x000020D4
#define DEBUG_DISABLE_HOLD_HIT	    	0x000020D8
#define DEBUG_FORCE_HOLD_HIT	    	0x000020DC
#define DEBUG_DISCARD_AUX_HIT           0x00002180
#define ENABLE_PRBS_CHECKER_HIT		0x000020E0
#define RESET_GTP_HIT			0x000020E4
#define ENABLE_AMCORE_DCDC		0x000020E8
//#define AMB_HIT_WILDCARD                0x000020EC
#define AMB_LAST_SEEN_L1ID              0x000020EC
#define AMBSLP_HIT_EVENT_COUNTER        0x000020FC
// registers for HOLD and EMPTY 1s counters
#define AMB_HIT_HOLD_CNT_1SEC_CH0       0x00002100
#define AMB_HIT_HOLD_CNT_1SEC_CH1       0x00002104
#define AMB_HIT_HOLD_CNT_1SEC_CH2       0x00002108
#define AMB_HIT_HOLD_CNT_1SEC_CH3       0x0000210C
#define AMB_HIT_HOLD_CNT_1SEC_CH4       0x00002110
#define AMB_HIT_HOLD_CNT_1SEC_CH5       0x00002114
#define AMB_HIT_HOLD_CNT_1SEC_CH6       0x00002118
#define AMB_HIT_HOLD_CNT_1SEC_CH7       0x0000211C
#define AMB_HIT_HOLD_CNT_1SEC_CH8       0x00002120
#define AMB_HIT_HOLD_CNT_1SEC_CH9       0x00002124
#define AMB_HIT_HOLD_CNT_1SEC_CH10      0x00002128
#define AMB_HIT_HOLD_CNT_1SEC_CH11      0x0000212C
#define AMB_HIT_EMPTY_CNT_1SEC_CH0      0x00002130
#define AMB_HIT_EMPTY_CNT_1SEC_CH1      0x00002134
#define AMB_HIT_EMPTY_CNT_1SEC_CH2      0x00002138
#define AMB_HIT_EMPTY_CNT_1SEC_CH3      0x0000213C
#define AMB_HIT_EMPTY_CNT_1SEC_CH4      0x00002140
#define AMB_HIT_EMPTY_CNT_1SEC_CH5      0x00002144
#define AMB_HIT_EMPTY_CNT_1SEC_CH6      0x00002148
#define AMB_HIT_EMPTY_CNT_1SEC_CH7      0x0000214C
#define AMB_HIT_EMPTY_CNT_1SEC_CH8      0x00002150
#define AMB_HIT_EMPTY_CNT_1SEC_CH9      0x00002154
#define AMB_HIT_EMPTY_CNT_1SEC_CH10     0x00002158
#define AMB_HIT_EMPTY_CNT_1SEC_CH11     0x0000215C

// Registers that control DC/DC converters
#define AMB_HIT_DCDC_COMMAND            0x00002160
#define AMB_HIT_DCDC_DATA               0x00002164

#define AMB_HIT_TEST_ADDR               0x00002178
#define AMD_HIT_TEST_DATA               0x0000217C

// DC/DC cmd,val,add values. These are not VME addresses
#define AMB_HIT_DCDC_add_WRITE          0x00008000
#define AMB_HIT_DCDC_add_BARRACUDA      0x00002900
#define AMB_HIT_DCDC_add_DUAL12_18      0x00000A00
#define AMB_HIT_DCDC_add_LAMB0          0x00001000
#define AMB_HIT_DCDC_add_LAMB1          0x00000E00
#define AMB_HIT_DCDC_add_LAMB2          0x00001100
#define AMB_HIT_DCDC_add_LAMB3          0x00000F00
#define AMB_HIT_DCDC_cmd_VLimit         0x00000040
#define AMB_HIT_DCDC_cmd_Vout						0x00000022
#define AMB_HIT_DCDC_cmd_READ_VIN       0x00000088
#define AMB_HIT_DCDC_cmd_READ_VOUT      0x0000008B
#define AMB_HIT_DCDC_cmd_READ_IOUT      0x0000008C
#define AMB_HIT_DCDC_cmd_READ_TMP1      0x0000008D
#define AMB_HIT_DCDC_cmd_READ_TMP2      0x0000008E
#define AMB_HIT_DCDC_cmd_READ_IOUT_GAIN 0x00000038
#define AMB_HIT_DCDC_cmd_READ_IOUT_OFFS 0x00000039
#define AMB_HIT_DCDC_val_V2700          0x18f60000 //TODO divanta 1270
#define AMB_HIT_DCDC_val_VoutScale			1.6666666666666667
// end of DC/DC cmd,val,add values. These are not VME addresses


#define AMB_HIT_DUMMY_HIT_WORD          0x00002168 
#define AMB_HIT_NHIT_VS_NIDLE           0x0000216C 
#define AMB_HIT_FORCE_EE_FROM_AUX				0x00002170
#define AMB_HIT_SLOW_PROCESSING					0x00002174
#define AMB_HIT_DNA_PORT_0              0x000021F8
#define AMB_HIT_DNA_PORT_1              0x000021FC
// Please add all registers before this line and update the value of AMB_HIT_LAST_REGISTER
#define AMB_HIT_LAST_REGISTER           0x00002200 // address of next free register

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ ROAD REGISTER +++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
// Global constants of ROAD FPGA
#define NCHANNELS_ROAD_INPUT 16
#define NTILES_RX_ROAD 16
#define NTILES_TX_ROAD 16
// Global constants of ROAD FPGA end
#define FIFOROADL0LINK0     		0x00004000
#define KROADL0LINK0     		0x00004004
#define FIFOROADL0LINK1     		0x00004008
#define KROADL0LINK1     		0x0000400C
#define FIFOROADL0LINK2     		0x00004010
#define KROADL0LINK2     		0x00004014
#define FIFOROADL0LINK3     		0x00004018
#define KROADL0LINK3     		0x0000401C
#define FIFOROADL1LINK0     		0x00004020
#define KROADL1LINK0     		0x00004024
#define FIFOROADL1LINK1     		0x00004028
#define KROADL1LINK1     		0x0000402C
#define FIFOROADL1LINK2     		0x00004030
#define KROADL1LINK2     		0x00004034
#define FIFOROADL1LINK3     		0x00004038
#define KROADL1LINK3     		0x0000403C
#define FIFOROADL2LINK0     		0x00004040
#define KROADL2LINK0     		0x00004044
#define FIFOROADL2LINK1     		0x00004048
#define KROADL2LINK1     		0x0000404C
#define FIFOROADL2LINK2     		0x00004050
#define KROADL2LINK2         		0x00004054
#define FIFOROADL2LINK3     		0x00004058
#define KROADL2LINK3     		0x0000405C
#define FIFOROADL3LINK0     		0x00004060
#define KROADL3LINK0         		0x00004064
#define FIFOROADL3LINK1     		0x00004068
#define KROADL3LINK1     		0x0000406C
#define FIFOROADL3LINK2     		0x00004070
#define KROADL3LINK2        		0x00004074
#define FIFOROADL3LINK3     		0x00004078
#define KROADL3LINK3        		0x0000407C
#define SPYROADL0STATUS0		0x00004080
#define SPYROADL0STATUS1		0x00004084
#define SPYROADL0STATUS2		0x00004088
#define SPYROADL0STATUS3		0x0000408C
#define SPYROADL1STATUS0		0x00004090
#define SPYROADL1STATUS1		0x00004094
#define SPYROADL1STATUS2		0x00004098
#define SPYROADL1STATUS3		0x0000409C
#define SPYROADL2STATUS0		0x000040A0
#define SPYROADL2STATUS1		0x000040A4
#define SPYROADL2STATUS2		0x000040A8
#define SPYROADL2STATUS3		0x000040AC
#define SPYROADL3STATUS0		0x000040B0 
#define SPYROADL3STATUS1		0x000040B4
#define SPYROADL3STATUS2		0x000040B8
#define SPYROADL3STATUS3		0x000040BC
#define PROGFULL_EMPTY_INPUTFIFO	0x000040C0
#define ROAD_STATUS_REGISTER		0x000040C4
// constants relating to ROAD_STATUS_REGISTER
#define ROAD_STATUS_REG_START_PLLLOCK_FAILURE 12
#define ROAD_STATUS_REG_START_PLLREFLOCK_FAILURE 16
// constants relating to ROAD_STATUS_REGISTER end
#define ROAD_CONTROL_REGISTER		0x000040C8
#define AMB_ROAD_FW_VERSION		0x000040CC
#define FORCE_EE_LINKS			0x000040D0
#define ROADALIGNGTP		    	0x000040D4
#define ROADRESETDONEGTP   	 	0x000040D8
#define CONFIG_ROAD	   	 	0x000040DC
#define CONFIG_POL_ROAD   	 	0x000040E0
#define KWORDMONITORING   	 	0x000040E4
#define PRBS_ERROR_QUAD_113		0x000040E8
#define PRBS_ERROR_QUAD_116   	 	0x000040EC
#define PRBS_ERROR_QUAD_213   	 	0x000040F0
#define PRBS_ERROR_QUAD_216   	 	0x000040F4
#define DEBUG_DISABLE_HOLD_AUX 	 	0x000040F8
#define HOLD_AUX_STATUS 	 	0x000040FC
#define COUNT_PRBS_GENERATOR		0x00004100
#define RESET_GTP_ROAD			0x00004104
#define ENABLE_PRBS_CHECKER_ROAD	0x00004108
#define RUN_PRBS_CHECKER_11X		0x0000410C
#define RUN_PRBS_CHECKER_21X		0x00004110
#define AMB_ROAD_INPUT_FSM_LAMB01      	0x00004128
#define AMB_ROAD_INPUT_FSM_LAMB23      	0x0000412C

// registers to check 8b/10b errors or disparity errors on the input from AM chips
#define AMB_ROAD_8B10B_ERRORS_LAMB0     0x00004130
#define AMB_ROAD_8B10B_ERRORS_LAMB1     0x00004134
#define AMB_ROAD_8B10B_ERRORS_LAMB2     0x00004138
#define AMB_ROAD_8B10B_ERRORS_LAMB3     0x0000413C
#define AMB_ROAD_IDLE_WORDS             0x00004140
#define NO_ROAD_FROM_LAMB               0x00004154
 
// register to disable output to the AUX, it can be used in combination with a
// fake event before normal operation, to discard any garbage roads produced by
// the AM Chips during the configuration procedure
#define DEBUG_DISCARD_OUTPUT_DATA       0x00004150

// registers for PATTERNX
#define AMB_PATTERNX_ADDRESS_VALUE      0x0 // value of the AM06 address at which patternX is written
#define AMB_ROAD_PATTERNX_ADDRESS       0x000041FC
#define AMB_ROAD_PATTERNX_LINK0         0x00004200
#define AMB_ROAD_PATTERNX_LINK1         0x00004204
#define AMB_ROAD_PATTERNX_LINK2         0x00004208
#define AMB_ROAD_PATTERNX_LINK3         0x0000420C
#define AMB_ROAD_PATTERNX_LINK4         0x00004210
#define AMB_ROAD_PATTERNX_LINK5         0x00004214
#define AMB_ROAD_PATTERNX_LINK6         0x00004218
#define AMB_ROAD_PATTERNX_LINK7         0x0000421C
#define AMB_ROAD_PATTERNX_LINK8         0x00004220
#define AMB_ROAD_PATTERNX_LINK9         0x00004224
#define AMB_ROAD_PATTERNX_LINK10        0x00004228
#define AMB_ROAD_PATTERNX_LINK11        0x0000422C
#define AMB_ROAD_PATTERNX_LINK12        0x00004230
#define AMB_ROAD_PATTERNX_LINK13        0x00004234
#define AMB_ROAD_PATTERNX_LINK14        0x00004238
#define AMB_ROAD_PATTERNX_LINK15        0x0000423C
// Please add all registers before this line and update the value of AMB_ROAD_LAST_REGISTER
#define AMB_ROAD_LAST_REGISTER          0x00004240 // address of next free register

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ HIT MEMORY ++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define ISPY_SCT0     				0x00080000
#define ISPY_SCT1			    	0x00088000
#define ISPY_SCT2			    	0x00090000
#define ISPY_SCT3	 		    	0x00098000
#define ISPY_PIX0	 		    	0x000A0000
#define ISPY_PIX1	 		    	0x000A8000
#define ISPY_PIX2	 		    	0x000B0000
#define ISPY_PIX3	 		    	0x000B8000
#define ISPY_PIX4	 		   	0x000C0000
#define ISPY_PIX5	 		    	0x000C8000
#define ISPY_PIX6	 		    	0x000D0000
#define ISPY_PIX7	 		    	0x000D8000

//++++++++++++++++++++++++++++++++++++++++++++
//++++++++++ ROAD MEMORY +++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define OSPY_L0_LINK0  			0x00100000
#define OSPY_L0_LINK1		    	0x00108000
#define OSPY_L0_LINK2		    	0x00110000
#define OSPY_L0_LINK3		    	0x00118000
#define OSPY_L1_LINK0 		    	0x00120000
#define OSPY_L1_LINK1 		    	0x00128000
#define OSPY_L1_LINK2 		    	0x00130000
#define OSPY_L1_LINK3		    	0x00138000
#define OSPY_L2_LINK0 		    	0x00140000
#define OSPY_L2_LINK1		    	0x00148000
#define OSPY_L2_LINK2		    	0x00150000
#define OSPY_L2_LINK3		    	0x00158000
#define OSPY_L3_LINK0  			0x00160000
#define OSPY_L3_LINK1		    	0x00168000
#define OSPY_L3_LINK2		    	0x00170000
#define OSPY_L3_LINK3 		    	0x00178000

//++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++ WORD +++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define FILL_WORD 			0xFFFFFFFF
#define BLANK_WORD 			0x00000000

/*
//++++++++++++++++++++++++++++++++++++++++++++
//++++++++ SPY BUFFER REGISTERS ++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++
#define ISPY0            0x1000
#define ISPY1            0x2000
#define ISPY2            0x3000
#define ISPY3            0x4000
#define ISPY4            0x5000
#define ISPY5            0x6000
#define ISPYSTATUS0      0x0100
#define ISPYSTATUS1      0x0200
#define ISPYSTATUS2      0x0300
#define ISPYSTATUS3      0x0400
#define ISPYSTATUS4      0x0500
#define ISPYSTATUS5      0x0600
//#define ISPYSTATUS10     0x0030
//#define ISPYSTATUS32     0x0034
//#define ISPYSTATUS54     0x0038
#define OSPY             0x7000
#define OSPYSTATUS       0x002C
//


#define REG0        0x000000
#define NPATT       0x00080
#define NEVENT      0x00090
#define NTIMER      0x000A0
#define BYPASS_BUS  0x000E0
#define OPCODE      0x000C0 // OLD ADDRESS fw version 1 (before ~ April 5th, 2012)
#define OPCODE_RAM  0x09000

#define PMASK            0x00003C
#define OUTPUT0          0x0003FC




// register of firmware version
#define FIRMWARE_VERSION 0x8000
#define CONFIG           0x00030
// the CONFIG register is defined as
// bit 3:0 AMB SLIM PCB version
//     1 = PCB version 1
//     0,2-15 = PCB version 2
// bit 7:4 data valid majority logic
//     0 disable DV majority logic 
//     1 enable  DV majority logic 
//
*/
#endif
