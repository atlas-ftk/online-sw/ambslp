#ifndef STATUSREGISTERAMBFACTORY_H
#define STATUSREGISTERAMBFACTORY_H

#include "ftkvme/StatusRegisterVMEFactory.h"

// okx schema generated class
#include "ambslp/dal/StatusRegisterAMBStatusNamed.h"

#include <string>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Reads out Registers from the AMB using StatusRegisterVMECollection objects
 * Will publish them to the IS once fully implemented.
 */
class StatusRegisterAMBFactory 
: public StatusRegisterVMEFactory
{
public:
	/*! \brief Class constructor
	 *
	 * \param ambName The name of the AMB being read from
	 * \param ambSlot The VME slot the AMB is situated in
	 * \param registersToRead tells the factory which register collections to read out:
	 *   V = VME Chip,
	 *   H = Hit FPGA,
	 *   R = Road FPGA,
	 *   C = Control VME
	 *
	 * \param isServerName The name of the IS server the register values will be sent to
	 * \param IPCPartition The IPCPartition to be used for IS publishing
	 */
	StatusRegisterAMBFactory(
		std::string ambName,
		u_int ambSlot,
		std::string registersToRead,
		std::string isServerName,
		IPCPartition ipcPartition
	);

	virtual ~StatusRegisterAMBFactory();

protected:
	/*! \brief Creates a collection if the corresponding character was passed to the Factory constructor.
	 *
	 * \param IDChar The character which must be passed to the constructor in order for this group of registers to be read out
	 * \param fpgaNum The fpga number the registers are to be read from
	 * \param firstAddress The address of the first register to be read
	 * \param finalAddress The address of the final register to be read
	 * \param addrIncremenet The increment between neighbouring addresses to be read
	 * \param collectionName A descriptive name for the collection of registers
	 * \param collectionShortName A brief name for the collection of registers
	 * \param ISObjectVector The vector in the IS interface object that the register values will be stored in
	 * \param ISObjectInfoVector If IS interface object is provided, a vector containing firstAddress, finalAddress, addrIncremenet, selectorAddress will be stored.
	 * \param selectorAddress Selector address, default value is 0
 	 * \param readerAddress Reader access for selector, default value is 0 
	 * \param type StatuRegister type (not used as it is hardhoded in StatusRegisterVMECollection::StatusRegisterVMECollection())
	 * \param access StatuRegister access type 
	 */
	virtual void setupCollection(
		char IDChar,
		uint fpgaNum,
		uint firstAddress, 
		uint finalAddress,
		uint addrIncrement,
		std::string collectionName, 
		std::string collectionShortName,
		std::vector<uint>* ISObjectVector,
		std::vector<uint>* ISObjectInfoVector=NULL,
                uint selectorAddress = 0,
                uint readerAddress = 0,
		srType type = srType::srOther,
                srAccess access = srAccess::dummy
	);

private:
	// Member Functions

private:
	// Member Variables

};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERAMBFACTORY_H */
