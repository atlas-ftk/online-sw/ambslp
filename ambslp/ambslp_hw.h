/** \addtogroup lib_ROM
 * @{
 *  ambslp_hw class declaration
 * 
 *  \file ambslp/ambslp_hw.h
 *  \brief hardware description for the ambslp; collects parameters 
 *         that cannot be extracted from OKS
 * 
 *  The ambslp_hw class collects the parameters related to the hardware, 
 *  such as the chip version, that cannot be set from OKS.  
 * 

 *  \struct daq::ftk::ambslp_hw
 *  \brief ambslp_hw class
 * 
 *  a description of the hardware, as the number of chips on LAMBs, 
 *  the number of LAMBs on board, the chip versions...
 * 

 *  \fn void daq::ftk::ambslp_hw::cloneInto( ambslp_hw* dest )
 *  \brief a method which fills the passed argument with the stored values
 * 
 *  Copy the values for the stored variables into the passed object; 
 *  this method is not responsible for the creation of the cloned object.
 * 
 *  \param[out] dest pointer to a new daq::ftk::ambslp_hw object to be
 *              filled with the values copied by this instance
 *  \return void; in case of problems, exceptions are thrown
 *  \throws daq::ftk::Issue_ambslp
 *  \todo reserve space for hardware variables
 *  \todo describe constructor/descructor
 *  \todo implement exceptions
 * @}
**/


#ifndef AMBSLP_HARDWARE_H
#define AMBSLP_HARDWARE_H


namespace daq {
  namespace ftk {

    class ambslp_hw {

     public:
      ambslp_hw();
      virtual ~ambslp_hw();
      void cloneInto( ambslp_hw* dest );
    };

  }
}


#endif  //  AMBSLP_HARDWARE_H
