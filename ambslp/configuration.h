

#ifndef AMB_HARDWARE_STATUS_CONFIGURATION_H
#define AMB_HARDWARE_STATUS_CONFIGURATION_H


#include <iostream>


namespace daq {
  namespace ftk {

    class  OptionHandle;
    class  AMBStatus;

    // for future needs
    enum CrateType { VME = 0, //
                     ATCA };


    // contains the minimal initial information fo rstarting the bootstrap
    struct InitConfiguration {
      int slot;
      CrateType crateType;
      // some other informative/necessary info for setup
    };


    // enumerate the possible steps in the configuration chain
    enum Step { BEGIN = 0, //
                READ_REGISTERS_FROM_VME, //
                ACTIVATE_DCDC, //
                DISCOVER_JTAG_CHAINS, //
                RESET_AUX, //
                RESET_GTP_HIT, //
                RESET_CHIPS, //
                RESET_GTP_ROAD, //
                EMPTY_FIFO, //
                END, //
                TOTAL_NUMBER_OF_STEPS };

    // this encapsulate the enumeration to invite the user to use meaningful names...
    struct ConfigurationStep {
      Step step;
    };



    class AMBConfiguration {

     public:
      AMBConfiguration( InitConfiguration *init );
      virtual ~AMBConfiguration();

      virtual void show( std::ostream &aStream ) = 0;          // push the configuration status in the stream
      virtual AMBStatus* configure(OptionHandle *opt) = 0;     // execute the current state, do transition to next, and return new AMBStatus
      virtual void restartFrom( ConfigurationStep *step ) = 0; // restart the configuration step from a given point; a deep reset of previous steps is done
      const AMBStatus * status();                              // return a pointer to the memory area storing the configuration (without configuring)

     protected:
      AMBStatus * currentStatus;
    };


  AMBConfiguration * get_AMBConfiguration( InitConfiguration *init );


  }  // ftk namespace
}  // daq namespace

#endif  //  AMB_HARDWARE_STATUS_CONFIGURATION_H

