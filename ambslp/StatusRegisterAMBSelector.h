#ifndef STATUSREGISTERAMBSELECTOR_H
#define STATUSREGISTERAMBSELECTOR_H

#include <iostream>

#include "ftkvme/StatusRegisterVMESelector.h"

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Accesses the VME and reads out a single register
 */
class StatusRegisterAMBSelector
: public StatusRegisterVMESelector 
{
public:
	/*! \brief Class constructor
	 * 
	 * \param vmeSlot The VME slot of the board to read the register from
	 * \param fpgaNum The FPGA number to read the register from
	 * \param registerName The name of the register
	 * \param registerAddress The address of the register
	 * \param type The type of register to be read (left as an input to make modification later on easier)
         * \param selectorAddress Selector address, default value is 0 
         * (in the selector scheme you want to write into 'selectorAddress' values from 'registerAddress' and read the outcome from 'readerAddress')
         * \param readerAddress Reader access for selector, default value is 0 (see also selectorAddress description)
	 */
	StatusRegisterAMBSelector(
		uint vmeSlot,
		uint fpgaNum,
		std::string registerName,
		uint registerAddress,
		srType type,
                uint selectorAddress,
                uint readerAddress		
	);
	~StatusRegisterAMBSelector();

	/*! \brief Reads the variable at the address passed to the constructor
	 */
	virtual void readout();

protected:
};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERAMBSELECTOR_H */
