tdaq_package()

# TODO: DEPENDENCIES

#### Create temporary file containing a string (named cmtversion) that is build using 
#### svn info command. The string can be printed out including version.ixx in the code 

execute_process(
  WORKING_DIRECTORY ../${TDAQ_PACKAGE_NAME}
  COMMAND svn info
  OUTPUT_VARIABLE SVNINFO
)

execute_process( 
  COMMAND mkdir -p ${TDAQ_PACKAGE_NAME}/cmt
)

execute_process(
  COMMAND echo -n "const char* cmtversion = R\"FtkDelim(" \nTag: ${TDAQ_PACKAGE_VERSION} \n${SVNINFO} ")FtkDelim\"; \n" 
  OUTPUT_FILE ${TDAQ_PACKAGE_NAME}/cmt/version.ixx
)

############################################################
# DAL: generation of C++ OKS interface from xml schema file 
#####

tdaq_generate_dal(schema/Readout_Ambslp.schema.xml
  NAMESPACE daq::ftk::dal
  INCLUDE ambslp/dal 
  INCLUDE_DIRECTORIES dal DFConfiguration ftkcommon gnamFTKLibdal gnamAMBLibdal 
  CPP_OUTPUT dal_cpp_srcs
  )

############################################################
# IS generation
#####
tdaq_generate_isinfo(ambslp_is_ISINFO 
  schema/ambslp_is.schema.xml
  NAMED 
  NAMESPACE daq::ftk
  OUTPUT_DIRECTORY ambslp/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )

tdaq_generate_isinfo(StatusRegisterAMBStatus_is_ISINFO 
  schema/StatusRegisterAMBStatus_is.schema.xml
  NAMED 
  NAMESPACE daq::ftk
  OUTPUT_DIRECTORY ambslp/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )

tdaq_generate_isinfo(FtkDataFlowSummaryAMB_is_ISINFO
  schema/FtkDataFlowSummaryAMB_is.schema.xml
  NAMED
  NAMESPACE daq::ftk
  PREFIX ftkcommon/dal
  OUTPUT_DIRECTORY ambslp/dal
  CPP_OUTPUT cpp_srcs
  JAVA_OUTPUT is_java_srcs
  )

tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/ambslp/info DESTINATION ambslp)
tdaq_add_header_directory(${CMAKE_CURRENT_BINARY_DIR}/ambslp/dal DESTINATION ambslp)

##########################################################
# ambslp library
#  - It is supposed to contain ONLY the functions neeeded by higher level 
#  - applications and libraries (es: ReadOutModule)
#  - In order include new functions:
#    - add the cxx in the list below
##########################################################
tdaq_add_library( ambslp_schema
  ${dal_cpp_srcs}		  
  LINK_LIBRARIES ftkvme ftkcommon ROOT::Core tdaq::rdbconfig 
  tdaq::oks tdaq::rdb tdaq::daq-df-dal PatternBank PatternBankDal
  SpyBuffer AMBEventFragment StatusRegister FTKEMonDataOut
  Boost::program_options Boost::iostreams Boost::filesystem
  tdaq::ROSCore ROOT::Tree tdaq-common::ers tdaq-common::eformat
  tdaq-common::eformat_write )

tdaq_add_library( ambslp
  src/AMChip.cxx
  src/ambslp_mainboard.cxx 
  src/ambslp_jtag_func.cxx
  src/ambslp_vme_jtag_func.cxx 
  src/ambslp_amchip.cxx
  src/PhaseTest.cxx 
  src/AMBoard.cxx 
  src/AMBoard_fifos.cxx 
  src/AMBoard_chips.cxx 
  src/AMBoard_procedures.cxx 
  src/AMBoard_spybuffers.cxx
  src/AMBoard_DCDC.cxx 
  src/AMBSpyBuffer.cxx
  src/DataFlowReaderAMB.cxx 
  src/ambslp_hw.cxx 
  src/ambslp_card.cxx 
  src/ambslp_settings.cxx 
  src/vmeOperations.cxx 
  src/ambstatus.cxx 
  src/monitoring_functions.cxx 
  src/ambstatus/sharedMemory_access.cxx 
  src/ambstatus/storagePolicy.cxx 
  src/ambstatus/optionHandle.cxx 
  src/ambstatus/state.cxx 
  src/ambstatus/fsm_states.cxx 
  src/ambstatus/fsm_vmeReader.cxx 
  src/ambstatus/fsm_begin.cxx 
  src/ambstatus/nominal_configuration.cxx
  DEFINITIONS WORD64BIT
	INCLUDE_DIRECTORIES ftkcommon ProcessingUnit
  LINK_LIBRARIES ftkvme ftkcommon ROOT::Core tdaq::rdbconfig PU_schema ambslp_schema
	tdaq::oks tdaq::rdb tdaq::daq-df-dal PatternBank PatternBankDal 
	SpyBuffer AMBEventFragment StatusRegister FTKEMonDataOut 
  Boost::program_options Boost::iostreams Boost::filesystem 
	tdaq::ROSCore ROOT::Tree tdaq-common::ers tdaq-common::eformat 	
	tdaq-common::eformat_write 
	ambslp_is_ISINFO StatusRegisterAMBStatus_is_ISINFO FtkDataFlowSummaryAMB_is_ISINFO)

tdaq_add_library( StatusRegisterAMB
  src/StatusRegisterAMBFactory.cxx
  src/StatusRegisterAMBSelector.cxx
  src/StatusRegisterAMBCollection.cxx
  DEFINITIONS WORD64BIT
  LINK_LIBRARIES ftkvme ftkcommon ROOT::Core Boost::program_options Boost::iostreams Boost::filesystem
                 SpyBuffer AMBEventFragment StatusRegister FTKEMonDataOut PatternBankDal 
                 PatternBank tdaq::ROSCore ROOT::Tree tdaq-common::ers tdaq-common::eformat tdaq-common::eformat_write
                 ROOT::Hist tdaq::rdbconfig tdaq::oks tdaq::rdb tdaq::daq-df-dal
                 StatusRegisterAMBStatus_is_ISINFO )

if(FALSE)
tdaq_add_library( ReadoutModule_Ambslp 
  src/ReadoutModuleAmbslp.cxx
  src/StatusRegisterAMBFactory.cxx
  src/StatusRegisterAMBSelector.cxx
  src/StatusRegisterAMBCollection.cxx
  DEFINITIONS WORD64BIT
  LINK_LIBRARIES ftkvme ftkcommon ROOT::Core Boost::program_options Boost::iostreams Boost::filesystem 
                 SpyBuffer AMBEventFragment StatusRegister FTKEMonDataOut PatternBankDal ambslp
                 PatternBank tdaq::ROSCore ROOT::Tree tdaq-common::ers tdaq-common::eformat tdaq-common::eformat_write 
                 ROOT::Hist tdaq::rdbconfig tdaq::oks tdaq::rdb tdaq::daq-df-dal )
endif()
##################################################
## Stand-alone applications 
##################################################
set(tdaq::app_libs tdaq::vme_rcc tdaq-common::eformat tdaq-common::ers)

tdaq_add_executable(ambslp_reset_gtp
    src/ambslp_reset_gtp.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem  
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_lambTemperatureMonitor
    src/ambslp_lambTemperatureMonitor.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_procedure
    src/ambslp_procedure.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_amchip_read_errDout32
    src/ambslp_amchip_read_errDout32.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_calculate_checksum
    src/ambslp_calculate_checksum.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(run_amb
    src/run_amb.cxx
    src/ambslp_test.cxx
    LINK_LIBRARIES ${app_libs} tdaq-common::eformat_write tdaq::oks Boost::program_options Boost::iostreams Boost::filesystem 
                   ftkvme ftkcommon tdaq::rdb tdaq::rdbconfig tdaq::daq-df-dal tdaq::DFExceptions
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_test_bootstrap
    src/ambslp_test_bootstrap.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_amchip_jpatt_cfg_main
    src/ambslp_amchip_jpatt_cfg.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem  
  ROOT::Tree ROOT::Core ambslp   
    )


tdaq_add_executable(ambslp_amchip_reg_healthcheck_main
    src/ambslp_amchip_reg_healthcheck.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem  
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_writepatterns_main
    src/ambslp_writepatterns.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_dcdc
    src/ambslp_dcdc.cxx
    LINK_LIBRARIES Boost::program_options Boost::iostreams Boost::filesystem ftkvme ftkcommon tdaq::vme_rcc tdaq-common::eformat tdaq-common::ers
      ROOT::Tree ROOT::Core ambslp  
    )

tdaq_add_executable(ambslp_configure_lamb_main
    src/ambslp_configure_lamb.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon Boost::program_options Boost::iostreams Boost::filesystem 
  ROOT::Tree ROOT::Core ambslp   
    )

tdaq_add_executable(ambslp_amchip_idcode_main
    src/ambslp_amchip_idcode.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_test_extra_lines_main
    src/ambslp_test_extra_lines.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_init_evt_main
    src/ambslp_amchip_init_evt.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_test_mode_main
    src/ambslp_amchip_test_mode.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_normal_mode_main
    src/ambslp_amchip_normal_mode.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp  
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_enable_bank_main
    src/ambslp_amchip_enable_bank.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_disable_bank_main
    src/ambslp_amchip_disable_bank.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_write_test_bank_main
    src/ambslp_amchip_write_test_bank.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_PRBS_test_main
    src/ambslp_PRBS_test.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_PRBS_pattin_test_main
    src/ambslp_PRBS_pattin_test.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_polling_stat_reg_main
    src/ambslp_amchip_polling_stat_reg.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_polling_stat_reg_pattin_main
    src/ambslp_amchip_polling_stat_reg_pattin.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_idlecfg_main
    src/ambslp_amchip_idlecfg.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_BER_main
    src/ambslp_amchip_BER.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_write_0x0_pattern_main
    src/ambslp_amchip_write_0x0_pattern.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_prbs_error_count_main
    src/ambslp_amchip_prbs_error_count.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_prbs_error_count_all_buses_main
    src/ambslp_amchip_prbs_error_count_all_buses.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_prbsgen_main
    src/ambslp_amchip_prbsgen.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_8b_10b_main
    src/ambslp_amchip_8b_10b.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_boardsim
    src/ambslp_boardsim.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_out_spy_main
    src/ambslp_out_spy.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_init_main
    src/ambslp_init.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_test_flash_RAM
    src/ambslp_test_flash_RAM.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_test_VME
    src/ambslp_test_VME.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_reset_spy_main
    src/ambslp_reset_spy.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_status_main
    src/ambslp_status.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_inp_spy_main
    src/ambslp_inp_spy.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_spybuffer_dumper_main
    src/ambslp_spybuffer_dumper.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp tdaq::emon 
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_idle_word_road_main
    src/ambslp_idle_word_road.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_dump_ospy_main
    src/ambslp_dump_ospy.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_feed_hit_main
    src/ambslp_feed_hit.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_feed_road_main
    src/ambslp_feed_road.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_gen_hits_DC_main
    src/ambslp_gen_hits_DC.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_road_diff_main
    src/ambslp_road_diff.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_serdes_config_main
    src/ambslp_amchip_serdes_config.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_xvc
    src/ambslp_xvc.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_svfplayer
    src/ambslp_svfplayer.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(ambslp_amchip_DTEST_select_main
    src/ambslp_amchip_DTEST_select.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

tdaq_add_executable(runAMBspybufferchecks
    src/runAMBspybufferchecks.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

##################################################
## Stand-alone applications
##################################################
tdaq_add_executable(ambslp_config_reg_main
    src/ambslp_config_reg.cxx
    LINK_LIBRARIES ${app_libs} ftkvme ftkcommon
  ROOT::Tree ROOT::Core ambslp   
    Boost::program_options Boost::iostreams Boost::filesystem )

##################################################
## Installing scripts
##################################################
tdaq_add_scripts(scripts/AMBCompareTestVecOutput.py)

tdaq_add_scripts(scripts/AMBDCDCUtility.py)

tdaq_add_scripts(scripts/AMBInSpyToRoads.py)

##################################################
## Installing schema
##################################################
tdaq_add_schema(schema/*.xml DESTINATION schema)

##################################################
## Installing IS schema
##################################################
tdaq_add_is_schema(schema/ambslp_is.schema.xml DESTINATION schema)

tdaq_add_is_schema(schema/StatusRegisterAMBStatus_is.schema.xml DESTINATION schema)

tdaq_add_is_schema(schema/FtkDataFlowSummaryAMB_is.schema.xml DESTINATION schema)
