/*           AMBSLP mainboard library                                          */
/*           Written for AMBSLP FTK collaboration                              */
/*           Starting date: 2015-07-03                                         */
/*           Authors: Annovi, Bertolucci, Biesuz, Kubota, Volpi                */

#include "ambslp/ambslp_mainboard.h"


// ------------ TILL HERE

// this is here because of the ambslp_gen_hits_DC block of functions
using namespace std;


namespace daq {
  namespace ftk {

    long unsigned int lamb::lambcfg[MAXLAMB_PER_BOARD] = {0x10100101, 0, 0, 0};


    //------------------



    /* define local functions */
    /* int Read_register(register); */


    //static u_int register_content;
    static u_int expected_value;
    static u_int test_word = 0x0; 
    //static u_int register_add;
    //static u_int register_test;
    //static u_int data;
    //static u_int debug;


    //static char termstring[10];



u_int readregister(u_int regname, int amc, bool isISmode, std::vector< std::vector<unsigned int> > statregc, std::vector< std::vector<unsigned int> > metainfoc)
{
  u_int reg_content, status;
  if(isISmode){
    reg_content = ReadSRFromISVectors(regname, statregc, metainfoc);
  }
  else{
    status = VME_ReadSafeUInt(amc, regname, &reg_content);
    CHECK_ERROR(status);
  }
  return reg_content;
}

unsigned int hammingDistance16(unsigned int a, unsigned int b) {
  unsigned int dist = 0;
  a = a & 0xFFFF;
  b = b & 0xFFFF;
  unsigned int val = a^b;
  while(val)
    {
      ++dist; 
      val &= val - 1;
    }
  return dist;
}

bool ambslp_status( int slot, int verbose_level, std::string is_part_name)
{

  u_int register_content;
  int am1, status;
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  u_int ret, vmeaddr;
  bool is_mode = !is_part_name.empty();
  //IPCPartition* is_partition;
  IPCPartition is_partition (is_part_name);
  std::string is_obj_name = "";
  std::vector< std::vector<unsigned int> > is_src_v;        // status register collections
  std::vector< std::vector<unsigned int> > is_src_info_v;   // collection meta info
  std::vector< std::string > is_src_names;
  // add or remove sr collections published to IS, make sure XXX_sr_v_info 
  // vector with meta info [firstAddr, lastAddr, step] is also available
  is_src_names.push_back("VME_Chip_sr_v");
  is_src_names.push_back("Hit_FPGA_sr_v");
  is_src_names.push_back("Road_FPGA_sr_v"); 
  is_src_names.push_back("Control_VME_sr_v");
  std::stringstream out_ss;


  /* Open VME and prepare to write */
  if(!is_mode)
  {
    vmeaddr = ((slot&0x1f) << 27);
    ret = VME_Open();
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      FTK_VME_ERROR( "- Leggireg0.cxx : Failure when calling the function 'VME_Open' "  );
    }

    master_map.vmebus_address   = vmeaddr;
    master_map.window_size      = 0x1000000;
    master_map.address_modifier = VME_A32;
    master_map.options          = 0;
    ret = VME_MasterMap(&master_map, &am1);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      FTK_VME_ERROR( "- Leggireg0.cxx : Failure when calling the function 'VME_MasterMap' "  );
    }
    status = VME_WriteSafeUInt(am1, AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_FW_VER);
    CHECK_ERROR(status);
  } 
  else{ // IS mode, 
    //create a partition to run in (= same as the RC partition)
    //is_partition = new IPCPartition(is_part_name);
    std::string server_name = "DF"; // DataFlow, might be changed in future
    is_obj_name = server_name + ".FTK.Amb" + std::to_string(slot);
    ERS_LOG("Trying to retrieve AMB SR collections from " << is_obj_name);
    for(std::string src_name : is_src_names){
      StatusRegisterISVector sr_temp(is_partition, is_obj_name, src_name, srType::srOther);
      sr_temp.readout();
      StatusRegisterISVector sr_info_temp(is_partition, is_obj_name, src_name + "_info" , srType::srOther);
      sr_info_temp.readout();

      is_src_v.push_back(sr_temp.getValues());
      is_src_info_v.push_back(sr_info_temp.getValues());

      if(is_src_info_v.back().size() < 3){
        std::stringstream message;
        message << "IS SR collection info vector " << src_name.c_str() << " doesn't have expected format [firstAddr, lastAddr, step, ...].";
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::error(ex);
      }
    }
  }

  //+++++++++++++++++++++++++++++++++++++++++
  //Print hostname
  out_ss 
    << "Runs on: " << std::getenv("HOSTNAME") << "\n\n";

  //+++++++++++++++++++++++++++++++++++++++++
  //Print all FW version registers

  out_ss 
    << "Hit Firmware version:  0x" <<  hex << setfill('0') << setw(8) << readregister(AMB_HIT_FW_VERSION, am1, is_mode, is_src_v, is_src_info_v) << "\n"
    << "CONTROL Firmware version:  0x" << setfill('0') << setw(8) <<  readregister(AMB_CONTROL_FW_VERSION, am1, is_mode, is_src_v, is_src_info_v) << "\n"
    << "VME Firmware version:  0x" << setfill('0') << setw(8) <<  readregister(AMB_VME_FW_VERSION, am1, is_mode, is_src_v, is_src_info_v) << "\n"
    << "ROAD Firmware version:  0x" << setfill('0') << setw(8)<<  readregister(AMB_ROAD_FW_VERSION, am1, is_mode, is_src_v, is_src_info_v) << "\n"
    << "LAMB Firmware version (8 bits per LAMB):  0x" << setfill('0') << setw(8) <<  readregister(AMB_VME_LAMB_INDIRECT_DATA, am1, is_mode, is_src_v, is_src_info_v) << dec << "\n";


  u_int TFront[MAXLAMB_PER_BOARD], TRear[MAXLAMB_PER_BOARD];
  register_content = readregister(AMB_VME_LAMB_TEMP_FRONT, am1, is_mode, is_src_v, is_src_info_v);
  for (int ii=0; ii<MAXLAMB_PER_BOARD; ii++)
    TFront[ii] = (register_content>>(8*ii)) & 0xFF;
  register_content = readregister(AMB_VME_LAMB_TEMP_REAR, am1, is_mode, is_src_v, is_src_info_v);
  for (int ii=0; ii<MAXLAMB_PER_BOARD; ii++)
    TRear[ii] = (register_content>>(8*ii)) & 0xFF;

  // temperature measurement
  out_ss << "\n++++++++++++++++++++++++++\n"
    << "Temperature measurements" << endl
    << "VME slot front | LAMB2 | LAMB2 | LAMB0 | LAMB0 | VME back plane" << endl
    << "VME slot front | LAMB3 | LAMB3 | LAMB1 | LAMB1 | VME back plane" << endl
    << "               | " 
    << setfill(' ') << setw(4) << TFront[2] << "  | " << setfill(' ') << setw(4) << TRear[2] << "  | "
    << setfill(' ') << setw(4) << TFront[0] << "  | " << setfill(' ') << setw(4) << TRear[0] << "  | " << endl
    << "               | " 
    << setfill(' ') << setw(4) << TFront[3] << "  | " << setfill(' ') << setw(4) << TRear[3] << "  | "
    << setfill(' ') << setw(4) << TFront[1] << "  | " << setfill(' ') << setw(4) << TRear[1] << "  | " << endl;

  register_content = readregister(AMB_HIT_DUMMY_HIT_WORD, am1, is_mode, is_src_v, is_src_info_v);
  int bitflips = 0;
  bitflips += hammingDistance16( (register_content>>16)&0xFFFF, 0x0 );
  bitflips += hammingDistance16( ((register_content>>16)&0xFFFF) , (register_content&0xFFFF) );
  bitflips += hammingDistance16( register_content&0xFFFF, 0x0 );
  out_ss << "DummyHit value at 0x" << hex << register_content << dec 
         << " number of average bitflips = " << bitflips/4. 
         << " average AM06 power usage = " << (bitflips/4.)/8*100 << "%"
         << endl;

  //+++++++++++++++++++++++++++++++++++++++++
  // Vin Vout measurement of DC-DC converter
  daq::ftk::AMBoard myAMB(slot);
  vector<float> dcdcinfo = myAMB.DCDC_read(false);
  out_ss << "\n++++++++++++++++++++++++++\n"
  	 << "Vout measurement of DC-DC converter" << endl
   	 << "        Vin   | Vout  | Iout" << endl;

  for (unsigned LAMB = 0; LAMB < MAXLAMB_PER_BOARD; LAMB++){ // loop over the LAMBS
    int LAMB_base = 5*LAMB;
    out_ss<< "LAMB" << LAMB << " | " << std::setprecision(4) << dcdcinfo[0+LAMB_base] << " | " << dcdcinfo[1+LAMB_base] << " | " << dcdcinfo[2+LAMB_base] << endl;
  }

  //+++++++++++++++++++++++++++++++++++++++++
  //Error severity
  register_content = readregister(AMB_CONTROL_ERROR_SEVERITY, am1, is_mode, is_src_v, is_src_info_v);
  out_ss << "\n++++++++++++++++++++++++++\n"
    << "Error severity" << endl
    << "Lost sync severity : " << ((register_content)&0x1) << "\n"
    << "Reserved for Road  : " << ((register_content >> 1)&0x1) << "\n"
    << "AUX error severity : " << ((register_content >> 2)&0x1)
    << endl;

  //+++++++++++++++++++++++++++++++++++++++++
  //Freeze status
  register_content = readregister(CONTROL_STATUS_REGISTER, am1, is_mode, is_src_v, is_src_info_v);
  out_ss << "\n++++++++++++++++++++++++++\n"
    << "Freeze status" << endl
    << "Freeze from AUX  : " << ((register_content >> CONTROL_AUX_FREEZE_BIT)&0x1) << "\n"
    << "Freeze from HIT  : " << ((register_content >> CONTROL_HIT_FREEZE_BIT)&0x1) << "\n"
    << "Freeze from ROAD : " << ((register_content >> CONTROL_ROAD_FREEZE_BIT)&0x1) << "\n"
    << "Freeze from LAMB : " << ((register_content >> CONTROL_LAMB_FREEZE_BIT)&0x1) << "\n"
    << "Freeze from VME  : " << ((register_content >> CONTROL_VME_FREEZE_BIT)&0x1)
    << endl;

  //+++++++++++++++++++++++++++++++++++++++++
  //PIX REGISTER STATUS
  //status register
  register_content = readregister( HIT_STATUS_REGISTER,am1, is_mode, is_src_v, is_src_info_v);
  out_ss << "\n++++++++++++++++++++++++++\n"
    << "HIT STATUS\n"
    //<< "Firmware:        " << std::dec << ((register_content>>8)&0xF) << " v." << ((register_content>>4)&0xF) << "\n"
    << "PLL lock:  " << std::hex << ((register_content)&0xF) << "\n"
    << "PLL REF LOCK:  " << ((register_content>>4)&0xf) << "\n"
    << "isK stable:  " << ((register_content>>8)&0xF) << "\n"
    << "isK changed:  " << ((register_content>>12)&0xf) << "\n\n";
    
    register_content = readregister( CONTROL_REGISTER_HIT,am1, is_mode, is_src_v, is_src_info_v);
    out_ss << "TMODE:  " << std::hex << ((register_content>>2)&0x1) << "\n"
    << "LoopFifoVME:  " << ((register_content>>1)&0x1) << "\n"
    << "LAMB mask:  " <<  readregister(FORCE_EE_LINKS, am1, is_mode, is_src_v, is_src_info_v) << "\n\n";

  register_content = readregister( HOLD_FLAG_HIT,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "SCT hold status:  "  << (register_content&0xf) << "\n"
    << "PIX hold status:  "         << ((register_content>>4)&0xff) << "\n\n";

  register_content = readregister( EMPTY_FLAG_HIT,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "SCT empty status:  " << (register_content&0xf) << "\n" 
    << "PIX empty status:  "      << ((register_content>>4)&0xff) << "\n\n";

  register_content = readregister( HITALIGNGTP,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "GTP align QUAD216:  "  << ((register_content>>2)&0x1)  << " - " << ((register_content>>1)&0x1)  << " - " << (register_content&0x1)<<"\n"
    << "GTP align QUAD213:  "       << ((register_content>>7)&0x1)  << " - " << ((register_content>>6)&0x1)  << " - " << ((register_content>>5)&0x1)<<"\n"
    << "GTP align QUAD116:  "       << ((register_content>>11)&0x1) << " - " << ((register_content>>10)&0x1) << " - " << ((register_content>>8)&0x1)<<"\n"
    << "GTP align QUAD113:  "       << ((register_content>>15)&0x1) << " - " << ((register_content>>13)&0x1) << " - " << ((register_content>>12)&0x1)<<"\n\n";

  register_content = readregister( HITRESETDONEGTP,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "GTP reset done QUAD 216 RX:  " << (register_content & 0xf)     << " - TX " << (register_content>>4 & 0xF)<<"\n"
    << "GTP reset done QUAD 213 RX:  "      << (register_content>>8 & 0xf)  << " - TX " << (register_content>>12 & 0xF)<<"\n"
    << "GTP reset done QUAD 116 RX:  "      << (register_content>>16 & 0xf) << " - TX " << (register_content>>20 & 0xF)<<"\n"
    << "GTP reset done QUAD 113 RX:  "      << (register_content>>24 & 0xf) << " - TX " << (register_content>>28 & 0xF)<<"\n";

  out_ss << endl;
  out_ss << "HIT SCT FSM status: 0x" << readregister( AMB_HIT_SCT_FSM, am1, is_mode, is_src_v, is_src_info_v) << endl;
  out_ss << "HIT PIX FSM status: 0x" << readregister( AMB_HIT_PIX_FSM, am1, is_mode, is_src_v, is_src_info_v) << endl;

  // print HOLD and EMPTY 1s counters
  const float counter_1second = 1E8;
  out_ss << "\n        \t" << std::dec;
  for (int ii=0; ii<AMB_INPUT_CHANNELS; ii++) {
    out_ss << "ch " << ii;  if (ii!=AMB_INPUT_CHANNELS-1) out_ss << "\t"; else out_ss << "\n";
  }
  out_ss << "HOLD  1s\t";
  for (int ii=0; ii<AMB_INPUT_CHANNELS; ii++) {
    out_ss << (readregister( AMB_HIT_HOLD_CNT_1SEC_CH0+4*ii, am1, is_mode, is_src_v, is_src_info_v)/counter_1second); 
    if (ii!=AMB_INPUT_CHANNELS-1) out_ss << "\t"; else out_ss << "\n";
  }
  out_ss << "EMPTY 1s\t";
  for (int ii=0; ii<AMB_INPUT_CHANNELS; ii++) {
    out_ss << (readregister( AMB_HIT_EMPTY_CNT_1SEC_CH0+4*ii, am1, is_mode, is_src_v, is_src_info_v)/counter_1second);
    if (ii!=AMB_INPUT_CHANNELS-1) out_ss << "\t"; else out_ss << "\n";
  }
  out_ss << std::hex;

  const unsigned int LOSS_OF_SYNC_MASK = 0xff;

  register_content = readregister( LOSS_OF_SYNC0,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "\nLost of Sync error counter. For each link count 0-255 and saturate at255 (0xff).\n"
    << "SCT link 0:    " << std::setw(2) << std::setfill('0') << (register_content & LOSS_OF_SYNC_MASK)<<" \n"
    << "SCT link 1:    " << std::setw(2) << std::setfill('0') << (register_content>>8 & LOSS_OF_SYNC_MASK)<<" \n"
    << "SCT link 2:    " << std::setw(2) << std::setfill('0') << (register_content>>16 & LOSS_OF_SYNC_MASK)<<" \n"
    << "SCT link 3:    " << std::setw(2) << std::setfill('0') << (register_content>>24 & LOSS_OF_SYNC_MASK)<<" \n";

  register_content = readregister( LOSS_OF_SYNC1,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "PIX link 0_0:  " << std::setw(2) << std::setfill('0') << (register_content & LOSS_OF_SYNC_MASK)<<" \n"
    << "PIX link 0_1:  "      << std::setw(2) << std::setfill('0') << (register_content>>8 & LOSS_OF_SYNC_MASK)<<" \n"
    << "PIX link 1_0:  "      << std::setw(2) << std::setfill('0') << (register_content>>16 & LOSS_OF_SYNC_MASK)<<" \n"
    << "PIX link 1_1:  "      << std::setw(2) << std::setfill('0') << (register_content>>24 & LOSS_OF_SYNC_MASK)<<" \n";

  register_content = readregister( LOSS_OF_SYNC2,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "PIX link 2_0:  " << std::setw(2) << std::setfill('0') << (register_content & LOSS_OF_SYNC_MASK)<<" \n"
    << "PIX link 2_1:  "      << std::setw(2) << std::setfill('0') << (register_content>>8 & LOSS_OF_SYNC_MASK)<<" \n"
    << "PIX link 3_0:  "      << std::setw(2) << std::setfill('0') << (register_content>>16 & LOSS_OF_SYNC_MASK)<<" \n"
    << "PIX link 3_1:  "      << std::setw(2) << std::setfill('0') << (register_content>>24 & LOSS_OF_SYNC_MASK)<<" \n";

  //+++++++++++++++++++++++++++++++++++++++++
  //CONTROL REGISTER STATUS
  //status register
  register_content = readregister( CONTROL_STATUS_REGISTER,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "\n++++++++++++++++++++++++++\n"
    << "CONTROL STATUS\n"
    << "Firmware:        " << std::dec << ((register_content>>4)&0xF) << " v." <<((register_content)&0xF) << "\n"
    << "FSM state Hit:   " << std::hex << ((register_content>>8)&0xF) << "\n"
    << "FSM state Road:  " << ((register_content>>12)&0xF) << "\n\n";

  out_ss << "Control FSMs are documented in Figure 4, section 1.2.1 of the AM board documentation.\n"
    << "FSM state HIT:  0 = wait for initial global init\n"
    << "                1 = send hits event N; wait for HIT end event on all channels and for DONE ROAD previous event\n"
    << "                2 = wait X cycles\n"
    << "                3 = DONE HIT event N, send INIT_EV\n"
    << "                4 = wait a few cycles\n"
    << "FSM state ROAD: 0 = wait for initial global init\n"
    << "                2 = DONE ROAD event N-1\n"
    << "                1 = send ROAD to AUX event N-1\n"
    << "                3 = wait a few cycles\n";


  //+++++++++++++++++++++++++++++++++++++++++
  //ROAD_EVEN REGISTER STATUS

  register_content = readregister( AMB_ROAD_INPUT_FSM_LAMB01, am1, is_mode, is_src_v, is_src_info_v);
  out_ss << "FSM state at each ROAD input (LAMB01):  " << (register_content);
  register_content = readregister( AMB_ROAD_INPUT_FSM_LAMB23, am1, is_mode, is_src_v, is_src_info_v);
  out_ss << "  (LAMB23):  " << (register_content) << "\n\n";

  //status register
  register_content = readregister( ROAD_STATUS_REGISTER,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "\n++++++++++++++++++++++++++\n"
    << "ROAD STATUS\n"  
    << "Firmware:      "  << std::dec << ((register_content>>8)&0xF) << " v." <<((register_content>>4)&0xF) << "\n"
    << "TMODE:         "  << std::hex << (register_content&0x1) << "\n"
    << "PLL lock:      "  << ((register_content>>12)&0xF) << "\n"
    << "PLL REF LOCK:  "  << ((register_content>>16)&0xf) << "\n\n";

  register_content = readregister(HOLD_AUX_STATUS,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "Hold Aux status:  "<< (register_content & 0xffff)<<"\n\n";

  register_content = readregister(PROGFULL_EMPTY_INPUTFIFO,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "Empty Fifo Lamb_0 Status:      " << ((register_content)&0xF) << "\n"
    << "Empty Fifo Lamb_1 Status:      "      << ((register_content>>4)&0xF) << "\n"
    << "Empty Fifo Lamb_2 Status:      "      << ((register_content>>8)&0xF) << "\n"
    << "Empty Fifo Lamb_3 Status:      "      << ((register_content>>12)&0xF) << "\n\n"
    << "Prog Full Fifo Lamb_0 Status:  "      << ((register_content>>16)&0xF) << "\n"
    << "Prog Full Fifo Lamb_1 Status:  "      << ((register_content>>20)&0xF) << "\n"
    << "Prog Full Fifo Lamb_2 Status:  "      << ((register_content>>24)&0xF) << "\n"
    << "Prog Full Fifo Lamb_3 Status:  "      << ((register_content>>28)&0xF) << "\n\n";

  register_content = readregister(ROADALIGNGTP,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "ROAD input link UP QUAD216:  "       << (register_content & 0xf) << "\n"
    << "ROAD input link UP QUAD213:  "       << (register_content>>4 &0xf) << "\n"
    << "ROAD input link UP QUAD116:  "       << (register_content>>8 &0xf) << "\n"
    << "ROAD input link UP QUAD113:  "       << (register_content>>12 &0xf) << "\n\n";

  register_content = readregister(ROADRESETDONEGTP,am1, is_mode, is_src_v, is_src_info_v);

  out_ss << "GTP reset done QUAD 216 RX:  " << (register_content & 0xf)     << " - TX " << (register_content>>4 & 0xF) << "\n"
    << "GTP reset done QUAD 213 RX:  "      << (register_content>>8 & 0xf)  << " - TX " << (register_content>>12 & 0xF) << "\n"
    << "GTP reset done QUAD 116 RX:  "      << (register_content>>16 & 0xf) << " - TX " << (register_content>>20 & 0xF) << "\n"
    << "GTP reset done QUAD 113 RX:  "      << (register_content>>24 & 0xf) << " - TX " << (register_content>>28 & 0xF) << "\n";

  // reading Missing patternX counters
  out_ss << endl;
  for (int ii=0; ii < MAXOUTLINK_PERLAMB*MAXLAMB_PER_BOARD; ii++) {
    register_content = readregister( AMB_ROAD_PATTERNX_LINK0 + 4*ii, am1, is_mode, is_src_v, is_src_info_v);
    if (ii%MAXOUTLINK_PERLAMB == 0)
      out_ss << "LAMB = " << ii/MAXOUTLINK_PERLAMB << endl;
    out_ss << "Missing patternX link " << ii << " = " << hex;
    for (int kk=0; kk<4; kk++)
      out_ss << "0x" << (0xFF&(register_content>>(8*kk))) << " ";
    out_ss << endl;
  }

  out_ss << "\n" << "LAMB0 RX 8b/10b or disparity errors = 0x" << hex << readregister( AMB_ROAD_8B10B_ERRORS_LAMB0, am1, is_mode, is_src_v, is_src_info_v) << endl;
  out_ss << "LAMB1 RX 8b/10b or disparity errors = 0x" << readregister( AMB_ROAD_8B10B_ERRORS_LAMB1, am1, is_mode, is_src_v, is_src_info_v) << endl;
  out_ss << "LAMB2 RX 8b/10b or disparity errors = 0x" << readregister( AMB_ROAD_8B10B_ERRORS_LAMB2, am1, is_mode, is_src_v, is_src_info_v) << endl;
  out_ss << "LAMB3 RX 8b/10b or disparity errors = 0x" << readregister( AMB_ROAD_8B10B_ERRORS_LAMB3, am1, is_mode, is_src_v, is_src_info_v) << dec << endl;

  register_content = readregister( AMBSLP_HIT_EVENT_COUNTER,am1, is_mode, is_src_v, is_src_info_v);
  usleep(100000);
  u_int register_content2 = readregister( AMBSLP_HIT_EVENT_COUNTER,am1, is_mode, is_src_v, is_src_info_v);
  float kHz = (register_content2 - register_content) *10./1000.;
  out_ss << "\n" << "Number of completed events: " << (register_content) <<" - approximate rate " << kHz << " kHz (last 0.1s) \n";

  if (verbose_level==1){
    std::cout << "++++++++++++++++++++++++++\nambslp_status() SLOT " << std::to_string(slot) << ", read from IS:\n" << std::to_string(is_mode) << endl << out_ss.str().c_str();
  }
  else if (verbose_level==0){
    ERS_LOG("ambslp_status() SLOT " << std::to_string(slot) << ", read from IS:\n" << std::to_string(is_mode) << out_ss.str().c_str());
  }

  if(!is_mode){
    /*Close VME*/
    ret = VME_MasterUnmap(am1);
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      FTK_VME_ERROR( "- Leggireg0.cxx : Failure when calling the function 'VME_Close' "  );
    }

    ret = VME_Close();
    if (ret != VME_SUCCESS)
    {
      VME_ErrorPrint(ret);
      FTK_VME_ERROR( "- Leggireg0.cxx : Failure when calling the function 'VME_Close' "  );
    }
  }else{

  }
  return true;
}



bool ambslp_configure_lamb(int slot)
{ 
  int i, j, k;
  //  int nwords;
  int status;
  int presence; 
  int all_chipcolumn = 0xffffffff;
  u_int  register_content;
  int totchips  = 0;
  int totchips_jtag  = 0;
  int totpatts = 0;
  int active_columns =0;
  int chipno = 0;
  int chainlength = 0;

  std::array<uint32_t,MAXDATAWORDS>  data;
  std::array<uint32_t,MAXDATAWORDS>  outdata;
  // FILE *pattfile; // unused var

  // int *pattbank;  // unused var
  int paddr = 0;
  int nPattPerChip;
  //int IdleCLKs;//, nblocks;
  // int tmp_chipno; // unused var


  int am1;
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  u_int ret, vmeaddr;

  int dface = 0;//amb_isDualFace(root);
  PRINT_LOG("Calculated dualFace option is " << dface << "\n");

  /* check input args */

  totchips = getNchips(/*lambcfg*/);

  nPattPerChip = paddr/totchips;
  totpatts = paddr;

  PRINT_LOG( "Tot chips = " << totchips << ", tot patts = " << totpatts << ", patt/chip= " << nPattPerChip );

  /* define active columns  & chip */
  vme_jtag vmejtag_info;
  try { getVmeJtag( &vmejtag_info ); } 
  catch( daq::ftk::FTKIssue &ex ) {
    daq::ftk::ftkException issue ( ERS_HERE, name_ftk(), "Failure on getting vme-jtag info");
    throw issue;
  }

  active_columns = vmejtag_info.active_columns; 
  chainlength = vmejtag_info.chainlength;

  AMcfg(/*lambcfg,*/ nPattPerChip); /* calculate AMchips configuration */

  PRINT_LOG( "Active columns are 0x" << std::hex << active_columns );
  PRINT_LOG( "Chain length is " << chainlength);

  /* build chip number */
  for (i=0, chipno=0; i<chainlength; i++)
    chipno = (chipno<<1) + 1; 
  chipno = (chainlength<<8) + chipno;


  /* define board */

  vmeaddr = ((slot&0x1f) << 27);
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "amb_configure_lamb.cxx : Failed VME_Open " );
  }

  master_map.vmebus_address   = vmeaddr;
  master_map.window_size      = 0x1000000;
  master_map.address_modifier = VME_A32;
  master_map.options          = 0;
  ret = VME_MasterMap(&master_map, &am1);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "amb_configure_lamb.cxx : Failed VME_MasterMap " );
  }

  /**************************************************************/
  //    Control the presence of the LAMB
  /**************************************************************/
  status = VME_ReadSafeUInt(am1, CONTROL_STATUS_REGISTER, &register_content);
  CHECK_ERROR(status);
  presence = (register_content >> 15) & 0xF;	  
  for(i=0;i<MAXLAMB_PER_BOARD;i++)
  {
    if((presence>>i) & 0x1)
      printf("LAMB %d is plugged\n", i);
    else
      printf("LAMB %d is NOT presence\n", i);
  }	

  /**************************************************************/
  //    Insert the JTAG counter of the number of chip
  /**************************************************************/

  GotoTestReset(slot);
  GotoIdlefromReset(slot, all_chipcolumn); 

  //put the chip in bypass mode
  ConfigureScam(slot, chipno, all_chipcolumn, BYPASS, 1);  // goto Run-Test/Idle 

  GotoShiftDRfromIdle(slot, all_chipcolumn);

  for(i=0; i<MAXCHIP_PER_VMECOL*3; i++)
  {
    if(i<MAXCHIP_PER_VMECOL)
      data[i] = 0;
    else if(i>=MAXCHIP_PER_VMECOL && i<MAXCHIP_PER_VMECOL*2)
      data[i] = FILL_WORD;
    else
      data[i] = 0; 	 	

    AMshift(slot, all_chipcolumn, MAXCOLUMNS, data.data(), outdata.data(), 0);
    for(j=0;j<MAXLAMB_PER_BOARD;j++)  
    {
      if(((presence>>j) & 0x1) == 1)
      {
        outdata[i] = (outdata[i] >> 8*j) & 0xFF;
        printf("data %x: \n", outdata[i]);
        if(i>=MAXCHIP_PER_VMECOL*2)
          for(k=0;k<MAXVMECOL_PER_LAMB;k++)	
            if((outdata[i]>>k) == 1)
              totchips_jtag++;	
      }
    }
  }

  GotoIdlefromExit1(slot, active_columns);

  printf("Number of chip: %d\n", totchips_jtag);

  ret = VME_MasterUnmap(am1);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR("- amb_configure_lamb.cxx : Failure when calling the function 'VME_MasterUnmap' in fucntion 'amb_configure_lamb'" );
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret); /* TODO creare le ecezioni per questo */
    FTK_VME_ERROR( "- amb_configure_lamb.cxx : Failure when calling the function 'VME_Close' in function 'amb_configure_lamb'" );
  }

  return true;

} 




// leave this here for the moment
#define NEV 1000
int ambslp_dump_ospy( int slot, int /* statusm */) 
{

  u_int register_content;
  u_int expected_value;
  u_int test_word = 0x0; 
  //u_int register_add;
  //u_int register_test;
  //u_int data;
  //u_int debug;
  // u_int data[1000]; // unused var


  FILE *fp;
  fp=fopen("pippo.txt","w");

  int am1, status;
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  u_int ret, vmeaddr;

  //  int i; // unused var
  int /*overflow, count,*/ overflow0, count0, overflow1, count1, overflow2, count2, overflow3, count3;
  // overflow and count were set, but not used

  //  root["info"] = "";


  //  PRINT_LOG( "Called Leggififo with parameters: slot: " << slot <<", root: "<< root ) ;

  /* Open VME and prepare to write */
  vmeaddr = ((slot&0x1f) << 27);
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "- LeggiOspy.cxx : Failure when calling the function 'VME_Open' "  );
  }

  master_map.vmebus_address   = vmeaddr;
  master_map.window_size      = 0x1000000;
  master_map.address_modifier = VME_A32;
  master_map.options          = 0;
  ret = VME_MasterMap(&master_map, &am1);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "- Leggiispy.cxx : Failure when calling the function 'VME_MasterUnmap' "  );
  }

  //freeze da control
  test_word = 0x1; 
  status = VME_WriteSafeUInt(am1, FREEZE, test_word);
  CHECK_ERROR(status);
  status = VME_ReadSafeUInt(am1, FREEZE, &register_content);
  CHECK_ERROR(status);
  expected_value = 0x1;
  if ((register_content != expected_value ))
  {
    std::stringstream myError;
    myError << "LeggiIOspy error: FREEZE: " << std::hex << register_content << " expected value: " << std::hex << expected_value ;
    std::cout << myError.str() << std::endl ;
    //	  root["warning"]= myError.str();
  }

  //read road even ispy 0 status
  //Daniel Magalotti
  status = VME_ReadSafeUInt(am1, SPYROADL0STATUS0, &register_content);
  CHECK_ERROR(status);

  //  overflow = (register_content>>14) & 0x1;  // set, but not used
  //  count = register_content&0x3fff; // set, but not used


  //Simone Donati
  //read road even ispy 0 status
  status = VME_ReadSafeUInt(am1, SPYROADL0STATUS0, &register_content);
  CHECK_ERROR(status);
  overflow0 = (register_content>>14) & 0x1;
  count0 = register_content&0x3fff;

  //read road even ispy 1 status
  status = VME_ReadSafeUInt(am1, SPYROADL0STATUS1, &register_content);
  CHECK_ERROR(status);
  overflow1 = (register_content>>14) & 0x1;
  count1 = register_content&0x3fff;

  //read road even ispy 2 status
  status = VME_ReadSafeUInt(am1, SPYROADL0STATUS2, &register_content);
  CHECK_ERROR(status);
  overflow2 = (register_content>>14) & 0x1;
  count2 = register_content&0x3fff;

  //read road even ispy 3 status
  status = VME_ReadSafeUInt(am1, SPYROADL0STATUS3, &register_content);
  CHECK_ERROR(status);
  overflow3 = (register_content>>14) & 0x1;
  count3 = register_content&0x3fff;


  printf("Overflow Count %d %d %d %d %d %d %d %d\n", overflow0, count0, overflow1, count1, overflow2, count2, overflow3, count3);

  int eeadd[4][NEV];
  int eeword[4][NEV];
  int eecount[4];

  for (int iii=0; iii<4; iii++){
    eecount[iii]=0;
    for (int ii=0; ii<NEV; ii++){eeadd[iii][ii]=0; eeword[iii][ii]=0;}}
  // int ii0=0;  // unused var
  // int ii1=0;  // unused var
  // int ii2=0;  // unused var
  // int ii3=0;  // unused var

  for (int idx = 0; idx  < count0; ++idx) {
    status = VME_ReadSafeUInt(am1, OSPY_L0_LINK0+((idx%(1<<10))<<2), &register_content);
    CHECK_ERROR(status);
    //      printf("%.4x\n",register_content);
    if ((register_content>>15)&0x1) {eeadd[0][eecount[0]]=idx; eeword[0][eecount[0]]=register_content;
      //      printf("Ciao %d %d %d\n",eeadd[0][eecount[0]],eecount[0], idx);  
      eecount[0]++;}    
  }

  for (int ii=0; ii<NEV; ii++){printf("eeadd[0] eeword[0] %d %.4x \n", eeadd[0][ii], eeword[0][ii]);}


  for (int idx = 0; idx  < count1; ++idx) {
    status = VME_ReadSafeUInt(am1, OSPY_L0_LINK1+((idx%(1<<10))<<2), &register_content);
    CHECK_ERROR(status);
    //      printf("%.4x\n",register_content);
    if ((register_content>>15)&0x1) {eeadd[1][eecount[1]]=idx; eeword[1][eecount[1]]=register_content;
      //      printf("Ciao %d %d %d\n",eeadd[1][eecount[1]],eecount[1], idx);  
      eecount[1]++;}    
  }

  for (int ii=0; ii<NEV; ii++){printf("eeadd[1] eeword[1] %d %.4x \n", eeadd[1][ii], eeword[1][ii]);}



  for (int idx = 0; idx  < count2; ++idx) {
    status = VME_ReadSafeUInt(am1, OSPY_L0_LINK2+((idx%(1<<10))<<2), &register_content);
    CHECK_ERROR(status);
    //      printf("%.4x\n",register_content);
    if ((register_content>>15)&0x1) {eeadd[2][eecount[2]]=idx; eeword[2][eecount[2]]=register_content;
      //      printf("Ciao %d %d %d\n",eeadd[2][eecount[2]],eecount[2], idx);  
      eecount[2]++;}    
  }

  for (int ii=0; ii<NEV; ii++){printf("eeadd[2] eeword[2] %d %.4x \n", eeadd[2][ii], eeword[2][ii]);}



  for (int idx = 0; idx  < count3; ++idx) {
    status = VME_ReadSafeUInt(am1, OSPY_L0_LINK3+((idx%(1<<10))<<2), &register_content);
    CHECK_ERROR(status);
    //      printf("%.4x\n",register_content);
    if ((register_content>>15)&0x1) {eeadd[3][eecount[3]]=idx; eeword[3][eecount[3]]=register_content;
      //      printf("Ciao %d %d %d\n",eeadd[3][eecount[3]],eecount[3], idx);  
      eecount[3]++;}    
  }

  for (int ii=0; ii<NEV; ii++){printf("eeadd[3] eeword[3] %d %.4x \n", eeadd[3][ii], eeword[3][ii]);}

  printf("This is the number of End Event words on \nLink0: %d \nLink1: %d \nLink2: %d \nLink3: %d\n", 
      eecount[0],eecount[1],eecount[2],eecount[3]);

  printf("Say if you want to dump a Link (1 means yes, 0 means no) and the number of Events you want to dump\n");
  int l0=0;
  int l1=0;
  int l2=0;
  int l3=0;	
  int numberev = 0;
  int use[4] = {0,0,0,0,};
  scanf("%d %d %d %d %d",&l0, &l1, &l2, &l3, &numberev);

  use[0] = l0;
  use[1] = l1;
  use[2] = l2;
  use[3] = l3;

  printf("Link0: %d\nLink1: %d\nLink2: %d\nLink3: %d\nNumber of events to dump: %d\n", 
      use[0],use[1],use[2],use[3],numberev);


  if( (eecount[0]!=eecount[1]) || (eecount[0]!=eecount[2]) || (eecount[0]!=eecount[3]) )
  { printf("Error the number of End Event words is not the same in the four links: %d %d %d %d\n", eecount[0], eecount[1], eecount[2], eecount[3]);}


  for (int ii=0; ii<NEV; ii++)
  { 
    if ( (eeword[0][ii]!=eeword[1][ii])  || (eeword[0][ii]!=eeword[2][ii]) || (eeword[0][ii]!=eeword[3][ii]) )
    {
      printf("Error End Event counter does not match %.4x %.4x %.4x %.4x \n", eeword[0][ii], eeword[1][ii], eeword[2][ii], eeword[3][ii]);
    }}


  //There are no errors relative to End Event

  //for (int ii=0; ii<eecount[0]; ii++){
  for (int ii=0; ii<numberev; ii++){
    int addin=0;

    //Link0

    if (use[0]==1){
      if(ii==0){addin = 0;}
      if(ii>0){addin = eeadd[0][ii-1]+1;} 

      //printf("%d %d\n",ii,addin);

      for (int idx = addin; idx  < eeadd[0][ii]; ++idx) {
        status = VME_ReadSafeUInt(am1, OSPY_L0_LINK0+((idx%(1<<10))<<2), &register_content);
        CHECK_ERROR(status);
        fprintf(fp,"0x%.4x\n",register_content);
      }
    }



    //Link1

    if(use[1]==1){
      if(ii==0){addin = 0;}
      if(ii>0){addin = eeadd[1][ii-1]+1;}

      for (int idx = addin; idx  < eeadd[1][ii]; ++idx) {
        status = VME_ReadSafeUInt(am1, OSPY_L0_LINK1+((idx%(1<<10))<<2), &register_content);
        CHECK_ERROR(status);
        fprintf(fp,"0x%.4x\n",register_content);
      }
    }



    //Link2

    if(use[2]==1){
      if(ii==0){addin = 0;}
      if(ii>0){addin = eeadd[2][ii-1]+1;} 

      for (int idx = addin; idx  < eeadd[2][ii]; ++idx) {
        status = VME_ReadSafeUInt(am1, OSPY_L0_LINK2+((idx%(1<<10))<<2), &register_content);
        CHECK_ERROR(status);
        fprintf(fp,"0x%.4x\n",register_content);
      }
    }



    //Link3
    if(use[3]==1){
      if(ii==0){addin = 0;}
      if(ii>0){addin = eeadd[3][ii-1]+1;}

      for (int idx = addin; idx  <= eeadd[3][ii]; ++idx) {
        status = VME_ReadSafeUInt(am1, OSPY_L0_LINK3+((idx%(1<<10))<<2), &register_content);
        CHECK_ERROR(status);
        fprintf(fp,"0x%.4x\n",register_content);
      }
    }

  }


  fclose(fp);


#if 0

  if(overflow) {
    int idx = count;
    for(i = 0; i < 1<<10; ++i) {
      status = VME_ReadSafeUInt(am1, ROADL0SPY0+((idx%(1<<10))<<2), &register_content);
      CHECK_ERROR(status);
      printf("%.4x\n",register_content);
      //printf("OSPY[%x] :\t\t%x\t->\t%x\n",i,register_content&0x3fffffff,1+(((register_content >> 28)&0x1)<<1) + (((register_content >> 22) & 0x1f)<<23) + ((register_content & 0x1fffff)<<2));
      idx++;
    }
  } else {
    for (int idx = 0; idx  < count; ++idx) {
      status = VME_ReadSafeUInt(am1, ROADL0SPY0+((idx%(1<<10))<<2), &register_content);
      CHECK_ERROR(status);
      printf("%.4x\n",register_content);
    }
  }

#endif


  // Write 0 to unfreeze
  //freeze da control
  test_word = 0x0; 
  status = VME_WriteSafeUInt(am1, FREEZE, test_word);
  CHECK_ERROR(status);
  status = VME_ReadSafeUInt(am1, FREEZE, &register_content);
  CHECK_ERROR(status);
  expected_value = 0x0;
  if ((register_content != expected_value ))
  {
    std::stringstream myError;
    myError << "LeggiIOspy error: FREEZE: " << std::hex << register_content << " expected value: " << std::hex << expected_value ;
    std::cout << myError.str() << std::endl ;
    //	  root["warning"]= myError.str();
  }


  return true;

}


// leave it here for the moment
//TODO: why this define is needed?
#define LAYERLSB 16


int ambftk_feed_road(int slot, int loopFifoVme, const char* roadfname /*, Json::Value & root*/ )
{
  struct timeval tstart;

  u_int register_content;
  u_int expected_value;
  u_int test_word = 0x0; 

  int status;
  static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
  u_int ret, vmeaddr;
  int am1;

  unsigned long int re0(0),re1(0),re2(0),re3(0),re4(0),re5(0),re6(0),re7(0),ro0(0),ro1(0),ro2(0),ro3(0),ro4(0),ro5(0),ro6(0),ro7(0);
  unsigned int isK_re0,isK_re1,isK_re2,isK_re3,isK_re4,isK_re5,isK_re6,isK_re7,isK_ro0,isK_ro1,isK_ro2,isK_ro3,isK_ro4,isK_ro5,isK_ro6,isK_ro7;

  FILE *roadfile;

  if (gettimeofday(&tstart, NULL)) {
    PRINT_LOG("Error getting time!");
    return -1;
  }      

  PRINT_LOG( "Called AMfeed_road with parameters : Road_file : " << roadfname <<", slot :  " << slot /*<< ", Root :" << root*/ );

  /* Open VME and prepare to write */
  vmeaddr = ((slot&0x1f) << 27);
  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "Failure when calling the function 'VME_Open' ");
  }

  master_map.vmebus_address   = vmeaddr;
  master_map.window_size      = 0x1000000;
  master_map.address_modifier = VME_A32;
  master_map.options          = 0;
  ret = VME_MasterMap(&master_map, &am1);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "Failure when calling the function 'VME_MasterMap' "  );
  }

  /* ************************ test TMODE Register ******************************** */
  // Write 1
  test_word = 0x4; 
  status = VME_WriteSafeUInt(am1, ROAD_CONTROL_REGISTER, test_word);
  CHECK_ERROR(status);
  status = VME_ReadSafeUInt(am1, ROAD_CONTROL_REGISTER, &register_content);
  CHECK_ERROR(status);
  expected_value = 0x4;
  if ((register_content != expected_value )) 
  {PRINT_LOG("error: TMODE ROAD ON 0x" << std::hex << register_content << " expected value 0x" << expected_value << " !!!");}
  else{PRINT_LOG("TEST MODE ROAD ON\n");}


  /* read hits from file */
  roadfile = fopen(roadfname, "r");
  if (!roadfile) {
    PRINT_LOG("Cannot open input road file");
    return -1;
  }

  // loop over events
  while (!feof(roadfile))
  {
    assert(16 == fscanf(roadfile,"%lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\t %lx\n", &re0,&re1,&re2,&re3,&re4,&re5,&re6,&re7,&ro0,&ro1,&ro2,&ro3,&ro4,&ro5,&ro6,&ro7));


    //write to road
    isK_re0 = (re0 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL0LINK0, isK_re0);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL0LINK0, re0);
    CHECK_ERROR(status);
    PRINT_LOG("[0] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re0 << " kword 0x" << std::hex << isK_re0);

    isK_re1 = (re1 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL0LINK1, isK_re1);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL0LINK1, re1);
    CHECK_ERROR(status);
    PRINT_LOG("[1] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re1 << " kword 0x" << std::hex << isK_re1);

    isK_re2 = (re2 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL0LINK2, isK_re2);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL0LINK2, re2);
    CHECK_ERROR(status);
    PRINT_LOG("[2] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re2 << " kword 0x" << std::hex << isK_re2);

    isK_re3 = (re3 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL0LINK3, isK_re3);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL0LINK3, re3);
    CHECK_ERROR(status);
    PRINT_LOG("[3] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re3 << " kword 0x" << std::hex << isK_re3);

    isK_re4 = (re4 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL1LINK0, isK_re4);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL1LINK0, re4);
    CHECK_ERROR(status);
    PRINT_LOG("[4] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re4 << " kword 0x" << std::hex << isK_re4);

    isK_re5 = (re5 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL1LINK1, isK_re5);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL1LINK1, re5);
    CHECK_ERROR(status);
    PRINT_LOG("[5] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re5 << " kword 0x" << std::hex << isK_re5);

    isK_re6 = (re6 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL1LINK2, isK_re6);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL1LINK2, re6);
    CHECK_ERROR(status);
    PRINT_LOG("[6] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re6 << " kword 0x" << std::hex << isK_re6);

    isK_re7 = (re7 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL1LINK3, isK_re7);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL1LINK3, re7);
    CHECK_ERROR(status);
    PRINT_LOG("[7] ROAD status " << std::dec <<  status << " word 0x"<< std::hex << re7 << " kword 0x" << std::hex << isK_re7);

    isK_ro0 = (ro0 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL2LINK0, isK_ro0);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL2LINK0, ro0);
    CHECK_ERROR(status);
    PRINT_LOG("[0] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro0 << " kword 0x" << std::hex << isK_ro0);

    isK_ro1 = (ro1 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL2LINK1, isK_ro1);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL2LINK1, ro1);
    CHECK_ERROR(status);
    PRINT_LOG("[1] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro1 << " kword 0x" << std::hex << isK_ro1);

    isK_ro2 = (ro2 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL2LINK2, isK_ro2);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL2LINK2, ro2);
    CHECK_ERROR(status);
    PRINT_LOG("[2] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro2 << " kword 0x" << std::hex << isK_ro2);

    isK_ro3 = (ro3 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL2LINK3, isK_ro3);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL2LINK3, ro3);
    CHECK_ERROR(status);
    PRINT_LOG("[3] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro3 << " kword 0x" << std::hex << isK_ro3);

    isK_ro4 = (ro4 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL3LINK0, isK_ro4);
    CHECK_ERROR(status);      
    status = VME_WriteSafeUInt(am1, FIFOROADL3LINK0, ro4);
    CHECK_ERROR(status);
    PRINT_LOG("[4] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro4 << " kword 0x" << std::hex << isK_ro4);

    isK_ro5 = (ro5 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL3LINK1, isK_ro5);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL3LINK1, ro5);
    CHECK_ERROR(status);
    PRINT_LOG("[5] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro5 << " kword 0x" << std::hex << isK_ro5);

    isK_ro6 = (ro6 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL3LINK2, isK_ro6);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL3LINK2, ro6);
    CHECK_ERROR(status);
    PRINT_LOG("[6] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro6 << " kword 0x" << std::hex << isK_ro6);

    isK_ro7 = (ro7 >> 32) & 0xF;
    status = VME_WriteSafeUInt(am1, KROADL3LINK3, isK_ro7);
    CHECK_ERROR(status);
    status = VME_WriteSafeUInt(am1, FIFOROADL3LINK3, ro7);
    CHECK_ERROR(status);
    PRINT_LOG("[7] ROAD status  " << std::dec <<  status << " word 0x"<< std::hex << ro7 << " kword 0x" << std::hex << isK_ro7);

  }
  fclose (roadfile);


  /* ************************ Set the start read of the fifo ******************************** */
  // Write 1
  if(loopFifoVme == 0){
    test_word = 0x1;
    expected_value = 0x5; //tmode + start read fifo
  }else{
    test_word = 0x3;
    expected_value = 0x7; //tmode + start read fifo + loop fifo vme
    PRINT_LOG("Loop FIFO VME enable");
  }

  status = VME_WriteSafeUInt(am1, ROAD_CONTROL_REGISTER, test_word);
  CHECK_ERROR(status);
  status = VME_ReadSafeUInt(am1, ROAD_CONTROL_REGISTER , &register_content);
  CHECK_ERROR(status);
  if ((register_content != expected_value )) 
  {PRINT_LOG("error: START READ FIFO OFF 0x" << std::hex << register_content << " expected value 0x" << expected_value << " !!!");}
  else{PRINT_LOG("START READ FIFO ROAD ON\n");}

  /*Close VME*/
  ret = VME_MasterUnmap(am1);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "- Amfeed_hit.cxx : Failure when calling the function 'VME_MasterUnmap' "  );
  }

  ret = VME_Close();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    FTK_VME_ERROR( "- Amfeed_hit.cxx : Failure when calling the function 'VME_Close' "  );
  }

  return true; 
}



int reduceDCbits(int to_reduce, int dc_bits, int DCprob, bool verbose=false){
  int reduced(0), tbit(0);
  for (int i = 0; i<dc_bits; i++){
    tbit = ((to_reduce)>>(2*i))&0x3;
    if(tbit==0x0){
      int prob = ((rand()%100)/10); 
      if(prob<DCprob)
        reduced|= 0x0<<dc_bits;
      else
        reduced|= 0x1<<dc_bits;
    }
    else if (tbit==0x1) reduced|= 0x0<<i;
    else if(tbit==0x2) reduced|= 0x1<<i;
    else{
      std::cout<<"Error: reduceDCbits(): this function does not supprt VETO bits: if no veto are inserted please check the number of dcbits you are using!"<<std::endl; 
      return -999;
    }
  }
  reduced|=((to_reduce>>(2*dc_bits))&0xffff)<<dc_bits;
  if(verbose) std::cout<<"Pattern: "<<std::hex<<to_reduce<<" reduced to: "<<std::hex<<reduced<<std::endl;
  return reduced;
}



// leace these here for the moment
#define EE_BIT 16

struct lpr{ int addr; int layer;}; //lpr is synonym of Lamb_Pattern_Road, a structure type

bool operator<(lpr a, lpr b)
{
  return (a.addr < b.addr) ;//|| ((a.addr == b.addr) && (a.layer < b.layer));
}



int ambslp_road_diff( int ignore_bits, int laymap, int skip64, const char * rif_fname, const char * meas_fname, const char * kill_fname )
{ 
  unsigned int i,j,k=0,z=0,strutt_temp1,strutt_temp_quattro,strutt_temp_cinque,error_counter=0,plus_error_event=0,minus_error_event=0, nkill=0;   //L`indice i lo associo al file di riferimento, j a quello misurato
  int EE_error_counter=0;

  lpr strutt_temp;  //declare lpr structure variable

  std::ifstream file_rif(rif_fname);// reference file is associated to argv[1]
  if (!file_rif) 
  {
    FTK_VME_ERROR( "Road_diff: No found  reference file sim.dat\n" );
  }
  std::ifstream file_mis(meas_fname);// Measured file is associated to  argv[2]
  if (!file_mis) 
  {
    FTK_VME_ERROR( "No found measured  file amb.dat\n" );
  }

  int useKill = strncmp("", kill_fname, 1) !=0;

  std::ifstream file_kill;

  if (useKill){
    file_kill.open(kill_fname);
    if (!file_mis) 
    {
      FTK_VME_ERROR( "Kill file not found: " << kill_fname << " \n" );
    }
  }

  //  std::ofstream file_out1("out1.dat"); // commentato da AA 13 aprile 2005 (ormai non serve piu')
  std::vector<lpr> strutt_rif, strutt_mis;//declaire vector class  
  std::vector<lpr> killed, killedby; // killed is the vector of killed roads
  // killedby is the vector of roads killing roads in killed vector
  // they correspond to each other by vector index
  // i.e. killed[1] is killedby killednby[1]

  //Acquisition part

  //Thereare more events in the file so the program checks bit of End-Event
  //
  //Use eof() standard function that return boolean variable true if the file is ended

  while (!file_rif.eof() || !file_mis.eof())
  {
    //Acquisition of meausered  file, mis.dat
    //the format is in two coloum fields: Address layer
    //Address in even index, Layer in uneven index
    file_rif >> std::hex >> strutt_temp1;
    strutt_temp.addr = strutt_temp1; 
    strutt_temp_quattro = strutt_temp1;
    strutt_temp1 = (strutt_temp1>>EE_BIT) & 0xFFFF; 
    while(strutt_temp1!=0xE0DA && !file_rif.eof())
    {
      if (laymap == 0)
        strutt_temp.layer = 0xFFFF;
      else
        file_rif>> std::hex >>strutt_temp.layer;

      strutt_temp.layer |= ignore_bits;
      file_rif >> std::hex >> strutt_temp1;
      strutt_temp.addr |= ignore_bits;
      strutt_rif.push_back(strutt_temp);	  
      strutt_temp_quattro = strutt_temp1;
      strutt_temp.addr = strutt_temp1;	 
      strutt_temp1 = (strutt_temp1>>EE_BIT) & 0xFFFF; /*modificato la posizione del bit ee*/
    }
    k++;//Counter of the event of reference file


    if (useKill) {
      //Acquisition of kill  file
      //the format is: DEBUG: 01c868 20003e killed by 01b3c6 20003e
      char pippo[100];
      file_kill>> pippo >> std::hex >> strutt_temp.addr;
      while((strutt_temp.addr>>22)!=1 && !file_kill.eof())
      {
        file_kill >> std::hex >> strutt_temp.layer;      
        if (laymap == 0) {
          strutt_temp.layer = 0xFF;
          strutt_temp.addr |= 0x200000; /* turing on EP */
        }

        strutt_temp.layer|= ignore_bits;
        strutt_temp.addr|= ignore_bits;
        killed.push_back(strutt_temp);

        /* reading in killedby road */
        file_kill>> pippo >> std::hex >> strutt_temp.addr;
        file_kill >> std::hex >> strutt_temp.layer;      
        if (laymap == 0) {
          strutt_temp.layer = 0xFF;
          strutt_temp.addr |= 0x200000; /* turing on EP */
        }
        strutt_temp.layer|= ignore_bits;
        strutt_temp.addr|= ignore_bits;
        killedby.push_back(strutt_temp);

        file_kill>> pippo >> std::hex >> strutt_temp.addr; // reading in next word
      }
      nkill++;
      assert(k==nkill);
    }

    /*      cout << "KILLED=" << killed.size() << "   " << "KILLEDBY=" << killedby.size() << "\n";
            if (killed.size()) {
            cout <<  std::hex << "KILLED=" << killed[0].addr << "   " << "KILLEDBY=" << killedby[0].addr << "\n";
            cout << "KILLED=" << killed[0].layer << "   " << "KILLEDBY=" << killedby[0].layer << "\n"; 
            } */


    //Acquisition of meausered  file, mis.dat
    //the format is in two line fields: Address layer
    //Address in even index, Layer in uneven index
    file_mis >> std::hex >> strutt_temp1;
    strutt_temp.addr = strutt_temp1;
    strutt_temp_cinque = strutt_temp1; 
    strutt_temp1 = (strutt_temp1 >> EE_BIT) & 0xFFFF; /*modificato la posizione del bit ee*/
    while(strutt_temp1!=0xE0DA &&  !file_mis.eof())
    {
      if (laymap == 0)
        strutt_temp.layer = 0xFFFF;
      else
        file_mis >> std::hex >> strutt_temp.layer;  

      strutt_temp.layer |= ignore_bits;
      file_mis >> std::hex >> strutt_temp1;
      strutt_temp.addr |= ignore_bits;
      strutt_mis.push_back(strutt_temp);
      strutt_temp_cinque = strutt_temp1;
      strutt_temp.addr = strutt_temp1;
      strutt_temp1= (strutt_temp1>>EE_BIT) & 0xFFFF; 
    }
    z++;//Counter of the event of measured fileC:\Users\SEMICLab\Desktop\Data Analisi Prototipo 0\source


    /*      for(i=0;i<strutt_mis.size();i++)//Sorted file  in out1.dat
            {
            file_out1<<strutt_mis[i].addr<<"\n";
            file_out1<<strutt_mis[i].layer<<"\n";

            }
            file_out1<<"\n";
            file_out1<<"\n";
            file_out1<<"\n"; */

    if (useKill) { 	/* changing measured roads beased on kill info from RWsim */
      for (i=0; i< strutt_mis.size(); i++) {
        for (j=0; j< killed.size(); j++) {
          if (strutt_mis[i].addr==killed[j].addr) {
            PRINT_LOG("DEBUG: replacing road" << std::hex << strutt_mis[i].addr << " with road " << std::hex << killedby[j].addr << "\n");
            strutt_mis[i] = killedby[j]; /* replacing killed road with killedby road */
          }
        }
      }
    }

    //Sorting part of the program, measured file and reference file are sorted
    //sort increasing
    //Sort measured file
    //sort(strutt_mis.begin(),strutt_mis.end());

    //Sort reference file
    //sort(strutt_rif.begin(),strutt_rif.end());
    /*      for(i=0;i<strutt_rif.size();i++)//Sorted  file is in  out1.dat
            {
            file_out1<<strutt_rif[i].addr<<"\n";
            file_out1<<strutt_rif[i].layer<<"\n";
            }  */	 

    //MD 19.07.2013 
    //mi sono creato due strutture dati composti da due vettori (address, layer)
    //con tutte le road del singolo evento
    //struct_rif -> l'evento di riferimento quello simulato
    //struct_mis -> l'evento dumpato dagli spy buffer 


    if(!file_rif.eof() && !file_mis.eof())  
    {      
      //Writing on video measure of events in reference and meausered file 
      //using .size function 
      PRINT_LOG( "# of roads in Ref/Meas event \t" << std::dec << strutt_rif.size() << "\t" << std::dec<<strutt_mis.size());
      PRINT_LOG( "Event # Ref/Meas \t\t" << k << "\t" << z);
      PRINT_LOG( "EE word Ref/Meas \t\t" << std::hex << strutt_temp_quattro << "\t"<<std::hex<<strutt_temp_cinque<< std::endl);

      strutt_temp_quattro|= ignore_bits;
      strutt_temp_cinque |= ignore_bits;

      if(strutt_temp_quattro!=strutt_temp_cinque)
      {
        PRINT_LOG("\n\n      ******ERROR: EE WORDS DIFFERENT******" << "\n" );
        EE_error_counter++;
      }	 

      if (skip64 && strutt_rif.size()>255) 
      {
        PRINT_LOG("  skipping this event (nroads=" << std::dec << strutt_rif.size() << std::hex << ")"<<"\n" );
      }
      else
      {

        //Compare the structures
        i=0;
        j=0;
        while((i<strutt_rif.size())&&(j<strutt_mis.size()))
        { 
          if(strutt_mis[j].addr==strutt_rif[i].addr)
          {		      
            if(strutt_mis[j].layer==strutt_rif[i].layer)
            {
              i++;//If compare between reference  and mesured event  is positive do not write
              j++; 
            }  
            else
            { 
              PRINT_LOG("ERROR: Meausered and reference Layer are different \n" << 
                  "Address  Meausered"<<"  "<<std::hex<<strutt_mis[j].addr<<
                  "    "<<"Reference"<<"  "<<std::hex<<strutt_rif[i].addr <<
                  "\nbitmap  Measured "<<"  "<<std::hex<<strutt_mis[j].layer
                  <<"    "<<"Reference"<<"  "<<std::hex<<strutt_rif[i].layer
                  <<"\n------------------------------------------------------------------------\n");
              j++;  
              i++;
              error_counter++;
            }
          }	
          else
          {
            if(strutt_mis[j].addr<strutt_rif[i].addr  && strutt_mis[j].layer<=strutt_rif[i].layer) //MD 19.07.2013  check also the bitmap and the jtag chian
            { 
              error_counter++;
              PRINT_LOG("ERROR: ****** The measured event is wrong ****** error counter=" 
                  << error_counter << "\n" 
                  <<"unexpected road: Measured address     "<<std::hex<<strutt_mis[j].addr<<"\n");
              if (laymap==1)
                PRINT_LOG(  "unexpected road: Measured bitmap      "<<std::hex<<strutt_mis[j].layer<<"\n");
              //PRINT_LOG(  "------------------------------------------------------------------------\n");
              j++;  

            }
            else if(strutt_mis[j].addr<strutt_rif[i].addr  && strutt_mis[j].layer!=strutt_rif[i].layer)
            {
              error_counter++;
              PRINT_LOG("ERROR: Meausered and reference Layer are different \n" <<
                  "Address  Meausered"<<"  "<<std::hex<<strutt_mis[j].addr<<
                  "    "<<"Reference"<<"  "<<std::hex<<strutt_rif[i].addr <<
                  "\nbitmap  Measured "<<"  "<<std::hex<<strutt_mis[j].layer
                  <<"    "<<"Reference"<<"  "<<std::hex<<strutt_rif[i].layer
                  <<"\n------------------------------------------------------------------------\n");
              j++;  
              i++;

            }
            else 
            {
              error_counter++;
              PRINT_LOG("ERROR: ****** The measured event is wrong ****** error counter=" 
                  << error_counter << "\n"
                  <<"missing road: Reference address     "<<std::hex<<strutt_rif[i].addr << "\n");
              if (laymap==1)
                PRINT_LOG("missing road: Reference bitmap      "<<std::hex<<strutt_rif[i].layer<<"\n");
              //PRINT_LOG(  "------------------------------------------------------------------------\n");
              i++;
            }
          }
        }//while close

        //If i is not at maximun value there are not a number of roads that is the different between
        // total reference roads and i value 
        if(i<strutt_rif.size())
        {
          PRINT_LOG("ERROR: Missing  Roads:          ");
          for(;i<strutt_rif.size();i++)
          {
            PRINT_LOG(  "Address                   "<<std::hex<<strutt_rif[i].addr);
            if (laymap==1)
              PRINT_LOG("bitmap                    "<<std::hex<<strutt_rif[i].layer<<"\n");
            //PRINT_LOG(  "------------------------------------------------------------------------\n");
            error_counter++;
          }
        }
        //If j is not at maximun value there are more roads and the number is the different beetween
        // total measured roads and j value
        if(j<strutt_mis.size())
        {
          PRINT_LOG("ERROR: Unexepcted Roads:");
          for(;j<strutt_mis.size();j++)
          {
            PRINT_LOG(  "Address               "<<std::hex<<strutt_mis[j].addr);
            if (laymap==1)
              PRINT_LOG("bitmap                "<<std::hex<<strutt_mis[j].layer<<"\n");
            //PRINT_LOG(  "------------------------------------------------------------------------\n");
            error_counter++;
          }
        }
        std::cout<<"\n\n\n\n\n";
      } /* end of skip64 */	 
    }
    else
    {      

      if (!file_rif.eof())
      {
        PRINT_LOG("ERROR: There is no Event \n\n\n");     

        PRINT_LOG("event  "<<std::dec<<k<<"\n");
        minus_error_event++;

        for(i=0;i<strutt_rif.size();i++)
        {
          PRINT_LOG(  "Address                   "<<std::hex<<strutt_rif[i].addr<<"\n");
          if (laymap==1)
            PRINT_LOG("bitmap                    "<<std::hex<<strutt_rif[i].layer<<"\n");
          PRINT_LOG(  "------------------------------------------------------------------------\n");
        }

      }
      if (!file_mis.eof())
      {
        PRINT_LOG("ERROR: There is one more Events \n" );
        plus_error_event++;
        {
          PRINT_LOG("event  "<<z<<"\n");		  

          for(i=0;i<strutt_mis.size();i++)
          {
            PRINT_LOG(  "Address                   "<<std::hex<<strutt_mis[i].addr<<"\n");
            if (laymap==1)
              PRINT_LOG("bitmap                    "<<std::hex<<strutt_mis[i].layer<<"\n");
            PRINT_LOG(  "------------------------------------------------------------------------\n");
          }

        }
      }	    

    }
    strutt_rif.resize(0);
    strutt_mis.resize(0);
    killed.resize(0);
    killedby.resize(0);
  }
  if(error_counter||plus_error_event||minus_error_event||EE_error_counter)
  {
    PRINT_LOG( std::dec << "ERROR:*** PATTERN ERRORS  "<<error_counter<<"  *** EXTRA EVENTS  "<<plus_error_event<<" *** MISSING EVENTS  "<<minus_error_event<<"\n");
    PRINT_LOG( std::dec << "ERROR: End Event word errors *** "<< EE_error_counter <<" *** \n");
  }
  else 
    PRINT_LOG("NO ERROR FOUND\n");

  return true;

}

/** \brief The ambslp_test_VME function tests the VME functionality of the AMB board
 *
 * \param vme is the VMEInterface used to access the board
 * \param loop is the number of loops to be executed. 0 means infinite. The code will exit on the first found error.
 */
bool ambslp_test_VME(VMEInterface *vme, int loop) {

  // main loop
  for (int i=0; i<loop || loop==0; i++) {
    vme->write_word(INIT_AMB, RESET_AMB);
  }

  return true;
} // end of ambslp_test_VME

}
}//namespaces
