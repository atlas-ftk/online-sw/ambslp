/* ambslp_amchip_test_mode.cxx
   Author N. Biesuz 2014
THIS
*/

#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv){
	using namespace  boost::program_options ;

	/////////////////////////////////////////////////////////////////
	//  Parsing parameters using namespace boost::program:options  //
	/////////////////////////////////////////////////////////////////
	options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
		("chainlength", value< std::string >()->default_value("4"), "The chain length. Default value is 4.")
		("chipnum", value< std::string > ()->default_value("15"), "The chips to read. Default value is 15=0xF.")
	;

	positional_options_description pd; 
	pd.add("pattern_file", 1);

	variables_map vm;
	try {
  		store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
	}
	catch( ... ) // In case of errors during the parsing process, desc is printed for help 
	{
  		std::cerr << desc << std::endl;
  		return 1;
	}
	notify(vm);

	if( vm.count("help") ) // if help is required, then desc is printed to output
	{
  		std::cout << std::endl <<  desc << std::endl ;
    		return 0;
	}
	int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
	int chainlength = daq::ftk::string_to_int( vm["chainlength"].as<std::string>() );
	int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );

  return daq::ftk::ambslp_amchip_test_mode(slot, chainlength, chipnum );
}
