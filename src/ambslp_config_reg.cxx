

//#include "ambslp/ambslp_vme_slimB.h"

#include "ambslp/ambslp_mainboard.h"
#include "ambslp/AMBoard.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>




int main(int argc, char **argv) 
{ 
	
	using namespace  boost::program_options ;

	///////////////////////////////////////////
	//  Parsing parameters using namespace boost::program:options
	//
  options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("fixpolarity", value< std::string >()->default_value("0"), "When 1 it fix the polarooty of the link between AMB and AUX")
    // ("boardver", value< std::string >()->default_value("0"), "When 1 it select the first version of the AMBSLP")
    ("dcdc", value< std::string >()->default_value("auto"), "all to turn on DC-DC converter for all LAMB. up for LAMB 0 and 2. down for LAMB 1 and 3")
    ("verbose,v", "If present verbose option will be used")
    ;
        
	variables_map vm;
	try {
		store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
	catch( ... ) // In case of errors during the parsing process, desc is printed for help 
	{
		std::cerr << desc << std::endl; 
		return 1;
	}

	notify(vm);

	if( vm.count("help") ) // if help is required, then desc is printed to output
	{	
		std::cout << std::endl <<  desc << std::endl ; 
		return 0;	
 	}
 	
	int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
	int FixPolarity = daq::ftk::string_to_int( vm["fixpolarity"].as<std::string>() );
	// int BoardVersion = daq::ftk::string_to_int( vm["boardver"].as<std::string>() );
  bool verbose = vm.count("verbose")>0 ? true : false;
	
	unsigned int dc = 0x0;
	if (vm["dcdc"].as<std::string>()=="auto") dc = 0xff;
	else if(vm["dcdc"].as<std::string>()=="all") dc=0x3f;
	else if(vm["dcdc"].as<std::string>()=="up") dc=0x38;
	else if(vm["dcdc"].as<std::string>()=="down") dc=0x07;


	
	daq::ftk::AMBoard myAMB(slot);
  if(verbose) myAMB.setVerbosity(daq::ftk::AMBoard::DEBUG);
  return myAMB.ambslp_config_reg(FixPolarity, dc); 

	//	return daq::ftk::
}


