#include "ambslp/AMChip.h"

AMChip *AMChip::global() {
  static AMChip instance;
  return &instance;
}

AMChip::AMChip() {
  m_NAME = "AMChip";                                       ///Generic AMCHip Name
  m_IDCODE_VAL =  0X000000000;                             ///Generic AMchip IDcode value
  m_MAXPATT_PERCHIP = 0;                                   ///Generic AMchip number of usable patterns
  m_ADDR_SIZE = 0;                                         ///Generic AMchip length of address register
  m_SELFTEST_REG_SIZE = 0;                                 ///Generic AMchip Seleftest configuration register (0XCD) size
}

AMChip05 *AMChip05::global() {
  static AMChip05 instance;
  return &instance;
}

AMChip05::AMChip05() {
  m_NAME = "AM05";                                          ///AMCHip05 Name
  m_IDCODE_VAL = 0x50004071;                                ///AMchip05 IDcode value
  m_MAXPATT_PERCHIP = 2048;                                 ///AMchip05 number of usable patterns
  m_ADDR_SIZE = 16;                                         ///AMchip05 length of address register
  m_SELFTEST_REG_SIZE = 32;                                 ///AMchip05 Seleftest configuration register (0XCD) size
}

AMChip06 *AMChip06::global() {
  static AMChip06 instance;
  return &instance;
}

AMChip06::AMChip06() {
  m_NAME = "AM06";                                          ///AMCHip06 Name
  m_IDCODE_VAL =  0x60003071;                               ///AMchip06 IDcode value
  m_MAXPATT_PERCHIP = 128*1024;                             ///AMchip06 number of usable patterns
  m_ADDR_SIZE = 17;                                         ///AMchip06 length of address register
  m_SELFTEST_REG_SIZE = 46;                                 ///AMchip06 Seleftest configuration register (0XCD) size
}


///////////////////////
// get_serdes_number //
///////////////////////

/** This function returns the parameters needed to address a chip SERDES given his name 
 *
 * @param serdes SERDES name: allowed options patt_duplex;  "pattout"; "pattin1"; "bus0"; "bus1"; "bus2"; "bus3"; "bus4"; "bus5"; "bus6"; "bus7"; 
 * @return It returns a str::pair of integers. Result .first is the serdes_number as required from  SERDES_SEL register. Result .second is rx_tx_enable indicating if the SERDES is rx (0x1), tx (0x2) or rx+tx (0x3). If an error is found the method returns <0xffffffff, 0xffffffff> This number is needed by register RXTXConfReg2.
 *
 */
std::pair<unsigned int, unsigned int> AMChip::get_serdes_number(const char* serdes){
  //serdes you want to act on
  unsigned int serdes_number(0xffffffff), rx_tx_enable(0xffffffff);
  std::pair<unsigned int, unsigned int> to_return;
  if(strcmp(serdes, "patt_duplex")==0){
    serdes_number = 0x0;
    rx_tx_enable = 0x3;
  }else if (strcmp(serdes, "pattout")==0){
    serdes_number = 0x0;
    rx_tx_enable = 0x1;
  }else if (strcmp(serdes, "pattin1")==0){
    serdes_number = 0x2;
    rx_tx_enable = 0x3;
  }else if (strcmp(serdes, "bus0")==0){
    serdes_number = 0x8;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus1")==0){
    serdes_number = 0x9;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus2")==0){
    serdes_number = 0xa;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus3")==0){
    serdes_number = 0xb;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus4")==0){
    serdes_number = 0xc;
    rx_tx_enable=0x2;
  }else if (strcmp(serdes, "bus5")==0){
    serdes_number = 0xd;
    rx_tx_enable=0x2;
  }else if (strcmp(serdes, "bus6")==0){
    serdes_number = 0xe;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus7")==0){
    serdes_number = 0xf;
    rx_tx_enable = 0x2;
  }else{
    FTK_STRING_PARAM_ERROR("AMChip::get_serdes_number(): Found an invalid option for serdes name: '"<<serdes<<"'!");
    to_return = std::make_pair(0xffffffff, 0xffffffff);
    return to_return;
  }
  to_return = std::make_pair(serdes_number, rx_tx_enable);
  return to_return;
}

////////////////////////////////
// get_operation_mode_number  //
////////////////////////////////

/** This function take as input a string with the operation mode of the SERDES ad returns the proper code as required from  SERDES_SEL register.
 *
 * @param operation_mode this is a char* containing the opration mode of the serdes you want to select, the allowed options are; normal, prbs, fixed_pattern.
 * @return an unsigned int containing the operation_mode_number, it returns 0xffffffff if an error is encoutered.
 */

unsigned int AMChip::get_operation_mode_number(const char* operation_mode){
  unsigned int mode(0xffffffff);
       if(strcmp(operation_mode, "normal")==0)          mode=0x0;
  else if(strcmp(operation_mode, "prbs")==0)            mode=0x1;
  else if(strcmp(operation_mode, "fixed_pattern")==0)   mode=0x2;
  else{
    FTK_STRING_PARAM_ERROR("AMChip::get_operation_mode_number: Found an invalid option for operation mode: '"<< operation_mode <<"'. Abort!");
    return 0xffffffff;
  }
  return mode;
}
