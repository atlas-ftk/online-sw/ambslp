//
//  runAMBspybufferchecks.cpp
//  
//
//  Created by Christopher Garnier on 8/5/18.
//

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
#include <ctime>
#include <utility>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <string>
#include <iomanip>

using std::endl;
using std::cout;
using std::cin;
using std::vector;
using std::string;
using std::ifstream;
using std::ofstream;

using namespace boost::program_options;

std::string filepath("/test/file/path");
std::string outname("emon_parsed");

// should be this format ??? this is not used for the time being (Memo by Tomoya, 2018.Sep.12)
void Usage ()
{

 cout<< "    -c /path/to/input Character string that acts as a path to the file where you are getting the unparsed data from."<< '\n';   
 cout<< "    -in fname1        Sends parsed input spybuffers to AMBslpspydumper.cxx" <<'\n';
 cout<< "    -out fname2       Sends parsed output spybuffers to AMBslpspydumper.cxx" << '\n';
 cout<< "    -both fname1 fname2    Sends parsed input and output spbuffers to AMBslpspydumper.cxx"<<'\n';
 
 cout<< '\n' << '\n'; 
 cout<< " -----------------------------READ ME-----------------------------------"<<'\n';
 cout<< " The following commands are for when using this script with FTK_spy_dump.py " << '\n';
 cout<< " Put these commands on the command line is the same format you would with FTK_spy_dump.py" << '\n';
 cout<< " usage of ftk_spy_dump.py: source  ftk_spy_dump.py (argument 1) (argument2) (argument n )"<< '\n'; 
 cout<< " -----------------------------------------------------------------------"<<'\n';
 cout<< "Get events from emon and dump the data. Then check AMB Spy Buffer content vs. Config"<<'\n';
 cout<< "    -spy            Calls ftk_spy_dump.py with arguments chosen from list below."<<'\n';
 cout<< "    -h               Help"<<'\n';
 cout<< "                   Selection criteria"<<'\n';
 cout<< "    -part part          Partition name"<<'\n';
 //cout<< "    -Fm freeze_mask   The mask to be set on freeze status in the common SpyStatus word (def=ignore mask)"<<'\n';
 cout<< "    -t boardTypes    Comma-separated list of board types to be dumped (def=FTK,i.e. all)"<<'\n';
 cout<< "                   Configuration"<<'\n';
 cout<< "    -D ms            Event timeout (def=1000ms)"<<'\n';
 cout<< "    -e evts          Number of events to sample (def=1)"<<'\n';
 cout<< "                   Output options"<<'\n';
 cout<< "    -o dirname       Dump individual spybuffers into files in a SpyDump_YYYYMMDD_HHMMSS subdirectory in the provided directory"<<'\n';
 cout<< "    -S               Only Print the sampler names and stop"<<'\n';
 cout<< "    -rId               Dump robs ids"<<'\n';
 cout<< "    -R               Dump raw data words of the eformat.FullEventFragment"<<'\n';
 cout<< "    -P level         Eformat pretty Print: 1=dump full event header, 2=dump robs header [you will want 2 in most cases]"<<'\n';
 cout<< "    -f fname         [debug] Dump the whole eformat.FullEventFragment into output file"<<'\n';
 cout<< "    -w delay-time    [debug] Delay in ms, the monitor should wait for each"<<'\n'; 
 cout<< "                               event to simulate processing (default 0)"<<'\n';

}

void outputStrings(string outputInputspybuffer, string outputOutputspybuffer, vector<string>& data,int dump)
{
  unsigned int k = 0;

  // parse both input and output spybuffer
  if(dump==2){

      ofstream outspy(outputOutputspybuffer.c_str());
      if(outspy.is_open()){
        cout << "Output file for output spy buffers has successfully opened." << endl;
      }else{
	cout << "Output file for output spy buffers failed to open!" << endl;
      }

      ofstream inpspy(outputInputspybuffer.c_str());
      if(inpspy.is_open()){
        cout << "Output file for input spy buffers has successfully opened." << endl;
      }else{
	cout << "Output file for input spy buffers failed to open!" << endl;
      }

      for(unsigned int j=0; j<(data.size()/8192); j++)
	{
	  if(j>0){
	    k+=8192;
	  }
	  for(unsigned int i=0; i<8192; i++)
	    {
	      if(j%2==0){
		inpspy<< j/2 <<" "<< data.at(i+k)<<endl;
	      }
	      else if( j>0 && !(j%2==0))
		{
		  outspy<< j/2<<" " << data.at(i+k)<<endl;
		}
	    }
	}
      inpspy.close();
      outspy.close();
    }

  // parse input spybuffer
  if(dump==0)
    {
      ofstream inpspy(outputInputspybuffer.c_str());
      if(inpspy.is_open()){
        cout << "Output file for input spy buffers successfully opened." << endl;
      }
      else
	{
	  cout << "Output file for input spy buffers failed to open!" << endl;
	}

      for(unsigned int j=0; j<(data.size()/8192); j++)
	{
	  if(j>0){
	    k+=8192;
	  }
	  for(unsigned int i=0; i<8192; i++)
	    {
	      if(j%2==0){
                inpspy<< j/2 <<" "<< data.at(i+k)<<endl;
	      }
	    }
	}
      inpspy.close();
    }

  // parse output spybuffer
  if(dump==1)
    {
      ofstream outspy(outputOutputspybuffer.c_str());
      if(outspy.is_open()){
        cout << "Output file for output spy buffers has successfully opened." << endl;
      }
      else
	{
	  cout << "Output file for output spy buffers failed to open!" << endl;
	}
      for(unsigned int j=0; j<(data.size()/8192); j++)
	{
	  if(j>0){
	    k+=8192;
	  }
	  for(unsigned int i=0; i<8192; i++)
	    {
	      if(j>0 && !((j%2)==0)){
		outspy<< j/2<< " " << data.at(i+k) << endl;
	      }

	    }

	}
      outspy.close();
    }

}


void parseEMONfile(string filepath,vector<string>& data,vector<string>& numWords,vector<string>& spydata,vector<string>& dec_spydata,vector<string>& description)
{
  ifstream infile(filepath.c_str());
  if(infile.is_open()){
    cout << "Input file opened successfully." << endl;
  }
  else
    {
      cout << "Input file failed to open!" << endl;
    }
  string numLines;
  string firststring;
  string secondstring;
  string thirdstring;
  string oneline;

  while(getline(infile, oneline)){	
    infile >> numLines >> firststring >> secondstring >> thirdstring;

    numWords.push_back(numLines);
    spydata.push_back(firststring);
    dec_spydata.push_back(secondstring);
    description.push_back(thirdstring);

  }
  infile.close();
  string s1;
  string s2;
  for(unsigned int i = 0; i<description.size();i++)
    { 
      s1.assign(description.at(i));
      s2 = "data[";
      if(s1.find(s2) !=string::npos)
	{	
	  data.push_back(spydata.at(i));
					   		
	}
    }
  cout<<data.size()<<endl;
}



void CallFTKspydump(){

}

void CallAMBslpspydump(){
}






	

int main( int argc, char* argv[])
{
    
  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  // options_description desc("Allowed options");
  // //  const unsigned int useDefaultDummyHit = 0xdeadbeef;
  // desc.add_options()
  //   ("help,h", "produce help message")    
  //   ("filepath,f", value<string>(&filepath)->default_value(filepath), "Input file name which was taken by ftk_spy_dump.py")
  //   ("outname,o", value<string>(&outname)->default_value(outname), "Output file name tag.")
  //   // ("banktype,T", value<int>(&bankType)->default_value(bankType), "Pattern bank file type")
  //   // ("ssoffset,O", value<bool>(&ssoffset)->default_value(ssoffset), "Enable the SSOffset or not")
  //   // ("nPattPerChip,N", value<unsigned int>(&nPattPerChip)->default_value(nPattPerChip), "The number of patterns per AM06")
  //   ;
  
  // positional_options_description pd;
  // pd.add("transition", -1);

  // variables_map vm;
  // try {
  //   store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  // }
  // catch( ... ) // In case of errors during the parsing process, desc is printed for help
  //   {
  //     std::cerr << desc << std::endl;
  //     return 1;
  //   }
  // notify(vm);
  
  // if( vm.count("help") ) // if help is required, then desc is printed to output
  //   {
  //     std::cout << std::endl <<  desc << std::endl ;
  //     return 0;
  //   }  
  
  
  vector<string> numWords;
  vector<string> spydata;
  vector<string> dec_spydata;
  vector<string> description;
  vector<string> data;
  string outputInputspybuffer;
  string outputOutputspybuffer;
  string output;
  //string ftk_spy_dump;
  string filepath;
 
  int dump = 2; 
  //cout<<"Please provide a file path to parse data from."<<endl;
  //getline(cin,filepath);




  //if(argv[i] == "-h"||"-p"||"-F"||"-t"||"-D"||"-e"||"-o"||"-s"||"-R"||"-r"||"-P"||"-f"||"-w"||"-L")
  for(int i = 0; i<argc; i++)
    {	
      if(argv[i]==string("-h"))
	{
	  Usage();
	}
      else if(argv[i]==string("-c"))
	{
	  filepath = argv[i+1];
	  i++;
	}
      else if(argv[i]==string("-in"))
	{
	  outputInputspybuffer=argv[i+1];
	  i++;
	  dump = 0;
	}
      else if(argv[i]==string("-out"))
	{
	  outputOutputspybuffer=argv[i+1];
	  i++;
	  dump=1;
	}
      else if(argv[i]==string("-both"))
	{
	  outputInputspybuffer=argv[i+1];
	  outputOutputspybuffer=argv[i+2];
	  i+=2;
	  dump=2;
	}	
      else if(argc == 1)
	{	
	  Usage();
	}
      else if(argv[i]==string("-spy"))
	{
	  string  ftk_spy_dump = "source ftk_spy_dump.py";
	  for(int j = i+1; j<argc; j++)
	    {
	      if(argv[j]==string("-R"))
		{
		  ftk_spy_dump.append(" -R");
		}
	      else if(argv[j]==string("-part"))
		{
		  ftk_spy_dump.append(" -p");
		  ftk_spy_dump.append(" ");
		  string partarg=string(argv[j+1]);
		  ftk_spy_dump.append(partarg);
		  j++;
		}
	      //	else if(argv[i]==string("-Fm"))
	      //	{
	      //	ftk_spy_dump.append(" -F");
	      //	}
	      else if(argv[j]==string("-rId"))
		{
		  ftk_spy_dump.append(" -r");
		}
	      else if(argv[j]==string("-f"))
		{
		  ftk_spy_dump.append(" -f");
		  ftk_spy_dump.append(" ");
		  string farg = string(argv[j+1]);
		  ftk_spy_dump.append(farg);
		  j++;
		}
	      else if(argv[j]==string("-o"))
		{
		  ftk_spy_dump.append(" -o");
		  ftk_spy_dump.append(" ");
		  string oarg = string(argv[j+1]);
		  ftk_spy_dump.append(oarg);
		  j++;
		}
	      else if(argv[j]==string("-P"))
		{
		  ftk_spy_dump.append(" -P");
		  ftk_spy_dump.append(" ");
		  ftk_spy_dump.append(string(argv[j+1]));
		  j++;
		}
	      else if(argv[j]==string("-t"))
		{
		  ftk_spy_dump.append(" -t");
		  ftk_spy_dump.append(" ");
		  ftk_spy_dump.append(string(argv[j+1]));
		  j++;
		}
	      else if(argv[j]==string("-e"))
		{
		  ftk_spy_dump.append(" -e");
		  ftk_spy_dump.append(" ");
		  ftk_spy_dump.append(string(argv[j+1]));
		}
	      else if(argv[j]==string("-D"))
		{
		  ftk_spy_dump.append(" -D");
		  ftk_spy_dump.append(" ");
		  ftk_spy_dump.append(string(argv[j+1]));
		  j++;
		}
	      else if(argv[j]==string("-S"))
		{
		  ftk_spy_dump.append(" -S");
		  ftk_spy_dump.append(" ");
		}
	      else if(argv[j]==string("-w"))
		{
		  ftk_spy_dump.append(" -w");
		  ftk_spy_dump.append(" ");
		  ftk_spy_dump.append(string(argv[j+1]));
		}

		
	      cout<<ftk_spy_dump<<endl;
	      system(ftk_spy_dump.c_str());
	    }
	}
      //else{
      //Usage();
      //}

    }


 
  parseEMONfile(filepath,data,numWords,spydata,dec_spydata,description);
  outputStrings(outputInputspybuffer,outputOutputspybuffer,data,dump);




    


  return EXIT_SUCCESS;
}
