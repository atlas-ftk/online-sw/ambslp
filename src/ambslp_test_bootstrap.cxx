#include "ambslp/status.h"

#include "ambslp/configuration.h"
#include "ambslp/optionHandle.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>


int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      return 0;	
    }

  //
  ///////////////////////////////////////////


  // preparing the input for the configuration bootstrap
  daq::ftk::InitConfiguration init;
  init.slot = 15;

  // this creates the objects for storing the amb status; 
  // if the memory already exists in some form, it just attaches it
  std::unique_ptr<daq::ftk::AMBConfiguration>  ambConfiguration ( daq::ftk::get_AMBConfiguration( &init ));


  // now doing the first step in the configuration: read the board registers
  std::unique_ptr<daq::ftk::Option_VMEReader> opt( new daq::ftk::Option_VMEReader());
  opt->slot = init.slot;

  daq::ftk::OptionHandle optHandle;
  optHandle.option = opt.get();
  ambConfiguration->configure( &optHandle );

  // print info
  ambConfiguration->show( std::cout );

  // try to access the same info by directly accessing the memory
  const daq::ftk::AMBStatus * status = ambConfiguration->status();
  std::cout << "\n\t check: size of status object in memory: " << sizeof(daq::ftk::AMBStatus) << " KB";
  std::cout << "\n\t latest state configured: " << (status->startFromHere).step;
  std::cout << "\n\t directly reading the status: ";
  std::cout << (status->vmeRegisters).connected_LAMBS_mask << " (connected lambs mask)" << std::endl;


}


