/** Author: G. Volpi */

#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_vme_regs.h"
#include "ambslp/ambslp_mainboard.h"
#include "ftkcommon/core.h"

#include <fstream>
#include <vector>
using namespace std;

namespace daq {
  namespace ftk {
    // populate the maps related to the filling of the input FIFOs
    unsigned int AMBoard::LinkIDToLinkWMEKMap[] = {KSCTLINK0, KSCTLINK1, KSCTLINK2, KSCTLINK3,
        KPIXLINK0, KPIXLINK1, KPIXLINK2, KPIXLINK3, KPIXLINK4, KPIXLINK5, KPIXLINK6, KPIXLINK7
    };
    unsigned int AMBoard::LinkIDToLinkWMEFIFOMap[] = {FIFOSCTLINK0, FIFOSCTLINK1, FIFOSCTLINK2, FIFOSCTLINK3,
        FIFOPIXLINK0, FIFOPIXLINK1, FIFOPIXLINK2, FIFOPIXLINK3, FIFOPIXLINK4, FIFOPIXLINK5, FIFOPIXLINK6, FIFOPIXLINK7
    };

    /** The function sets the TMODE for the hit chip.
     * This has to be done before the hit feed
     */
    void AMBoard::feed_hit_init() {
      //cout << "Execute m_vme->write_check_word(CONTROL_REGISTER_HIT, 0x4);   \n";
      // set the the TMODE on the HIT chips, the TMODE is is the 3rd
      //      m_vme->write_check_word(CONTROL_REGISTER_HIT, 0x4);
      unsigned toBeWritten = 0x4;
      m_vme->write_word(CONTROL_REGISTER_HIT, toBeWritten);
      unsigned readValue = m_vme->read_word(CONTROL_REGISTER_HIT);
      if ((readValue & 0x7) != toBeWritten) {
      	FTK_VME_ERROR("Error while setting test mode to use feed hit");
      }
      //cout << "After Execute m_vme->write_check_word(CONTROL_REGISTER_HIT, 0x4);   \n";
    }

    /** The function sends the data loaded to the FIFO to the chips. They
     * can be sent in a single seqeunce, default, or repeated.
     */
    void AMBoard::feed_hit_end(bool loop) {
      if (loop) {
        // send hits in a loop
        m_vme->write_word(CONTROL_REGISTER_HIT, 0x3);
        if (m_vme->read_word(CONTROL_REGISTER_HIT)!=0x7) {
	  FTK_VME_ERROR("Error trying to set loop mode from HIT FIFOs");
        }
      }
      else {
        // send the stream once
        m_vme->write_word(CONTROL_REGISTER_HIT, 0x1);
        if (m_vme->read_word(CONTROL_REGISTER_HIT)!=0x5) {
	  FTK_VME_ERROR("Error trying to set input from HIT FIFOs");
	}
      }
      ERS_LOG("AMBoard::feed_hit_end  loop = " << loop);
    }
    
    /** This function reads hit from a file into the HIT input FIFOS. The input
     * format is assumed to be 2 columns (tag word). The layer where the hits should
     * be loaded has to specified
     */
    void AMBoard::feed_hit(const char * /*fpath*/, int /*bus*/) {
    	//TODO method should allow to support input from 12 file 2-columns format
    }

    /** This function reads hit from a file that represents a set of data words
     * to loaded in all the input busses. 12 columns with 36 bits data words are expected.
     */
    void AMBoard::feed_hit(const char *fpath) {
      // open the input file
      ifstream datafile(fpath);
      if (!datafile) {
	std::stringstream message;
	message << "No hit file is specified to be loaded into the HIT FPGA";
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ers::info(ex);
      } else {

      bool keepGoing(true); // main loop control
			unsigned int read_lines = 0;

      while (keepGoing) { // loop over the feedhit file
				read_lines++;
      	for (unsigned int icol=0; icol!=12; ++icol) { // loop over the columns
      		unsigned long long int fullword;
      		datafile >> hex >> fullword;

      		//cout << icol << " " << fullword << endl;

      		if (datafile.eof()) {
      			// end of the file reached, exit, this is ok if happens at icol==0
      			if (icol==0) {
      				keepGoing = false;
      				break;
      			}
      			else {
      				FTK_VME_ERROR("File finished before expected");
      			}
      		}

      		const unsigned int kword((fullword>>32)&0xf);
      		const unsigned int dword(fullword&0xffffffff);

      		if (kword&0x4) continue; // alignment word, not required in the FIFO

      		// write the data into the bus
      		m_vme->write_check_word(LinkIDToLinkWMEKMap[icol], kword);
      		m_vme->write_check_word(LinkIDToLinkWMEFIFOMap[icol], dword);
      	} // loop over the columns
      } // end loop over the feedhit file

      //ambslp_feed_hit(m_slot, 0, fpath);

      datafile.close();
			ERS_LOG("AMBoard::feed_hit fpath = " << fpath << "  Number of loaded lines = " << read_lines);
      }
    }

    /** Write a stream of 36 bit data in a specific link. */
    void AMBoard::feed_hit(const unsigned long int *data, const unsigned int ndata, int link)  {
      for (unsigned int i=0; i!=ndata; ++i) { // loop over the input data stream
        unsigned int kword = (data[i]>>32) & 0xf; // extract the K-word from the most significant bit
        unsigned int dword = data[i] & 0xffffffff; // the data to be written are filtered from the LSB

        // possible to skip a data word if the k-word has the bit 2 up
        if (kword&0x4) continue;

        // fill the registers for the K-word and the data
        m_vme->write_check_word(LinkIDToLinkWMEKMap[link], kword);
        m_vme->write_check_word(LinkIDToLinkWMEFIFOMap[link], dword);
      } // end loop over the input data stream
    }
  }
}
