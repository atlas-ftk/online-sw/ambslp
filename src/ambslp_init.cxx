/*           Program to issue an AM_INIT signal to AMBFTK board                */
/*           Written for AMBFTK board v1.0   FTK collaboration                 */

//#include "rcc_error/rcc_error.h"
//#include "vme_rcc/vme_rcc.h"
//#include "ambslp/ambslp.h"

#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
using namespace std;

// Parsing input parmaeters and calling the am_init function
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("loop", value<int>()->default_value(1), "executes n times, 0 means infinte loop")
    ("slot", value<int>()->default_value(15), "The card slot")
    ("amb_lamb", value<int>()->default_value(0),"0=Reset ONLY the AMboard, 1=Reset ONLY the LAMBs, 2=Reset ONLY the FIFOs memory inside AMBoard, 3=Reset configuration registers")
    ;

  positional_options_description pd;
    pd.add("amb_lamb", 1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      cerr << "- am_init.cxx : Failure in command_line_parser" << endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      cout << desc << endl;
      return 0;	
    }

  int loop = vm["loop"].as<int>();
  int slot = vm["slot"].as<int>();
  int amb_lamb = vm["amb_lamb"].as<int>();

  //
  ///////////////////////////////////////////

  // calling the function and checking the result
  daq::ftk::AMBoard myAMB(slot);
  return  myAMB.init(amb_lamb, loop) ;
}


