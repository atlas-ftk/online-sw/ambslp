// $Id: 
/************************************************************************/
/*									*/
/* File: am_jtag_func.cpp						*/
/*									*/
/* JTAG helper functions for LAMBs		 			*/
/*									*/
/* 17. Dec. 10  Francesco Crescioli  created				*/
/*									*/
/************************************************************************/

#include "ambslp/ambslp_jtag_func.h"
#include "ambslp/ambslp_vme_jtag_func.h"

#undef SLOW 
//#define SLOW

//#define BLOCK
#undef BLOCK

#define BLOCKSIZE 58

int GotoTestReset(int slot)
{ 
  /*  Puts the state machine in TEST-LOGIC/RESET , starting from any state  */
  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }
  
  return GotoTestReset( vme );

}

int GotoTestReset( VMEInterface *vme )
{ 
  /*  Puts the state machine in TEST-LOGIC/RESET , starting from any state  */
  /* Enable TCK */
  vme->write_word( ENABLE_TCK, FILL_WORD );

  /* tms = 1 , toggle 7 times TCK   */
  for(int j=0;j<7;j++) vme->write_word( TMS, FILL_WORD );

  return 1; 

}

/* ========================================================================== */
int GotoIdlefromReset(int slot, int column)
{

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }
  
  return GotoIdlefromReset( vme, column );

}

int GotoIdlefromReset( VMEInterface *vme, int column)
{
  u_int select_column;

  /* column bits complement */
  select_column = -column-1;  

  /* Enable TCK */
  vme->write_word( ENABLE_TCK, FILL_WORD );
  vme->write_word( TMS, select_column );

  return 1;

}

/* ========================================================================== */
int   GotoShiftIRfromReset(int slot,int column)
{
  /*  Puts the state machine in SHIFT-IR, starting from TEST-LOGIC/RESET  */
  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return GotoShiftIRfromReset( vme, column ); 

}

int  GotoShiftIRfromReset(VMEInterface *vme,int column)
{
  /*  Puts the state machine in SHIFT-IR, starting from TEST-LOGIC/RESET  */
  int j;

  u_int select_column;

  /* column bits complement */
  select_column = -column-1;

  vme->write_word( ENABLE_TCK, FILL_WORD );
  vme->write_word( TMS, select_column );

  /* tms = 1 , toggle 2 times TCK  -> SELECT-IR-SCAN   */
  for(j=0;j<2;j++) vme->write_word( TMS, FILL_WORD );

  /* tms = 0 , toggle 2 times TCK  -> SHIFT-IR         */
  for(j=0;j<2;j++) vme->write_word( TMS, select_column );

  return 1; 
}

/* ========================================================================== */
int   GotoShiftIRfromIdle(int slot, int column)
{
  /*  Puts the state machine in SHIFT-IR, starting from Run-Test/Idle*/
  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return GotoShiftIRfromIdle( vme, column);

}

int   GotoShiftIRfromIdle(VMEInterface *vme, int column)
{
  /*  Puts the state machine in SHIFT-IR, starting from Run-Test/Idle*/
  int j;
  
  u_int complement_column;

  /* column bits complement */
  complement_column = -column-1;
  vme->write_word( ENABLE_TCK, FILL_WORD );

  /* tms = 1 , toggle 2 times TCK  -> SELECT-IR-SCAN   */
  for(j=0;j<2;j++) vme->write_word( TMS, FILL_WORD );

  /* tms = 0 , toggle 2 times TCK  -> SHIFT-IR         */
  for(j=0;j<2;j++) vme->write_word( TMS, complement_column );  
  /* N.B. tms is held low by last instruction !! */

  return 1; 
}

int   GotoShiftDRfromIdle(int slot, int column)
{
  /*  Puts the state machine in SHIFT-DR, starting from IDLE              */
  /*               tms sequence 100              */
  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return GotoShiftDRfromIdle( vme, column);

}

int GotoShiftDRfromIdle(VMEInterface *vme, int column)
{
  /*  Puts the state machine in SHIFT-DR, starting from IDLE              */
  /*               tms sequence 100              */ 
  int j;

  /* Enable TCK */
  vme->write_word(ENABLE_TCK, FILL_WORD);

  /* tms = 1 in selected columns, toggle TCK  -> SELECT-DR-SCAN           */
  vme->write_word(TMS, column);

  /* tms = 0 , toggle 2 times TCK  -> SHIFT-DR         */
  for(j=0;j<2;j++) vme->write_word(TMS, BLANK_WORD);

  return 0; // return value to be removed
}


/*================================================================================================*/
int GotoIdlefromExit1(int slot, int column)
{
  /*Puts the state machine in RUN-TEST/IDLE starting from Exit1-DR or Exit1-IR*/
  /*               tms sequence 10              */
  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return GotoIdlefromExit1(vme, column);

}

int GotoIdlefromExit1(VMEInterface *vme, int column)
{
  /*Puts the state machine in RUN-TEST/IDLE starting from Exit1-DR or Exit1-IR*/
  /*               tms sequence 10              */
  /* Enable TCK */
  vme->write_word (ENABLE_TCK, FILL_WORD);

  /* tms = 1 in selected columns, toggle TCK once  -> UPDATE Reg    */
  vme->write_word (TMS, column);  

  /* tms = 0, toggle TCK  -> RUN-TEST/IDLE           */
  vme->write_word (TMS, BLANK_WORD);

  return 1;
}

/* ========================================================================== */
int GotoSelectDRfromExit1(int slot, int column)
{
  /*Puts the state machine in SelectDR starting from Exit1-DR or Exit1-IR*/
  /*               tms sequence 11              */

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return GotoSelectDRfromExit1(vme, column);

}

int GotoSelectDRfromExit1( VMEInterface *vme, int column)
{
  /*Puts the state machine in SelectDR starting from Exit1-DR or Exit1-IR*/
  /*               tms sequence 11              */

  int i;

  /* Enable TCK */
  vme->write_word (ENABLE_TCK, FILL_WORD);

  for (i=0; i<2; i++) vme->write_word (TMS, column);

  return 1;
}

int StayIdle(int slot)
{

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return StayIdle(vme);

}

int StayIdle(VMEInterface *vme)
{

  /* Enable TCK */
  vme->write_word (ENABLE_TCK, FILL_WORD);

  vme->write_word (TMS, BLANK_WORD);

  return 1;

}

int AMshift(int slot, u_int columns, u_int nwords, const uint32_t *indata, uint32_t *outdata, int myexit ) {

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return AMshift(vme, columns, nwords, indata, outdata, myexit);
}

int AMshift( VMEInterface *vme, u_int columns, u_int nwords,  const uint32_t *indata, uint32_t *outdata, int myexit )
{
  /* This function shifts in a vector of data (tdi)
   * and reads out the corresponding output (tdo)
   * Input:  VISION salve
   *         columns affected columns
   *         nwords # of word to write
   *         indata  array of input data words
   *         optional exit flag
   * Output: VISIONwrite return code
   *         outdata array of output data words
   *
   * Assume the TAP controller is in SHIFT-IR or SHIFT-DR for the affected columns
   * Assume all other columns are in Run-Test/Idle
   * Assume all TMS are LOW
   * Assume outdata is sufficiently large to accept all outcoming words
   * if outdata==NULL no readout is performed,
   * in this case the writing operation is faster.
   * With the exit option on the last write TMS is set to HIGH
   * for selected coulumns in order to leave the SHIFT state
   */

  unsigned int i;
  int status = true;
  //u_int vmedata;

  if(outdata or nwords<4){
    vme->write_word ( ENABLE_TCK, FILL_WORD );
    for (i=0; i<nwords; i++)
    {
      ///if to check the last bit	
      if (myexit && i==nwords-1) /* set TMS HIGH */
      {
        vme->write_word (ENABLE_TCK, BLANK_WORD);
        vme->write_word ( TMS, columns );  
        vme->write_word (ENABLE_TCK, FILL_WORD);
      }

      // read TDO 
      if (outdata)
      {
        vme->write_word (ENABLE_TCK, BLANK_WORD);
        outdata[i] = vme->read_word (TDO); ///, outdata+i);
        vme->write_word (ENABLE_TCK, FILL_WORD);  
      }

      if(indata)
      {
        // write TDI 
        vme->write_word (TDI, indata[i] );
      }
      else
      {
        // write TDI to 0 
        vme->write_word (TDI, BLANK_WORD);
      }	
    }
  }else{
    std::vector<uint32_t> myData;
    myData.reserve(nwords);
    for (i=0; i<nwords; i++){
      if (indata) myData.emplace_back(indata[i]);
      else myData.emplace_back(BLANK_WORD);
    }
    vme->write_word ( ENABLE_TCK, FILL_WORD );
    if (myexit) {
      u_int last_word = myData.back();
      myData.pop_back();
      vme->write_block (TDI, myData ); // send all TDI words except the last one

      // set TMS HIGH on last cycle
      vme->write_word (ENABLE_TCK, BLANK_WORD);
      vme->write_word ( TMS, columns );
      vme->write_word (ENABLE_TCK, FILL_WORD);
      vme->write_word (TDI, last_word );
    }
    else {
      vme->write_block (TDI, myData );
    }
  }
  return status;

}

/* ========================================================================== */
int ConfigureScam(int slot,  int chipno, int column, int scaminstruction, int gotoruntest)
{

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return ConfigureScam(vme, chipno, column, scaminstruction, gotoruntest);

}

int ConfigureScam( VMEInterface *vme, int chipno, int column, int scaminstruction, int gotoruntest)
{
  /* SAME AS CONFIGURECHIP, but for SCAM CHIP!!!!
   * Initialize the bscan chain for the issue of instruction 'instruction'
   * in chips 'chipno', in chain 'column'.
   * variable 'chipno'(32 bits) = chain length(8MSB)|chips number(8LSB)
   * the 8 LSB of chipno represent the active chips in the chain (up to 8 chips)
   * Initial TAP state: Run-Test/Idle
   * Final TAP state: if (gotoruntest==1)  Run-Test/Idle
   *                  if (gotoruntest==0)  SelectDR
   * E.g. chipno = 2
   *  
   *           ======     ======     ======     ======     
   *      TDI _!    !_____!    !_____!    !_____!    !
   *      TMS _!  1 !_____! 2  !_____! 3  !_____! 4  !
   *      TCK _!    !_____!    !_____!    !_____!    !
   *           !    !     ! X  !     !    !     !    !      
   *           ======     ======     ======     ======   
   *           bypass     config     bypass     bypass     
   *
   * the routine downloads the correct instruction and then issues the 
   * correct TMS string to make the instructions current. 
   * The bits set in 'column' (total 32 bits) select columns to act on.
   * Chips in columns that are not selected are put in bypass mode
   * NB actual code implements 8bits instructions for SCAM chips
   * More Instruction compatibility to be added
   *========================================================================= 
   */
  
  int ChipNumber=chipno&0xFF;
  int ChainLength = chipno >> 8;
 // printf("chian length %x, chip number %x\n", ChainLength, ChipNumber);
  const int SCAM_INSTR_LENGTH = 8;
  std::vector<uint32_t> instr(SCAM_INSTR_LENGTH*MAXCHIP_PER_VMECOL*4);
  int i,j;
  int status;

  /* Prepare data */
  int tot_length = (ChainLength-1)*SCAM_INSTR_LENGTH; 
  for (i=0; i<ChainLength; i++)
  {
    for (j=0; j<SCAM_INSTR_LENGTH ;j++)
    {
      int index = tot_length - (i*SCAM_INSTR_LENGTH) + j;
      if (ChipNumber & (1<<i))
      {
        instr[index]  = ( (scaminstruction>>j) & 1) ? FILL_WORD : 0x0;
      }else{
        instr[index] = FILL_WORD;
      }
    }
  }

  status = GotoShiftIRfromIdle( vme, column );
  status &= AMshift(vme, column, SCAM_INSTR_LENGTH*ChainLength, instr.data(), 0, 1);

  if (gotoruntest)
  {
    /* Go to Run-Test/Idle */
    status = GotoIdlefromExit1( vme, column);
  }
  else
  {
    /* Go to SelectDR */
    status = GotoSelectDRfromExit1(vme, column);
    //printf ("SelectDR");    
  }
  return status;  
}

int char2int(char input)
{
  if(input >= '0' && input <= '9')
    return input - '0';
  if(input >= 'A' && input <= 'F')
    return input - 'A' + 10;
  if(input >= 'a' && input <= 'f')
    return input - 'a' + 10;
  return -1;
}

// This function assumes src to be a zero terminated sanitized string with
// an even number of [0-9a-f] characters, and target to be sufficiently large
int hex2bin(const char* src, unsigned char* target)
{
  int ret = 0;
  while(*src && src[1])
  {
    *(target++) = char2int(*src)*16 + char2int(src[1]);
    src += 2;
    ret++;
  }
  return ret;
}

/* CONFIGURE REGISTER (when all columns are written the same)
reg_data: bit array to be written in the register (will be translated into an array of integers in this function)
reg_length: The register length in bits
outdata: The output array with the data
*/
int ConfigureRegister(int slot,  int chipno, int active_columns, int scaminstruction, const bool *reg_data, int reg_length, uint32_t *outdata, bool compare_in_out)
{

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return ConfigureRegister(vme, chipno, active_columns, scaminstruction, reg_data, reg_length, outdata, compare_in_out);

}

int ConfigureRegister(VMEInterface *vme,  int chipno, int active_columns, int scaminstruction, const bool *reg_data, int reg_length, uint32_t *outdata, bool compare_in_out)
{
  int status;
  int ChipNumber=chipno&0xFF;
  int ChainLength = (chipno >> 8)&0xF;

  int numberOfWords = 0;
  std::vector<uint32_t> wrdata (reg_length);
  std::vector<uint32_t> indata (reg_length*ChainLength);

  for(int i=0; i < reg_length; i++)
  {
    if (reg_data[i]) wrdata[i]= active_columns;
    else wrdata[i]= 0x00000000;
  }

  int indataPointer = 0;
  for (int j=ChainLength-1; j>=0; j--)
  {
    if (ChipNumber & (1<<j))
    {
      numberOfWords += reg_length;
      for (int k = 0; k < reg_length; k++)
      {
        indata[k + indataPointer] = wrdata[k];
      }
      indataPointer += reg_length;
    }
    else
    {
      numberOfWords += 1;
      indata[indataPointer] = 0x00000000;
      indataPointer++;
    }
  }
 
  ConfigureScam(vme, chipno, active_columns, scaminstruction, 1);
  GotoShiftDRfromIdle(vme, active_columns);
  status = AMshift(vme, active_columns, numberOfWords, indata.data(), outdata, 1);
  GotoIdlefromExit1(vme, active_columns);

  // commented out to avoid large number of WARNINGS: yet to be understood [Takashi + Carlo] 2017/09/04
  /*
  if(scaminstruction==JPATT_ADDRrd){
    if(compare_in_out){
      u_int diff=0;
      for(int i=0; i<(numberOfWords); i++) {
	if(indata[i]!=outdata[i]) {
	  std::stringstream ss;
	  ss << "Read back information is not the same as input for IR:" << std::hex << scaminstruction << std::dec <<" at "<<i%reg_length<<"th bit, columns with a problem: ";
	  diff = indata[i] ^ outdata[i];
	  for(u_int j=0; j<32; j++) if(diff & (1<<j))  ss << j << " ";
	  ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), ss.str()));
	}
      }
    }
  }
  */

  return status;
}

/* CONFIGURE REGISTER (when each columns is written differently from the others)
reg_data: array of integrers that represent the exact data to be written in the registers
reg_length: The length of the integer array (register_length*2 for AM06, register_length*4 for AM05)
outdata: The output array with the data
*/
int ConfigureRegister(int slot,  int chipno, int active_columns, int scaminstruction, const uint32_t *vme_data, int data_size, uint32_t *outdata, bool compare_in_out)
{

  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  return ConfigureRegister(vme, chipno, active_columns, scaminstruction, vme_data, data_size, outdata, compare_in_out);

}

int ConfigureRegister(VMEInterface *vme,  int chipno, int active_columns, int scaminstruction, const uint32_t *vme_data, int data_size, uint32_t*outdata, bool compare_in_out)
{
  int status;
  int ChainLength = (chipno >> 8)&0xF;

  ConfigureScam(vme, chipno, active_columns, scaminstruction, 1);
  GotoShiftDRfromIdle(vme, active_columns);
  status = AMshift(vme, active_columns, data_size*ChainLength, vme_data, outdata, 1);
  GotoIdlefromExit1(vme, active_columns);

   // commented out to avoid large number of WARNINGS: yet to be understood [Takashi + Carlo] 2017/09/04  
  if(scaminstruction==JPATT_ADDRrd){
    if(compare_in_out){
      u_int diff=0;
      for(int i=0; i<(data_size*ChainLength); i++) {
	if(vme_data[i]!=outdata[i]) {
	  std::stringstream ss;
	  ss << "Read back information is not the same as input for IR:" << std::hex << scaminstruction << std::dec <<" at "<<i%data_size<<"th bit, columns with a problem: ";
	  diff = vme_data[i] ^ outdata[i];
	  for(u_int j=0; j<32; j++) if(diff & (1<<j))  ss << j << " ";
	  ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), ss.str()));
	}
      }
    }
  }

  return status;
}


void PrintRegisterContent(int chipno, int reglength, const uint32_t *regcontent)
{
  int ChipNumber=chipno&0xFF;
  int ChainLength = (chipno >> 8) &0xF;
  int numberOfBytes = reglength/8;
  if (reglength%8 > 0)  numberOfBytes++;

  std::vector<uint32_t> cont0(numberOfBytes);
  std::vector<uint32_t> cont1(numberOfBytes);
  std::vector<uint32_t> cont2(numberOfBytes);
  std::vector<uint32_t> cont3(numberOfBytes);

  for(int lamb=0; lamb<4; lamb++)
  {
    for (int chain =0; chain < 8; chain++)
    {   		
      int datap = 0;
      int chipnum = 0;

      for (int rc = 0; rc < numberOfBytes; rc++)
      {
        cont0[rc] = 0;
        cont1[rc] = 0;
        cont2[rc] = 0;
        cont3[rc] = 0;
      }
      for (int j=ChainLength-1; j>=0; j--)
      {
        if (ChipNumber & (1<<j))
        {
          for (int k = 0; k < reglength; k++)
          {
            int kp = k/8;
            switch (chipnum)
            {
              case 0:
                cont0[kp] = cont0[kp] + (((regcontent[k + datap]>>((lamb*8)+chain))&0x1) << (k%8));
                break;                
              case 1:               
                cont1[kp] = cont1[kp] + (((regcontent[k + datap]>>((lamb*8)+chain))&0x1) << (k%8));
                break;
              case 2:
                cont2[kp] = cont2[kp] + (((regcontent[k + datap]>>((lamb*8)+chain))&0x1) << (k%8));
                break;
              case 3:
                cont3[kp] = cont3[kp] + (((regcontent[k + datap]>>((lamb*8)+chain))&0x1) << (k%8));
                break;
            }
          }
          datap += reglength;
        }
        else
        {
          datap++;
          for (int i = 0; i < numberOfBytes; i++)
          {
            switch (chipnum)
            {
              case 0:
                cont0[i] = 0;
                break;
              case 1:
                cont1[i] = 0;
                break;
              case 2:
                cont2[i] = 0;
                break;
              case 3:
                cont3[i] = 0;
                break;
            }
          }
        }
        chipnum++;
      }
      printf("Chain %i LAMB %i ", chain, lamb);
      for (int l = numberOfBytes - 1; l >= 0; l--)
      {
        if (l == 0) printf("%02x \t", cont0[l]);
        else printf("%02x|", cont0[l]);
      }
      for (int m = numberOfBytes - 1; m >= 0; m--)
      {
        if (m == 0) printf("%02x \t", cont1[m]);
        else printf("%02x|", cont1[m]);
      }
      for (int n = numberOfBytes - 1; n >= 0; n--)
      {
        if (n == 0) printf("%02x \t", cont2[n]);
        else printf("%02x|", cont2[n]);
      }
      for (int o = numberOfBytes - 1; o >= 0; o--)
      {
        if (o == 0) printf("%02x \n", cont3[o]);
        else printf("%02x|", cont3[o]);
      }
    }
  }


}

