/*           Program to test the FLASH RAM functions                 */
/*           Written for AMBSLP board v5.0   FTK collaboration       */

//#include "rcc_error/rcc_error.h"
//#include "vme_rcc/vme_rcc.h"
//#include "ambslp/ambslp.h"

//#include "ambslp/ambslp_mainboard.h"
#include "ambslp/ambslp_vme_regs.h"
#include "ftkvme/VMEManager.h"
#include "ambslp/exceptions.h"
#include "ftkcommon/core.h"

namespace daq {
  namespace ftk {
    namespace amb_vme {
      
      // this list corresponds to the position of the signals in the vme register (VME firmware)
      // the register is: 16bit_data & 16bit_signals (this signals)
      const int CLE      =  0x1;
      const int CEneg    =  0x2;
      const int WEneg    =  0x4;
      const int ALE      =  0x8;
      const int REneg    = 0x10;
      const int WPneg    = 0x20;
      const int OUT_EN   = 0x40;
      const int XIObit   = 16;
      const int XIOmask  = 0xFFFF0000;
      int xx = 0;

      // each of this "state" is used to create the right waveform.
      unsigned int FR_RESET_word = CEneg + WEneg + REneg + WPneg;
      unsigned int FR_IDLE_word =  WEneg + REneg + WPneg;

      unsigned int FR_command_PRE = CLE + REneg + WPneg + OUT_EN;
      unsigned int FR_command_LATCH = CLE + WEneg + REneg + WPneg + OUT_EN;

      unsigned int FR_command_90h_PRE = CLE + REneg + WPneg + OUT_EN + (0x90 << XIObit);
      unsigned int FR_command_90h_LATCH = CLE + WEneg + REneg + WPneg + OUT_EN + (0x90 << XIObit);

      unsigned int FR_ALE_PREPRE = WEneg + ALE + REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_ALE_PRE = ALE + REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_ALE_LATCH = WEneg + ALE + REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_ALE_LATCHLATCH = WEneg + REneg + WPneg + OUT_EN + (0x00 << XIObit);

      unsigned int FR_ALE_00h_PREPRE = WEneg + ALE + REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_ALE_00h_PRE = ALE + REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_ALE_00h_LATCH = WEneg + ALE + REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_ALE_00h_LATCHLATCH = WEneg + REneg + WPneg + OUT_EN + (0x00 << XIObit);

      unsigned int FR_READ_PRE = WEneg + WPneg;

      unsigned int FR_WRITE_PRE = REneg + WPneg + OUT_EN + (0x00 << XIObit);
      unsigned int FR_WRITE =  REneg + (0x00 << XIObit); // vedere se serve
      unsigned int FR_WRITE_LATCH =  WEneg + REneg + WPneg + OUT_EN + (0x00 << XIObit);


      void ambslp_flash_RAM_CMD(VMEInterface *m_vme, int CMD) {
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_command_PRE + (CMD << XIObit) );
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_command_LATCH + (CMD << XIObit) );
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
      }


      void ambslp_flash_RAM_WR(VMEInterface *m_vme, int DATAWR) {
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_WRITE_PRE + (DATAWR << XIObit) );
	//	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_WRITE + (DATAWR << XIObit) ); // non serve?
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_WRITE_LATCH + (DATAWR << XIObit));
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);

      }



      void ambslp_flash_RAM_ADD(VMEInterface *m_vme, int ADD) {
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_PREPRE + (ADD << XIObit) );
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_PRE + (ADD << XIObit) );
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_LATCH + (ADD << XIObit));
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_LATCHLATCH + (ADD << XIObit) );
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
      }



      
      unsigned int ambslp_flash_RAM_read(VMEInterface *m_vme) {
	unsigned int read_word = 0;
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_READ_PRE);
	//read_word = m_vme->read_word(0x10);
        read_word = m_vme->read_word(0x64);
	m_vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
	std::cout << "FR_READ = 0x" << std::hex << read_word << std::dec << "\n";
	return read_word;
      }

      void ambslp_flash_RAM_read_page(VMEInterface *m_vme, int col_add, int row_add) {
	ambslp_flash_RAM_CMD(m_vme,0x00);

	//address decoding to be checked
	ambslp_flash_RAM_ADD(m_vme, col_add & 0xff);
	ambslp_flash_RAM_ADD(m_vme, (col_add >> 8) & 0xff);
	ambslp_flash_RAM_ADD(m_vme, row_add & 0xff);
	ambslp_flash_RAM_ADD(m_vme, (row_add >> 8) & 0xff);
	ambslp_flash_RAM_ADD(m_vme, (row_add >> 16) & 0xff);
	ambslp_flash_RAM_CMD(m_vme,0x30);
	
	//waiting 1 sec insead of 50 us (tr time of the flash ram)
	sleep(1);

	ambslp_flash_RAM_read(m_vme);
	ambslp_flash_RAM_read(m_vme);
	ambslp_flash_RAM_read(m_vme);
	ambslp_flash_RAM_read(m_vme);
	ambslp_flash_RAM_read(m_vme);
        ambslp_flash_RAM_read(m_vme);
        ambslp_flash_RAM_read(m_vme);


       }

      void ambslp_flash_RAM_write_page(VMEInterface *m_vme, int col_add, int row_add) {
	ambslp_flash_RAM_CMD(m_vme,0x80);

	//address decoding to be written
	ambslp_flash_RAM_ADD(m_vme, col_add & 0xff);
	ambslp_flash_RAM_ADD(m_vme, (col_add >> 8) & 0xff);
	ambslp_flash_RAM_ADD(m_vme, row_add & 0xff);
	ambslp_flash_RAM_ADD(m_vme, (row_add >> 8) & 0xff);
	ambslp_flash_RAM_ADD(m_vme, (row_add >> 16) & 0xff);
	
	//waiting 1 sec insead of 50 us (tr time of the flash ram)
	sleep(1);

	ambslp_flash_RAM_WR(m_vme, 0xaa11);
	ambslp_flash_RAM_WR(m_vme, 0xbb22);
	ambslp_flash_RAM_WR(m_vme, 0xcc33);
	ambslp_flash_RAM_WR(m_vme, 0xdd44);
        ambslp_flash_RAM_WR(m_vme, 0x55aa);
        ambslp_flash_RAM_WR(m_vme, 0xaa55);

	ambslp_flash_RAM_CMD(m_vme,0x10);
	sleep(1);
	ambslp_flash_RAM_CMD(m_vme,0x70);  // read command status
	sleep(1);
	
	// leggo se ho avuto errore in scrittura (0 = ok; 1 = errore)
	std::cout << "Page Program report\n";
	std::cout << "0 = ok; 1 = error\n";
	ambslp_flash_RAM_read(m_vme);

       }


      //************************************************************************
      //******************* MAIN ***********************************************
      //************************************************************************

    bool ambslp_test_flash_RAM(int slot, int loop) {

      // try to connect to the amb via vme                                                                                                                    
      VMEInterface *vme = 0;
      try {  vme = VMEManager::global().amb(slot);  }
      catch (daq::ftk::VmeError &ex) {
	daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
        throw issue;
      }
      
	
      printf("ciao marco %x\n",  FR_IDLE_word);

	//	command_word = AMB_VME_BIT_CLE + AMB_VME_BIT_CENEG + AMB_VME_BIT_WENEG + AMB_VME_BIT_ALE + AMB_VME_BIT_RENEG + AMB_VME_BIT_WPNEG + AMB_VME_BIT_3STATE;  
	
      // try to read IDCODE
        std::cout << "\n";
        std::cout << "START\n";
	std::cout << "Reading the IDCODE with low level commands...\n";

	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_RESET_word);
	sleep(1);
	
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_command_90h_PRE);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_command_90h_LATCH);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);

	std::cout << "FR_command_90h done\n";

	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_00h_PREPRE);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_00h_PRE);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_00h_LATCH);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_ALE_00h_LATCHLATCH);
	//vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_RESET_word);
	vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);

	std::cout << "FR_ALE_00h done\n";
        printf("prova marco %x\n", AMB_VME_FLASH_RAM_COMMAND);

	unsigned int read_word = 0;

	for (int ii=0; ii<5; ii++) {
	  read_word = 0;
	  vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_READ_PRE);
	  //read_word = vme->read_word(0x10);
          read_word = vme->read_word(0x64);
	  vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_IDLE_word);
	  //vme->write_check_word(AMB_VME_FLASH_RAM_COMMAND, FR_RESET_word);
	  std::cout << "FR_READ = 0x" << std::hex << read_word << std::dec << "\n";
	}

	std::cout << "DONE1: read IDCODE with low level commands\n";
	std::cout << "\n";
	std::cout << "\n";

	std::cout << "Reading the IDCODE with functions...\n";
	
	// try to read the same idcode with functions
	ambslp_flash_RAM_CMD(vme,0x90);
	ambslp_flash_RAM_ADD(vme, 0x00);
	ambslp_flash_RAM_read(vme);
	ambslp_flash_RAM_read(vme);
	ambslp_flash_RAM_read(vme);
	ambslp_flash_RAM_read(vme);
	ambslp_flash_RAM_read(vme);

	std::cout << "DONE2: read IDCODE with functions\n";
	std::cout << "\n";
	std::cout << "\n";


	// read a page
	std::cout << "Reading part of a memory page...\n";
	ambslp_flash_RAM_read_page(vme, 0 ,0);

	std::cout << "DONE3: read part of a memory page\n";
	std::cout << "\n";
	std::cout << "\n";

	// write a page
	std::cout << "Writing part of a memory page with fixed data...\n";	
	ambslp_flash_RAM_write_page(vme, 0 , 0);  // write fixed data (check the function)
	
	std::cout << "DONE4: write_page\n";
	std::cout << "\n";
	std::cout << "\n";

	// reading again the page
	std::cout << "Reading part of a memory page to check the previous write process...\n";
	ambslp_flash_RAM_read_page(vme, 0 ,0);

	std::cout << "DONE5: read_page\n";	
	std::cout << "\n";
	std::cout << "\n";
	

	

      return true; // return "true" unless something goes wrong
    }

      } // namespace amb_vme
  } // namespace daq
} // namespace ftk



//////////////////////////////////////////////////////////////////////////


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

// Parsing input parmaeters and calling the am_init function
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("loop", value< std::string >()->default_value("1"), "executes n times, 0 means infinte loop")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    //    ("amb_lamb", value< std::string >()->default_value("0"),"0=Reset ONLY the AMboard, 1=Reset ONLY the LAMBs, 2=Reset ONLY the FIFOs memory inside AMBoard, 3=Reset configuration registers")
    //("verbose_level", value< std::string >()->default_value("1"),"0=RunControl, 1=StandAlone")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      FTK_VME_ERROR( "- am_init.cxx : Failure in command_line_parser" );
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      ERS_LOG( desc ); 
      return 0;	
    }

  int loop = daq::ftk::string_to_int( vm["loop"].as<std::string>() );
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  //  int amb_lamb = daq::ftk::string_to_int( vm["amb_lamb"].as<std::string>() );
  //int verbose_level = daq::ftk::string_to_int( vm["verbose_level"].as<std::string>() );

  //
  ///////////////////////////////////////////

  // calling the function and checking the result
  return  daq::ftk::amb_vme::ambslp_test_flash_RAM(slot, loop);
  
}


