#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_vme_regs.h"

#include <memory> //shared_ptr

namespace daq {
  namespace ftk {
    const unsigned int InputSpyBufferStatus [] = { 
      SPYSCTSTATUS0, SPYSCTSTATUS1, SPYSCTSTATUS2, SPYSCTSTATUS3,
      SPYPIXSTATUS0, SPYPIXSTATUS1, SPYPIXSTATUS2, SPYPIXSTATUS3,
      SPYPIXSTATUS4, SPYPIXSTATUS5, SPYPIXSTATUS6, SPYPIXSTATUS7
    };
    const unsigned int InputSpyBufferAddr[] = { 
      ISPY_SCT0, ISPY_SCT1, ISPY_SCT2, ISPY_SCT3,
      ISPY_PIX0, ISPY_PIX1, ISPY_PIX2, ISPY_PIX3,
      ISPY_PIX4, ISPY_PIX5, ISPY_PIX6, ISPY_PIX7
    };

    const unsigned int OutputSpyBufferStatus [] = {
      SPYROADL0STATUS0, SPYROADL0STATUS1, SPYROADL0STATUS2, SPYROADL0STATUS3,
      SPYROADL1STATUS0, SPYROADL1STATUS1, SPYROADL1STATUS2, SPYROADL1STATUS3,
      SPYROADL2STATUS0, SPYROADL2STATUS1, SPYROADL2STATUS2, SPYROADL2STATUS3,
      SPYROADL3STATUS0, SPYROADL3STATUS1, SPYROADL3STATUS2, SPYROADL3STATUS3
    };
    const unsigned int OutputSpyBufferAddr[] = {
      OSPY_L0_LINK0, OSPY_L0_LINK1, OSPY_L0_LINK2, OSPY_L0_LINK3,
      OSPY_L1_LINK0, OSPY_L1_LINK1, OSPY_L1_LINK2, OSPY_L1_LINK3,
      OSPY_L2_LINK0, OSPY_L2_LINK1, OSPY_L2_LINK2, OSPY_L2_LINK3,
      OSPY_L3_LINK0, OSPY_L3_LINK1, OSPY_L3_LINK2, OSPY_L3_LINK3
    };


    const unsigned int SPY_BUFFER_DIM = 13;

    struct str_spy_buff{
      std::vector<u_int> bus;
      u_int eeword;
      u_int nbuff;
      std::vector<u_int> buffer;  
    };


    //_________________________________________________________________________
    /** Freezes all the spy-buffers, preparing to read them.
     *
     * The operation is mandatory */
    bool AMBoard::freezeSpyBuffers() {

      try { m_vme->write_check_word(FREEZE, 0x1); }

      catch (daq::ftk::VmeError &ex) {
        stringstream buffer;
        buffer << "freezeSpyBuffer failed for AMBoard in slot " << m_slot;
        daq::ftk::ftkException issue(ERS_HERE, name_ftk(), buffer.str());
        throw issue;
	return false;
      }
      return true;
    }


    //_________________________________________________________________________
    /** Release the spy-buffer */
    bool AMBoard::unfreezeSpyBuffers() {

      try { m_vme->write_check_word(FREEZE, 0x0); }

      catch (daq::ftk::VmeError &ex) {
        stringstream buffer;
        buffer << "unfreezeSpyBuffer failed for AMBoard in slot " << m_slot;
        daq::ftk::ftkException issue(ERS_HERE, name_ftk(), buffer.str());
        throw issue;
	return false;
      }
      return true;
    }


    //_________________________________________________________________________
    void AMBoard::readInputSpyBuffers() {
      bool old_value = m_vme->getDisableBlockTransfers();
      m_vme->disableBlockTransfers(true);
      m_InputSpyBuffer.clear();
      std::vector< std::shared_ptr< AMBSpyBuffer > >().swap(m_InputSpyBuffer);
      for (size_t i=0; i!=NCHANNELS_HIT_INPUT; ++i) {
	std::shared_ptr< AMBSpyBuffer > ptr_spyBuffer;
        ptr_spyBuffer = std::make_shared< daq::ftk::AMBSpyBuffer >(InputSpyBufferStatus[i], InputSpyBufferAddr[i], m_vme);
        ptr_spyBuffer->setSpyDimension(SPY_BUFFER_DIM); // force the size, as number of bits of the spy buffer
        ptr_spyBuffer->read_spy_buffer();
	m_InputSpyBuffer.push_back(std::move(ptr_spyBuffer));
      }
      m_vme->disableBlockTransfers(old_value);
    }


    //_________________________________________________________________________
    void AMBoard::readOutputSpyBuffers() {
      bool old_value = m_vme->getDisableBlockTransfers();
      m_vme->disableBlockTransfers(true);
      m_OutputSpyBuffer.clear();
      std::vector< std::shared_ptr< AMBSpyBuffer > >().swap(m_OutputSpyBuffer);
      for (size_t i=0; i!=NCHANNELS_ROAD_INPUT; ++i) {
	std::shared_ptr< AMBSpyBuffer > ptr_spyBuffer;
        ptr_spyBuffer = std::make_shared< daq::ftk::AMBSpyBuffer >(OutputSpyBufferStatus[i], OutputSpyBufferAddr[i], m_vme);
        ptr_spyBuffer->setSpyDimension(SPY_BUFFER_DIM); // force the size, as number of bits of the spy buffer
        ptr_spyBuffer->read_spy_buffer();
	m_OutputSpyBuffer.push_back(std::move(ptr_spyBuffer));
      }
      m_vme->disableBlockTransfers(old_value);
    }


    //_________________________________________________________________________
    bool AMBoard::reset_spy(u_int addr, std::string success_message, std::string fail_message) {

      u_int _test_word        = 0x0;
      u_int _expected_value   = _test_word;

      m_vme->write_word(addr, _test_word);

      u_int _register_content = m_vme->read_word(addr);

      if ( ( (_register_content & 0xFFFF) != _expected_value ) ) {

	PRINT_LOG(fail_message);
	return false;
      }

      PRINT_LOG(success_message);
      return true;
    }


    //_________________________________________________________________________
    bool AMBoard::reset_spy_hit() {

      bool _reset_status_SCT_0 = reset_spy(SPYSCTSTATUS0, std::string("Spy Buffer SCT 0 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_SCT_1 = reset_spy(SPYSCTSTATUS1, std::string("Spy Buffer SCT 1 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_SCT_2 = reset_spy(SPYSCTSTATUS2, std::string("Spy Buffer SCT 2 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_SCT_3 = reset_spy(SPYSCTSTATUS3, std::string("Spy Buffer SCT 3 reset!"),   std::string("Unfreeze NOT set!!") );

      bool _reset_status_PIX_0 = reset_spy(SPYPIXSTATUS0, std::string("Spy Buffer PIXEL 0 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_1 = reset_spy(SPYPIXSTATUS1, std::string("Spy Buffer PIXEL 1 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_2 = reset_spy(SPYPIXSTATUS2, std::string("Spy Buffer PIXEL 2 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_3 = reset_spy(SPYPIXSTATUS3, std::string("Spy Buffer PIXEL 3 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_4 = reset_spy(SPYPIXSTATUS4, std::string("Spy Buffer PIXEL 4 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_5 = reset_spy(SPYPIXSTATUS5, std::string("Spy Buffer PIXEL 5 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_6 = reset_spy(SPYPIXSTATUS6, std::string("Spy Buffer PIXEL 6 reset!"), std::string("Unfreeze NOT set!!") );
      bool _reset_status_PIX_7 = reset_spy(SPYPIXSTATUS7, std::string("Spy Buffer PIXEL 7 reset!"), std::string("Unfreeze NOT set!!") );

      bool _reset_status_hit   = (_reset_status_SCT_0 && _reset_status_SCT_1 && _reset_status_SCT_2 && _reset_status_SCT_3 &&
				  _reset_status_PIX_0 && _reset_status_PIX_1 && _reset_status_PIX_2 && _reset_status_PIX_3 && 
				  _reset_status_PIX_4 && _reset_status_PIX_5 && _reset_status_PIX_6 && _reset_status_PIX_7);

      return _reset_status_hit;
    }


    //_________________________________________________________________________
    bool AMBoard::reset_spy_road() {

      bool _reset_status_ROAD_L0_S_0 = reset_spy(SPYROADL0STATUS0, std::string("Spy Buffer ROAD L0 EVEN 0 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L0_S_1 = reset_spy(SPYROADL0STATUS1, std::string("Spy Buffer ROAD L0 EVEN 1 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L0_S_2 = reset_spy(SPYROADL0STATUS2, std::string("Spy Buffer ROAD L0 EVEN 2 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L0_S_3 = reset_spy(SPYROADL0STATUS3, std::string("Spy Buffer ROAD L0 EVEN 3 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L2_S_0 = reset_spy(SPYROADL2STATUS0, std::string("Spy Buffer ROAD L2 EVEN 0 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L2_S_1 = reset_spy(SPYROADL2STATUS1, std::string("Spy Buffer ROAD L2 EVEN 1 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L2_S_2 = reset_spy(SPYROADL2STATUS2, std::string("Spy Buffer ROAD L2 EVEN 2 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L2_S_3 = reset_spy(SPYROADL2STATUS3, std::string("Spy Buffer ROAD L2 EVEN 3 reset!"),   std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L1_S_0 = reset_spy(SPYROADL1STATUS0, std::string("Spy Buffer ROAD L1 ODD 0 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L1_S_1 = reset_spy(SPYROADL1STATUS1, std::string("Spy Buffer ROAD L1 ODD 1 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L1_S_2 = reset_spy(SPYROADL1STATUS2, std::string("Spy Buffer ROAD L1 ODD 2 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L1_S_3 = reset_spy(SPYROADL1STATUS3, std::string("Spy Buffer ROAD L1 ODD 3 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L3_S_0 = reset_spy(SPYROADL3STATUS0, std::string("Spy Buffer ROAD L3 ODD 0 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L3_S_1 = reset_spy(SPYROADL3STATUS1, std::string("Spy Buffer ROAD L3 ODD 1 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L3_S_2 = reset_spy(SPYROADL3STATUS2, std::string("Spy Buffer ROAD L3 ODD 2 reset!"),    std::string("Unfreeze NOT set!!") );
      bool _reset_status_ROAD_L3_S_3 = reset_spy(SPYROADL3STATUS3, std::string("Spy Buffer ROAD L3 ODD 3 reset!"),    std::string("Unfreeze NOT set!!") );

      bool _reset_status_road        = (_reset_status_ROAD_L0_S_0 && _reset_status_ROAD_L0_S_1 && _reset_status_ROAD_L0_S_2 && _reset_status_ROAD_L0_S_3 && 
					_reset_status_ROAD_L2_S_0 && _reset_status_ROAD_L2_S_1 && _reset_status_ROAD_L2_S_2 && _reset_status_ROAD_L2_S_3 && 
					_reset_status_ROAD_L1_S_0 && _reset_status_ROAD_L1_S_1 && _reset_status_ROAD_L1_S_2 && _reset_status_ROAD_L1_S_3 && 
					_reset_status_ROAD_L3_S_0 && _reset_status_ROAD_L3_S_1 && _reset_status_ROAD_L3_S_2 && _reset_status_ROAD_L3_S_3);
      return _reset_status_road;
    }


    //_________________________________________________________________________
    bool AMBoard::resetSpyBuffers(int hit, int road) {

      std::cout << " [VALIDATION]  AMBoard::resetSpyBuffers  " << std::endl ; 


      //Unfreeze the spy buffer 
      spys_freezed = !unfreezeSpyBuffers();
      // m_vme->write_check_word(FREEZE, 0x0);

      bool reset_status_hit  = true;
      bool reset_status_road = true;

      //Reset the input spy buffer
      if (hit  == 1)  reset_status_hit  = reset_spy_hit();

      //reset the spy buffer of the road
      if (road == 1)  reset_status_road = reset_spy_road();

      return (reset_status_hit && reset_status_road);
    }



    //_________________________________________________________________________
    void AMBoard::print_ISPY(u_int address) {

      u_int register_content = m_vme->read_word(address);

      printf("ISPY : %.4x(%s %s) ", (register_content & 0x1fff) , (register_content>>14) & 0x1 ? "o" : "-", (register_content>>15) & 0x1 ? "f" : "-");
    }


    //_________________________________________________________________________
    void AMBoard::print_OSPY(u_int address) {

      u_int register_content = m_vme->read_word(address);

      printf("OSPY : %.4x(%s %s) ", (register_content & 0x1fff) , (register_content>>14) & 0x1 ? "o" : "-", (register_content>>15) & 0x1 ? "f" : "-");
    }


    //_________________________________________________________________________
    bool AMBoard::inputSpyBuffers(int method, int keepFreeze) {

      std::cout << " [VALIDATION]  AMBoard::inputSpyBuffers  " << std::endl ; 

      u_int test_word = 0x0;

      int w0,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11;
      int la_i0, la_i1, la_i2, la_i3, la_i4, la_i5, la_i6, la_i7, la_i8, la_i9, la_i10, la_i11;


      if ((method == 1) || (method == 2) || (method == 3)) {

	spys_freezed = freezeSpyBuffers();

	PRINT_LOG("inputSpyBuffers. Reading\n");

	std::unique_ptr<AMBSpyBuffer> sb[12];
	sb[ 0].reset( new AMBSpyBuffer(SPYSCTSTATUS0, ISPY_SCT0, m_vme)); 
	sb[ 1].reset( new AMBSpyBuffer(SPYSCTSTATUS1, ISPY_SCT1, m_vme)); 
	sb[ 2].reset( new AMBSpyBuffer(SPYSCTSTATUS2, ISPY_SCT2, m_vme)); 
	sb[ 3].reset( new AMBSpyBuffer(SPYSCTSTATUS3, ISPY_SCT3, m_vme)); 
	sb[ 4].reset( new AMBSpyBuffer(SPYPIXSTATUS0, ISPY_PIX0, m_vme)); 
	sb[ 5].reset( new AMBSpyBuffer(SPYPIXSTATUS1, ISPY_PIX1, m_vme)); 
	sb[ 6].reset( new AMBSpyBuffer(SPYPIXSTATUS2, ISPY_PIX2, m_vme)); 
	sb[ 7].reset( new AMBSpyBuffer(SPYPIXSTATUS3, ISPY_PIX3, m_vme));
	sb[ 8].reset( new AMBSpyBuffer(SPYPIXSTATUS4, ISPY_PIX4, m_vme)); 
	sb[ 9].reset( new AMBSpyBuffer(SPYPIXSTATUS5, ISPY_PIX5, m_vme)); 
	sb[10].reset( new AMBSpyBuffer(SPYPIXSTATUS6, ISPY_PIX6, m_vme)); 
	sb[11].reset( new AMBSpyBuffer(SPYPIXSTATUS7, ISPY_PIX7, m_vme));


	// ---------------------------------------------------- method 1

	if (method == 1) {

	  for (int ispy = 0; ispy < 12; ispy++) {

	    sb[ispy]->read_spy_buffer();
	    std::vector<unsigned int> d_ptr = sb[ispy]->getBuffer();

	    for (unsigned int i = 0; i < d_ptr.size(); i++) {

	      std::cerr << std::hex << ispy << " ";
	      std::cerr << std::setw(8)  << std::setfill('0') << std::right << std::hex << d_ptr.at(i) << std::endl;
	    }
	  }


	// ---------------------------------------------------- method 2

	} else if (method == 2) {

	  std::vector<str_spy_buff> spyBuffers;
	  spyBuffers.clear();

	  for (int ispy = 0; ispy < 12; ispy++) {

	    sb[ispy]->read_spy_buffer();
	    std::vector<unsigned int> d_ptr = sb[ispy]->getBuffer();

	    bool  first_ee = false;
	    bool  already  = false;
	    u_int ee       = 0;
	    std::vector<u_int> tbuff;
	    tbuff.clear();

	    for (unsigned int iptr = 0; iptr < d_ptr.size(); iptr++) {

	      if ( !first_ee && (((d_ptr.at(iptr)>>24)&0xff) == 0xf7) ) { first_ee = true; continue; }
	      if (  first_ee && (((d_ptr.at(iptr)>>24)&0xff) != 0xf7) ) { tbuff.push_back(d_ptr.at(iptr)); continue; }
	      if (  first_ee && (((d_ptr.at(iptr)>>24)&0xff) == 0xf7) ) {

		ee = d_ptr.at(iptr);
		for (int j = 0; j < (int)spyBuffers.size(); j++) {
		  if (spyBuffers.at(j).eeword == ee) {
		    spyBuffers.at(j).nbuff++;
		    for (int k = 0; k < (int)tbuff.size(); k++) {
		      spyBuffers.at(j).buffer.push_back(tbuff.at(k));
		      spyBuffers.at(j).bus.push_back(ispy);
		    }
		    already = true;
		  }
		}
		if (!already) {// In case spyBuffers.size()==0 or no spyBuffer for the ee

		  str_spy_buff s_str;
		  s_str.eeword = ee;
		  s_str.nbuff  = 1;

		  for (int j = 0; j < (int)tbuff.size(); j++) {
		    s_str.buffer.push_back(tbuff.at(j));
		    s_str.bus.push_back(ispy);
		  }
		  spyBuffers.push_back(s_str);
		}
		already = false;
		tbuff.clear();
	      }
	    }
	  }

	  for (int ibuf = 0; ibuf < (int)spyBuffers.size(); ibuf++) {

	    if (spyBuffers.at(ibuf).nbuff == 12) {
	      for (int j = 0; j < (int)spyBuffers.at(ibuf).buffer.size(); j++) {
		std::cerr << std::hex << spyBuffers.at(ibuf).bus.at(j) << " ";
		std::cerr << std::setw(8) << std::setfill('0') << std::right << std::hex << spyBuffers.at(ibuf).buffer.at(j)<<std::endl;
	      }
	      std::cerr << std::hex << 15 << " ";	      
	      std::cerr << std::hex <<spyBuffers.at(ibuf).eeword<<std::endl;
	    }
	  }


	// ---------------------------------------------------- method 3

	} else if (method == 3) {

	  std::vector<str_spy_buff> spyBuffers; spyBuffers.clear();
	  std::vector<std::vector<EventFragment*>> fragmentss; fragmentss.clear();

	  for (int ispy = 0; ispy < 12; ispy++) {
	    // skip masked channels
	    sb[ispy]->read_spy_buffer();
	    fragmentss.push_back(AMBHit_splitFragments(sb[ispy]->getBuffer()));
	  }

	  std::vector<EventFragmentCollection*> eventCollections = build_eventFragmentCollection(fragmentss);

	  for (int i = 0; i < (int)eventCollections.size(); i++){
	    for(int ivecs = 0; ivecs < 12; ivecs++){

	      EventFragmentAMBHit* efah = (static_cast<EventFragmentAMBHit*>((eventCollections.at(i))->getEventFragment(ivecs)));
	      AMBHitObjectEvent    ahoe = efah->getEventHits();

	      for (int ivec = 0; ivec < (int)ahoe.getNHit(); ivec++) {

		AMBHitObject aho = (ahoe.getHits()).at(ivec);
		aho.print();
	      }
	    }
	  }

	} // method 3


	if ( !keepFreeze ) {

	  spys_freezed = !unfreezeSpyBuffers();
	}

	return true;
      }



      // ---------------------------------------------------- then this is method 0 ...


      spys_freezed = freezeSpyBuffers();

      PRINT_LOG("inputSpyBuffers. Reading\n");

      la_i0  = ( (m_vme->read_word(SPYSCTSTATUS0)) & 0x1fff );
      la_i1  = ( (m_vme->read_word(SPYSCTSTATUS1)) & 0x1fff );
      la_i2  = ( (m_vme->read_word(SPYSCTSTATUS2)) & 0x1fff );
      la_i3  = ( (m_vme->read_word(SPYSCTSTATUS3)) & 0x1fff );
      la_i4  = ( (m_vme->read_word(SPYPIXSTATUS0)) & 0x1fff );
      la_i5  = ( (m_vme->read_word(SPYPIXSTATUS1)) & 0x1fff );
      la_i6  = ( (m_vme->read_word(SPYPIXSTATUS2)) & 0x1fff );
      la_i7  = ( (m_vme->read_word(SPYPIXSTATUS3)) & 0x1fff );
      la_i8  = ( (m_vme->read_word(SPYPIXSTATUS4)) & 0x1fff );
      la_i9  = ( (m_vme->read_word(SPYPIXSTATUS5)) & 0x1fff );
      la_i10 = ( (m_vme->read_word(SPYPIXSTATUS6)) & 0x1fff );
      la_i11 = ( (m_vme->read_word(SPYPIXSTATUS7)) & 0x1fff );


      for (int index = 0; index < (1<<SPY_BUFFER_DIM); ++index) {  //for 0 to 8192 !! 

	// [TBC] What is the purpouse of this for loop? Only the last iteration will be stored in the wX's variables ... is it really needed??

	w0  = m_vme->read_word( ISPY_SCT0 + ( ( (index + la_i0 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w1  = m_vme->read_word( ISPY_SCT1 + ( ( (index + la_i1 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w2  = m_vme->read_word( ISPY_SCT2 + ( ( (index + la_i2 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w3  = m_vme->read_word( ISPY_SCT3 + ( ( (index + la_i3 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );

	w4  = m_vme->read_word( ISPY_PIX0 + ( ( (index + la_i4 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w5  = m_vme->read_word( ISPY_PIX1 + ( ( (index + la_i5 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w6  = m_vme->read_word( ISPY_PIX2 + ( ( (index + la_i6 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w7  = m_vme->read_word( ISPY_PIX3 + ( ( (index + la_i7 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w8  = m_vme->read_word( ISPY_PIX4 + ( ( (index + la_i8 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w9  = m_vme->read_word( ISPY_PIX5 + ( ( (index + la_i9 ) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w10 = m_vme->read_word( ISPY_PIX6 + ( ( (index + la_i10) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	w11 = m_vme->read_word( ISPY_PIX7 + ( ( (index + la_i11) % (1<<SPY_BUFFER_DIM) ) << 2 ) );


	printf("[%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x [%.4x] %08x\n",
	       ( (index + la_i0)  % (1<<SPY_BUFFER_DIM) ), w0,
	       ( (index + la_i1)  % (1<<SPY_BUFFER_DIM) ), w1,
	       ( (index + la_i2)  % (1<<SPY_BUFFER_DIM) ), w2,
	       ( (index + la_i3)  % (1<<SPY_BUFFER_DIM) ), w3,
	       ( (index + la_i4)  % (1<<SPY_BUFFER_DIM) ), w4,
	       ( (index + la_i5)  % (1<<SPY_BUFFER_DIM) ), w5,
	       ( (index + la_i6)  % (1<<SPY_BUFFER_DIM) ), w6,
	       ( (index + la_i7)  % (1<<SPY_BUFFER_DIM) ), w7,
	       ( (index + la_i8)  % (1<<SPY_BUFFER_DIM) ), w8,
	       ( (index + la_i9)  % (1<<SPY_BUFFER_DIM) ), w9,
	       ( (index + la_i10) % (1<<SPY_BUFFER_DIM) ), w10,
	       ( (index + la_i11) % (1<<SPY_BUFFER_DIM) ), w11);
      }



      printf("SCT SPY STATUS\n");
      print_ISPY(SPYSCTSTATUS0);
      print_ISPY(SPYSCTSTATUS1);
      print_ISPY(SPYSCTSTATUS2);
      print_ISPY(SPYSCTSTATUS3);
      printf("\n");

      printf("PIXEL SPY STATUS\n");
      print_ISPY(SPYPIXSTATUS0);
      print_ISPY(SPYPIXSTATUS1);
      print_ISPY(SPYPIXSTATUS2);
      print_ISPY(SPYPIXSTATUS3);
      print_ISPY(SPYPIXSTATUS4);
      print_ISPY(SPYPIXSTATUS5);
      print_ISPY(SPYPIXSTATUS6);
      print_ISPY(SPYPIXSTATUS7);
      printf("\n");



      // Write 0 to unfreeze
      if ( !keepFreeze ) {

	spys_freezed = !unfreezeSpyBuffers();
      }


      return true;
    }



    // ===========================================================================================

    bool AMBoard::outputSpyBuffers(int method, int keepFreeze) {

      std::cout << " [VALIDATION]  AMBoard::outputSpyBuffers  " << std::endl ; 

      std::cout << "outputSpyBuffers() SLOT " << std::to_string(m_slot) << std::endl;

      u_int test_word = 0x0;

      int wo0,wo1,wo2,wo3,wo4,wo5,wo6,wo7;
      int we0,we1,we2,we3,we4,we5,we6,we7;
      int la_o0, la_o1, la_o2, la_o3, la_o4, la_o5, la_o6, la_o7;
      int la_e0, la_e1, la_e2, la_e3, la_e4, la_e5, la_e6, la_e7;


      unsigned int LAMBs = getLAMBMap();

      bool DisableBlockTransfers_old_value = m_vme->getDisableBlockTransfers();
      m_vme->disableBlockTransfers(true);

      int spymask = 0x0;
      for (int ii = 0; ii < MAXLAMB_PER_BOARD; ii++) 
	if (LAMBs & (1<<ii) ) spymask |= 0xF << (4*ii);


      int nLink = 0;
      for (int ibit = 0; ibit < 16; ibit++)
	if ( ( (spymask>>ibit) & 1 ) == 1 )  ++nLink;


      if ( (method == 1) || (method == 2) || (method == 3) ) {

	spys_freezed = freezeSpyBuffers();

	PRINT_LOG("outputSpyBuffers. Reading\n");

	std::unique_ptr<AMBSpyBuffer> sb[16];
	if (spymask&(1<<0) ) sb[ 0].reset( new AMBSpyBuffer(SPYROADL0STATUS0, OSPY_L0_LINK0, m_vme) );
	if (spymask&(1<<1) ) sb[ 1].reset( new AMBSpyBuffer(SPYROADL0STATUS1, OSPY_L0_LINK1, m_vme) );
	if (spymask&(1<<2) ) sb[ 2].reset( new AMBSpyBuffer(SPYROADL0STATUS2, OSPY_L0_LINK2, m_vme) );
	if (spymask&(1<<3) ) sb[ 3].reset( new AMBSpyBuffer(SPYROADL0STATUS3, OSPY_L0_LINK3, m_vme) );
	if (spymask&(1<<4) ) sb[ 4].reset( new AMBSpyBuffer(SPYROADL1STATUS0, OSPY_L1_LINK0, m_vme) );
	if (spymask&(1<<5) ) sb[ 5].reset( new AMBSpyBuffer(SPYROADL1STATUS1, OSPY_L1_LINK1, m_vme) );
	if (spymask&(1<<6) ) sb[ 6].reset( new AMBSpyBuffer(SPYROADL1STATUS2, OSPY_L1_LINK2, m_vme) );
	if (spymask&(1<<7) ) sb[ 7].reset( new AMBSpyBuffer(SPYROADL1STATUS3, OSPY_L1_LINK3, m_vme) );
	if (spymask&(1<<8) ) sb[ 8].reset( new AMBSpyBuffer(SPYROADL2STATUS0, OSPY_L2_LINK0, m_vme) );
	if (spymask&(1<<9) ) sb[ 9].reset( new AMBSpyBuffer(SPYROADL2STATUS1, OSPY_L2_LINK1, m_vme) );
	if (spymask&(1<<10)) sb[10].reset( new AMBSpyBuffer(SPYROADL2STATUS2, OSPY_L2_LINK2, m_vme) );
	if (spymask&(1<<11)) sb[11].reset( new AMBSpyBuffer(SPYROADL2STATUS3, OSPY_L2_LINK3, m_vme) );
	if (spymask&(1<<12)) sb[12].reset( new AMBSpyBuffer(SPYROADL3STATUS0, OSPY_L3_LINK0, m_vme) );
	if (spymask&(1<<13)) sb[13].reset( new AMBSpyBuffer(SPYROADL3STATUS1, OSPY_L3_LINK1, m_vme) );
	if (spymask&(1<<14)) sb[14].reset( new AMBSpyBuffer(SPYROADL3STATUS2, OSPY_L3_LINK2, m_vme) );
	if (spymask&(1<<15)) sb[15].reset( new AMBSpyBuffer(SPYROADL3STATUS3, OSPY_L3_LINK3, m_vme) );


	// ---------------------------------------------------- method 1

	if ( method == 1 ) {

	  for (int ispy = 0; ispy < 16; ispy++) {

	    if ( (spymask & (1<<ispy)) == 0 ) continue;

	    sb[ispy]->read_spy_buffer();
	    std::vector<unsigned int> d_ptr = sb[ispy]->getBuffer();
	    for (unsigned int i = 0; i < d_ptr.size(); i++) {

	      std::cerr << std::setw(8) << std::setfill('0') << std::right << std::hex << d_ptr.at(i) << std::endl;
	    }
	  }


	// ---------------------------------------------------- method 2

	} else if ( method == 2 ) {

	  std::vector<str_spy_buff> spyBuffers;
	  spyBuffers.clear();

	  for (int ispy = 0; ispy < 16; ispy++) {

	    if ( (spymask & (1<<ispy)) == 0 ) continue;

	    sb[ispy]->read_spy_buffer();
	    std::vector<unsigned int> d_ptr = sb[ispy]->getBuffer();
	    bool  first_ee = false;
	    bool  already  = false;
	    u_int ee       = 0;
	    std::vector<u_int> tbuff;
	    tbuff.clear();

	    for (unsigned int iptr = 0; iptr < d_ptr.size(); iptr++) {

	      if ( !first_ee && ( ((d_ptr.at(iptr)>>20) & 0xfff) == 0xf78 ) ) { first_ee = true;                 continue; }
	      if (  first_ee && ( ((d_ptr.at(iptr)>>20) & 0xfff) != 0xf78 ) ) { tbuff.push_back(d_ptr.at(iptr)); continue; }
	      if (  first_ee && ( ((d_ptr.at(iptr)>>20) & 0xfff) == 0xf78 ) ) {

		ee = d_ptr.at(iptr);

		for (int j = 0; j < (int)spyBuffers.size(); j++) {

		  if ( spyBuffers.at(j).eeword == ee ) {

		    spyBuffers.at(j).nbuff++;

		    for (int k = 0; k < (int)tbuff.size(); k++) {

		      spyBuffers.at(j).buffer.push_back(tbuff.at(k));
		      spyBuffers.at(j).bus.push_back(ispy);
		    }
		    already = true;
		  }
		}

		if ( !already ) {// In case spyBuffers.size()==0 or no spyBuffer for the ee

		  str_spy_buff s_str;
		  s_str.eeword = ee;
		  s_str.nbuff  = 1;

		  for (int j = 0; j < (int)tbuff.size(); j++) {

		    s_str.buffer.push_back(tbuff.at(j));
		    s_str.bus.push_back(ispy);
		  }
		  spyBuffers.push_back(s_str);
		}
		already = false;
		tbuff.clear();

	      }  //  if (  first_ee && ( ((d_ptr.at(iptr)>>20)&0xfff) == 0xf78 ) )

	    }  // for iptr
	  }  // for ispy

	  for (int ibuf = 0; ibuf < (int)spyBuffers.size(); ibuf++) {

	    if ( spyBuffers.at(ibuf).nbuff == static_cast<unsigned int>(nLink) ) {

	      for (int j = 0; j < (int)spyBuffers.at(ibuf).buffer.size(); j++) {
		std::cerr << std::setw(8) << std::setfill('0') << std::right << std::hex << spyBuffers.at(ibuf).buffer.at(j) << std::endl;
	      }
	      std::cerr << std::hex << spyBuffers.at(ibuf).eeword << std::endl;
	    }
	  }


	// ---------------------------------------------------- method 3

	} else if ( method == 3 ) {

	  std::vector<str_spy_buff>                spyBuffers;
	  spyBuffers.clear();
	  std::vector<std::vector<EventFragment*>> fragmentss;
	  fragmentss.clear();

	  for (int ispy = 0; ispy < 16; ispy++) {
	    // skip masked channels
	    if ( (spymask & (1<<ispy)) == 0 ) continue;

	    sb[ispy]->read_spy_buffer();
	    fragmentss.push_back( AMBRoad_splitFragments( sb[ispy]->getBuffer() ) );
	  }

	  std::vector<EventFragmentCollection*> eventCollections = build_eventFragmentCollection(fragmentss);

	  for (int i = 0; i < (int)eventCollections.size(); i++) {

	    for (int ivecs = 0; ivecs < nLink; ivecs++) {

	      EventFragmentAMBRoad* efar = (static_cast<EventFragmentAMBRoad*>( (eventCollections.at(i)) -> getEventFragment(ivecs) ) );
	      AMBRoadObjectEvent    aroe = efar->getEventRoads();

	      for (int ivec = 0; ivec < (int)aroe.getNRoad(); ivec++) {

		AMBRoadObject aro = (aroe.getRoads()).at(ivec);
		aro.print();
	      }
	    }
	  }
	}

	if ( !keepFreeze ) {

	  spys_freezed = !unfreezeSpyBuffers();
	}


	m_vme->disableBlockTransfers(DisableBlockTransfers_old_value);

	return true;

      }



      // ---------------------------------------------------- then this is method 0 ...




      spys_freezed = freezeSpyBuffers();

      // [TBC] this check needs to be re-implemented - on the status of the previous vme operation (write_check FREEZE) : implemented with spys_freezed

      // if ( true ) {
      if ( spys_freezed ) {

	PRINT_LOG("outputSpyBuffers. Reading\n");

	//road even
	la_e0 = ( (m_vme->read_word(SPYROADL0STATUS0)) & 0x1fff );
	la_e1 = ( (m_vme->read_word(SPYROADL0STATUS1)) & 0x1fff );
	la_e2 = ( (m_vme->read_word(SPYROADL0STATUS2)) & 0x1fff );
	la_e3 = ( (m_vme->read_word(SPYROADL0STATUS3)) & 0x1fff );

	la_e4 = ( (m_vme->read_word(SPYROADL1STATUS0)) & 0x1fff );
	la_e5 = ( (m_vme->read_word(SPYROADL1STATUS1)) & 0x1fff );
	la_e6 = ( (m_vme->read_word(SPYROADL1STATUS2)) & 0x1fff );
	la_e7 = ( (m_vme->read_word(SPYROADL1STATUS3)) & 0x1fff );

	//road odd
	la_o0 = ( (m_vme->read_word(SPYROADL2STATUS0)) & 0x1fff );
	la_o1 = ( (m_vme->read_word(SPYROADL2STATUS1)) & 0x1fff );
	la_o2 = ( (m_vme->read_word(SPYROADL2STATUS2)) & 0x1fff );
	la_o3 = ( (m_vme->read_word(SPYROADL2STATUS3)) & 0x1fff );

	la_o4 = ( (m_vme->read_word(SPYROADL3STATUS0)) & 0x1fff );
	la_o5 = ( (m_vme->read_word(SPYROADL3STATUS1)) & 0x1fff );
	la_o6 = ( (m_vme->read_word(SPYROADL3STATUS2)) & 0x1fff );
	la_o7 = ( (m_vme->read_word(SPYROADL3STATUS3)) & 0x1fff );


	for(int index = 0; index < (1<<SPY_BUFFER_DIM); ++index) {  //for 0 to 8192


	  // [TBC] What is the purpouse of this for loop? Only the last iteration will be stored in the wX's variables ... is it really needed ??

	  //road even
	  we0  = m_vme->read_word( OSPY_L0_LINK0 + ( ( (index+la_e0) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  we1  = m_vme->read_word( OSPY_L0_LINK1 + ( ( (index+la_e1) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  we2  = m_vme->read_word( OSPY_L0_LINK2 + ( ( (index+la_e2) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  we3  = m_vme->read_word( OSPY_L0_LINK3 + ( ( (index+la_e3) % (1<<SPY_BUFFER_DIM) ) << 2 ) );

	  we4  = m_vme->read_word( OSPY_L1_LINK0 + ( ( (index+la_e4) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  we5  = m_vme->read_word( OSPY_L1_LINK1 + ( ( (index+la_e5) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  we6  = m_vme->read_word( OSPY_L1_LINK2 + ( ( (index+la_e6) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  we7  = m_vme->read_word( OSPY_L1_LINK3 + ( ( (index+la_e7) % (1<<SPY_BUFFER_DIM) ) << 2 ) );

	  //road odd
	  wo0  = m_vme->read_word( OSPY_L2_LINK0 + ( ( (index+la_o0) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  wo1  = m_vme->read_word( OSPY_L2_LINK1 + ( ( (index+la_o1) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  wo2  = m_vme->read_word( OSPY_L2_LINK2 + ( ( (index+la_o2) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  wo3  = m_vme->read_word( OSPY_L2_LINK3 + ( ( (index+la_o3) % (1<<SPY_BUFFER_DIM) ) << 2 ) );

	  wo4  = m_vme->read_word( OSPY_L3_LINK0 + ( ( (index+la_o4) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  wo5  = m_vme->read_word( OSPY_L3_LINK1 + ( ( (index+la_o5) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  wo6  = m_vme->read_word( OSPY_L3_LINK2 + ( ( (index+la_o6) % (1<<SPY_BUFFER_DIM) ) << 2 ) );
	  wo7  = m_vme->read_word( OSPY_L3_LINK3 + ( ( (index+la_o7) % (1<<SPY_BUFFER_DIM) ) << 2 ) );


	  // print the spy-buffer status, orizontally formatted
	  cout.fill('0');
	  if (spymask&(1<< 0)) cout << '[' << hex << setw(4) << ( (index+la_e0) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we0 << ' ';
	  if (spymask&(1<< 1)) cout << '[' << hex << setw(4) << ( (index+la_e1) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we1 << ' ';
	  if (spymask&(1<< 2)) cout << '[' << hex << setw(4) << ( (index+la_e2) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we2 << ' ';
	  if (spymask&(1<< 3)) cout << '[' << hex << setw(4) << ( (index+la_e3) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we3 << ' ';
	  if (spymask&(1<< 4)) cout << '[' << hex << setw(4) << ( (index+la_e4) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we4 << ' ';
	  if (spymask&(1<< 5)) cout << '[' << hex << setw(4) << ( (index+la_e5) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we5 << ' ';
	  if (spymask&(1<< 6)) cout << '[' << hex << setw(4) << ( (index+la_e6) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we6 << ' ';
	  if (spymask&(1<< 7)) cout << '[' << hex << setw(4) << ( (index+la_e7) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  we7 << ' ';
	  if (spymask&(1<< 8)) cout << '[' << hex << setw(4) << ( (index+la_o0) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo0 << ' ';
	  if (spymask&(1<< 9)) cout << '[' << hex << setw(4) << ( (index+la_o1) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo1 << ' ';
	  if (spymask&(1<<10)) cout << '[' << hex << setw(4) << ( (index+la_o2) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo2 << ' ';
	  if (spymask&(1<<11)) cout << '[' << hex << setw(4) << ( (index+la_o3) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo3 << ' ';
	  if (spymask&(1<<12)) cout << '[' << hex << setw(4) << ( (index+la_o4) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo4 << ' ';
	  if (spymask&(1<<13)) cout << '[' << hex << setw(4) << ( (index+la_o5) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo5 << ' ';
	  if (spymask&(1<<14)) cout << '[' << hex << setw(4) << ( (index+la_o6) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo6 << ' ';
	  if (spymask&(1<<15)) cout << '[' << hex << setw(4) << ( (index+la_o7) % (1<<SPY_BUFFER_DIM) ) << "] " << setw(8) <<  wo7;
	  cout.fill();
	  cout << endl;
	}

	//CHECK THE NUMBER OF THE WORD

	printf("ROAD SPY STATUS: LAMB0\n");
	print_OSPY(SPYROADL0STATUS0);
	print_OSPY(SPYROADL0STATUS1);
	print_OSPY(SPYROADL0STATUS2);
	print_OSPY(SPYROADL0STATUS3);
	printf("\n");

	printf("ROAD SPY STATUS: LAMB1\n");
	print_OSPY(SPYROADL1STATUS0);
	print_OSPY(SPYROADL1STATUS1);
	print_OSPY(SPYROADL1STATUS2);
	print_OSPY(SPYROADL1STATUS3);
	printf("\n");

	printf("ROAD SPY STATUS: LAMB2\n");
	print_OSPY(SPYROADL2STATUS0);
	print_OSPY(SPYROADL2STATUS1);
	print_OSPY(SPYROADL2STATUS2);
	print_OSPY(SPYROADL2STATUS3);
	printf("\n");

	printf("ROAD SPY STATUS: LAMB3\n");
	print_OSPY(SPYROADL3STATUS0);
	print_OSPY(SPYROADL3STATUS1);
	print_OSPY(SPYROADL3STATUS2);
	print_OSPY(SPYROADL3STATUS3);
	printf("\n");

      }


      // Write 0 to unfreeze
      if ( !keepFreeze ) {

	spys_freezed = !unfreezeSpyBuffers();
      }

      m_vme->disableBlockTransfers(DisableBlockTransfers_old_value);

      return true;

    }



  }   // namespace ftk
}   // namespace daq
