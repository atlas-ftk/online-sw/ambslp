/*
This script tests the AMchip input links in a few steps:
1)Enable PRBS generator 2)Input links are tested 3)Disable PRBS
4)Retest 5)Enable PRBS 6)Retest 7)Stop for 10 seconds 8)Retest 9)Stop for 10 minutes 10)Retest
If an error is found at any step the test stops and a detailed output is printed.
*/
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/ambslp_vme_regs.h"
#include "ambslp/AMBoard.h"
#include "ftkvme/VMEManager.h"
#include "ambslp/exceptions.h"
#include "ftkcommon/core.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <thread>         // std::this_thread::sleep_for 
#include <chrono>         // std::chrono::seconds 
int main(int argc, char **argv){
  using namespace  boost::program_options ;

  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("quick,q","If present the test will run without the 10 minutes break")
    ("verbose,v", "If present verbose option will be used")
    ;

  positional_options_description pd;
  pd.add("pattern_file", 1);


  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);


  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  printf("Testing the input links in a few steps:\n");
  printf("1)Enable PRBS generator 2)Input links are tested 3)Disable PRBS 4)Retest\n");
  printf("5)Enable PRBS generator 6)Retest 7)Stop for 10 seconds 8)Retest 9)Stop for 10 minutes 10)Retest\n");
  printf("If an error is found at any step the test stops and a detailed output is printed\n");

  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chainlength = MAXCHIP_PER_VMECOL;
  int chipnum = 3; //The chips to read. Default value is 3=0x3 (two chips).
  bool testPassed = true;
  int enable = 0x3;
  int disable = 0x0;
  int wordInRegister;

  // create the AMB object
  int int_verbose = vm.count("verbose")>0 ? 1 : 0;
  bool verbose(false);
  if (int_verbose==1) verbose=true;

  int int_quick = vm.count("quick")>0 ? 1 : 0;
  bool quick(false);
  if (int_quick==1) quick=true;

  daq::ftk::AMBoard myAMB(slot);
  if(verbose) myAMB.setVerbosity(daq::ftk::AMBoard::DEBUG);

  //Enable the PRBS generator on the HIT fpga (AMboard)
  myAMB.getVME()->write_word(ENABLE_PRBS_CHECKER_HIT, enable);
  wordInRegister = myAMB.getVME()->read_word(ENABLE_PRBS_CHECKER_HIT);//read what is written in the register
  if( (wordInRegister & 0x3) != enable){printf("\nWriting in VME failed!\n");}

  printf("\nEnabling the PRBS generator and testing the input links, takes around 20 seconds...\n");

  daq::ftk::ambslp_amchip_prbs_error_count_all_buses( slot, chainlength, chipnum, "disable", verbose);
  daq::ftk::ambslp_amchip_prbs_error_count_all_buses( slot, chainlength, chipnum, "enable", verbose);

  //fourth option is set to true when the PRBS generator is enabled and to false when it is disabled
  testPassed = daq::ftk::ambslp_amchip_polling_stat_reg_bus(slot, chainlength, chipnum, true, verbose);

  if(testPassed)
    {
      printf("\nFirst test passed! Now desabling the PRBS generator and retesting, takes around 20 seconds...\n");
      //Disable the PRBS generator on the HIT fpga (AMboard)
      myAMB.getVME()->write_word(ENABLE_PRBS_CHECKER_HIT, disable);
      if( (wordInRegister & 0x0) != disable){printf("\nWriting in VME failed!!!\n");}
  
      daq::ftk::ambslp_amchip_prbs_error_count_all_buses( slot, chainlength, chipnum, "disable", verbose);
      daq::ftk::ambslp_amchip_prbs_error_count_all_buses( slot, chainlength, chipnum, "enable", verbose );

      testPassed = daq::ftk::ambslp_amchip_polling_stat_reg_bus(slot, chainlength, chipnum, false, verbose);
    }

  if(testPassed)
    {
      printf("\nTest with PRBS generator disabled passed! Now enabling the PRBS generator and retesting, takes around 20 seconds...\n");
      //Enable the PRBS generator on the HIT fpga (AMboard)
      myAMB.getVME()->write_word(ENABLE_PRBS_CHECKER_HIT, enable);
      if( (wordInRegister & 0x3) != enable){printf("\nWriting in VME failed!!!\n");}

      daq::ftk::ambslp_amchip_prbs_error_count_all_buses( slot, chainlength, chipnum, "disable", verbose);
      daq::ftk::ambslp_amchip_prbs_error_count_all_buses( slot, chainlength, chipnum, "enable", verbose);

      testPassed = daq::ftk::ambslp_amchip_polling_stat_reg_bus(slot, chainlength, chipnum, true, verbose);
    }

  if(testPassed)
    {
      printf("\nSecond test passed! Stopping 10 seconds and then retesting...\n");
      std::this_thread::sleep_for (std::chrono::seconds(10));
      testPassed = daq::ftk::ambslp_amchip_polling_stat_reg_bus(slot, chainlength, chipnum, true, verbose);
    }

  if(testPassed && !quick)
    {
      printf("\nThird test passed! Stopping 10 minutes and then retesting...\n");
      std::this_thread::sleep_for (std::chrono::seconds(10*60));
      testPassed = daq::ftk::ambslp_amchip_polling_stat_reg_bus(slot, chainlength, chipnum, true, verbose);
    }

  if(testPassed)
    printf("\nGood! All test on the input links passed!!!\n");
  else
    printf("\nTest not passed, details are printed above!\n");
    
  return testPassed;

}

