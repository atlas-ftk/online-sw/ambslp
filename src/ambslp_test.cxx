/*****************************************************************/
/*                                                               */
/* Author: M.Franchini, P.Bryant                                 */
/*                                                               */
/*                                                               */
/* 14 April 2016: Class for aux-amb standalone testing           */
/*                                                               */
/*****************************************************************/

#include "ambslp/ambslp_test.h"

#include <ctime>

namespace daq {
  namespace ftk {

    ambslp_test::ambslp_test(uint _slot):
      m_slot(_slot)
    { 
      ERS_LOG("ambslp_test::constructor: Entered");

      //m_configuration = 0;
      m_LAMB_mask = 0;
      m_NPattPerChip = 2048; // set a meaningful number of chips to be written
      m_AMB.reset(new AMBoard(m_slot));

      ERS_LOG("ambslp_test::constructor: Done");
    }

    // /***********************/
    // ambslp_test::~ambslp_test() noexcept
    // /***********************/
    // {
    //   //ML delete all created StatusRegisterCollection objects
    //   delete m_statusRegisterAMBFactory;
    //   delete m_dfReaderAmb;

    //   delete m_hlast_lvl1id;
    //   delete m_hnroads;
    //   delete m_hgeoaddress;
    //   delete m_AMB;

    //   ERS_LOG("ambslp_test::destructor: Done");  
    // }

    /************************************************************/
    void ambslp_test::setup(const dal::ReadoutModule_Ambslp* module_dal)
    /************************************************************/
    {
      ERS_LOG("ambslp_test::setup: Entered");
  
      // Get online objects from daq::rc::OnlineServices
      // m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
      // std::string appName = daq::rc::OnlineServices::instance().applicationName();
      // auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
      // Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
      // // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
      // auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

      // // Read some info from the DB
      // const ftk::dal::ambslp_test * module_dal = conf->get<ftk::dal::ambslp_test>(configuration->getString("UID"));
      //      m_name = module_dal->UID();

      m_slot = module_dal->get_Slot();

      uint32_t tmp_crate = module_dal->get_Crate();
      std::stringstream tmp_name;
      tmp_name << "FTK-AMB-" << std::to_string(tmp_crate) << "-" << std::to_string(m_slot);
      m_name = tmp_name.str();

      m_ForceWrite = module_dal->get_ForceWrite(); // checking if the bank is force to be updated
      m_dumpFolder = module_dal->get_DumpFolder();
      m_mask = module_dal->get_LAMBMask();
      m_thr = module_dal->get_Threshold();
      m_initThr = module_dal->get_InitialThreshold();
      //m_useLaymap = module_dal->get_UseLaymap();
      //m_dualFace = module_dal->get_DualFace();
      m_checkBank = module_dal->get_CheckBank();
      m_bypassBus = module_dal->get_BypassBus();
      m_pcbVersion = module_dal->get_PCBversion();
      m_dvMajority = module_dal->get_DVmajority();
      //m_isServerName = readOutConfig->get_ISServerName(); 
      m_dryRun = module_dal->get_DryRun();


      if(m_dryRun)
	{ 
	  std::stringstream message;
	  message << "ambslp_test.DryRun option set in DB, IPBus calls with be skipped!";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::warning(ex);
	};
      
      ERS_LOG("ambslp_test::setup: Done");

    }

    unsigned int countBits(unsigned int data) {
      int count=0;
      for (unsigned int i=0; i<sizeof(unsigned int)*8; i++) 
	if ( (data>>i)&0x1 ) count++;
      return count;
    }


    /**
     * ambslp_test::reset_weak_config is a temporary function where to collect configuration code 
     *  that need to be called twice or more: during config and/or connect and/or prepare for run
     *  because they are not safe w.r.t. init signal
     */
    /*this function should not be needed anymore, the register is now safe against resets - just after reprogramming. Leave it in once in the beginning and remove it from anywhere else.*/
    /********************************/
    void ambslp_test::reset_weak_config()
    /********************************/
    {
      // Mask unused links from the LAMB on ROAD FPGA (force sending EE)
      unsigned int anti_FORCE_EE_LINKS_mask = 0;
      // define 16 bit mask of links that need to have EE forced 
      for (int ii=0; ii<MAXLAMB_PER_BOARD; ii++) {
	if ( ((m_LAMB_mask>>ii)&0x1) == 1 ){
	  anti_FORCE_EE_LINKS_mask |= 0xf << (ii*4); 
	}
      }

      unsigned int FORCE_EE_LINKS_mask = ~anti_FORCE_EE_LINKS_mask & 0xFFFF;
 
      m_AMB->getVME()->write_check_word(FORCE_EE_LINKS, FORCE_EE_LINKS_mask);

      // The following line is to force hold to the AUX to test if this is respected
      //m_AMB->write_check_word(DEBUG_FORCE_HOLD_HIT, 0xfff);      
    }


    /********************************/
    void ambslp_test::configure()
    /********************************/
    {  
      ERS_LOG("ambslp_test: configure()");
      // In dryRun mode bypass all the vme commands
      if(m_dryRun)
	{
	  ERS_LOG("ambslp_test: [dryRun] done");
	  return;
	}
      try
	{	 

	 m_AMB->RC_configure();

	  // prepare the pattern bank accord the flags
	 std::vector<uint32_t> out;
	 m_AMB->readPatternBank(m_banktype, m_patternBank, m_ssoffset,out);

	  if (!m_ForceWrite) {
	    // read the checksum of the existing bank and skip the following part
	    unsigned int bank_checksum = m_AMB->getVME()->read_word(AMB_VME_BANKCHECKSUM);

	    // if the checksums are equal the write part of the bank is skipped
	    if (bank_checksum == m_AMB->getPatternBank().getChecksum()) {
	      ERS_LOG("A bank with checksum " << hex << bank_checksum << dec << " is already loaded");
	      return;
	    }
	  }

	  // Prepare AMchips to load patterns 
	  u_int disable_mask = 0; // enable all blocks (including Top2 in AM05)
	  m_AMB->amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);
	  m_AMB->amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);

	  // Flush remnant pattens from the previous setting
	  m_AMB->amchip_init_evt(0x3);

	  //      daq::ftk::ambslp_amchip_disable_bank(m_slot, 2, 3, 3072, 0);
	  m_AMB->disable_bank(2, 3, m_AMB->getAMChip()->getMAXPATT_PERCHIP());

	  if (m_AMB->getAMChip()->getNAME() == AMChip05::global()->getNAME())
	    disable_mask = 0x2; // disable Top2 in AM05
	  m_AMB->amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);
	  m_AMB->amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);
	  m_AMB->amchip_init_evt(0x3);

	  // Write pattern banks
	  if( ! m_patternBank.empty() ) {
	    // take m_banktype and m_ssoffset from oks. m_banktype: 0=root file, 1=ascii, 
	    // set m_ssoffset to false for now when testing pattern matching with root bank
	    m_AMB->writepatterns(m_NPattPerChip, 0, false, false);
	  }

	  // Configure AMchips
	  m_AMB->amchip_init_evt(0x3);
	  m_AMB->amchip_jpatt_cfg(0x3, "a",  m_thr, 0, disable_mask, 0, 0, 1, 3, 0);
	  m_AMB->amchip_jpatt_cfg(0x3, "a",  m_thr, 0, disable_mask, 0, 0, 1, 3, 0);
	  m_AMB->amchip_init_evt(0x3);
	}
      catch( daq::ftk::VmeError & err )
	{
	  //throw daq::ftk::VmeError(ERS_HERE, "ambslp_test::configure: Error when initializing AmBoard at slot : " + m_slot );
	  throw daq::ftk::ftkException(ERS_HERE, name_ftk(), "ambslp_test::configure: Error when initializing AmBoard at slot : " + m_slot );
	}

      ERS_LOG("ambslp_test: configure() done");

    }

    /**************************/
    void ambslp_test::unconfigure()
    /**************************/
    {
      ERS_LOG("ambslp_test: unconfigure()");
      // Fill ME
      ERS_LOG("ambslp_test: unconfigure() done");
    }


    /**************************/
    void ambslp_test::connect()
    /**************************/
    {
      ERS_LOG("ambslp_test: connect()");

      // In dryRun mode bypass all the vme commands
      if(m_dryRun)
	{
	  ERS_LOG("ambslp_test: connect() [dryRun] done");
	  return;
	}

      try
	{	 

        m_AMB->RC_connect();
	}
      catch( daq::ftk::VmeError & err )
	{
	  //throw daq::ftk::VmeError(ERS_HERE, "ambslp_test::configure: Error when initializing AmBoard at slot : " + m_slot );
	  throw daq::ftk::ftkException(ERS_HERE, name_ftk(), "ambslp_test::configure: Error when initializing AmBoard at slot : " + m_slot );
	}

      //ML create all required StatusRegisterCollection objects here.

      ERS_LOG("ambslp_test: connect() done");
    }


    /**************************/
    void ambslp_test::disconnect()
    /**************************/
    {
      ERS_LOG("ambslp_test: disconnect()");
      // Fill ME
      ERS_LOG("ambslp_test: disconnect() done");
    }


    /**********************************/    
    void ambslp_test::prepareForRun()
    /**********************************/    
    {
      ERS_LOG("ambslp_test: prepareForRun()");

      // Reset histograms
      // m_hlast_lvl1id->Reset();
      // m_hnroads->Reset();
      // m_hgeoaddress->Reset();

      // In dryRun mode bypass all the vme commands
      if(m_dryRun)
	{
	  ERS_LOG("ambslp_test: prepareForRun() [dryRun] done");
	  return;
	}

      try
	{	 
	  // 1. Reset
	  // reset FIFOs
	  m_AMB->init(2,1);
	  m_AMB->init(2,1);
	  // reset FSMs
	  // Alberto: should not be done there
	  m_AMB->init(0,1);

	  // reset spy buffer
	  // daq::ftk::ambslp_reset_spy(m_slot, 1, 1);
	  m_AMB->resetSpyBuffers( 1, 1);
    
	  // 2. Enable communication from upstream
	  // enable sending HOLD upstream
	  //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_HIT, 0x0);

	  // start accepting data from upstream (stable init OFF)
	  //daq::ftk::ambslp_stable_init_off(m_slot, 1);
      
	  // 3. Enable communication to downstream
	  // comply with HOLD from downstream
	  //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0x0);
      
	  // allow data to be sent downstream
	  
	  // force the value of the error severity register that controls the freeze
	  //m_AMB->getVME()->write_check_word(AMB_CONTROL_ERROR_SEVERITY, 0x1);
	  //m_AMB->getVME()->write_check_word(AMB_CONTROL_FREEZE_RESET, 0x1); // force the freeze to be reset
	  //m_AMB->getVME()->write_check_word(AMB_CONTROL_FREEZE_RESET, 0x0); // allow to freeze at the next error
	}
      catch( daq::ftk::VmeError & err )
	{
	  //throw daq::ftk::VmeError(ERS_HERE, "ambslp_test::configure: Error when initializing AmBoard at slot : " + m_slot );
	  throw daq::ftk::ftkException(ERS_HERE, name_ftk(), "ambslp_test::configure: Error when initializing AmBoard at slot : " + m_slot );
	}

      ERS_LOG("ambslp_test: prepareForRun() done");
    }

    /**************************/
    void ambslp_test::stopDC()
    /**************************/
    {
      ERS_LOG("ambslp_test: stopDC()");
      // Fill ME

      // 1. Publish in IS/OH
      // last standard publication and, if needed, additional end of run informatoin

      // 2. Disable communication to downstream
      // prevent data fro being sent
      //ignore HOLD
      //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0xffff);

      // 3. disable communication from upstream
      // prevent HOLD to upstream
      //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_HIT, 0xfff);
      //ignore data

      // 4. Setup high speed links
      // so that all TX links are sending IDLE
      // Reset GTPs
      m_AMB->reset_gtp(19, false);
      // Setup SERDES of AMchips
      m_AMB->amchip8b10b();
      m_AMB->amchip8b10b();
      // Reset GTPs again
      m_AMB->reset_gtp(19, false);

      ERS_LOG("ambslp_test: stopDC() done");
    }


    // /**************************/
    // void ambslp_test::publish()
    // /**************************/
    // {
    //   ERS_LOG("Publishing ...");

    //   //++++++++++++++++++++++++++++++++++++++
    //   // 2  Access the registers and publish in IS and OH (to be moved back between 1.4 and 2.1)
    //   //++++++++++++++++++++++++++++++++++++++

    //   //ML call readout function for each StatusRegisterCollection, get values and set attributes of m_ambslpNamed; checkin m_ambslpNamed
    //   if(m_dryRun)
    // 	{
    // 	  // Left blank deliberately. Coded this way for consistency with the rest of the code.
    // 	} 
    //   else 
    // 	{
    // 	  m_statusRegisterAMBFactory->readout();
    // 	  m_dfReaderAmb->readIS();
    // 	  m_dfReaderAmb->publishAllHistos();
    // 	}

    //   ERS_LOG("Publishing Done...");

    // }

    // /**************************/
    // void ambslp_test::publishFullStats()
    // /**************************/
    // {
    //   ERS_LOG("Publishing Full Stats...");
    //   if (0){
    // 	//++++++++++++++++++++++++++++++++++++++
    // 	// 1  Manage spyBuffer
    // 	//++++++++++++++++++++++++++++++++++++++
  
    // 	std::vector< std::shared_ptr< AMBSpyBuffer > > spyBuffers;

    // 	//++++++++++++++++++++++
    // 	// 1.1 Read the Spy Buffer in binary format
    // 	//++++++++++++++++++++++

    // 	ERS_LOG("Read spy byffer");
    // 	ERS_LOG("Freezing spy buffer");
    // 	m_AMB->getVME()->write_check_word(FREEZE, 0x1);

    // 	readSpyBuffer(spyBuffers);

    // 	ERS_LOG("Restarting spy buffer");
    // 	m_AMB->getVME()->write_check_word(FREEZE, 0x0);
    
    // 	//++++++++++++++++++++++
    // 	// 1.2 Ship the spybuffer to EMonDataOut module
    // 	//++++++++++++++++++++++

    // 	//ERS_LOG("spyBuffers.size() = "<<spyBuffers.size());
    // 	if(!spyBuffers.size())
    // 	  {
    // 	    daq::ftk::FTKIssue e(ERS_HERE, "No spy buffer was read out on AMBSLP.");
    // 	    ers::warning(e);  //or throw e;  
    // 	  }
    // 	else
    // 	  shipSpyBuffer(spyBuffers);

    // 	//++++++++++++++++++++++
    // 	// 1.3 Decode the spybuffer to extract information to be published in IS 
    // 	//++++++++++++++++++++++

    // 	ERS_LOG("Decode spy buffer");
    // 	// AMB spy buffer
    // 	std::vector<std::vector<EventFragment*>> fragmentss; fragmentss.clear();

    // 	for(uint32_t isb = 0; isb < spyBuffers.size(); isb++){
    // 	  //ERS_LOG("(spyBuffers["<<isb<<"].getBuffer()).size() = "<<(spyBuffers[isb].getBuffer()).size());
    // 	  fragmentss.push_back(AMBRoad_splitFragments(spyBuffers[isb]->getBuffer()));
    // 	}
  
    // 	//ERS_LOG("fragmentss.size() = "<<fragmentss.size());
    // 	std::vector<EventFragmentCollection*> eventCollections = build_eventFragmentCollection(fragmentss);
    // 	//ERS_LOG("eventCollections.size() = "<<eventCollections.size());

    // 	//Monitoring the decoded result (temporal)
    // 	/*
    // 	  for (int i=0; i<(int)eventCollections.size(); i++){
    // 	  for(int ivecs=0; ivecs<spyBuffers.size(); ivecs++){
      
    // 	  EventFragmentAMBRoad* efar = (static_cast<EventFragmentAMBRoad*>((eventCollections.at(i))->getEventFragment(ivecs)));
    // 	  AMBRoadObjectEvent aroe = efar->getEventRoads();
      
    // 	  ERS_LOG("aroe.getNRoad() = "<<aroe.getNRoad());
    // 	  for(int ivec=0; ivec<(int)aroe.getNRoad(); ivec++) {
    // 	  AMBRoadObject aro = (aroe.getRoads()).at(ivec);
    // 	  aro.print();
    // 	  }
    // 	  }
    // 	  }
    // 	*/
    // 	ERS_LOG("Done ...");
    // 	//++++++++++++++++++++++
    // 	// 1.4 Delete the memory when you have done
    // 	//++++++++++++++++++++++
    // 	//delete[] spyBuff;

  
    // 	//++++++++++++++++++++++
    // 	// 2.1 Read registers and fill monitoring variable
    // 	//++++++++++++++++++++++

    // 	m_ambslpNamed->ExampleString = "Test";
    // 	m_ambslpNamed->ExampleU16    = 77;
    // 	m_ambslpNamed->ExampleS32    = rand()%100;
  
    // 	// Fill the histogram
    // 	int tmp_lvl1id=0;
    // 	for (int iev=0; iev<(int)eventCollections.size(); iev++)
    // 	  {
    // 	    for(int isb=0; isb<spyBuffers.size(); isb++)
    // 	      {  
    // 		EventFragmentAMBRoad* efar = (static_cast<EventFragmentAMBRoad*>((eventCollections.at(iev))->getEventFragment(isb)));
    // 		AMBRoadObjectEvent aroe = efar->getEventRoads();
    // 		if(aroe.getL1ID() > tmp_lvl1id) tmp_lvl1id=aroe.getL1ID();
    // 		//ERS_LOG("tmp_lvl1id = "<<tmp_lvl1id);
    // 		m_hnroads->Fill(aroe.getNRoad());

    // 		for(int ird=0; ird<(int)aroe.getNRoad(); ird++) {
    // 		  AMBRoadObject aro = (aroe.getRoads()).at(ird);
    // 		  //ERS_LOG("aro.getGeoAddress() = "<<aro.getGeoAddress());
    // 		  m_hgeoaddress->Fill(aro.getGeoAddress());	
    // 		}
    // 	      }
    // 	  }
    // 	m_hlast_lvl1id->Fill(tmp_lvl1id);
    // 	//++++++++++++++++++++++
    // 	// 2.2 Pushish in IS  with schema (i.e.: properly)
    // 	//++++++++++++++++++++++
    // 	try { m_ambslpNamed->checkin();}
    // 	catch ( daq::oh::Exception & ex)
    // 	  { // Raise a warning or throw an exception. 
    // 	    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    // 	    ers::warning(e);  //or throw e;  
    // 	  }
    // 	//++++++++++++++++++++++
    // 	// 3 Publish the histograms
    // 	//++++++++++++++++++++++

    // 	// Publishing Histogram
    // 	try { 
    // 	  m_ohProvider->publish( *m_hlast_lvl1id, m_hlast_lvl1id->GetName(), true ); 
    // 	  m_ohProvider->publish( *m_hnroads, m_hnroads->GetName(), true ); 
    // 	  m_ohProvider->publish( *m_hgeoaddress, m_hgeoaddress->GetName(), true ); 
    // 	}
    // 	catch ( daq::oh::Exception & ex)
    // 	  { // Raise a warning or throw an exception. 
    // 	    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    // 	    ers::warning(e);  //or throw e;  
    // 	  }
    //   }

    // }

    /**************************/
    void ambslp_test::resynch()
    /**************************/
    {
      ERS_LOG("ambslp_test: resynch()");
      // Fill ME
      ERS_LOG("ambslp_test: resynch() done");
    }
   

    // /**************************/
    // void ambslp_test::clearInfo() 
    // /**************************/
    // {
    //   ERS_LOG("ambslp_test: Clear histograms and counters");

    //   // Reset histograms
    //   m_hlast_lvl1id->Reset();
    //   m_hnroads->Reset();
    //   m_hgeoaddress->Reset();


    //   // Reset the IS counters
    //   m_ambslpNamed->ExampleS32 = 0 ;
    //   m_ambslpNamed->ExampleU16 = 0 ;
    // }


    // //FOR THE PLUGIN FACTORY
    // extern "C"
    // {
    //   extern ROS::ReadoutModule* createambslp_test();
    // }

    // ROS::ReadoutModule* createambslp_test()
    // {
    //   return (new ambslp_test());
    // }


    /**************************/
    ////void  ambslp_test::dumpSpybufferOnFile(Json::Value &info)
    /**************************/
    /***{
     // Opening the spybuffer output file
     std::string username("unknown");
     struct passwd * pw = ::getpwuid(geteuid());
     if( pw ) { username = pw->pw_name; }

     std::string bufFile = m_dumpFolder + "/spybuffer." + username + ".dat";
     std::ofstream spyFile (  bufFile.c_str() , std::ios::trunc ) ;

     // Checking if file was correctly opened
     if ( spyFile.fail() )
     {
     throw daq::ftk::IOError( ERS_HERE,"Impossible to open spybuffer output file " +  m_dumpFolder + " for Amboard " + m_name );
     }

     try
     {
     // Here decompacting the infos
     if ( info.isMember("SpyBuffer") )
     {
     // Printing every line returned
     Json::Value lines = info["SpyBuffer"];
     for ( unsigned int i = 0; i < lines.size(); i++ )
     {
     spyFile << lines[i].asString() << std::endl;
     }
     }
     else
     {
     // Flushing and closing
     spyFile.flush();
     spyFile.close();

     throw daq::ftk::VmeError( ERS_HERE,
     "Invalid spyBuffer from AmBoard at slot " + m_slot  ) ;
     }

     // Flushing and closing
     spyFile.flush();
     spyFile.close();
     }
     catch( std::ios_base::failure & error )
     {
     throw daq::ftk::IOError( ERS_HERE,
     "ambslp_test::configure: VME Error when writing spybuffer to file from AmBoard at slot : " +  m_slot );
     }
     }***/

    // /**********************************/    
    // void ambslp_test::readSpyBuffer(std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > >& spyBuffers)
    // /**********************************/    
    // {
    //   int spymask = 0x0;
    //   for (int ii=0; ii<MAXLAMB_PER_BOARD; ii++) 
    // 	if (m_LAMB_mask & (1<<ii) ) spymask |= 0xF << (4*ii);

    //   ERS_LOG("PLEASE FIX ME ambslp_test::readSpyBuffer");

    //   std::shared_ptr< daq::ftk::AMBSpyBuffer > sb[16];
    //   if (spymask&(1<<0)) sb[0] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL0STATUS0, OSPY_L0_LINK0, m_AMB->getVME());
    //   if (spymask&(1<<1)) sb[1] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL0STATUS1, OSPY_L0_LINK1, m_AMB->getVME());
    //   if (spymask&(1<<2)) sb[2] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL0STATUS2, OSPY_L0_LINK2, m_AMB->getVME());
    //   if (spymask&(1<<3)) sb[3] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL0STATUS3, OSPY_L0_LINK3, m_AMB->getVME());
    //   if (spymask&(1<<4)) sb[4] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL1STATUS0, OSPY_L1_LINK0, m_AMB->getVME());
    //   if (spymask&(1<<5)) sb[5] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL1STATUS1, OSPY_L1_LINK1, m_AMB->getVME());
    //   if (spymask&(1<<6)) sb[6] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL1STATUS2, OSPY_L1_LINK2, m_AMB->getVME());
    //   if (spymask&(1<<7)) sb[7] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL1STATUS3, OSPY_L1_LINK3, m_AMB->getVME());
    //   if (spymask&(1<<8)) sb[8] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL2STATUS0, OSPY_L2_LINK0, m_AMB->getVME());
    //   if (spymask&(1<<9)) sb[9] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL2STATUS1, OSPY_L2_LINK1, m_AMB->getVME());
    //   if (spymask&(1<<10)) sb[10] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL2STATUS2, OSPY_L2_LINK2, m_AMB->getVME());
    //   if (spymask&(1<<11)) sb[11] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL2STATUS3, OSPY_L2_LINK3, m_AMB->getVME());
    //   if (spymask&(1<<12)) sb[12] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL3STATUS0, OSPY_L3_LINK0, m_AMB->getVME());
    //   if (spymask&(1<<13)) sb[13] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL3STATUS1, OSPY_L3_LINK1, m_AMB->getVME());
    //   if (spymask&(1<<14)) sb[14] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL3STATUS2, OSPY_L3_LINK2, m_AMB->getVME());
    //   if (spymask&(1<<15)) sb[15] = std::make_shared< daq::ftk::AMBSpyBuffer >(SPYROADL3STATUS3, OSPY_L3_LINK3, m_AMB->getVME());

    //   //Reading ROAD Spy Buffer
    //   for(int ispy=0; ispy<16; ispy++)
    // 	{
    // 	  // skip masked channels
    // 	  if ((spymask&(1<<ispy))==0) continue;
    // 	  sb[ispy]->read_spy_buffer();
    // 	  spyBuffers.push_back(sb[ispy]);
    // 	}
 
    // }


    // /**********************************/    
    // void ambslp_test::shipSpyBuffer(std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > > & vecSpyBuffers)
    // /**********************************/    
    // {
    //   // Protection for m_ftkemonDataOut pointer
    //   if(!m_ftkemonDataOut)
    // 	return;

    //   //total number of words contained in the SpyBuffers
    //   uint32_t num_words = 0;

    //   //vector of the pairs to be shipped
    //   std::vector< std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > > vec_pairs;

    //   for(uint32_t isb = 0; isb < vecSpyBuffers.size(); isb++)
    // 	{
    // 	  //creating the source ID for the SpyBuffer
    // 	  uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::AMB, 1, isb, daq::ftk::Position::IN);

    // 	  num_words += vecSpyBuffers[isb]->getBuffer().size();

    // 	  //creating the pair
    // 	  std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > pair2ship = std::make_pair( vecSpyBuffers[isb], sourceid);

    // 	  //ERS_LOG("Push back spybuffer lane = " << isb);
    // 	  vec_pairs.push_back(pair2ship);
    // 	}

    //   // Make SpyBuffers available to FtkEMonDataOut
    //   ERS_LOG("Make SpyBuffers available to FtkEMonDataOut ...");
    //   m_ftkemonDataOut->sendData(vec_pairs);
    //   ERS_LOG("SpyBuffers total number of words: " << num_words << " were made available to FtkEMonDataOut" );

    // }

    /**************************/
    uint32_t ambslp_test::readSpyBufferExample(uint32_t* &data)
    /**************************/
    {
      uint32_t payloadWords = 20;
      data  = new uint32_t[payloadWords];
      for(uint32_t i=0; i< payloadWords; i++) 
	data[i] = 0xBEEF0000+i;
      return payloadWords;
    }
  }
}
