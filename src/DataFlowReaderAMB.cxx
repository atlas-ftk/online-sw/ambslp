#include "ambslp/DataFlowReaderAMB.h"
#include "ftkcommon/ReadSRFromISVectors.h"

//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>


// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"



using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderAMB::DataFlowReaderAMB(std::shared_ptr<OHRawProvider<>> ohRawProvider, bool forceIS) 
   : DataFlowReader(ohRawProvider, forceIS)
/************************************************************/
{
}

/***************************************************************************************************************/
void DataFlowReaderAMB::init(const string& deviceName, const string& partitionName, const string& isServerName)
/***************************************************************************************************************/
{
  DataFlowReader::init(deviceName, partitionName, isServerName);
  DataFlowReader::name_dataflow();
  m_theAMBSummary = std::make_shared<FtkDataFlowSummaryAMBNamed>(m_ipcp,name_dataflow());
  m_theSummary= std::static_pointer_cast<FtkDataFlowSummaryNamed>(m_theAMBSummary);
  
  ERS_LOG("Initializing AMB dataflowsummary: "<<name_dataflow()<<" :pointer");
  
}
/************************************************************/
vector<string> DataFlowReaderAMB::getISVectorNames()
/************************************************************/
{
  vector<string> srv_names;

  srv_names.push_back("VME_Chip_sr_v");
  srv_names.push_back("Hit_FPGA_sr_v");
  srv_names.push_back("Road_FPGA_sr_v");
  srv_names.push_back("Control_VME_sr_v");

  return srv_names;
}

/************************************************************/
void DataFlowReaderAMB::getFifoInBusyFraction(vector<float>& srv, uint32_t option)
/************************************************************/
{
  const float counter_1second = 1E8;
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH4))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH5))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH6))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH7))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH8))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH9))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH10))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH11))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH0))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH1))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH2))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_HOLD_CNT_1SEC_CH3))/counter_1second);

}

/************************************************************/
void DataFlowReaderAMB::getFifoInEmptyFraction(vector<float>& srv, uint32_t option)
/************************************************************/
{
  const float counter_1second = 1E8;
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH4))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH5))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH6))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH7))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH8))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH9))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH10))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH11))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH0))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH1))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH2))/counter_1second);
  srv.push_back((readRegister(AMB_HIT_EMPTY_CNT_1SEC_CH3))/counter_1second);

}

/************************************************************/
void DataFlowReaderAMB::getFifoInBusy(vector<int64_t>& srv)
/************************************************************/
{
  // HOLD from HIT input channels
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>4) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>5) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>6) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>7) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>8) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>9) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>10) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>11) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>1) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>2) & 0x1);
  srv.push_back((readRegister(HOLD_FLAG_HIT)>>3) & 0x1);

}

/************************************************************/
void DataFlowReaderAMB::getFifoInEmpty(vector<int64_t>& srv)
/************************************************************/
{
  // Empty from HIT input channels
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>4) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>5) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>6) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>7) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>8) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>9) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>10) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>11) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>1) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>2) & 0x1);
  srv.push_back((readRegister(EMPTY_FLAG_HIT)>>3) & 0x1);

}


/************************************************************/
void DataFlowReaderAMB::getFifoOutBusy(vector<int64_t>& srv)
/************************************************************/
{
  // HOLD from ROAD input channels
  for(int i=0; i<NCHANNELS_ROAD_INPUT; i++){
    srv.push_back((readRegister(PROGFULL_EMPTY_INPUTFIFO)>>(i+16)) & 0x1);
  }

}

/************************************************************/
void DataFlowReaderAMB::getFifoOutEmpty(vector<int64_t>& srv)
/************************************************************/
{
  // Empty from ROAD input channels
  for(int i=0; i<NCHANNELS_ROAD_INPUT; i++){
    srv.push_back((readRegister(PROGFULL_EMPTY_INPUTFIFO)>>i) & 0x1);
  }
}

/************************************************************/
//void DataFlowReaderAMB::getEventRate(vector<int64_t>& srv)
/************************************************************/
//{
//}

/************************************************************/                                                                                            
void DataFlowReaderAMB::getL1id(vector<int64_t>& srv)                                                                                                         
/************************************************************/                                                                                            
{                                                                                                                                                         
  srv.push_back(readRegister(AMB_LAST_SEEN_L1ID));                                                                                                        
}

/************************************************************/
void DataFlowReaderAMB::getLinkInStatus(vector<int64_t>& srv)
/************************************************************/
{

  //Take AND of ResetDone and Align of Rx of HIT FPGA
  srv.push_back( ((readRegister(HITRESETDONEGTP)>> 5) & 0x1) && ((readRegister(HITALIGNGTP)>>13) & 0x1)); //PIXEL-0
  srv.push_back( ((readRegister(HITRESETDONEGTP)>> 4) & 0x1) && ((readRegister(HITALIGNGTP)>>12) & 0x1)); //PIXEL-1
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>12) & 0x1) && ((readRegister(HITALIGNGTP)>> 8) & 0x1)); //PIXEL-2
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>30) & 0x1) && ((readRegister(HITALIGNGTP)>> 2) & 0x1)); //PIXEL-3
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>15) & 0x1) && ((readRegister(HITALIGNGTP)>>11) & 0x1)); //PIXEL-4
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>14) & 0x1) && ((readRegister(HITALIGNGTP)>>10) & 0x1)); //PIXEL-5
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>29) & 0x1) && ((readRegister(HITALIGNGTP)>> 1) & 0x1)); //PIXEL-6
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>28) & 0x1) && ((readRegister(HITALIGNGTP)) & 0x1)); //PIXEL-7
  srv.push_back( ((readRegister(HITRESETDONEGTP)>> 7) & 0x1) && ((readRegister(HITALIGNGTP)>>15) & 0x1)); //SCT-0
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>21) & 0x1) && ((readRegister(HITALIGNGTP)>>5) & 0x1)); //SCT-1
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>23) & 0x1) && ((readRegister(HITALIGNGTP)>>7) & 0x1)); //SCT-2
  srv.push_back( ((readRegister(HITRESETDONEGTP)>>22) & 0x1) && ((readRegister(HITALIGNGTP)>>6) & 0x1)); //SCT-3

}

/************************************************************/
void DataFlowReaderAMB::getLinkOutStatus(vector<int64_t>& srv)
/************************************************************/
{

  //ResetDone of Tx of HIT FPGA
  srv.push_back((readRegister(ROADRESETDONEGTP)>>10) & 0x1); //LAMB0_0
  srv.push_back((readRegister(ROADRESETDONEGTP)>>25) & 0x1); //LAMB0_1
  srv.push_back((readRegister(ROADRESETDONEGTP)>> 9) & 0x1); //LAMB0_2
  srv.push_back((readRegister(ROADRESETDONEGTP)>>11) & 0x1); //LAMB0_3
  srv.push_back((readRegister(ROADRESETDONEGTP)>>24) & 0x1); //LAMB1_0
  srv.push_back((readRegister(ROADRESETDONEGTP)>> 8) & 0x1); //LAMB1_1
  srv.push_back((readRegister(ROADRESETDONEGTP)>>26) & 0x1); //LAMB1_2
  srv.push_back((readRegister(ROADRESETDONEGTP)>>27) & 0x1); //LAMB1_3
  srv.push_back((readRegister(ROADRESETDONEGTP)>> 1) & 0x1); //LAMB2_0
  srv.push_back((readRegister(ROADRESETDONEGTP)>> 2) & 0x1); //LAMB2_1
  srv.push_back((readRegister(ROADRESETDONEGTP)>> 0) & 0x1); //LAMB2_2
  srv.push_back((readRegister(ROADRESETDONEGTP)>> 3) & 0x1); //LAMB2_3
  srv.push_back((readRegister(ROADRESETDONEGTP)>>16) & 0x1); //LAMB3_0
  srv.push_back((readRegister(ROADRESETDONEGTP)>>17) & 0x1); //LAMB3_1
  srv.push_back((readRegister(ROADRESETDONEGTP)>>18) & 0x1); //LAMB3_2
  srv.push_back((readRegister(ROADRESETDONEGTP)>>19) & 0x1); //LAMB3_3

}

/************************************************************/
void DataFlowReaderAMB::getFpgaTemperature(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_FRONT)) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_REAR)) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_FRONT)>>8) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_REAR)>>8) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_FRONT)>>16) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_REAR)>>16) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_FRONT)>>24) & 0xFF);
  srv.push_back((readRegister(AMB_VME_LAMB_TEMP_REAR)>>24) & 0xFF);

}

/************************************************************/
void DataFlowReaderAMB::getNEventsProcessed(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(AMBSLP_HIT_EVENT_COUNTER));                                                                                                  
}

/************************************************************/
void DataFlowReaderAMB::getAmbPllLockIn(vector<int64_t>& srv)
/************************************************************/
{

  int flag;

   for(int i=HIT_STATUS_REG_START_PLLLOCK_FAILURE; i<HIT_STATUS_REG_START_PLLLOCK_FAILURE+4; i++){
    flag = (readRegister(HIT_STATUS_REGISTER)>>i) & 0x1;
    srv.push_back(flag);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbPllRefLockIn(vector<int64_t>& srv)
/************************************************************/
{

  int flag;

  for(int i=HIT_STATUS_REG_START_PLLREFLOCK_FAILURE; i<HIT_STATUS_REG_START_PLLREFLOCK_FAILURE+4; i++){
    flag = (readRegister(HIT_STATUS_REGISTER)>>i) & 0x1;
    srv.push_back(flag);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbPllLockOut(vector<int64_t>& srv)
/************************************************************/
{

  int flag;

  for(int i=ROAD_STATUS_REG_START_PLLLOCK_FAILURE; i<ROAD_STATUS_REG_START_PLLLOCK_FAILURE+4; i++){
    flag = (readRegister(ROAD_STATUS_REGISTER)>>i) & 0x1;
    srv.push_back(flag);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbPllRefLockOut(vector<int64_t>& srv)
/************************************************************/
{

  int flag;

  for(int i=ROAD_STATUS_REG_START_PLLREFLOCK_FAILURE; i<ROAD_STATUS_REG_START_PLLREFLOCK_FAILURE+4; i++){
    flag = (readRegister(ROAD_STATUS_REGISTER)>>i) & 0x1;
    srv.push_back(flag);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbFsmStateIn(vector<int64_t>& srv)
/************************************************************/
{

  int flag;

  for(int i=0; i<NSCTBLOCKS_HIT; i++){
    flag = (readRegister(AMB_HIT_SCT_FSM)>>(i*4)) & 0xf;
    srv.push_back(flag);
  }

  for(int i=0; i<NPIXBLOCKS_HIT; i++){
    flag = (readRegister(AMB_HIT_PIX_FSM)>>(i*4)) & 0xf;
    srv.push_back(flag);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbLostSyncCntIn(vector<int64_t>& srv)
/************************************************************/
{

  int count;
  
  for(int i=0; i<NSCTBLOCKS_HIT; i++){
    count = (readRegister(LOSS_OF_SYNC0)>>(i*8)) & 0xff;
    srv.push_back(count);
  }

  for(int i=0; i<NPIXBLOCKS_HIT; i++){
    count = (readRegister(LOSS_OF_SYNC1)>>(i*8)) & 0xff;
    srv.push_back(count);
  }

  for(int i=0; i<NPIXBLOCKS_HIT; i++){
    count = (readRegister(LOSS_OF_SYNC2)>>(i*8)) & 0xff;
    srv.push_back(count);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbAuxHoldOut(vector<int64_t>& srv)
/************************************************************/
{

  int flag;
  
  for(int i=0; i<16; i++){
    flag = (readRegister(HOLD_AUX_STATUS)>>i) & 0x1;
    srv.push_back(flag);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbMissingPatternXOut(vector<int64_t>& srv)
/************************************************************/
{

  int count;

  for (int i=0; i < MAXOUTLINK_PERLAMB*MAXLAMB_PER_BOARD; i++) {
    count = (readRegister(AMB_ROAD_PATTERNX_LINK0+4*i));
    for (int j=0; j<4; j++) srv.push_back((count>>(8*j))&0xff);
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbRx8b10bDisparityOut(vector<int64_t>& srv)
/************************************************************/
{

  for(int i=0; i<4; i++){
    srv.push_back( ((readRegister(AMB_ROAD_8B10B_ERRORS_LAMB0)>>(i*4)) & 0xf) );
  }
  for(int i=0; i<4; i++){
    srv.push_back( ((readRegister(AMB_ROAD_8B10B_ERRORS_LAMB1)>>(i*4)) & 0xf) );
  }
  for(int i=0; i<4; i++){
    srv.push_back( ((readRegister(AMB_ROAD_8B10B_ERRORS_LAMB2)>>(i*4)) & 0xf) );
  }
  for(int i=0; i<4; i++){
    srv.push_back( ((readRegister(AMB_ROAD_8B10B_ERRORS_LAMB3)>>(i*4)) & 0xf) );
  }

}

/************************************************************/
void DataFlowReaderAMB::getAmbFreezeStatus(vector<int64_t>& srv)
/************************************************************/
{

  srv.push_back( ((readRegister(CONTROL_STATUS_REGISTER)>>CONTROL_ROAD_FREEZE_BIT) & 0x1) );
  srv.push_back( ((readRegister(CONTROL_STATUS_REGISTER)>>CONTROL_HIT_FREEZE_BIT) & 0x1) );
  srv.push_back( ((readRegister(CONTROL_STATUS_REGISTER)>>CONTROL_AUX_FREEZE_BIT) & 0x1) );
  srv.push_back( (readRegister(FREEZE) & 0x1) );

}

/************************************************************/
void DataFlowReaderAMB::publishExtraHistos(uint32_t option)
/************************************************************/
{
  std::vector<int64_t> srv;

  srv.clear();
  getAmbPllLockIn(srv); 
  publishSingleTH1F(srv,"AmbPllLockIn");
  m_theAMBSummary->AmbPllLockIn = srv;

  srv.clear();
  getAmbPllRefLockIn(srv); 
  publishSingleTH1F(srv,"AmbPllRefLockIn");
  m_theAMBSummary->AmbPllRefLockIn = srv;

  srv.clear();
  getAmbPllLockOut(srv); 
  publishSingleTH1F(srv,"AmbPllLockOut");
  m_theAMBSummary->AmbPllLockOut = srv;

  srv.clear();
  getAmbPllRefLockOut(srv); 
  publishSingleTH1F(srv,"AmbPllRefLockOut");
  m_theAMBSummary->AmbPllRefLockOut = srv;

  srv.clear();
  getAmbFsmStateIn(srv); 
  publishSingleTH1F(srv,"AmbFsmStateIn");
  m_theAMBSummary->AmbFsmStateIn = srv;

  srv.clear();
  getAmbLostSyncCntIn(srv); 
  publishSingleTH1F(srv,"AmbLostSyncCntIn");
  m_theAMBSummary->AmbLostSyncCntIn = srv;

  srv.clear();
  getAmbAuxHoldOut(srv); 
  publishSingleTH1F(srv,"AmbAuxHoldOut");
  m_theAMBSummary->AmbAuxHoldOut = srv;

  srv.clear();
  getAmbMissingPatternXOut(srv); 
  publishSingleTH1F(srv,"AmbMissingPatternXOut");
  m_theAMBSummary->AmbMissingPatternXOut = srv;

  srv.clear();
  getAmbRx8b10bDisparityOut(srv); 
  publishSingleTH1F(srv,"AmbRx8b10bDisparityOut");
  m_theAMBSummary->AmbRx8b10bDisparityOut = srv;

  srv.clear();
  getAmbFreezeStatus(srv); 
  publishSingleTH1F(srv,"AmbFreezeStatus");
  m_theAMBSummary->AmbFreezeStatus = srv;

//  std::vector<float> srvf;
//  srvf.clear();
//  getBusyFraction(srvf); 
//  publishSingleTH1F(srvf,"BusyFraction");

}
