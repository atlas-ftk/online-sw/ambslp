

#include "ambslp/ambslp_standalone.h"
#include "ambslp/monitoring_functions.h"

#include "ftkvme/VMEInterface.h"
#include "ambslp/exceptions.h"

#include <iomanip>
#include <iostream>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
using namespace boost::program_options;

// print the temperature with some format
void printLambTemperature( daq::ftk::LambTemperature * lambTemp, int slot );


int main(int argc, char **argv) {

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
  ("help", "produce help message")
  ("slot", value<unsigned int>()->default_value(15), "The card slot. Default value is 15.")
  ;

  variables_map vm;
  try
  {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
  {
    std::cerr << desc << std::endl;
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
  {
    cout << endl <<  desc << endl ;
    return 0;
  }

  int slot  = vm["slot"].as<unsigned int>();


  // try to connect to the amb via vme
  VMEInterface *vme = 0;                                              
                                                                          
  try {                                                               
    vme = VMEManager::global().amb(slot);                             
  }                                                                   
  catch (daq::ftk::VmeError &ex) {                                          
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue;
  } 

  // create temperature monitor and read lamb temperature from vme
  std::unique_ptr<daq::ftk::TemperatureMonitor> tempMonitor ( new daq::ftk::LambTemperatureMonitor());
  daq::ftk::LambTemperature *lambTemp = 0;
  lambTemp = (daq::ftk::LambTemperature *)tempMonitor->readTemperature( vme );


  printLambTemperature( lambTemp, slot );


    //  if here, everything went right
  return 0;


}



void printLambTemperature( daq::ftk::LambTemperature * lambTemp, int slot ) {

  // header
  std::cout << std::setfill(' ');
  std::cout << "\n\t temperatures for slot " << slot << ":\n";

  // upper part of the table
  std::cout << "\n" << std::setw(12);
  std::cout << "|" << std::setw(11) << "LEFT |" << std::setw(12) << "RIGHT |\n";

  std::cout << "   " << std::setfill('-') << std::setw(32)  << "\n";
  std::cout << std::setfill(' ');

  // do not like this at all!
  //   but that's it.
  for ( int i=0; i<4; i++ ) {

    int left = lambTemp[i].leftSensor;
    int right = lambTemp[i].rightSensor;
    std::cout << std::setw(9) << "lamb" << std::setw(1) << i << " |" //
      << std::setw(9) << left << std::setw(0) << " |" //
      << std::setw(9) << right << std::setw(1) << " |\n";
  }

  // bottom of the table
  std::cout << "   " << std::setfill('-') << std::setw(32)  << "\n";
  std::cout << std::setfill(' ');

  std::cout << "\n\n";
}
