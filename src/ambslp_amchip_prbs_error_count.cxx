/* ambslp_amchip_prbsgen_inout.cxx

*/


// as of July, 21st 2015,
// the following standalone programs have been removed:
//
//   ambslp_amchip_disable_prbs_error_count_pattin1_main
//   ambslp_amchip_disable_prbs_error_count_pattin_pattout_main
//   ambslp_amchip_enable_prbs_error_count_pattin1_main
//   ambslp_amchip_enable_prbs_error_count_pattin_pattout_main
//   
// and substituted with the current main.
// to mimic the old behaviour of the functions above:
//
//    ambslp_amchip_disable_prbs_error_count_pattin1_main  SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_prbs_error_count_main  SLOT CHAINLENGTH CHIPNUM COLUMNS disable pattin1
//
//    ambslp_amchip_disable_prbs_error_count_pattin_pattout_main  SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_prbs_error_count_main  SLOT CHAINLENGTH CHIPNUM COLUMNS disable patt_duplex
//
//    ambslp_amchip_enable_prbs_error_count_pattin1_main  SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_prbs_error_count_main  SLOT CHAINLENGTH CHIPNUM COLUMNS enable pattin1
//
//    ambslp_amchip_enable_prbs_error_count_pattin_pattout_main  SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_prbs_error_count_main  SLOT CHAINLENGTH CHIPNUM COLUMNS enable patt_duplex
//
//-----------------------------------------------------------------------------


#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) 
{ 
	
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("chainlength", value< std::string >()->default_value("2"), "The chain length. Default value is 2.")
    ("chipnum", value< std::string > ()->default_value("3"), "The chips to read. Default value is 3=0x3.")
    ("columns", value< std::string > ()->default_value("a"), "The vme columns that must be addressed. Default value is 'a'=all. Valid values are 'a' all, 'e' even, 'o' odd.")
    ("policy", value<std::string>()->default_value("enable"), "Choose whether to enebale or not. Default value is 'enable'. Valid values are 'enable', 'disable', 'pattern'")
    ("serdes", value<std::string>()->default_value("pattin1"), "The serdes value. Default is 'pattin1'. Valid values are 'patt_duplex', 'pattin1'")

      ;
  positional_options_description pd; 
  pd.add("pattern_file", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chainlength = daq::ftk::string_to_int( vm["chainlength"].as<std::string>() );
  int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );
  const char* columns = (vm["columns"].as<std::string>()).c_str();
  if(not(strcmp(columns, "a")==0) and not(strcmp(columns, "e")==0) and not(strcmp(columns, "e")==0) ){
    std::cerr<<"warning: function called with invalid option columns: '"<<columns<<"'. Using default value 'a'!"<<std::endl;
    columns="a";
  }
  const char* policy = ( vm["policy"].as<std::string>() ).c_str();
  if ( !(strcmp(policy, "enable")) and !(strcmp(policy, "disable")) and !(strcmp(policy, "pattern")) ) {
    std::cerr << "warning: function called with invalid options policy: '" << policy << "'. Using default value 'enable'!" << std::endl;
    policy = "enable";
  }
  const char* serdes = ( vm["serdes"].as<std::string>() ).c_str();
  if ( !(strcmp(serdes, "patt_duplex")) and !(strcmp(serdes, "pattin1")) ) {
    std::cerr << "warning: function called with invalid options serdes: '" << serdes << "'. Using default value 'pattin1'!" << std::endl;
    serdes = "pattin1";
  }
  

  return daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, columns, policy, serdes, true);
}
