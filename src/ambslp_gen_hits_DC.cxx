#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"
#include "ftkcommon/patternbank_lib.h"
#include "ambslp/AMChip.h"

#include <fstream>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

using namespace std;

namespace daq {
  namespace ftk {

  bool AMBOutputFormat(false);

  /** \brief The function controls the procedure that generate random patterns.
   *
   * \param nevents Number of events to be generated in one file
   * \param nroads  Number of full roads that will generate one set of hits
   * \param n7roads Number of N-1 roads that generate one set of hits
   * \param n6roads Number of N-2 roads that generate one set of hits
   * \param nnoise Number of random hits
   * \param seed Seed to be used by the random generator used to extract the roads
   * \param randomseed If true a random seed is used: the seed is generated using th current time
   * \param DCmax Number of DC bits used during bank generation
   * \param DCprob Integer between 0 and 10 that defines the probability of getting a 0 in a DC position
   * \param pattfname Path to the pattern file
   * \param type Pattern file format as used by PattBank_lib::loadPatternBank
   * \param SSoffset If true add 8 to patterns in order to avoid 0x0000 value in pattern
   * \param addrfname Path to address file name
   * \param hitfname Prefix for output file names
   * \param use_known IF true use only superstrips that match patters in addrfname file
   * \param verbose If true sets verbose mode
   *
   *
   */
  int ambslp_gen_hits_DC_new(int nevents, int nroads, int n7roads, int n6roads, int nnoise, int seed, bool randomseed, int DCmax, double DCprob,
  		const  char * pattfname, int type,  bool SSOffset, const char* addrfname, const  char * hitfname, bool use_known,
  		int pattmin, int pattmax, uint max_hits_per_event, unsigned int AMChipV, bool verbose ){
  	AMChip *m_AMChip = AMChip05::global();
  	if (AMChipV==6) m_AMChip = AMChip06::global();

  	// get current time
  	struct timeval tnow;
  	gettimeofday(&tnow, NULL);

  	// check if the user asked for a specific seed (!=0) otherwise ueses the timeval.tv_usec structure
  	if (randomseed)
  		seed = tnow.tv_usec;
  	PRINT_INFO("Using seed = " << seed);
  	srand(seed);

  	int nroads_int;

  	/* increment those numbers to generate random between 0 and command line paramater */
  	nroads+=1;
  	n7roads+=1;
  	n6roads+=1;
  	nnoise+=1;

  	/* read patterns from file */
  	int totpatts(MAXCOLUMNS*MAXCHIP_PER_VMECOL*m_AMChip->getMAXPATT_PERCHIP());
  	FTKPatternBank PattBank;
  	PattBank.setNLayers (NLAYERS);
  	// set an offset for the SS to avoid SS 0
  	for (int il=0;il!=NLAYERS&&SSOffset;++il) PattBank.setSSOffset(il,8);
  	int res = PattBank.loadPatternBank(pattfname, type,totpatts);
  	/*TODO by now it expects pattfname to be an ASCII file: set mode properly*/
  	switch(res){
  	case 0:
  		PRINT_INFO("Pattern bank correctly loadaed in memory.");
  		break;
  	case -1:
  		FTK_STRING_PARAM_ERROR("ambslp_amchip_writepatterns(): Error while loading bank in memory.");
  		return false;
  	case -2:
  		FTK_STRING_PARAM_ERROR("ambslp_amchip_writepatterns(): Error while loading bank in memory: the number of requested patterns cannot be loaded");
  		return false;
  	case -3:
  		FTK_STRING_PARAM_ERROR("ambslp_amchip_writepatterns(): Error while loading bank in memory: the format cannot be read");
  		return false;
  	default:
  		FTK_STRING_PARAM_ERROR("ambslp_amchip_writepatterns(): Warning while loading bank in memory: unknow error code");
  		return -1;
  	}
  	PattBank.printConfiguration();


  	/*if use_known load a list of knonw pattern addresses*/
  	std::vector<int> av_addr;
  	if(use_known){
  		int  read;
  		av_addr.reserve(totpatts);
  		ifstream addrf;
  		addrf.open(addrfname, std::ios_base::in);
  		if (not addrf.is_open()) {
  			FTK_STRING_PARAM_ERROR("ERROR: ambslp_gen_hits_DC_new(): cannot open input addresses file: "<< addrfname);
  			return -1;
  		}else if(verbose)PRINT_INFO("INFO: ambslp_gen_hits_DC_new(): open input addresses file: "<< addrfname);
  		while(addrf.good()){
  			addrf >>std::hex>> read;
  			if(verbose) PRINT_INFO("INFO: ambslp_gen_hits_DC_new(): read address: "<< read);
  			//FIXME the next line probably made sense for AMChip05 but as 03/03/2017 is broken and skipped
  			//int addr = (((read>>18)&0x3)*m_AMChip->getMAXPATT_PERCHIP()) + (((read>>20)&0x3)*MAXOUTLINK_PERLAMB*m_AMChip->getMAXPATT_PERCHIP()) + (((read>>22)&0x3)* MAXCHIP_PER_LAMB*m_AMChip->getMAXPATT_PERCHIP()) + (read&0x1ffff);
  			av_addr.push_back(read);
  		}
  		addrf.close();

  		cout << "Read " << av_addr.size() << " addresses from the external file" << endl;
  	}

  	//clear output file
  	if (AMBOutputFormat) {
  		// create the AMB feed hit file, starts with an empty event
  		ofstream ambhitfile(hitfname, ofstream::out | ofstream::trunc);
  		for (int ibus=0; ibus!=12; ++ibus) { // loop over the input bus fifos
  			if (ibus>0) ambhitfile << " ";
  			ambhitfile << hex << setw(9) << 0x8f7000000;
  		} // end loop over the input bus fifos
  		ambhitfile << endl;
  		ambhitfile.close();
  	}
  	else {
  		// create the aux files, put an empty event at the begin
  		for(int i_lay=0;i_lay!=8; i_lay++){
  			ofstream auxfile;
  			std::stringstream filename;
  			filename <<"aux_"<<i_lay<<"_"<<hitfname;
  			auxfile.open (filename.str(), std::ofstream::out | std::ofstream::trunc);
  			auxfile << dec << 1 << "\t" << hex << showbase << uppercase << setw(10) << setfill('0') << internal << 0 << endl;
  			auxfile.close();
  		}
  	}

  	// the pattern to be converted in hits is copied in this structure
	std::vector<uint32_t> extracted(NLAYERS);
	std::vector<uint32_t> extractedDC(NLAYERS);

  	// evaluate the total number of pattern that can be extracted,
  	// later useful to evaluate extract the indexes
  	int totindexes = totpatts;
  	if (pattmax-pattmin<totpatts) totpatts = pattmax-pattmin;

  	//generate hits using random() function
  	unsigned int avAddrOff(0);

  	for(int k=0;k<nevents;k++) { // loop over the events
  		std::vector<std::vector<int>> hits(NLAYERS); // structure that holds the generated hits
  		nroads_int = rand()%nroads; // extract the number of full roads for this events
  		for(int j=0;j<nroads_int;j++){ // loop over the full roads
		  //int index = rand()%totindexes+pattmin; // index of the current random road
		  std::default_random_engine generator;
		  std::uniform_int_distribution<int> distribution(0,9);
		  int index = distribution(generator)%totindexes+pattmin;

  			if(use_known){//if use knonw take the address from the list of patterns
  				int serv = avAddrOff++;
  				if (avAddrOff==av_addr.size()) avAddrOff=0; // restart
  				index=av_addr.at(serv);
  			}

  			if(verbose)std::clog<<"INFO: ambslp_gen_hits_DC_new(): getting pattern with address: "<<index<<std::endl;
  			// retrieve the pattern content
  			PattBank.getPatternAndDC(index, extracted, extractedDC);
  			for(int i_lay=0;i_lay!=NLAYERS; i_lay++){
  				for(int i_bit=0; i_bit<DCmax;i_bit++){
  					if(((extractedDC[i_lay]>>i_bit)&0x1)==0x1){
  						int prob = ((rand()%100)/10);
  						if(prob>DCprob)
  							extracted[i_lay]|= 0x1<<i_bit;
  					}
  				}
  				hits.at(i_lay).push_back(extracted[i_lay]);
  			}
  		} // end loop over the full roads

  		nroads_int=rand()%n7roads;
  		for(int j=0;j<nroads_int;j++){ // loop over the N-1 roads
  			int index = rand()%totindexes+pattmin;

  			if(use_known){//if use knonw take the address from the list of patterns
  				int serv = (nroads_int*k)+j;
  				while(serv >= (int)av_addr.size())serv-= av_addr.size();//if you did not load enough patterns cycle on loaded patterns
  				index=av_addr.at(serv);
  			}
  			if(verbose)std::clog<<"INFO: ambslp_gen_hits_DC_new(): getting pattern with address: "<<index<<std::endl;
  			// retrieve the pattern content
  			PattBank.getPatternAndDC(index, extracted, extractedDC);
  			int skip = rand()%NLAYERS;
  			for(int i_lay=0;i_lay!=NLAYERS; i_lay++){
  				if(i_lay==skip)continue;
  				for(int i_bit=0; i_bit<DCmax;i_bit++){
  					if(((extractedDC[i_lay]>>i_bit)&0x1)==0x1){
  						int prob = ((rand()%100)/10);
  						if(prob>DCprob)
  							extracted[i_lay]|= 0x1<<i_bit;
  					}
  				}
  				hits.at(i_lay).push_back(extracted[i_lay]);
  			}
  		} // end loop over the N-1 roads

  		nroads_int=rand()%n6roads;
  		for(int j=0;j<nroads_int;j++) { // loop over the N-2 roads
  			int index = rand()%totindexes+pattmin;
  			if(use_known){//if use knonw take the address from the list of patterns
  				int serv = (nroads_int*k)+j;
  				while(serv >= (int)av_addr.size())serv-= av_addr.size();//if you did not load enough patterns cycle on loaded patterns
  				index=av_addr.at(serv);
  			}
  			if(verbose)std::clog<<"INFO: ambslp_gen_hits_DC_new(): getting pattern with address: "<<index<<std::endl;
  			// retrieve the pattern content
  			PattBank.getPatternAndDC(index, extracted, extractedDC);
  			int skip_0 = rand()%NLAYERS;
  			int skip_1 = rand()%NLAYERS;
  			for(int i_lay=0;i_lay!=NLAYERS; i_lay++){
  				if(i_lay==skip_0 || i_lay==skip_1)continue;

  				for(int i_bit=0; i_bit<DCmax;i_bit++){
  					if(((extractedDC[i_lay]>>i_bit)&0x1)==0x1){
  						int prob = ((rand()%100)/10);
  						if(prob>DCprob)
  							extracted[i_lay]|= 0x1<<i_bit;
  					}
  				}
  				hits.at(i_lay).push_back(extracted[i_lay]);
  			}
  		} // end loop over the N-2 roads

  		int nnoise_int=random()%nnoise;
  		for(int j=0;j<nnoise_int;j++) { // loop over the noisy hits
  			for(int i_lay=0;i_lay!=NLAYERS; i_lay++){
  				int prob = ((rand()%100)/10);
  				if(prob >= 8) continue;
  				hits.at(i_lay).push_back(random()&0xffff);
  			}
  		} // loop over the noisy hits


  		// mix all the hits
  		for(int i_lay=0;i_lay!=NLAYERS; i_lay++){
  			std::random_shuffle(hits.at(i_lay).begin(),hits.at(i_lay).end());
  			//	  for (; hits.at(i_lay).size()>max_hits; )
  			//	  const int max_hits = 1000;
  			if (hits.at(i_lay).size()>max_hits_per_event)
  				hits.at(i_lay).erase(hits.at(i_lay).begin()+max_hits_per_event-1, hits.at(i_lay).end() );
  		}


  		// store the event in the output files
  		if (AMBOutputFormat) {
  			// store the output in the AMB hit format

  			// reopen the AMB feed hit file in append mode
    		ofstream ambhitfile(hitfname, ofstream::app);

    		// pointers to the hit lists, to keep track where the stream is in writing them
    		std::vector<vector<int>::const_iterator> hitsIter(NLAYERS);
    		std::vector<vector<int>::const_iterator> hitsIterE(NLAYERS);
    		for(int i_lay=0;i_lay!=NLAYERS; ++i_lay){
    			hitsIter[i_lay] = hits[i_lay].begin();
    			hitsIterE[i_lay] = hits[i_lay].end();
    		}

    		//TODO this map should be part of the AMBoard class
    		// bus to layer map
    		const unsigned int busToLayer_map[] = {4, 5, 6, 7, 0, 0, 1, 1, 2, 2, 3, 3};

    		// mask that control when all the layers are completed
    		unsigned int hasData(~((~0)<<NLAYERS));

    		while (hasData) { // loop over the input data, until at least one layer has data
    			/* data from hits[] are copied in a file organized like a table, used
    			 * by ambslp_feedhit, this loop finishes when all hits data are written
    			 * in the file, following the proper alignment
    			 */
					for (int ibus=0; ibus!=12; ++ibus) { // loop over the input bus fifos
						/* the data the data to be written is a 36-bit integer, it can be:
						 * - 0x0AAAABBBB, where AAAA and BBBB are 2 16-bit SS words
						 * - 0x4XXXXXXXX, data to be skipped, here only to maintain the lines synchronized
						 */
						unsigned long long int busdata(0x400000000);

						const unsigned int &ilay(busToLayer_map[ibus]);

						if ((hasData>>ilay)&1) { // has hits
							busdata = 0x0; // reset all the bits

							// try to retrieve 2 hits from the current layer
							unsigned nhits(0);
							for (unsigned int ihit=0; ihit!=2 && hitsIter[ilay]!=hitsIterE[ilay]; ++ihit,hitsIter[ilay]++) {
								const unsigned int &dw(*hitsIter[ilay]);
								busdata |= dw<<(ihit*16); // 2nd hits written in the MSB
								nhits += 1; // increment the counter on how many hits are in the word, max 2
							}

							// check if the hits' stream ended during the process
							if (hitsIter[ilay]==hitsIterE[ilay]) {
								hasData &= ~(1<<ilay); // zero the bit for this layer
								if (nhits!=2) {
									// incomplete data, copy the LSB on the MSB portion
									busdata |= (busdata<<16);
								}
							}
						}

						if (ibus>0) ambhitfile << " ";
						ambhitfile << hex << setw(9) << setfill('0') << busdata;
					} // end loop over the input bus fifos
					ambhitfile << endl;
    		} // end loop over the input data

    		// close the event adding the EE tag for all the streams
    		for (int ibus=0; ibus!=12; ++ibus) {
    			if (ibus>0) ambhitfile << " ";
    			ambhitfile << hex << setw(9) << (0x8f7000000 | (k+1)); // EE tag with event number+1
    		}
    		ambhitfile << endl;

    		// close the file and clean the temperary pointers
    		ambhitfile.close();

  		}
  		else {
  			// store the output in the AUX feed format

				for (int i_lay=0;i_lay!=NLAYERS;++i_lay) { // loop over the layers
					// open the file related to the current stream in append mode
					ofstream auxfile;
					std::stringstream filename;
					filename <<"aux_"<<i_lay<<"_"<<hitfname;
					auxfile.open (filename.str(), std::ios_base::app);
					if(!auxfile.is_open()){
						std::cerr<<"Error: ambslp_gen_hits_DC_new(): can not open file " << filename.str() <<std::endl;
						return -1;
					}

					unsigned int outword(0); // word to accumulate the output
					unsigned int outpart(0); // counter of the word portion
					for (auto rndhit : hits[i_lay]) { // loop over the hits
						outword |= rndhit<<(16*outpart); // fill the proper portion of the word

						outpart += 1; // increment the part counter

						if (outpart==2) {
							// the word is complete and has to be dumped in the output file
							auxfile << dec << 0 << '\t' << hex << showbase << uppercase << setw(10) << setfill('0') << internal << outword << endl;
							// reset the output related vars
							outpart = 0;
							outword = 0;
						}
					} // end loop ov<r ths hits

					// check if a final hit has to saved
					if (outpart) {
						outword |= outword<<16; // replicate the up to 16 bits set in the up 16 bits
						auxfile << dec << 0 << '\t' << hex << showbase << uppercase << setw(10) << setfill('0') << internal << outword << endl;
					}

					// save the end event tag and close the file
					auxfile << dec << 1 << "\t" << hex << showbase << uppercase << setw(10) << setfill('0') << internal << k+1 << endl; // there was an empty event
					auxfile.close();
				} // end loop over the layers
  		}

  		// clear the list of this
  		for(int i_lay=0;i_lay!=NLAYERS; i_lay++){
  			hits[i_lay].clear();
  		}

  	} // end loop over the events


  	if(verbose)std::clog<<"INFO: ambslp_gen_hits_DC_new(): DONE, bye!"<<std::endl;
  	return 0;
  }

  }
}
int main(int argc, char **argv) 
{ 
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("events,e", value< std::string >()->default_value("3"), "# of events")
    ("roads-event,r", value< std::string >()->default_value("5"), "# of roads/event with 8 hits (8/8) ")
    ("r7", value< std::string >()->default_value("0"), "# of roads/event with 7 hits (7/8)")
    ("r6", value< std::string >()->default_value("0"), "# of roads/event with 6 hits (6/8)")
    ("noise,n", value< std::string >()->default_value("10"), "# of noise hits/event *actually not used*")
    ("seed,s", value< std::string >()->default_value("1"), "set random seed")
    ("DCmax", value< std::string >()->default_value("2"), "Number of ternary bits used in the bank")
    ("DCprob", value< std::string >()->default_value("5"), "Probalility of don't care bits [0-10]")
    ("hit_file", value< std::string >()->default_value("hits.txt"), "suffix added to the name of the output file")
    ("patt_address", value< std::string >()->default_value("addresses.txt"), "File containing the address of the patterns you want to fire. Used only if --dp is present")
    ("rs", "If present is used a \"random seed\"")
    ("amb", "If present the output is generated in the AMB format, single file with 12 columns")
    ("dp", "If present is used a will generate use only patterns contained in patt_address file")
    ("v", "If present use sets verbose mode")
    ("pattern_file", value< std::string >()->default_value(""), "PatternBank file (can be specified in any parameter position, also omitting \"--pattern_file\")")
    ("bankType", value< std::string > ()->default_value("1"), "0 for ROOT bank file. 1 for text bank file. Default value is 1.")
    ("ssoff","If present the SS offset is used to avoid SS=0")
    ("pattmin",value<int>()->default_value(0),"Minimum index of the pattern")
    ("pattmax",value<int>()->default_value(0xffffff), "Maximum index of a random pattern, if larger than the pattern size will be limited")
    ("MAXhitEvent", value< std::string >()->default_value("1000"), "Maximum number of hits (16 bits) per event per link")
    ("AMChip", value<unsigned int>()->default_value(6), "AM chip version")
    ;


  positional_options_description pd;
  pd.add("pattern_file", -1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
  {
    std::cerr << desc << std::endl;
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
  {
    std::cout << std::endl <<  desc << std::endl ;
    return 0;
  }

  int nevents           = daq::ftk::string_to_int( vm["events"].as<std::string>());
  int nroads            = daq::ftk::string_to_int( vm["roads-event"].as<std::string>());
  int n7roads           = daq::ftk::string_to_int( vm["r7"].as<std::string>());
  int n6roads           = daq::ftk::string_to_int( vm["r6"].as<std::string>());
  int nnoise            = daq::ftk::string_to_int( vm["noise"].as<std::string>());
  int seed              = daq::ftk::string_to_int( vm["seed"].as<std::string>());
  int DCmax             = daq::ftk::string_to_int( vm["DCmax"].as<std::string>());
  int DCprob            = daq::ftk::string_to_int( vm["DCprob"].as<std::string>());
  int type = daq::ftk::string_to_int( vm["bankType"].as<std::string>() );
  bool randomseed	= vm.count("rs") ? 1 : 0 ;
  bool use_known	= vm.count("dp") ? 1 : 0 ;
  bool verbose		= vm.count("v") ? 1 : 0 ;
  bool SSOffset = vm.count("ssoff") ? true : false;
  
  int pattmin = vm["pattmin"].as<int>();
  int pattmax = vm["pattmax"].as<int>();
  int max_hits_per_event = daq::ftk::string_to_int( vm["MAXhitEvent"].as<std::string>());

  daq::ftk::AMBOutputFormat = vm.count("amb")>0 ? true : false;

  unsigned int AMChipV = vm["AMChip"].as<unsigned int>();

  std::string hitfname =  vm["hit_file"].as<std::string>();
  daq::ftk::assert_string_parameter_not_empty( hitfname, std::string("hit_file") ); // checking that is not empty
  
  std::string addrfname =  vm["patt_address"].as<std::string>();
  daq::ftk::assert_string_parameter_not_empty( addrfname, std::string("patt_address") ); // checking that is not empty
  
  std::string pattfname =  vm["pattern_file"].as<std::string>();
  daq::ftk::assert_string_parameter_not_empty( pattfname, std::string("pattern_file") ); // checking that is not empty
  
  return daq::ftk::ambslp_gen_hits_DC_new(nevents, nroads, n7roads, n6roads, nnoise, seed, randomseed, DCmax, DCprob, pattfname.c_str(), type, SSOffset, addrfname.c_str(), hitfname.c_str(), use_known, pattmin, pattmax, max_hits_per_event, AMChipV, verbose);
}
