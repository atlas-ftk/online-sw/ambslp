// functions for StatusRegisterAMBSelector.h

#include "ambslp/StatusRegisterAMBSelector.h"

namespace daq {
namespace ftk {

// Public Member Functions:
StatusRegisterAMBSelector::StatusRegisterAMBSelector(
	uint vmeSlot,
	uint fpgaNum,
	std::string registerName,
	uint registerAddress,
	srType type,
	uint selectorAddress,
	uint readerAddress
) : StatusRegisterVMESelector(vmeSlot, fpgaNum, registerName, registerAddress, type, selectorAddress, readerAddress)
{
	// The AMB Selector class only allows access via AMB_selector, so variables are set accordingly
	m_access = srAccess::AMB_selector;
}

StatusRegisterAMBSelector::~StatusRegisterAMBSelector() {
}

void StatusRegisterAMBSelector::readout() {
	// This has to be defined by AMB experts
	// if needed, addiitonal variables can be passed to the StatusRegisterAMBSelector constructor, stored as member variables and used here
	// vmeSlot, fpgaNum, registerAddress, selectorAddress, readerAddress should uniquely define a register
}

} // namespace ftk
} // namespace daq
