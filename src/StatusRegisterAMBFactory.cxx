// functions for StatusRegisterAMBFactory.h

#include "ambslp/StatusRegisterAMBFactory.h"
#include "ambslp/StatusRegisterAMBCollection.h"
#include "ambslp/ambslp_vme_regs.h"

#include "ftkcommon/exceptions.h"

namespace daq {
namespace ftk {

StatusRegisterAMBFactory::StatusRegisterAMBFactory(
	std::string ambName,
	u_int ambSlot,
	std::string registersToRead,
	std::string isServerName,
	IPCPartition ipcPartition
	) : StatusRegisterVMEFactory (ambName, ambSlot, registersToRead, isServerName) 
	{

	std::unique_ptr<StatusRegisterAMBStatusNamed> m_srAMBSNamed;
	try 
	{
		m_srAMBSNamed = std::make_unique<StatusRegisterAMBStatusNamed>(ipcPartition, m_isProvider.c_str());
	} 
	catch(ers::Issue &ex)  {
	  //daq::ftk::ISException e(ERS_HERE, "Error while creating StatusRegisterAMBStatusNamed object", ex);  // Append the original exception (ex) to the new one 
	  daq::ftk::ftkException e(ERS_HERE, name_ftk(), "Error while creating StatusRegisterAMBStatusNamed object");  
	  ers::warning(e);  //or throw e;
	}

	// Store the name and slot of the board in StatusRegisterAMBStatusNamed
	m_srAMBSNamed->Name = m_name;
	m_srAMBSNamed->Slot = m_slot;
	
	// To read different registers, modify the following lines:
	// symbolic constants defined in ambslp_vme_regs.h are used
	setupCollection('V',0,TMODE_VME,AMB_VME_LAST_REGISTER,4,"VME Chip Registers: [0xXX000008-> last reg]","Chip",&(m_srAMBSNamed->VME_Chip_sr_v), &(m_srAMBSNamed->VME_Chip_sr_v_info));
	setupCollection('H',0,HOLD_FLAG_HIT,AMB_HIT_LAST_REGISTER,4,"Hit FPGA Registers: [0xXX002090-> last reg]","Hit",&(m_srAMBSNamed->Hit_FPGA_sr_v),&(m_srAMBSNamed->Hit_FPGA_sr_v_info));
	setupCollection('R',0,PROGFULL_EMPTY_INPUTFIFO,AMB_ROAD_LAST_REGISTER,4,"Road FPGA Registers: [0xXX0040C0-> last reg]","Road",&(m_srAMBSNamed->Road_FPGA_sr_v),&(m_srAMBSNamed->Road_FPGA_sr_v_info));
	setupCollection('C',0,CONTROL_STATUS_REGISTER,AMB_CONTROL_LAST_REGISTER,4,"Control VME Registers: [0xXX006000-> last reg]","Control",&(m_srAMBSNamed->Control_VME_sr_v),&(m_srAMBSNamed->Control_VME_sr_v_info));
	// Create also a selector-type register collection
	setupCollection('S',0,0x15,0x19,1,"Test selector AMB Registers: ???","SelectorTest",&(m_srAMBSNamed->Selector_AMB_sr_v),&(m_srAMBSNamed->Selector_AMB_sr_v_info), 0xF, 0xA, srType::srOther, srAccess::AMB_selector);

	// Pass the StatusRegisterAMBStatusNamed to SRVMEFactory so it can be checked into IS during readout()
	m_srxNamed = std::move(m_srAMBSNamed);
}

StatusRegisterAMBFactory::~StatusRegisterAMBFactory() {
	// No need to delete m_srAMBsNamed here as it's done by StatusRegisterVMEFactory when it deletes m_srxNamed
}

void StatusRegisterAMBFactory::setupCollection(
	char IDChar,
	uint fpgaNum,
	uint firstAddress, 
	uint finalAddress,
	uint addrIncrement,
	std::string collectionName, 
	std::string collectionShortName,
	std::vector<uint>* ISObjectVector,
	std::vector<uint>* ISObjectInfoVector,
	uint selectorAddress,
	uint readerAddress,
	srType type,
        srAccess access
	) 
{
  if( access != srAccess::AMB_selector ) {
    StatusRegisterVMEFactory::setupCollection(IDChar, fpgaNum, firstAddress, finalAddress, addrIncrement, collectionName, collectionShortName, ISObjectVector, ISObjectInfoVector,
	selectorAddress, readerAddress, type, access);
  } else {
    // Checks if the input string contains the identifying character for the collection
    if (std::string::npos != m_inputString.find_first_of(toupper(IDChar))) {
      // If it does, a collection for the corresponding set of registers is created
      std::unique_ptr<StatusRegisterAMBCollection> srVMEc = std::make_unique<StatusRegisterAMBCollection>(m_slot,fpgaNum,firstAddress,finalAddress,addrIncrement,collectionName,collectionShortName,
						selectorAddress, readerAddress, type, access);
      m_collections.push_back(std::move(srVMEc));
      m_isObjectVectors.push_back(ISObjectVector);
      if(ISObjectInfoVector){
        setupCollectionInfo(ISObjectInfoVector, firstAddress, finalAddress, addrIncrement, fpgaNum, selectorAddress, readerAddress);
      }
    }
  }
}


} // namespace ftk
} // namespace daq
