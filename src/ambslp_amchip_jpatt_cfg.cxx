/* ambslp_amchip_jpatt_cfg.cxx

   Author N. Biesuz 2014 nicolo.vladi.biesuz@cern.ch
*/


#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv){
  using namespace  boost::program_options ;

  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("chipnum", value< std::string > ()->default_value("3"), "The chips to read. Default value is 15=0xF.")
    ("columns", value< std::string > ()->default_value("a"), "The vme columns that must be addressed. Default value is 'a'=all. Valid values are 'a' all, 'e' even, 'o' odd.")
    ("thr", value< std::string > ()->default_value("8"), "threshold, default 8")
    ("req_lay", value< std::string > ()->default_value("0"), "request layer 0, default 0")
    // ("disable_xoram", value< std::string > ()->default_value("0"), "disable xoram clock distribution, default 0")
    // ("disable_top2", value< std::string > ()->default_value("1"), "disable top2 clock distribution, default 1")
    ("disable_mask", value< std::string > ()->default_value("0"), "Disable any of the 11 AM06 pattern blocks. If different from0, it ignores disable_xoram and disable_top2.")
    ("tmode", value< std::string > ()->default_value("0"), "test mode, default 0")
    ("disable_pflow", value< std::string > ()->default_value("0"), "disable the output pattern flow, default 0")
    ("driving_strength", value< std::string > ()->default_value("1"), "single ended pads driving strength (0 for low, 1 for high), default 1")
    ("dcbits", value< std::string > ()->default_value("3"), "number of dont'care bits for each layer, default 3")
    ("continuous", value< std::string > ()->default_value("0"), "continuous mode, default 0")
    ("verbose,v", "If present sets verbose level to maximum")
    ;

  positional_options_description pd;
  pd.add("pattern_file", 1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );
  const char* columns = (vm["columns"].as<std::string>()).c_str();
  if(not(strcmp(columns, "a")==0) and not(strcmp(columns, "e")==0) and not(strcmp(columns, "e")==0) ){
    std::cerr<<"warning: function called with invalid option columns: '"<<columns<<"'. Using default value 'a'!"<<std::endl;
    columns="a";
  }
  int thr = daq::ftk::string_to_int( vm["thr"].as<std::string>() );
  int req_lay = daq::ftk::string_to_int( vm["req_lay"].as<std::string>() );
  // int dis_xoram = daq::ftk::string_to_int( vm["disable_xoram"].as<std::string>() );
  // int dis_top2 = daq::ftk::string_to_int( vm["disable_top2"].as<std::string>() );
  int disable_mask = daq::ftk::string_to_int( vm["disable_mask"].as<std::string>() );
  int tmode = daq::ftk::string_to_int( vm["tmode"].as<std::string>() );
  int dis_pflow = daq::ftk::string_to_int( vm["disable_pflow"].as<std::string>() );
  int drv_str = daq::ftk::string_to_int( vm["driving_strength"].as<std::string>() );
  int dcbits = daq::ftk::string_to_int( vm["dcbits"].as<std::string>() );
  int cmode = daq::ftk::string_to_int( vm["continuous"].as<std::string>() );
  bool verbose = vm.count("verbose")>0 ? true : false ;

  daq::ftk::AMBoard myAMB(slot);
  if(verbose) myAMB.setVerbosity(daq::ftk::AMBoard::DEBUG);
  return myAMB.amchip_jpatt_cfg(chipnum, columns, thr, req_lay, disable_mask, tmode,  dis_pflow,  drv_str, dcbits, cmode);

}

