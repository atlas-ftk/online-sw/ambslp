
/** Author: A. Annnovi, N. Biesuz, G. Volpi
Date: 2016/01/25
*/

#include "ambslp/AMBoard.h"

using namespace std;

struct am_rec_address {
  uint32_t index;
  uint32_t hitmap;
  uint32_t rec_address;
  uint32_t correct_address;
  uint32_t valid;
};

namespace daq {
  namespace ftk {
    AMBoard::AMBoard(int slot, VerbosityLevel verblvl) :
      m_DCDC_val_Vout(0x02e30000),
      m_slot(slot),
      m_vme(0x0), m_AMBVersion(AMBvNONE), // m_LAMBVersion(LAMBvNONE),
      m_chainlength(0),
      m_NMaxChips(64), m_NMaxLAMBs(4), m_NColumns(32), m_NChipsPerLAMB(16),
      m_LAMBPresence(0),
      m_LAMB_mask(0),
      m_LAMBMap(0x0),
      m_DCDCstatus(0),
      m_NPattPerChip(2048), m_AddTestPattern(false), m_OverrideSpecialPattern(true), 
      m_AMChipMap(0x0), m_AMChip(0x0),
      m_patternBankFileName(""),
      m_WCSS(0x7000),
      m_initThr(15), m_matchThr(7), m_DummyHit(0x7a0075a0), 
      m_FreezeSeverityError(0x7),
      m_DoStandaloneTest(false), m_testConfLoop(false), m_CanDoPhase(true), m_testFilePath(""),
      m_PhaseTest(), m_PattProbTolerance(1.),
      m_VerbosityLevel(verblvl)
    {

      // initialize a VME interface for the given slot
      try {
        m_vme = VMEManager::global().amb(m_slot);  }
      catch (daq::ftk::VmeError &ex) {
        stringstream buffer;
        buffer << "VME access via interface manager has failed for ambslp: cannot connect to AMBoard in slot " << m_slot;
        daq::ftk::ftkException issue(ERS_HERE, name_ftk(), buffer.str());
        throw issue;
      }
      // reset the SpyBuffer pointers
      m_InputSpyBuffer.clear();
      m_OutputSpyBuffer.clear();

      spys_freezed = false;

      // set a pointer of a generic, empty AM chip
      m_AMChip = AMChip::global();

      // read the configuration from VME registers
      readConfig();

      // read chip inforation from AM chip requests
      read_AM_idcodes();
    } // end of AMBoard::AMBoard(int slot)

    // Configure AMBoard dummy hit
    // slowly increase the number of dummy hit active bits
    // in order to slowly increase power usage.
    // ramping up to the desired dummy hit value.
    void AMBoard::configureDummyHit()
    {
      const struct timespec deltaT10ms = { 0, 10000000};
      unsigned int low(m_DummyHit&0xFFFF);
      unsigned int high((m_DummyHit>>16)&0xFFFF);
      unsigned int common(low&high);
      
      // first select the common bits among the two half words
      // normally common bits will be 0x70007000.
      common |= (common<<16);
      try{
        m_vme->write_check_word(AMB_HIT_DUMMY_HIT_WORD, common);

        for(int i=0; i<16; i++){// enable 2 more dummy hit bits at a time.
          common |= m_DummyHit&(0x3<<(2*i));
          nanosleep(&deltaT10ms,0x0); // wait 10ms
          m_vme->write_check_word(AMB_HIT_DUMMY_HIT_WORD, common);
        }

      }catch(daq::ftk::VmeError &vmeerror){
        daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "I was unable to set up the correct DummyHit value. Dummy hit may be disabled and cause missing roads. Please check with expert!");
        throw issue;
      }
    }


    void AMBoard::unconfigDummyHit()
    {
      try{
        m_vme->write_check_word(AMB_HIT_DUMMY_HIT_WORD, 0x0);
      }catch(daq::ftk::VmeError &vmeerror){
        daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "I was unable to reset DummyHit value. This can cause anomalous power consumption. Please check with expert!");
        throw issue;
      }
    }


    /**
     * This function read the configuration of the AMboard and sets tghe proper data members
     */

    void AMBoard::readConfig()
    {
      //read the Control FPGA status register
      unsigned crtl_status = m_vme->read_word(CONTROL_STATUS_REGISTER);
      //read AMBoard version;
      unsigned int AMBVerVal = (crtl_status >>AMB_CONTROL_STATUS_BOARD_VERSION_1STBIT) & 0xF;
      /* Patch: AMBVerVal reflects the real board version, physical from the board, but
       * only with recent FW version. The current assumption is that if the value is 1
       * is an old FW version and by default means AMBv3, otherwise the value is left
       * as it is, in future a FW version can be used
       */
      if (AMBVerVal==1) AMBVerVal=3;
      // setting according the enums
      m_AMBVersion = static_cast<BoardVer>(AMBVerVal);

      //read LAMB presence pins status
      m_LAMBPresence = (crtl_status>>AMB_CONTROL_STATUS_LAMB_PRESENCE_1STBIT ) & 0xF;
      PRINT_LOG("m_LAMBPresence = 0x" << std::hex << m_LAMBPresence);

      //read DCDC converters status
      m_DCDCstatus = m_vme->read_word(ENABLE_AMCORE_DCDC);
      this->configure(); // make sure AMBv3 powers on all DC/DC

      //Read all FW version registers
      m_HITFWVersion = m_vme->read_word(AMB_HIT_FW_VERSION);
      m_CTRLFWVersion = m_vme->read_word(AMB_CONTROL_FW_VERSION);
      m_VMEFWVersion = m_vme->read_word(AMB_VME_FW_VERSION);
      m_ROADFWVersion = m_vme->read_word(AMB_ROAD_FW_VERSION);
    }


    void AMBoard::configure()
    {
      if(m_AMBVersion == AMBv3) {
        /*
         * Turn-on DC/DC converters based on AMB presence (AMBv3 only), in V4 the register has not effect
         */
        u_int dc = 0x0; // reset the dc value
        if ( m_LAMBPresence & 0x5 ) dc |= 0x38; // upper LAMBs
        if ( m_LAMBPresence & 0xa ) dc |= 0x07; // lower LAMBs

        if (m_DCDCstatus!=dc){
          // Always enable 1.0V DC-DC converters for AMchips
          PRINT_LOG("Enable DC/DC for AM core with value 0x" << std::hex << dc);
          //m_vme->write_check_word(ENABLE_AMCORE_DCDC, dc);
          m_vme->write_word(ENABLE_AMCORE_DCDC, dc);
          sleep(10);
        }
      }
    }

    /** The function initialize the board, switching on the DCDC convertes.
     *
     * \param slot: slot number of the board
     * \param FixPolarity: force the polarity for old versions of AUX and AMB
     * \param dc: a bit mask that sets which DCDC converters need to be set. If not given autoconfigure is tried.
     */
    bool AMBoard::ambslp_config_reg(int FixPolarity, unsigned dc)
    {
      PRINT_LOG("Running AMBoard::ambslp_config_reg(...)");

      unsigned int test_word, register_content, expected_value;

      /* These lines where required by the AMBv1 that cannot be handled by this code
         if(m_AMBVersion == AMBv1)
         {
         PRINT_LOG("Select AMBSLP v.1");

         test_word = 0x1;
         m_vme->write_word(AMB_HIT_FW_VERSION, test_word);

         register_content = m_vme->read_word(AMB_HIT_FW_VERSION);

         expected_value = 0x1;
         if ((register_content != expected_value ))
         {
         PRINT_LOG("error: Not set the register 0x" << std::hex << register_content << ", expected value 0x" << expected_value << " !!!" );
         }

         }
         */
      if (dc==0xff) {
        /* Check, through the presence pins, which DC/DC converters
         * should be switched on
         */


        // Posssible improvements in case interface is cleaned
        /*
         * opt1:
         * unsigned int lambconf = GetLAMBConf(slot);
         * dc = 0x0;
         * if (lambconf&(0x5)) dc |= 0x38;
         * if (lambconf&(0xa)) dc |= 0x7;
         *
         * opt2:
         *
         */


        // retrieve the presence pin values, from the STATUS register word
        register_content = m_vme->read_word(CONTROL_STATUS_REGISTER);

        // check if the up DCDC has to be switched on, means LAMB 0 or 2 are present
        dc = 0x0; // reset the dc value, otherwise remain to 0xff, not good
        if (register_content&(0x5<<16)) dc |= 0x38;
        // check if the down DCDC has to be switched on, means LAMB 1 or 3 are present
        if (register_content&(0xa<<16)) dc |= 0x7;

        PRINT_LOG("Will try setting DC/DC for AM core with setup " << std::hex << dc << ".");
      }

      // Always enable 1.0V DC-DC converters for AMchips
      PRINT_LOG("Enable DC/DC for AM core with value 0x" << std::hex << dc);
      test_word = dc;
      m_vme->write_word(ENABLE_AMCORE_DCDC, test_word);
      register_content = m_vme->read_word(ENABLE_AMCORE_DCDC);

      expected_value = dc;
      if ((register_content != expected_value )) {
        PRINT_LOG("error: setting the enable DC/DC for AM core" << std::hex << register_content << ", expected value 0x" << expected_value << " !!!" );
      }

      if(FixPolarity == 1)
      {
        PRINT_LOG("Fix polarity AUX card");
        test_word = 0x25410000;
        m_vme->write_word(CONFIG_POL_HIT, test_word);
        register_content = m_vme->read_word(CONFIG_POL_HIT);

        expected_value = 0x25410000;
        if ((register_content != expected_value ))
        {
          PRINT_LOG("error: Not Polarity Fixed 0x" << std::hex << register_content << ", expected value 0x" << expected_value << " !!!" );
        }else{PRINT_LOG("Polarity Road Fixed!");}

        test_word = 0x569A;
        m_vme->write_word(CONFIG_POL_ROAD, test_word);
        register_content = m_vme->read_word(CONFIG_POL_ROAD);

        expected_value = 0x569A;
        if ((register_content != expected_value ))
        {
          PRINT_LOG("error: Not Polarity Fixed 0x" << std::hex << register_content << ", expected value 0x" << expected_value << " !!!" );
        }else{PRINT_LOG("Polarity Hit Fixed!");}
      }

      PRINT_LOG("Monitoring Error link");
      register_content = m_vme->read_word(LOSS_OF_SYNC0);
      PRINT_LOG("SCT link 0:" << std::hex << (register_content & 0xf));
      PRINT_LOG("SCT link 1:" << std::hex << (register_content>>8 & 0xf));
      PRINT_LOG("SCT link 2:" << std::hex << (register_content>>16 & 0xf));
      PRINT_LOG("SCT link 3:" << std::hex << (register_content>>24 & 0xf));

      // Mask unused links from the LAMB on ROAD FPGA (force sending EE)
      unsigned int anti_FORCE_EE_LINKS_mask = 0;

      // define 16 bit mask of links that need to have EE forced 
      for (int ii=0; ii<MAXLAMB_PER_BOARD; ii++) {
        if ( ((m_LAMBPresence>>ii)&0x1) == 1 ){
          anti_FORCE_EE_LINKS_mask |= 0xf << (ii*4); 
        }
      }

      unsigned int FORCE_EE_LINKS_mask = ~anti_FORCE_EE_LINKS_mask & 0xFFFF;

      m_vme->write_check_word(FORCE_EE_LINKS, FORCE_EE_LINKS_mask);
      m_vme->write_check_word(AMB_ROAD_IDLE_WORDS, 0x80); // set 0x70 IDLE words to timeout on AM06 patterns

      return true;
    }

    /** This function finds the chain length (returned in output) and finds the chip_mask and columns_mask returned as paramters by reference
     *
     * The procedure works as follows:
     *   1) We assume all columns are active and are full of chips: 2*(expected chainlength) = 4, columns mask = 0xffffffff;
     *   2) We set all chips in bypass mode:
     *     - columns without chips will just be ignored by the hardware istelf;
     *     - if the chain length is smaller than the expected one we will write multiple times in the register of the same chip. Since we are writing only '1' this will cause no problems; 
     *     - if the chain is longer than the expected latest chip in columns will not be programmed properly: this is the reason why we are using 2*(expected chainlength).                 
     *   3) Enter in shift DR mode;
     *   4) Write shift a '1' on all columns;
     *   5) Loop for 2*(expected chainlength):
     *     - shift 0;
     *     - read back the content;
     *     - loop on chains:
     *       + did you read '1' ? YES==(column is active, chain length== number of shifted '0's before the '1') NO==continue;
     *   6) Proper format chip_mask and column_mask.
     *   7) Results are put in int* chip_mask, int* columns_mask
     */

    int AMBoard::getJtagChainInfo(int &chip_mask, int &columns_mask) {
      columns_mask = 0x00000000;
      chip_mask = 0x00000001;
      int found(0x00000000);

      unsigned int all_columns(0);
      int max_chip_in_col(0);
      unsigned int zero(0x00000000);
      unsigned int one(0xffffffff);

      const int TDI_SHIFTS = 2*MAXCHIP_PER_VMECOL+1;

      std::array<uint32_t,TDI_SHIFTS>  indata;
      indata[0] = one;
      for(int i=1; i<TDI_SHIFTS;i++) indata[i]=zero;
      std::array<uint32_t,TDI_SHIFTS> outdata;
      for(int column=0; column < MAXCOLUMNS; column++)all_columns|=(0x1<<column);
      GotoTestReset(m_vme);
      GotoIdlefromReset(m_vme, all_columns);

      // TODO replace 0x40f with a function of MAXCHIP_PER_VMECOL
      ConfigureScam(m_vme, 0x40f, all_columns, BYPASS, 1);
      GotoShiftDRfromIdle(m_vme, all_columns);
      AMshift(m_vme, all_columns, TDI_SHIFTS, indata.data(), outdata.data(), 1);

      for(int chip=0; chip<TDI_SHIFTS; chip++) {
        for(int column=0; column < MAXCOLUMNS; column++) {
          if ( ((outdata[chip]>>column)&0x1) ==0x1 ) {
            if(((found>>column)&0x1) == 0x1){
              int msk =0x00000000;
              msk |= (0x1<<column);
              msk = msk xor 0xffffffff;
              columns_mask = (columns_mask &  msk) ; 
            }else{
              columns_mask |= 0x1<<column;
              found |= 0x1<<column;
              if(max_chip_in_col<chip) {
                max_chip_in_col=chip;
              }
            }
          }
        }
        if(getVerbosity()<=AMBoard::DEBUG){
          std::cout << "Results of getJtagChainInfo at chip = " << chip << std::endl;
          std::cout << "outdata[chip]    = 0x" << std::hex << outdata[chip] << dec << std::endl;
          std::cout << "columns_mask    = 0x" << std::hex << columns_mask << dec << std::endl;
          std::cout << "found           = 0x" << std::hex << found << dec << std::endl;
          std::cout << "max_chip_in_col = 0x" << std::hex << max_chip_in_col << dec << std::endl;
        }
      }

      //      ConfigureRegister(m_vme, 0x40F, 0xFFFFFFFF, 0xFF, bool* reg_data, int reg_length, u_int* outdata)
      ConfigureScam( m_vme, 0x40F, 0xFFFFFFFF, 0x08, 1);

      for(int chip=0; chip<max_chip_in_col; chip++) chip_mask|=(0x1<<chip);
      return max_chip_in_col;
    } // end of AMBoard::getJtagChainInfo(...)


    //==============================================================================================================
    /** \brief read ID codes from AM chip and fill configuration data members
     *
     * Fills the configuration data member 
     * m_LAMBMap with 4 bits bitmap of installed LAMBs.
     *  A LAMB is declared installed if any of the chips has the AM05 or AM06 IDCODE
     * m_AMchipMap with map of available AM chips 64 bits. (TO BE implemented)
     * m_AMchipType returns the type of the first AM05 or AM06 chip found
     */
    void AMBoard::read_AM_idcodes() {

      int active_columns = 0xffffffff;
      int all_chipcolumn = 0xffffffff;

      int chip_mask      = 0;
      int columns_mask   = 0;
      int chipno         = 0;

      std::vector<uint32_t> outdata(MAXDATAWORDS);
      std::vector<uint32_t> indata;


      //u_int indata[MAXDATAWORDS];
      //
      //  federico: please note that the block transfer me be failing here!
      //            using NULL for indata is useful because it sets to 0 the value to be shifted
      //            in the IR; as a consequence, the output of this function is *almost* coherent
      //            even if no chip is present (LAMB v2 or missing LAMB)
      //u_int *indata = nullptr;


      m_AMChip    = AMChip::global();
      m_AMChipMap = 0;
      m_LAMBMap   = 0;


      // set DCS server semaphore red (stop DCS monitoring)

      if(m_vme->read_word(AMB_CONTROL_DCS_RUN) == 0xCAFE) {

	const struct timespec deltaT_DCS = {5, 0};  // This is a deltaT of 5 sec. Is this too much?? [TBC]
        nanosleep(&deltaT_DCS, 0x0);

        if (m_vme->read_word(AMB_CONTROL_DCS_RUN) == 0xCAFE) {

          std::stringstream message;
          message << "DCS semaphore is already taken";
          daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
          ers::warning(ex);
        }
      }

      ERS_LOG("Stopping DCS monitoring during read_AM_idcodes");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0xCAFE);


      //-----

      m_chainlength = getJtagChainInfo(chip_mask, columns_mask);

      /* build chip number */
      chipno = (m_chainlength<<8) + chip_mask;


      bool JTAGnotOK = (m_chainlength != 2) || (chip_mask != 3) || (columns_mask != all_chipcolumn);

      if (JTAGnotOK) { // issue a FTK error

        std::stringstream message;
        message << "JTAG chains not read correctly. m_chainlength="<<m_chainlength<< " chip_mask="<<chip_mask << " columns_mask="<< std::hex << "0x" << columns_mask << std::dec;
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::error(ex);
      }
	
      // Print the result of checking the JTAG chains depending on verbosity, or forced if the results are not as expected
      if (getVerbosity()<=AMBoard::INFO || JTAGnotOK) {
        std::cout << " chainlength  determined from HW via JTAG is : " << m_chainlength << std::endl;
        std::cout << " chip_mask    determined from HW via JTAG is : " << chip_mask     << std::endl;
        std::cout << " columns_mask determined from HW via JTAG is : " << std::hex << "0x" << columns_mask << std::dec << std::endl;
        printf("chipno %x\n", chipno);  
      }

      //**********************************************************************
      //Read the IDCODE

      GotoTestReset(m_vme); //RESET tap 

      GotoIdlefromReset(m_vme, all_chipcolumn); //go to IDLE

      ConfigureRegister(m_vme, chipno, active_columns, IDCODE, indata.data(), IDCODE_REG_SIZE, outdata.data());


      unsigned int _idcode_AM05 = AMChip05::global()->getIDCODE_VAL();
      unsigned int _idcode_AM06 = AMChip06::global()->getIDCODE_VAL();


      for(int lamb = 0; lamb < 4; lamb++) {           // loop over the LAMBs, up to 4

        for (int chain = 0; chain < 8; chain++) {     // loop over the number of chains per LAMB, up to 8

	  unsigned int idcode0 = 0;
	  unsigned int idcode1 = 0;
	  unsigned int idcode2 = 0;
	  unsigned int idcode3 = 0;

          for(int i = 0; i < m_chainlength*32; i++) { // loop of the bits: 32 bit for each element of the chain

            // ordinamento degli id e' invertito rispetto al id numerico del chip nella catena?

	    unsigned int _v_ = ( ( (outdata[i]>>((lamb*8)+chain)) & 0x1 ) << i);

            if     (i/32==0) idcode0 += _v_; // first block
            else if(i/32==1) idcode1 += _v_; //   2nd block
            else if(i/32==2) idcode2 += _v_; //   3rd block
            else if(i/32==3) idcode3 += _v_; //   4th block

          }  // end loop over the returning jtag data

          if (getVerbosity()<=AMBoard::INFO) {
            printf("Idcode chain %i LAMB %i %x \t %x \t %x \t %x \n", chain, lamb, idcode0, idcode1, idcode2, idcode3);
          }


          // Checks only AMChip in first two position as in LAMB v2 and LAMB v3
          if (idcode0 == _idcode_AM05 || idcode1 == _idcode_AM05 || idcode2 == _idcode_AM05 || idcode3 == _idcode_AM05 ||
              idcode0 == _idcode_AM06 || idcode1 == _idcode_AM06 || idcode2 == _idcode_AM06 || idcode3 == _idcode_AM06)
            m_LAMBMap |= 1 << lamb;


          // in the next two if statements, if the chip kind is unset, from the idcode the correct
          // chip pointer is set

          if (m_AMChip->getNAME() == AMChip::global()->getNAME() ) {

              if      ( idcode0 == _idcode_AM06 || idcode1 == _idcode_AM06 || idcode2 == _idcode_AM06 || idcode3 == _idcode_AM06 )  m_AMChip = AMChip06::global();
              else if ( idcode0 == _idcode_AM05 || idcode1 == _idcode_AM05 || idcode2 == _idcode_AM05 || idcode3 == _idcode_AM05 )  m_AMChip = AMChip05::global();
	  }

        } // end loop over the chains
      }   // end loop over the LAMBs

      /* The LAMB version is a combination of chain length and chip type: chainlength==4 is for v1,
       * chainlength==2 and AM05 is v2, chainlength 2 and AM06 is v3/v4.
       * However there are no differences in handling v2, v3 and v4 so chainlenght is enough
       */
      if (m_chainlength != 2) {
        // AA 2018-11-21 only LAMBv2 and greater are supported (m_chainlength==2)
        std::stringstream message;
        message << "ERROR: LAMBv1 is not supported by AMB online SW any longer";
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::error(ex);
        // m_LAMBVersion = LAMBv1;
      }

      //      m_LAMBVersion = LAMBv2;


      // Comparing the LAMB senses with the presence of AM chips in the LAMBs

      // ERS_LOG("LAMB mask (from presence lines) found to be 0x" << std::hex << m_LAMBPresence << std::dec << ". AMchip found from IDCODE is " << m_AMChip->getNAME() << ".");
      ERS_LOG("LAMB mask (from presence lines) found to be 0x" << std::hex << m_LAMBPresence << std::dec);
      ERS_LOG("The LAMBs who have at least one chip with AM05 or AM06 IDCODE are 0x" << std::hex << m_LAMBMap << std::dec);
      ERS_LOG("The first AMchip identified among all LAMBs (from IDCODE) is a " << m_AMChip->getNAME());

      if (m_LAMBPresence != m_LAMBMap) {

	// sleep(1); // Sleep time reduced from 100s to 1s - this can probably be completely removed. --> REMOVED !

        std::stringstream message;
        message << "ERROR: LAMB mask (from presence lines) 0x" << std::hex << m_LAMBPresence << "  from IDcodes 0x" << m_LAMBMap << std::dec;
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::error(ex);
      }

      if ( m_AMChip->getNAME() != AMChip06::global()->getNAME() ) {

        std::stringstream message;
        message << "ERROR: AMChip name is " << m_AMChip->getNAME() << "  instead of  " << AMChip06::global()->getNAME();
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::error(ex);
      }


      // std::cout << "The map of installed AM chips is 0x" << m_AMChipMap << " to be implemented...." << std::endl;



      // set DCS server semaphore green (re-enable DCS monitoring)

      ERS_LOG("Re-starting DCS monitoring after read_AM_idcodes");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0x0000);

      return;
    } // AMBoard::read_AM_idcodes()



    /** This function propagates an init state to the board.
     *
     * There ara different init levels:
     *  - 0: initialize the board, doesn't have effect on the chips, mostly sage
     *  - 1: send  initi to the LAMB, it requests to reconfigure the chips
     *  - 2: init to the FIFOs, useful to reset the states
     *  - 3: fully reconfigure the boards, requires to repeat the board initialization
     *
     * \param loop  repeat the procedure many times, -1 forever
     * \param level set the init level to be prapagated to the board
     *
     * \return a boolean to verify the init procedure succeeded
     */
    bool AMBoard::init(int level, int loop)
    {
      const struct timespec deltaT = { 0, 1000000};
      // Each init has a specif reset word to be set
      unsigned int resetWord(0);
      if      (level == 0)  resetWord = RESET_AMB;
      else if (level == 1)  resetWord = RESET_LAMB;
      else if (level == 2)  resetWord = RESET_FIFO;
      else if (level == 3)  resetWord = RESET_CONF;
      else  return false;

      for (int i = 0; i != loop; ++i) { // loop over the inits
        // set init
        m_vme->write_word(INIT_AMB, resetWord);

        nanosleep(&deltaT,0x0);

        // clear init
        m_vme->write_word(INIT_AMB, BLANK_WORD);
      } // end loop over the inits

      return true;
    }

    /** The function checks if the GTP of the HIT FPGA are aligned, mostly for internal use.
     *
     * \param mask A value interpreted bitwise representing the links expected to be aligned
     *
     * \return true if all the link are aligned, false otherwise
     *  */
    bool AMBoard::check_HIT_GTP_aligned(unsigned int HIT_active_links) {
      const struct timespec deltaT100ms = { 0, 100000000};

      unsigned int  register_content(0);
      register_content = m_vme->read_word(HITRESETDONEGTP);
      PRINT_LOG("HIT GTP reset done QUAD 216 RX: " << hex << (register_content & 0xf) << " - TX " << (register_content>>4 & 0xF));
      PRINT_LOG("HIT GTP reset done QUAD 213 RX: " << hex << (register_content>>8 & 0xf) << " - TX " << (register_content>>12 & 0xF));
      PRINT_LOG("HIT GTP reset done QUAD 116 RX: " << hex << (register_content>>16 & 0xf) << " - TX " << (register_content>>20 & 0xF));
      PRINT_LOG("HIT GTP reset done QUAD 113 RX: " << hex << (register_content>>24 & 0xf) << " - TX " << (register_content>>28 & 0xF));

      if (register_content != HIT_active_links)
        nanosleep(&deltaT100ms,0x0); // wait 100ms but don't check reste DONE again

      register_content = m_vme->read_word(HITALIGNGTP);
      PRINT_LOG("HIT GTP align \t" << hex << register_content);
      PRINT_LOG("HIT GTP align QUAD216 \t" << hex << ((register_content>>2) &0x1) << " - " << ((register_content>>1)&0x1) << " - " << (register_content&0x1));
      PRINT_LOG("HIT GTP align QUAD213 \t" << hex << ((register_content>>7) &0x1) << " - " << ((register_content>>6)&0x1) << " - " << ((register_content>>5)&0x1));
      PRINT_LOG("HIT GTP align QUAD116 \t" << hex << ((register_content>>11) &0x1) << " - " << ((register_content>>10) & 0x1) << " - " << ((register_content>>8)&0x1));
      PRINT_LOG("HIT GTP align QUAD113 \t" << hex << ((register_content>>15) &0x1) << " - " << ((register_content>>13)&0x1) << " - " << ((register_content>>12)&0x1));

      return ( (register_content & HIT_active_links) == HIT_active_links);
    }

    /**
     * Checks AM board ROAD GTP aligned status against ROAD_active_links parameter
     */
    unsigned int AMBoard::check_ROAD_GTP_aligned(unsigned int ROAD_active_links) {
      const struct timespec deltaT100ms = { 0, 100000000};

      unsigned int register_content(0);

      register_content = m_vme->read_word(ROADRESETDONEGTP);
      PRINT_LOG("ROAD GTP reset done QUAD 216 RX: "<< hex << (register_content & 0xf) <<" - TX "<< (register_content>>4 & 0xF));
      PRINT_LOG("ROAD GTP reset done QUAD 213 RX: "<< hex << (register_content>>8 & 0xf) <<" - TX "<< (register_content>>12 & 0xF));
      PRINT_LOG("ROAD GTP reset done QUAD 116 RX: "<< hex << (register_content>>16 & 0xf) <<" - TX "<< (register_content>>20 & 0xF));
      PRINT_LOG("ROAD GTP reset done QUAD 113 RX: "<< hex << (register_content>>24 & 0xf) <<" - TX "<< (register_content>>28 & 0xF));

      if (register_content != ROAD_active_links)
        nanosleep(&deltaT100ms,0x0); // wait 100ms but don't check reste DONE again

      register_content = m_vme->read_word(ROADALIGNGTP);
      PRINT_LOG("ROAD GTP align QUAD216 \t" << hex << (register_content & 0xf) );
      PRINT_LOG("ROAD GTP align QUAD213 \t" << hex << (register_content>>4 &0xf) );
      PRINT_LOG("ROAD GTP align QUAD116 \t" << hex << (register_content>>8 &0xf) );
      PRINT_LOG("ROAD GTP align QUAD113 \t" << hex << (register_content>>12 &0xf) );

      //      return ( (register_content & ROAD_active_links) == ROAD_active_links);
      return register_content;
    }

    /** This function reconfigure the GTPs on the board. The function returns true
     *
     * The code first attempts to align HIT, then aligns the ROAD FPGA. If the HIT alignement
     * fails the ROAD chip cannot be configure. The bahavior is different if a specific
     * FPGA has to be aligned.
     *
     * \param max_tries if the GTPs are not aligned this in the number of attempts that will be done
     * \param force if true at the first attempt force the link reconfiguration
     * \param mode  Possible values are: 0x3, resets HIT and ROAD, 0x1 resets only HIT, 0x2 only ROAD
     *
     *  \return 0 in case of success, 1 if hit is not aligned, 2 if road is not aligned, 3 if both are not
     */
    int AMBoard::reset_gtp(int max_tries, bool force, int mode) {
      const struct timespec deltaT1s = { 1, 0};
      const struct timespec deltaT1ms = { 0, 1000000};

      // Copy mode variable into reset_res
      // reset_res(reset_result) stores in two bits if  HIT and ROAD need to be reset.
      // As soon as HIT or ROAD are successfully reset the corresponding bit is zeroed.
      int reset_res(mode);

      // first: HIT reset
      if (reset_res & 0x1) {
        //TODO: VARIABLES TO BE READ FROM BOARD SETUP
        const int HIT_which_gtps = 0xF; //0x1 means all GTPS (to be confirmed)
        const int HIT_active_links = 0xbde7; //0xbde7 are the 12 HIT input links

        // initialize the control variables
        bool HIT_GTP_OK  = false;

        for (int iTries=0; iTries<max_tries; iTries++) { // loop over the tries
          PRINT_LOG("HIT reset, GTP reset try #" << iTries << " / " << max_tries );

          HIT_GTP_OK  = check_HIT_GTP_aligned(HIT_active_links) && (!force || iTries!=0);


          if (!HIT_GTP_OK) { // if the GTPs are no aligned, reconfgire them
            // remove HIT GTPs reset
            m_vme->write_check_word(RESET_GTP_HIT, 0x0);
            nanosleep(&deltaT1ms,0x0);
            // apply HIT GTPs reset
            m_vme->write_check_word(RESET_GTP_HIT, HIT_which_gtps);
            nanosleep(&deltaT1ms,0x0);
            // remove HIT GTPs reset
            m_vme->write_check_word(RESET_GTP_HIT, 0x0);
            nanosleep(&deltaT1s,0x0);
          }
          else {
            PRINT_LOG("HIT GTPs already aligned");
            reset_res &= (~0x1);
            break;
          }
        }
      }

      // second: the ROAD reset
      if (reset_res & 0x2) {

        // LAMB OUTLINKs MASKs, they are related to FW and board design details
        const int LAMB_MASK0 = 0xD040;
        const int LAMB_MASK1 = 0x003C;
        const int LAMB_MASK2 = 0x0303;
        const int LAMB_MASK3 = 0x2C80;

        /* combine the map of the active LAMBs and the previous contants to build
         * the reference value related to successfull alignment of the ROAD links
         */
        int ROAD_active_links = 0;
        if (m_LAMBMap & 0x1) ROAD_active_links |= LAMB_MASK0;
        if (m_LAMBMap & 0x2) ROAD_active_links |= LAMB_MASK1;
        if (m_LAMBMap & 0x4) ROAD_active_links |= LAMB_MASK2;
        if (m_LAMBMap & 0x8) ROAD_active_links |= LAMB_MASK3;

        bool ROAD_GTP_OK = false;

        for (int iTries=0; iTries<max_tries; iTries++) { // loop over the tries
          PRINT_LOG("ROAD reset, GTP reset try # " << iTries << " / " << max_tries);

          // flag for the eventual reset of the GTPs
          int ROAD_which_gtps = 0;

          // check the status, if they aren't ok
          unsigned int register_content = check_ROAD_GTP_aligned(ROAD_active_links);
          if (force && iTries==0) register_content = 0; // force the value of the register content to cause full reset

          ROAD_GTP_OK = ( ( register_content & ROAD_active_links) == (u_int)ROAD_active_links );

          PRINT_LOG(ROAD_GTP_OK << " " << hex << register_content << " " << ROAD_active_links << dec);

          // check the GTP quads to be reset
          for (int kk=0; kk<4; kk++) { // loop over the quads
            // The reset bit is set if the 4 bits related to the quad are equal to
            // the refence value in the ROAD_active_links word
            if ( (((register_content&ROAD_active_links)>>(4*kk))&0xF) != ((ROAD_active_links>>(4*kk))&0xF) )
              ROAD_which_gtps |= 1<<kk; // set the bit for the reset
          } // end loop over the quads


          if (!ROAD_GTP_OK) {
            PRINT_LOG("GTP BLOCK TO RESET = " << hex << ROAD_which_gtps << dec);

            // remove GTPs reset
            m_vme->write_check_word(RESET_GTP_ROAD, 0x0);
            nanosleep(&deltaT1ms,0x0);

            // invert ROAD GTP reset
            unsigned int tmp = ROAD_which_gtps;
            ROAD_which_gtps = 0;
            if (tmp&0x8) ROAD_which_gtps |= 1;
            if (tmp&0x4) ROAD_which_gtps |= 2;
            if (tmp&0x2) ROAD_which_gtps |= 4;
            if (tmp&0x1) ROAD_which_gtps |= 8;

            // apply GTPs reset
            m_vme->write_check_word(RESET_GTP_ROAD, ROAD_which_gtps);
            nanosleep(&deltaT1ms,0x0);
            // remove GTPs reset
            m_vme->write_check_word(RESET_GTP_ROAD, 0x0);
            nanosleep(&deltaT1s,0x0);

          }
          else {
            PRINT_LOG("ROAD GTPs already aligned");
            reset_res &= (~0x2);
            break;
          }
        }

        //return 0;
      }

      return reset_res;
    }

    /** The initialization to read the bank requires to set
     * * the number of layers, probably just always set to 8
     * * the DC setup in the bank and in the chips
     * */
    int AMBoard::readPatternBankDCconfig(int type, std::string pattfname,
        std::vector<uint32_t> &ndc) {

      m_PatternBank.setNLayers(NLAYERS);
      if (ndc.size()>0) {
        // if the NDC in the bank is passed the value is set before the the file is read
        // the file may override this vaulue if a DC configuration blcok
        // is found in the file itself
        for (int il=0;il!=NLAYERS;++il) m_PatternBank.setDCConfig(ndc);
      }
      int res;
      std::ifstream patternfile(pattfname.c_str());

      if(patternfile){
        res = m_PatternBank.loadPatternBankDCconfig(pattfname.c_str(), type);
        switch(res){
          case 0:
            if(getVerbosity()<=AMBoard::INFO)
              PRINT_LOG("Pattern bank correctly loaded in memory and DCconfig stored.");
            break;
          case -1:
            if(getVerbosity()<=AMBoard::ERROR)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBank(): Error while loading bank in memory.";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }
            break;
          case -2:
            if(getVerbosity()<=AMBoard::INFO)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBankDCconfig(): No SSMap config found in ROOT pbank file -> default bank DCconfig set [3,3,3,3,3,3,3,3].";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }
            break;
          default:
            if(getVerbosity()<=AMBoard::WARNING)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBank(): Warning while loading bank in memory: unknow error code";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }
            break;
        }

      } else {
        res = -1;
        if(getVerbosity()<=AMBoard::ERROR)
        {
          std::stringstream message;
          message << "AMBoard::readPatternBankDCconfig(): Pattern bank file does not exist";
          daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
          ers::error(ex);
        }
      }
      patternfile.close();          
      return res;
    }

    /** The initialization to read the bank requires to set
     * * the number of layers, probably just always set to 8
     * * the number of patterns we want to read, question: what we if the setup requires for more that the available patterns?
     * * the DC setup in the bank and in the chips, the first can be made available in the bank itself, but now is not, the
     *   second will always be available
     * */
    int AMBoard::readPatternBank(int type, std::string pattfname, bool SSOffset,
        std::vector<uint32_t> &ndc) {
      const int NPattsPerChip = m_AMChip->getMAXPATT_PERCHIP();

      m_PatternBank.setNLayers(NLAYERS);
      for (int il=0;il!=NLAYERS;++il) m_PatternBank.setSSOffset(il,8*SSOffset);

      if (ndc.size()!=0) {
        // if the NDC in the bank is passed the value is set before the the file is read
        // the file may override this vaulue if a DC configuration blcok
        // is found in the file itself
        for (int il=0;il!=NLAYERS;++il) m_PatternBank.setDCConfig(ndc);
      }
      std::ifstream patternfile(pattfname.c_str());
      int res;
      if(patternfile){
        res = m_PatternBank.loadPatternBank(pattfname.c_str(), type, MAXCOLUMNS*MAXCHIP_PER_VMECOL*NPattsPerChip);
        switch(res){
          case 0:
            if(getVerbosity()<=AMBoard::INFO)
              PRINT_LOG("Pattern bank correctly loaded in memory.");
            break;
          case -1:
            if(getVerbosity()<=AMBoard::ERROR)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBank(): Error while loading bank in memory.";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }
            break;
          case -2:
            if(getVerbosity()<=AMBoard::ERROR)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBank(): Error while loading bank in memory: the number of requested patterns cannot be loaded";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }  
            break;
          case -3:
            if(getVerbosity()<=AMBoard::ERROR)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBank(): Error while loading bank in memory: the format cannot be read";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }
            break;
          default:
            if(getVerbosity()<=AMBoard::WARNING)
	      {
	        std::stringstream message;
	        message << "AMBoard::readPatternBank(): Warning while loading bank in memory: unknow error code";
	        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	        ers::warning(ex);
	      }
            break;
        }

        if (m_OverrideSpecialPattern) {
          /* the pattern bank read from the disk is expected to have special dummy patterns
           * in positions that correspond to the first location of each chip. Those patterns
           * may not have the exact SS value that is expected and because of this a special
           * value will be set
           */
          const int off = SSOffset ? 8 : 0;
          m_PatternBank.overrideSpecialPattern(m_WCSS-off, NPattsPerChip);
        }
      } else {
        res = -1;
        if(getVerbosity()<=AMBoard::ERROR)
        {
          std::stringstream message;
          message << "AMBoard::readPatternBank(): Pattern bank file does not exist";
          daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
          ers::error(ex);
        }
      }
      patternfile.close();

      return res;
    }

    /** This method prepares a random pattern bank to be laoded into the board */
    int AMBoard::prepareRandomPatternBank(bool SSOffset) {
      const int NPattsPerChip = m_AMChip->getMAXPATT_PERCHIP();

      m_PatternBank.setNLayers(NLAYERS);
      for (int il=0;il!=NLAYERS;++il) m_PatternBank.setSSOffset(il,8*SSOffset);

      int res = m_PatternBank.createRandomBank(MAXCOLUMNS*MAXCHIP_PER_VMECOL*NPattsPerChip, 0);
      switch (res){
        case 0:
          if(getVerbosity()<=AMBoard::INFO)
            PRINT_LOG("Pattern bank correctly generated and loadaed in memory.");
          break;
        case -1:
          if(getVerbosity()<=AMBoard::ERROR)
            PRINT_LOG("ambslp_amchip_writepatterns(): Error while creating random bank.");
          break;
      }

      if (m_OverrideSpecialPattern) {
        m_PatternBank.overrideSpecialPattern(m_WCSS, NPattsPerChip);
      }

      return res;
    }

    /** the method prepares a sequential pattern bank */
    int AMBoard::prepareSequentialBank(int maxSS, bool SSOffset) {
      const int NPattsPerChip = m_AMChip->getMAXPATT_PERCHIP();

      m_PatternBank.setNLayers(NLAYERS);
      for (int il=0;il!=NLAYERS;++il) m_PatternBank.setSSOffset(il,8*SSOffset);

      int res = m_PatternBank.createSequentialBank(MAXCOLUMNS*MAXCHIP_PER_VMECOL*NPattsPerChip, maxSS);
      switch (res){
        case 0:
          if(getVerbosity()<=AMBoard::INFO)
            PRINT_LOG("Pattern bank correctly generated and loadaed in memory.");
          break;
        case -1:
          if(getVerbosity()<=AMBoard::ERROR)
            PRINT_LOG("Error while creating the sequential bank.");
          break;
      }

      if (m_OverrideSpecialPattern) {
        m_PatternBank.overrideSpecialPattern(m_WCSS, NPattsPerChip);
      }

      return res;
    }


    /** The method loads a patterns bank, according the parameters that describe the jtag part
     * int the board. The pattern bank should have been already prepared for this goal
     */
    //FIXME the method still uses the old VME API
    bool AMBoard::writepatterns(int nPattsPerChip, int offset, bool verbose, bool test_written_pattern, int test_interval, bool write_pattern) {

      if(!test_written_pattern&&!write_pattern) return true;

      int active_columns =0;
      int chipno = 0;

      const int chipnum((1<<m_chainlength)-1);

      const int NMaxPattsPerChip = m_AMChip->getMAXPATT_PERCHIP();

      std::array<uint32_t,MAXDATAWORDS> vmedata;
      std::array<uint32_t,MAXDATAWORDS> outdata;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      chipno = (m_chainlength<<8) + chipnum;

      //**********************************************************************
      GotoTestReset(m_slot); // TODO should receive m_vme
      GotoIdlefromReset(m_slot, all_chipcolumn); // TODO should receive m_vme

      // dummy variable that holds the patterns while retrieving the information
      // data to be sent to the JTAG
      std::vector<std::vector<uint32_t>> patt_block;
      patt_block.reserve(MAXAMCHIPS);
      patt_block.clear();

      // flag that check if a pattern is valid, used for matching, or not
      std::array<bool,MAXCHIP_PER_VMECOL*MAXCOLUMNS> pattern_valid;

      // prepare an offset for the position of the patterns into the chips
      unsigned int ipatt_writeoffset(AMB_PATTERNX_ADDRESS_VALUE);
      int ipattS = (nPattsPerChip+19)/20;

      auto begin = std::chrono::high_resolution_clock::now();                                                                                                          
      if (write_pattern) {
        // First of all, reset the checksum register
        m_vme->write_check_word(AMB_VME_BANKCHECKSUM, 0x0);

      // Enable the procedure that writes a special pattern in position 0 of each chip
      if (m_AddTestPattern) {
        // write a dummy pattern as first patterns
        PRINT_LOG("Write a special test pattern in location " << AMB_PATTERNX_ADDRESS_VALUE);
        std::vector<uint32_t> curpatt(NLAYERS);
        for(int icolumn=0; icolumn<MAXCOLUMNS; icolumn++){ // loop over the JTAG columns
          for(int ichip=0; ichip<MAXCHIP_PER_VMECOL; ichip++){ // loop over the chips in a column
            // create the pseudo pattern
            for (unsigned int il=0;il!=8;++il) curpatt[il] = FTKPatternBank::convertSSAdjustTernaryBits(m_WCSS,0,m_PatternBank.getChipDCBits(il)); //TODO make the DC number 2 a variable

            // set the pattern valid word
            pattern_valid[(icolumn*MAXCHIP_PER_VMECOL)+ichip]=true;

            // copy the data in the patt_block structure to be translated into VME words later
            patt_block.push_back(curpatt);
          } // end loop over the chips within a column
        }

        // prepare the data to be sent to the to the VME bus
        //        if(m_LAMBVersion==LAMBv1) pattern2vmedata_v1(vmedata.data(), patt_block, pattern_valid.data(), verbose);
        //        else
        pattern2vmedata(vmedata.data(), patt_block, pattern_valid.data(), verbose);

        // start write one pattern in all the chips
        write_single_pattern(m_slot, m_chainlength, chipnum, active_columns, ipatt_writeoffset, vmedata.data());

        patt_block.clear();
        // end test pattern write

        ipatt_writeoffset += 1; // increment the offset for the location where the pattern is
      }

        std::vector<uint32_t> curpatt(NLAYERS);
        for(int ipatt=offset; ipatt<(offset+nPattsPerChip); ipatt++) {// loop over the patterns

          if ((ipatt%ipattS)==0) {PRINT_LOG("Writing pattern " << ipatt << " of " << nPattsPerChip);}

          // The code writes a single pattern in all chips at each loop
          for(int icolumn=0; icolumn<MAXCOLUMNS; icolumn++){ // loop over the JTAG columns
            for(int ichip=0; ichip<MAXCHIP_PER_VMECOL; ichip++){ // loop over the chips in a column

              // collect the data of the pattern, check if the pattern is valid and eventually update the falg
              int res = m_PatternBank.getPattern(ipatt+(ichip*NMaxPattsPerChip)+(icolumn*MAXCHIP_PER_VMECOL*NMaxPattsPerChip), curpatt);
              switch(res){
                case 0:
                  pattern_valid[(icolumn*MAXCHIP_PER_VMECOL)+ichip]=true;
                  break;
                case -1:
                  pattern_valid[(icolumn*MAXCHIP_PER_VMECOL)+ichip]=false;
                  break;
              }            
              if(ipatt<2 || (icolumn == 0 && ichip == 0 && (ipatt<100 || ipatt%1024==0))) {
                if(getVerbosity()<=AMBoard::DEBUG){ 
                  std::cout << "ipatt=" << ipatt <<"\t icolumn=" << icolumn << " \tichip=" << ichip << " " << std::hex;
                  for(int bbb=0;bbb<(int)NLAYERS;bbb++){ std::cout<<"0x"<<curpatt[bbb]<<"\t";} std::cout<<std::dec<<std::endl;
                }
              }

              // copy the data in the patt_block structure to be translated into VME words later
              patt_block.push_back(curpatt);

            } // end loop over the chips within a column
          } // end loop over the columns

          // collect the data to be send thorugh the VME

          //          if(m_LAMBVersion==LAMBv1) pattern2vmedata_v1(vmedata.data(), patt_block, pattern_valid.data(), verbose);
          //          else 
          pattern2vmedata(vmedata.data(), patt_block, pattern_valid.data(), verbose);

          // start write a single pattern in all the chips
          //if(ipatt==offset) write_single_pattern(m_slot, m_chainlength, chipnum, active_columns, ipatt+ipatt_writeoffset, vmedata, write_pattern);
          if((ipatt%64)==offset) write_single_pattern(m_slot, m_chainlength, chipnum, active_columns, ipatt+ipatt_writeoffset, vmedata.data(), write_pattern);
          else write_single_pattern(m_slot, m_chainlength, chipnum, active_columns, -1, vmedata.data(), write_pattern);

          patt_block.clear();
        } // end loop over the patterns
	 ///send init event via jtag to clean possible spurious matches
	ConfigureScam(m_slot, chipno, active_columns, INIT_EVop, 1); 
      }

      auto middle = std::chrono::high_resolution_clock::now();                                                                                                            

      int nchecked_patterns=0;
      double max_prob=0;
      uint32_t num_bad_patterns[MAXAMCHIPS] = {0}; 
      int total_num_bad_patterns = 0;
      std::vector<std::vector<am_rec_address*>*> bad_chips;
      int check_offset = rand() % test_interval;

      // Start test of AM06 patterns
      if(test_written_pattern){
        srand (time(NULL));
        ERS_LOG("Pattern address offset for the check of written patterns (check_offset): " << check_offset
                << "  interval among checked patterns (test_interval): " << test_interval);

        std::vector<uint32_t> curpatt(NLAYERS);
        int npatt_to_be_checked  = m_AMChip->getMAXPATT_PERCHIP()/test_interval;
        int npatt_div10 = (npatt_to_be_checked/10==0) ? 1 : npatt_to_be_checked/10;
        PRINT_LOG("Checking written patterns: "<<npatt_to_be_checked<<" patterns are to be checked.");
        for(int ipatt=offset; ipatt<(offset+nPattsPerChip); ipatt++) {// loop over the patterns
	  
          if( !((ipatt-offset-check_offset)%(test_interval) == 0) ) continue;

          if(nchecked_patterns%npatt_div10==0) {PRINT_LOG(nchecked_patterns<<" patterns checked.");}

 	  //if( !( (ipatt==0x2515) || (ipatt==0x2555) || (ipatt==0xf60b) || (ipatt==0xf68f) || (ipatt==0x18cca) || (ipatt==0x1e6b4) ) ) continue;

          for(int icolumn=0; icolumn<MAXCOLUMNS; icolumn++){ // loop over the JTAG columns
            for(int ichip=0; ichip<MAXCHIP_PER_VMECOL; ichip++){ // loop over the chips in a column

              // collect the data of the pattern, check if the pattern is valid and eventually update the falg
              int res = m_PatternBank.getPattern(ipatt+(ichip*NMaxPattsPerChip)+(icolumn*MAXCHIP_PER_VMECOL*NMaxPattsPerChip), curpatt);
              switch(res){
                case 0:
                  pattern_valid[(icolumn*MAXCHIP_PER_VMECOL)+ichip]=true;
                  break;
                case -1:
                  pattern_valid[(icolumn*MAXCHIP_PER_VMECOL)+ichip]=false;
                  break;
              }
              // copy the data in the patt_block structure to be translated into VME words later
              patt_block.push_back(curpatt);

            } // end loop over the chips within a column
          } // end loop over the columns

          // collect the data to be send thorugh the VME

          //          if(m_LAMBVersion==LAMBv1) pattern2vmedata_v1(vmedata.data(), patt_block, pattern_valid.data(), verbose);
          //          else 
          pattern2vmedata(vmedata.data(), patt_block, pattern_valid.data(), verbose);

          // check patterns every post_check_offset address
          bool is_ok(false);
          std::array<bool,MAXAMCHIPS> result{false};
          std::array<uint32_t,MAXAMCHIPS> rec_address{0};

          is_ok = test_pattern(m_slot, m_chainlength, chipnum, active_columns, ipatt+ipatt_writeoffset, patt_block, pattern_valid.data(), result.data(), rec_address.data(), verbose);
          if(!is_ok){
            // do we have a memory leak here?
	    std::vector<am_rec_address*> * v_ara = new std::vector<am_rec_address*>; 
	    v_ara->clear();
            for (int i_badchip=0; i_badchip<MAXAMCHIPS; i_badchip++){
              if(!result[i_badchip]) {
		am_rec_address * ara = new am_rec_address();
		ara->index = i_badchip;
		ara->correct_address = ipatt+ipatt_writeoffset;
		ara->hitmap = (rec_address[i_badchip]>>18)&0xff;
		ara->rec_address = (rec_address[i_badchip]>>1)&0xffff;
		ara->valid = rec_address[i_badchip]&0x1;
		v_ara->push_back(ara);
		++num_bad_patterns[i_badchip];
                total_num_bad_patterns++;
              }
            }
	    bad_chips.push_back(v_ara);
          }
          nchecked_patterns++;
          patt_block.clear();
        } // end loop over the patterns
	
	for(int i_bc=0; i_bc<bad_chips.size() && i_bc<100; i_bc++){ // print up to the first 100 addresses with errors
	  for(int i_chip=0; i_chip<(bad_chips.at(i_bc))->size(); i_chip++){
	    ERS_LOG("Problem detected with AMchip " << \
		    ((bad_chips.at(i_bc))->at(i_chip))->index << std::hex << \
		    " for address 0x"    << ((bad_chips.at(i_bc))->at(i_chip))->correct_address << \
		    ": hitmap = 0x"      << ((bad_chips.at(i_bc))->at(i_chip))->hitmap << \
		    ", rec_address = 0x" << ((bad_chips.at(i_bc))->at(i_chip))->rec_address << \
		    ", valid = 0x"       << ((bad_chips.at(i_bc))->at(i_chip))->valid << std::dec);
	  }
	}
       
	for(int i=0; i<MAXAMCHIPS; i++){if(num_bad_patterns[i] > max_prob) max_prob = num_bad_patterns[i];}
        
        // we shall delete bad_chips and the contained vectors here.
      }

      auto end = std::chrono::high_resolution_clock::now();
      ERS_LOG("time for writepatterns (middle - begin) = "<<std::chrono::duration_cast<std::chrono::seconds>(middle - begin).count() << " s"); 
      ERS_LOG("time for writepatterns (end - middle) = "<<std::chrono::duration_cast<std::chrono::seconds>(end - middle).count() << " s");

      if (getVerbosity()<=AMBoard::INFO) std::clog << "INFO: ambslp_writepatterns(): going to write 0 in JPATTDATA register" << std::endl;
      for (int g=0; g<MAXDATAWORDS;g++) vmedata[g]=0x00000000;

      ConfigureRegister(m_slot, chipno, active_columns, JPATT_DATA, vmedata.data(), DATA_SIZE, outdata.data());
      ConfigureRegister(m_slot, chipno, active_columns, JPATT_DATArd, vmedata.data(), DATA_SIZE, outdata.data(), true);

      if (write_pattern) { // write the final checksum into the board
        m_vme->write_check_word( AMB_VME_BANKCHECKSUM, m_PatternBank.getChecksum() );
        ERS_LOG("AM board in slot " << m_slot << " register checksum updated to value 0x" << m_PatternBank.getChecksum() 
                << " after loading patterns from: " << m_patternBankFileName );
      }

      if(test_written_pattern){
        if (total_num_bad_patterns<=0) { // in case all checked patterns are good
          ERS_LOG("AM board in slot " << m_slot << \
		  ". All patterns checked are GOOD. Patterns checked with parameters test_interval=" << test_interval << \
		  " check_offset=" <<  check_offset << " nchecked_patterns=" << nchecked_patterns << "." );
        } else { // in case at least one pattern is not correctly written in the AM ASIC
	  std::stringstream message; // message to go with a warning or error depending of max fraction of errors on one AM06
          message << "Bad patterns are written in AM chips (chip with most bad patterns has " << max_prob << " bad addresses). AMBoard in slot " << m_slot 
                  << ". Patterns checked with parameters test_interval=" << test_interval << " check_offset=" <<  check_offset
                  << " nchecked_patterns=" << nchecked_patterns << ".";          

	  // [TEMPORARY] Exceptions for failed pattern check NOT issued  [+++++]
	  // daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());

          // print a LOG with errors for each AM06 before issuing warning or error
          std::stringstream log_message;
          log_message << message.str(); // re-use the start of message that will go in the warning or error
          log_message << std::endl << "AM06 number - number of patterns with errors - fraction of patterns with errors / nchecked_patterns --" << std::endl;
          for(int i=0; i<MAXAMCHIPS; i++) {
            if ( (i%MAXCHIP_PER_LAMB) == 0 ) log_message << "LAMB" << i/MAXCHIP_PER_LAMB << std::endl;
            if (i<10) log_message << " "; // do some padding for nicer output
            log_message << "#" << i << " " << num_bad_patterns[i] << " " << num_bad_patterns[i]*1. / nchecked_patterns << " -- ";
            if ( (i%4) == 3 ) log_message << std::endl; // list 4 AM06 per line
          }
          ERS_LOG(log_message.str());


	  // [TEMPORARY] Exceptions for failed pattern check NOT issued  [+++++]

          ERS_LOG("[TEMPORARY] Exceptions for failed pattern check NOT issued. (Message for the exception shown below)");
          ERS_LOG(message.str());


          // // Issue a warning or error depending of max fraction of errors on one AM06 
          // if ( ( (max_prob / static_cast<double>(nchecked_patterns)) * 100 ) >=  m_PattProbTolerance ) {
          //   ers::error(ex);
          //   return false;
          // } else {
          //   ers::warning(ex);
          // }



	} // end else if (total_num_bad_patterns<=0) {
      } // end if(test_written_pattern){

      return true;
    } // end of writepatterns



    /** @fn void write_single_pattern(int slot, int chainlength, int chipnum, int active_columns, int patt_number,unsigned int* vme_data)
     * @brief this function write a single pattern in each of the 64 chips mounted on one ambslp.
     * 
     * @param slot Crate slot in which trhe board is plugged
     * @param chainlength JTAG chain length (aka numer of chip in the chain)
     * @param active_columns JATG colums mask
     * @param pattern_number Address where oyu want to write t5he pattern
     * @param vme_data Data you want to write as given by pattern2vmedata function
     */

    void AMBoard::write_single_pattern(int slot, int chainlength, int chipnum, int active_columns, int patt_number,uint32_t *vme_data, bool write_pattern){
      //ERS_LOG("write_single_pattern()"); 
      int chipno = 0;

      chipno = (m_chainlength<<8) + chipnum;
      PRINT_DEBUG_VME("chipno"<<std::hex<<chipno);
      PRINT_DEBUG_VME("chainlength"<<std::hex<<chainlength);
      std::array<uint32_t,MAXDATAWORDS> indata;
      std::array<uint32_t,MAXDATAWORDS> outdata;
      std::array<bool,145> wrdata;
      wrdata.fill(false);

      //reset jtag state machine and go to idle
      GotoTestReset(slot);
      GotoIdlefromReset(slot, active_columns); 

      if(patt_number >= 0) {
        //address the correct memory location 
        PRINT_DEBUG_VME("Writing pattern"<<std::dec<<patt_number);
        for(unsigned i=0; i < m_AMChip->getADDR_SIZE(); i++){
          if((patt_number>>i)&0x1)indata[i] = 0xffffffff;
          else indata[i] = 0x00000000;
        }
        for (unsigned i=0; i<m_AMChip->getADDR_SIZE();i++){
          for(unsigned j=1; j<m_chainlength; j++)
            indata[i+m_AMChip->getADDR_SIZE()*j] = indata[i];
        }
        ConfigureRegister(slot, chipno, active_columns, JPATT_ADDR, indata.data(), m_AMChip->getADDR_SIZE(), outdata.data());
        ConfigureRegister(slot, chipno, active_columns, JPATT_ADDRrd, indata.data(), m_AMChip->getADDR_SIZE(), outdata.data(), true);

        if(getVerbosity()<=AMBoard::DEBUG){
          PRINT_DEBUG_VME("Reading back the addressed pattern");
          ConfigureRegister(slot, chipno, active_columns, JPATT_ADDRrd, wrdata.data(), m_AMChip->getADDR_SIZE(), outdata.data());
          PrintRegisterContent(chipno, m_AMChip->getADDR_SIZE(), outdata.data());
        }
      }
      //load the pattern in the write_pattern shift register

      if(write_pattern){
        ConfigureRegister(slot, chipno, active_columns, JPATT_DATA, vme_data, DATA_SIZE, 0);
        if(getVerbosity()<=AMBoard::DEBUG){
          std::cout<<"Reading back the pattern"<<std::endl;	
          ConfigureRegister(slot, chipno, active_columns, JPATT_DATArd, vme_data, DATA_SIZE, outdata.data(), true);
          PrintRegisterContent(chipno, DATA_SIZE, outdata.data());
        }
      }

      //write pattern and increase address
      ConfigureScam(slot, chipno, active_columns, WR_INCop, 1);
      if(getVerbosity()<=AMBoard::DEBUG){
        std::cout<<"Reading back the new pattern address"<<std::endl;
        ConfigureRegister(slot, chipno, active_columns, JPATT_ADDRrd, wrdata.data(), m_AMChip->getADDR_SIZE(), outdata.data());
        PrintRegisterContent(chipno, m_AMChip->getADDR_SIZE(), outdata.data());
      }

      return;
    }



    bool AMBoard :: disable_bank(int chainlength, int chipnum, int npatt){
      // definition of useful variables
      ERS_LOG("Disabling the pattern bank...");
      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS> indata;
      std::array<uint32_t,MAXDATAWORDS> outdata;

      std::array<bool,MAXDATAWORDS> wrdata_ir;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (int i=0, chipno=0; i<chainlength; i++)
        chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;
      //printf("chipno %x\n", chipno);  
      //printf("chainlength %x\n", chainlength);

      // First of all, reset the checksum register
      m_vme->write_check_word(AMB_VME_BANKCHECKSUM, 0x0);

      //**********************************************************************1
      //**********************************************************************
      GotoTestReset(m_slot);
      GotoIdlefromReset(m_slot, all_chipcolumn); 

      if(npatt <= 0){
        if(m_AMChip->getMAXPATT_PERCHIP()>3072) npatt = m_AMChip->getMAXPATT_PERCHIP();
        else npatt = 3072;
      }
      const int NibbleNum = 16;
      const int MiniBlockSize = 32;

      //      const int NibbleNum = 1;
      //      const int MiniBlockSize = 128*1024;
      int NBlocks = 128*1024/NibbleNum/MiniBlockSize;

      // disable all patterns up to npatt-1
      // initialized the SSID values to different values in blocks of MiniBlockSize
      for (int nibble=0; nibble<NibbleNum; nibble++) { // loop over 16 possible values for a "nibble"
        for (int block=0; block<NBlocks; block++) { // loop over blocks
          // for each block write
          // addr 0 - 511 with nibble value 0
          // addr 512 - 1023 with nibble value 1 ...
          // ...
          // addr 15*512 - 16*512-1 with nibble value 15 
          // the second block repeats the above starting from address 16*512

          int base_addr = block * NibbleNum * MiniBlockSize + MiniBlockSize*nibble;
          if (! (base_addr < npatt) )
            break; // do not initialize patterns beyond npatt

          // Set pattern address to 0 *** write in C4 ***
          for(int i=0; i < m_AMChip->getADDR_SIZE(); i++){
            if ((base_addr>>i)&0x1) wrdata_ir[i] = true;
            else wrdata_ir[i] = false;
          }

          ConfigureRegister(m_slot, chipno, active_columns, JPATT_ADDR, wrdata_ir.data(), m_AMChip->getADDR_SIZE(), outdata.data());

          for(int k=0;k<8;k++){
            int value0 = (nibble+k) % NibbleNum;
            int value1 = (nibble+k+8) % NibbleNum;
            int value2 = (nibble+k+4) % NibbleNum;
            int value3 = (nibble+k+12) % NibbleNum;
            int value = (value3 << 12) + (value2 << 8) + (value1 <<4) + value0;
            //            value = 0;
            int DR = expand_DC_bits(value,2);
            for(int r=0; r <NBITS_CAMLAYER; r++){
              if((((DR>>r)&0x1)==1))
                wrdata_ir[((k*NBITS_CAMLAYER)+r)] = true;
              else
                wrdata_ir[((k*NBITS_CAMLAYER)+r)] = false;
            }	
          }
          wrdata_ir[144] = true; //disable pattern @ majority level 
          ConfigureRegister(m_slot, chipno, active_columns, JPATT_DATA, wrdata_ir.data(), 145,0);
          ConfigureRegister(m_slot, chipno, active_columns, JPATT_DATArd, wrdata_ir.data(), 145, outdata.data(), true);
          if(getVerbosity()<=AMBoard::DEBUG) PrintRegisterContent(chipno, 145, outdata.data());
          //delete [] wrdata_ir;
          //wrdata_ir = nullptr;

          for(int i=0; i<MiniBlockSize;i++){ 
            int pattern_ID = base_addr + i;
            if (pattern_ID>=npatt)
              break;  // do not initialize patterns beyond npatt 
            ConfigureScam(m_slot, chipno, active_columns, WR_INCop, 1);
          }
        } // loop over blocks
      } // loop over nibbles

      // set the JPATT_DATA register to 0
      //bool* wrdata_ir = new bool[145];
      for(int k=0;k<8;k++)
        for(int r=0; r <NBITS_CAMLAYER; r++)
          wrdata_ir[((k*NBITS_CAMLAYER)+r)] = true;
      wrdata_ir[144] = 0;  
      std::vector<uint32_t> out;
      ConfigureRegister(m_slot, chipno, active_columns, JPATT_DATA, wrdata_ir.data(), 145, out.data());
      ConfigureRegister(m_slot, chipno, active_columns, JPATT_DATArd, wrdata_ir.data(), 145, outdata.data(), true);
      if(getVerbosity()<=AMBoard::DEBUG) PrintRegisterContent(chipno, 145, outdata.data());


      return true;
    }

    /**
     * The function tests a given checksum with the pattern bank checksum register. If
     * the checksum is valid, e.g. not 0x0, and agrees returns true, false otherwise.
     */
    bool AMBoard::checkPatternBankChecksum(unsigned int value) {
      if (!value) return false; // asking an invalid checksum

      // read the checksum of the existing bank and skip the following part
      unsigned int bank_checksum = m_vme->read_word(AMB_VME_BANKCHECKSUM);

      if (!bank_checksum) {
        ERS_LOG("Found invalid checksum at slot " << m_slot << ". Checksum value = " << bank_checksum);
        return false; // found invalid checksum in the board
      }
      //else { std::cout << "Found valid checksum at slot " << m_slot << ". Checksum value = " << bank_checksum << std::endl; }

      ERS_LOG("register checksum: 0x"<<std::hex<<bank_checksum<<", OKS checksum: 0x"<<value<<std::dec);

      if (bank_checksum != value) return false; // not matching values

      return true; // expected the same bank
    }


    //========================================================================

    void AMBoard::check_FPGA_FW_version(std::string FPGA_name) {

      int FPGA_onboard_FWver  = 0;
      int FPGA_expected_FWver = 0;

      if      (FPGA_name == std::string("VME")     )  {  FPGA_onboard_FWver = m_vme->read_word(AMB_VME_FW_VERSION);      FPGA_expected_FWver = m_vmefwver;   }
      else if (FPGA_name == std::string("CONTROL") )  {  FPGA_onboard_FWver = m_vme->read_word(AMB_CONTROL_FW_VERSION);  FPGA_expected_FWver = m_ctrlfwver;  }
      else if (FPGA_name == std::string("HIT")     )  {  FPGA_onboard_FWver = m_vme->read_word(AMB_HIT_FW_VERSION);      FPGA_expected_FWver = m_hitfwver;   }
      else if (FPGA_name == std::string("ROAD")    )  {  FPGA_onboard_FWver = m_vme->read_word(AMB_ROAD_FW_VERSION);     FPGA_expected_FWver = m_roadfwver;  }
      else if (FPGA_name == std::string("LAMB")    )  {

	m_vme -> write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_FW_VER);
	FPGA_onboard_FWver  = m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA);

	FPGA_expected_FWver = ( (m_lambfwver&0xff) | ((m_lambfwver&0xff)<<8) | ((m_lambfwver&0xff)<<16) | ((m_lambfwver&0xff)<<24));
      }

      std::stringstream message;
      message << "FTK FW Version: Board " << m_name << " FPGA " << FPGA_name << ":\t " << std::hex << FPGA_onboard_FWver;
      ers::info(daq::ftk::ftkException(ERS_HERE, name_ftk(), message.str()));

      if (FPGA_onboard_FWver != FPGA_expected_FWver) {

	daq::ftk::WrongFwVersion exception(ERS_HERE, name_ftk(), FPGA_name, 0, FPGA_onboard_FWver, FPGA_expected_FWver);

	ers::warning(exception);
      }
      return;
    }









    /**@fn void testExtraLines()

     *@brief This function is for extra lines testing. For the DTEST line it sets the DTEST signal to 0 and then reads the value on a ROAD register. The function then force the DTEST to 1 and reads it again. This process i repeted a second time, starting with the DTEST at 1. In this way FREEZE and HOLD lines are tested between AMB and AUX card. Forcing them to one and 0 from AMB and from AUX.
     *
     * @param slot Crate slot in which trhe board is plugged
     */

    int AMBoard::testExtraLines(int slot, bool verb){

      bool verbose = false;
      verbose = verb;

      int test_error = 0;
      int test_error_temp = 0x0;
      int test_error_temp_b = 0x0;
      //TEST of the DTEST line from the AM chips to ROAD FPGA

      if(verbose){ std::cout<< "------------------------------------------------------------" << std::endl;}
      if(verbose){ std::cout<< "-------------------Test of the DTEST lines------------------" << std::endl;}
      if(verbose){ std::cout<< "------------------------------------------------------------" << std::endl;}

      string serdes = "pattout"; // Selection of the PattOut Serdes 
      string DTEST_mode("none"); // Initialization of the DTEST signal mode
      bool enable(false); // Initializing enable variable for the proper AM chip serdes configuration

      for(size_t iter = 0; iter<2; iter++){ //Two oteration test: First the DTEST is set to 0 than to 1

        bool perform_test(false), expected_value(false), found_error(false); //variables initiazialization
        unsigned int lambmask(0xf); // initialization of the LAMBs mask

        if(iter == 0){ 
          DTEST_mode = "0"; // Setting the DTEST singal to 0: first iteration
          expected_value=false; // Setting the expected DTEST singal to 0: first iteration
        }
        else{
          DTEST_mode = "1"; // Setting the DTEST singal to 1: second iteration
          expected_value=true;//  Setting the expected DTEST singal to 1: second iteration
        }

        enable = 1; // Setting proper enable value

	if(verbose){ std::cout << "DTESTout: " << std::hex <<DTEST_mode <<  std::endl; } // printing DTEST mode value for crosscheck
        DTEST_select(serdes.c_str(), DTEST_mode.c_str(), enable); //setting the DTEST value in the AMchip
        lambmask = getLAMBMap(); // Getting the LAMBs mask
        perform_test=true;

        if(verbose){ std::cout << "Reading DTest values:" << std::endl; } // Reading DTEST values on every single AM chip
        uint64_t dtest_values = read_chip_DTEST();
        for(unsigned int i_lamb=0; i_lamb<MAXLAMB_PER_BOARD; i_lamb++){
          if(not ((lambmask>>i_lamb)&0x1)) continue;
          for(unsigned int i_chain=0; i_chain<MAXVMECOL_PER_LAMB; i_chain++){
            for(unsigned int i_chip=0; i_chip<MAXCHIP_PER_VMECOL; i_chip++){
              std::stringstream print_buffer;
              print_buffer << "LAMB: " << i_lamb;
              print_buffer << " CHAIN: " << i_chain;
              print_buffer << " CHIP: " << i_chip;
              bool value = ( dtest_values>>( (i_lamb*MAXCHIP_PER_LAMB)+(i_chain*MAXCHIP_PER_VMECOL)+i_chip) )&0x1;
              print_buffer << " value: " << value;
              if(verbose){ std::cout << print_buffer.str() << std::endl; }
              if(perform_test and value!=expected_value) found_error = true; //checking if the set value is the one that is read on the AM chip
            }
          }
        }

        if(found_error and perform_test) std::cout << "POSSIBLE ISSUE IN DTESTOUT!!! PLease check!" << std::endl; //error message

        unsigned int dtest_lines_road = m_vme->read_word(NO_ROAD_FROM_LAMB); //reading the DTEST signals on the ROAD register "0xXX004154"
        // The DTEST signal value is stored in the ROAD register as the or of the 4 DTEST signals coming from a chip quartet
        // This means that it the 16 bits read from ROAD will carry the information about the 16 quartet of a complete AM board
        unsigned int dtest_lamb[4];

        for(unsigned int i=0 ; i<4 ; i++){    
          dtest_lamb[i] =  ((dtest_lines_road)>>(i*4))&(0xf);  // masking the DTEST values from LAMB 0,1,2,3
          if(verbose){ std::cout<< "" << std::endl; }
          if(verbose){ std::cout<< "DTEST value read on ROAD register for LAMB"<< i << ": " << std::bitset<4>(dtest_lamb[i]) << std::endl; }
        }

        //Read DTEST value from BOUSCA FPGA register
        m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR,AMB_VME_LAMB_INDIRECT_ADDRVAL_NO_ROAD);		
        if(iter == 0){
          m_vme->write_word(AMB_VME_LAMB_INDIRECT_DATA,0x55555555);//Forcing "1" in DTEST signal with even bits
          if(verbose){ std::cout<< "" << std::endl;}
	  if(verbose){ std::cout <<"======> Forcing to 1 the DTEST signal from BOUSCA" << std::endl;}
        }
        else {  
          m_vme->write_word(AMB_VME_LAMB_INDIRECT_DATA,0xaaaaaaaa);//Forcing "0" in DTEST signal with odd bits
	  if(verbose){ std::cout<< "" << std::endl; }
          if(verbose){ std::cout <<"======> Forcing to 0 the DTEST signal from BOUSCA" << std::endl; }
        }

        dtest_lines_road = m_vme->read_word(NO_ROAD_FROM_LAMB); //reading the DTEST signals on the ROAD register "0xXX004154"
        string test_status;  

        for(int i=0 ; i < 4 ; i++){
          dtest_lamb[i] = ((dtest_lines_road)>>(i*4))&(0xf);    // masking the DTEST values from LAMB 0,1,2,3

          if(iter == 0){
            if(dtest_lamb[i] == 0xf){ //Checking if DTEST are 1 in order to determine the test status
              test_status = "OK";
            }
            else{
              test_status = "MISMATCH";
              test_error = -1;
              test_error_temp = 1;
            }
          }
          else{
            if(dtest_lamb[i] == 0x0){ //Checking if DTEST are 0 in order to determine the test status
              test_status = "OK";
            }
            else{
              test_status = "MISMATCH";
              test_error = -1;
              test_error_temp = 1;
            } 
          }
          //DTest lines test summary
          if(verbose){ std::cout<< "DTEST value read on ROAD register for LAMB"<< i << ": " << std::bitset<4>(dtest_lamb[i]) << " Line Status ----> " << test_status << std::endl; }
          if(verbose){ std::cout << std::endl;}
        }
        if(test_error_temp == 0) test_error_temp_b = ((test_error_temp_b)<<1)|0x1; //Updating bits for the total test summary
        if(test_error_temp == 1) test_error_temp_b = ((test_error_temp_b)<<1)|0x0;
        test_error_temp = 0;
      }

      //----------------------------------- Test of the HOLD lines begin------------------------------------------
      //The test disables the HOLD from AUX and checks if the HOLD status in AMB is indeed 0. Then the test forces 1 form AUX and checks the status on the aMB regs
      //The same procedure is repeted from the AMB side

      VMEInterface *input1=VMEManager::global().aux(slot,1);  //decalring vme interfaces --- aux inputs and processors
      VMEInterface *input2=VMEManager::global().aux(slot,2);
      VMEInterface *proc3=VMEManager::global().aux(slot,3);
      VMEInterface *proc4=VMEManager::global().aux(slot,4);
      VMEInterface *proc5=VMEManager::global().aux(slot,5);
      VMEInterface *proc6=VMEManager::global().aux(slot,6);

      VMEInterface *proc[4]; //putting processors interfaces in array

      proc[0] = proc3;
      proc[1] = proc4;
      proc[2] = proc5;
      proc[3] = proc6;

      unsigned int amb_hold_reg_content ;
      unsigned int aux_hold_reg_content_1;
      unsigned int aux_hold_reg_content_2 ;

      if(verbose){
	std::cout<< "" << std::endl;
	std::cout<< "------------------------------------------------------------" << std::endl;
	std::cout<< "-------------------Test of the HOLD lines-------------------" << std::endl;
	std::cout<< "------------------------------------------------------------" << std::endl;
      }

      for(unsigned int iter= 0; iter<2 ; iter++){

        //----Forcing HOLD from AMB---
        if(iter == 0){ //Forcing 0
          m_vme->write_word(DEBUG_DISABLE_HOLD_HIT,0x00000fff);             //Disabling HOLD lines to force them to 0
          m_vme->write_word(DEBUG_FORCE_HOLD_HIT,0x00000000);               //Putting down the Force HOLD
          amb_hold_reg_content = m_vme->read_word(DEBUG_DISABLE_HOLD_HIT);  //Reading the AMB register for the HOLD status
          aux_hold_reg_content_1 = input1->read_word(HOLD_STATUS);          //Reading the two AUX inputs FPGA HOLD status regiter
          aux_hold_reg_content_2 = input2->read_word(HOLD_STATUS);
	  if(verbose){ std::cout<< "Forcing HOLD lines to 0 from AMB to AUX " << std::endl; }
          //std::cout<<"Reading AMB HOLD DISABLE register: "<<std::bitset<12>(amb_hold_reg_content)<< std::endl;
        }
        else{//Forcing 1
          m_vme->write_word(DEBUG_DISABLE_HOLD_HIT,0x00000000);
          m_vme->write_word(DEBUG_FORCE_HOLD_HIT,0x00000fff);
          amb_hold_reg_content = m_vme->read_word(DEBUG_FORCE_HOLD_HIT);
          aux_hold_reg_content_1 = input1->read_word(HOLD_STATUS);
          aux_hold_reg_content_2 = input2->read_word(HOLD_STATUS);
          if(verbose){ std::cout<< "Forcing HOLD lines to 1 from AMB to AUX " << std::endl; }
          //std::cout<<"Reading AMB HOLD FORCE register: "<<std::bitset<12>(amb_hold_reg_content)<< std::endl;
        }

        unsigned int aux_reg_content_tot = 0x0;
        aux_reg_content_tot = ( aux_reg_content_tot << 8) |  aux_hold_reg_content_1; //putting all the AMB to AUX HOLD lines bits together
        aux_reg_content_tot = ( aux_reg_content_tot << 4) |  aux_hold_reg_content_2;

	if(verbose){  
	  std::cout<<"------------------------AMB Input Links-------------------------------"<< std::endl; //Comparing the AUX and AMB status bits
	  std::cout<<"AUX->AMB Link Number"; 
	  for(unsigned int i = 0; i<12 ; i++){
	    if(i< 10) std::cout<<"|  "<< i << " |";
	    else std::cout<<"| "<< std::dec << i << " |";
	  }
	  std::cout << std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"     AMB HOLD       ";
	}

        for(unsigned int i = 0; i<12 ; i++){
          unsigned int link_n;
          if(iter == 0) link_n = not(((amb_hold_reg_content)>>i)&0x1); //not of the disable bits for the 0 force test
          else link_n = ((amb_hold_reg_content)>>i)&0x1;               //taking the HOLD force for the 1 force test
          if(verbose){ std::cout<< "|  " << link_n << " |" ; }
        }

	if(verbose){ 
	  std::cout << std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"     AUX HOLD       ";
	}
        for(unsigned int i = 0; i<12 ; i++){
          unsigned int link_n = ((aux_reg_content_tot)>>i)&0x1; // AUX HOLD lines status for comparison
          if(verbose){ std::cout<< "|  " << link_n << " |" ; }           
        }

        test_error_temp = 0;
	if(verbose){ 
	  std::cout << std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"   LINE STATUS      ";
	}
        for(unsigned int i = 0; i<12 ; i++){
          if(iter == 0){
            if( not(((amb_hold_reg_content)>>i)&0x1) == (((aux_reg_content_tot)>>i)&0x1) ){ //status comparison
              if(verbose){ std::cout<< "| OK |"; }
            }
            else {
              if(verbose){ std::cout<< "| MIS|"; }
              test_error = -1;         //updating test status
              test_error_temp = 2;              
            }
          }
          else{
            if( (((amb_hold_reg_content)>>i)&0x1) == (((aux_reg_content_tot)>>i)&0x1) ){ //status comparison
              if(verbose){ std::cout<< "| OK |"; }
            }
            else {
              if(verbose){ std::cout<< "| MIS|"; }
              test_error = -1;
              test_error_temp = 2;
            }
          }
        }

        if(test_error_temp < 2 && verbose) std::cout<<"---------->PASSED"<<std::endl;  
        if(test_error_temp == 2 && verbose) std::cout<<"---------->NOT PASSED"<<std::endl;  
        if(verbose){ std::cout << std::endl; }

        if(test_error_temp < 2) test_error_temp_b = ((test_error_temp_b)<<1)|0x1;  // updating test status
        if(test_error_temp == 2) test_error_temp_b = ((test_error_temp_b)<<1)|0x0;

        // std::cout<<"Reading AUX HOLD 1 register: "<<std::bitset<12>(aux_hold_reg_content_1)<<std::endl;
        // std::cout<<"Reading AUX HOLD 2 register: "<<std::bitset<12>(aux_hold_reg_content_2)<<std::endl;
      }

      m_vme->write_word(DEBUG_FORCE_HOLD_HIT,0x0);

      //forcing HOLD from AUX
      for(unsigned int iter = 0; iter< 2; iter++){

        unsigned int aux_hold_reg_content_3;
        unsigned int aux_hold_reg_content_4;
        unsigned int aux_hold_reg_content_5;
        unsigned int aux_hold_reg_content_6;

        if(iter == 0){
          //disabling HOLD form AUX
          if(verbose){ std::cout<< "Forcing HOLD lines to 0 from AUX to AMB " << std::endl; }
          for(unsigned int i = 0; i<4; i++){
            proc[i]->write_word(0x0000003c,0x1);
            proc[i]->write_word(AUX_DISABLE_HOLDS,0xf);        //disabling HOLD in AUX reg 0xb0
            proc[i]->write_word(0x0000003c,0x0);
          }

          aux_hold_reg_content_3 = proc3->read_word(AUX_DISABLE_HOLDS); //saving HOLD status for all processors
          aux_hold_reg_content_4 = proc4->read_word(AUX_DISABLE_HOLDS);
          aux_hold_reg_content_5 = proc5->read_word(AUX_DISABLE_HOLDS);
          aux_hold_reg_content_6 = proc6->read_word(AUX_DISABLE_HOLDS);
        }

        else{

          //forcing HOLD from AUX
          if(verbose){ std::cout<< "Forcing HOLD lines to 1 from AUX to AMB " << std::endl; }
          for(unsigned int i = 0; i<4; i++){
            proc[i]->write_word(0x0000003c,0x1);
            proc[i]->write_word(AUX_DISABLE_HOLDS,0xf0000);       //Forcing HOLD in AUX
            proc[i]->write_word(0x0000003c,0x0);
          }

          aux_hold_reg_content_3 = proc3->read_word(AUX_DISABLE_HOLDS); //saving HOLD status for all processors
          aux_hold_reg_content_4 = proc4->read_word(AUX_DISABLE_HOLDS);
          aux_hold_reg_content_5 = proc5->read_word(AUX_DISABLE_HOLDS);
          aux_hold_reg_content_6 = proc6->read_word(AUX_DISABLE_HOLDS);

        }

        unsigned int aux_reg_content_tot = 0x0;
        aux_reg_content_tot = ( aux_reg_content_tot << 4) |  aux_hold_reg_content_6; //putting precessors statuses together
        aux_reg_content_tot = ( aux_reg_content_tot << 4) |  aux_hold_reg_content_5;
        aux_reg_content_tot = ( aux_reg_content_tot << 4) |  aux_hold_reg_content_4;
        aux_reg_content_tot = ( aux_reg_content_tot << 4) |  aux_hold_reg_content_3;

        if(iter == 1) aux_reg_content_tot = (aux_reg_content_tot>>16);
        amb_hold_reg_content = m_vme->read_word(HOLD_AUX_STATUS);
        //Making Comparisons
	if(verbose){ 
	  std::cout<<"--------------------------------------------------AUX Input Links---------------------------------------------------"<< std::endl;
	  std::cout<<"AUX->AMB Link Number";
	  for(unsigned int i = 0; i<16 ; i++){
	    if(i< 10) std::cout<<"|  "<< i << " |";
	    else std::cout<<"| "<< std::dec << i << " |";
	  }

	  std::cout << std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"     AUX HOLD       ";
	}

        for(unsigned int i = 0; i<16 ; i++){
          unsigned int link_n;
          if(iter == 0){
            link_n = not(((aux_reg_content_tot)>>i)&0x1); //not of the disable HOLD bits print AUX
	    if(verbose){ std::cout<< "|  " << link_n << " |" ; }
          }
          else{
            link_n = (((aux_reg_content_tot)>>i)&0x1); //Force HOLD bits print AUX
            if(verbose){ std::cout<< "|  " << link_n << " |" ; }
          }
        }

        test_error_temp = 0;

	if(verbose){
	  std::cout << std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"     AMB HOLD       ";
	}
        for(unsigned int i = 0; i<16 ; i++) {
          if(iter == 0){
            unsigned int link_n = (((amb_hold_reg_content)>>i)&0x1);  //not of the disable HOLD bits print AMB
            if(verbose){ std::cout<< "|  " << link_n << " |" ; }            
          }
          else {
            unsigned int link_n = ((amb_hold_reg_content)>>i)&0x1; //Force HOLD bits print AMB
            if(verbose){ std::cout<< "|  " << link_n << " |" ;}            
          }
        }
	
	if(verbose){
	  std::cout << std::endl;
	  std::cout<<"--------------------------------------------------------------------------------------------------------------------"<<std::endl;
	  std::cout<<"   LINE STATUS      ";
	}
        for(unsigned int i = 0; i<16 ; i++){
          if(iter == 0){
            if((((amb_hold_reg_content)>>i)&0x1) == not(((aux_reg_content_tot)>>i)&0x1)){ // Comparison beteween AUX and AMB HOLD bits
              if(verbose){ std::cout<< "| OK |"; }
            }
            else {
              if(verbose){ std::cout<< "| MIS|"; }//updating test status
              test_error = -1;
              test_error_temp = 3;
            }
          }
          else{
            if((((amb_hold_reg_content)>>i)&0x1) == (((aux_reg_content_tot)>>i)&0x1)){ // Comparison beteween AUX and AMB HOLD bits
              if(verbose){ std::cout<< "| OK |"; }
            }
            else {
              if(verbose){ std::cout<< "| MIS|"; } //updating test status
              test_error = -1;
              test_error_temp = 3;
            }
          } 
        }


        if(test_error_temp < 3 && verbose) std::cout<<"---------->PASSED"<<std::endl;  //print of the test result
        if(test_error_temp == 3 && verbose) std::cout<<"---------->NOT PASSED"<<std::endl;      

        std::cout << std::endl;
        std::cout << "" <<std::endl;

        if(test_error_temp < 3) test_error_temp_b = ((test_error_temp_b)<<1)|0x1; //updating the test summary 
        if(test_error_temp == 3) test_error_temp_b = ((test_error_temp_b)<<1)|0x0;

      }

      //FREEZE LINES TEST
      //Same as before, the test is performed forcing the freeze lines from AMB then checked from AUX and vice versa. This time, when we read the Freeze in AUX, is sufficient to read it at least from one processor to pass the test

      if(verbose){
	std::cout<< "-------------------------------------------------------------" << std::endl;
	std::cout<< "------------------Test of the FREEZE lines-------------------" << std::endl;
	std::cout<< "-------------------------------------------------------------" << std::endl;
      }

      for(unsigned int iter = 0; iter < 2; iter++){
        unsigned int amb_reg_content = 0x0;
        unsigned int aux_reg_content = 0x0;
        unsigned int aux_freeze  = 0x0;

        //Forcing FREEZE from AMB

        if(iter == 0) {
          m_vme->write_word(FREEZE,0x1); //forcing freeze to 1
	  if(verbose){ std::cout<< "Forcing FREEZE to 1" << std::endl; }
        }
        else{
	  if(verbose){ std::cout << "" << std::endl; }
          m_vme->write_word(FREEZE,0x0); //forcing freeze to 0
          if(verbose){ std::cout << "Forcing FREEZE to 0" << std::endl; }
        }
	if(verbose){ std::cout<< "Forcing FREEZE from AMB" << std::endl; }
        int count = 0;
        for(unsigned int i = 0; i < 4; i++){
          amb_reg_content = m_vme->read_word(FREEZE);       //reading freeze form AMB
          aux_reg_content = proc[i]->read_word(0x00000110); //reading Freeze form AUX processors

          aux_freeze = ((aux_reg_content)>>25)&0x1;          //selecting the proper bit form the reg content
          if(aux_freeze == (amb_reg_content)&0x1)  count++;  //Comparison btw AMB and AUX
        }

        if( count > 0 ){  
          if(verbose){ std::cout << "AUX FREEZE bit: "<< aux_freeze  <<"---->FREEZE form AMB TEST PASSED " << std::endl;}  //printing and uptating test status
          test_error_temp_b = (test_error_temp_b<<1)|0x1;
        }
        if (count == 0){
	  if(verbose){ std::cout << "AUX FREEZE bit: "<< aux_freeze  <<"---->FREEZE form AMB TEST NOT PASSED " << std::endl; }
          test_error_temp_b = (test_error_temp_b<<1)|0x0;
        }

        m_vme->write_word(FREEZE,0x0); //resetting freeze form AMB to 0

        //Forcing FREEZE from AUX
        if(verbose){ std::cout<< "Forcing FREEZE from AUX" << std::endl; }
        count = 0;

        for(unsigned int i = 0; i < 4; i++){

          if(iter == 0){
            proc[i]->write_word(0x0000003c,0x1); //Forcing freeze in AUX
            proc[i]->write_word(0x00000010,0x4);
            proc[i]->write_word(0x0000003c,0x0);
            aux_reg_content = proc[i]->read_word(0x00000110); //Reading AUX reg
          }
          else{
            proc[i]->write_word(0x0000003c,0x1); //Disabling freeze in AUX
            proc[i]->write_word(0x00000010,0x0);
            proc[i]->write_word(0x0000003c,0x0);
            aux_reg_content = proc[i]->read_word(0x00000110); //Reading AUX reg
          }
          amb_reg_content = m_vme->read_word(FREEZE_FROM_AUX);
          if( ((((aux_reg_content)>>24)&0x1) ==  ((amb_reg_content)&0x1) && iter == 0 && ((amb_reg_content)&0x1) == 0x1 ) || ((((aux_reg_content)>>24)&0x1) ==  ((amb_reg_content)&0x1) && iter == 1 && ((amb_reg_content)&0x1) == 0x0) ) count++; //AUX - AMB comparison -> (iter == 0 AND aux == amb AND amb == 1) OR (iter == 1 AND aux == amb AND amb == 0) 
        }
        if( count > 0 ){  
          if(verbose){ std::cout << "AMB FREEZE bit: "<< amb_reg_content  <<"--------->FREEZE form AUX TEST PASSED " << std::endl; } //printing and updating test result
          test_error_temp_b = (test_error_temp_b<<1)|0x1;
        }
        if(count == 0){
          if(verbose){ std::cout << "AMB FREEZE bit: "<< amb_reg_content  <<"--------->FREEZE form AUX TEST NOT PASSED " << std::endl; }
          test_error_temp_b = (test_error_temp_b<<1)|0x0;
        } 
      }

      unsigned int DTEST = (((test_error_temp_b)>>8)&0x3);    //Splitting test results bits
      unsigned int AMB_HOLD = (((test_error_temp_b)>>6)&0x3);
      unsigned int AUX_HOLD = (((test_error_temp_b)>>4)&0x3);
      unsigned int AMB_FREEZE = (((test_error_temp_b)>>2)&0x3);
      unsigned int AUX_FREEZE = ((test_error_temp_b)&0x3);

      string DTEST_r ;
      string AMB_HOLD_r ;
      string AUX_HOLD_r ;
      string AMB_FREEZE_r;
      string AUX_FREEZE_r;


      if(DTEST == 0x3)  DTEST_r = "    Pass";
      else  DTEST_r = "Not Pass";
      if(AMB_HOLD == 0x3) AMB_HOLD_r = "      Pass";
      else AMB_HOLD_r = "  Not Pass";
      if(AUX_HOLD == 0x3) AUX_HOLD_r = "      Pass";
      else AUX_HOLD_r = "  Not Pass";
      if(AMB_FREEZE == 0x3) AMB_FREEZE_r = "        Pass";
      else AMB_FREEZE_r = "    Not Pass";
      if(AUX_FREEZE == 0x3) AUX_FREEZE_r = "        Pass";
      else AUX_FREEZE_r = "    Not Pass";

      //Summary print
      std::cout<< "" <<std::endl;  
      std::cout<< "------------------------EXTRA LINES TEST SUMMARY----------------------" <<std::endl;  
      std::cout<< "----------------------------------------------------------------------" <<std::endl;  
      std::cout<< "TEST   || DTEST  || AMB HOLD || AUX HOLD || AMB FREEZE || AUX FREEZE ||" <<std::endl;
      std::cout<< "----------------------------------------------------------------------" <<std::endl;  
      std::cout<< "TYPE   || en|dis ||  en|dis  ||  en|dis  ||   en|dis   ||   en|dis   ||" <<std::endl;  
      std::cout<< "----------------------------------------------------------------------" <<std::endl;  
      std::cout<< "1/0    ||  "<<  std::bitset<2>(DTEST)<<"    || " <<std::bitset<2>(AMB_HOLD) << "       || " << std::bitset<2>(AUX_HOLD)<< "       || " <<std::bitset<2>(AMB_FREEZE) <<"         || "<< std::bitset<2>(AUX_FREEZE) << "         ||" <<std::endl;  
      std::cout<< "----------------------------------------------------------------------" <<std::endl;  
      std::cout<< "RESULT ||"<< DTEST_r <<"||"<< AMB_HOLD_r <<"||"<< AUX_HOLD_r <<"||"<< AMB_FREEZE_r <<"||"<< AUX_FREEZE_r <<"||" <<std::endl;  
      return test_error;
    }      

    /**@fn void cfg2vmedata(unsigned int* vmedata, int chainlength, int active_columns, int thr=8, int req_lay=0, int dis_mask=0, int tmode=0, int dis_pflow=0, int drv_str=1, int dcbits=2, int cmode=0)
     *
     * @brief This function takes as input the settings of jpatt register. It generates a vector of int with the correct data to be written in the register
     * 
     * @param vmedata Pointer to the location where you want to store the data in hardware format
     * @param chainlength Number of chips per JTAG chain
     * @param active_columns Mask active columns
     * @param thr Chip thereshold
     * @param req_lay Require layer 0 hit to fira a match
     * @param dis_mask Disables clock to pattern blocks. In AM05 bit 0 is Xoram and bit 1 is Top2. In AM06 disable any combination of the 11 blocks.
     * @param tmode AMchip test mode
     * @param dis_pflow Disable the chip serial output, at end event no pattern will be sent out of the chip
     * @param drv_str Single ended pads driving strength
     * @param dcbits Number of ternary bits
     * @param cmode Continuous mode
     *
     * This function takes as input the settings of configueration register. 
     * For more information about those parameters please refer to the AM06 documentation.
     * The settings are translated into a vector of integer (*vmedata) int the format needed for the hardware configuration.
     * That is, each 32-bits word (int) in the vector refers to a bit in JPATT config register of the chip; the i-th  bit in each 32-bits word refers to the i-th JATG chain.
     * If n chips are contained in each JATG chain the structure is repeated n times.
     *
     */
    void AMBoard::cfg2vmedata(uint32_t *vmedata, int chainlength, int active_columns, int thr=8, int req_lay=0, int dis_mask=0, int tmode=0, int dis_pflow=0, int drv_str=1, int dcbits=3, int cmode=0) {

      unsigned int i,j,k;
      for (i=0; i<chainlength; i++){		/* loop over chps in chain */
        for (j=0; j<CONFIG_SIZE; j++){		/* loop over bits in one layer */
          int pos = CONFIG_SIZE*i+ j;
          vmedata[pos] = 0;
          for (k=0; k<MAXCOLUMNS; k++) {
            if ((active_columns>>k)&0x1){
              int bit = 0;
              if (j==0||j==1||j==2||j==3 ) bit = ((thr>>j)&0x1);
              else if (j==4) bit = req_lay&0x1;
              else if(j>7 && j<16) {
                unsigned int geo_pos(0);
								try{
									geo_pos = this->getGeographicalAddress(k, i);
								}catch(daq::ftk::ftkException ex){
									ERS_LOG(string("Got exception from getGeographicalAddress method: ")+ ex.what() );
								}
                if (m_AMChip->getNAME() == AMChip05::global()->getNAME())
								{
                  geo_pos <<= 1; //for AM05 need to shift by 1 bit
								}
                bit = ((geo_pos>>(j-8))&0x1);
              }
              //          else if(j==16) bit = dis_mask&0x1;
              //          else if(j==17) bit = dis_top2&0x1;
              else if (j>=16 && j<27) bit = (dis_mask>>(j-16)) & 0x1;
              else if(j==40) bit = tmode;
              else if(j==52) bit = dis_pflow&0x1;
              else if(j==60) bit = drv_str&0x1;

              else if(j>=64&&j<68){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(0);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=68&&j<72){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(1);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=72&&j<76){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(2);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=76&&j<80){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(3);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=80&&j<84){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(4);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=84&&j<88){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(5);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=88&&j<92){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(6);
                bit = (((dcbits)>>dc_pos)&0x1);
              }
              else if(j>=92&&j<96){
                int dc_pos = (j-64)%4;
                dcbits =  m_PatternBank.getChipDCBits(7);
                bit = (((dcbits)>>dc_pos)&0x1);
              }

              else if(j==96) bit = cmode&0x1;
              else bit=0x0;
              vmedata[pos] |= (bit&0x1) <<k;
            }
          }
        }
      }
      return;
    }

  } // namespace daq
} // namespace ftk
