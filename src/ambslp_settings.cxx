

#include "ambslp/ambslp_settings.h"
#include "ambslp/exceptions.h"

using namespace daq::ftk;

void ambslp_settings::cloneInto ( ambslp_settings *dest ) {

  if ( !dest ) {
    daq::ftk::ftkException issue( ERS_HERE, name_ftk(), "invalid ambslp_settings object passed to store cloned values");
    throw issue;
  }

  dest->crate = crate;
  dest->slot  = slot;

}
