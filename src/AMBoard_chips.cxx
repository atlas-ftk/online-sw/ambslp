/** Author: G. Volpi */
/** Editor: T. Kubota */

#include "ambslp/AMBoard.h"

#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ambslp/ambslp_jtag_func.h"
#include "ambslp/ambslp_vme_jtag_func.h"

using namespace std;

namespace daq {
  namespace ftk {


    /** @fn bool ambslp_amchip_reg_healthcheck (int chipnum, int chainlength, int reg_write, int reg_read, int reg_length, int n_checks)
     *
     * @brief This function performs if a type of register of all 64 AM chips are writable/readable by writing/reading-back/comparing  them 
     * @param chipnum Bit mask of AM chips to be checked in a JTAG chain (default: 0x3)
     * @param chainlength JTAG chain length (aka numer of chip in the chain) (default: 2)
     * @param reg_write Address of the register to be written (default: C6)
     * @param reg_read Address of the register to be read (default: E6, must be the same as reg_write, or the "R" version of it)
     * @param reg_length Length of the length of the reg_write
     * @param n_checks The number of write/read-back/compare sequences performed
     */
    bool AMBoard::amchip_reg_healthcheck(int chipnum, int chainlength, int reg_write, int reg_read, int reg_length, int n_checks){

      int active_columns =0xffffffff;
      int chipno = 0;
      // std::array<uint32_t,MAXDATAWORDS>  data;
      std::array<uint32_t,MAXDATAWORDS>  outdata;

      int id_code = AM06_IDCODE;
      std::array<uint32_t,MAXDATAWORDS> indata;
      std::array<bool,32> wrdata_ir_idcode;
      u_int n_errors_idcode[MAXCOLUMNS*MAXCHIP_PER_VMECOL] = {0};

      chipno = (chainlength<<8) + chipnum;

      std::array<bool,MAXDATAWORDS> wrdata_ir;
      u_int n_errors[MAXCOLUMNS*MAXCHIP_PER_VMECOL] = {0};

      PRINT_LOG("Doing a health check of registers: Write IR = "<<std::hex<<reg_write<<", read IR = "<<reg_read<<", reg size = "<<std::dec<<reg_length);

      for(int i=0; i<n_checks; i++){
        srand (time(NULL));
        for(int k=0;k<reg_length;k++){
          if(rand()%10<5) {wrdata_ir[k]=0;}
          else {wrdata_ir[k]=1;}
        }

        ConfigureRegister(m_vme, chipno, active_columns, reg_write, wrdata_ir.data(), reg_length, outdata.data() );
        ConfigureRegister(m_vme, chipno, active_columns, reg_read, wrdata_ir.data(), reg_length, outdata.data() );
        if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, reg_length, outdata.data());

        for(int j=0; j<reg_length; j++){	  
          for(int k=0; k<MAXCOLUMNS; k++) {
            if(!(wrdata_ir[j] == ((outdata[j]>>k)&0x1))) {n_errors[k]++;}
            if(!(wrdata_ir[j] == ((outdata[reg_length+j]>>k)&0x1))) {n_errors[MAXCOLUMNS+k]++;}
          }
        }

        for(int k=0;k<32;k++){
          if(((id_code>>k)&0x1)==0x0) {wrdata_ir_idcode[k]=false;}
          else {wrdata_ir_idcode[k]=true;}
        }

        ConfigureRegister(m_vme, chipno, active_columns, 0x01, indata.data(), 32, outdata.data());
        for(int j=0; j<32; j++){

          for(int k=0; k<MAXCOLUMNS; k++) {
            if(!(wrdata_ir_idcode[j] == ((outdata[j]>>k)&0x1))) {n_errors_idcode[k]++;}
            if(!(wrdata_ir_idcode[j] == ((outdata[32+j]>>k)&0x1))) {n_errors_idcode[MAXCOLUMNS+k]++;}
          }
        }

        unsigned int idcode0 = 0;
        unsigned int idcode1 = 0;

        for(int lamb=0; lamb<4; lamb++){
          for (int chain =0; chain < 8; chain++){
            for(int ibit=0; ibit<2*32; ibit++){
              if(ibit/32==0) // first block                                                                                                                                                     
                idcode0 |= (((outdata[ibit]>>((lamb*8)+chain))&0x1) << ibit);                                                                                                             
              else if(ibit/32==1) // 2nd block                                                                                                                                                  
                idcode1 |= (((outdata[ibit]>>((lamb*8)+chain))&0x1) << ibit);                                                                                                             
            }
          }
        }
      }

      for(int i=0; i<MAXCOLUMNS*MAXCHIP_PER_VMECOL; i++) {PRINT_LOG("# of error occured in AM chip "<<i<<" = "<<n_errors[i]<<"/"<<n_checks<<" evts.");}
      if(getVerbosity()<=AMBoard::INFO){ 
        for(int i=0; i<MAXCOLUMNS*MAXCHIP_PER_VMECOL; i++) {PRINT_LOG("# of error occured in AM chip in reading IDCODE"<<i<<" = "<<n_errors_idcode[i]<<"/"<<n_checks<<" evts.");}
      }


      return true;

    } // end of ambslp_reg_healthcheck

    void AMBoard::read_CRC() {
      int active_columns =0;
      int chipno = 0;
      std::array<uint32_t,MAXDATAWORDS> outdata;
      //u_int indata[MAXDATAWORDS];
      //
      //  federico: please note that the block transfer me be failing here!
      //            using NULL for indata is useful because it sets to 0 the value to be shifted
      //            in the IR; as a consequence, the output of this function is *almost* coherent
      //            even if no chip is present (LAMB v2 or missing LAMB)
      std::vector<uint32_t> indata ;

      unsigned int idcode0 = 0;
      unsigned int idcode1 = 0;
      unsigned int idcode2 = 0;
      unsigned int idcode3 = 0;
      m_LAMBMap = 0;

      m_AMChip = AMChip::global();
      m_AMChipMap = 0;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      //-----
      int chip_mask; int columns_mask;
      m_chainlength = getJtagChainInfo(chip_mask, columns_mask);

      /* build chip number */
      chipno = (m_chainlength<<8) + chip_mask;

      if(getVerbosity()<=AMBoard::INFO){
        std::cout << " chainlength determined from HW via JTAG is : " << m_chainlength << std::endl;
        std::cout << "   chip_mask determined from HW via JTAG is : " << chip_mask << std::endl;
        std::cout << "columns_mask determined from HW via JTAG is : " << std::hex << "0x" << columns_mask << std::dec << std::endl;
        printf("chipno %x\n", chipno);  
      }

      //**********************************************************************
      //Read the CRC

      GotoTestReset(m_vme); //RESET tap 

      GotoIdlefromReset(m_vme, all_chipcolumn); //go to IDLE

      ConfigureRegister(m_vme, chipno, active_columns, CRC_REG, indata.data(), CRC_REG_SIZE, outdata.data());

      for(int lamb=0; lamb<4; lamb++){ // loop over the LAMBs, up to 4
        for (int chain =0; chain < 8; chain++){  // loop over the number of chains per LAMB, up to 8
          for(int i=0; i<m_chainlength*32; i++){ // loop of the bits: 32 bit for each element of the chain
            // ordinamento degli id e' invertito rispetto al id numerico del chip nella catena?
            if(i/32==0) // first block
              idcode0 = idcode0 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            else if(i/32==1) // 2nd block
              idcode1 = idcode1 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            else if(i/32==2) // 3rd block
              idcode2 = idcode2 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            else if(i/32==3) // 4th block
              idcode3 = idcode3 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
          }	 // end loop over the returning jtag data

          if(getVerbosity()<=AMBoard::INFO){
            printf("CRC chain %i LAMB %i %x \t",chain,lamb,idcode0);
            printf("%x \t", idcode1);
            printf("%x \t", idcode2);
            printf("%x \n", idcode3);
          }

        } // end loop over the chains
      } // end loop over the LAMBs
    } // AMBoard::read_CRC()



    /** the SMART method setup the output SERDES for the AM chips.
      Smart means that the phase of the output SERDES clock is verified to be GOOD*/
    bool AMBoard::amchip8b10b_smart(int nTries, bool isTest) {
      unsigned iAttempt(0);
      bool smart(true);
      int resVal(0);
      
      try{
        m_PhaseTest.Initialize();
      }catch (daq::ftk::FTKIssue & e){
        ers::warning(e);
      }

      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_FW_VER);
      unsigned int FW_VERSION (m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
      for(unsigned int i_lamb(0); i_lamb < m_NMaxLAMBs; i_lamb++){
        if (((m_LAMBPresence >> i_lamb) & 0x1) == 0x1){
          m_CanDoPhase &= (((FW_VERSION >> i_lamb*0x8) & 0xff) != 0x3);
        }
      }

      while ( smart and (iAttempt < (unsigned)nTries) ) { // reconfiguration loop
        iAttempt += 1; // increment the number of attempts
        smart = false;

        // prepares variable used to check the errors
        uint32_t counters[64];
        for(unsigned int i(0); i<64; i++) counters[i]=0;
        
        if (m_CanDoPhase) runPhaseTest(counters, isTest);

        // ***********************//
        //  OLD PROCEDURE: begin  //
        // ***********************//
        //
        // Run old smart procedure only if Phase test succeded
        bool runSmart(true);
        for(unsigned int i(0); i<64; i++) runSmart &= counters[i]==0? true: false;
        runSmart |= (not m_CanDoPhase); // if phase test did not run run the old smart

        if(runSmart){
          runPatternTest(counters, isTest);
          unsigned int NEvents = m_vme->read_word(AMBSLP_HIT_EVENT_COUNTER);
          if (NEvents<1000) { // check that enough events have been processed via feed_hit
            smart=true; // repeat the process in case not enough events are counted
            break; // Exit from the loop since the procedure needs to be repeated
          }
          ERS_LOG("Number of events for patternX check " << NEvents );
        }
        // *********************//
        //  OLD PROCEDURE: end  //
        // *********************//
        
        if (m_VerbosityLevel==DEBUG || isTest) {
          // print the counter on the screen
          ostringstream ss_out;
          ss_out << "Test counters: ";
          for (int i=0;i!=64;++i) {
            ss_out << counters[i] << " ";
            if (i%16 == 15) ss_out << "-- ";
          }
          ERS_LOG(ss_out.str());
        }

        // it is considered a problem to reported if a counter has >=1 mismatches
        bool doConfigure = false;
        for (int i=0;i!=64;++i) {
          if (counters[i]>0) {
            doConfigure = true;
            smart = true; // errors found. Stop the loop and proceed to reconfigure
            break; // if boolean changed no need to scan others
          }
        }

        if(not doConfigure) continue;

        // prepare the columns mask for each possible missing chip in the chain
        const unsigned int nchips(2);

        // this is a list of columns for each possible positions within the columns
        std::vector<uint32_t> active_columns(nchips);
        for (unsigned int i=0;i!=nchips;++i) {
          // reset the chip bitmask
          active_columns[i] = 0;
        }

        for (int i=0;i!=64;++i) { // loop over the chips
          // it is considered a problem to reported if a counter has >=1 mismatches
          if (counters[i]>0) {
            /* if an error counter is above threshold this chip will be reset, in order to do that
             * the jtag chain where it belongs and the position within the chain are identified.
             * The column is the chain where the chip is.
             * The position of the chip in the column is counted from the back.
             */

            // mask a column (2 AM06), taking
            const unsigned int blocksize = 1; // one column
            const unsigned int blockmask = (1<<blocksize)-1; 
            const unsigned int blocknum = i / (2*blocksize); 

            // asking to reset the LAMB because FW issues
            // identify all JTAG-VME columns that need to be reset
            active_columns[(i%nchips)] |= blockmask << blocknum*blocksize;
            resVal += 1;
          }
        } // end loop over the chips

        for (unsigned int i=0;i!=nchips&&!isTest;++i) { // loop over the chips within the jtag chain
          if (!active_columns[i]) continue; // skip the step if no faulty chips integration
          smart = true; // repeat the check and test
          ERS_LOG("Asking reset of 8b10 chip " << i << ", in columns: 0x" << hex << active_columns[i] << dec);
          amchip8b10b(0x2, 1<<i, active_columns[i]); // reset the output SERDES for the columns that have at least 1 fault chip in the given position
        } // end loop over the chips within the jtag chain

      } // end of while (smart&&iAttempt<nTries) 
      try{
        m_PhaseTest.Delete();
      }catch (daq::ftk::FTKIssue & e){
        ers::warning(e);
      }

      // if the test flag is set the return value is taken from previous flow
      return resVal;
    } // end of AMBoard::amchip8b10b_smart(int nTries)


    /** the method setup the SERDES for all the bus and chips, there is the possibility */
    bool AMBoard::amchip8b10b(int part, unsigned int chipmask, unsigned int active_columns) {
      const char* mode = "normal";

      float freq = 2.;
      int chipnum((1<<m_chainlength)-1);
      bool to_return = true;

      bool ret(true);

      if (part & 0x1) { // AM chip input bus[0-7]
	//ERS_LOG("Going to configure INPUT AM06 SERDES with part = "<<part<<".");
	// add log of AM06 SERDES reconfigure for bus0-7
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus0", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus1", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus2", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus3", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus4", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus5", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus6", mode, freq);
        to_return = to_return and ret;
        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask, active_columns, "bus7", mode, freq);
        to_return = to_return and ret;
      }

      if (part & 0x2) { // AM chip pattin0/1 and pattout
	//ERS_LOG("Going to configure OUTPUT AM 06SERDES with part = "<<part<<".");

        unsigned int pattinF_chip(0x0);
        unsigned int pattinF_col(0x0);
        unsigned int pattoutF_chip(0x0);
        unsigned int pattoutF_col(0x0);
        unsigned int pattdupF_chip(0x0);
        unsigned int pattdupF_col(0x0);

//         if(m_LAMBVersion==LAMBv1){
//           pattinF_chip = 0x1; // only the first chip of the columns
//           pattinF_col = 0xffffffff; // all columns are involved
//           pattoutF_chip = 0x6; // the 2nd and 3rd chips are involved
//           pattoutF_col = 0xffffffff; // all columns
//           pattdupF_chip = 0x9; // the 1st and 4th chips are involved
//           pattdupF_col = 0xffffffff; // all columns
//         }
//        else if(m_LAMBVersion==LAMBv2){

        // AA 2018-11-21 only LAMBv2 and greater are supported
          pattinF_chip = 0x1; // only the first chip of the columns
          pattinF_col = 0xaaaaaaaa; // all columns are involved
          pattoutF_chip = 0x2; // the 2nd is involved
          pattoutF_col =  0xffffffff; // all columns
          pattdupF_chip = 0x1; // the 1st and 4th chips are involved
          pattdupF_col = 0xffffffff; // all columns
//        }

        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask&pattinF_chip, active_columns&pattinF_col, "pattin1", mode, freq);
        to_return = to_return and ret;

        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask&pattoutF_chip, active_columns&pattoutF_col, "pattout", mode, freq);
        to_return = to_return and ret;

        ret = amchip_SERDES_setup(m_chainlength, chipnum&chipmask&pattdupF_chip, active_columns&pattdupF_col, "patt_duplex", mode, freq);
        to_return = to_return and ret;
      }

      return to_return;
    }

    /** @fn bool ambslp_amchip_read_errDout32(int slot, int chainlength, int chipnum)
     *  @brief This function read the LinkUp32 of all buses of the AMChips on an AMBoard.
     *  @param if (part&0x1) input links are checked; if (part&0x2) output links are checked
     *  @param max_tries is the number of max trials to reconfigure the links
     *  @param chainlength is the length of each JTAG chain (unique for a LAMB: 2 for LAMB ver 2/3/4, 4 for LAMB ver 1)
     *  @param chipnum  integer converted from the mask pattern of chips to be read (e.g. if you are going to read out all chips in a chain with length 2, this param should be 3 = 11b)
     */
    bool AMBoard::amchip8b10b_up_and_check(int part, int max_tries, int chainlength, int chipnum, bool do_config) { 
      int active_columns =0;
      int chipno = 0;

      const struct timespec deltaT = { 0, 1000000};

      std::array<uint32_t,MAXDATAWORDS>  outdata;
      int i; 

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      //**********************************************************************
      //**********************************************************************
      GotoTestReset(m_vme);
      GotoIdlefromReset(m_vme, all_chipcolumn);
      int DR = 0x00000000;
      std::array<bool,32> wrdata;

      bool all_linkup=false;
      int n_tried=0;
      while( !all_linkup && n_tried < max_tries){
	
	all_linkup=true;
	++n_tried;

	//configuring AMchip SERDES
	if(do_config) {
	  amchip8b10b(part);
	  nanosleep(&deltaT,0x0);
	  amchip8b10b(part);
	  nanosleep(&deltaT,0x0);
	}

	if(part & 0x1) {
	  for(int bus=0;bus<8;bus++){
	    
	    //***Choose a bus for SERDES_SEL register*************************
	    DR = 0x00000008+bus;
	    for(int i=0; i < 32; i++){
	      if (((DR>>i)&0x1)==1)
		wrdata[i] = true;
	      else
		wrdata[i] = false;
	    }			
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true);

	    //***Initialise in SERDES_REG register ************************ 
	    DR = 0x00000000;
	    for(int i=0; i < 32; i++){
	      if (((DR>>i)&0x1)==1)
		wrdata[i] = true;
	      else
	      wrdata[i] = false;
	    }			
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
	    
	    //***read err32 count via SERDES_STAT register ************************ 
	    DR = 0x00000000;
	    for(int i=0; i < 32; i++){
	      if (((DR>>i)&0x1)==1)
		wrdata[i] = true;
	      else
		wrdata[i] = false;
	    }
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_STAT_SIZE, outdata.data());
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_STAT_SIZE, outdata.data(), true);

	    for(int i=0; i<32; i++) {
	      all_linkup &= ( ((outdata[20]>>i)&0x1)==1 );
	      if ( ((outdata[20]>>i)&0x1)==0 ) 
					try{
						ERS_LOG("AM chip with geographical address: "<< this->getGeographicalAddress(i,0) <<", link of input bus "<<bus<<" is not up!");
					}catch(daq::ftk::ftkException ex){
						ERS_LOG(string("Got exception from getGeographicalAddress method: ")+ ex.what() );
					}
			}
	    for(int i=0; i<32; i++) {
	      all_linkup &= ( ((outdata[52]>>i)&0x1)==1 );
	      if ( ((outdata[52]>>i)&0x1)==0 ){
					try{
						ERS_LOG("AM chip with geographical address: "<< this->getGeographicalAddress(i,1) <<", link of input bus "<<bus<<" is not up!");
					}catch(daq::ftk::ftkException ex){
						ERS_LOG(string("Got exception from getGeographicalAddress method: ")+ ex.what() );
					}
				}	
			}
	    //for(int i=0; i<64; i++)
	      //std::cerr<<"bus = "<<bus+8<<", patt outdata["<<i<<"] = "<<std::hex<<outdata[i]<<std::dec<<std::endl;;
	    
	  }
	}
	
	if(part & 0x2) {
	  for(int bus=0;bus<3;bus++){
	    if(bus==1) continue;

	    //***Choose a bus for SERDES_SEL register*************************
	    DR = 0x00000000+bus;
	    for(int i=0; i < 32; i++){
	      if (((DR>>i)&0x1)==1)
		wrdata[i] = true;
	      else
		wrdata[i] = false;
	    }			
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true);
	    
	    //***Initialise in SERDES_REG register ************************ 
	    DR = 0x00000000;
	    for(int i=0; i < 32; i++){
	      if (((DR>>i)&0x1)==1)
		wrdata[i] = true;
	      else
		wrdata[i] = false;
	    }			
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
	    
	    //***read err32 count via SERDES_STAT register ************************ 
	    DR = 0x00000000;
	    for(int i=0; i < 32; i++){
	      if (((DR>>i)&0x1)==1)
		wrdata[i] = true;
	      else
		wrdata[i] = false;
	    }
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_STAT_SIZE, outdata.data());
	    ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_STAT_SIZE, outdata.data(), true);
	    
	    if(bus==0){
	      for(int i=0; i<32; i++) {
		all_linkup &= ( ((outdata[52]>>i)&0x1)==1 );
		if ( ((outdata[52]>>i)&0x1)==0 ) ERS_LOG("AM06 chip "<<i+MAXCOLUMNS<<", link of pattin0/pattout bus is not up!");
	      }
	    } else if (bus==2){
	      for(int i=0; i<16; i++) {
		all_linkup &= ( ((outdata[52]>>(i*2+1))&0x1)==1 );
		if ( ((outdata[52]>>(i*2+1))&0x1)==0 ) ERS_LOG("AM06 chip "<<(i*2+1)+MAXCOLUMNS<<", link of pattin1 bus is not up!");
	      }
	    }
 	    //for(int i=0; i<64; i++)
	    //std::cerr<<"bus = "<<bus<<", patt outdata["<<i<<"] = "<<std::hex<<outdata[i]<<std::dec<<std::endl;;

	  }
	}
      }
      
      //std::cerr<<"all_linkup = "<<all_linkup<<std::endl;
      if( (part & 0x3)!=0 )ERS_LOG("Status of AM06 input links: "<<all_linkup<<" (1: all links up, 0: some links are down)");
      return all_linkup;

    }

    /** @fn bool ambslp_amchip_read_errDout32(int slot, int chainlength, int chipnum)
     *  @brief This function read the errDout32 of all buses of the AMChips on an AMBoard. errRd32 will be set at 0 after reading out.
     *  @param if (part&0x1) input links are checked; if (part&0x2) output links are checked
     *  @param chainlength is the length of each JTAG chain (unique for a LAMB: 2 for LAMB ver 2/3/4, 4 for LAMB ver 1)
     *  @param chipnum  integer converted from the mask pattern of chips to be read (e.g. if you are going to read out all chips in a chain with length 2, this param should be 3 = 11b)
     *  @param time: duration of the error counting in nano-seconds 
     *  @param serDesMode: 00 - normal operation, 01 - PRBS7 mode, 10 - Pattern mode, 11 - Loopback mode
     */
    bool AMBoard::amchip8b10b_count_errDout32(std::vector<std::vector<unsigned int>>& errorDout32_count, std::vector<std::vector<unsigned int>>& rx_stream_error, std::vector<std::vector<unsigned int>>& tx_stream_error, int part, int chainlength, int chipnum, int time, int serDesMode) { 
      int active_columns =0;
      int chipno = 0;
      
      // const struct timespec deltaT = { 0, 1000000};
      
      std::array<uint32_t,MAXDATAWORDS>  outdata;
      int i; 
      
      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;
      
      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;
      
      //**********************************************************************
      //**********************************************************************
      GotoTestReset(m_vme);
      GotoIdlefromReset(m_vme, all_chipcolumn);
      
      int DR = 0x00000000;
      std::array<bool,32> wrdata;
      
      if(part & 0x1) {
	for(int bus=0;bus<8;bus++){
	  
	  //***write in SERDES_SEL the bus selection*****************************************
	  DR = 0x00000008+bus;
	  for(int i=0; i < 32; i++){
	    if((((DR>>i)&0x1)==1))
	      wrdata[i] = true;
	    else
	      wrdata[i] = false;
	  }			
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true);
	  
	  //***write in SERDES_REG 0x00000000 (turn on errRd32)************************
	  DR = 0x00000000;
	  DR |= serDesMode;
	  for(int i=0; i < 32; i++){
	    if (((DR>>i)&0x1)==1)
	      wrdata[i] = true;
	    else
	      wrdata[i] = false;
	  }			
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);	  
	}
	
	usleep(time);
	
	for(int bus=0;bus<8;bus++){
	  
	  //***write in SERDES_REG 0x00001000 (turn off errRd32)************************ 
	  DR = 0x00001000; 
	  for(int i=0; i < 32; i++){
	    if (((DR>>i)&0x1)==1)
	      wrdata[i] = true;
	    else
	      wrdata[i] = false;
	  }			
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
	  
	  //***read err32 count via SERDES_STAT register************************ 
	  DR = 0x00000000;
	  for(int i=0; i < 32; i++){
	    if (((DR>>i)&0x1)==1)
	      wrdata[i] = true;
	    else
	      wrdata[i] = false;
	  }			
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_STAT_SIZE, outdata.data());
	  ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_STAT_SIZE, outdata.data(), true);
	  
	  //for(int i=0; i<64; i++)
	  //std::cerr<<"bus = "<<bus<<", patt outdata["<<i<<"] = "<<std::hex<<outdata[i]<<std::dec<<std::endl;;
	  
	  for(int ichip=0; ichip<MAXCOLUMNS; ichip++){
	    for(int ibit=0; ibit<NLAYERS; ibit++){
	      //std::cerr<<"outdata["<<ibit+ERRDOUT32_OFFSET<<"] = "<<std::hex<<outdata[ibit+ERRDOUT32_OFFSET]<<std::dec<<std::endl;
	      //std::cerr<<"outdata["<<ibit+ERRDOUT32_OFFSET+MAXCOLUMNS<<"] = "<<std::hex<<outdata[ibit+ERRDOUT32_OFFSET+MAXCOLUMNS]<<std::dec<<std::endl;
	      errorDout32_count[ichip][bus] |= (((outdata[ibit+ERRDOUT32_OFFSET]>>ichip)&0x1)<<ibit);
	      errorDout32_count[ichip+MAXCOLUMNS][bus] |= (((outdata[ibit+ERRDOUT32_OFFSET+MAXCOLUMNS]>>ichip)&0x1)<<ibit);
	      
	      //std::cerr<<"errorDout32_count["<<ichip<<"]["<<bus<<"] = "<<std::hex<<errorDout32_count[ichip][bus]<<std::dec<<std::endl;
	      //std::cerr<<"errorDout32_count["<<ichip+MAXCOLUMNS<<"]["<<bus<<"] = "<<std::hex<<errorDout32_count[ichip+MAXCOLUMNS][bus]<<std::dec<<std::endl;
	      
	    }
	    rx_stream_error[ichip][bus] |= ((outdata[RX_STREAM_ERROR_OFFSET]>>ichip)&0x1);
	    rx_stream_error[ichip+MAXCOLUMNS][bus] |= ((outdata[RX_STREAM_ERROR_OFFSET+MAXCOLUMNS]>>ichip)&0x1);
	    tx_stream_error[ichip][bus] |= ((outdata[TX_STREAM_ERROR_OFFSET]>>ichip)&0x1);
	    tx_stream_error[ichip+MAXCOLUMNS][bus] |= ((outdata[TX_STREAM_ERROR_OFFSET+MAXCOLUMNS]>>ichip)&0x1);
	  }
	}
	

	// for(int bus=0;bus<8;bus++){
	  
	// //***write in SERDES_REG 0x00000000 (turn off errRd32)************************ 
	// DR = 0x00000000;
	// for(int i=0; i < 32; i++){
	//   if (((DR>>i)&0x1)==1)
	//     wrdata[i] = true;
	//   else
	//     wrdata[i] = false;
	// }			
	// ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
	// ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
	
	// }

	
      }
      
      if(part & 0x2) {
	for(int bus=0;bus<3;bus++){

	  //To be filled

	}
      }
      
      return true;
      
    }

    /** This function checks the register connected to the test patterns, assumed to be installed in position 0 of
     * each chip, and reports the number of missing matches since the last reset.
     *
     * The function return a point with 64 counters, the memory has to be cleaned by the returning function.
     */
    void AMBoard::getNMissingTestMatches(uint32_t counts[64]) {
      const unsigned int TotLinks(MAXOUTLINK_PERLAMB*m_NMaxLAMBs); // total number of output link to check
      for (unsigned int ilink=0; ilink != TotLinks; ++ilink) { // loop over the number of output links, usually 16
        const unsigned LambID(ilink/MAXOUTLINK_PERLAMB);
        if ((m_LAMBPresence>>LambID)&0x1) {
          // if the LAMB appears as installed the register is read
          // for each linke there is a 32-bit register representing 4 counters
          unsigned int link_counters = m_vme->read_word(AMB_ROAD_PATTERNX_LINK0 + 4*ilink); // the registers are contiguos

          for (unsigned int iblock=0; iblock!=4; ++iblock) { // loop over the 8 bit counter blocks
            // assign the counter value to the proper counter
            counts[iblock+ilink*4] = (link_counters>>(8*iblock)) & 0xff; // shift right of enough bit and extract the first 8 bits
          }
        }
        else {
          // if the LAMB is not installed the counters are set at 0 by default
          for (unsigned int iblock=0; iblock!=4; ++iblock) counts[iblock+ilink*4] = 0;
        }
      }

      // loop over the LA
    }

    /** Sent and an init event to the chips
     *
     */
    bool AMBoard::amchip_init_evt(int chipnum) {
      // calculate the chip number
      int chipno = 0;
      int all_chipcolumn = 0xffffffff;
      int active_columns = 0xffffffff;

      chipno = (m_chainlength<<8) + chipnum;

      //ERS_LOG( " chipno " <<  hex << "0x" << chipno << dec << "\n");
      //ERS_LOG( " chainlength " << m_chainlength << "\n");

      // send the init to the board

      // this is the central part of this function: how to control its exceptions?
      GotoTestReset( m_vme );
      GotoIdlefromReset( m_vme, all_chipcolumn);
      // send init event
      ConfigureScam(m_vme, chipno, active_columns, 0xD6, 1);
      GotoTestReset( m_vme );
      // end of core functionality


      // if here, everything is ok
      return true;
    }

    /** @fn bool ambslp_amchip_jpatt_cfg(int slot, int chainlength, int chipnum, const char* columns, int thr, int req_lay, int dis_mask, int tmode, int dis_pflow, int drv_str, int dcbits, int cmode, bool is_lamb_v1 )
     *
     * @brief This function is used to configure the jpatt register of AMchip05/06.  The variales in input are bits in the jpatt_register.
     * @param slot Crate slot in which the board is plugged
     * @param chainlength JTAG chain length (aka numer of chip in the chain)
     * @param active_columns JATG colums mask
     *
     */
    bool AMBoard::amchip_jpatt_cfg(int chipnum, const char* columns, int thr, int req_lay, int dis_mask, int tmode, int dis_pflow, int drv_str, int dcbits, int cmode){


      int active_columns =0;
      int chipno = 0;
      std::array<uint32_t,MAXDATAWORDS> data;
      std::array<uint32_t,MAXDATAWORDS> outdata;
      std::array<bool,145> wrdata_ir;
      //unsigned int indata[MAXDATAWORDS];  // unused var

      //chipcolumns you want to act on
      //PRINT_LOG("Setting up columns");
      int all_chipcolumn = 0;
      int even_chipcolumn=0;
      int odd_chipcolumn=0;
      for(unsigned int t=0;t<MAXCOLUMNS;t++){
        if((t%2)==0) even_chipcolumn |= 1<<t;
        else odd_chipcolumn |= 1<<t;
        all_chipcolumn |= 1<<t;
      }
      if(strcmp(columns, "a")==0) active_columns = all_chipcolumn;
      else if (strcmp(columns, "o")==0) active_columns = odd_chipcolumn;
      else if (strcmp(columns, "e")==0) active_columns = even_chipcolumn;
      else{
        PRINT_LOG("error: Found an invalid option for columns: '"<<columns<<"'. Abort!");
        return false;
      }

//       if(m_LAMBVersion==LAMBv1){
//         for(int lamb=0;lamb<m_NMaxLAMBs;lamb++) active_columns |= (0x1b<<(8*lamb));
//       }
      /* build chip number */

      chipno = (m_chainlength<<8) + chipnum;

      //PRINT_LOG("chipno"<<chipno);
      //PRINT_LOG("chianlength"<<m_chainlength);

      GotoTestReset(m_vme); // RESTE TAP CONTROLLER
      GotoIdlefromReset(m_vme, all_chipcolumn); //go to IDLE
      for(int t=0;t<MAXDATAWORDS;t++) data[t]=0;
      cfg2vmedata(data.data(), m_chainlength, active_columns, thr, req_lay, dis_mask, tmode, dis_pflow, drv_str, dcbits, cmode);
      ConfigureRegister(m_vme, chipno, active_columns, CONFIG_REG, data.data(), CONFIG_SIZE, outdata.data());
      ConfigureRegister(m_vme, chipno, active_columns, CONFIG_REGrd, data.data(), CONFIG_SIZE, outdata.data(), true);
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, CONFIG_SIZE, outdata.data()); //PRINT OUTPUT
      GotoTestReset(m_vme);


      for(int k=0;k<145;k++)
        wrdata_ir[k]=0;

      ConfigureRegister(m_vme, chipno, active_columns, JPATT_DATA, wrdata_ir.data(), DATA_SIZE, outdata.data());
      //ConfigureRegister(m_vme, chipno, active_columns, JPATT_DATArd, wrdata_ir, DATA_SIZE, outdata, true);
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, DATA_SIZE, outdata.data());



      return true;
    } // end of ambslp_amchip_idcode_4chip

    /** @fn bool amchip_SERDES_setup(int slot, int chainlength, int chipnum, const char* columns, const char* serdes, const char* operation_mode, float frequency)
     *
     * @brief This function sets up the serdes of amchip 05 and amchip 06
     *
     * @param slot Vme slot in which the board is plugged
     * @param chainlength number of chips per JTAG chain
     * @param chipnum Mask the chip you want to program
     * @param columns Columns you want to anct on
     * @param serdes SERDES you want to set-up
     * @param operation_mode Operation mode of the SERDES
     * @param frequency Opearating frequency of the serdes in Gbit/s
     *
     * This function sets up the serdes of amchip 05 and amchip 06
     * columns is an indication of the jtag columns to be addressed, 'a' stands for all, 'e' stands for even, 'o' stands for odd
     * serdes is an indication of the serdes to be set up, allowed options:
     *  'patt_duplex', indicating pattin 0 and pattout;
     *  'pattin0';
     *  'pattin1';
     *  'pattout';
     *  'bus0';
     *  'bus1';
     *  'bus2';
     *  'bus3';
     *  'bus4';
     *  'bus5';
     *  'bus6';
     *  'bus7'.
     * operation_mode is an indication of the serdes operation mode, allowed options are:
     *  'normal', serdes performs 8b 10b decode/encode;
     *  'prbs',  serdes sends/expect a pseudo random bit sequence with period 2^7-1;
     *  'fixed_pattern'serdes send a fixed pattern.
     * frequency is the frequency of operation expressed in Gbit/second: the clock multiplier and divider are evaluated assuming a reference clock frequency of 100 MHz
     *
     */

    bool AMBoard::amchip_SERDES_setup(int chainlength, int chipnum, unsigned int active_columns, const char* serdes, const char* operation_mode, float frequency){

      std::array<uint32_t,MAXDATAWORDS> outdata;

      int chipno(0);
      unsigned int i(0);
      unsigned int serdes_number(0), rx_tx_enable(0), mode(0);

      // provide a bitmask with 1 all the columns
      unsigned int all_chipcolumns = (MAXCOLUMNS >= 32 ? -1 : (1<<MAXCOLUMNS)-1);

      PRINT_DEBUG_VME("Setting up serdes_number and rx_tx_enable");
      std::pair<int,int> ser_txrx = AMChip::get_serdes_number(serdes);
      serdes_number = ser_txrx.first;
      rx_tx_enable = ser_txrx.second;
      if(serdes_number==0xffffffff or rx_tx_enable==0xffffffff){
        PRINT_LOG("ERROR!!! Could not retrieve SERDES number for SERDES name:\t" << serdes );
        return false;
      }

      PRINT_DEBUG_VME("Setting up operation mode");
      //operation mode selection
      mode = AMChip::get_operation_mode_number(operation_mode);
      if(mode==0xffffffff){
        PRINT_LOG("ERROR!!! Could not retrieve operation mode number for operation mode name:\t" << operation_mode );
        return false;
      }
      PRINT_DEBUG_VME("Setting up frequency");
      //frequency set up
      int prod = (int)(frequency*1E1);
      /// int count(0);   // unused var
      if(not ((frequency*1E1)-prod==0)){
        PRINT_INFO("ambslp_amchip::serdes_serup(): got a wrong bit rate, setting up default bit rate og 2.0 Gbit/s!");
        frequency = 2.0;
        prod = (int)(frequency*1E1);
      }
      unsigned int div(1), mult(1);
      PRINT_DEBUG_VME("Getting a list of prime numbers");
      //getting a list of prime numbers lower than target product
      vector<int> prime_numbers;
      prime_numbers.reserve(prod);
      for(int num=2; num<prod; num++){
        int n_div(0);
        for(int div=2; div<num; div++){
          if((num%div)==0) n_div++;
          else continue;
        }
        if(n_div==0) prime_numbers.push_back(num);
      }
      PRINT_DEBUG_VME("Factorizing frequency");
      //fattorizzo il la frequenza
      int serv_prod(prod);
      vector<int> fac;
      fac.reserve(prod);
      PRINT_DEBUG_VME("prime numbers size: "<<prime_numbers.size());
      for(i=0; i<prime_numbers.size();i++){
        PRINT_DEBUG_VME("prime_numbers[i]: "<<prime_numbers.at(i));
        while((serv_prod%prime_numbers.at(i))==0){
          fac.push_back(prime_numbers.at(i));
          serv_prod=serv_prod/prime_numbers.at(i);
        }
      }
      for(i=0;i<fac.size();i++){
        div=div*fac.at(i);
        mult = prod*div;
        if((100/div)<15){
          div=div/fac.at(i);
          mult = prod*div;
          break;
        }
        if((uint)mult>0xffffffff){
          div = div/fac.at(i);
          mult = prod*div;
          break;
        }
      }
      
      //PRINT_INFO("SERDES type: "<<serdes<<", div: 0x"<<hex<<div<<", mult: 0x"<<hex<<mult)

      unsigned int preset_div = 10.;
      unsigned int preset_mult = 200.;
      if((mult/div - preset_mult/preset_div) > 0.001*preset_mult/preset_div){
        std::stringstream message;
        message << "SERDES "<<serdes<<", link seed significantly differ: div = "<<div<<", mult = "<<mult<<"freq = "<<100*mult/div<<" MHz";
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
        ers::error(ex);
      }

      div = preset_div;
      mult = preset_mult;
      PRINT_INFO("Manually setting div and mult for SERDES "<<serdes<<", div: 0x"<<hex<<div<<", mult: 0x"<<hex<<mult);

      /* build chip number */
      chipno = (chainlength<<8) + chipnum;

      //PRINT_DEBUG_VME("chipno "<<hex<<chipno);
      //PRINT_DEBUG_VME("chainlength "<<hex<<chainlength);

      GotoTestReset(m_vme); //6 colpi di TCK con TMS alto: TRST non funziona. Stato di reset di tap controller.
      GotoIdlefromReset(m_vme, all_chipcolumns); //un colpo di TCK con TMS 0: idle

      //INIZIO CONFIGURAZIONE BUS0!!!!!!**************************************

      // accumulate the selector and register
      unsigned int SelDRWords[] = {
        0x00000020+serdes_number,
        0x00000020+serdes_number,
        0x00000010+serdes_number,
        0x00000030+serdes_number,
        0x00000040+serdes_number,
        0x00000050+serdes_number,
        0x00000060+serdes_number,
        0x00000000+serdes_number,
        0x00000020+serdes_number,
        0xffffffff // end
      };

      unsigned int RegDRWords[] = {
        0x10000001+(mult<<16)+(div<<8),
        0x00000001+(mult<<16)+(div<<8),
        0x00000000,
        0x00000000,
        0x01222000+(rx_tx_enable<<4)+rx_tx_enable,
        0x0000000B,
        0x00000361,
        0x00000000+mode,
        0x00000000+(mult<<16)+(div<<8),
        0xffffffff // end
      };

      for (unsigned int iStep=0;;++iStep) { // loop over the register to be set
        unsigned int SelW = SelDRWords[iStep];
        unsigned int RegW = RegDRWords[iStep];

        if (SelW==0xffffffff && RegW==0xffffffff) break;

        // set the words to set the registers
        std::array<bool,7> wrdata_ir;
        for(int i=0; i < 7; i++){
          if(((SelW>>i)&0x1)==1)
            wrdata_ir[i] = true;
          else
            wrdata_ir[i] = false;
        }
        ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
        ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true);
        if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, 7, outdata.data());

        // set the word to registered
        std::array<bool,32> wrdata;
        for(int i=0; i < 32; i++){
          if (((RegW>>i)&0x1)==1)
            wrdata[i] = true;
          else
            wrdata[i] = false;
        }
        ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
        ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);
        if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, 32, outdata.data());
      } // end loop over the register to be set

      return true;
    }


    //////////////////////
    // read_link_errors //
    //////////////////////

    /** This function reads the PRBS errors counted by a chip serdes.
     *
     * @param chip_pos Position of the chip in the Jtag chain 
     * @param column_number number of the column you want to check
     * @param serdes Name of the serdes you want to access (see AMChip::get_serdes_number)
     * @param info is the information you want to mask, allowed options are: StreamError; LinkUP; errDOUT. See AMchip documentation for more infos on SEREDS statistics;
     * @return This function returns the bit of SERDES_STAT register masked by info parameter. If info has an invalid value it returns a negative value.
     *
     */

    int AMBoard::read_link_errors(int chip_pos, int column_number, const char* serdes, const char* info){
      std::array<uint32_t,MAXDATAWORDS>  outdata;
      unsigned int serdes_number = AMChip::get_serdes_number(serdes).first;
      if (serdes_number==0xffffffff) return -999;
      unsigned int active_columns=0xffffffff;
      int chipno = 0;
      for (int i=0, chipno=0; i<MAXCHIP_PER_VMECOL; i++) chipno = (chipno<<1) + 1; 
      chipno += (MAXCHIP_PER_VMECOL<<8);


      GotoTestReset(m_vme);
      GotoIdlefromReset(m_vme, active_columns);

      //***scrivo in C9 0x00+serdes_number ************************ 
      int  DR = 0x00000000+serdes_number;
      std::array<bool,32> wrdata;
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1) wrdata[i] = true;
        else wrdata[i] = false;
      }			

      ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true); 
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, 7, outdata.data()); 
      //***scrivo in EA 0x0000************************ 
      DR = 0x00000000;
      DR = 0x00000000;
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1) wrdata[i] = true;
        else wrdata[i] = false;
      }			
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_REG_SIZE, outdata.data());
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, SERDES_REG_SIZE, outdata.data());
      if(strcmp(info, "StreamError")==0){
        if (AMChip::get_serdes_number(serdes).second==0x1) 
          return ((outdata[28+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1);
        else if (AMChip::get_serdes_number(serdes).second==0x2) 
          return ((outdata[24+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1);
        else if (AMChip::get_serdes_number(serdes).second==0x3) 
          return ((((outdata[24+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1)<<1) +((outdata[28+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1));
        else return -999;
      }else if(strcmp(info, "LinkUP")==0)
        return ((outdata[20+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1);
      else if(strcmp(info, "errDOUT")==0){
        int to_return(0);
        for(int f=0; f<8;f++)
          to_return|=((outdata[f+8+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1)<<f;
        return to_return;
      }else return -999;


      // Nicolo':
      //    I think this function is not used can someone make a standalone function that eveluates the BER for all links? 
      //    If you want you can use ambslp_amchip_BER.cxx to start
    }

    /////////////////////////
    // pilot_error_counter //
    /////////////////////////
    /** This method enables/disables the error counter of the chip SERDES.
     *
     * @param serdes Name of the serdes you want to access (see AMChip::get_serdes_number)
     * @param operation_mode The operation mode of the SERDES for available modes see AMChip::get_operation_mode_number, NOTE that error counter content depends on operation_mode parameter please refer to AMChip_06 documantation
     * @param enable_counter If true enables the counte, if false it disables it;
     * @return False if the operation failed, alse true.
     *
     */

    bool AMBoard::pilot_error_count(const char* serdes, const char* operation_mode, bool enable_counter){
      unsigned int serdes_number = AMChip::get_serdes_number(serdes).first;
      if (serdes_number==0xffffffff){
        PRINT_LOG("AMBoard::pilot_error_count(): Could not retrieve SERDES number for seredes_name:\t" << serdes );
        return false;
      }
      unsigned int mode(0x0);
      mode = AMChip::get_operation_mode_number(operation_mode);
      if(mode==0xffffffff){
        PRINT_LOG("AMBoard::pilot_error_count(): Could not retrieve operation mode number for operation mode name:\t" << operation_mode );
        return false;
      }

      std::array<uint32_t,MAXDATAWORDS>  outdata;
      unsigned int active_columns=0xffffffff;
      int chipno = 0;
      for (int i=0, chipno=0; i<MAXCHIP_PER_VMECOL; i++) chipno = (chipno<<1) + 1; 
      chipno += (MAXCHIP_PER_VMECOL<<8);


      GotoTestReset(m_vme);
      GotoIdlefromReset(m_vme, active_columns);

      //***scrivo in C9 0x00+serdes_number ************************ 
      int  DR = 0x00000000+serdes_number;
      std::array<bool,32> wrdata;
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1) wrdata[i] = true;
        else wrdata[i] = false;
      }			

      ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true); 
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, 7, outdata.data()); 
      //***scrivo in CA 0x0001************************
      DR = 0x00000000;
      if (enable_counter) DR = 0x00001100+(mode&0x1);
      else DR = 0x00000000+(mode&0x1);
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1) wrdata[i] = true;
        else wrdata[i] = false;
      }			
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, SERDES_REG_SIZE, outdata.data());

      return true;
    }

    //////////////////
    // DTEST_select //
    //////////////////
    /** This method enables/disables the error counter of the chip SERDES.
     *
     * @param serdes Name of the serdes you want to access (see AMChip::get_serdes_number)
     * @param DTEST_mode Selects the signal to be connected to DTEST pin.
     * @param enable Eneble value for DTETST out pin.
     * @return False if the operation failed, alse true.
     *
     */

    bool AMBoard::DTEST_select(const char* serdes, const char* DTEST_mode, bool enable){
      unsigned int active_columns=0xffffffff;

      int chipno(0);
      for (int i=0; i<MAXCHIP_PER_VMECOL; i++) chipno = (chipno<<1) + 1; 
      chipno |= (MAXCHIP_PER_VMECOL<<8);
      if (m_VerbosityLevel==DEBUG) std::cout << "chipno:\t" << std::hex << chipno << std::endl;

      unsigned int serdes_number(0xffffffff);
      unsigned int rx_tx_enable(0xffffffff);
      if(strcmp(serdes, "data_available")==0){
        serdes_number=0x5;
        rx_tx_enable=0x3;
      }else if(strcmp(serdes, "pattout_data_valid")==0){
        serdes_number=0x4;
        rx_tx_enable=0x3;
      }else{
        serdes_number = AMChip::get_serdes_number(serdes).first;
        rx_tx_enable = AMChip::get_serdes_number(serdes).second;
      }
      if (serdes_number==0xffffffff or rx_tx_enable==0xffffffff){
        PRINT_LOG("AMBoard::DTEST_select(): Could not retrieve SERDES number for seredes_name:\t" << serdes );
        return false;
      }

      unsigned int mode(0x0);
      if(strcmp(DTEST_mode, "1")==0){
        mode = 0x0;
      }else if(strcmp(DTEST_mode, "0")==0){
        mode = 0x1;
      }else if(strcmp(DTEST_mode, "REFCLK")==0){
        mode = 0x2;
      }else if(strcmp(DTEST_mode, "clock_for_lock")==0){
        mode = 0x3;
      }else if(strcmp(DTEST_mode, "preLOCK")==0){
        mode = 0x4;
      }else if(strcmp(DTEST_mode, "CLK40")==0){
        mode = 0x5;
      }else if(strcmp(DTEST_mode, "CLK10")==0){
        mode = 0x6;
      }else if(strcmp(DTEST_mode, "LOCK")==0){
        mode = 0x7;
      }else{
        FTK_STRING_PARAM_ERROR("AMBoard::DTEST_select(): Found an invalid option for DTEST mode: '"<<DTEST_mode );
        return false;
      }

      unsigned int enable_bit(0x0);
      if(enable) enable_bit=0x1;

      std::array<uint32_t,MAXDATAWORDS> outdata;

      GotoTestReset(m_vme );
      GotoIdlefromReset(m_vme , active_columns);

      //*** Write 0x40+serdes_number in C9: select ConfReg2 of the serdes
      int  DR = 0x00000040+serdes_number;
      std::array<bool,SERDES_SEL_SIZE> wrdata;
      for(int i=0; i < SERDES_SEL_SIZE; i++){
        if((DR>>i)&0x1) wrdata[i] = true;
        else wrdata[i] = false;
      }			
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, 0);
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true); 
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, SERDES_SEL_SIZE, outdata.data()); 

      std::array<bool,SERDES_REG_SIZE> wrdata1;
      //*** Write in register CA the proper configuration
      DR = 0x00002000;
      DR |= ((enable_bit&0x1)<<24)|((mode&0x7)<<20)|((mode&0x7)<<16)|((rx_tx_enable&0x3)<<4)|(rx_tx_enable&0x3);
      for(int i=0; i < SERDES_REG_SIZE; i++){
        if((DR>>i)&0x1) wrdata1[i] = true;
        else wrdata1[i] = false;
      }			

      ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata1.data(), SERDES_REG_SIZE, 0);
      ConfigureRegister(m_vme, chipno, active_columns, SERDES_REG, wrdata1.data(), SERDES_REG_SIZE, outdata.data(), true);
      if (m_VerbosityLevel==DEBUG) PrintRegisterContent(chipno, SERDES_REG_SIZE, outdata.data()); 
      return true;
    }//end of DTEST_select



    /////////////////////
    // read_chip_DTEST //
    /////////////////////

    /** This function reads the DTestOut pin values for all the chips on an AMBoard.
     *
     * @return Return an uint64_t where each bit is the value read as DTEST out pin of each chip
     *
     */

    uint64_t AMBoard :: read_chip_DTEST(){
      //define constants for reading
      const unsigned int READ_QUARTETS(2);
      const unsigned int CHIP_per_QUARTET(4);
      const unsigned int QUARTETS_per_LAMB(4);
      //define output variable
      uint64_t test_result(0);
      //read quartets 0 and 1 for each lamb
      m_vme->write_word(0x00000038, 0x02020202);
      unsigned int chain01= m_vme->read_word(0x0000003c);
      //format output
      for(unsigned int i_lamb=0; i_lamb<m_NMaxLAMBs; i_lamb++){
        for(unsigned int i_quartet=0; i_quartet<READ_QUARTETS; i_quartet++){
          uint64_t dtest_quartet(0);
          dtest_quartet = (chain01>>((CHIP_per_QUARTET*i_quartet)+(CHIP_per_QUARTET*READ_QUARTETS*i_lamb)))&0xf;
          if (m_VerbosityLevel==DEBUG) std::cout << "LAMB: " << i_lamb <<" quartet: " << i_quartet << " DTEST values: "  << std::hex << dtest_quartet << std::dec << std::endl;
          test_result |= dtest_quartet<<(i_quartet*CHIP_per_QUARTET+ i_lamb*QUARTETS_per_LAMB*CHIP_per_QUARTET);
        }
      }
      //read quartets 0 and 1 for each lamb
      m_vme->write_word(0x00000038, 0x03030303);
      chain01= m_vme->read_word(0x0000003c);
      //format output
      for(unsigned int i_lamb=0; i_lamb<m_NMaxLAMBs; i_lamb++){
        for(unsigned int i_quartet=2; i_quartet< 2+READ_QUARTETS; i_quartet++){
          uint64_t dtest_quartet(0);
          dtest_quartet = (chain01>>((CHIP_per_QUARTET*(i_quartet%2))+(CHIP_per_QUARTET*READ_QUARTETS*i_lamb)))&0xf;
          if (m_VerbosityLevel==DEBUG) std::cout << "LAMB: " << i_lamb <<" quartet: " << i_quartet << " DTEST values: "  << std::hex << dtest_quartet << std::dec << std::endl;
          test_result |= dtest_quartet<<(i_quartet*CHIP_per_QUARTET+ i_lamb*QUARTETS_per_LAMB*CHIP_per_QUARTET);
        }
      }
      if (m_VerbosityLevel==DEBUG) std::cout << "read_chip_DTEST result: " << std::hex << test_result << std::dec << std::endl;
      return test_result;
    }

    void AMBoard::runPhaseTest(uint32_t counts [64], bool isTest){
      // CLK40 on the AMchip DTEST pin
      DTEST_select("patt_duplex", "CLK40", true);
      //--- Enable and disalble counters, used fo phase measument ---
      //Reset counters
      unsigned int cmd = (0x0);
      for(unsigned i_lamb(0); i_lamb < m_NMaxLAMBs; i_lamb++) cmd |= LAMB_PHASE_RESET << (8*i_lamb);
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_CLOCK_CONTROL);
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_DATA, cmd);
      //Enable counters
      cmd = (0x0);
      for(unsigned i_lamb(0); i_lamb < m_NMaxLAMBs; i_lamb++) cmd |= LAMB_PHASE_ENABLE << (8*i_lamb);
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_CLOCK_CONTROL);
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_DATA, cmd);
      //Sleep for one second
      const struct timespec deltaTphase = { 1 , 0 };
      nanosleep(&deltaTphase,0x0);
      //Disable counters
      cmd = (0x0);
      for(unsigned i_lamb(0); i_lamb < m_NMaxLAMBs; i_lamb++) cmd |= LAMB_PHASE_DISABLE << (8*i_lamb);
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_CLOCK_CONTROL);
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_DATA, cmd);
      // Read missed PLL Locks
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_PHASE_PLL_ULOCK_1);
      unsigned int PLL_LOCKS_1 (m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_PHASE_PLL_ULOCK_2);
      unsigned int PLL_LOCKS_2 ( m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
      //Read LAMB reference clock
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_REF_CLOCK_LOW);
      unsigned int lowREF_reg ( m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_REF_CLOCK_MED);
      unsigned int medREF_reg ( m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
      m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_REF_CLOCK_HIGH);
      unsigned int highREF_reg (m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));

      uint32_t counters[64];
      if(isTest) runPatternTest(counters);

      // loop over the chips
      for (unsigned int i_chip=0; i_chip != m_NChipsPerLAMB; ++i_chip) { 
        //Read chip counter
        //This is a 24-bit counter for each chip and it is split in three separate registers
        //The first address is 11
        unsigned int regNum=((i_chip)*(3))+11;
        //Replicate address for all LAMBs
        unsigned int regAdd =  (regNum<<24)+(regNum<<16)+(regNum<<8)+(regNum);
        //Read first address
        m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, regAdd);
        unsigned int lowChipCounter_reg (m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
        //Increment address
        regNum++;
        regAdd =  (regNum<<24)+(regNum<<16)+(regNum<<8)+(regNum);
        //Read second address
        m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, regAdd);
        unsigned int medChipCounter_reg (m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
        //Increment address
        regNum++;
        regAdd =  (regNum<<24)+(regNum<<16)+(regNum<<8)+(regNum);
        //Read third address
        m_vme->write_word(AMB_VME_LAMB_INDIRECT_ADDR, regAdd);
        unsigned int highChipCounter_reg (m_vme->read_word(AMB_VME_LAMB_INDIRECT_DATA));
        // loop over the chips
        for (unsigned int i_lamb=0; i_lamb!=m_NMaxLAMBs; ++i_lamb) { 
          //define variables used for shift and indexing
          unsigned int index = i_lamb*16+i_chip;
          unsigned int shift = i_lamb*8;
          //Check the FPGA PLL is locked to the external clock
          unsigned int missLock = ((( PLL_LOCKS_2 >>shift) & 0xff ) << 8) + (( PLL_LOCKS_1 >>shift) & 0xff );
          if(missLock != 0){
            if (counts) counts[index] = 1; 
            if(i_chip == 0) PRINT_INFO("Phase measurement Failed for LAMB :" << i_lamb << " Missed PLL_LOCK count is: " << missLock );
            continue;
          }
          //Read LAMB reference clock
          unsigned int lowREF   = (( lowREF_reg  >>shift) & 0xff );
          unsigned int medREF   = (( medREF_reg  >>shift) & 0xff );
          unsigned int highREF  = (( highREF_reg >>shift) & 0xff );
          unsigned int REFclock = lowREF + (medREF<<8) + (highREF<<16);
          //if overflow repeat the test
          if( ((REFclock>>22)&0x3) != 0){// TODO this should be improved
            if (counts) counts[index] = 1; 
            if(i_chip == 0) PRINT_INFO("Phase measurement Failed for LAMB :" << i_lamb << " REFclock is in overflow: "<< REFclock );
            continue;
          }
          //Read AMChip clock xor reference clock counts
          unsigned int lowChipCounter       = ( (lowChipCounter_reg  >>  shift) & 0xff);
          unsigned int medChipCounter       = ( (medChipCounter_reg  >>  shift) & 0xff);
          unsigned int highChipCounter      = ( (highChipCounter_reg >>  shift) & 0xff);
          unsigned int chipClock = (highChipCounter<<16)+(medChipCounter<<8)+lowChipCounter;
          //if overflow repeat the test
          if( ((chipClock>>22)&0x3) != 0){
            if (counts) counts[index] += 1; 
            if(i_chip == 0) PRINT_INFO("Phase measurement Failed for Chip :" << index << " ChipCounter is in overflow. All the AM chips on this LAMB will be reconfigured!");
            continue;
          }
          // Define the phase
          float ratio = (float)(chipClock)/(float)(REFclock);
          try {
	    if (counts) counts[index] = m_PhaseTest.IsGood(i_chip, ratio)? 0 : 1;
	    else ERS_LOG("Phase measured for chip: " << i_chip << " on LAMB: " << i_lamb << " is: " << ratio );
	  } catch (daq::ftk::ftkException & e) {
            ers::error(e);
            throw e;
          }
          if(isTest){
            if(counters[index]>0)
              cout << "BAD\tChipN: " << dec << i_chip << "\tLAMB: " << i_lamb << "\t REFClock: " << hex  << REFclock << "\t chipClock: " << hex  << chipClock << "\t ratio: " << ratio << endl;
            else
              cout << "GOOD\tChipN: " << dec << i_chip << "\tLAMB: " << i_lamb << "\t REFClock: " << hex  << REFclock << "\t chipClock: " << hex  << chipClock << "\t ratio: " << ratio << endl;
          }
        } 
      }
    }


    void AMBoard::runPatternTest(uint32_t counts [64], bool isTest){
      // build the sequence of test words
      unsigned long int WCSS2 = getWCSSValue();
      WCSS2 = WCSS2 + (WCSS2<<16); // duplicate the wilcard value
      unsigned long int data[] = { WCSS2, 0x8f700FFF1};
      if (m_VerbosityLevel==DEBUG || isTest) {
        ERS_LOG(hex << "Using HIT word 0x" << WCSS2 << dec);
      }

      init(2, 2); // reset FIFOs and patternX registers, twice

      feed_hit_init(); // prepare the HIT FIFO loading
      for (unsigned int ilink=0; ilink!=12; ++ilink) {
        // load the data into the fifo
        feed_hit(data,3,ilink);
      }
      feed_hit_end(true); // send the data in loop
      m_vme->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0xFFFF); // disable all HOLDs from AUX

      // prepares variable used to check the errors
      // int NEvents;
      uint32_t counters[64];
      for(unsigned int i(0); i<64; i++) counters[i]=0;
      // loop N times. At every loop:
      //   sleep 1 second, check counter, exit if some counters are not 0

      for (unsigned tt=0; tt<10; tt++) {
        // wait some time and send an init signal to the board
        const struct timespec deltaT = { 1, 0 };
        nanosleep(&deltaT,0x0);
        getNMissingTestMatches(counters);

        // it is considered a problem to reported if a counter has >=1 mismatches
        bool doConfigure = false;
        for (int i=0;i!=64;++i) {
          if (counters[i]>0) {
            doConfigure = true; // errors found. Stop the loop and proceed to reconfigure
            break; // if boolean changed no need to scan others
          }
        }
        if (doConfigure) break;
      } // end intrnal loops
      init(0); // to stop the loop send an init to the board, level 0
      
      for (unsigned int i(0); i<64; i++) {
        unsigned int index = i - (i%2) + (1-i%2);
        counts[i] += counters[index]; //to keep a chip numbering consistent with runPhaseTest and the documentation
      }
      return;
    }


  } //namespace ftk
} //namespace daq
