/* ambslp_amchip_DTEST_select.cxx

   Author N.V. Biesuz
*/ 
#include <inttypes.h>
#include <sstream>
#include "ambslp/AMBoard.h"
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/exceptions.h"

#include <iomanip>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

using namespace std;

int main(int argc, char **argv) 
{ 
 	using namespace  boost::program_options ;

	///////////////////////////////////////////
	//  Parsing parameters using namespace boost::program:options
	//
	options_description desc("Allowed options");
	desc.add_options()
		("help,h", "Produce help message")
    ("verbose,v","If present verbose option will be used")
		("slot",   value<int>()->default_value(15), "The card slot. Default value is 15.")
		("read_write,r", value<unsigned int>(), "MANDATORY FLAG! if bit 0 is '1' the DTEST value is set in AMchips. If bit 1 is '1' the DTest value is read back from AMchips")
    ("serdes", value<string>(), "MANDATORY FLAG! SERDES name. Valid values are: patt_duplex (for pattin0), pattout, pattin1, bus0, bus1, bus2, bus3, bus4, bus5, bus6, bus7, data_available, pattout_data_valid")
    ("DTestOut", value<string>()->default_value("1"), "Signal you want to see on DTEST pin, if value is 1 or 0 it is used to test all chain are working. Valid values 1, 0, REFCLK, clock_for_lock, preLOCK, CLK40, CLK10, LOCK")
		("enable",   value<bool>()->default_value(true), "Enable the signale for this serdes.")
		;

	positional_options_description pd; 

	variables_map vm;
	try 
	{
		store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
	}
	catch( ... ) // In case of errors during the parsing process, desc is printed for help 
	{
		cerr << desc << endl;
		return 1;
	}

	notify(vm);

	if( vm.count("help") ) // if help is required, then desc is printed to output
	{
		cout << endl <<  desc << endl ;
		return 0;
	}

  /*if(vm.count("serdes")==0 or vm.count("read_write"))
  {
    std::cerr << "ERROR! Options serdaes and read_write are mandatory!" << std::endl;
    return -1;
  }*/
	int slot = vm["slot"].as<int>();
	int rw = vm["read_write"].as<unsigned int>();
  string serdes("none");
  string DTEST_mode("none");
  bool enable(false);
  if(rw&0x1){
    serdes = vm["serdes"].as<string>();
    DTEST_mode = vm["DTestOut"].as<string>();
    enable = vm["enable"].as<bool>();
  }
  bool verbose = vm.count("verbose")>0? true : false;
  
	daq::ftk::AMBoard myAMB(slot, verbose? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::INFO);

  if(rw&0x1) myAMB.DTEST_select(serdes.c_str(), DTEST_mode.c_str(), enable);
  
  if((rw>>1)&0x1){
    bool perform_test(false), expected_value(false), found_error(false);
    unsigned int lambmask(0xf);
    lambmask = myAMB.getLAMBMap();
    if(DTEST_mode.compare("1")==0){
      perform_test=true;
      expected_value=true;
    }else if (DTEST_mode.compare("0")==0){
      perform_test=true;
      expected_value=false;
    }
    std::cout << "Reading DTest values:" << std::endl;
    uint64_t dtest_values = myAMB.read_chip_DTEST();
    for(unsigned int i_lamb=0; i_lamb<MAXLAMB_PER_BOARD; i_lamb++){
      if(not ((lambmask>>i_lamb)&0x1)) continue;
      for(unsigned int i_chain=0; i_chain<MAXVMECOL_PER_LAMB; i_chain++){
        for(unsigned int i_chip=0; i_chip<MAXCHIP_PER_VMECOL; i_chip++){
          std::stringstream print_buffer;
          print_buffer << "LAMB: " << i_lamb;
          print_buffer << " CHAIN: " << i_chain;
          print_buffer << " CHIP: " << i_chip;
          bool value = ( dtest_values>>( (i_lamb*MAXCHIP_PER_LAMB)+(i_chain*MAXCHIP_PER_VMECOL)+i_chip) )&0x1;
          print_buffer << " value: " << value;
          std::cout << print_buffer.str() << std::endl;
          if(perform_test and value!=expected_value) found_error = true;
        }
      }
    }
    if(found_error and perform_test) std::cout << "POSSIBLE ISSUE IN DTESTOUT!!! PLease check!" << std::endl;
  }

	return 0;
}



