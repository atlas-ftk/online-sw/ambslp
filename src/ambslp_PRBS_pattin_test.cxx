/*
This script tests the AMchip input links in a few steps:
1)Enable PRBS generator 2)Input links are tested 3)Disable PRBS 4)Retest 5)Enable PRBS 6)Retest 7)Stop for 10 seconds 8)Retest 9)Stop for 10 minutes 10)Retest
If an error is found at any step the test stops and a detailed output is printed.
*/
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"
#include "ambslp/exceptions.h"
#include "ftkcommon/core.h"
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <thread>         // std::this_thread::sleep_for 
#include <chrono>         // std::chrono::seconds 
int main(int argc, char **argv){
  using namespace  boost::program_options ;

  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("quick,q","If present the test will run without the 10 minutes break")
    ("verbose,v", "If present verbose option will be used")
    ;

  positional_options_description pd;
  pd.add("pattern_file", 1);


  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);


  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chainlength = MAXCHIP_PER_VMECOL;
  int chipnum = 3; //The chips to read. Default value is 3=0x3 (two chips).
  bool pattin0Passed = true;
  bool pattin1Passed = true;

  // create the AMB object
  int int_verbose = vm.count("verbose")>0 ? 1 : 0;
  bool verbose(false);
  if (int_verbose==1) verbose=true;

  int int_quick = vm.count("quick")>0 ? 1 : 0;
  bool quick(false);
  if (int_quick==1) quick=true;

  printf("\nEnabling the PRBS generator and testing pattin0, takes around 5 seconds...\n");
  daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "disable", "patt_duplex", verbose);
  daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "enable", "patt_duplex", verbose);

  //fourth option is set to true when the PRBS generator is enabled and to false when it is disabled
  pattin0Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin0", true );

  if(pattin0Passed)//Go to pattern mode
    {
      printf("First test passed! Now desabling the PRBS generator and retesting, takes around 5 seconds...\n");
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "disable", "patt_duplex", verbose);
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "pattern", "patt_duplex", verbose);
      pattin0Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin0", false );
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "enable", "patt_duplex", verbose);
    }

  if(pattin0Passed)
    {
      printf("Test with PRBS generator disabled passed! Now enabling the PRBS generator and retesting, takes around 5 seconds...\n");
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "disable", "patt_duplex", verbose);
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "enable", "patt_duplex", verbose);
      pattin0Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin0", true );
      if(pattin0Passed)
	{
	  printf("Second test passed! Stopping 10 seconds and then retesting...\n");
	  std::this_thread::sleep_for (std::chrono::seconds(10));
	  pattin0Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin0", true );
	  if(pattin0Passed && !quick)
	    {
	      printf("Third test passed! Stopping 10 minutes and then retesting...\n");
	      std::this_thread::sleep_for (std::chrono::seconds(10*60));
	      pattin0Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin0", true );
	    }
	}
    }

  if(pattin0Passed){printf("\nGood! All pattin0 test on the input links passed!!!\n");}
  else{printf("\nPattin0 test not passed, details are printed above!\n");}

  printf("\nEnabling the PRBS generator and testing pattin1, takes around 5 seconds...\n");
  daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "disable", "pattin1", verbose);
  daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "enable", "pattin1", verbose);

  //fourth option is set to true when the PRBS generator is enabled and to false when it is disabled
  pattin1Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin1", true );

  if(pattin1Passed)//Go to pattern mode
    {
      printf("First test passed! Now desabling the PRBS generator and retesting, takes around 5 seconds...\n");
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "disable", "pattin1", verbose);
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "pattern", "pattin1", verbose);
      pattin1Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin1", false );
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "enable", "pattin1", verbose);
    }

  if(pattin1Passed)
    {
      printf("Test with PRBS generator disabled passed! Now enabling the PRBS generator and retesting, takes around 5 seconds...\n");
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "disable", "pattin1", verbose);
      daq::ftk::ambslp_amchip_prbs_error_count(slot, chainlength, chipnum, "a", "enable", "pattin1", verbose);
      pattin1Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin1", true );
      if(pattin1Passed)
	{
	  printf("Second test passed! Stopping 10 seconds and then retesting...\n");
	  std::this_thread::sleep_for (std::chrono::seconds(10));
	  pattin1Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin1", true );
	  if(pattin1Passed && !quick)
	    {
	      printf("Third test passed! Stopping 10 minutes and then retesting...\n");
	      std::this_thread::sleep_for (std::chrono::seconds(10*60));
	      pattin1Passed = daq::ftk::ambslp_amchip_polling_stat_reg_pattin(slot, chainlength, chipnum, "a", "pattin1", true );
	    }
	}
    }

  if(pattin1Passed){printf("\nGood! All pattin1 test on the input links passed!!!\n");}
  else{printf("\nPattin1 test not passed, details are printed above!\n");}
    
  return (pattin0Passed && pattin1Passed) ;

}

