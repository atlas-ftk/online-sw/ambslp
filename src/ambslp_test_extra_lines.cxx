/* ambslp_test_extra_lines.cxx

   Author A. Annovi 2004
*/

#include "ambslp/AMBoard.h"
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/exceptions.h"

#include <iomanip>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

using namespace std;

int main(int argc, char **argv)
{
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< string >()->default_value("15"), "The card slot. Default value is 15.")
    //              ("chainlength", value< string >()->default_value("2"), "The chain length. Default value is 4.")
    //              ("chipnum", value< string > ()->default_value("3"), "The chips to read. Default value is 15=0xF.")
    ("verbose,v", "If present verbose option will be used")

    ;

  positional_options_description pd;
  pd.add("pattern_file", 1);

  variables_map vm;
        try
          {
            store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
          }
        catch( ... ) // In case of errors during the parsing process, desc is printed for help
          {
            cerr << desc << endl;
            return 1;
          }

        notify(vm);

        if( vm.count("help") ) // if help is required, then desc is printed to output
          {
            cout << endl <<  desc << endl ;
            return 0;
          }

        int slot = daq::ftk::string_to_int( vm["slot"].as<string>() );       
        bool verbose = vm.count("verbose")>0? true : false;

        //daq::ftk::AMBoard myAMB(slot, verbose? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::INFO);
        daq::ftk::AMBoard myAMB(slot, verbose? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::WARNING);

	return myAMB.testExtraLines(slot, verbose);
        
}
