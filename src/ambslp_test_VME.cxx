/*           Program to test the FLASH RAM functions                 */
/*           Written for AMBSLP board v5.0   FTK collaboration       */

//#include "rcc_error/rcc_error.h"
//#include "vme_rcc/vme_rcc.h"
//#include "ambslp/ambslp.h"

#include "ambslp/ambslp_mainboard.h"
#include "ambslp/ambslp_vme_regs.h"
#include "ftkvme/VMEManager.h"
#include "ambslp/exceptions.h"
#include "ftkcommon/core.h"

namespace daq {
  namespace ftk {
    namespace amb_vme {
      

      } // namespace amb_vme
  } // namespace daq
} // namespace ftk



//////////////////////////////////////////////////////////////////////////


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

// Parsing input parmaeters and calling the am_init function
int main(int argc, char **argv) 
{ 
 
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("loop", value< std::string >()->default_value("1"), "executes n times, 0 means infinte loop")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    //    ("amb_lamb", value< std::string >()->default_value("0"),"0=Reset ONLY the AMboard, 1=Reset ONLY the LAMBs, 2=Reset ONLY the FIFOs memory inside AMBoard, 3=Reset configuration registers")
    //("verbose_level", value< std::string >()->default_value("1"),"0=RunControl, 1=StandAlone")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      FTK_VME_ERROR( "- am_init.cxx : Failure in command_line_parser" );
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      ERS_LOG( desc ); 
      return 0;	
    }

  int loop = daq::ftk::string_to_int( vm["loop"].as<std::string>() );
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  //  int amb_lamb = daq::ftk::string_to_int( vm["amb_lamb"].as<std::string>() );
  //int verbose_level = daq::ftk::string_to_int( vm["verbose_level"].as<std::string>() );

  //
  ///////////////////////////////////////////

  // try to connect to the amb via vme
  VMEInterface *vme = 0;
  try {  vme = VMEManager::global().amb(slot);  }
  catch (daq::ftk::VmeError &ex) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
    throw issue; 
  }

  // calling the function and checking the result
  return  daq::ftk::ambslp_test_VME(vme, loop);  
}


