#include "ambslp/ambslp_vme_jtag_func.h"

////////// LIBRARY : ADDED THE DEFINITIONS for THIS ARRAYS ///////////
int poffset[MAXLAMB_PER_BOARD*MAXVMECOL_PER_LAMB][MAXCHIP_PER_VMECOL];
int roadaddr[MAXAMCHIPS];
int roadaddr_new[MAXAMCHIPS];
//////////////////////////////////////////////////////////////////////


int patt2addr(int pattnum)
{
  /* convert a sequential pattern number from 0 to 5119 to a pattern address
     from 0 to 7423 */
  return pattnum; //BLOCK_ADDRSPACE*(pattnum/NUMPATT_INBLOCK)+(pattnum%NUMPATT_INBLOCK);
}

/* ========================================================================== */
void getVmeJtag( vme_jtag* info ) {
 

  // federico: non capisco un benemerito 
 
  // throwing an exception in case the passted vme_jtag is a zero-pointer
  if ( !info ) {
    daq::ftk::ftkException issue( ERS_HERE, name_ftk(), "invalid vme_jtag pointer passed");
    throw issue;
  }

  unsigned long active_columns =0;
  unsigned long chainlength =0;
  int iLamb, jVmeCol, kChip;
 
  for (iLamb=0; iLamb<MAXLAMB_PER_BOARD; iLamb++)
    for (jVmeCol=0; jVmeCol<MAXVMECOL_PER_LAMB; jVmeCol++)
      if (daq::ftk::lamb::lambcfg[iLamb] & (0xF << 4*jVmeCol) )
	{
	  unsigned int tmp_chainlength=0;

	  active_columns|= (1 <<(jVmeCol+iLamb*MAXVMECOL_PER_LAMB));
	  for (kChip=0; kChip<4; kChip++) /* count active chips */
	    if ((daq::ftk::lamb::lambcfg[iLamb]>> (4*jVmeCol+kChip)) & 0x1)
	      tmp_chainlength++;

          // if here, some problem has occurred
          //  throw exception
	  if (tmp_chainlength!=chainlength) {
            if ( chainlength != 0 ) {
              daq::ftk::ftkException issue ( ERS_HERE, name_ftk(), "empty chainlenght found when expected non-empty!");
              throw issue;
            }
	    chainlength = tmp_chainlength ;
	  }
	}

  info->active_columns = active_columns;
  info->chainlength = chainlength;
}

/* ========================================================================== */
int getroadaddr(int lamb, int jtagcol, int chipnum, int pattnum)
{
  // CDF addressing
    //return pattnum+(lamb<<18)+(jtagcol/2<<16)+(getgeoadd(jtagcol, chipnum)<<13);
  //MD 02.05.2011 funziona con indirizzo 21 bit (non so bene perche') 
  //return pattnum+(lamb<<21)+(jtagcol/2<<19)+(getgeoadd(jtagcol, chipnum)<<13);
  int geoadd_0, geoadd_1;
  geoadd_0 = getgeoadd(/*jtagcol,*/ chipnum) & 0x1;  // jtagcol unused function
  printf("geoadd %x\n", geoadd_0);
  geoadd_1 = (getgeoadd(/*jtagcol,*/ chipnum)>>1) & 0x1; // jtagcol unused function

  return pattnum + (lamb<<20) + (jtagcol<<17) + (geoadd_0<<13) + (geoadd_1<<16);

  
  //SLIM addressing
  //return pattnum+(lamb<<21)+(jtagcol/2<<17)+(getgeoadd(jtagcol, chipnum)<<13);
}

/* ========================================================================== */
void AMcfg(/*unsigned long lambcfg[MAXLAMB_PER_BOARD],*/ int nPattPerChip)
{

  
  unsigned long active_columns = 0; 
  int chainlength = 0;

  unique_ptr<vme_jtag> vmejtag_info( new vme_jtag());
  try { getVmeJtag( vmejtag_info.get() ); }
  catch( daq::ftk::FTKIssue &ex ) {
    daq::ftk::ftkException issue ( ERS_HERE, name_ftk(), "AMcfg failure on getting vme-jtag info");
    throw issue;
  }

  chainlength = vmejtag_info->chainlength;
  active_columns = vmejtag_info->active_columns;

  int i,j,k;

  int chipcount=0;
  for (i=0; i<MAXLAMB_PER_BOARD; i++) /* loop over LAMBs */
    for (j=0; j<MAXVMECOL_PER_LAMB; j++) /* loop over VME columns */
      {
	int COL[MAXVMECOL_PER_LAMB] = {1, 0, 3, 2};//, 4, 5, 6, 7}; /* sort VME column following paddress */
	int tmpcolumn = COL[j]+i*MAXVMECOL_PER_LAMB;

	if ((active_columns >> tmpcolumn)&0x1)
	  {
	    for (k=0;        k<chainlength; k++) /* loop over chips in chain */
	      {
		int chipnum = chipcount;
		
		if (COL[j]<MAXVMECOL_PER_LAMB/2)  /* if column has rev_en_Low HIGH */
		  chipnum += k;
		else
		  chipnum += chainlength-1-k;		    

		roadaddr[chipnum] = getroadaddr(i, COL[j], k, 0);
	//	roadaddr_new[chipnum] = ((i%4)<<16)+(((COL[j]/2)%4)<<14)+((chipnum%4)<<12);

		poffset[tmpcolumn][k]=nPattPerChip*(chipnum);
	
	      }
	    chipcount+=chainlength;
	  }
      }
}

/* ========================================================================== */
int getgeoadd(/*int jtagcol,*/ int chipnum) // jtagcol is unused var; actually, strange function... to be fixed?
{
  return chipnum;
  /*switch (jtagcol)    //if all chian chip is mounted
    {
    case 0:
    case 2:
    case 4:
    case 6: 
      return chipnum;
      break;
    case 1:
    case 3:
   case 5:
    case 7: 
      return 3-chipnum;
      break;
    default:
      return -1;
      }*/

   /*switch (jtagcol)
    {
    case 1:
    case 3:
    case 4:
    case 6: // chip is far from glue 
      //return (jtagcol<3) ? chipnum : 3-chipnum;
      return (jtagcol<=3) ? chipnum : 3-chipnum; //MD 22.03.2012
      break;
    case 0:
    case 2:
    case 5:
    case 7: // chip is near to glue 
      return (jtagcol<3) ? 4+chipnum : 7-chipnum;
      break;
    default:
      return -1;
      }*/
}

/* ========================================================================== */

/* ========================================================================== */
int getNchips(/*unsigned long lambcfg[MAXLAMB_PER_BOARD]*/)
{
  int i, j;
  int totchips=0;

  for (i=0; i<MAXLAMB_PER_BOARD; i++)
    for (j=0; j<MAXCHIP_PER_LAMB; j++)
      if ( (daq::ftk::lamb::lambcfg[i]>>j) & 0x1 )
	totchips++;

  return totchips;
}
