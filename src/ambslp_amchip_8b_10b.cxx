/* ambslp_amchip_8b_10b.cxx

   Author N.Biesuz nicolo.vladi.biesuz@cern.ch 2014
*/

#include "ambslp/ambslp_amchip.h"
#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <ctime>
using namespace std;

int main(int argc, char **argv) 
{ 
	
  using namespace  boost::program_options ;

  unsigned int nTries(10);

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("verbose,v", "Verbosity level")
    ("mode",value<unsigned int>()->default_value(3),"The type of SERDES to be reset")
    ("slot", value<unsigned int>()->default_value(15), "The card slot. Default value is 15.")
    ("smart","Check the test patterns and reset only the chips with missing matches")
    ("test","Don't do anything, only use the preparation infrastructure")
    ("nTries,N", value<unsigned int>(&nTries)->default_value(nTries), "Maximum number of attempts")
    ;

  positional_options_description pd; 
  pd.add("pattern_file", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      cout << endl <<  desc << endl ;
      return 0;
    }

  unsigned int verbosity = vm.count("verbose");

  int slot = vm["slot"].as<unsigned int>();
  unsigned int reset_mode = vm["mode"].as<unsigned int>();

  bool smart = vm.count("smart")>0 ? true : false;

  // the return is set according the program flow, 0 is ok !=0 problems
  int resVal(0);

  daq::ftk::AMBoard myAMB(slot, verbosity>2 ? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::WARNING);

  bool isTest = vm.count("test")>0;

  cout << "Resetting links: " << reset_mode << endl;

  if (!smart) {
    // reset SERDES, the method amchip8b10b setup the SERDES for all the bus and chips
    resVal = myAMB.amchip8b10b(reset_mode);
  } else {
    // reset AM chip input (bus0-7) SERDES if requested 
    if (reset_mode&0x1)
      resVal = myAMB.amchip8b10b(0x1);

    // exit in case of errors
    if (resVal) return resVal;

    // reset ONLY the AM chip OUTPUT SERDES using the smart mode
    resVal = myAMB.amchip8b10b_smart(nTries, isTest);
  }

  // if the test flag is set the return value is taken from previous flow
  return resVal;
}

