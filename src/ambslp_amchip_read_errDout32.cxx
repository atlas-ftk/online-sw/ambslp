#include "ambslp/AMBoard.h"
#include <ambslp/ambslp_mainboard.h>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <string>
#include <cstdlib>
using namespace boost::program_options;
using namespace std;

string PartName("FTK.data.xml"), RMName("Amb1");
bool doCount(false);
bool doUp(false);
bool doCheck(false);

int main(int argc, char **argv) 
{ 

  //setting the FTK_LOAD enviroment variable to force the loading of the configurations
  std::cout << "ambslp_procedures: setting FTK_LOAD_MODE to true" << std::endl;
  setenv("FTK_LOAD_MODE", "1", true);
 	
  options_description desc("Allowed options");
//  const unsigned int useDefaultDummyHit = 0xdeadbeef;
  desc.add_options()
    ("help,h", "produce help message")
    ("slot", value<int>()->default_value(15), "The card slot. Default value is 15.")
    ("serDesMode", value<int>()->default_value(0), "00 - normal operation, 01 - PRBS7 mode, 10 - Pattern mode, 11 - Loopback mode")
    ("doCount,C", value<bool>(&doCount)->default_value(doCount), "Do error counting") 
    ("doUp", value<bool>(&doUp)->default_value(doUp), "Do link enabling")
    ("doCheck", value<bool>(&doCheck)->default_value(doCheck), "Do link checking")
    ("time", value<int>()->default_value(100000), "duration of the error counting in nano-seconds")
    ("PartName,P", value<string>(&PartName)->default_value(PartName), "Partition name, the partition from wich load the configuration")
    ("RMName,R", value<string>(&RMName)->default_value(RMName), "ReadoutModule name, the RM in use")
    ;

  positional_options_description pd;
  pd.add("transition", -1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  int slot = vm["slot"].as<int>();
  int serDesMode = vm["serDesMode"].as<int>();
  int time = vm["time"].as<int>();

  std::string partition = "oksconfig:ftk/partitions/" + PartName;	
  std::cout << "Partition Name: " << partition << std::endl;
  std::cout << "RMName: " << RMName << std::endl;
  
  // Read some info from the DB
  std::cout << "Loading OKS Configuration." << std::endl;
  std::unique_ptr<::Configuration> db;
  try 
	{
    db.reset(new::Configuration(partition));
  } 
	catch(daq::config::Generic e) 
	{
    std::cerr << "ERROR: Cannot load OKS partition." << std::endl;
		throw e; 
    return -1;
  }
  
  const daq::ftk::dal::ReadoutModule_PU* settings = db->get<daq::ftk::dal::ReadoutModule_PU>(RMName);
  if(settings==0)
  {
    std::cerr << "Invalid tag " << RMName << "! exiting..." << std::endl;
    return -1;
  }
  
  daq::ftk::AMBoard myAMB(slot);
  myAMB.RC_setup(settings);	

  if(doCount){
    std::vector<std::vector<unsigned int>> errorDout32_count;
    std::vector<std::vector<unsigned int>> tx_stream_error;
    std::vector<std::vector<unsigned int>> rx_stream_error;
    errorDout32_count.resize(MAXAMCHIPS);
    tx_stream_error.resize(MAXAMCHIPS);
    rx_stream_error.resize(MAXAMCHIPS);
    for(int i=0; i<MAXAMCHIPS; i++){
      errorDout32_count[i].resize(NLAYERS);
      tx_stream_error[i].resize(NLAYERS);
      rx_stream_error[i].resize(NLAYERS);
    }
    
    myAMB.amchip8b10b_count_errDout32(errorDout32_count, rx_stream_error, tx_stream_error, 0x1, 2, 3, time, serDesMode);
    
    for(int i=0; i<MAXAMCHIPS; i++){
      for(int j=0; j<NLAYERS; j++){
	ERS_LOG("Chip "<<i<<" bus "<<j<<": errDout32 = 0x"<< std::hex << errorDout32_count[i][j] << "  RxStreamError = "<< rx_stream_error[i][j] <<"  TxStreamError = "<<tx_stream_error[i][j]);
      }
    }
    
  }

  if(doUp || doCheck){
    myAMB.amchip8b10b_up_and_check((doCheck? 0x03 : 0x0), 1, 2, 3, doUp);
  }   
 
}

