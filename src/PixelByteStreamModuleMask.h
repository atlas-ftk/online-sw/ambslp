#ifndef PIXELRAWDATABYTESTREAMCNV_PIXELBYTESTREAMMODULEMASK_H
#define PIXELRAWDATABYTESTREAMCNV_PIXELBYTESTREAMMODULEMASK_H

// module header word
const uint32_t PRB_BCIDmask = 0xFF;   // BCID header word mask
const uint32_t PRB_BCIDskip = 0;   // BCID header word skip
const uint32_t PRB_L1IDmask = 0xF;   // L1ID header word mask
const uint32_t PRB_L1IDskip = 8;   // L1ID header word skip
const uint32_t PRB_L1IDSKIPmask = 0xF;   // L1ID skip header word mask
const uint32_t PRB_L1IDSKIPskip = 12;	// L1ID skip header word skip
const uint32_t PRB_MODULEmask = 0x7F;	// module link header word mask
const uint32_t PRB_MODULEskip = 16;   // module link header word skip
const uint32_t PRB_MODULEerrormask = 0x4; // module link header boundary check mask
const uint32_t PRB_HEADERERRORSmask = 0xF;   // header errors header word mask
const uint32_t PRB_HEADERERRORSskip = 25;   // header errors header word skip
const uint32_t PRB_HEADERBITFLIPmask = 0x3;
const uint32_t PRB_HEADERBITFLIPskip = 23;
// module hit word
const uint32_t PRB_ROWmask = 0xFF;   // row hit word mask
const uint32_t PRB_ROWskip = 0;   // row hit word skip
const uint32_t PRB_COLUMNmask = 0x1F;	// column hit word mask
const uint32_t PRB_COLUMNskip = 8;   // column hit word skip
const uint32_t PRB_TOTmask = 0xFF;  // TOT hit word mask
const uint32_t PRB_TOTskip = 16;   // TOT hit word skip
const uint32_t PRB_FEmask = 0xF;   // FE hit word mask
const uint32_t PRB_FEskip = 24;   // FE hit word skip
const uint32_t PRB_HITBITFLIPmask = 0x8007;
const uint32_t PRB_HITBITFLIPskip = 13;
// module trailer word
const uint32_t PRB_TRAILERERRORSmask = 0x7;   // trailer errors trailer word mask
const uint32_t PRB_TRAILERERRORSskip = 26;   // trailer errors trailer word skip
const uint32_t PRB_TRAILERBITFLIPmask = 0x3FFFFFF;
const uint32_t PRB_TRAILERBITFLIPskip = 0;
// module flag type 1 words
const uint32_t PRB_FEFlagmask1 = 0xF;	// FE flags type 1 word mask
const uint32_t PRB_FEFlagskip1 = 0;   // FE flags type 1 word skip
const uint32_t PRB_FE2mask = 0xF;   // FE flag second FE number mask (to compare with first Circuitnumber)
const uint32_t PRB_FE2skip = 4;   // FE flag second FE number skip
const uint32_t PRB_FEFLAG1BITFLIPmask = 0x7FF;
const uint32_t PRB_FEFLAG1BITFLIPskip = 13;
// module flag type 2 words
const uint32_t PRB_FEFlagmask2 = 0xFF;   // FE flags type 2 word mask
const uint32_t PRB_FEFlagskip2 = 0;   // FE flags type 2 word skip
const uint32_t PRB_MCCFlagmask = 0x1F;   // FE flags type 2 word mask
const uint32_t PRB_MCCFlagskip = 8;   // FE flags type 2 word skip
const uint32_t PRB_FEFLAG2BITFLIPmask = 0x7;
const uint32_t PRB_FEFLAG2BITFLIPskip = 21;
// word type masks (just use bits 29-31)
const uint32_t PRB_DATAMASK    = 0xE0000000;   // word type mask
const uint32_t PRB_LINKHEADER  = 0x20000000;   // module header word mask
const uint32_t PRB_LINKTRAILER = 0x40000000;   // module trailer word mask
const uint32_t PRB_RAWDATA     = 0x60000000;   // raw data word mask
const uint32_t PRB_DATAWORD    = 0x80000000;   // hit word mask
// special word type masks
const uint32_t PRB_TIMEOUT     = 0x20004000;   // timeout word mask - does not use word type mask
const uint32_t PRB_FEERRORMASK = 0xF0000000;   // flag word type mask
const uint32_t PRB_FEERROR1    = 0x00000000;   // type 1 flag word
const uint32_t PRB_FEERROR2    = 0x10000000;   // type 2 flag word
const uint32_t PRB_UNKNOWNWORD = 0xFFFFFFFF;   // unknown word
// word empty bits (filled by ROD with 0's) masks
const uint32_t PRB_LINKHEADEREMPTY     = 0x01800000;   // module header word empty bits (filled by ROD with 0's) mask
const uint32_t PRB_DATAWORDEMPTY       = 0x1000E000;   // hit word empty bits (filled by ROD with 0's) mask
const uint32_t PRB_LINKTRAILEREMPTY    = 0x03FFFFFF;   // module trailer word empty bits (filled by ROD with 0's) mask
const uint32_t PRB_FEERROR1CHECK       = 0x00FFFF00;   // flag word type 1 determined bits mask
const uint32_t PRB_FEERROR2CHECK       = 0x00FF0000;   // flag word type 2 determined bits mask
const uint32_t PRB_FEERROR1CHECKRESULT = 0x00001E00;   // flag word type 1 determined bits mask result
const uint32_t PRB_FEERROR2CHECKRESULT = 0x001F0000;   // flag word type 2 determined bits mask result

#endif
