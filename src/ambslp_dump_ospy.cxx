#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/******************************/
int main(int argc, char *argv[])
/******************************/
{
	using namespace  boost::program_options ;

	///////////////////////////////////////////
	//  Parsing parameters using namespace boost::program:options
	//
 	options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
   	("status", value< std::string >()->default_value("0"), "The status mode")
    ;

  variables_map vm;
	try {
  	store(command_line_parser(argc, argv).options(desc).run(), vm);
 	}
	catch( ... ) // CHecing for error during the parsing process, desc is for help 
	{
		std::cerr << desc << std::endl; 
		return 1;
	}

	notify(vm);

	if( vm.count("help") ) // if required printing help
	{	
		std::cout << std::endl <<  desc << std::endl ; 
		return 0;	
 	}
 	
	int slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
	int statusm = daq::ftk::string_to_int( vm["status"].as<std::string>() );
	///////////////////////////////////////////

  return daq::ftk::ambslp_dump_ospy( slot, statusm);

}


