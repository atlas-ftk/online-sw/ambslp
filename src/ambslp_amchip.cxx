/* ambslp_amchip.cxx

   author nicolo.vladi.biesuz@cern.ch 2015

*/
//this file contains a library with few useful functions to setup the amchip parameters
//functions are written for AMchip 05

#include "ambslp/ambslp_amchip.h"

/** @fn bool ambslp_amchip_SERDES_setup(int slot, int chainlength, int chipnum, const char* columns, const char* serdes, const char* operation_mode, float frequency)
 *
 * @brief This function sets up the serdes of amchip 05 and amchip 06
 *
 * @param slot Vme slot in which the board is plugged
 * @param chainlength number of chips per JTAG chain
 * @param chipnum Mask the chip you want to program
 * @param columns Columns you want to anct on
 * @param serdes SERDES you want to set-up
 * @param operation_mode Operation mode of the SERDES
 * @param frequency Opearating frequency of the serdes in Gbit/s
 *
 * This function sets up the serdes of amchip 05 and amchip 06
 * columns is an indication of the jtag columns to be addressed, 'a' stands for all, 'e' stands for even, 'o' stands for odd
 * serdes is an indication of the serdes to be set up, allowed options:
 *  'patt_duplex', indicating pattin 0 and pattout;
 *  'pattin0';
 *  'pattin1';
 *  'pattout';
 *  'bus0';
 *  'bus1';
 *  'bus2';
 *  'bus3';
 *  'bus4';
 *  'bus5';
 *  'bus6';
 *  'bus7'.
 * operation_mode is an indication of the serdes operation mode, allowed options are:
 *  'normal', serdes performs 8b 10b decode/encode;
 *  'prbs',  serdes sends/expect a pseudo random bit sequence with period 2^7-1; 
 *  'fixed_pattern'serdes send a fixed pattern.
 * frequency is the frequency of operation expressed in Gbit/second: the clock multiplier and divider are evaluated assuming a reference clock frequency of 100 MHz
 *
 */

bool ambslp_amchip_SERDES_setup(int slot, int chainlength, int chipnum, const char* columns, const char* serdes, const char* operation_mode, float frequency){

  std::array<uint32_t,MAXDATAWORDS> outdata;
  int active_columns(0);
  int chipno(0);
  unsigned int i(0);
  int serdes_number(0),rx_tx_enable(0), mode(0);

  //chipcolumns you want to act on
  int all_chipcolumn = 0;
  int even_chipcolumn=0;
  int odd_chipcolumn=0;
  for(unsigned int t=0;t<MAXCOLUMNS;t++){
    if((t%2)==0) even_chipcolumn |= 1<<t;
    else odd_chipcolumn |= 1<<t;
    all_chipcolumn |= 1<<t;
  }
  if(strcmp(columns, "a")==0) active_columns = all_chipcolumn;
  else if (strcmp(columns, "o")==0) active_columns = odd_chipcolumn;
  else if (strcmp(columns, "e")==0) active_columns = even_chipcolumn;
  else{
    FTK_STRING_PARAM_ERROR("error: Found an invalid option for columns: '"<<columns<<"'. Abort!");
    return false;
  }
  /*TODO use get_serdes_number function*/
  //serdes you want to act on
  if(strcmp(serdes, "patt_duplex")==0){
    serdes_number = 0x0;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "pattout")==0){
    serdes_number = 0x0;
    rx_tx_enable=0x1;
  }else if (strcmp(serdes, "pattin1")==0){
    serdes_number = 0x2;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus0")==0){
    serdes_number = 0x8;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus1")==0){
    serdes_number = 0x9;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus2")==0){
    serdes_number = 0xa;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus3")==0){
    serdes_number = 0xb;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus4")==0){
    serdes_number = 0xc;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus5")==0){
    serdes_number = 0xd;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus6")==0){
    serdes_number = 0xe;
    rx_tx_enable=0x3;
  }else if (strcmp(serdes, "bus7")==0){
    serdes_number = 0xf;
    rx_tx_enable=0x3;
  }else{
    FTK_STRING_PARAM_ERROR("error: Found an invalid option for serdes name: '"<<serdes<<"'. Abort!");
    return false;
  }
  PRINT_DEBUG_VME("Setting up operation mode");
  //operation mode selection
  if(strcmp(operation_mode, "normal")==0) mode=0x0;
  else if(strcmp(operation_mode, "prbs")==0) mode=0x1;
  else if(strcmp(operation_mode, "fixed_pattern")==0){
    if(rx_tx_enable&0x1){
      mode=0x2;
    }else{
      FTK_STRING_PARAM_ERROR("error. fixed patten mode available only for transmitter! Abort!");
      return false;
    }
  }
  else{
    FTK_STRING_PARAM_ERROR("error: Found an invalid option for columns: '"<<columns<<"'. Abort!");
    return false;
  }
  PRINT_DEBUG_VME("Setting up frequency");
  //frequency set up
  int prod = (int)(frequency*1E1);
  /// int count(0);   // unused var
  if(not ((frequency*1E1)-prod==0)){
    PRINT_INFO("ambslp_amchip::serdes_serup(): got a wrong bit rate, setting up default bit rate og 2.0 Gbit/s!");
    frequency = 2.0;
    prod = (int)(frequency*1E1);
  }
  int div(1), mult(1);
  PRINT_DEBUG_VME("Getting a list of prime numbers");
  //getting a list of prime numbers lower than target product
  std::vector<int> prime_numbers;
  prime_numbers.reserve(prod);
  for(int num=2; num<prod; num++){
    int n_div(0);
    for(int div=2; div<num; div++){
      if((num%div)==0) n_div++;
      else continue;
    }
    if(n_div==0) prime_numbers.push_back(num);
  }
  PRINT_DEBUG_VME("Factorizing frequency");
  //fattorizzo il la frequenza
  int serv_prod(prod);
  std::vector<int> fac;
  fac.reserve(prod);
  PRINT_DEBUG_VME("prime numbers size: "<<prime_numbers.size());
  for(i=0; i<prime_numbers.size();i++){
    PRINT_DEBUG_VME("prime_numbers[i]: "<<prime_numbers.at(i));
    while((serv_prod%prime_numbers.at(i))==0){
      fac.push_back(prime_numbers.at(i));
      serv_prod=serv_prod/prime_numbers.at(i);
    }
  }
  for(i=0;i<fac.size();i++){
    div=div*fac.at(i);
    mult = prod*div;
    if((100/div)<15){
      div=div/fac.at(i);
      mult = prod*div;
      break;
    }
    if((uint)mult>0xffffffff){
      div = div/fac.at(i);
      mult = prod*div;
      break;
    }
  }

  PRINT_INFO("div: 0x"<<std::hex<<div);  
  PRINT_INFO("mult: 0x"<<std::hex<<mult);

  PRINT_INFO("Manually setting div and mult for SERDES  ");  
  div = 10.;
  mult = 200.;
  PRINT_INFO("div: 0x"<<std::hex<<div);  
  PRINT_INFO("mult: 0x"<<std::hex<<mult);
  /* build chip number */
  for (i=0, chipno=0; (int)i<chainlength; i++) chipno = (chipno<<1) + 1; 
  chipno = (chainlength<<8) + chipnum;

  PRINT_DEBUG_VME("chipno "<<std::hex<<chipno);  
  PRINT_DEBUG_VME("chainlength "<<std::hex<<chainlength);

  GotoTestReset(slot); //6 colpi di TCK con TMS alto: TRST non funziona. Stato di reset di tap controller.
  GotoIdlefromReset(slot, all_chipcolumn); //un colpo di TCK con TMS 0: idle
  //INIZIO CONFIGURAZIONE BUS0!!!!!!**************************************

  //***scrivo in C9 0x22 ************************
  std::array<bool,7> wrdata_ir;
  int DR = 0x00000020+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true);
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x10500401************************ Reset della pipe interface && powerdown dei SERDES
  std::array<bool,32> wrdata;
  DR = 0x00000000;
  DR = 0x10000001+(mult<<16)+(div<<8);
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true); 
  PrintRegisterContent(chipno, 32, outdata.data());   

  //***scrivo in C9 0x22 ************************
  DR = 0x00000000;
  DR = 0x00000020+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(),0);
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 

  //***scrivo in CA 0x00500401************************ Tolgo SERDES da reset: powerdown dei SERDES
  DR = 0x00000000;
  DR = 0x00000001+(mult<<16)+(div<<8);
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true); 
  PrintRegisterContent(chipno, 32, outdata.data());  

  //Scrivo 0x12 nel registro SERDES_SEL *****************************************
  DR = 0x00000000; //preparo il dato da scrivere in SERDES_SEL
  DR = 0x10+serdes_number; //preparo il dato da scrivere in SERDES_SEL
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data()); //scrivo dato in SERDES_SEL
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true); //scrivo dato in SERDES_SEL
  //rileggo il dato 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //reset delle variabili

  //***scrivo in CA 0x00000000 ************************ Built In self Test dei SERDES: funziona solo per la coppia pattin0/pattout
  DR = 0x00000000;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());//scrivo il dato
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);//scrivo il dato
  //leggo il dato scritto
  PrintRegisterContent(chipno, 32, outdata.data());   

  //***scrivo in C9 0x32 *************************
  DR = 0x00000000; 
  DR = 0x00000030+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true);
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x00000000 ************************ Aggiustamento fine del pll di TX
  DR = 0x00000000;
  DR = 0x00000000;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);
  PrintRegisterContent(chipno, 32, outdata.data());   

  //***scrivo in C9 0x42 *************************
  DR = 0x00000000;
  DR = 0x00000040+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x015020xx*************************
  //Enable: TX-RX; Enable: TX-RX termination; termination trimmer to nominal value; DTestOut == RXclock40; Enable DTestOut
  DR = 0x00000000; 
  DR = 0x01002000+(rx_tx_enable<<4)+rx_tx_enable;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);
  PrintRegisterContent(chipno, 32, outdata.data());   
  //***scrivo in C9 0x52 ************************ 
  DR = 0x00000000;
  DR = 0x00000050+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x0000000B************************Maximum driving strength; bandgap trim to nominal value
  DR = 0x00000000;
  DR = 0x0000000B;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);
  PrintRegisterContent(chipno, 32, outdata.data());   

  //***scrivo in C9 0x62 ************************
  DR = 0x00000000;
  DR = 0x00000060+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata_ir.data(), 7, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x00000361************************spread spectrum modulator settings (Witchcraft)
  DR = 0x00000000;
  DR = 0x00000361;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true); 
  PrintRegisterContent(chipno, 32, outdata.data());  

  //***scrivo in C9 0x02 ************************ 
  DR = 0x00000000;
  DR = 0x00000000+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x0000************************
  DR = 0x00000000;
  DR = 0x00000000+mode;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true); 
  PrintRegisterContent(chipno, 32, outdata.data());   
  //***scrivo in C9 0x22 ************************
  DR = 0x00000000;
  DR = 0x00000020+serdes_number;
  for(int i=0; i < 7; i++){
    if((((DR>>i)&0x1)==1))
      wrdata_ir[i] = true;
    else
      wrdata_ir[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata_ir.data(), 7, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x00500400************************ Power up serdes!
  DR = 0x00000000;
  DR = 0x00000000+(mult<<16)+(div<<8);
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1)
      wrdata[i] = true;
    else
      wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);
  PrintRegisterContent(chipno, 32, outdata.data());   

  return true;
}


/**@fn void cfg2vmedata(unsigned int* vmedata, int chainlength, int active_columns, int thr=8, int req_lay=0, int dis_mask=0, int tmode=0, int dis_pflow=0, int drv_str=1, int dcbits=2, int cmode=0, bool is_lamb_v1= false)
 *
 * @brief This function takes as input the settings of jpatt register. It generates a vector of int with the correct data to be written in the register
 * 
 * @param vmedata Pointer to the location where you want to store the data in hardware format
 * @param chainlength Number of chips per JTAG chain
 * @param active_columns Mask active columns
 * @param thr Chip thereshold
 * @param req_lay Require layer 0 hit to fira a match
 * @param dis_mask Disables clock to pattern blocks. In AM05 bit 0 is Xoram and bit 1 is Top2. In AM06 disable any combination of the 11 blocks.
 * @param tmode AMchip test mode
 * @param dis_pflow Disable the chip serial output, at end event no pattern will be sent out of the chip
 * @param drv_str Single ended pads driving strength
 * @param dcbits Number of ternary bits
 * @param cmode Continuous mode
 * is_lamb_v1 If true use the hardware configuration of lamb v1
 *
 * This function takes as input the settings of jpatt register. For more information about those parameters please refer to the AMchip documentation.
 * The settings are translated into a vector of integer (*vmedata) int the format needed for the hardware configuration.
 * That is, each 32-bits word (int) in the vector refers to a bit in JPATT config register of the chip; the i-th  bit in each 32-bits word refers to the i-th JATG chain.
 * If n chips are contained in each JATG chain the structure is repeated n times.
 *
 */
//void daq::ftk::AMBoard::cfg2vmedata(unsigned int* vmedata, int chainlength, int active_columns, int thr=8, int req_lay=0, int dis_mask=0, int tmode=0, int dis_pflow=0, int drv_str=1, int dcbits=2, int cmode=0, bool is_lamb_v1= false){
//
//  int i,j,k;
//  for (i=0; i<chainlength; i++){		/* loop over chps in chain */
//    for (j=0; j<CONFIG_SIZE; j++){		/* loop over bits in one layer */
//      int pos = CONFIG_SIZE*i+ j;
//      vmedata[pos] = 0;
//      for (k=0; k<MAXCOLUMNS; k++) {
//        if ((active_columns>>k)&0x1){
//          int bit = 0;
//          if (j==0||j==1||j==2||j==3 ) bit = ((thr>>j)&0x1);
//          else if (j==4) bit = req_lay&0x1;
//          else if(j>7 && j<16) {
//            int lamb= k/8; //lamb number is thechain index divided as integer by the number of chain per lamb
//            int link = (k%8)/2; //the link number is the link index inside the lamb divided by the number of JTAG chains belonging to the same link
//            int chip = ((k%2)*2)+i; //the chip number
//            if(is_lamb_v1){
//              if((k%8)<2)
//                link = (k%8);
//              else
//                link = (k%8)-1;
//              if((i/2)==0) 
//                chip = 2 + i%2;
//              else
//                chip = 1 - i%2;
//            }
//            // building the geographical address for AM06
//            int geo_pos = ((lamb<<4)+(link<<2)+chip); 
//
//            if (m_AMChip->getNAME() == AMChip05::global()->getNAME())
//              geo_pos <<= 1; //for AM05 need to shift by 1 bit
//
//            bit = ((geo_pos>>(j-8))&0x1);
//          }
//          //          else if(j==16) bit = dis_mask&0x1;
//          //          else if(j==17) bit = dis_top2&0x1;
//          else if (j>=16 && j<27) bit = (dis_mask>>(j-16)) & 0x1;
//          else if(j==40) bit = tmode;
//          else if(j==52) bit = dis_pflow&0x1;
//          else if(j==60) bit = drv_str&0x1;
//          else if(j>63&&j<96){
//            int dc_pos = (j-64)%4;
//            bit = ((dcbits>>dc_pos)&0x1);
//          }
//          else if(j==96) bit = cmode&0x1;
//          else bit=0x0;
//          vmedata[pos] |= (bit&0x1) <<k;
//        }
//      }
//    }
//  }
//  return;
//}

/** @fn expand_DC_bits(int data, int dc_bits = 2)
 * @brief This function take as input a superstrip and the number of ternary bits used, it returns the ss hardware wncoding
 *
 * @param data Input superstrip
 * @param dc_bits Number of ternary bits
 *
 * This function take as input a superstrip and the number of ternary bits used, the  default value is 2. This number must match the number of ternary bist set in jpattcfg.
 * The input superstrip is expected to have (18-(number_of_ternary_bits)) bits.
 * This function returns an 18 bits word in which dc bits are encoded in the correct way (1==10,0==01(,veto==00,don't care==11)). The unused MSB of the integer are set to 0.
 *
 */

int expand_DC_bits(int data, int dc_bits = 2){

  int expanded(0);
  for(int i=0;i<NBITS_CAMLAYER-dc_bits;i++){
    if(i<dc_bits){
      if(((data>>i)&0x1)==1)
        expanded = expanded|(2<<(i*2));
      else
        expanded = expanded|(1<<(i*2));
    }else{
      expanded = expanded|(((data>>i)&0x1)<<(i+dc_bits));
    }
  }
  PRINT_DEBUG_VME("pattern "<<std::hex<<data<<" epanded to "<<std::hex<<expanded);
  return expanded;
}

/** @fn bool pattern2vmedata(unsigned int* vmedata, std::vector<unsigned int*> pattern, bool* pattern_valid, bool verbose = false)
 *
 * @brief This function translate a list of patterns to write in a matrix of bits suitable for the jtag functions of the amchip
 * 
 * @param vmedata is expected to be a vector of (DATA_SIZE*MAXCHIP_PER_VMECOL) unsigned integers. Each bit of the integers represents a jtag chain on the ambslp. Each integer represent a different bit of the jpatt_data register.
 * @param pattern is expected to be a matrix NLAYERS x MAXAMCHIPS of integers. Each row of the matrix represents a complete pattern for one chip.
 * @param pattern_valid is expected to be a vector of MAXAMCHIP bool. The n-th element tells if the pattern to be written in n-th chip is valid.
 * @param verbose If true verbose mode is on
 * @return true, if conversion succeded, false in case of error 
 */

bool pattern2vmedata(uint32_t *vmedata, std::vector<std::vector<uint32_t>> &pattern, bool *pattern_valid, bool verbose = false){

  if(verbose){
    std::clog<<"INFO: pattern2vmedata(): Reading the input pattern"<<std::endl;
    for(int j=0; j<(int)pattern.size(); j++){
      for(int i=0; i<8; i++){
        std::clog<<std::hex<<(pattern.at(j))[i]<<"\t";
      }
      std::clog<<std::endl;
    }
  }
  for(int kkk=0; kkk<DATA_SIZE*MAXCHIP_PER_VMECOL; kkk++){
    vmedata[kkk] = 0x00000000;
  }
  unsigned int bit_to_write(0);
  unsigned int chip_number(0);
  for(unsigned int chain=0;chain<MAXCOLUMNS;chain++){ // loop over the columns
    for(unsigned int chip=0; chip<MAXCHIP_PER_VMECOL; chip++){ // loop over the chips within a column
      for(unsigned int layer=0;layer<NLAYERS;layer++){ // loop over the layers
        for(unsigned int bit=0;bit<NBITLAYERS;bit++){ // loop over the bits
          // identify where the bit has to be written according the bit and the chip in the chain
          bit_to_write = (chip*DATA_SIZE)+(layer*NBITLAYERS)+bit;
          chip_number = (MAXCHIP_PER_VMECOL*chain)+chip;

          // copy the bit in the proper position within the output structure
          vmedata[bit_to_write] |= (((((pattern.at(chip_number))[layer])>>bit)&0x1)<<chain);
          if((layer==(NLAYERS-1)) and (bit==(NBITLAYERS-1))){ // close the pattern data
            bit_to_write++;
            unsigned int disable(0);
            if(!pattern_valid[chip_number]){
              disable=1;
              std::cerr<<"Warning:pattern2vmedata(): Invalid pattern found! I will disable it!"<<std::endl;
            }
            else disable=0; 
            vmedata[bit_to_write]|= ((disable&0x1)<<chain);
          }
        } // end loop over the bits
      } // end loop over the layers
    } // end loop over the chips within a column`
  } // end loop over the columns
  if(verbose){
    std::clog<<"INFO: pattern2vmedata(): Reading back the pattern"<<std::endl;
    int chipno = (2<<8)+3;
    PrintRegisterContent(chipno, DATA_SIZE, vmedata);
  }
  return true;
}


/** @fn bool pattern2vmedata_v1(unsigned int* vmedata, std::vector<unsigned int*> pattern, bool* pattern_valid, bool verbose = false)
 *
 * @brief This function translate a list of patterns to write in a matrix of bits suitable for the jtag functions of the amchip. in the output ppatterns are arranged as needed for LAMB_v1
 *
 * @param vmedata is expected to be a vector of (DATA_SIZE*MAXCHIP_PER_VMECOL) unsigned integers. Each bit of the integers represents a jtag chain on the ambslp. Each integer represent a different bit of the jpatt_data register.
 * @param pattern is expected to be a matrix NLAYERS x MAXAMCHIPS of integers. Each row of the matrix represents a complete pattern for one chip.
 * @param pattern_valid is expected to be a vector of MAXAMCHIP bool. The n-th element tells if the pattern to be written in n-th chip is valid.
 * @param verbose IF true verbose mode is on
 * @return true, if conversion succeded, false in case of error 
 *
 * NOTE: THIS VERSION OF THE FUNCTION HAS TO BE USED FOR LAMB V1
 */




bool pattern2vmedata_v1(uint32_t *vmedata, std::vector<std::vector<uint32_t>> &pattern, bool *pattern_valid, bool verbose = false){

  if(verbose){
    std::clog<<"INFO: pattern2vmedata(): Reading the input pattern"<<std::endl;
    for(int j=0; j<(int)pattern.size(); j++){
      for(int i=0; i<8; i++){
        std::clog<<std::hex<<(pattern.at(j))[i]<<"\t";
      }
      std::clog<<std::endl;
    }
  }
  for(int kkk=0; kkk<DATA_SIZE*4; kkk++){
    vmedata[kkk] = 0x00000000;
  }
  unsigned int used_columns(0x1b1b1b1b);
  unsigned int bit_to_write(0);
  unsigned int chip_number(0);
  for(unsigned int chain=0;chain<MAXCOLUMNS;chain++){
    if( ((used_columns>>chain)&0x1) == 0x0) continue;
    for(unsigned int chip=0; chip<4; chip++){
      for(unsigned int layer=0;layer<NLAYERS;layer++){
        for(unsigned int bit=0;bit<NBITLAYERS;bit++){
          bit_to_write = (chip*DATA_SIZE)+(layer*NBITLAYERS)+bit;
          unsigned int chip_in_chain(-999),  written_chains(chain%8);
          if ( written_chains > 1 ) written_chains--;
          if ( (chip/2) == 0 ) chip_in_chain = 2 + chip%2;
          else chip_in_chain = 1 - chip%2;
          chip_number = (16*(chain/8))+(written_chains*4)+chip_in_chain;
          if(!(pattern.at(chip_number))[layer]){
            std::cerr<<"!(pattern.at("<<chip_number<<"))["<<layer<<"]"<<std::endl;
            return false;
          }else{
            vmedata[bit_to_write] |= (((((pattern.at(chip_number))[layer])>>bit)&0x1)<<chain);
            if((layer==(NLAYERS-1)) and (bit==(NBITLAYERS-1))){
              bit_to_write++;
              unsigned int disable(0);
              if(!pattern_valid[chip_number]){
                disable=1;
                std::cerr<<"Warning:pattern2vmedata(): Invalid pattern found! I will disable it!"<<std::endl;
              }
              else disable=0; 
              vmedata[bit_to_write]|= ((disable&0x1)<<chain);
            }
          }
        }
      }
    }
  }
  if(verbose){
    std::clog<<"INFO: pattern2vmedata(): Reading back the pattern"<<std::endl;
    int chipno = (4<<8)+15;
    PrintRegisterContent(chipno, DATA_SIZE, vmedata);
  }
  return true;
}

//////////////////////
// read_link_errors //
//////////////////////

/** This function reads the PRBS errors counted by a chip serdes.
 *
 * @param slot Crate slot in which the board is plugged
 * @param chip_pos Position of the chip in the Jtag chain 
 * @param column_number number of the column you want to check
 * @param serdes Name of the serdes you want to access (see get serdes number)
 * @param info is the information you want to mask, allowed options are: StreamError; LinkUP; errDOUT. See AMchip documentation for more infos on SEREDS statistics;
 * @return This function returns the bit of SERDES_STAT register mascked by info parameter. If info has an invalid value it returns a negative value.
 *
 */

int read_link_errors(int slot, int chip_pos, int column_number, const char* serdes, const char* info){
  std::array<uint32_t,MAXDATAWORDS>  outdata;
  /*unsigned*/ int serdes_number = get_serdes_number(serdes).first;
  if (serdes_number==-999) return -999;
  unsigned int active_columns=0xffffffff;
  int chipno = 0;
  for (int i=0, chipno=0; i<MAXCHIP_PER_VMECOL; i++) chipno = (chipno<<1) + 1; 
  chipno += (MAXCHIP_PER_VMECOL<<8);


  GotoTestReset(slot);
  GotoIdlefromReset(slot, active_columns);

  //***scrivo in C9 0x00+serdes_number ************************ 
  int  DR = 0x00000000*serdes_number;
  std::array<bool,32> wrdata;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1) wrdata[i] = true;
    else wrdata[i] = false;
  }			

  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in EA 0x0000************************ 
  DR = 0x00000000;
  DR = 0x00000000;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1) wrdata[i] = true;
    else wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_REG_SIZE, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_STAT, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
  if(strcmp(info, "StreamError")==0){
    if (get_serdes_number(serdes).second==0x1) 
      return ((outdata[28+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1);
    else if (get_serdes_number(serdes).second==0x2) 
      return ((outdata[24+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1);
    else if (get_serdes_number(serdes).second==0x3) 
      return ((((outdata[24+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1)<<1) +((outdata[28+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1));
    else return -999;
  }else if(strcmp(info, "LinkUP")==0)
    return ((outdata[20+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1);
  else if(strcmp(info, "errDOUT")==0){
    int to_return(0);
    for(int f=0; f<8;f++)
      to_return|=((outdata[f+8+(chip_pos*SERDES_REG_SIZE)]>>column_number)&0x1)<<f;
    return to_return;
  }else return -999;

  // Nicolo':
  //    I think this fuunction is not used can someone make a standalone function that eveluates the BER for all links? 
  //    If you want you can use ambslp_amchip_BER.cxx to start
}

///////////////////////
// get_serdes_number //
///////////////////////

/** This function returns the parameters needed to address a chip SERDES given his name 
 *
 * @param serdes SERDES name: allowed options patt_duplex;  "pattout"; "pattin1"; "bus0"; "bus1"; "bus2"; "bus3"; "bus4"; "bus5"; "bus6"; "bus7"; 
 * @return It returns a str::ppair of integers. Result .first is the serdes_number as required from  SERDES_SEL register. Result .second is rx_tx_enable indicating if the SERDES is rx (0x1), tx (0x2) or rx+tx (0x3). This number is needed by register RXTXConfReg2.
 *
 */
std::pair<int, int> get_serdes_number(const char* serdes){
  //serdes you want to act on
  int serdes_number(-999), rx_tx_enable(-999);
  std::pair<int,int> to_return;
  if(strcmp(serdes, "patt_duplex")==0){
    serdes_number = 0x0;
    rx_tx_enable = 0x3;
  }else if (strcmp(serdes, "pattout")==0){
    serdes_number = 0x0;
    rx_tx_enable = 0x1;
  }else if (strcmp(serdes, "pattin1")==0){
    serdes_number = 0x2;
    rx_tx_enable = 0x3; //////////TEST HN
  }else if (strcmp(serdes, "bus0")==0){
    serdes_number = 0x8;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus1")==0){
    serdes_number = 0x9;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus2")==0){
    serdes_number = 0xa;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus3")==0){
    serdes_number = 0xb;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus4")==0){
    serdes_number = 0xc;
    rx_tx_enable=0x2;
  }else if (strcmp(serdes, "bus5")==0){
    serdes_number = 0xd;
    rx_tx_enable=0x2;
  }else if (strcmp(serdes, "bus6")==0){
    serdes_number = 0xe;
    rx_tx_enable = 0x2;
  }else if (strcmp(serdes, "bus7")==0){
    serdes_number = 0xf;
    rx_tx_enable = 0x2;
  }else{
    FTK_STRING_PARAM_ERROR("ambslp_amchip::get_serdes_number(): Found an invalid option for serdes name: '"<<serdes<<"'!");
    to_return = std::make_pair(-999,-999);
    return to_return;
  }
  to_return = std::make_pair(serdes_number, rx_tx_enable);
  return to_return;
}

/////////////////////////
// pilot_error_counter //
/////////////////////////

int pilot_error_count(int slot, const char* serdes, const char* operation_mode, bool enable_counter){
  /*unsigned*/ int serdes_number = get_serdes_number(serdes).first;
  if (serdes_number==-999) return -999;
  unsigned int mode(0x0);
  if(strcmp(operation_mode, "normal")==0) mode=0x0;
  else if(strcmp(operation_mode, "prbs")==0) mode=0x1;
  else{
    FTK_STRING_PARAM_ERROR("error: Found an invalid option for operation_mode: '"<<operation_mode<<"'. Abort!");
    return -1;
  }
  std::array<uint32_t,MAXDATAWORDS>  outdata;
  unsigned int active_columns=0xffffffff;
  int chipno = 0;
  for (int i=0, chipno=0; i<MAXCHIP_PER_VMECOL; i++) chipno = (chipno<<1) + 1; 
  chipno += (MAXCHIP_PER_VMECOL<<8);


  GotoTestReset(slot);
  GotoIdlefromReset(slot, active_columns);

  //***scrivo in C9 0x00+serdes_number ************************ 
  int  DR = 0x00000000*serdes_number;
  std::array<bool,21> wrdata;
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1) wrdata[i] = true;
    else wrdata[i] = false;
  }			

  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata.data(), SERDES_SEL_SIZE, outdata.data(), true); 
  PrintRegisterContent(chipno, 7, outdata.data()); 
  //***scrivo in CA 0x0001************************
  DR = 0x00000000;
  if (enable_counter) DR = 0x00001100+(mode&0x1);
  else DR = 0x00000000+(mode&0x1);
  for(int i=0; i < 32; i++){
    if (((DR>>i)&0x1)==1) wrdata[i] = true;
    else wrdata[i] = false;
  }			
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data());
  ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), SERDES_REG_SIZE, outdata.data(), true);
  return 1;
}

/** @fn bool test_pattern(int slot, int chainlength, int chipnum, int active_columns, int address, std::vector<unsigned int*> patt_block, bool *pattern_valid, int* result, bool verbose=false) 
 *
 *
 * @brief This function can be used to check if patterns are written correctly in the AMchip.
 *
 * @param address is the address where you expect the pattern to be written
 * @param patt_block is a vector of patterns (the addresss-th pattern for each chip on the board)
 * @param pattern_valid is a vector of booleans indicating if the pattren in the vector above should be tested. 
 * @param result is the vector in which results will be stored. This vector is expected to have one location for each chip in the board. Returned values will be 3 bits words: bit 0 --> pattern is found; bit 1 --> pattern is valid; bit 3 --> pattern has bitmap equal to 0xff
 * @param rec_address is the pointer to an array of int which stores rec_addresss read out from the chips
 * @return value is true if the addressed pattern has been found in the output, it is valid and, it has bitmap 0xff.
 * 
 * Please note that this function expect the chip to be configured as follow;
 *      TEST MODE == 1
 *      THRESHOLD == 8
 *      DISABLE PATTERN FLOW == 1
 *      CONTINUOUS MODE == 0
 * 
 * PLEASE NOTE THAT TO DO THIS CHECK WE NEED TO READ THE COMPLETE LIST OF PATTERNS VIA JTAG EACH TIME. THE FUNCTION IS CALLED: THIS WILL SLOW THE WRITE PATTERN PROCEDURE OF A FACTOR ~2^NPATT
 *
 */

bool test_pattern(int slot, int chainlength, int chipnum, int active_columns, int address, std::vector<std::vector<uint32_t>> &patt_block, bool *pattern_valid, bool *result, uint32_t *rec_address, bool verbose=false){ 
  AMChip *m_AMChip = AMChip06::global();
  int chipno = 0;
  chipno = (chainlength<<8) + chipnum;
  if(verbose){PRINT_DEBUG_VME("chipno"<<std::hex<<chipno);}
  if(verbose){PRINT_DEBUG_VME("chainlength"<<std::hex<<chainlength);}
  std::array<uint32_t,MAXDATAWORDS> indata;
  std::array<uint32_t,MAXDATAWORDS> outdata;
  pattern2vmedata(indata.data(), patt_block, pattern_valid, verbose);

  //Reset the value in JPATT_DATA
  std::array<bool,145> wrdata_ir;                                                                                                                                                                       
  for(int i=0;i<145;i++) wrdata_ir[i]=0;
  GotoTestReset(slot);
  GotoIdlefromReset(slot, active_columns);
  ConfigureRegister(slot, chipno, active_columns, JPATT_DATA, wrdata_ir.data(), DATA_SIZE, outdata.data());

  ///send init event via jtag - twice
  ConfigureScam(slot, chipno, active_columns, INIT_EVop, 1); 

  ///load the pattern in the write_pattern shift register 
  ConfigureRegister(slot, chipno, active_columns, JPATT_DATA, indata.data(), DATA_SIZE, outdata.data());

  ///send init event via jtag
  ConfigureScam(slot, chipno, active_columns, INIT_EVop, 1); 

  std::array<bool,MAXAMCHIPS> is_reco;
  bool to_return = true; ///prepare bool containing condensed information
  for(unsigned int kkk=0; kkk<MAXAMCHIPS;kkk++) is_reco[kkk] = false; /// initialize the vector to pattern not found
  for(unsigned int kkk=0; kkk<MAXDATAWORDS;kkk++) indata[kkk]= 0; 

  // Checking the fired patterns
  ConfigureRegister(slot, chipno, active_columns, REC_ADDR, indata.data(), REC_ADDR_SIZE, outdata.data());  
  int reco_add =0;
  for(int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){///loop on all lambs 
    for (int chain =0; chain <MAXVMECOL_PER_LAMB; chain++){ ///loop on all chains 
      for(int chip = 0; chip<MAXCHIP_PER_VMECOL; chip++){  ///loop on all chips in the chain
	int index = (lamb*MAXCHIP_PER_LAMB)+(chain*MAXCHIP_PER_VMECOL)+chip;
	if(verbose) {ERS_LOG("lamb: "<<lamb<<", chain: "<<chain<<", chip: "<<chip<<", pattern_valid["<<index<<"] = "<<pattern_valid[index]);}
	if(not pattern_valid[index]) continue;
	int this_column = (lamb*MAXVMECOL_PER_LAMB)+chain;
	if(verbose) {ERS_LOG("active_columns: "<< std::hex << active_columns<<", this_column"<< std::hex <<this_column);}
	if(not((active_columns>>this_column)&0x1)) continue;
	for(int bit=0;bit<REC_ADDR_SIZE; bit++){
	  reco_add |= (((outdata[(chip*REC_ADDR_SIZE)+bit]>>this_column)&0x1) << bit);
	}
	rec_address[index] = reco_add;
	if(verbose) {ERS_LOG("reco_add: "<< std::hex << reco_add <<", reco_add>>18: "<< std::hex << (reco_add>>18)<<", ((reco_add>>1)&0x1ffff) = "<< std::hex << ((reco_add>>1)&0x1ffff)<<", address = "<< std::hex <<address<<", reco_add&0x1: "<< std::hex<< (reco_add&0x1) << std::dec);}
	if(reco_add&0x1){
	  if(((reco_add>>18)&0xff)==0xff){
	    if((((reco_add>>1)&0x1ffff)==address)) {
	      is_reco[index] = true;
	    }
	  }
	}
      }
    }
  }
  std::copy(is_reco.begin(), is_reco.end(), result);//copy the result in result vector
  
  to_return = true;
  for(int kkk=0; kkk<MAXAMCHIPS;kkk++){
    if(pattern_valid[kkk] and not(is_reco[kkk])) to_return = false;
  } 
  return to_return;

}

bool patternIsProperlyReconstructed( int reco_add, int address ) {

  bool hasTriggered = patternHasTriggered(reco_add);
  bool isValid = patternIsValid(reco_add);
  bool addressIsMatched = patternAddressIsMatched(reco_add, address);
  bool result = hasTriggered && isValid && addressIsMatched;

  return result;
}

bool patternHasTriggered( int reco_add ) {

  // the 8 msb of this 25-bit word are all set if the pattern has triggered in the chip
  // each bit represents one layer
  return (((reco_add>>18) & 0xff) == 0xff);

}

bool patternIsValid( int reco_add ) {

  // the lsb in this 25-bit word is set if the pattern was set as valid when copying it into the chip
  return (reco_add & 0x1);
}


bool patternAddressIsMatched( int reco_add, int address ) {

  // the 16 bits from 17->2 in this 25 bit word represent the pattern address in the chip
  return (( (reco_add>>1) & 0xffff ) == address);
}



bool test_pattern_renaming ( int slot, 
                             int chainlength, 
                             int chipnum, 
                             int active_columns, 
                             int address, 
                             std::vector<std::vector<uint32_t>> &patt_block, 
                             bool *pattern_valid, 
                             bool *result, 
                             bool verbose=false) {

  AMChip *m_AMChip = AMChip05::global();
  int chipno = 0;
  chipno = (chainlength<<8) + chipnum;

  std::array<uint32_t,MAXDATAWORDS> indata; 
  std::array<uint32_t,MAXDATAWORDS> outdata;

  pattern2vmedata(indata.data(), patt_block, pattern_valid, verbose);

  resetTAP_and_gotoIdle( slot, active_columns );
  loadPatterns( slot, chipno, active_columns, chainlength, indata.data(), outdata.data());

  //send init event via jtag
  ConfigureScam(slot, chipno, active_columns, INIT_EVop, 1); 

  //  std::vector<bool> is_reco(MAXAMCHIPS); // prepare a vector to store information
  std::array<bool,MAXAMCHIPS> is_reco;
  bool to_return = true; //prepare bool containing condensed information
  for(unsigned int kkk=0; kkk<MAXAMCHIPS;kkk++) is_reco[kkk] = false; // initialize the vector to pattern not found
  for(unsigned int kkk=0; kkk<MAXDATAWORDS;kkk++) indata[kkk]= 0; 

  //loop on all patterns
  for(unsigned int patt=0; patt<m_AMChip->getMAXPATT_PERCHIP();patt++){

    readAddressOfFiredPattern( slot, chipno, active_columns, chainlength, indata.data(), outdata.data() );

    //require next rec address (for next step in the loop)
    ConfigureScam(slot, chipno, active_columns, NEXT_REC_ADDR, 1); 

    int reco_add =0;
    for(unsigned int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){//loop on all lambs 
      for (unsigned int chain =0; chain <MAXVMECOL_PER_LAMB; chain++){ //loop on all chains 
        for(unsigned int chip = 0; chip<MAXCHIP_PER_VMECOL; chip++){  //loop on all chips in the chain

          int index = (lamb*MAXCHIP_PER_LAMB)+(chain*MAXCHIP_PER_VMECOL)+chip;
        if(not pattern_valid[index])continue;
          int this_column = (lamb*MAXVMECOL_PER_LAMB)+chain;
          if(not((active_columns>>this_column)&0x1))continue;
          reco_add = 0;

          for(unsigned int bit=0;bit<REC_ADDR_SIZE; bit++){
            reco_add |= (((outdata[(chip*REC_ADDR_SIZE)+bit]>>this_column)&0x1) << bit);
          }

          is_reco[index] = patternIsProperlyReconstructed( reco_add, address );
        }
      }
    }
    to_return = true;
    for(unsigned int kkk=0; kkk<MAXAMCHIPS;kkk++){
      if(pattern_valid[kkk] and not(is_reco[kkk])) to_return = false;
    }
    if(to_return) break;
  }
  std::copy(is_reco.begin(), is_reco.end(), result);//copy the result in result vector
  return to_return;
}


void resetTAP_and_gotoIdle( int slot, int active_columns ) {

    GotoTestReset(slot);
    GotoIdlefromReset(slot, active_columns); 
}


			     void loadPatterns( int slot, int chipno, int active_columns, int chainlength,const uint32_t *indata,uint32_t *outdata) {

  //load the pattern in the write_pattern shift register
  ConfigureRegister(slot, chipno, active_columns, JPATT_DATA, indata, DATA_SIZE, outdata);

}



			     void readAddressOfFiredPattern( int slot, int chipno, int active_columns, int chainlength, const uint32_t *indata,uint32_t *outdata ) {

  ConfigureRegister(slot, chipno, active_columns, REC_ADDR, indata, REC_ADDR_SIZE, outdata);

}

//
namespace daq { 
  namespace ftk { 

    // VMEInterface
    // ERS logging
    // ERS issues
    //--------


    bool ambslp_amchip_BER(int slot, int chainlength, int chipnum) { 
      int active_columns =0;
      int chipno = 0;

      // u_int data[MAXDATAWORDS];  // unused var
      std::array<uint32_t,MAXDATAWORDS>  outdata;
      // u_int indata[MAXDATAWORDS];  // unused var
      int i; // ,j;  // j is unused var

      // int idcode0 = 0;  // unused var
      // int idcode1 = 0;  // unused var
      // int idcode2 = 0;  // unused var
      // int idcode3 = 0;  // unused var
      time_t start, stop_0, stop_1, stop_2, stop_3,actual_time;
      bool control_0(true),control_1(true),control_2(true),control_3(true);
      int err_0(0),err_1(0),err_2(0),err_3(0);
      double ber(0);
      int step(0);
      float time_diff = 0;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      printf("chipno %x\n", chipno);  
      printf("chainlength %x\n", chainlength);

      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn);
      time(&start);
      while(true){
        for(int bus=0;bus<8;bus++){
          err_0=0;
          err_1=0;
          err_2=0;
          err_3=0;
          //INIZIO CONFIGURAZIONE BUS0!!!!!!**************************************
          //**********************************************************************6
          //**********************************************************************
          //***scrivo in C9 0x08 ************************ 
          int  DR = 0x00000008+bus;
	  std::array<bool,32> wrdata;
          for(int i=0; i < 32; i++){
            if (((DR>>i)&0x1)==1)
              wrdata[i] = true;
            else
              wrdata[i] = false;
          }			
          ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
          ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true);
          PrintRegisterContent(chipno, 7, outdata.data());
          //***scrivo in CA 0x0001************************ 
          DR = 0x00000000;
          DR = 0x00000000;
          for(int i=0; i < 32; i++){
            if (((DR>>i)&0x1)==1)
              wrdata[i] = true;
            else
              wrdata[i] = false;
          }			
          ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data());
          ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data(), true); 
          PrintRegisterContent(chipno, 32, outdata.data());
          //searching for errors in chains
          for(int hh =8; hh<16;hh++){
            err_0 =  err_0 +(((outdata[hh]>>((16)+0))&0x1) <<(hh-8));
          }
          if(err_0!=0 && control_0 == true){//if an error is found for the first time stop acquisition 
            time(&stop_0);
            control_0=false;
          }else control_0=true;
          for(int hh =8; hh<16;hh++){
            err_1 =  err_1 +(((outdata[hh]>>((16)+1))&0x1) <<(hh-8));
          }
          if(err_1!=0 && control_1 == true) {//if an error is found for the first time stop acquisition
            time(&stop_1);
            control_1=false;
          }else control_1=true;
          for(int hh =8; hh<16;hh++){
            err_2 =  err_2 +(((outdata[hh]>>((16)+3))&0x1) <<(hh-8));
          }
          if(err_2!=0 && control_2 == true) {//if an error is found for the first time stop acquisition
            time(&stop_2);
            control_2=false;
          }else control_2=true;
          for(int hh =8; hh<16;hh++){
            err_3 =  err_3 +(((outdata[hh]>>((16)+4))&0x1) <<(hh-8));
          }
          if(err_3!=0 && control_3 == true) {//if an error is found for the first time stop acquisition
            time(&stop_3);
            control_3=false;
          }else control_3=true;
          time(&actual_time);
          //print errors found in the i-th cycle
          printf("Error found at step %i in bus %i : %i %i %i %i \n",step,bus, err_0, err_1, err_2, err_3);
          if(!control_0){
            ber =0;
            time_diff = difftime(stop_0,start);
            ber = 1/(2000000000*time_diff);
            printf("Error found in chain 0: BER cahin 0 > %e \n",ber);
          }else{
            ber =0;
            time_diff = difftime(actual_time,start);
            ber = 1/(2000000000*time_diff);
            printf("BER cahin 0 < %e",ber);
          }
          if(!control_1){
            ber =0;
            time_diff = difftime(stop_1,start);
            ber = 1/(2000000000*time_diff);
            printf("Error found in chain 1: BER cahin 1 > %e \n",ber);
          }else{
            ber =0;
            time_diff = difftime(actual_time,start);
            ber = 1/(2000000000*time_diff);
            printf("BER cahin 0 < %e",ber);
          }
          if(!control_2){
            ber =0;
            time_diff = difftime(stop_2,start);
            ber = 1/(2000000000*time_diff);
            printf("Error found in chain 3: BER cahin 3 > %e \n",ber);
          }else{
            ber =0;
            time_diff = difftime(actual_time,start);
            ber = 1/(2000000000*time_diff);
            printf("BER cahin 0 < %e",ber);
          }
          if(!control_3){
            ber =0;
            time_diff = difftime(stop_3,start);
            ber = 1/(2000000000*time_diff);
            printf("Error found in chain 4: BER cahin 4 > %e \n",ber);
          }else{
            ber =0;
            time_diff = difftime(actual_time,start);
            ber = 1/(2000000000*time_diff);
            printf("BER cahin 0 < %e",ber);
          }
          //***scrivo in C9 0x08 *************************
          DR = 0x00000008+bus;
          for(int i=0; i < 32; i++){
            if (((DR>>i)&0x1)==1)
              wrdata[i] = true;
            else
              wrdata[i] = false;
          }			
          ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
          ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true);
          PrintRegisterContent(chipno, 7, outdata.data());
          //***scrivo in CA 0x00000001 ************************ 
          DR = 0x00000001;
          DR = 0x00000001;
          for(int i=0; i < 32; i++){
            if (((DR>>i)&0x1)==1)
              wrdata[i] = true;
            else
              wrdata[i] = false;
          }			
          ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data());
          ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data(), true);
          PrintRegisterContent(chipno, 32, outdata.data());  
          //Scrivo 0x08 nel registro 0xC9 *****************************************
          DR = 0x08+bus;
          for(int i=0; i < 32; i++){
            if((((DR>>i)&0x1)==1))
              wrdata[i] = true;
            else
              wrdata[i] = false;
          }			
          ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
          ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true); 
          PrintRegisterContent(chipno, 7, outdata.data());
          //***scrivo in CA 0x00001101 ************************ 
          DR = 0x00001101;
          for(int i=0; i < 32; i++){
            if (((DR>>i)&0x1)==1)
              wrdata[i] = true;
            else
              wrdata[i] = false;
          }			
          ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data());
          ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data(), true);
          PrintRegisterContent(chipno, 32, outdata.data());
        }
        step++;
        sleep(60);
      }
 
      return true;
    }

    /** @fn bool ambslp_amchip_read_errDout32(int slot, int chainlength, int chipnum)
     *  @brief This function read the errDout32 of all buses of the AMChips on an AMBoard. errRd32 will be set at 0 after reading out.
     *  @param slot is the target slot number 
     *  @param chainlength is the length of each JTAG chain (unique for a LAMB: 2 for LAMB ver 2/3/4, 4 for LAMB ver 1)
     *  @param chipnum  integer converted from the mask pattern of chips to be read (e.g. if you are going to read out all chips in a chain with length 2, this param should be 3 = 11b)
     */
    bool ambslp_amchip_read_errDout32(int slot, int chainlength, int chipnum) { 
      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS>  outdata;
      int i; 

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      printf("chipno %x\n", chipno);  
      printf("chainlength %x\n", chainlength);

      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn);

      int DR = 0x00000000;
      std::array<bool,32> wrdata;

      for(int bus=0;bus<8;bus++){

	//***write in C9 *************************
	DR = 0x00000008+bus;
	for(int i=0; i < 32; i++){
	  if (((DR>>i)&0x1)==1)
	    wrdata[i] = true;
	  else
	    wrdata[i] = false;
	}			
	ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
	ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true);
	
	//***write in CA ************************ 
	DR = 0x00000000;
	for(int i=0; i < 32; i++){
	  if (((DR>>i)&0x1)==1)
	    wrdata[i] = true;
	  else
	    wrdata[i] = false;
	}			
	ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data());
	ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data(), true);
		
      }

      while(true){
      auto begin = std::chrono::high_resolution_clock::now();

      for(int bus=0;bus<8;bus++){

	//***write in C9 the bus selection*****************************************
	DR = 0x00000008+bus;
	for(int i=0; i < 32; i++){
	  if((((DR>>i)&0x1)==1))
	    wrdata[i] = true;
	  else
	    wrdata[i] = false;
	}			
	ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
	ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true);
	
	//***write in CA 0x00001100 (turn on errRd32 and errRd8) ************************ 
	DR = 0x00001100;
	for(int i=0; i < 32; i++){
	  if (((DR>>i)&0x1)==1)
	    wrdata[i] = true;
	  else
	    wrdata[i] = false;
	}			
	ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data());
	ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data(), true);
	
	//***read EA () ************************ 
	DR = 0x00000000;
	for(int i=0; i < 32; i++){
	  if (((DR>>i)&0x1)==1)
	    wrdata[i] = true;
	  else
	    wrdata[i] = false;
	}			
	ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data());
	ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data(), true);

	//***write in CA 0x00001101 ************************ 
	DR = 0x00000000;
	for(int i=0; i < 32; i++){
	  if (((DR>>i)&0x1)==1)
	    wrdata[i] = true;
	  else
	    wrdata[i] = false;
	}			
	ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data());
	ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data(), true);

      }

      auto end = std::chrono::high_resolution_clock::now();
      std::cerr<<"time for reading out 8 buses = "<<std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " ms"<<std::endl;
      sleep(1);
      }

    
      return true;
    }


    bool ambslp_amchip_enable_bank(int slot, int chainlength, int chipnum, int npatt, int offset) { 

      AMChip *m_AMChip = AMChip05::global();
      // definition of useful variables
      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS> outdata;
      std::array<uint32_t,MAXDATAWORDS> indata;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (int i=0, chipno=0; i<chainlength; i++)
        chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      printf("chipno %x\n", chipno);  
      printf("chianlength %x\n", chainlength);

      //**********************************************************************1
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn); 

      // Set pattern address to 0
      // *** scrivo in C4 ***
      ConfigureScam(slot, chipno, active_columns, JPATT_ADDR, 1);  
      GotoShiftDRfromIdle(slot, active_columns);
      printf("Start writing from pattern %i", offset);
      for(int i=0; i < 16; i++){
        if((offset>>i)&0x1)indata[i] = 0xffffffff;
        else indata[i] = 0x00000000;
      }
      for (int i=0; i<16;i++){
        indata[i+16*1] = indata[i];
        indata[i+16*2] = indata[i];
        indata[i+16*3] = indata[i];
      }
      AMshift(slot, active_columns, m_AMChip->getADDR_SIZE()*4, indata.data(), outdata.data(), 1);
      GotoIdlefromExit1(slot, active_columns);

      for(int i=0; i<npatt;i++){
	std::array<bool,145> wrdata_ir;
        for(int k=0;k<8;k++){
          //int DR = expand_DC_bits(0xfafa,2);
          int DR = 0x1;
          for(int r=0; r <NBITS_CAMLAYER; r++){
            if((((DR>>r)&0x1)==1))
              wrdata_ir[((k*NBITS_CAMLAYER)+r)] = true;
            else
              wrdata_ir[((k*NBITS_CAMLAYER)+r)] = false;
          }	
        }
        wrdata_ir[144] = false; //disable pattern @ majority level 
        ConfigureRegister(slot, chipno, active_columns, JPATT_DATA, wrdata_ir.data(), DATA_SIZE, outdata.data());
        ConfigureScam(slot, chipno, active_columns, WR_INCop, 1);
        ConfigureRegister(slot, chipno, active_columns, JPATT_ADDRrd, wrdata_ir.data(), m_AMChip->getADDR_SIZE(), outdata.data());
        PrintRegisterContent(chipno, m_AMChip->getADDR_SIZE(), outdata.data());
        ConfigureRegister(slot, chipno, active_columns, JPATT_DATArd, wrdata_ir.data(), DATA_SIZE, outdata.data());
        PrintRegisterContent(chipno, DATA_SIZE, outdata.data());
      }

      std::array<bool,145> wrdata_ir;
      //int DR = 0xffff;
      int DR = 0x0;
      for(int k=1;k<9;k++){
        for(int r=0; r <NBITS_CAMLAYER; r++){
          if((((DR>>r)&0x1)==1))
            wrdata_ir[((k*NBITS_CAMLAYER)+r)] = true;
          else
            wrdata_ir[((k*NBITS_CAMLAYER)+r)] = false;
        }  				
      }
      wrdata_ir[144] = 0;  
      ConfigureRegister(slot, chipno, active_columns, JPATT_DATA, wrdata_ir.data(), DATA_SIZE, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, JPATT_DATArd, wrdata_ir.data(), DATA_SIZE, outdata.data(), true);

      return true;
    } // end of ambslp_amchip_idcode_4chip

    bool ambslp_amchip_idlecfg(int slot, int chainlength, int chipnum){ 

      // definition of useful variables
      int active_columns =0;
      int chipno = 0;

      // u_int data[MAXDATAWORDS];  // unused var
      std::array<uint32_t,MAXDATAWORDS>  outdata;
      // u_int indata[MAXDATAWORDS]; // unused var

      int idcode0 = 0;
      int idcode1 = 0;
      int idcode2 = 0;
      int idcode3 = 0;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (int i=0, chipno=0; i<chainlength; i++)
        chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      printf("chipno %x\n", chipno);  
      printf("chianlength %x\n", chainlength);
      //const char DR[] = "4000700";


      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn); 
      //      int DR = 0x4000700;
      std::array<bool,21> wrdata;
      for(int i=0; i < 32; i++){
        if (i==23 or i==26)
	  wrdata[i] = true;
        else
	  wrdata[i] = false;
      }
	

      //ConfigureRegister(slot, chipno, active_columns, 0xCB, DR, 32, outdata,false); //TK 24/06/2017 this function was removed; refere to ambslp-01-00-17 if necessary
      //ConfigureRegister(slot, chipno, active_columns, 0xCB, DR, 32, outdata,true); //TK 24/06/2017 this function was removed; refere to ambslp-01-00-17 if necessary
      ConfigureRegister(slot, chipno, active_columns, 0xCB, wrdata.data(), 32, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, 0xCB, wrdata.data(), 32, outdata.data(), true);
      //**********************************************************************
      //**********************************************************************
      idcode0=0;
      idcode1=0;
      idcode2=0;
      idcode3=0;
      for(int lamb=0; lamb<4; lamb++){
        for (int chain =0; chain < 8; chain++){   
          for(int i=0; i<chainlength*32; i++){
            if(i<32) idcode0 = idcode0 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            else if(i>=32 && i<2*32) idcode1 = idcode1 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            else if(i>=2*32 && i<3*32) idcode2 = idcode2 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            else if(i>=32*3) idcode3 = idcode3 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
          }	
          printf("SysStatReg chain %i LAMB %i %x \t",chain,lamb,idcode0);
          printf("%x \t", idcode1);
          printf("%x \t", idcode2);
          printf("%x \n", idcode3);
          idcode0 = 0;
          idcode1 = 0;
          idcode2 = 0;
          idcode3 = 0;
        }
      }

      return true;
    } // end of ambslp_amchip_idcode_4chip

    bool ambslp_amchip_test_mode(int slot, int chainlength, int chipnum, bool is_duplicate) { 

      // definition of useful variables
      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS> outdata;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (int i=0, chipno=0; i<chainlength; i++)
        chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      printf("chipno %x\n", chipno);  
      printf("chianlength %x\n", chainlength);

      //**********************************************************************1
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn); 
      std::array<bool,97> wrdata;
      for(int i=0; i < 97; i++) {
	if (is_duplicate && ((i==8) or (i==9) or (i==11) or (i==12) or (i==40) or (i==60) or (i==65) or (i==69) or (i==73) or (i==77) or (i==81) or (i==85) or (i==89) or (i==93))) {
	  wrdata[i] = true;
	}
	else if (!is_duplicate && ((i==8) or (i==9) or (i==11) or (i==12) or (i==60) or (i==65) or (i==69) or (i==73) or (i==77) or (i==81) or (i==85) or (i==89) or (i==93))){
	  wrdata[i] = true;
	}
	else{
	  wrdata[i] = false;
	}
      }
      ConfigureRegister(slot, chipno, active_columns, 0xC6, wrdata.data(), 97, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, 0xC6, wrdata.data(), 97, outdata.data(), true);
      PrintRegisterContent(chipno, 97, outdata.data());
      GotoIdlefromExit1(slot, active_columns); 

      return true;
    } // end of ambslp_amchip_idcode_4chip


    bool ambslp_amchip_polling_stat_reg_bus(int slot, int chainlength, int chipnum, bool prbsEnabled, bool verbose = false){
      bool allPassed = true;
      int active_columns =0;
      int chipno = 0;
      std::array<uint32_t,MAXDATAWORDS> outdata;
      int i;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; //m: useless?
      chipno = (chainlength<<8) + chipnum; 

      if(verbose){
	printf("chipno %x\n", chipno);  
	printf("chainlength %x\n", chainlength);
      }
      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn);

      std::array<bool,NLAYERS> busErrors; busErrors.fill(false);//false if no error is found in the bus
      bool lambErrors[NLAYERS][MAXLAMB_PER_BOARD] = {false};//false if no error is found in the lamb
      bool chipErrors[NLAYERS][MAXVMECOL_PER_LAMB][MAXVMECOL_PER_LAMB][MAXCHIP_PER_VMECOL] = {false};//8 bus, 4 lambs, 8 chains, 2 chips
      int chipErrorNum[NLAYERS][MAXVMECOL_PER_LAMB][MAXVMECOL_PER_LAMB][MAXCHIP_PER_VMECOL] = {0};//8 bus, 4 lambs, 8 chains, 2 chips 
      std::array<bool,MAXLAMB_PER_BOARD> lambPassed; lambPassed.fill(true);

      for(int bus=0; bus<NLAYERS; bus++){
        //START BUS 0 CONFIGURATION!!!!!!***************************************
        //**********************************************************************
        //**********************************************************************
        //***scrivo in C9 0x08+bus ************************ 
        int  DR = 0x00000008+bus;
	std::array<bool,32> wrdata;
        for(int i=0; i < 32; i++){
          if (((DR>>i)&0x1)==1)
            wrdata[i] = true;
          else
            wrdata[i] = false;
        }			
        ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
        ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true); 
        //PrintRegisterContent(chipno, 7, outdata); 
        //***scrivo in EA 0x0000************************ 
        sleep(1);
        DR = 0x00000000;
        DR = 0x00000000;
        for(int i=0; i < 32; i++){
          if (((DR>>i)&0x1)==1)
            wrdata[i] = true;
          else
            wrdata[i] = false;
        }			
        ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data());
        ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data(), true); 

        int regOut0(0); //variables "regOut" were called "idcode" 
        int regOut1(0);
        //int regOut2(0); //not needed for new lambs
        //int regOut3(0);
        for(int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){
          for (int chain =0; chain < MAXVMECOL_PER_LAMB; chain++){   
            for(int i=0; i<chainlength*32; i++){
              if(i<32) regOut0 = regOut0 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
              else if(i>=32 && i<2*32) regOut1 = regOut1 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
              //else if(i>=2*32 && i<3*32) regOut2 = regOut2 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i); //not needed for new lambs 
              //else if(i>=32*3) regOut3 = regOut3 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
            }	
	    chipErrorNum[bus][lamb][chain][0] = (regOut0>>8)&0xff;
	    chipErrorNum[bus][lamb][chain][1] = (regOut1>>8)&0xff;

	    if( ( prbsEnabled && chipErrorNum[bus][lamb][chain][0] != 0) || ( !prbsEnabled && chipErrorNum[bus][lamb][chain][0] < 90) )
		chipErrors[bus][lamb][chain][0] = true;
	    if( ( prbsEnabled && chipErrorNum[bus][lamb][chain][1] != 0) || ( !prbsEnabled && chipErrorNum[bus][lamb][chain][1] < 90) )
		chipErrors[bus][lamb][chain][1] = true;
	    if(verbose){
	      printf("SysStatReg chain %i LAMB %i BUS %i : \t", chain, lamb, bus);
	      printf("%x \t", chipErrorNum[bus][lamb][chain][0] );
	      printf("%x \n", chipErrorNum[bus][lamb][chain][1] );
	      //printf("%x \t", ((regOut2>>8)&0xff)); //not needed for new lambs
	      //printf("%x \n", ((regOut3>>8)&0xff));
	    }
	    if( chipErrors[bus][lamb][chain][0] || chipErrors[bus][lamb][chain][1] ){ //if one of the two chips does not pass the test
	      busErrors[bus] = true; //true if any error is found in this bus
	      lambErrors[bus][lamb] = true; //true if any error is found in this lamb for this bus
	    }//if( !chipErrors ...
            regOut0 = 0;
            regOut1 = 0;
            //regOut2 = 0; // not needed for new lamb
            //regOut3 = 0;
          }
        }//for(int lamb=0 ...
        //PrintRegisterContent(chipno, 32, outdata); 
      }//for(int bus...

      if(prbsEnabled)
	printf("PRBS generator enabled, the link does not pass the test when one or more errors are detected.\n");
      else
	printf("PRBS generator disabled, the link does not pass the test when the output is less than 90.\n");

      for(int bus=0; bus<NLAYERS; bus++){
	if( busErrors[bus] ){
	  allPassed = false;
	  printf("\nBus %d did not pass the test\n", bus);
	  for(int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){
	    if( lambErrors[bus][lamb] ){
	      lambPassed[lamb] = false;
	      printf("Lamb %d did not pass the test, the output for each chip is printed below:\n", lamb);
	      printf("\nchain 0: %x | %x  chain 6: %x | %x", chipErrorNum[bus][lamb][0][0], chipErrorNum[bus][lamb][0][1], chipErrorNum[bus][lamb][6][0], chipErrorNum[bus][lamb][6][1]);
	      printf("\nchain 1: %x | %x  chain 7: %x | %x", chipErrorNum[bus][lamb][1][0], chipErrorNum[bus][lamb][1][1], chipErrorNum[bus][lamb][7][0], chipErrorNum[bus][lamb][7][1]);
	      printf("\nchain 3: %x | %x  chain 5: %x | %x", chipErrorNum[bus][lamb][3][0], chipErrorNum[bus][lamb][3][1], chipErrorNum[bus][lamb][5][0], chipErrorNum[bus][lamb][5][1]);
	      printf("\nchain 2: %x | %x  chain 4: %x | %x\n", chipErrorNum[bus][lamb][2][0], chipErrorNum[bus][lamb][2][1], chipErrorNum[bus][lamb][4][0], chipErrorNum[bus][lamb][4][1]);
	      printf("\nChains not passing the test (for bus %d in lamb %d):", bus, lamb);
	      for (int chain =0; chain < MAXVMECOL_PER_LAMB; chain++){ 
		if( chipErrors[bus][lamb][chain][0] || chipErrors[bus][lamb][chain][1] ){
		  printf("\nchain %d, chip:", chain );
		  if( chipErrors[bus][lamb][chain][0] )
		    printf(" 0 (lamb chip %d),", ambslp_amchip_chip_number(chain, 0) );
		  if( chipErrors[bus][lamb][chain][1] )
		    printf(" 1 (lamb chip %d).", ambslp_amchip_chip_number(chain, 1) );
		}
	      }//for chain
	      printf("\n");

	    }//for lamb
	  }//if lamb not passed
	}//if bus not passed
      }//for bus

      for(int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){
	if(lambPassed[lamb])
	  printf("LAMB %d passed the test!\n", lamb);
      }

      return allPassed;
    }


    //get the chain number (from 0 to 7) and the chip number (0 or 1) and return the number of the chip on the LAMB
    int ambslp_amchip_chip_number(int chain, int chip){
      int chipLambNumber = 0 ;
      if( chain < MAXVMECOL_PER_LAMB && chip < MAXCHIP_PER_VMECOL)  //all other numbers do not make sense
	chipLambNumber = ( chain * 2 ) + chip ;
      else
        std::cerr << "Wrong chain number or wrong chip number!"<<std::endl;
      return chipLambNumber;
    }

    bool ambslp_amchip_prbsgen_inout(int slot, int lambver){ 
      const char* mode = "prbs";
      const char* columns="a";
      float freq = 2.;
      int chainlength(2), chipnum(3);
      bool to_return = true;
      if(lambver==1){
        chainlength = 0x4;
        chipnum = 0xf;
      }else if(lambver==2){
        chainlength = 0x2;
        chipnum = 0x3;
      }else{
        std::cerr<<"error version of lamb not supported! Abort!"<<std::endl;
        return false;
      }
      bool ret(true); 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus0", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus1", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus2", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus3", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus4", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus5", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus6", mode, freq);
      to_return = to_return and ret; 
      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "bus7", mode, freq);
      to_return = to_return and ret;
      if(lambver==1){
        chainlength = 0x4;
        chipnum = 0x1;
        columns="a";
      }else if(lambver==2){
        chainlength = 0x2;
        chipnum = 0x1;
        columns="o";
      }

      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "pattin1", mode, freq);
      to_return = to_return and ret;
      if(lambver==1){
        chainlength = 0x4;
        chipnum = 0x6;
        columns="a";
      }else if(lambver==2){
        chainlength = 0x2;
        chipnum = 0x2;
        columns="a";
      }

      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "pattout", mode, freq);
      to_return = to_return and ret;
      if(lambver==1){
        chainlength = 0x4;
        chipnum = 0x9;
        columns="a";
      }else if(lambver==2){
        chainlength = 0x2;
        chipnum = 0x1;
        columns="a";
      }

      ret = ambslp_amchip_SERDES_setup(slot, chainlength, chipnum, columns, "patt_duplex", mode, freq);
      to_return = to_return and ret;
      return to_return;
    } 

    bool ambslp_amchip_enable_bank_DUPLICATE(int slot, int chainlength, int chipnum){ 

      // definition of useful variables
      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS> outdata;
      std::array<uint32_t,MAXDATAWORDS> indata;

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (int i=0, chipno=0; i<chainlength; i++)
        chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      printf("chipno %x\n", chipno);  
      printf("chianlength %x\n", chainlength);

      //**********************************************************************1
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn); 

      // Set pattern address to 0
      // *** scrivo in C4 ***
      ConfigureScam(slot, chipno, active_columns, 0xC4, 1);  
      GotoShiftDRfromIdle(slot, active_columns);
      for(int i=0; i < 16; i++){
        indata[i] = 0x00000000;
      }
      for (int i=0; i<16;i++){
        indata[i+16*1] = indata[i];
        indata[i+16*2] = indata[i];
        indata[i+16*3] = indata[i];
      }
      AMshift(slot, active_columns, 16*4, indata.data(), outdata.data(), 1);
      GotoIdlefromExit1(slot, active_columns);

      // *** scrivo in C5 ***
      // write 0 pattern and enable pattern
      ConfigureScam(slot, chipno, active_columns, 0xC5, 1);  
      GotoShiftDRfromIdle(slot, active_columns);
      for(int i=0; i < 145; i++){
        if(i==0 or i==2 or i==18 or i==20 or i==36 or i==38 or i==54 or i==56 or i==72 or i==74 or i== 90 or i==92 or i==108 or i==110 or i==126 or i==128)
          indata[i] = 0xffffffff;
        else
          indata[i]=0x00000000;
      }
      for (int i=0; i<145;i++){
        indata[i+145*1] = indata[i];
        indata[i+145*2] = indata[i];
        indata[i+145*3] = indata[i];
      }
      AMshift(slot, active_columns, 145*4, indata.data(), outdata.data(), 1);
      GotoIdlefromExit1(slot, active_columns);
      for(int i=0; i<1000;i++){
        ConfigureScam(slot, chipno, active_columns, 0xD4, 1);
      }
      // *** scrivo in C5 ***
      // write 0 pattern and enable pattern
      ConfigureScam(slot, chipno, active_columns, 0xC5, 1);  
      GotoShiftDRfromIdle(slot, active_columns);
      for(int i=0; i < 145; i++){
        indata[i] = 0x00000000;
      }
      for (int i=0; i<145;i++){
        indata[i+145*1] = indata[i];
        indata[i+145*2] = indata[i];
        indata[i+145*3] = indata[i];
      }
      AMshift(slot, active_columns, 145*4, indata.data(), outdata.data(), 1);
      GotoIdlefromExit1(slot, active_columns);

      return true;
    } // end of ambslp_amchip_idcode_4chip


    bool ambslp_amchip_write_test_bank(int slot, int chainlength, int chipnum, int npatt, int offset){ 

      // definition of useful variables
      int active_columns =0;
      int chipno = 0;
      int bank[NPATTS][8];

      for(int jj=0;jj<NPATTS;jj++){
        for(int hh=0;hh<8;hh++){

          bank[jj][hh]=jj|(hh<<12);
        }
        printf("%i -esimo pattern: %x %x %x %x %x %x %x %x\n",jj,bank[jj][0],bank[jj][1],bank[jj][2],bank[jj][3],bank[jj][4],bank[jj][5],bank[jj][6],bank[jj][7]);
      }


      // u_int data[MAXDATAWORDS];  // unused var
      std::array<uint32_t,MAXDATAWORDS> outdata;
      std::array<uint32_t,MAXDATAWORDS> indata;

      // int idcode0 = 0;  // unused var
      // int idcode1 = 0; // unused var
      // int idcode2 = 0; // unused var
      // int idcode3 = 0; // unused var
      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (int i=0, chipno=0; i<chainlength; i++)
        chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;
      printf("chipno %x\n", chipno);  
      printf("chianlength %x\n", chainlength);

      //**********************************************************************1
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn); 

      // Set pattern address to 0
      // *** scrivo in C4 ***
      ConfigureScam(slot, chipno, active_columns, 0xC4, 1);  
      GotoShiftDRfromIdle(slot, active_columns);
      printf("Start writing from pattern %i", offset);
      for(int i=0; i < 16; i++){
        if((offset>>i)&0x1)indata[i] = 0xffffffff;
        else indata[i] = 0x00000000;
      }
      for (int i=0; i<16;i++){
        indata[i+16*1] = indata[i];
        indata[i+16*2] = indata[i];
        indata[i+16*3] = indata[i];
      }
      AMshift(slot, active_columns, 16*4, indata.data(), outdata.data(), 1);
      GotoIdlefromExit1(slot, active_columns);

      for(int i=0; i<npatt;i++){
	std::array<bool,145> wrdata_ir;
        for(int k=0;k<8;k++){
          int DR = expand_DC_bits(bank[i][k],2);
          for(int r=0; r <NBITS_CAMLAYER; r++){
            if((((DR>>r)&0x1)==1))
              wrdata_ir[((k*NBITS_CAMLAYER)+r)] = true;
            else
              wrdata_ir[((k*NBITS_CAMLAYER)+r)] = false;
          }	
        }
        wrdata_ir[144] = 0; 
        ConfigureRegister(slot, chipno, active_columns, 0xC5, wrdata_ir.data(), 145, outdata.data());
        ConfigureRegister(slot, chipno, active_columns, 0xC5, wrdata_ir.data(), 145, outdata.data(), true);
        ConfigureScam(slot, chipno, active_columns, 0xD4, 1);
      }

      int DR = 0x0;
      std::array<bool,145> wrdata_ir;
      for(int k=1;k<9;k++){
        for(int r=0; r <NBITS_CAMLAYER; r++){
          if((((DR>>r)&0x1)==1))
            wrdata_ir[((k*NBITS_CAMLAYER)+r)] = true;
          else
            wrdata_ir[((k*NBITS_CAMLAYER)+r)] = false;
        }  				
      }
      wrdata_ir[144] = false; 
      ConfigureRegister(slot, chipno, active_columns, 0xC5, wrdata_ir.data(), 145, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, 0xC5, wrdata_ir.data(), 145, outdata.data(), true);
      GotoTestReset(slot);

      return true;
    } // end of ambslp_amchip_idcode_4chip




    bool ambslp_amchip_8b_10b_all_buses(int slot, int chainlength, int chipnum ){ 


      const char * mode = "normal";
      const char * columns = "a";
      float freq = 2.;
      bool result = true;

      for ( int iBus=0; iBus<8; iBus++) {

        // allowed by C++11
        std::string busName = "bus";
        busName += std::to_string( iBus );

        result = result and ambslp_amchip_SERDES_setup( slot, chainlength, chipnum, columns, busName.c_str(), mode, freq ); 
      }

      return result;

    }



    bool ambslp_amchip_prbsgen_inout_all_buses(int slot, int chainlength, int chipnum){ 



      const char * mode = "prbs";
      const char * columns = "a";
      float freq = 2.;
      bool result = true;

      for ( int iBus=0; iBus<8; iBus++) {

        // allowed by C++11
        std::string busName = "bus";
        busName += std::to_string( iBus );

        result = result and ambslp_amchip_SERDES_setup( slot, chainlength, chipnum, columns, busName.c_str(), mode, freq ); 
      }

      return result;
    } 



    bool ambslp_amchip_polling_stat_reg_pattin(int slot, int chainlength, int chipnum, const char* columns, const char* serdes,  bool prbsEnabled){ 
      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS> outdata;
      int i(0);

      int all_chipcolumn = 0;
      int even_chipcolumn=0;
      int odd_chipcolumn=0;
      for(unsigned int t=0;t<MAXCOLUMNS;t++){
        if((t%2)==0) even_chipcolumn |= 1<<t;
        else odd_chipcolumn |= 1<<t;
        all_chipcolumn |= 1<<t;
      }
      if(strcmp(columns, "a")==0) active_columns = all_chipcolumn;
      else if (strcmp(columns, "o")==0) active_columns = odd_chipcolumn;
      else if (strcmp(columns, "e")==0) active_columns = even_chipcolumn;
      else{
        std::cerr<<"error: Found an invalid option for columns: '"<<columns<<"'. Abort!"<<std::endl;
        return -1;
      }

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      //printf("chipno %x\n", chipno);  
      //printf("chainlength %x\n", chainlength);

      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn);
      //INIZIO CONFIGURAZIONE BUS0!!!!!!**************************************
      //**********************************************************************6
      //**********************************************************************
      //***scrivo in C9 0x00 ************************ 

      int DR = 0x0;
      if ( strcmp(serdes, "pattin1")==0 )
        /*int*/ DR = 0x00000002;   // why int was there?!?!?!

      std::array<bool,32> wrdata;
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1)
          wrdata[i] = true;
        else
          wrdata[i] = false;
      }			

      ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, 0);
      ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true); 
      //ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata, 7, outdata); Output not needed
      //ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata, 7, outdata); 
      //PrintRegisterContent(chipno, 7, outdata); 
      //***scrivo in EA 0x0000************************ 
      sleep(1);
      DR = 0x00000000;
      DR = 0x00000000;
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1)
          wrdata[i] = true;
        else
          wrdata[i] = false;
      }			
      ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, 0xEA, wrdata.data(), 32, outdata.data(), true);
      //PrintRegisterContent(chipno, 32, outdata);

      int regOut0(0); 
      int regOut1(0);
      int errorsCounted = 0;
      bool testPassed = true;
      for(int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){
	for (int chain =0; chain < MAXVMECOL_PER_LAMB; chain++){   
	  for(int i=0; i<chainlength*32; i++){
	    if(i<32) regOut0 = regOut0 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
	    else if(i>=32 && i<2*32) regOut1 = regOut1 + (((outdata[i]>>((lamb*8)+chain))&0x1) << i);
	  }	
	  if( (strcmp(serdes, "pattin0")==0) || (strcmp(serdes, "pattin1")==0 && chain%2!=0) )
	    {
	      errorsCounted = (regOut1>>8)&0xff;
	      if(prbsEnabled && (errorsCounted != 0) )
		{
		  testPassed = false;
		  printf("Found error in chain %i LAMB %i chip %i\t", chain, lamb, ambslp_amchip_chip_number(chain, 0));
		  printf("%x \n", errorsCounted );
		}
	      if( (!prbsEnabled) && (errorsCounted < 90) )
		{
		  testPassed = false;
		  printf("Found error in chain %i LAMB %i chip %i\t", chain, lamb, ambslp_amchip_chip_number(chain, 0));
		  printf("%x \n", errorsCounted );
		}
	    }
	  regOut0 = 0;
	  regOut1 = 0;
	}
      }//for(int lamb=0 ...

      return testPassed;
    }


    bool ambslp_amchip_prbs_error_count_all_buses(int slot, int chainlength, int chipnum, const char* policy, bool verbose = true ){

      int active_columns =0;
      int chipno = 0;

      // u_int data[MAXDATAWORDS];  // unused var
      std::array<uint32_t,MAXDATAWORDS> outdata;
      // u_int indata[MAXDATAWORDS];  // unused var
      int i; // ,j;  // j is unused var

      // int idcode0 = 0; // unused var
      // int idcode1 = 0; // unused var
      // int idcode2 = 0; // unused var
      // int idcode3 = 0; // unused var

      int all_chipcolumn = 0xffffffff;
      active_columns = 0xffffffff;

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      if(verbose){
	printf("chipno %x\n", chipno);  
	printf("chainlength %x\n", chainlength);
      }
      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn);

      for(int bus=0; bus<8;bus++){
        //INIZIO CONFIGURAZIONE BUS0!!!!!!**************************************
        //Scrivo 0x02 nel registro 0xC9 *****************************************
        int DR = 0x08+bus;
	std::array<bool,32> wrdata;
        for(int i=0; i < 32; i++){
          if((((DR>>i)&0x1)==1))
            wrdata[i] = true;
          else
            wrdata[i] = false;
        }

        ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data());
        ConfigureRegister(slot, chipno, active_columns, 0xC9, wrdata.data(), 7, outdata.data(), true); 
	if(verbose){PrintRegisterContent(chipno, 7, outdata.data());}


        // select enable or disable
        sleep(1);
        DR = 0x00000001;
        if ( strcmp( policy, "enable" )==0 )
          DR = 0x00001101;

        for(int i=0; i < 32; i++){
          if (((DR>>i)&0x1)==1)
            wrdata[i] = true;
          else
            wrdata[i] = false;
        }	
		
        ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data());
        ConfigureRegister(slot, chipno, active_columns, 0xCA, wrdata.data(), 32, outdata.data(), true);
        if(verbose){PrintRegisterContent(chipno, 32, outdata.data());}   
      }

      return true;
    }



    bool ambslp_amchip_prbs_error_count(int slot, int chainlength, int chipnum, const char* columns, const char* policy, const char* serdes, bool verbose = true ){ 

      int active_columns =0;
      int chipno = 0;

      std::array<uint32_t,MAXDATAWORDS> outdata;
      int i(0);

      int all_chipcolumn = 0;
      int even_chipcolumn=0;
      int odd_chipcolumn=0;
      for(unsigned int t=0;t<MAXCOLUMNS;t++){
        if((t%2)==0) even_chipcolumn |= 1<<t;
        else odd_chipcolumn |= 1<<t;
        all_chipcolumn |= 1<<t;
      }
      if(strcmp(columns, "a")==0) active_columns = all_chipcolumn;
      else if (strcmp(columns, "o")==0) active_columns = odd_chipcolumn;
      else if (strcmp(columns, "e")==0) active_columns = even_chipcolumn;
      else{
        std::cerr<<"error: Found an invalid option for columns: '"<<columns<<"'. Abort!"<<std::endl;
        return -1;
      }

      /* build chip number */
      for (i=0, chipno=0; i<chainlength; i++) chipno = (chipno<<1) + 1; 
      chipno = (chainlength<<8) + chipnum;

      if(verbose)
	{
	  printf("chipno %x\n", chipno);  
	  printf("chainlength %x\n", chainlength);
	}
      //**********************************************************************
      //**********************************************************************
      GotoTestReset(slot);
      GotoIdlefromReset(slot, all_chipcolumn);

      //**********************************************************************2
      //**********************************************************************
      //***scrivo in C9 (SERDES_SEL 0XC9) 0x02 (SERDES_SEL_SIZE 7) *************************
      //with SERDES_SEL I select either pattin0/pattout (patt_duplex) writing 0000, either pattin2 writin 0010 (2)
      //in both cases it is selecting SysConfigReg (7:4, 000) and if "patt_duplex" selects pattin0/pattout (3:0, 0000), if("pattin1")  selects pattin 1 (3:0, 0010) 
      sleep(1);     
      int DR = 0x00000002;
      if ( strcmp(serdes, "patt_duplex")==0 )
        DR= 0x0;
      std::array<bool,32> wrdata;
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1)
          wrdata[i] = true;
        else
          wrdata[i] = false;
      }			
      ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata.data(), 7, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, SERDES_SEL, wrdata.data(), 7, outdata.data(), true);
      if(verbose){PrintRegisterContent(chipno, 7, outdata.data()); }

      //***scrivo in CA (SERDES_REG 0XCA) 0x00000001 (SERDES_REG_SIZE 32)************************ 
      //now the SysConfigReg operation mode is chosen, the PRBS mode is set (1:0, 01), patSel is set to 0 (6:4, 000). 
      sleep(1);
      DR = 0x00000001; // errRd8 and errRd32 are set to 0 (8, 0), (12, 0), this means that the error counters are reset to 0.
      if ( strcmp(policy, "enable")==0 )
        DR = 0x00001101;   // errRd8 and errRd32 are set to 1 (8, 1), (12, 1), this means that the error counters are enabled.
      else
	if( strcmp(policy, "pattern")==0)
	  DR = 0x00001100;   // counters enabled (8, 1), (12, 1), and pattern mode setted.	  
      for(int i=0; i < 32; i++){
        if (((DR>>i)&0x1)==1)
          wrdata[i] = true;
        else
          wrdata[i] = false;
      }			
      ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data());
      ConfigureRegister(slot, chipno, active_columns, SERDES_REG, wrdata.data(), 32, outdata.data(), true);
      if(verbose){PrintRegisterContent(chipno, 32, outdata.data());}   

      return true;
    }

    ////////////////////////////
    // ambslp_amchip_selftest //
    ////////////////////////////

    /**
     * \brief This function sets up and executes the chip built in test.
     * \param vme Pointer to a VMEInterface object (the board interface)
     * \param chip_mask mask active chips in one jtag chain
     * \param columns_mask mask active columns in the AMboard
     * \return each bit of the returned long unsigned int represent thge test result for a single chip ordered as ((chain*chips_in_chain)+chip_position in chain). The bit is set to '1' if the test succeded,else '0'. For disabled chips the result is '0' by default. 
     * The selftest procedure is performed by configuring a bult-in self test register, giving a start signal and comparing the result with the expeted one.
     * The self test configuration is performed in two steps: 1) the CRC_REG is confugured and oput in a reste state. 2) The reset bit is put to 0 and the configuration is loaded (for more details on the BIST configuration please see AMChip-06 documentation)
     * The self test is started with a start signal fed throgh the SELFTEST JTAG register. Execution of the test can be monitored with the DTESTOUT pin, '0' == executing, '1' == DONE. The test takes around one second. (Since we have no direct access to DTESTOUT pin we wait 2 seconds).
     * The result of the test is retrieved reading back the CRC_REGREGISTER register.
     * NOTE: THE AMCHIPS ARE EXPECTED TO BE IN TEST MODE. This function does not act on the chip configuration, you can set-up the cip configuration throgh the ambslp_amchip_jpatt_cfg funcion. You have to be aware of the chip configuration. Disabled AM-blocks will result in test failure. 
     * NOTE: AS A BY-PRODUCT THIS PROCEDURE PUTS ALL PATTERNS IN A KNOWN STATE, ACTIVE WITH MSB SET TO '1'. If the requirements in the previous NOTE are not fillfilled than the final configuration is unkonw.
     */

    long unsigned ambslp_amchip_selftest(VMEInterface *vme, unsigned chip_mask, unsigned columns_mask){

      long unsigned DR(0x0);                                          //the data you want to write in a single chip register
      long unsigned result(0x0);                                      //the result
      unsigned bits_before(0x0);                                      //counter of already written bits in shift registers
      unsigned chipno = chip_mask + (MAXCHIP_PER_VMECOL << 8);        //chips you want to put in bypass mode
      std::array<uint32_t,SELFTEST_SIZE*MAXCHIP_PER_VMECOL> indata;
      std::array<uint32_t,SELFTEST_SIZE*MAXCHIP_PER_VMECOL> outdata;

      //initialize vme input and output 
      for(int i = 0; i< SELFTEST_SIZE*MAXCHIP_PER_VMECOL; i++ ){
        indata[i]=0x0;
        outdata[i]=0x0;
      }
      
      //reset selftest
      DR = 0x27ffff7ff000;
      bits_before = 0x0;
      for(int j = 0; j< MAXCHIP_PER_VMECOL; j++){
        if (((chip_mask>>j)&0x1) == 0x1){
          for(int i=1; i<SELFTEST_SIZE;i++){
            if(((DR>>i)&0x1) == 0x1) indata[i+bits_before] = columns_mask;
            else indata[i+bits_before] = 0x0;
          }
          bits_before += SELFTEST_SIZE;
        }else{
          bits_before += 1;
          indata[bits_before] = 0x0;
        }
      }
      GotoTestReset(vme);
      GotoIdlefromReset(vme, columns_mask);
      ConfigureScam(vme, chipno, columns_mask, SELFTEST, 1);
      GotoShiftDRfromIdle(vme, columns_mask);
      AMshift(vme, columns_mask, bits_before, indata.data(), outdata.data(), 1);

      //remove selftest reset
      DR = 0x07ffff7ff000;
      bits_before = 0x0;
      for(int j = 0; j< MAXCHIP_PER_VMECOL; j++){
        if (((chip_mask>>j)&0x1)==0x1){
          for(int i=1; i<SELFTEST_SIZE;i++){
            if((DR>>i)&0x1) indata[i+bits_before] = columns_mask;
            else indata[i+bits_before] = 0x0;
          }
          bits_before += SELFTEST_SIZE;
        }else{
          bits_before += 1;
          indata[bits_before] = 0x0;
        }
      }
      GotoTestReset(vme);
      GotoIdlefromReset(vme, columns_mask);
      ConfigureScam(vme, chipno, columns_mask, SELFTEST, 1);
      GotoShiftDRfromIdle(vme, columns_mask);
      AMshift(vme, columns_mask, bits_before, indata.data(), outdata.data(), 1);

      //start selftest
      DR = 0x07ffff7ff001;
      bits_before = 0x0;
      for(int j = 0; j< MAXCHIP_PER_VMECOL; j++){
        if (((chip_mask>>j)&0x1)==0x1){
          for(int i=1; i<SELFTEST_SIZE;i++){
            if(((DR>>i)&0x1) == 0x1) indata[i+bits_before] = columns_mask;
            else indata[i+bits_before] = 0x0;
          }
          bits_before += SELFTEST_SIZE;
        }else{
          bits_before += 1;
          indata[bits_before] = 0x0;
        }
      }
      GotoTestReset(vme);
      GotoIdlefromReset(vme, columns_mask);
      ConfigureScam(vme, chipno, columns_mask, SELFTEST, 1);
      GotoShiftDRfromIdle(vme, columns_mask);
      AMshift(vme, columns_mask, bits_before, indata.data(), outdata.data(), 1);
      
      //wait for test to finish; to be modified. When the test is done DTESTOUT pin goes to '1', by now we are not able to read it.
      sleep(2);

      //read test result
      DR = 0x0;
      bits_before = 0x0;
      for(int j = 0; j< MAXCHIP_PER_VMECOL; j++){
        if (((chip_mask>>j)&0x1)==0x1){
          for(int i=1; i<CRC_REG_SIZE;i++){
            if((DR>>i)&0x1) indata[i+bits_before] = columns_mask;
            else indata[i+bits_before] = 0x0;
          }
          bits_before += CRC_REG_SIZE;
        }else{
          bits_before += 1;
          indata[bits_before] = 0x0;
        }
      }
      GotoTestReset(vme);
      GotoIdlefromReset(vme, columns_mask);
      ConfigureScam(vme, chipno, columns_mask, CRC_REG, 1);
      GotoShiftDRfromIdle(vme, columns_mask);
      AMshift(vme, columns_mask, bits_before, indata.data(), outdata.data(), 1);
      for(int lamb=0; lamb<MAXLAMB_PER_BOARD; lamb++){
        for (int chain =0; chain < MAXCHIP_PER_VMECOL; chain++){
	  if( not((columns_mask>>chain)&0x1)) continue;
          bits_before = 0x0;
	  for (int chip =0; chip < MAXCHIP_PER_VMECOL; chip++){  
            if((chip_mask>>chip)&0x1){
              unsigned data(0x0);
              for(int i = 0; i<CRC_REG_SIZE; i++) data = data + (((outdata[i+bits_before]>>((lamb*8)+chain))&0x1) << i);
              bits_before += CRC_REG_SIZE;
              result |= ((data == 0x1edd9dcb)<<((chain*MAXCHIP_PER_VMECOL)+chip));
	    }else{
              bits_before += 1;
              result |= (0x0<<((chain*MAXCHIP_PER_VMECOL)+chip));
            }
          }
        }
      }

      //clear SLEFTEST register
      DR = 0x0;
      bits_before = 0x0;
      for(int j = 0; j< MAXCHIP_PER_VMECOL; j++){
        if (((chip_mask>>j)&0x1)==0x1){
          for(int i=1; i<SELFTEST_SIZE;i++){
            if((DR>>i)&0x1) indata[i+bits_before] = columns_mask;
            else indata[i+bits_before] = 0x0;
          }
          bits_before += SELFTEST_SIZE;
        }else{
          bits_before += 1;
          indata[bits_before] = 0x0;
        }
      }
      GotoTestReset(vme);
      GotoIdlefromReset(vme, columns_mask);
      ConfigureScam(vme, chipno, columns_mask, SELFTEST, 1);
      GotoShiftDRfromIdle(vme, columns_mask);
      AMshift(vme, columns_mask, bits_before, indata.data(), outdata.data(), 1);
      return result; 
    }
  } // namespace ftk      
} // namespace daq 
