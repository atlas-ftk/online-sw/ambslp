/* ambslp+boardsim
 * 2015-06-23 Guido Volpi
 * The program provides an interface the emulate the expected output from a single board */

//#include "rcc_error/rcc_error.h"
//#include "vme_rcc/vme_rcc.h"
#include "ambslp/ambslp.h"
#include "ftkcommon/patternbank_lib.h"

#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <set>
using namespace std;

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
using namespace  boost::program_options;


namespace daq {
  namespace ftk {
    unsigned int boardsim_verbosity(0); // messaging level

    FTKPatternBank PattBank; // Object handling the pattern bank
    unsigned SSoff(0); // flag to check if the offset to avoid SS=0 has to be applied

    string NDC("");

    unsigned int ninput_files(0); // number of input files
    std::vector<fstream> datafile; // vector of input files
    std::vector<unsigned int>dataiomap; // map from input busses to chip busses
    std::vector<set<unsigned int>> chipdata; // input dataa
    ofstream outfile; //output file

    unsigned int match_threshold(7); // match threshold
    vector<unsigned int> output_roads; // list of output roads

    unsigned int NChipsPerLAMB(16); // number of chips within a LAMB mezzanine
    unsigned int NChipsPerChain(4);  // number of chips within a readout chain
    unsigned int NLAMBs(4); // number of LAMBS
    unsigned int NPattsPerChip(2048); // number of patterns stored within a chip

     // setup the Pattern bank
    void ambslp_boardsim_init(unsigned int nlayers, unsigned int nfiles) {
      PattBank.setNLayers(nlayers); // set the number of the layers

      // input can arrive through a number of files larger than the numebr
      ninput_files = nfiles;
      datafile = std::vector<fstream>(nfiles);
      dataiomap = std::vector<unsigned int>(nfiles);
      chipdata = std::vector< set<unsigned int> >(nlayers);

      // set an offset for the SS to avoid SS 0
      for (unsigned int il=0;il!=nlayers;++il) PattBank.setSSOffset(il,SSoff);

      // set the DC expcted for the bank
      if (NDC.size()>0) {
        // split the string using :
        vector<string> split_vec;
        boost::algorithm::split(split_vec, NDC, boost::algorithm::is_any_of(":"));


        for (unsigned int il=0;il!=nlayers;++il) {
          // parse the string and cast to integer
          unsigned int ndc_val = stoi(split_vec[il]);
          PattBank.setDCConfig(il,ndc_val);
        }

      }

    }

    int ambslp_boardsim_loadpatterns(string bankpath, unsigned int type, unsigned int npatterns) {
      // load the patterns into the PattBank object
      int res = PattBank.loadPatternBank(bankpath.c_str(),type,npatterns);

      PattBank.printConfiguration();

      return res;
    }

    /**
     * Set the input file for a given input bus, according the AUX setup convention, and
     * the output bus ID, the layer in the pattern matching.
     * */
    void ambslp_boardsim_setinput(string inputfile, unsigned int inbusid, unsigned int outbusid) {
      dataiomap[inbusid] = outbusid;
      datafile[inbusid].open(inputfile.c_str());
      if (!datafile[inbusid]) {
        cerr << "Error opening the input data file: " << inputfile << endl;
      }
    }

    /** The function matches the hit in the current event with the
     * patterns stored in board
     */
    void ambslp_boardsim_matchpatterns() {
      output_roads.clear(); // clear the data


      const unsigned int nlayers = PattBank.getNLayers(); // retrieve the number of layers
      std::vector<uint32_t> curpatt(nlayers); // global variable used to retrieve a pattern
      std::vector<uint32_t> curdc(nlayers); // global variable used to retrieve a pattern DC bits

      if (boardsim_verbosity>0) {
        // print information on the input data
        for (unsigned int il=0;il!=nlayers;++il) { // layers loop
          cout << "Number of hits in layer " << il << " = " << chipdata[il].size() << endl;
          cout << hex;
          for (auto w: chipdata[il]) cout << w << " ";
          cout << dec << endl;
        }
      }

      unsigned int numHitExistLayer = 0;
      for (unsigned int il=0;il!=nlayers;++il) { // layers loop
	if(chipdata[il].size() > 0) numHitExistLayer++;
      }

      // skip pattern matching to perform boardsim speedy,
      // especially in case prescale is used and almost all of the events do not have hit data.
      if(numHitExistLayer >= match_threshold){
      for (unsigned int ipatt=0; ipatt!=PattBank.getNPatterns(); ++ipatt) { // loop over the patterns
        unsigned int bitmask(0); //match bitmask
        unsigned int layercounter(0); // counter of the number of matched layers

        // retrieve the pattern
        PattBank.getPatternAndDC(ipatt,curpatt, curdc);

        for (unsigned int il=0;il!=nlayers;++il) { // layers loop
          // extract the number of DC bits and possible number of configurations
          const unsigned int ndcbits(PattBank.getDCConfig(il));

          // variable to hold the number of configuration to consider as result of the number DC bits that were set
          unsigned int ndcconf(1);
          for (unsigned int ibit=0; ibit!=ndcbits; ++ibit) {
            // if a DC bit is set the number of configuration to check is incremented
            // because they are increment as power of 2 the result is obtained just
            // shift left the ndcconf value
            if (curdc[il]&(1<<ibit)) ndcconf <<= 1; // *=2
          }

          unsigned basess = curpatt[il] & (~curdc[il]); // make sure the basess bits are q0 in all position involved by DC bits

          for (unsigned int dcconf=0; dcconf!=ndcconf; ++dcconf) { // loop over the configurations caused by the DC
            unsigned int dcmask(0); // this variable will represent the current configuration, compatible with the DC
            unsigned int pos(0); // pointer to the bit position in the dcconf to be copied into the dcmask, used by the following loop
            for (unsigned ibit=0; ibit!=ndcbits; ++ibit) { // loop over the bits involved by the DC
              /* check where the different bits of the dcconf counter has to be
               * redistributed. Bits not involved by the DC bits need to be left
               * untouched, only the bits where DC bits are set need to loop over
               * the configurations.
               */
              if (curdc[il]&(1<<ibit)) {
                // if the bit is involved by the DC the current position in the counter
                // is copied. The bit at the "pos" place is extracted and its value is 
                // set into the "ibit" position of the dcmask variable
                dcmask |= ((dcconf>>pos)&1) << ibit;
                pos += 1; // move the position for the next bit
              }
            } // end loop the the bits involved by the DC

            // create the SS for the current configuration, given the base SS for the pattern
            // the base SS is assumed to be set to 0 where the DC is set
            unsigned curss = basess | dcmask;

            // check if the SS linked to the current pattern is compatible with the current event
            if (chipdata[il].find(curss)!=chipdata[il].end()) {
              // if the SS has been found update the road matching info
              bitmask |= 1<<il;
              layercounter += 1;
              break; // avoid futher updates to the bitmask (useless) and layercounter (error)
            }
          } // end loop over the configurations compatible with the DC setup

          /* calculate the maximum threshold that this pattern can reach in
           * the next layers. If the threshold cannot be reached the loop
           * is stopped
           */
          if (layercounter+nlayers-il-1<match_threshold) break; // this pattern cannot match
        } // end layers loop

        if (layercounter>=match_threshold) {
          /* to convert the index of the pattern in the road ID uses a helper function.
           * The helper function parameters are the number of patterns per chip and the
           * first useful position within the chip */
          unsigned int roadID = FTKPatternBank::getHWRoadID(ipatt,NPattsPerChip,0);
          unsigned int outword = roadID | (bitmask<<24);

          output_roads.push_back(outword); // put the output in the output
        }
      }

      }
    }

    /** This method reads the input data for the emulation from multiple files,
     * as used by the routines used to feed hits in the AUX output spy-buffers.
     */
    int ambslp_boardsim_readdata_auxfeedhit() {
      // EE word
      unsigned EEword(0);

      // scan all the input file until the begin of next event
      for (unsigned int ifile=0; ifile!=ninput_files;++ifile) { // file loop
        bool keepgoing(true);
        do {

          unsigned int tagword;
          unsigned int dataword;
          datafile[ifile] >> tagword >> hex >> dataword;

          // when the end of a single file is reached before the EE word this
          // indicates that there aren't other good events to be read
          if  (datafile[ifile].eof()) return -1; // no good input, triggers the end of the task
          else if (!datafile[ifile]) {
            // generic I/O error, all stops
            cerr << "Error reading input file " << ifile << endl;
            return -1;
          }
          else if (tagword==1) { // good input, check if it is a EE
            if (EEword==0) EEword =  dataword; // set the EE tag from the first file
            else if (EEword!=dataword)  {
              // verify the other files are representing the same event
              cerr << "Input files got out of synch" << endl;
              return -1;
            }
            break;
          }


          // if the data word is an hit the data is stored accoridng the outputbuts, stored in the I/O map variable
          // each 32 bit word contains 2 single 16 bit words
          const unsigned int halfwordmask(~((~0)<<16));
          chipdata[dataiomap[ifile]].insert(dataword&halfwordmask);
          chipdata[dataiomap[ifile]].insert((dataword>>16)&halfwordmask);
        } while (keepgoing);
      } // end file loop

      return EEword;
    }


    /** This method parses a file for the input data that is in the same format
     * used by the code that loads the input FIFOs of the HIT FPGA in the AMB.
     */
    int ambslp_boardsim_readdata_ambfeedhit() {
    	// constant array representing the mapping between the column position
    	// within the file and the matching bus
    	const unsigned int columnsmap[] = {4, 5, 6, 7, 0, 0, 1, 1, 2, 2, 3, 3};

    	unsigned int EEword(0); // EE word
    	unsigned int EEMask(0); // keep track of which columns met the EE conditions

    	do { // file read loop

    		// each data is a 36-bit word containing a tag and a real data word
    		unsigned long long int dataword;

    		for (unsigned int ibus=0; ibus!=12; ++ibus) { // columns loop
    			// each time a different column of the same row is read
    			datafile[0] >> hex >> dataword;

    			if (datafile[0].eof()) {
    				// end of the file reached
    				cerr << "End of the file reached" << endl;
    				return -1;
    			}
    			else if (!datafile[0]) {
    				// other I/O errors
    				cerr << "Error reading from the input file" << endl;
    				return -1;
    			}

    			const unsigned int tag(dataword>>32); // the [39:36] bits represent a tag
    			const unsigned int word(dataword&0xffffffff); // the 32-bit part represents the real data

    			//cout << hex << dataword << " " << tag << " " << word << endl;
    			if (tag==0x4) {
    				// just skip, go to the next column
    				continue;
    			}
    			else if (tag==0x8) {

    				// for the EE word only 24 bits are valid, avoid to appear negative when used as signed integer
    				unsigned int curEEword(word&0xffffff);

    				// end-event tag
            if (EEMask==0) EEword =  curEEword; // set the EE tag from the first file
            else if (EEword!=curEEword || ((EEMask>>ibus)&1))  {
              // verify the other files are representing the same event and that this
            	// columns didn't reach already an EE condition
              cerr << "Input file's columns got out of synch" << endl;
              return -1;
            }

            EEMask |= (1<<ibus);

            if (EEMask==0xfff) {
            	// all columns read the N-th event, prepare to close the event

            	// if the current column isn't the last all content untill the end of the line
            	// can be discarded
            	ibus += 1;
            	for (;ibus!=12; ++ibus) datafile[0] >> dataword;
            	//datafile[0].ignore(500, '\n');

            	return EEword;
            }
    			}
    			else {
    				// insert the dataword into the stream for a specific bus

    				// if the data word is an hit the data is stored accoridng the outputbuts, stored in the I/O map variable
    				// each 32 bit word contains 2 single 16 bit words
    				const unsigned int halfwordmask(~((~0)<<16));
    				chipdata[columnsmap[ibus]].insert(word&halfwordmask);
    				chipdata[columnsmap[ibus]].insert((word>>16)&halfwordmask);
    			}
    		} // end columns loop

    	} while(1); // end file read loop

    	// the code should never reach this point, in this case this is an error condition
    	return -1;
    }

    /** Get the next event, load the data, perform the pattern matching */
    int ambslp_boardsim_nextevent() {
      // reset the data
      for (unsigned int i=0;i!=PattBank.getNLayers();++i) chipdata[i].clear();

      // the methods used to read the next event will return the event tag
      int EEword(0);

      if (ninput_files>1) {
      	EEword = ambslp_boardsim_readdata_auxfeedhit();
      }
      else {
      	EEword = ambslp_boardsim_readdata_ambfeedhit();
      }

      // negative values for the event tag represents errors or no more events
			if (EEword<0) return EEword;

      // run the matching procedure
      ambslp_boardsim_matchpatterns();

      for (auto word : output_roads) {
        outfile << hex << word << endl;
      }
      // printing the EE word the 0xf7800000 tag has to be added
      outfile << hex << (0xf7800000 | EEword) << dec << endl;

      if (boardsim_verbosity>0) {
        for (auto word : output_roads) {
          cout << hex << word << endl;
        }
        // printing the EE word the 0xf7800000 tag has to be added
        cout << hex << (0xf7800000 | EEword) << dec << endl;
      }
      return 0;
    }
  }
}

int main(int argc, char **argv){


  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
        ("help,h", "produce help message")
        ("NChipsPerLAMB", value<unsigned int>(&daq::ftk::NChipsPerLAMB)->default_value(16), "Number of chips within a LAMB")
        ("NChipsPerChain", value<unsigned int>(&daq::ftk::NChipsPerChain)->default_value(4), "Number of chips within a chain")
        ("NLAMBs", value<unsigned int>(&daq::ftk::NLAMBs)->default_value(4), "Number LAMB mezzanines")
        ("NPattsPerChip,N", value<unsigned int>(&daq::ftk::NPattsPerChip)->default_value(2048), "Number of patterns within a chip")
        ("BankDCConfig", value<string>(&daq::ftk::NDC)->default_value(""), "String of the DC bits: e.g. 2:2:2:2:2:2:2:2")
        ("bankType,T", value<unsigned int>()->default_value(1), "File type for the pattern bank: 0 DC cache, 1 ASCII file")
        ("verbose,v", "If present verbose option will be used")
        ("ssoff","If present the SS offset is used to avoid SS=0, shift set to 8")
        ("nEvents,e", value<int>()->default_value(-1), "Number of events to emulate, -1 (def) all events")
        ("output-file", value<string>()->default_value("outroads_sim.out"), "Output file containing the result of the simulation")
        ("pattern-file", value<string>()->required(), "Pattern file")
        ("input-file", value< vector<string> >()->required(), "Input file")
        ("threshold",value<unsigned int>(&daq::ftk::match_threshold)->default_value(7), "Match threshold")
        ;

  /*
   * unsigned int NChipsPerLAMB(16); // number of chips within a LAMB mezzanine
    unsigned int NChipsPerChain(4);  // number of chips within a readout chain
    unsigned int NLAMBs(4); // number of LAMBS
    unsigned int NPattsPerChip(2048); // number of patterns stored within a chip
   */
  positional_options_description pd;
  pd.add("pattern-file", -1);
  pd.add("input-file", -1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    notify(vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
  //catch(std::bad_alloc e) // In case of errors during the parsing process, desc is printed for help
  {
    //    std::cerr << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  }

  if( vm.count("help") ) // if help is required, then desc is printed to output
  {
    std::cout << std::endl <<  desc << std::endl ;
    return 0;
  }


  daq::ftk::boardsim_verbosity = vm.count("verbose");
  cout << "Verbosity level: " << daq::ftk::boardsim_verbosity << endl;

  daq::ftk::SSoff = vm.count("ssoff") > 0 ? 8 : 0;

  vector<string> inputs = vm["input-file"].as< vector<string> >();
  unsigned int nfiles = inputs.size();
  cout << "Input files: " << nfiles << endl;

  // prepare the internal memory for the files
  daq::ftk::ambslp_boardsim_init(8,nfiles);

  // retrieve the information to open the bank file
  std::string pattfname = vm["pattern-file"].as<string>();
  unsigned int banktype = vm["bankType"].as<unsigned int>();
  // load the pattern bank in memory

  /* the number of patterns to be loaded is calculated as function of the installed chip
   * and number of patterns requested to be loaded within each chip
   */
  unsigned int npatterns = daq::ftk::NPattsPerChip*daq::ftk::NLAMBs*daq::ftk::NChipsPerLAMB;
  daq::ftk::ambslp_boardsim_loadpatterns(pattfname, banktype, npatterns);

  // retrieve the number of events, if -1 means all the events in the input files
  int nevents = vm["nEvents"].as<int>();

  // Set the files from which the input has to be read
  cout << "Setting the input files" << endl;
  if (nfiles==8) {
    // in this setup there is an input file for each bus
    for (unsigned int i=0; i!=8; ++i) daq::ftk::ambslp_boardsim_setinput(inputs[i], i, i);
  }
  else if (nfiles==12) {
    // in this setup there are 2 input files for the first 4 busses and 1 for the others

    // the first 8 input streams are assigned in pairs to the same input stream
    for (unsigned int i=0; i!=8; ++i) daq::ftk::ambslp_boardsim_setinput(inputs[i], i, i/2);
    // the next 4 streams are assigned to a single output stream
    for (unsigned int i=8; i!=12; ++i) daq::ftk::ambslp_boardsim_setinput(inputs[i], i, i-4);
  }
  else if (nfiles==1) {
  	// in this case the input is in the format used for AMB FIFO load: 1 file with 12 columns
  	daq::ftk::ambslp_boardsim_setinput(inputs[0], 0, 0);
  }
  else {
    cerr << "The configuration with " << nfiles << " input files is not supported" << endl;
    return -1;
  }

  // open the output file
  daq::ftk::outfile.open(vm["output-file"].as<string>());

  // The loop ends when a _nextevent returns not 0 value or when all the
  // requested events are read
  for (int i=0; i!=nevents; ++i) { // loop over the events
    cout << "Running event " << i << endl;
    if (daq::ftk::ambslp_boardsim_nextevent()!=0) {
      cout << "No more events" << endl;
      break;
    }
  } // end loop over the events

  // close the output file
  daq::ftk::outfile.close();

  return 0;
}
