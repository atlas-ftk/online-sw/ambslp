
#ifndef AMB_HARDWARE_NOMINAL_CONFIG_FSM_H
#define AMB_HARDWARE_NOMINAL_CONFIG_FSM_H

#include "ambslp/configuration.h"
#include "state.h"
#include <map>



namespace daq {
  namespace ftk {


    class   AMB_state;
    struct  OptionHandle;
    struct  InitConfiguration;

    typedef  std::pair< AMB_state*, std::map<ConfigExitCode, Step> > SingleTransition; 
    typedef  std::map< Step, SingleTransition > Transitions;

// description of the nominal bootstrap FSM
//
    class AMBConfiguration_nominal : public AMBConfiguration {

     public:
      AMBConfiguration_nominal( InitConfiguration *init );
      virtual ~AMBConfiguration_nominal();

      void show( std::ostream & aStream );                             
      AMBStatus * configure(OptionHandle *opt);        
      void restartFrom( ConfigurationStep *step );      

     private:
      AMB_state * latestState;
      Transitions transitions;
    };
//---------------------------

  }  // namespace ftk
}  // namespace daq


#endif  // AMB_HARDWARE_NOMINAL_CONFIG_FSM_H


