
#ifndef AMB_HARDWARE_STATUS_STATE_H
#define AMB_HARDWARE_STATUS_STATE_H

#include <iostream>

namespace daq {
  namespace ftk {


    class AMBStatus;
    class OptionHandle;
    class StateInfo;

    enum ConfigExitCode{ FAILURE = 0, //
	                 SUCCESS, //
                         UNDEFINED };



    class AMB_state {

     public:
      AMB_state( AMBStatus * current );
      virtual ~AMB_state();
      void show( std::ostream &aStream );                          //  push this state info into the stream
      virtual ConfigExitCode configure(OptionHandle *opt) = 0;     //  configure this state and prepare the context for the next transistion

     protected:
//      virtual bool isConfigured() = 0;
//      bool isLatest() = 0;
//      bool isLatestState;
      AMBStatus * currentStatus;
      void streamStateInfo( std::ostream & stream, StateInfo* state);
     private:
      virtual void do_show( std::ostream &aStream ) = 0; 
    };

  }  // ftk namespace
}  // daq namespace


#endif  //  AMB_HARDWARE_STATUS_STATE_H


