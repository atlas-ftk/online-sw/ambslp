
#include "fsm_states.h"
#include "ambslp/status.h"

#include <ostream>
#include <iomanip>

using namespace daq::ftk;



void setupStateInfo( StateInfo *state ) {

    state->isLatestStatus = true;
    state->isConfigured = true;
    time_t now = time(0);
    state->latestConfiguration = *localtime(&now);
}






ConfigurationDone::ConfigurationDone( AMBStatus *current ) : AMB_state(current) //
{
//  done_data = get_ConfigurationDone( confStorage );
}


ConfigurationDone::~ConfigurationDone() {}


void ConfigurationDone::do_show( std::ostream & stream ) {

  stream << "\n\n\tSTATE: done.";
//  StateInfo *state = &(done_data->state);
//  streamHeader( stream, state );
}


ConfigExitCode ConfigurationDone::configure( OptionHandle *option ) {

  // always return SUCCESS code
  ConfigExitCode eCode = SUCCESS;
  return eCode;
}
//-----------------------------------------------
//




//-----------------------------------------------
// ConfigurationFailure


ConfigurationFailure::ConfigurationFailure( AMBStatus *current ) : AMB_state(current) //
{  
//  failure_data = get_ConfigurationFailure();
}


ConfigurationFailure::~ConfigurationFailure() { }

void ConfigurationFailure::do_show( std::ostream & stream ) {

  stream << "\n\n\tSTATE: failed.";
//  StateInfo *state = &(failure_data->state);
//  streamHeader( stream, state );

}

ConfigExitCode ConfigurationFailure::configure( OptionHandle *option ) {

  // failure state always return FAILURE
  ConfigExitCode eCode = FAILURE;
  return eCode;
}
//-----------------------------------------------



