
#include "storagePolicy.h"
#include "ambslp/status.h"
#include "sharedMemory_access.h"

using namespace daq::ftk;


AMBStatus * get_AMBStatus( InitConfiguration * init ) {

  int slot = init->slot;
  return getStatusFromSharedMemory( slot );
}


StoragePolicy::StoragePolicy() {}

StoragePolicy::~StoragePolicy() {}

AMBStatus * StoragePolicy::getConfiguration( int slot ) {

  return this->get_Configuration( slot );
}



SharedMemorySegment::SharedMemorySegment() : StoragePolicy() {}

SharedMemorySegment::~SharedMemorySegment() {}

AMBStatus * SharedMemorySegment::get_Configuration( int slot ) {

  AMBStatus *currentStatus = 0;
  currentStatus = getStatusFromSharedMemory( slot );

  return currentStatus;
}


