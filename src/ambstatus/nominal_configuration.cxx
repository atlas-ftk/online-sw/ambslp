
#include "ambslp/status.h"
#include "nominal_configuration.h"
#include "ambslp/configuration.h"
#include "state.h"
#include "storagePolicy.h"
#include "fsm_states.h"

#include <iostream>
#include <memory>

using namespace daq::ftk;


AMBConfiguration_nominal::AMBConfiguration_nominal( InitConfiguration * init ) : AMBConfiguration(init), //
                                                                                 latestState(0) //
{
  std::shared_ptr<StoragePolicy> storagePolicy;
  storagePolicy.reset( new SharedMemorySegment());
  currentStatus = storagePolicy->getConfiguration( init->slot );

  // create and fill states and transitions
  std::shared_ptr<AMB_state> begin; 
  begin.reset( new BeginState( currentStatus ));
  std::map<ConfigExitCode, Step> transBegin;
  transBegin[SUCCESS] = READ_REGISTERS_FROM_VME;
  transBegin[FAILURE] = READ_REGISTERS_FROM_VME;
  SingleTransition beginTransition( begin.get(), transBegin );

  std::shared_ptr<AMB_state> state ;
  state.reset(new VMEReader( currentStatus ));
  
  std::map<ConfigExitCode, Step> trans;
  trans[SUCCESS] = READ_REGISTERS_FROM_VME;
  trans[FAILURE] = READ_REGISTERS_FROM_VME;
  SingleTransition singleTransition (state.get(), trans);

  //---!
  transitions[READ_REGISTERS_FROM_VME] = singleTransition;
  latestState = transitions[READ_REGISTERS_FROM_VME].first;
  transitions[BEGIN] = beginTransition;
  latestState = transitions[BEGIN].first;

}


AMBConfiguration_nominal::~AMBConfiguration_nominal() {

// when fully working, delete here the states
}

void AMBConfiguration_nominal::show( std::ostream &stream ) {

  Transitions::iterator transition_it;
  for ( transition_it = transitions.begin(); transition_it!=transitions.end(); ++transition_it) {
    ((transition_it->second).first)->show( stream );
  }
}

void AMBConfiguration_nominal::restartFrom( ConfigurationStep * step ) {

  AMB_state * restarting_state = transitions[step->step].first;
  latestState = restarting_state;
  currentStatus->startFromHere = *step;
}


AMBStatus * AMBConfiguration_nominal::configure( OptionHandle *opt ) {

  Step step = (currentStatus->startFromHere).step;
  std::cout << "\n\t\t---> step: " << step << std::endl;

  ConfigExitCode code = (transitions[step].first)->configure( opt );
  std::map<ConfigExitCode, Step> trans = transitions[step].second;
  Step * nextStep = &(trans[code]);
  
  ConfigurationStep configurationStep;
  configurationStep.step = *nextStep;
  currentStatus->startFromHere = configurationStep;
  latestState = (transitions[configurationStep.step]).first;

  return currentStatus;
}


