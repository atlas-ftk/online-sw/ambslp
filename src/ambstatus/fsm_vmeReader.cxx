
#include "fsm_states.h"
#include "ambslp/status.h"
#include "ambslp/optionHandle.h"

#include "ambslp/vmeOperations.h"
#include "ambslp/exceptions.h"
#include "ftkvme/VMEInterface.h"

#include "ambslp/ambslp_vme_regs.h"

#include <ostream>
#include <iomanip>
#include <bitset>

using namespace daq::ftk;




//-----------------------------------------------
// VMEReader


VMEReader::VMEReader( AMBStatus * current ) : AMB_state(current) //
{

//  vme_data = get_VMEConfig();
}


VMEReader::~VMEReader() {}


void VMEReader::do_show( std::ostream & stream ) {

  stream << "\n\n\tSTATE: Reading registers from VME.";
  StateInfo *state = &(currentStatus->vmeRegisters.stateInfo);
  streamStateInfo( stream, state );

  printRegisters( stream );
}


void VMEReader::printRegisters( std::ostream &stream ) {

  std::bitset<20> connected_LAMBS_mask( (currentStatus->vmeRegisters).connected_LAMBS_mask );
  std::bitset<6> enabled_board_DCDC( (currentStatus->vmeRegisters).enabled_board_DCDC );

  stream << "\n" << std::setw(40) << " register    | ";
  stream << std::setw(90) << "   6         5         4         3         2         1          |       1          ";
//  stream << std::setw(20) << "1          ";
  stream << "\n" << std::setw(40+72) << "3  0   6   2   8   4   0   6   2   8   4   0   6   2   8   4   0|";
  stream << " 6   2   8   4   0\n\n";  


  stream << "\n" << std::setw(40) << "CONTROL_STATUS_REGISTER  | ";
  stream << std::setw(71) << connected_LAMBS_mask.to_string();
  stream << std::setw(19) << std::hex << connected_LAMBS_mask.to_ulong() << std::dec;
  stream << "\n" << std::setw(40) << "ENABLE_AMCORE_DCDC  | ";
  stream << std::setw(71) << enabled_board_DCDC.to_string();
  stream << std::setw(19) << std::hex << enabled_board_DCDC.to_ulong() << std::dec;

  stream << "\n\n";
}


ConfigExitCode VMEReader::configure( OptionHandle *option ) {

  Option *opt = option->option;
  Option_VMEReader *reader = (Option_VMEReader*)opt;
  int slot = reader->slot;


  StateInfo *state = &(currentStatus->vmeRegisters.stateInfo);
  state->isConfigured = false;
  state->isLatestStatus = true;

  ConfigExitCode eCode = FAILURE;
  // 1. connect to vme
  VMEInterface *vme = 0;
  try {
    vme = connectToVME_OrThrow( slot );
  }
  catch ( daq::ftk::Issue_ambslp &issue) {
    eCode = FAILURE;
    return eCode;
  }

  int connected_LAMBS_mask = 0;
  int enabled_board_DCDC = 0;
  try {
    connected_LAMBS_mask = readBoard_OrThrow( vme, CONTROL_STATUS_REGISTER );
    enabled_board_DCDC = readBoard_OrThrow( vme, ENABLE_AMCORE_DCDC );

  }
  catch ( daq::ftk::Issue_ambslp &issue ) {
    eCode = FAILURE;
    return eCode;
  }

  (currentStatus->vmeRegisters).connected_LAMBS_mask = connected_LAMBS_mask;
  (currentStatus->vmeRegisters).enabled_board_DCDC = enabled_board_DCDC;

  //   (currentStatus->VMERegisters).ROAD_GTP_links_mask = vme->read_word( ROAD_GTP_links_mask );

  state->isLatestStatus = true;
  state->isConfigured = true;
  time_t rawtime;
  time(&rawtime);
  state->latestConfiguration = *(localtime(&rawtime));

  eCode = SUCCESS;
  return eCode;
}

