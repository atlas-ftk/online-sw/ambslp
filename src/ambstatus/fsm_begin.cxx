
#include "fsm_states.h"
#include "ambslp/status.h"
#include "ambslp/optionHandle.h"

#include <ostream>


using namespace daq::ftk;

BeginState::BeginState( AMBStatus * current ) : AMB_state(current) //
{
}

BeginState::~BeginState() {}


void BeginState::do_show( std::ostream & stream) {

  stream << "\n\n\tSTATE: BEGIN state of the amb configuration.";
  StateInfo *state = &(currentStatus->board.stateInfo);
  streamStateInfo( stream, state);
  stream << "\n\t board slot: " << currentStatus->board.slot;
}


ConfigExitCode BeginState::configure( OptionHandle *option) {

  Option *opt = option->option;
  Option_BeginState *beginState =(Option_BeginState*)opt;
  int slot = beginState->slot;

  StateInfo *state = &(currentStatus->board.stateInfo);
  state->isConfigured = false;
  state->isLatestStatus = true;

  currentStatus->board.slot = slot;
  ConfigExitCode eCode = SUCCESS;
  state->isConfigured = true;
  state->isLatestStatus = true;

  time_t rawtime;
  time(&rawtime);
  state->latestConfiguration = *(localtime(&rawtime));

  return eCode;

}
