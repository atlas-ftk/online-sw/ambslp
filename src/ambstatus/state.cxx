
#include "state.h"
#include "ambslp/status.h"
#include "ambslp/optionHandle.h"

#include <iostream>
#include <iomanip>

using namespace daq::ftk;

AMB_state::AMB_state( AMBStatus * current ) : currentStatus(current) {}

AMB_state::~AMB_state() {}

void AMB_state::show( std::ostream &aStream ) {

  this->do_show( aStream );
}



void AMB_state::streamStateInfo( std::ostream & stream, StateInfo* state ) {

  stream << "\n\t\t is configured: " << std::boolalpha << state->isConfigured;
  stream << "\n\t\t is latest status reached: " << std::boolalpha << state->isLatestStatus;
  stream << "\n\t\t time latestConfiguration: " << asctime(&(state->latestConfiguration));
//  stream << std::setfill('-') << std::setw(20) << "\n" << std::setfill(' ');
}

