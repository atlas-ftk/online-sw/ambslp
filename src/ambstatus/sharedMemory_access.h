
#ifndef SHARED_MEMORY_ACCESS
#define SHARED_MEMORY_ACCESS

#include <sys/types.h>


namespace daq{
  namespace ftk{

  class AMBStatus;


  AMBStatus * getStatusFromSharedMemory( int slot );

  char * getRawMemory( int slot );
  key_t getKey( int slot );
  int getSharedMemory( key_t shm_key );
  void * attachSharedMemory( int shm_id );

  char * getSeedFile();
  bool fileAlreadyExists( const char * fullName );
  void touchNewFile( const char * fullName );

  }  // namespace ftk
}  // namespace daq


#endif  //  SHARED_MEMORY_ACCESS

