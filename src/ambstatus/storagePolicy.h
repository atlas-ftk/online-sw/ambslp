
#ifndef AMB_HARDWARE_STATUS_STORAGEPOLICY_H
#define AMB_HARDWARE_STATUS_STORAGEPOLICY_H


namespace daq {
  namespace ftk {

    class AMBStatus;
    class InitConfiguration;

    class StoragePolicy {
    
     public:
      StoragePolicy();
      virtual ~StoragePolicy();
      AMBStatus * getConfiguration( int slot );

     private:
      virtual AMBStatus * get_Configuration( int slot ) = 0;
    };


    class SharedMemorySegment : public StoragePolicy {
    
     public:
      SharedMemorySegment ();
      virtual ~SharedMemorySegment ();

     private:
      AMBStatus * get_Configuration( int slot );
    };


    AMBStatus * get_AMBStatus( InitConfiguration * init );


  }  // namespace ftk
}  // namespace daq

#endif  //  AMB_HARDWARE_STATUS_STORAGEPOLICY_H


