

#ifndef AMB_HARDWARE_IMPL_FSM_STATES_H
#define AMB_HARDWARE_IMPL_FSM_STATES_H


#include "state.h"

#include <iostream>


namespace daq {
  namespace ftk {

    struct  OptionHandle;



// configuration steps finished without any evident issue
//
    class ConfigurationDone : public AMB_state {

     public:
      ConfigurationDone(AMBStatus* current);
      virtual ~ConfigurationDone();
      ConfigExitCode configure (OptionHandle *opt = 0);

     private:
      void do_show( std::ostream & aStream );
//      struct ConfigDone* done_data;
    };
//---------------------------



// configuration procedure has failed somewhere
//
    class ConfigurationFailure : public AMB_state {

     public:
      ConfigurationFailure( AMBStatus *current );
      virtual ~ConfigurationFailure();
      ConfigExitCode configure (OptionHandle *opt = 0);

     private:
      void do_show( std::ostream & aStream );
//      struct ConfigFailure* failure_data;
    };
//---------------------------




// read VME registers used for the bootstrap
//
    class VMEReader : public AMB_state {

      public:
       VMEReader( AMBStatus * current );
       virtual ~VMEReader();
       ConfigExitCode configure(OptionHandle *opt);

      private:
       void do_show( std::ostream & aStream );
       void printRegisters( std::ostream & aStream );
//        struct VMEdata* vme_data;
    };
//---------------------------



// read VME registers used for the bootstrap
//
    class BeginState : public AMB_state {

      public:
       BeginState( AMBStatus * current );
       virtual ~BeginState();
       ConfigExitCode configure(OptionHandle *opt);

      private:
       void do_show( std::ostream & aStream );
    };
//---------------------------



  }  // ftk namespace
}  // daq namespace

#endif  // AMB_HARDWARE_IMPL_FSM_STATES_H

