

#include "sharedMemory_access.h"
#include "ambslp/status.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// dedicate 1024 Byte = 1KB of memory in RAM per SBC
#define SHM_SIZE 1024


using namespace daq::ftk;


AMBStatus * daq::ftk::getStatusFromSharedMemory( int slot ) {

  AMBStatus *status = 0;
  status = (AMBStatus*)getRawMemory( slot );

  return status;
}


char * daq::ftk::getRawMemory( int slot ) {

  key_t shm_key = getKey( slot );
  int shm_id = getSharedMemory( shm_key );
  void *rawMemory  = attachSharedMemory( shm_id );
  char * data = (char*)rawMemory;

  return data;
}




key_t daq::ftk::getKey( int initValue ) {

  char *seedFile = getSeedFile();
  key_t shm_key = ftok( seedFile, initValue );
  free(seedFile);

  if ( shm_key == -1) {
    perror("ftok");
    return -1;
  }
  return shm_key;

}


int daq::ftk::getSharedMemory( key_t shm_key ) {

  int shm_id = shmget ( shm_key, SHM_SIZE, 0644 | IPC_CREAT );
  if ( shm_id == -1 ) {
    perror("shmget");
    return -1;
  }
  return shm_id;
}


void * daq::ftk::attachSharedMemory( int shm_id ) {

  void *rawMemory  = (char *)(shmat (shm_id, (void *)0, 0));
  if ( rawMemory == (void *)(-1) ) {
    perror ("shmat");
  }
  return rawMemory;
}



char * daq::ftk::getSeedFile() {

  const char *path = getenv( "HOME" );
  const char * fileName = "/seedFile.ipc.ftk";
  char * fullName = (char *)malloc( 1024 );
  strcpy( fullName, path );
  strcat( fullName, fileName );

  bool fileExists = fileAlreadyExists( fullName );
  if ( !fileExists ) {
    touchNewFile( fullName );
  }

  return fullName;
}


bool daq::ftk::fileAlreadyExists( const char * fullName ) {

  struct stat buffer;
  return ( stat(fullName, &buffer) == 0);

}


void daq::ftk::touchNewFile( const char * fullName ) {

  FILE *file = fopen( fullName, "w" );
  if ( file == NULL ) {
    perror( "fopen" );
  }
}


