#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include "ambslp/AMBoard.h"
#include "ambslp/vmeOperations.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctime>
#include <utility>

using namespace std;

namespace daq {
  namespace ftk {

    /** The method dumps a vector of data, from a single input spy-buffer, as  a file
     * with a 2-column format, as originally used to feed AUX and suppoerted by
     * the online AMB emulation */
    int dump_SSfiles(std::string fname, vector<unsigned int> &data) {
      ofstream output_file(fname);

      for (const unsigned int word: data)
  	{
	  if( (word&0xf7000000)!=0xf7000000 ) {
	    output_file << "0 0x" << hex << setw(8) << setfill('0') << word << endl;
	  }
	  else {
	    // In the case of EE tag for the input file the MSB 2 hex digits should be 0
	    output_file << "1 0x" << hex << setw(8) << setfill('0') << (word&0xffffff) << endl;
	  }
  	}
      output_file.close();
      return 0;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * List of addresses where to look at in order to check spybuffers
     */

    const unsigned int InputSpyBufferStatus [] = { SPYSCTSTATUS0, SPYSCTSTATUS1,
						   SPYSCTSTATUS2, SPYSCTSTATUS3, SPYPIXSTATUS0, SPYPIXSTATUS1,
						   SPYPIXSTATUS2, SPYPIXSTATUS3, SPYPIXSTATUS4, SPYPIXSTATUS5,
						   SPYPIXSTATUS6, SPYPIXSTATUS7 };

    const unsigned int OutputSpyBufferStatus [] = {
      SPYROADL0STATUS0, SPYROADL0STATUS1, SPYROADL0STATUS2, SPYROADL0STATUS3,
      SPYROADL1STATUS0, SPYROADL1STATUS1, SPYROADL1STATUS2, SPYROADL1STATUS3,
      SPYROADL2STATUS0, SPYROADL2STATUS1, SPYROADL2STATUS2, SPYROADL2STATUS3,
      SPYROADL3STATUS0, SPYROADL3STATUS1, SPYROADL3STATUS2, SPYROADL3STATUS3 };
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * This function finds first and last element in the L1ID vector among lanes, in case the input data is fed by loop mode.
     */
    
    int find_first_last_ee_looped(std::vector< std::vector <unsigned int > > vec_L1IDs, std::vector< std::vector <unsigned int > > vec_ptrL1IDs, std::vector<unsigned int> &ptr_first_ee, std::vector<unsigned int> &ptr_last_ee){

      // if(ptr_first_ee==nullptr || ptr_last_ee==nullptr || common_first_ee==nullptr || common_last_ee==nullptr || nEvents==nullptr){
      // 	std::cout << "" << std::endl;
      // 	return -1;
      // }

      std::cout << "processing looped data." << std::endl;

      unsigned int nEvents = 0;
      unsigned int nLanes  = 28;
      unsigned int in_dataBits  = 0x00ffffff;
      unsigned int in_eeBits    = 0xf7000000;
      unsigned int out_dataBits = 0x000fffff;
      unsigned int out_eeBits   = 0xf7800000;

      ptr_first_ee.resize(nLanes);
      ptr_last_ee.resize(nLanes);

      std::vector<std::vector<unsigned int>::iterator> itr_first_ee (nLanes);
      std::vector<std::vector<unsigned int>::iterator> itr_last_ee (nLanes);
      std::vector<std::vector<unsigned int>::iterator> itr_min_ee (nLanes);
      std::vector<std::vector<unsigned int>::iterator> itr_max_ee (nLanes);
      std::vector<unsigned int> last_ee (nLanes);
      std::vector<unsigned int> nEvents_lane (nLanes);
      std::vector<unsigned int>::iterator itr_common_last_ee;
      std::vector<unsigned int>::iterator itr_nEvents;
      std::vector<unsigned int>::iterator itr_min_first_ee;
      std::vector<unsigned int>::iterator itr_max_first_ee;
      std::vector<unsigned int>::iterator itr_min_last_ee;
      std::vector<unsigned int>::iterator itr_max_last_ee;
      std::vector<unsigned int>::iterator itr_each_common_last_ee;
      unsigned int common_last_ee;
      unsigned int tmp_common_last_ee;
      unsigned int min_last_ee;
      unsigned int max_last_ee;
      unsigned int distance_last_ee;

      bool isLoopBoundary = false;

      // find last ee, and will take only last loop.
      for(unsigned int ispy=0; ispy<nLanes; ispy++){

      	itr_last_ee[ispy] = vec_L1IDs.at(ispy).end()-1;
	if(ispy < 12)
	  last_ee[ispy] = (*itr_last_ee[ispy])&in_dataBits;
	else
	  last_ee[ispy] = (*itr_last_ee[ispy])&out_dataBits;

      	itr_min_ee[ispy] = std::min_element(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end());
      	itr_max_ee[ispy] = std::max_element(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end());
	nEvents_lane[ispy]  = (*itr_max_ee[ispy]) - (*itr_min_ee[ispy]) + 1;
      }  
      
      // define common range
      itr_nEvents  = std::max_element(nEvents_lane.begin(), nEvents_lane.end());
      nEvents = *itr_nEvents;// would be # events in a loop

      // search the minimum last among lanes.
      itr_min_last_ee = std::min_element(last_ee.begin(), last_ee.end());
      itr_max_last_ee = std::max_element(last_ee.begin(), last_ee.end());
      min_last_ee = *itr_min_last_ee;
      max_last_ee = *itr_max_last_ee;

      isLoopBoundary = min_last_ee < nEvents * 0.15 && nEvents * 0.85 < max_last_ee;

      // L1IDs are loop boundary
      if(isLoopBoundary){
	tmp_common_last_ee = nEvents;
	for(unsigned int ispy=0; ispy<nLanes; ispy++){
	  if(nEvents * 0.85 < last_ee[ispy] && last_ee[ispy] < tmp_common_last_ee){
	    tmp_common_last_ee = last_ee[ispy];
	  }
	}
	common_last_ee = tmp_common_last_ee;
      }else{
	itr_common_last_ee = std::min_element(last_ee.begin(), last_ee.end());
	common_last_ee = *itr_common_last_ee;
      }

      for(unsigned int ispy=0; ispy<nLanes; ispy++){
	// the lane contain less than 1 loop.
	if(vec_L1IDs.at(ispy).size() < nEvents){
	  if(ispy < 12)
	    itr_each_common_last_ee = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_last_ee+in_eeBits);
	  else
	    itr_each_common_last_ee = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_last_ee+out_eeBits);
	}else{
	  if(ispy < 12)
	    itr_each_common_last_ee = find(vec_L1IDs.at(ispy).end() - nEvents, vec_L1IDs.at(ispy).end(), common_last_ee+in_eeBits);
	  else
	    itr_each_common_last_ee = find(vec_L1IDs.at(ispy).end() - nEvents, vec_L1IDs.at(ispy).end(), common_last_ee+out_eeBits);
	}

	distance_last_ee = std::distance(vec_L1IDs.at(ispy).begin(), itr_each_common_last_ee);
	ptr_last_ee[ispy] = vec_ptrL1IDs.at(ispy).at(distance_last_ee);

	if(distance_last_ee <= nEvents){
	  std::stringstream message;
	  message << "#All events are less than #events/loop in lane " << ispy << ". #All Events: " << distance_last_ee << ", #events/loop: " << nEvents << "  method4 Exiting!!";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::error(ex);

	  return -1;
	}else{
	  ptr_first_ee[ispy] = vec_ptrL1IDs.at(ispy).at(distance_last_ee - nEvents);
	}
      }

      return 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * This function finds first and last element in the L1ID vector among lanes, in case the input data has continuous ee.
     */
    
    int find_first_last_ee_continuous(bool is_overflow, std::vector< std::vector <unsigned int > > vec_L1IDs, std::vector< std::vector <unsigned int > > vec_ptrL1IDs, std::vector<unsigned int> &ptr_first_ee, std::vector<unsigned int> &ptr_last_ee){

      // if(ptr_first_ee==nullptr || ptr_last_ee==nullptr || common_first_ee==nullptr || common_last_ee==nullptr || nEvents==nullptr){
      // 	std::cout << "" << std::endl;
      // 	return -1;
      // }

      std::cout << "processing continuous data." << std::endl;

      unsigned int nLanes  = 28;
      unsigned int in_dataBits  = 0x00ffffff;
      unsigned int in_eeBits    = 0xf7000000;
      unsigned int out_dataBits = 0x000fffff;
      unsigned int out_eeBits   = 0xf7800000;

      ptr_first_ee.resize(nLanes);
      ptr_last_ee.resize(nLanes);

      std::vector<std::vector<unsigned int>::iterator> itr_first_ee (nLanes);
      std::vector<std::vector<unsigned int>::iterator> itr_last_ee (nLanes);
      std::vector<unsigned int> first_ee (nLanes);
      std::vector<unsigned int> last_ee (nLanes);
      std::vector<unsigned int>::iterator itr_common_first_ee;
      std::vector<unsigned int>::iterator itr_common_last_ee;
      std::vector<unsigned int>::iterator itr_each_common_first_ee;
      std::vector<unsigned int>::iterator itr_each_common_last_ee;
      unsigned int common_first_ee;
      unsigned int common_last_ee;
      unsigned int distance_first_ee;
      unsigned int distance_last_ee;

      // find max and min ee
      for(unsigned int ispy=0; ispy<nLanes; ispy++){
      	itr_first_ee[ispy] = vec_L1IDs.at(ispy).begin();
	first_ee[ispy] = *itr_first_ee[ispy];
      	itr_last_ee[ispy] = vec_L1IDs.at(ispy).end()-1;
	last_ee[ispy] = *itr_last_ee[ispy];

	if(ispy < 12){
	  first_ee[ispy] = (first_ee[ispy])&in_dataBits;
	  last_ee[ispy] = (last_ee[ispy])&in_dataBits;
	}else{
	  first_ee[ispy] = (first_ee[ispy])&out_dataBits;
	  last_ee[ispy] = (last_ee[ispy])&out_dataBits;
	}

      }

      itr_common_first_ee = std::max_element(first_ee.begin(), first_ee.end());
      itr_common_last_ee  = std::min_element(last_ee.begin(),  last_ee.end());
      common_first_ee = *(itr_common_first_ee);
      common_last_ee  = *(itr_common_last_ee);

      for(unsigned int ispy=0; ispy<nLanes; ispy++){
	
	if(ispy < 12){
	  itr_each_common_first_ee = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_first_ee+in_eeBits);
	  itr_each_common_last_ee  = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_last_ee+in_eeBits);
	}else{
	  itr_each_common_first_ee = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_first_ee+out_eeBits);
	  itr_each_common_last_ee  = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_last_ee+out_eeBits);
	}

	if(!is_overflow){
	  ptr_first_ee[ispy] = 0;
	}else{
	  distance_first_ee = std::distance(vec_L1IDs.at(ispy).begin(), itr_each_common_first_ee);
	  ptr_first_ee[ispy] = vec_ptrL1IDs.at(ispy).at(distance_first_ee);
	}

	distance_last_ee = std::distance(vec_L1IDs.at(ispy).begin(), itr_each_common_last_ee);
	ptr_last_ee[ispy] = vec_ptrL1IDs.at(ispy).at(distance_last_ee);

      }

      return 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * This function finds first and last element in the L1ID vector among lanes, in case the input data includes ECR.
     */
    
    int find_first_last_ee_ECR(bool is_overflow, std::vector< std::vector <unsigned int > > vec_L1IDs, std::vector< std::vector <unsigned int > > vec_ptrL1IDs, std::vector<unsigned int> &ptr_first_ee, std::vector<unsigned int> &ptr_last_ee){

      // if(ptr_first_ee==nullptr || ptr_last_ee==nullptr || common_first_ee==nullptr || common_last_ee==nullptr || nEvents==nullptr){
      // 	std::cout << "" << std::endl;
      // 	return -1;
      // }

      std::cout << "processing ECR including data." << std::endl;

      unsigned int nLanes  = 28;
      unsigned int in_dataBits  = 0x00ffffff;
      unsigned int in_eeBits    = 0xf7000000;
      unsigned int out_dataBits = 0x000fffff;
      unsigned int out_eeBits   = 0xf7800000;

      ptr_first_ee.resize(nLanes);
      ptr_last_ee.resize(nLanes);

      std::vector<std::vector<unsigned int>::iterator> itr_first_ee (nLanes);
      std::vector<std::vector<unsigned int>::iterator> itr_last_ee (nLanes);
      std::vector<std::vector<unsigned int>::iterator> itr_max_ee (nLanes);
      std::vector<unsigned int> first_ee (nLanes);
      std::vector<unsigned int> last_ee (nLanes);
      std::vector<unsigned int> max_ee (nLanes);
      std::vector<unsigned int>::iterator itr_common_max_ee;
      std::vector<unsigned int>::iterator itr_each_common_first_ee;
      std::vector<unsigned int>::iterator itr_each_common_last_ee;
      std::vector<unsigned int>::iterator itr_min_first_ee;
      std::vector<unsigned int>::iterator itr_max_first_ee;
      std::vector<unsigned int>::iterator itr_min_last_ee;
      std::vector<unsigned int>::iterator itr_max_last_ee;
      unsigned int tmp_common_first_ee;
      unsigned int tmp_common_last_ee;
      unsigned int min_first_ee;
      unsigned int max_first_ee;
      unsigned int min_last_ee;
      unsigned int max_last_ee;
      unsigned int common_first_ee;
      unsigned int common_last_ee;
      unsigned int common_max_ee;
      unsigned int distance_first_ee;
      unsigned int distance_last_ee;

      bool first_isECR  = false;
      bool last_isECR   = false;
      bool global_isECR = false;

      // find max and min ee
      for(unsigned int ispy=0; ispy<nLanes; ispy++){
      	itr_first_ee[ispy] = vec_L1IDs.at(ispy).begin();
	first_ee[ispy] = *itr_first_ee[ispy];
      	itr_last_ee[ispy] = vec_L1IDs.at(ispy).end()-1;
	last_ee[ispy] = *itr_last_ee[ispy];
	itr_max_ee[ispy] = std::max_element(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end());
	max_ee[ispy] = *itr_max_ee[ispy];

	if(ispy < 12){
	  first_ee[ispy] = (first_ee[ispy])&in_dataBits;
	  last_ee[ispy] = (last_ee[ispy])&in_dataBits;
	  max_ee[ispy] = (max_ee[ispy])&in_dataBits;
	}else{
	  first_ee[ispy] = (first_ee[ispy])&out_dataBits;
	  last_ee[ispy] = (last_ee[ispy])&out_dataBits;
	  max_ee[ispy] = (max_ee[ispy])&out_dataBits;
	}

      }

      // search the min/max first/last among lanes.
      itr_min_first_ee = std::min_element(first_ee.begin(), first_ee.end());
      itr_max_first_ee = std::max_element(first_ee.begin(), first_ee.end());
      itr_min_last_ee  = std::min_element(last_ee.begin(), last_ee.end());
      itr_max_last_ee  = std::max_element(last_ee.begin(), last_ee.end());
      min_first_ee = *itr_min_first_ee;
      max_first_ee = *itr_max_first_ee;
      min_last_ee  = *itr_min_last_ee;
      max_last_ee  = *itr_max_last_ee;

      // max ee among all spybuffer
      itr_common_max_ee  = std::max_element(max_ee.begin(), max_ee.end());
      common_max_ee = *itr_common_max_ee;

      first_isECR  = min_first_ee < common_max_ee * 0.15 && common_max_ee * 0.85 < max_first_ee;
      last_isECR   = min_last_ee  < common_max_ee * 0.15 && common_max_ee * 0.85 < max_last_ee;
      if(!first_isECR && !last_isECR)
	global_isECR = min_first_ee < common_max_ee * 0.15 && common_max_ee * 0.85 < max_last_ee;

      if(first_isECR && last_isECR){
	  std::stringstream message;
	  message << "Both first ee and last ee include ECR, cannot take common event range!! method4 Exiting!!";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::error(ex);
	return -1;
      }

      // Treatment for ECR boundary
      if(first_isECR){
	tmp_common_first_ee = 0;
	for(unsigned int ispy=0; ispy<nLanes; ispy++){
	  if(first_ee[ispy] < common_max_ee * 0.15 && first_ee[ispy] > tmp_common_first_ee){
	    tmp_common_first_ee = first_ee[ispy];
	  }
	}
	common_first_ee = tmp_common_first_ee;
	common_last_ee  = min_last_ee;
      }else if(last_isECR){
	tmp_common_last_ee = common_max_ee;
	for(unsigned int ispy=0; ispy<nLanes; ispy++){
	  if(common_max_ee * 0.85 < last_ee[ispy] && last_ee[ispy] < tmp_common_last_ee){
	    tmp_common_last_ee = last_ee[ispy];
	  }
	}
	common_last_ee  = tmp_common_last_ee;
	common_first_ee = max_first_ee;
      }else if(global_isECR){
	common_first_ee = max_first_ee;
	common_last_ee  = min_last_ee;
      }else{
	std::stringstream message;
	message << "ECR. cannot handle.. something wrong, maybe so many L1IDs and cannot identify which is start. method4 Exiting!!";
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ers::error(ex);
	return -1;
      }

      for(unsigned int ispy=0; ispy<nLanes; ispy++){
	
	if(ispy < 12){
	  itr_each_common_first_ee = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_first_ee+in_eeBits);
	  itr_each_common_last_ee  = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_last_ee+in_eeBits);
	}else{
	  itr_each_common_first_ee = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_first_ee+out_eeBits);
	  itr_each_common_last_ee  = find(vec_L1IDs.at(ispy).begin(), vec_L1IDs.at(ispy).end(), common_last_ee+out_eeBits);
	}

	if(!is_overflow){
	  ptr_first_ee[ispy] = 0;
	}else{
	  if(itr_each_common_first_ee != vec_L1IDs.at(ispy).end() && itr_each_common_last_ee != vec_L1IDs.at(ispy).end()){
	    distance_first_ee = std::distance(vec_L1IDs.at(ispy).begin(), itr_each_common_first_ee);
	    ptr_first_ee[ispy] = vec_ptrL1IDs.at(ispy).at(distance_first_ee);
	  }else{
	    std::stringstream message;
	    message << "ECR. cannot handle.. something wrong, maybe so many L1IDs and mismatch in lane. method4 Exiting!!";
	    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	    ers::error(ex);
	    return -1;
	  }
	}

	distance_last_ee = std::distance(vec_L1IDs.at(ispy).begin(), itr_each_common_last_ee);
	ptr_last_ee[ispy] = vec_ptrL1IDs.at(ispy).at(distance_last_ee);

      }

      return 0;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** Method used to save the data in the input spy=buffers in the format used by ambslp_feedhit
     * to use the current content for a future test.
     */
    int dump_SShitfile(std::string fname, std::vector< std::vector <unsigned int > >  &data) {
      // open the output file
      ofstream outfile(fname);

      // iterators for the check the position and end of each bus
      vector<unsigned int>::const_iterator busPos[12];
      vector<unsigned int>::const_iterator busPosE[12];
      for (unsigned int ibus=0; ibus!=12; ++ibus) {
	busPos[ibus] = data[ibus].begin();
	busPosE[ibus] = data[ibus].end();
      }

      // in the mask each bit represents a bus, when the bit is 1 the data for the event in the bus are finished
      unsigned int EEmask(0);
      // similar to the above mask but store the condition of having reached the end of the data stream
      unsigned int EOSmask(0);

      while (EOSmask!=0xfff) { // loop on the data, until all streams are finished
	unsigned long long int lineData[12]; // data words to be written in this line
	for (unsigned int ibus=0; ibus!=12; ++ibus) { // loop over the streams to collect data
	  if (busPos[ibus]==busPosE[ibus]) { // check the EOS condition
	    EOSmask |= 1<<ibus; // set the EOS condition for the bus, can set multiple times
	    continue; // mark the stream as finished and move, this should happens at the same time in all layers
	  }
	  else {

	    // if the stream is not finished write something: data word or idle for synchronization
	    const unsigned int &dword = *busPos[ibus]; // current data word

	    if ((dword&0xff000000)!=0xf7000000) {
	      // normal data word, this has to be written and the position updated
	      lineData[ibus] = dword;
	      busPos[ibus]++;
	    }
	    else { // end event case
	      // mark the event as reached, set the bit, remain in the position
	      EEmask |= 1<<ibus; // if already set is not an issue
	      lineData[ibus] = 0x400000000; // a dummy word for synchronization will be written
	    }
	  }

	} // endl loop over the streams to collect data

	// check if the EE bit is up for all the link, close it and allow to move
	if (EEmask==0xfff) {
	  for (unsigned int ibus=0; ibus!=12; ++ibus) { // loop over the streams
	    // write the EE, no check done, and increment the current bus position
	    outfile << '8' << hex << setw(8) << setfill('0') << (*busPos[ibus]) << ' ';
	    busPos[ibus]++;
	  } // end loop over the streams

	  EEmask = 0; // reset the EE mask
	}
	else {
	  // for not EE lines the collected data are written
	  for (unsigned int ibus=0; ibus!=12; ++ibus) { // loop over the streams
	    // write the EE, no check done, and increment the current bus position
	    outfile << hex << setw(9) << setfill('0') << lineData[ibus] << ' ';
	  } // end loop over the streams
	}

	// end the line
	outfile << endl;

      } // end loop over the data

      outfile.close();

      return 0;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  } //ftk
} //daq


// ftkcommon
#include <ftkcommon/SpyBuffer.h>
#include "ftkcommon/SourceIDSpyBuffer.h"
#include <ftkcommon/Utils.h>

// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

// emon
#include <emon/EventIterator.h>
#include <eformat/ROBFragmentNoTemplates.h>
#include <eformat/FullEventFragmentNoTemplates.h>
#include <eformat/Issue.h>

// ipc
#include <ipc/partition.h>
#include <ipc/core.h>


/**
 * This function set EMON partition and push back data vector
 */

void fillSpyBuffer(std::vector< std::vector <unsigned int > >& vec, bool isIn, daq::ftk::AMBoard* myAMB, std::string part_name) {
  
  if(part_name.empty()) { // local mode
    // wait for Francesco's implementation
  } else { // emon mode
    
    //create a partition to run in (= same as the RC partition)
    IPCPartition partition(part_name);
    
    emon::SelectionCriteria emonCriteria( emon::L1TriggerType(0, true), emon::SmartBitValue(), emon::SmartStreamValue(), emon::StatusWord() );
    emon::SamplingAddress emonAddress( "ReadoutApplication", "FTK-RCD-VmeCrate-2");
    
    std::unique_ptr<emon::EventIterator> eit;
    try {
      eit.reset( new emon::EventIterator( partition, emonAddress, emonCriteria ));
    } catch (emon::Exception & ex) {
      ers::error( ex );
      return;
    }
    
    uint32_t timeout = 10;  // emon timeout in sec
    // read out the next event from emon (wait for the event to appear for timeout seconds)
    emon::Event event;
    try {
      printf("Waiting %i seconds for the events on EMON to appear \n",timeout);
      event = eit -> nextEvent( timeout*1000 ); 
    } catch ( emon::NoMoreEvents & ex ) {
      ers::warning( ex );
    } catch ( emon::Exception & ex ) {
      ers::error( ex );
    }
    
    const unsigned int * data;
    uint32_t ndata=0;
    
    eformat::read::FullEventFragment fe( event.data() );
    try {
      fe.check();
    } catch (eformat::Issue ex) {
      throw ex;
    }

    std::vector< eformat::read::ROBFragment > ROBFragments_v;
    fe.robs( ROBFragments_v );
    for(auto& ROBFragment : ROBFragments_v){
      
      daq::ftk::SourceIDSpyBuffer SBSource_id = daq::ftk::decode_SourceIDSpyBuffer( ROBFragment.source_id() );
      if(SBSource_id.boardType != (uint32_t)daq::ftk::BoardType::AMB ) continue;
      if(SBSource_id.position != (isIn ? (uint32_t)daq::ftk::Position::IN : (uint32_t)daq::ftk::Position::OUT) ) continue;
      if(SBSource_id.boardNumber != (uint32_t)myAMB->getSlot() ) continue;

      data = ROBFragment.rod_data();
      ndata = ROBFragment.rod_ndata();
      
      std::vector<unsigned int> vec_data;
      for(uint32_t i=0; i<ndata; i++) vec_data.push_back(data[i]);

      vec.push_back(vec_data);
    }
  }
}

/******************************/
int main(int argc, char *argv[])
/******************************/
{
  using namespace  boost::program_options ;

  /**
   * Options available
   *  1. --slot (Default 15)
   *  2. --method (For output dumping: 0, 1, 2, 4 are available)
   *  3. --inp (dump input spybuffer for method 0, 1, 2)
   *  4. --out (dump output spybuffer for method 0, 1, 2)
   *  5. --both (dump both input and output spybuffer for method 0, 1, 2)
   *  6. --keepFreeze (Set it if you want to keep freezing buffers after readout to keep situation)
   *  7. --tag (Set it if you want to customize your stored files; default is time and date)
   *
   * Usage:
   * ambslp_spybuffer_dumper_main --slot SLOT --tag WHATEVER_YOU_WANT --flag1 --flag2 and so on...
   */

  time_t rawtime;
  struct tm *timeinfo;
  char buffer[80];
  time (&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer,80,"%d-%m-%Y_%I-%M-%S",timeinfo);
  std::string datetime(buffer);

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("method", value<int>()->default_value(0), "The dumping method, takes both inspy and outspy with same freeze")
    ("inp", "Input spybuffers dump for method 0, 1, 2")
    ("out", "Output spybuffers dump for method 0, 1, 2")
    ("both","Dump both Input and Output spybuffer with same freeze for method 0, 1, 2")
    ("keepFreeze", value<int>()->default_value(0), "keep freezing buffers after readout to keep situation")
    ("dump_meth4out", value<int>()->default_value(0), "dump output spybuffer for method 4 for comparison in AMBTest.py. (Is there better way?)")
    ("tag", value<string>()->default_value(datetime), "String to be appended to generated files; default is current time and date")
    ("partition,p", value< std::string >()->default_value(""), "Partition name. If set, will try to read info from IS.")
    ("verbose,v",                                              "If present verbose option will be used")
    ;

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl; 
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {      
      std::cout << std::endl <<  desc << std::endl ; 
      return 0;      
    } 
    
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int method = vm["method"].as<int>();
  std::string label("none");
  label = vm["tag"].as<std::string>();
  std::string part_name = vm["partition"].as<std::string>();
  std::string filename;
      
  const int keepFreeze = vm["keepFreeze"].as<int>();
  const int dump_meth4 = vm["dump_meth4out"].as<int>();

  if(!part_name.empty()){
    //initialize communication library (= ipc)
    IPCCore::init(argc,argv);
  }

  /**
   * If the option --method is set to 0, 1, 2, 3, output is dumpped and file is created, by the legacy ambslp_inp/out_spy.
   * 0: dump raw output from spybuffer.
   * 1: merge raw output among the lanes.
   * 2: merge and modify output among the lanes.
   * 3: ???
   *
   **/
      
  if(method == 0 || method == 1 || method == 2 || method == 3){ 

    // bool both = false;
    // if(!vm.count("inp") && !vm.count("out") && !vm.count("both")){
    //   both = true;
    //   // std::cerr << "  Set \"--inp\" or \"--out\" or \"--both\" for method 0, 1, 2 !!" << std::endl; 
    //   // return 1;
    // }

    // if (vm.count("both") || both)
    //   daq::ftk::ambslp_inp_spy(slot, method, 1);
    // if(vm.count("inp"))
    //   daq::ftk::ambslp_inp_spy(slot, method, keepFreeze);
    // if(vm.count("out") || vm.count("both") || both)
    //   daq::ftk::ambslp_out_spy(slot, method, keepFreeze);


    bool verbose   = vm.count("verbose")>0 ? true : false;

    bool _s_both   = false;
    bool _s_input  = false;
    bool _s_output = false;

    if ( vm.count("inp") )  _s_input  = true;
    if ( vm.count("out") )  _s_output = true;

    if ( vm.count("both") || ( !_s_input && !_s_output ) )  _s_both = true;



    daq::ftk::AMBoard _AM_board(slot, verbose? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::WARNING);

    _AM_board.outputSpyBuffers(method, keepFreeze);

    if ( _s_both ) {

      _AM_board.inputSpyBuffers( method, 1);
      _AM_board.outputSpyBuffers(method, keepFreeze);

    } else if (_s_input) {

      _AM_board.inputSpyBuffers( method, keepFreeze);

    } else if (_s_output) {

      _AM_board.outputSpyBuffers(method, keepFreeze);
    }


  } else if (method == 4) {

    /**
     * If the option --method is set to 4, inspy and outspy data are taken and the format is modified to be fit to do comparison.
     * i.e. Inspy data format is modified to be compatible with the input of ambslp_boardsim,
     *      and outspy data format is modified to be comapatible with the ambslp_boardsim output.
     *
     **/
      
    /**
     * Preparing stuff for reading spybuffers data without VME direct access: 
     *  1. Selecting slot and calling VME interface
     *  2. Freezing and checking if the freeze word was read and written properly
     *  3. Reading Input and Output data
     */

    daq::ftk::AMBoard myAMB(slot);
    myAMB.freezeSpyBuffers();     ///Freeze & check
    myAMB.readInputSpyBuffers();  ///Read In
    myAMB.readOutputSpyBuffers(); ///Read Out             
   
    std::cout << "Reading input data..." <<std::endl;
    // std::vector< std::shared_ptr< daq::ftk::vme_spyBuffer > > sbi;
    // std::vector< std::shared_ptr< daq::ftk::vme_spyBuffer > > sbo;
    std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > > sbi;
    std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > > sbo;
    std::vector< std::vector <unsigned int > >  in_spy;
    std::vector< std::vector <unsigned int > >  in_spy_rsz;
    std::vector< std::vector <unsigned int > >  synch_in;
    std::vector< std::vector <unsigned int > >  end_events;
    std::vector< std::vector <unsigned int > >  full_data;
    std::vector< std::vector <unsigned int > >  L1IDs;
    std::vector< std::vector <unsigned int > >  ptr_L1IDs;
    std::vector< unsigned int> in_ptrs (12);
    std::vector< bool> in_sts (12);
    std::vector< unsigned int> idiff (12);
    int dimension_spy=13;
    unsigned int range=(1<<dimension_spy);
    int isb=0, osb=0;
    unsigned int prev_ee = -1;
    bool overflow_in=false;
    bool in_looped = false;
    bool in_ECR = false;

    /**
     * For each input spybuffers, 3 main vectors are created in order to prevent VME direct access
     *  1. in_spy, i.e. data, in std::vector< std::vector <unsigned int > > form
     *  2. Statuses in std::vector< bool> form: if true, spybuffers are in overflow, otherwise not
     *  3. Pointers in std::vector< unsigned int> form: this vector will be used when dumping spybuffers with traditional output (method-0)
     *
     * The full_data vector is also filled and it'll be used afterwards to find common end events; dimension of the inner std::vector is 28, whose first 12 position are input spybuffers, while the remaining ones (16) are output
     * An end_events vector is set to be filled in the next steps (It will contain all end-event tags for input and output spybuffers, whose structure will be the same as the one used for full_data)
     *
     * In case they're not in overflow an additional vector of differences will be created: it will contain the difference between max spybuffer size (8192) and the current one
     */

    for(int ispy=0; ispy<12; ispy++){
      if(myAMB.getInputSpyBufferSize() >= ispy) sbi.push_back(myAMB.getInputSpyBuffer(ispy));
      else {			
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), "The input spybuffer has not been read...");
	ers::error(ex);
      }
      L1IDs.push_back(std::vector<unsigned int>());
      ptr_L1IDs.push_back(std::vector<unsigned int>());
      end_events.push_back(std::vector<unsigned int>());
      full_data.push_back(sbi[ispy]->getBuffer());
      in_spy.push_back(sbi[ispy]->getBuffer());
      if(in_spy.at(ispy).size() == 0){
	if(ispy < 4) {
	  std::stringstream message;
	  message << "SCT" << ispy << " Spybuffer empty";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::error(ex);
	}else{
	  std::stringstream message;
	  message << "PIX" << ispy-4 << " Spybuffer empty";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::error(ex);
	}

	isb++;
      }
      in_ptrs[ispy] = sbi[ispy]->getSpyPointer();
      in_sts[ispy] = sbi[ispy]->getSpyOverflow();
      //       int dim = sbi[ispy]->getSpyDimension(); ---> Method getSpyDimension() doesn't work at the moment: it should return 13, i.e. the number of bits used for each spybuffer, but the result is 0

      if(in_sts[ispy]) overflow_in=true;   
      else idiff[ispy] = range - in_spy.at(ispy).size();

      /**
       * While filling end_events vector with those values, an additional check is performed to ensure they're consecutive, otherwise it'll print out the ones whose difference isn't 1, as well as the spybuffer index
       */

      std::pair<unsigned int, unsigned int> init_ee_comp (0,0);
      std::vector<unsigned int>::iterator iter_loopCheck;
      prev_ee = -1;
      for(unsigned int iptr=0; iptr<in_spy.at(ispy).size(); iptr++){
	if(((in_spy.at(ispy).at(iptr)>>24)&0xff)==0xf7){

	  // search duplicated L1ID to identify this is looped TV or not.
	  iter_loopCheck = find(L1IDs.at(ispy).begin(), L1IDs.at(ispy).end(), in_spy.at(ispy).at(iptr));
	  if(iter_loopCheck != L1IDs.at(ispy).end())
	    in_looped = true;

	  if(prev_ee == in_spy.at(ispy).at(iptr)){
	    // std::cerr << "  2 consecutive same ee in inspy lane " << ispy << "!" << std::endl;
	    // std::cerr << "  iptr: " << iptr << ", prev_ee: 0x" << std::hex << prev_ee << std::dec << std::endl;
	    // std::cerr << "  method4 Exiting!!" << std::endl;
	    std::stringstream message;
	    message << "2 consecutive same ee in inspy lane " << ispy << "!  iptr: " << iptr << ", prev_ee: 0x" << std::hex << prev_ee << std::dec << "  method4 Exiting!!";
	    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	    ers::error(ex);

	    if(!keepFreeze)
	      myAMB.unfreezeSpyBuffers();
	    return -1;
	  }

	  L1IDs.at(ispy).push_back(in_spy.at(ispy).at(iptr));
	  ptr_L1IDs.at(ispy).push_back(iptr);

	  prev_ee = in_spy.at(ispy).at(iptr);

	  // if(init_ee_comp.first==0) {
	  //   init_ee_comp = std::make_pair(in_spy.at(ispy).at(iptr), 0);
	  //   end_events.at(ispy).push_back(in_spy.at(ispy).at(iptr));
	  //   continue;
	  // }else if(init_ee_comp.first!=0){
	  //   end_events.at(ispy).push_back(in_spy.at(ispy).at(iptr));
	  //   init_ee_comp.second = in_spy.at(ispy).at(iptr);
	  //   unsigned int diff = init_ee_comp.second - init_ee_comp.first;
	  //   if(diff == 1)
	  //     init_ee_comp.first = in_spy.at(ispy).at(iptr);
	  //   else {
	  //     // std::cerr << "Problem in Input SpyBuffer " << ispy << " detected --- First EE: " << std::hex << init_ee_comp.first << " Second EE: " << init_ee_comp.second << " DIFFERENCE IS NOT 1" << std::endl;
	  //     init_ee_comp.first = in_spy.at(ispy).at(iptr);
	  //     // break;
	  //   }
	  // }
	  // else continue;
	}
      }
    }

    /**
     * For each output spybuffers, 3 main vectors are created
     *  1. Data in std::vector< std::vector <unsigned int > > form
     *  2. Statuses in std::vector< bool> form: if true, spybuffers are in overflow, otherwise not
     *  3. Pointers in std::vector< unsigned int> form: this vector will be used when dumping spybuffers with traditional output (method-0)
     *
     * In case they're not in overflow an additional vector of differences will be created: it will contain the difference between max spybuffer size (8192 or 1fff) and the current one
     */

    std::cout << "Reading output data..." <<std::endl;
    std::vector< std::vector <unsigned int > >  out_spy;
    std::vector< unsigned int> out_ptrs (16);
    std::vector< bool> out_sts (16); 
    std::vector< unsigned int> odiff (16);
    bool overflow_out=false;
    bool out_looped = false;
    bool out_ECR = false;

    for(int ispy=0; ispy<16; ispy++){
      if(myAMB.getOutputSpyBufferSize() >= ispy) sbo.push_back(myAMB.getOutputSpyBuffer(ispy));
      else {
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), "The output spybuffer has not been read...");
	ers::error(ex);
      }
      L1IDs.push_back(std::vector<unsigned int>());
      ptr_L1IDs.push_back(std::vector<unsigned int>());
      end_events.push_back(std::vector<unsigned int>());
      full_data.push_back(sbo[ispy]->getBuffer());
      out_spy.push_back(sbo[ispy]->getBuffer());
      if(out_spy.at(ispy).size() == 0){
	int LAMB = ispy/4;
	int ROAD = ispy % 4;
	std::stringstream message;
	message << "LAMB " << LAMB << ", ROAD " << ROAD << " Spybuffer empty";
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ers::error(ex);
	osb++;
      }
      out_ptrs[ispy] = sbo[ispy]->getSpyPointer();
      out_sts[ispy] = sbo[ispy]->getSpyOverflow();
      if(out_sts[ispy]) overflow_out=true;
      else odiff[ispy] = range - out_spy.at(ispy).size();

      /**
       * Just like it was done for input spybuffers, the same is for output ones, finishing the creation of end_events vector.
       * While filling end_events vector with those values, an additional check is performed to ensure they're consecutive, otherwise it'll print out the ones whose difference isn't 1, as well as the spybuffer index
       */

      std::pair<unsigned int, unsigned int> ee_comp (0,0);
      std::vector<unsigned int>::iterator iter_loopCheck;
      prev_ee = -1;
      for(unsigned int iptr=0; iptr<out_spy.at(ispy).size(); iptr++){
	if(((out_spy.at(ispy).at(iptr)>>20)&0xfff)==0xf78){

	  // search duplicated L1ID to identify this is looped TV or not.
	  iter_loopCheck = find(L1IDs.at(ispy+12).begin(), L1IDs.at(ispy+12).end(), out_spy.at(ispy).at(iptr));
	  if(iter_loopCheck != L1IDs.at(ispy+12).end())
	    out_looped = true;

	  if(prev_ee == out_spy.at(ispy).at(iptr)){
	    // std::cerr << "  2 consecutive same ee in outspy lane " << ispy << "!" << std::endl;
	    // std::cerr << "  iptr: " << iptr << ", prev_ee: 0x" << std::hex << prev_ee << std::dec << std::endl;
	    // std::cerr << "  method4 Exiting!!" << std::endl;
	    std::stringstream message;
	    message << "2 consecutive same ee in outspy lane " << ispy << "!  iptr: " << iptr << ", prev_ee: 0x" << std::hex << prev_ee << std::dec << "  method4 Exiting!!";
	    daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	    ers::error(ex);

	    if(!keepFreeze)
	      myAMB.unfreezeSpyBuffers();
	    return -1;
	  }

	  L1IDs.at(ispy+12).push_back(out_spy.at(ispy).at(iptr));
	  ptr_L1IDs.at(ispy+12).push_back(iptr);

	  prev_ee = out_spy.at(ispy).at(iptr);

	  // if(ee_comp.first==0) {
	  //   ee_comp = std::make_pair(out_spy.at(ispy).at(iptr), 0);
	  //   end_events.at(ispy+12).push_back(out_spy.at(ispy).at(iptr));
	  //   continue;
	  // }else if(ee_comp.first!=0){
	  //   end_events.at(ispy+12).push_back(out_spy.at(ispy).at(iptr));
	  //   ee_comp.second = out_spy.at(ispy).at(iptr);
	  //   unsigned int diff = ee_comp.second - ee_comp.first;
	  //   if(diff == 1)
	  //     ee_comp.first = out_spy.at(ispy).at(iptr);
	  //   else {
	  //     // std::cerr << "Problem in Output SpyBuffer " << ispy << " detected --- First EE: " << std::hex << ee_comp.first << " Second EE: " << ee_comp.second << " DIFFERENCE IS NOT 1" << std::endl;
	  //     ee_comp.first = out_spy.at(ispy).at(iptr);
	  //     // break;
	  //   }
	  // }
	  // else continue;
	}
      }
    }

    /**
     * Check the case of events are looping but all lanes does not include complete 1 loop.
     */

    unsigned int tmp_first;
    unsigned int tmp_last;
    std::vector<unsigned int>::iterator itr_tmp_first;
    std::vector<unsigned int>::iterator itr_tmp_last;

    if(isb==0 && !in_looped){
      for(int ispy=0; ispy<12; ispy++){
	itr_tmp_first = L1IDs.at(ispy).begin();
	itr_tmp_last  = L1IDs.at(ispy).end()-1;
	tmp_first = *itr_tmp_first;
	tmp_last  = *itr_tmp_last;
	// if(tmp_first > tmp_last)
	//   in_ECR = true;
	if(tmp_first > tmp_last){
	  in_ECR = true;
	}
      }
    }

    if(osb==0 && !out_looped){
      for(int ispy=12; ispy<28; ispy++){
	itr_tmp_first = L1IDs.at(ispy).begin();
	itr_tmp_last  = L1IDs.at(ispy).end()-1;
	tmp_first = *itr_tmp_first;
	tmp_last  = *itr_tmp_last;
	// if(tmp_first > tmp_last)
	//   out_ECR = true;
	if(tmp_first > tmp_last){
	  out_ECR = true;
	}
      }
    }

    /**
     * Get common events among lanes of all inspy and outspy.
     */

    std::vector<unsigned int> inout_ptr_common_first_ee (28);
    std::vector<unsigned int> inout_ptr_common_last_ee (28);

    bool overflow = overflow_in || overflow_out;

    unsigned int success_search = 0;

    
    if(isb!=0){
      std::stringstream message;
      message << "At least one of the lanes of input spy buffer is empty! Exiting!!";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::error(ex);
      myAMB.unfreezeSpyBuffers();
      return -1;
    }

    if(osb!=0){
      std::stringstream message;
      message << "At least one of the lanes of output spy buffer is empty! Exiting!!";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::error(ex);
      myAMB.unfreezeSpyBuffers();
      return -1;
    }

    // std::cout << "check mode: " << std::dec << in_looped << " " << out_looped << " " << in_ECR << " " << out_ECR << " " << L1IDs.size() << std::endl;

    if(isb==0 && osb==0){

      if(in_looped || out_looped){// events are looped in at least one of the lanes.
	success_search = daq::ftk::find_first_last_ee_looped(L1IDs, ptr_L1IDs, inout_ptr_common_first_ee, inout_ptr_common_last_ee);
      }else if(in_ECR || out_ECR){// includes ECR
	success_search = daq::ftk::find_first_last_ee_ECR(overflow, L1IDs, ptr_L1IDs, inout_ptr_common_first_ee, inout_ptr_common_last_ee);
      }else{// continuous events
	success_search = daq::ftk::find_first_last_ee_continuous(overflow, L1IDs, ptr_L1IDs, inout_ptr_common_first_ee, inout_ptr_common_last_ee);
      }

    }

    if(success_search!=0){
      std::stringstream message;
      message << "Failed to find last and first ee from common! method4 Exiting!!";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::error(ex);
      if(!keepFreeze)
	myAMB.unfreezeSpyBuffers();
      return -1;
    }

    /**
     * Creation of 12 *.ss hitfiles for boardsim input; 3 options available to deal with input data
     *  1. Spybuffers not in overflow, with flag --sim
     *  2. Spybuffers in overflow, with flag --sim (useful for debugging purposes): input data vector is resized in order to include complete events common to all 12 spybuffers, without looking at the output
     *  3. Spybuffers in overflow, with flag --sync: input data vector is resized in order to include complete events common to input and output
     */

    /**
     * If the option --comp is set, creation of a 12-columns *.ss hits file (test vectors) with input spybuffers data, ready for ambslp_feed_hit input
     * The structure is the same to the one discussed in the step before
     */ 
    
    std::cout << "Creation of sshits file enabled" << std::endl;
    synch_in = in_spy;
    for(int ispy=0; ispy<12; ispy++){
      synch_in.at(ispy).erase(synch_in.at(ispy).begin()+inout_ptr_common_last_ee.at(ispy)+1, synch_in.at(ispy).begin()+synch_in.at(ispy).size());
      synch_in.at(ispy).erase(synch_in.at(ispy).begin(), synch_in.at(ispy).begin()+inout_ptr_common_first_ee.at(ispy));
    }

    // dump input spybuffer
    filename = "sshits_comp_"+label+".ss";
    daq::ftk::dump_SShitfile(filename, synch_in);
    // if(daq::ftk::dump_SShitfile(filename, synch_in) == 0) std::cout << "Creation of synch sshits file complete" << std::endl;
    
    // dump output spybuffer
    filename = "outspy_comp_"+label+".txt";
    ofstream outstream;
    outstream.open (filename);
    bool write=false;
    for(int ispy=12; ispy<28; ispy++){
      for(unsigned int iptr=0; iptr<full_data.at(ispy).size(); iptr++){

	if(inout_ptr_common_first_ee.at(ispy) == 0 || iptr==inout_ptr_common_first_ee.at(ispy))
	  write=true;

	if(write){
	  outstream << std::setw(8) << std::setfill('0') << std::right << std::hex << full_data.at(ispy).at(iptr) << '\n';
	  //below cerr is for comparison in AMBTest.py, is there better way??
	  if(dump_meth4)
	    std::cerr << std::setw(8) << std::setfill('0') << std::right << std::hex << full_data.at(ispy).at(iptr) << std::endl;
	}

	if(iptr==inout_ptr_common_last_ee.at(ispy)){
	  write=false;
	  break;
	}
      }
    }
    std::cout << "Creation of synch files completed" << std::endl;

    /**
     * AMB Spybuffers are unfreezed; it also checks if the unfreeze word was read and written properly
     */

    if(!keepFreeze)
      myAMB.unfreezeSpyBuffers();
      
  }

  return 0;

}
