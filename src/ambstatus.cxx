

#include "ambslp/configuration.h"
#include "ambstatus/sharedMemory_access.h"
#include "ambstatus/nominal_configuration.h"

using namespace daq::ftk;


AMBConfiguration::AMBConfiguration( InitConfiguration *init ) : currentStatus(0) //
{
}

AMBConfiguration::~AMBConfiguration() { }

const AMBStatus * AMBConfiguration::status() {
  return currentStatus;
}


daq::ftk::AMBConfiguration* daq::ftk::get_AMBConfiguration( daq::ftk::InitConfiguration *init ) {

  return new AMBConfiguration_nominal( init );
}


