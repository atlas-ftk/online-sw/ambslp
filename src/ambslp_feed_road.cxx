/* AMfeed.cpp
 
   Author: Francesco Crescioli
   Date: 11/4/08

   rev MP 26.07.2010

   Based on AMsim by
   Author: Alberto Annovi
   Date  : October 14, 2004

*/


#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv){ 
  using namespace  boost::program_options ;
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("road_file", value< std::string >()->default_value(""), 
        "Road Pattern file (can be specified in any parameter position, also omitting \"--road_file\")")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("loopfifovme", value< std::string >()->default_value("0"), "Enable the loop of the VME fifo")
  ;

  positional_options_description pd; 
  pd.add("road_file", 1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
  {
    std::cerr << desc << std::endl;
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
  {
    std::cout << std::endl <<  desc << std::endl ;
    return 0;
  }

  int slot              = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int loopFifoVme              = daq::ftk::string_to_int( vm["loopfifovme"].as<std::string>() );
  std::string roadfname = vm["road_file"].as<std::string>() ;
  daq::ftk::assert_string_parameter_not_empty( roadfname, std::string("roadfname") ); // checking that is not empty
 	
  return daq::ftk::ambftk_feed_road( slot, loopFifoVme, roadfname.c_str() );
}



