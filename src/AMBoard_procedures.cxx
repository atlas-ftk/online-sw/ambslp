#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_mainboard.h"
#include <ambslp/ambslp_vme_regs.h>
#include "ambslp/exceptions.h"

#include <ctime>
using namespace std;

namespace daq {
  namespace ftk {

    void AMBoard::RC_setup(const dal::ReadoutModule_PU* module_dal){
      //Reading the attributes from OKS
      uint32_t tmp_crate = module_dal->get_Crate();
      std::stringstream tmp_name;
      tmp_name << "FTK-AMB-" << std::to_string(tmp_crate) << "-" << std::to_string(m_slot);
      m_name = tmp_name.str();
      ERS_LOG("ReadoutModule_PU::setup: NAME = " << m_name);

      ftkTimeLog tLog(ERS_HERE, name_ftk(), m_name + " setup step 0");

      m_ForceWrite = module_dal->get_ForceWrite(); // checking if the bank is force to be updated

      m_NumberOfPatternsToCheck = module_dal->get_NumberOfPatternsToCheck();

      m_dumpFolder = module_dal->get_DumpFolder();
      m_lambmask = module_dal->get_LAMBMask();
      m_hitfwver = module_dal->get_HITFWVersion();
      m_roadfwver = module_dal->get_ROADFWVersion();
      m_vmefwver = module_dal->get_VMEFWVersion();
      m_ctrlfwver = module_dal->get_CTRLFWVersion();
      m_lambfwver = module_dal->get_LAMBFWVersion();
      m_DummyHit = module_dal->get_DummyHit();
      m_SlowLinkMask = module_dal->get_SlowLinkMask();
      m_useblock = module_dal->get_UseBlock();
      m_CanDoPhase = module_dal->get_UsePhase();
      m_matchThr = module_dal->get_Threshold();
      m_initThr = module_dal->get_InitialThreshold();
      m_checkBank = module_dal->get_CheckBank();
      m_bypassBus = module_dal->get_BypassBus();
      m_pcbVersion = module_dal->get_PCBversion();
      m_dvMajority = module_dal->get_DVmajority();
      m_nAM06Align = module_dal->get_NAM06Align();
      m_nHITAlign = module_dal->get_NHITAlign();
      m_nROADAlign = module_dal->get_NROADAlign();
      m_nPattCheckInterval = module_dal->get_NPattCheckInterval();
      m_PattProbTolerance = module_dal->get_PattProbTolerance();
      m_FreezeSeverityError = module_dal->get_FreezeSeverity();
      m_nAM06LinkErrors = 0;
      m_nHITLinkErrors = 0;
      m_nROADLinkErrors = 0;

      ERS_LOG("ReadoutModule_PU::setup: SLOT = " << std::to_string(m_slot));

      m_skip_load_and_check = false;
      if (m_NumberOfPatternsToCheck == 0)  m_skip_load_and_check = true;

      // Pattern Bank  configuration
      const ftk::dal::PatternBankConf *PBankConf_dal = module_dal->get_PatternBankConf();
      if (PBankConf_dal) {
	ERS_LOG("Loading pattern bank from PatternBankConf object");
	m_NPattPerChip = PBankConf_dal->get_NumPattPerChip();
	m_patternBankFileName = PBankConf_dal->get_FilePath();
	m_banktype = PBankConf_dal->get_BankType();
	m_ssoffset = PBankConf_dal->get_SSOffset();
	m_ExpAMBChecksum = PBankConf_dal->get_AMBChecksum();

	ostringstream msgbank;
	msgbank << "Patterns from: " << m_patternBankFileName << "(" << m_banktype << "), NP/Chip=" << m_NPattPerChip << ", SSoff=" << m_ssoffset <<endl;
	msgbank << "Expected checksum: " << hex << m_ExpAMBChecksum << dec;
	ERS_LOG(msgbank.str());

      } else {
	ERS_LOG("No pattern bank configuration found, using default value");
      }

      //AMBStandaloneTest
      const ftk::dal::AMBStandaloneTest *SATest_dal = module_dal->get_AMBStandaloneTest();
      if (SATest_dal) {
	ERS_LOG("Loading pattern bank from Stand Alone TestConf object");
	m_DoStandaloneTest = true;
	m_testConfLoop = SATest_dal->get_Loop();
	m_testFilePath = SATest_dal->get_TestFilePath();

	ostringstream msgbank;
	msgbank << "Path to test hit file: " << m_testFilePath << ", Loop=" << m_testConfLoop;
	ERS_LOG(msgbank.str());

      }
      else {
	ERS_LOG("No stand alone test configuration found, using default value");
      }

      m_vme->disableBlockTransfers(not m_useblock);
      m_LAMB_mask = getLAMBMap();


      // Check FPGS FW versions
      tLog.end(ERS_HERE);  // End log conter

      ftkTimeLog tLog1(ERS_HERE, name_ftk(), m_name + " setup step 1 Reading_FW_versions");

      check_FPGA_FW_version(std::string("VME")    );
      check_FPGA_FW_version(std::string("CONTROL"));
      check_FPGA_FW_version(std::string("HIT")    );
      check_FPGA_FW_version(std::string("ROAD")   );
      check_FPGA_FW_version(std::string("LAMB")   );

      tLog1.end(ERS_HERE);  // End log conter

    } // RC_setup()


    // ================================================================================

    void AMBoard::RC_configure() {

      ftkTimeLog tLog0(ERS_HERE, name_ftk(), m_name + " configure step 0");

      //construct the FTKPatternBank m_PatternBank object
      m_PatternBank = FTKPatternBank();
      
      // prepare a delta-time variable to be used to pause between some steps
      const struct timespec deltaT     = { 0, 1000000};
      const struct timespec deltaT_DCS = { 5, 0};
      bool all_links_up(false);
      unsigned int n_tries(1);

      // set DCS server semaphore red (stop DCS monitoring)
      if(m_vme->read_word(AMB_CONTROL_DCS_RUN) == 0xCAFE) { 
	nanosleep(&deltaT_DCS,0x0);
	if(m_vme->read_word(AMB_CONTROL_DCS_RUN) == 0xCAFE) {
	  std::stringstream message;
	  message << "DCS semaphore is already taken";

	  // daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  // ers::warning(ex);

	  daq::ftk::Information exception(ERS_HERE, name_ftk(), message.str());
	  ers::info(exception);
	}
      }
      ERS_LOG("Stopping DCS monitoring during configure");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0xCAFE);

      tLog0.end(ERS_HERE);  // End log conter

      ftkTimeLog tLog1(ERS_HERE, name_ftk(), m_name + " configure step 1 Reset_AMB_LAMB");

      // Reset AMB and LAMB, isolate the board
      init(1, 2); // reset the LAMBs before the AMB. This is needed to clear errors from LAMBs first
      nanosleep(&deltaT,0x0);
      init(3, 2);
      nanosleep(&deltaT,0x0);
      //init(0, 2);
      //nanosleep(&deltaT,0x0);
      init(1, 2);
      nanosleep(&deltaT,0x0);
	
      //Resetting the DummyHit
      unconfigDummyHit();

      // Force HOLD to AUX until prepareForRun
      idle_mode();

      // Configure the Bousca (LAMB FPGA)
      m_vme->write_check_word(AMB_VME_LAMB_INDIRECT_ADDR, AMB_VME_LAMB_INDIRECT_ADDRVAL_NO_ROAD);		
      m_vme->write_check_word(AMB_VME_LAMB_INDIRECT_DATA, 0x0); // make sure NO_ROAD signal to ROAD-FPGA are not forced

      // -------------------------------
      // Configure of minimum number of idles on HIT FPGA to AM ASIC links  [NEW]
      m_vme->write_check_word(AMB_HIT_NHIT_VS_NIDLE, 0x04000004); // 0x400 data words max interleaved with 0x4 idle words

      tLog1.end(ERS_HERE);  // End log conter

      ftkTimeLog tLog2(ERS_HERE, name_ftk(), m_name + " configure step 2 Set_voltage_limits");

      // refresh the Voltage limits and output values
      //--------------------------------------------------------------------------------------

      // Hack to be fixed !!!
      // res_DC[20] > 0 ==>  V in range for all LAMBS
      bool DC_values_all_good = true;

      PRINT_LOG("Setting voltages: out/max = " << hex << (m_DCDC_val_Vout>>16) << "/" << (AMB_HIT_DCDC_cmd_VLimit>>16) << dec);
      std::vector<float> res_DC;
      DCDC_setVLimits();
      DCDC_setVOutputs();
      res_DC = DCDC_read();
      // Hack to be fixed !!!
      if ( res_DC.at(20) < 0.0 )  DC_values_all_good = false;

      PRINT_LOG("DCDC_read tot value  =  " << res_DC.at(20) << "    DC_values_all_good  =  " << DC_values_all_good << "  try 0");

      if ( DC_values_all_good ) {
	PRINT_LOG("DC values are all good.");
      } else {
	int itry = 0;
	int ntry = 5;
	while(itry<ntry){
	  std::stringstream message;
	  message << "Some DC/DC value is not in the expected range. Re-configuring DC-DC converter. " << itry+1 << "/" << ntry << " retry.";
	  // daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  // ers::warning(ex);

	  daq::ftk::Information exception(ERS_HERE, name_ftk(), message.str());
	  ers::warning(exception);

	  res_DC.clear();
	  DCDC_setVLimits();
	  DCDC_setVOutputs();
	  res_DC = DCDC_read();
	  // Hack to be fixed !!!
	  if ( res_DC.at(20) < 0.0 )  DC_values_all_good = false;
	  else                        DC_values_all_good = true;

	  itry++;

	  PRINT_LOG("DCDC_read tot value  =  " << res_DC.at(20) << "    DC_values_all_good  =  " << DC_values_all_good << "  try " << itry+1 << "/" << ntry);
	  // std::cout << "tomoya check res: " << res_DC[20] << std::endl;
	  if ( DC_values_all_good ) break;
	}

	if ( !DC_values_all_good ) {
	  std::stringstream message;
	  message << "Some DC value is not in the expected range. Re-configuring DC-DC converter does not work.";

	  // daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  // ers::error(ex);

	  daq::ftk::ftkException exception(ERS_HERE, name_ftk(), message.str());
	  // ers::error(exception);   // [EH] Commented out to avoid error duplications: the ers::error/fatal should be issued in the ReadoutModule_PU, where the excetion is cought!
	  throw exception;            // [EH] To be catched in the ReadoutModuel_PU
	}
      }


      tLog2.end(ERS_HERE);  // End log conter
      //--------------------------------------------------------------------------------------

      ftkTimeLog tLog3(ERS_HERE, name_ftk(), m_name + " configure step 3 Check_links");

      ambslp_config_reg(0);
      nanosleep(&deltaT,0x0);
     
      // Set slowlink mask.  0x0f for SCT. Took value from OKS                    
      m_vme->write_check_word(AMB_HIT_SLOW_PROCESSING, m_SlowLinkMask);              
      if (m_SlowLinkMask!=0)
	ERS_LOG("Slow SCT Module " << m_SlowLinkMask);
    
      unsigned int err_lamb_0(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB0));
      unsigned int err_lamb_1(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB1));
      unsigned int err_lamb_2(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB2));
      unsigned int err_lamb_3(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB3));
      bool amup=false;
      while((not all_links_up) and (n_tries<=m_nROADAlign)){
        ERS_LOG("Starting Configure procedure: try " << n_tries << "/" << m_nROADAlign);

        n_tries += 1;
        all_links_up = true;


        // Reset GTPs HIT
	/* AUX links to AMB (HIT FPGA) may not be up and running yet.
	 * Reset HIT GTPs only once. Ignore the fact that input links may not be aligned. Just make sure output links are reset.
         * this is required to eventually run the internal AM chip test procedure,
         * there is currently (2017/01/17)
         */
        reset_gtp(1, true, 0x1);

        nanosleep(&deltaT,0x0);

        // Setup all SERDES of AMchips
	amup = amchip8b10b_up_and_check(0x3, 1);
	all_links_up &= amup;

        // Reset ROAD GTPs. Make sure they align on AM06 output links.
        all_links_up = all_links_up && (reset_gtp(10, true, 0x2)==0);

        nanosleep(&deltaT,0x0);

        // Reset non-configuration registers and FIFOs
        init(0,1);
        nanosleep(&deltaT,0x0);
        init(2);
        nanosleep(&deltaT,0x0);


        //check lambs link are up 
        err_lamb_0 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB0);
        err_lamb_1 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB1);
        err_lamb_2 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB2);
        err_lamb_3 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB3);
        ERS_LOG("8b10b Error counter LAMB output link 0: " << hex << err_lamb_0 << dec);
        ERS_LOG("8b10b Error counter LAMB output link 1: " << hex << err_lamb_1 << dec);
        ERS_LOG("8b10b Error counter LAMB output link 2: " << hex << err_lamb_2 << dec);
        ERS_LOG("8b10b Error counter LAMB output link 3: " << hex << err_lamb_3 << dec);
        all_links_up = all_links_up && (err_lamb_0 ==  0x0000);
        all_links_up = all_links_up && (err_lamb_1 ==  0x0000);
        all_links_up = all_links_up && (err_lamb_2 ==  0x0000);
        all_links_up = all_links_up && (err_lamb_3 ==  0x0000);
				
	if(not all_links_up){
	  std::stringstream message;
	  message << "AMB link alignment failed during config procedure. TRYING AGAIN. AMB: " << m_name << " LAMB output link error counts (LAMB0 - LAMB1 - LAMB2 - LAMB3): " << hex << err_lamb_0 
		  << " - " << hex << err_lamb_1 << " - " <<   hex << err_lamb_2 << " - " << hex << err_lamb_3 <<", AM06 links error bit: "<<!amup<<" (0: no problem, 1: some problem)"; 
	  // daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  // ers::info(ex);

	  daq::ftk::Information exception(ERS_HERE, name_ftk(), message.str());
	  ers::info(exception);
	}
      }   //while((not all_links_up) and (n_tries<=5)){

      if (not all_links_up) {
	daq::ftk::ambslp_status( m_slot, 0, "");
	std::stringstream message;
        message << "AMB link alignment failed during config procedure. LAMB output link error counts (LAMB0 - LAMB1 - LAMB2 - LAMB3): " << hex << err_lamb_0 
		<< " - " << hex << err_lamb_1 << " - " << hex << err_lamb_2 << " - " << hex << err_lamb_3 <<", AM06 links error bit: "<<!amup<<" (0: no problem, 1: some problem)"; 
        // daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	// ers::error(ex);

	daq::ftk::ftkException exception(ERS_HERE, name_ftk(), message.str());
	// ers::error(exception);   // [EH] Commented out to avoid error duplications: the ers::error/fatal should be issued in the ReadoutModule_PU, where the excetion is cought!
	throw exception;            // [EH] To be catched in the ReadoutModuel_PU
      }

      // Make a very short test to verify the SERDES configuration of the chips
      amchip8b10b_smart(); //also configures register AMB_ROAD_PATTERNX_ADDRESS 
      nanosleep(&deltaT,0x0);

      //reset spy buffers - this should in principle only be done in prepareForRun() but found to be useful to do it already here
      // daq::ftk::ambslp_reset_spy(m_slot, 1, 1);
      resetSpyBuffers(1, 1);

      /* Before the pattern bank is read from the local resources, the expected checksum
       * is compared, with the one wrote in the board register. If the expected checksum
       * isn't 0x0 and agrees with the register the read part is skipped, saving extra time
       * and resources.
       */
      std::vector<uint32_t> ndc;
      
      int  ntrial               = 0;
      bool written_pattern_fine = false;


      tLog3.end(ERS_HERE);  // End log conter

      ftkTimeLog tLog4(ERS_HERE, name_ftk(), m_name + " configure step 4 Read_pattern_bank ");

      readPatternBankDCconfig(m_banktype, m_patternBankFileName, ndc);
      
      tLog4.end(ERS_HERE);  // End log conter


      ftkTimeLog tLog5(ERS_HERE, name_ftk(), m_name + " configure step 5 Load_check_pattern_bank ");

      if ( checkPatternBankChecksum(m_ExpAMBChecksum) ) { // checksum good

        if (!daq::ftk::isLoadMode()) { // isLoadMode(0)

          if (m_skip_load_and_check) { // skip_load_and_check

            ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(0): OKS checksum already present in the AMB register, bank loading skipped");

          } else {

            ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(0): OKS checksum already present in the AMB register, bank test only");

            // readPatternBankDCconfig(m_banktype, m_patternBankFileName, ndc);
            readPatternBank(m_banktype, m_patternBankFileName, m_ssoffset, ndc);

            HL_writebank(m_NPattPerChip, true, true, 8*m_nPattCheckInterval, false); 

          }

        } else { // isLoadMode(1)

          if (m_ForceWrite) {// ForceWrite

            ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(1): OKS checksum matches AMB register checksum, but ForceWrite is active - loading bank.");
	    ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), "Slot " + std::to_string(m_slot)
                                              + " AMB in isLoadMode(1): OKS checksum matches AMB register checksum, but ForceWrite is active - loading bank."));

            readPatternBank(m_banktype, m_patternBankFileName, m_ssoffset, ndc);

            written_pattern_fine     = HL_writebank(m_NPattPerChip,  true, true, m_nPattCheckInterval, m_ForceWrite); //write and check patterns
            if (!written_pattern_fine) HL_writebank(m_NPattPerChip, false, true, m_nPattCheckInterval, m_ForceWrite); //re-write patterns once if there are more than N (=thr) wrong patterns

          } else {// !ForceWrite

            ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(1): OKS checksum already present in the AMB register, bank loading skipped");
            // [???] Message incoherent with "if (!written_pattern_fine) then force rewrite" ... 

            // readPatternBankDCconfig(m_banktype, m_patternBankFileName, ndc);
            readPatternBank(        m_banktype, m_patternBankFileName, m_ssoffset, ndc);

            written_pattern_fine     = HL_writebank(m_NPattPerChip,  true, true, m_nPattCheckInterval, m_ForceWrite); //check patterns
            if (!written_pattern_fine) HL_writebank(m_NPattPerChip, false, true, m_nPattCheckInterval, true);         //re-write patterns once if there are more than N (=thr) wrong patterns
          }
        }

      } else { // checksum not good

	if (!daq::ftk::isLoadMode()){ // isLoadMode(0)

          ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(0): OKS checksum different from AMB register checksum!");
	  ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), "Slot "+ std::to_string(m_slot) + " AMB in isLoadMode(0): OKS checksum different from AMB register checksum!"));

        } else { // isLoadMode(1)

          while ( (!checkPatternBankChecksum(m_ExpAMBChecksum) || !written_pattern_fine) && ntrial<2) {

            ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(1): OKS checksum different from AMB register checksum! "<<ntrial<<"/1 trials");
	    ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), "Slot " + std::to_string(m_slot) + " AMB in isLoadMode(1): OKS checksum different from AMB register checksum! "
                                              + std::to_string(ntrial) + "/1 trials"));

            readPatternBank(m_banktype, m_patternBankFileName, m_ssoffset, ndc);

            written_pattern_fine = HL_writebank(m_NPattPerChip, (ntrial==0) ? true : false, true, m_nPattCheckInterval, true); 

            ntrial++;
          }

          if (ntrial == 2) {
            ERS_LOG("Slot "<< std::to_string(m_slot) <<" AMB in isLoadMode(1): Pattern bank can't be written correctly according to the checksum!");
	    ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), "Slot " + std::to_string(m_slot) + " AMB in isLoadMode(1): Pattern bank can't be written correctly according to the checksum!"));
          }

        }
      } // checksum

      tLog5.end(ERS_HERE);  // End log conter


      ftkTimeLog tLog6(ERS_HERE, name_ftk(), m_name + " configure step 6 Finalize_configuration ");

      Configure_AMchips();
      ERS_LOG("Clearing the bank..");
      m_PatternBank.clearAll();
      ERS_LOG("Bank cleared..");
      
      // set DCS server semaphore green (restart DCS monitoring)
      ERS_LOG("Re-starting DCS monitoring after configure");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0x0000);


      tLog6.end(ERS_HERE);  // End log conter
      
      return;
    } // RC_configure()





    // =========================================================================================================================

    /** Perform the steps for the connection, before the run preparation */
    void AMBoard::RC_connect()
    {
      ftkTimeLog tLog0(ERS_HERE, name_ftk(), m_name + " connect step 0");

      m_ftkemonDataOut=reinterpret_cast< daq::ftk::FtkEMonDataOut* >(ROS::DataOut::sampled());
      if(!m_ftkemonDataOut)
	{ 
	  std::stringstream message;
	  message << "EMonDataOut plugin not found! SpyBuffer data will not be pushished in emon";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::warning(ex);	  
	} 
      else
	{ 
	  m_ftkemonDataOut->setScalingFactor(1);
	  ERS_LOG("Found EMonDataOut plugin"); 
	}
      
      // set DCS server semaphore red (stop DCS monitoring)
      ERS_LOG("Stopping DCS monitoring during connect");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0xCAFE);

      tLog0.end(ERS_HERE);  // End log conter
  // ------------
      ftkTimeLog tLog1(ERS_HERE, name_ftk(), m_name + " connect step 1 HIT_link_check");

      const struct timespec deltaT = { 1, 0};
      bool all_links_up(false); 
      unsigned int n_tries(1); 

      while((not all_links_up) and (n_tries<=m_nHITAlign)){
        ERS_LOG("Starting HIT FPGA Rx alignment procedure: try " << n_tries << "/" << m_nHITAlign);
        all_links_up = true;
        n_tries++;

        // reset HIT RX GTP links (once RX/TX reset separated):
        // Reset GTPs again
	if(!m_DoStandaloneTest){
	  all_links_up = all_links_up && (reset_gtp(19, false, 0x1)==0);
	  nanosleep(&deltaT,0x0);
	}

        // Reset non-configuration registers and FIFOs,
        init(0,1);
        init(2,1);

	if(not all_links_up){
	  std::stringstream message;
	  message << "AMB link alignment failed during connect procedure. Not all HIT FPGA Rx are aligned.";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::info(ex);
	}

      }

      if(not all_links_up){
	std::stringstream message;
        message << "AMB link alignment failed during connect procedure. Not all HIT FPGA Rx are aligned.";
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ERS_LOG(message.str());
	ers::error(ex);
      }


      tLog1.end(ERS_HERE);  // End log conter
  // ------------
      ftkTimeLog tLog2(ERS_HERE, name_ftk(), m_name + " connect step 2 LAMB_link_check ");

      unsigned int err_lamb_0(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB0));
      unsigned int err_lamb_1(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB1));
      unsigned int err_lamb_2(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB2));
      unsigned int err_lamb_3(m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB3));
      all_links_up=false;
      n_tries=1;
      bool amup=false;
      while((not all_links_up) and (n_tries<=m_nAM06Align)){
        ERS_LOG("Starting AM06 alignment procedure: try " << n_tries << "/" << m_nAM06Align);
        all_links_up = true;
        n_tries++;

	if(!m_DoStandaloneTest){
	  // Setup input SERDES of AMchips (Do the same function twice for now (one for config, the other for link check) to avoid link errors)
	  bool tmp = amchip8b10b_up_and_check(0x0, 1, 2, 3, true);
	  tmp &= true;   // just to avoid compilation warning;
	  nanosleep(&deltaT,0x0);
	  amup = amchip8b10b_up_and_check(0x3, 1, 2, 3, false);
	  all_links_up &= amup;
	}

        // Flush remnant pattens from the previous setting
        amchip_init_evt(0x3);
        amchip_init_evt(0x3);

        // Reset non-configuration registers and FIFOs,
        init(0,1);
        init(2,1);

        //check lambs link are up
        err_lamb_0 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB0);
        err_lamb_1 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB1);
        err_lamb_2 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB2);
        err_lamb_3 = m_vme->read_word( AMB_ROAD_8B10B_ERRORS_LAMB3);
        ERS_LOG("8b10b Error counter LAMB output link 0: " << hex << err_lamb_0 << dec);
        ERS_LOG("8b10b Error counter LAMB output link 1: " << hex << err_lamb_1 << dec);
        ERS_LOG("8b10b Error counter LAMB output link 2: " << hex << err_lamb_2 << dec);
        ERS_LOG("8b10b Error counter LAMB output link 3: " << hex << err_lamb_3 << dec);
        all_links_up = all_links_up && (err_lamb_0 ==  0x0000);
        all_links_up = all_links_up && (err_lamb_1 ==  0x0000);
        all_links_up = all_links_up && (err_lamb_2 ==  0x0000);
        all_links_up = all_links_up && (err_lamb_3 ==  0x0000);
	
	if(not all_links_up){
	  std::stringstream message;
	  message << "AMB link alignment failed during the AM06 link config procedure. LAMB output link error counts (LAMB0 - LAMB1 - LAMB2 - LAMB3): " << hex << err_lamb_0 
		  << " - " << hex << err_lamb_1 << " - " << hex << err_lamb_2 << " - " << hex << err_lamb_3<<", AM06 links error bit: "<<!amup<<" (0: no problem, 1: some problem)"; 
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::info(ex);
	}	
      }

      if(not all_links_up){
	daq::ftk::ambslp_status( m_slot, 0, "");
	std::stringstream message;
        message << "AMB link alignment failed during the AM06 link config procedure. LAMB output link error counts (LAMB0 - LAMB1 - LAMB2 - LAMB3): " << hex << err_lamb_0 
		<< " - " << hex << err_lamb_1 << " - " << hex << err_lamb_2 << " - " << hex << err_lamb_3<<", AM06 links error bit: "<<!amup<<" (0: no problem, 1: some problem)"; 
        daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ERS_LOG(message.str());
	ers::error(ex);
      }

      // set DCS server semaphore green (restart DCS monitoring)
      ERS_LOG("Re-starting DCS monitoring after connect");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0x0000);

      tLog2.end(ERS_HERE);  // End log conter

      return;
    }  // RC_connect()


    /** Function that reports the procedure required by the prepareForRun
     * transition.
     *
     * The procedure enables the board to recevie data from AUX.
     */
    void AMBoard::RC_prepareForRun() {

      ftkTimeLog tLog0(ERS_HERE, name_ftk(), m_name + " prepareForRun step 0");

      // set DCS server semaphore red (stop DCS monitoring)
      ERS_LOG("Stopping DCS monitoring during prepareForRun");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0xCAFE);

      const struct timespec deltaT = { 1, 0};

      // Set the dummy-hit, setting the minimum power consumption to avoid
      // problems
      ERS_LOG("Going to activate dummy hits. (This will increase the level of bit flip in steps.)");
      daq::ftk::ambslp_status( m_slot, 1, "");
      configureDummyHit();
      ERS_LOG("Finished activating dummy hits.");
      daq::ftk::ambslp_status( m_slot, 1, "");

      // Flush remnant patterns from the previous setting, reset FIFOs, reset spybuffers
      cleanup();

      // Taking AMB back from isolation to talking with AUX
      if(!m_DoStandaloneTest){
	m_vme->write_check_word(AMB_CONTROL_ERROR_SEVERITY, m_FreezeSeverityError);
	m_vme->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0x0000);
	m_vme->write_check_word(DEBUG_DISCARD_AUX_HIT, 0x000);
	nanosleep(&deltaT,0x0);
	m_vme->write_check_word(DEBUG_FORCE_HOLD_HIT, 0x000);	
      }

      m_vme->write_check_word(DEBUG_DISCARD_OUTPUT_DATA, 0x0000);
      
      if(m_DoStandaloneTest){
      	// set the severity error in CTRL to control freezes
	m_vme->write_check_word(AMB_CONTROL_ERROR_SEVERITY, m_FreezeSeverityError & 0x3);
	m_vme->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0xffff);
      	if(m_testFilePath.c_str()!=""){
	  ERS_LOG("testFilePath: " << m_testFilePath << ", testConfLoop " << m_testConfLoop);
	  m_vme->write_word(CONTROL_REGISTER_HIT, 0x30000);
	  feed_hit_init();
	  feed_hit(m_testFilePath.c_str());
	  feed_hit_end(m_testConfLoop);
	  ERS_LOG( "0x4080 content  " << hex << m_vme->read_word(0x4080));
	  //daq::ftk::ambslp_out_spy(m_slot, 0); 					//Added for debugging
	  daq::ftk::ambslp_status( m_slot, 1, "");
      	}
      }

      //initialise the number of link errors member variables
      m_nAM06LinkErrors = 0;
      m_nHITLinkErrors = 0;
      m_nROADLinkErrors = 0;

      // set DCS server semaphore green (restart DCS monitoring)
      ERS_LOG("Re-starting DCS monitoring after prepareForRun");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0x0000);


      tLog0.end(ERS_HERE);  // End log conter

      return;
    }  // RC_prepareForRun()


    /** Function that reports the procedure required by the unconfigure  transition.
     *
     * The procedure unconfigure the board
     */
    void AMBoard::RC_unconfigure() {

      ftkTimeLog tLog0(ERS_HERE, name_ftk(), m_name + " unconfigure");

      unconfigDummyHit();
      ERS_LOG("Finished dis-activating dummy hits.");
      
      // Disable forced hold to AUX
      //m_vme->write_check_word(DEBUG_FORCE_HOLD_HIT, 0x000);
      //m_vme->write_check_word(DEBUG_DISCARD_OUTPUT_DATA, 0x0000);
      //m_vme->write_check_word(DEBUG_DISCARD_AUX_HIT, 0x000);

      tLog0.end(ERS_HERE);  // End log conter

      return;
    }  // RC_unconfigure()


    /** This method configures the AM06 chips. This function is called after writing the pattern bank.
     *  It must be also called during board configuration.
     * */
    void AMBoard::Configure_AMchips() {
      u_int disable_mask = 0; // enable all blocks (including Top2 if it is a AM05)
      if (m_AMChip->getNAME() == AMChip05::global()->getNAME())
        disable_mask = 0x2; // disable Top2 in AM05  

      // Configure AMchips
      amchip_init_evt(0x3);
      amchip_jpatt_cfg(0x3, "a",  m_matchThr, 0, disable_mask, 0, 0, 1, 3, 0);
      amchip_jpatt_cfg(0x3, "a",  m_matchThr, 0, disable_mask, 0, 0, 1, 3, 0);
      amchip_init_evt(0x3);
    } 

    /** This function is the more standard procedure to
     * write a pattern bank into the board
     * */
    bool AMBoard::HL_writebank(int npatts, bool firstTime, bool test_written_pattern, int test_interval, bool write_pattern) {

      ftkTimeLog tLog0(ERS_HERE, name_ftk(), m_name + " HL_writebank step 0");

      /* update the checksum before the bank is used, according other configuration data,
       * this should allow the value to change if the same pattern bank is used in different
       * ways, e.g. loading a smaller number of patterns
       [???] why this is not done after the filling of the patternBank object?? 
       */
      const unsigned int oldChksum = m_PatternBank.getChecksum();
      std::vector<uint32_t> data={npatts};

      tLog0.end(ERS_HERE);  // End log conter

      // evaluate the checksum of the patterns actually loaded on chips + 1 step (data vector: 1 element with value npatt)
      // firstTime variable protects from multiple iterations of this last step
      
      ftkTimeLog tLog1(ERS_HERE, name_ftk(), m_name + " HL_writebank step 1 CHECKSUM_update");
      if (firstTime) {
	      m_PatternBank.updateChecksum(1,data);
      }
      tLog1.end(ERS_HERE);  // End log conter


      const unsigned int pb_checksum = m_PatternBank.getChecksum();
      PRINT_LOG("Pbank checksum update: " << hex << oldChksum << " -> " << pb_checksum << dec);

      if ((pb_checksum != m_ExpAMBChecksum) && (m_ExpAMBChecksum != 0)) {

	std::stringstream message;
	message << "Slot " << m_slot <<": expected checksum from OKS (0x"<< hex << m_ExpAMBChecksum <<") doesn't match pbank checksum (0x"<< hex << pb_checksum << dec <<") from bank file  " << m_patternBankFileName ;
	ERS_LOG(message.str());
	ers::error(daq::ftk::ftkException(ERS_HERE, name_ftk(), message.str()));
      }


      // read the checksum of the existing bank and skip the following part
      unsigned int reg_checksum = m_vme->read_word(AMB_VME_BANKCHECKSUM);
      
      if (!reg_checksum) {
	std::stringstream message;
	message << "Invalid (0x0) register checksum found: " << hex << reg_checksum << dec;
	ERS_LOG(message.str());
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ers::warning(ex);
      } else if (reg_checksum != pb_checksum) {
	std::stringstream message;
	message << "Slot " << m_slot <<": register checksum (0x"<< hex << reg_checksum <<") doesn't match pbank checksum (0x"<< hex << pb_checksum << dec <<"). Possibly loading a different bank.";
	ERS_LOG(message.str());
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ers::warning(ex);
      }


      if (write_pattern) {
	ERS_LOG("Going to load on AM chips the pattern bank: "  << m_PatternBank.getName().c_str() << " chksum = " << hex << pb_checksum);
      } else {
	ERS_LOG("Pattern bank: "  << m_PatternBank.getName().c_str() << " chksum = " << hex << pb_checksum << "  is loaded on AM chips. Going to check it.");
      }


      // Prepare AMchips to load patterns 
      u_int disable_mask = 0; // enable all blocks (including Top2 in AM05)
      amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);
      amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);

      // Flush remnant pattens from the previous setting
      amchip_init_evt(0x3);

      //      daq::ftk::ambslp_amchip_disable_bank(m_slot, 2, 3, 3072, 0);

      if (write_pattern) {
	if (npatts != AMChip06::global()->getMAXPATT_PERCHIP()) {

	  disable_bank(2, 3, m_AMChip->getMAXPATT_PERCHIP());

	} else {

	  ERS_LOG("Number of patterns to write per AM06 = " << npatts << " / " << m_AMChip->getMAXPATT_PERCHIP() << "  -  disable_bank not needed! ");
	}
      }

      // if (write_pattern) disable_bank(2, 3, m_AMChip->getMAXPATT_PERCHIP());

      if (m_AMChip->getNAME() == AMChip05::global()->getNAME())
        disable_mask = 0x2; // disable Top2 in AM05
      amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);
      amchip_jpatt_cfg(0x3, "a", m_initThr, 0, disable_mask, 1, 1, 1, 3, 0);
      amchip_init_evt(0x3);

      // Write pattern banks
      bool is_write_pattern_success = false;
      if( m_PatternBank.getIsOK() ) {
        // take m_banktype and m_ssoffset from oks. m_banktype: 0=root file, 1=ascii, 
        // set m_ssoffset to false for now when testing pattern maAtching with root bank
        //writepatterns(npatts, 0, false, false); //temporary commented by TK.
	//TK
	//auto begin = std::chrono::high_resolution_clock::now();
 	amchip_init_evt(0x3);
 	amchip_jpatt_cfg(0x3, "a",  8, 0, disable_mask, 1, 1, 1, 3, 0); //set threshold = 8 for testing written patterns
 	amchip_jpatt_cfg(0x3, "a",  8, 0, disable_mask, 1, 1, 1, 3, 0); //set threshold = 8 for testing written patterns
 	amchip_init_evt(0x3);

      
      ftkTimeLog tLog2(ERS_HERE, name_ftk(), m_name + " HL_writebank step 2 writepatterns ");
	      is_write_pattern_success = writepatterns(npatts, 0, false, test_written_pattern, test_interval, write_pattern);
      tLog2.end(ERS_HERE);  // End log conter


 	//auto end = std::chrono::high_resolution_clock::now();
 	//ERS_LOG("time for writepatterns in AMBoard_procedures = "<<std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << " s");
 	//TK end
      }
      else{
	std::stringstream message;
	message << "AMB Pattern bank not correctly written in memory - pattern bank loading on AMB not possible";
	daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	ers::error(ex);
      }


      return is_write_pattern_success;

      // After the content is copied in memory the pattern bank is cleared
      //m_PatternBank.clearBank();
     
      //TODO to be removed S.S. 21/12/2017                                 
      //Configure_AMchips();

      // Assuming that the pattern bank has test patterns that fire with the WCSS value
      // this is set as refernce word in the DUMMY hit register
      // TODO enable when the firmware can support this
      //m_vme->write_check_word(AMB_HIT_DUMMY_HIT_WORD, (m_WCSS<<16) | m_WCSS);

    }


    /** The method, compatible with the STOP transition of the ATLAS run controller,
     * is designed to put the board in an IDLE condition, when data are not expected.
     */
    void AMBoard::RC_stop() {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), m_name + " stop");

      // Disable communication to downstream and disable communication from upstream
      // ignore HOLD and prevent HOLD to upstream
      idle_mode();

      // Flush remnant patterns from the previous setting, reset FIFOs, reset spybuffers
      cleanup();

      // Stop feeding hit in the AMB standalone mode
      if (m_DoStandaloneTest && m_testConfLoop) 
	{
	  m_vme->write_word(CONTROL_REGISTER_HIT, 0x30000);
	}

      unconfigDummyHit();
			
      // Check that the FIFOs have been cleaned
      if (!Empty_fifo())
	{
	  std::stringstream message;
	  message << "AMB FIFOs have not been correctly cleaned during the STOP procedure!";
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::error(ex);
	}
      tLog.end(ERS_HERE);  // End log conter

      return;
    }  // RC_stop()


    /** The method is to check the patterns written in the AMchips
     * npatt should be the same as the number of patterns written in the AMchips
     * interval represents the address interval with which patterns are checked (default: 512)
     */
    void AMBoard::HL_checkpatterns(int npatts, int interval) {
      
      ftkTimeLog tLog0(ERS_HERE, name_ftk(), m_name + " HL_checkpatterns step 0 ");
      const unsigned int oldChksum = m_PatternBank.getChecksum();
      std::vector<uint32_t> data={npatts};
      tLog0.end(ERS_HERE);  // End log conter

      ftkTimeLog tLog1(ERS_HERE, name_ftk(), m_name + " HL_checkpatterns step 1 CHECKSUM_update ");
      m_PatternBank.updateChecksum(1,data);
      tLog1.end(ERS_HERE);  // End log conter

      PRINT_LOG("Pbank checksum update: " << hex << oldChksum << " -> " << m_PatternBank.getChecksum() << dec);

      ftkTimeLog tLog2(ERS_HERE, name_ftk(), m_name + " HL_checkpatterns step 2 writepatterns ");
      // Prepare AMchips to load patterns 
      u_int disable_mask = 0; // enable all blocks (including Top2 if it is a AM05)
      if (m_AMChip->getNAME() == AMChip05::global()->getNAME())
        disable_mask = 0x2; // disable Top2 in AM05  

      if( m_PatternBank.getIsOK() ) 
	{
	  amchip_init_evt(0x3);
	  amchip_jpatt_cfg(0x3, "a",  8, 0, disable_mask, 1, 1, 1, 3, 0); //set threshold = 8 for testing written patterns
	  amchip_jpatt_cfg(0x3, "a",  8, 0, disable_mask, 1, 1, 1, 3, 0); //set threshold = 8 for testing written patterns
	  amchip_init_evt(0x3);
	  writepatterns(npatts, 0, false, true, interval, false);
	}

      Configure_AMchips();

      // After the content is copied in memory the pattern bank is cleared
      m_PatternBank.clearBank();

      tLog2.end(ERS_HERE);  // End log conter

      return;
    }  // HL_checkpatterns(int npatts, int interval)


    void AMBoard::idle_mode()
    {
      // Force-disable HOLD and set idle mode
      m_vme->write_check_word(DEBUG_DISCARD_OUTPUT_DATA, 0xffff);
      m_vme->write_check_word(DEBUG_FORCE_HOLD_HIT, 0xfff);
      m_vme->write_check_word(DEBUG_DISCARD_AUX_HIT, 0xfff);
      m_vme->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0xffff);

      return;
    }  // idle_mode()
     

    void AMBoard::cleanup(){

      // Flush remnant patterns from the previous setting
      amchip_init_evt(0x3);
      amchip_init_evt(0x3);
      amchip_init_evt(0x3);
      amchip_init_evt(0x3);

      // reset FIFOs
      init(2,2);

      // reset spy buffer
      // daq::ftk::ambslp_reset_spy(m_slot, 1, 1);
      // daq::ftk::ambslp_reset_spy(m_slot, 1, 1);
      // daq::ftk::ambslp_reset_spy(m_slot, 1, 1);
      resetSpyBuffers(1, 1);
      resetSpyBuffers(1, 1);  // [TBC] ??
      resetSpyBuffers(1, 1);  // [TBC] ??

      return;
    }  // cleanup()


    bool AMBoard::Empty_fifo()
    {
      ERS_LOG("Empty_fifo(): going to readout the register to check if the FIFOs are empty");
      int in_fifo  = m_vme->read_word(EMPTY_FLAG_HIT);
      //if any bit is one the corresponging FIFO is empty (12 lsbits)
      int out_fifo = m_vme->read_word(PROGFULL_EMPTY_INPUTFIFO);
     //this are the FIFOs in ROAD FPGA for data from the AMBchip
      //if any bit is on the corresponging FIFO is almost full
      //The EMPTY flags are the least significant 16 bits
      ERS_LOG("FIFO registers. IN: " << std::hex << in_fifo << "			out: " << std::hex << out_fifo );
      if(in_fifo == 0xfff && out_fifo==0xffff) 
	return true;
      else 
	return false;
    }  // Empty_fifo()


    /**********************************/
    void AMBoard::RC_publishFull(std::vector<EventFragmentCollection*> &Road, std::vector<EventFragmentCollection*> &Hit)
    /**********************************/
    {
      ftkTimeLog tLog(ERS_HERE, name_ftk(), m_name + " publishFull ");

      //++++++++++++++++++++++++++++++++++++++
      // 1  Manage spyBuffer
      //++++++++++++++++++++++++++++++++++++++
      std::vector< std::shared_ptr< AMBSpyBuffer > > spyBuffersRoad;
      std::vector< std::shared_ptr< AMBSpyBuffer > > spyBuffersHit;
  
      //++++++++++++++++++++++
      // 1.1 Read the Spy Buffer in binary format
      //++++++++++++++++++++++

      // status monitor for slice debugging
      daq::ftk::ambslp_status( m_slot, 0, "");

      freezeSpyBuffers();
      readInputSpyBuffers();
      readOutputSpyBuffers();
      unfreezeSpyBuffers();	

      //++++++++++++++++++++++
      // 1.2 Ship the spybuffer to EMonDataOut module
      //++++++++++++++++++++++
      if(getOutputSpyBufferSize() == 0)
	{ 
	  daq::ftk::ftkException e(ERS_HERE, name_ftk(), "No Road spy buffer was read out on AMBSLP.");
	  ers::warning(e);  //or throw e;  
	}
      else {
      	int spymask = 0x0;
      	for(int ii=0; ii<MAXLAMB_PER_BOARD; ii++) 
	  if(m_LAMB_mask & (1<<ii) ) spymask |= 0xF << (4*ii);
  
      	for(int ispy=0; ispy<NCHANNELS_ROAD_INPUT; ispy++)
	  if(spymask&(1<<ispy)) spyBuffersRoad.push_back(getOutputSpyBuffer(ispy));
      	shipSpyBuffer(spyBuffersRoad, false);
      }
  
      if(getInputSpyBufferSize() == 0)
	{
	  daq::ftk::ftkException e(ERS_HERE, name_ftk(), "No Hit spy buffer was read out on AMBSLP.");
	  ers::warning(e);  //or throw e;  
	}
      else {
      	for(int ispy=0; ispy<NCHANNELS_HIT_INPUT; ispy++)
	  spyBuffersHit.push_back(getInputSpyBuffer(ispy));
      	shipSpyBuffer(spyBuffersHit);
      }
      ERS_LOG("Spybuffer shipped");

      //AM06 link error monitoring
      /*
      std::vector<std::vector<unsigned int>> errorDout32_count;
      std::vector<std::vector<unsigned int>> tx_stream_error;
      std::vector<std::vector<unsigned int>> rx_stream_error;
      errorDout32_count.resize(MAXAMCHIPS);
      tx_stream_error.resize(MAXAMCHIPS);
      rx_stream_error.resize(MAXAMCHIPS);
      for(int i=0; i<MAXAMCHIPS; i++){
	errorDout32_count[i].resize(NLAYERS);
	tx_stream_error[i].resize(NLAYERS);
	rx_stream_error[i].resize(NLAYERS);
      }
      
      amchip8b10b_count_errDout32(errorDout32_count, rx_stream_error, tx_stream_error, 0x1, 2, 3);
      
      for(int i=0; i<MAXAMCHIPS; i++){
	for(int j=0; j<NLAYERS; j++){
	  ERS_LOG("Chip "<<i<<" bus "<<j<<": errDout32 = 0x"<< std::hex << errorDout32_count[i][j] << "  RxStreamError = "<< rx_stream_error[i][j] <<"  TxStreamError = "<<tx_stream_error[i][j]);
	}
      }
      */
      //AM06 link error monitoring end

      tLog.end(ERS_HERE);  // End log conter

      return;
    }  // RC_publishFull(std::vector<EventFragmentCollection*> &Road, std::vector<EventFragmentCollection*> &Hit)


    /**********************************/
    void AMBoard::shipSpyBuffer(std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > > & vecSpyBuffers, bool isHit)
    /**********************************/
    {
      // Protection for m_ftkemonDataOut pointer
      if(!m_ftkemonDataOut)
      	return;
      ERS_LOG("ReadoutModule_PU::shipSpyBuffer: SLOT = " << std::to_string(m_slot));
      //total number of words contained in the SpyBuffers
      uint32_t num_words = 0;

      //vector of the pairs to be shipped
      std::vector< std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > > vec_pairs;

      for(uint32_t isb = 0; isb < vecSpyBuffers.size(); isb++)
	{
	  //creating the source ID for the SpyBuffer
	  daq::ftk::Position position;
	  if(isHit) position = daq::ftk::Position::IN;
	  else position = daq::ftk::Position::OUT;

	  uint32_t sourceid = daq::ftk::encode_SourceIDSpyBuffer(daq::ftk::BoardType::AMB, getSlot(), isb, position);

	  num_words += vecSpyBuffers[isb]->getBuffer().size();
	  //creating the pair
	  std::pair< std::shared_ptr< daq::ftk::SpyBuffer >, uint32_t > pair2ship = std::make_pair( vecSpyBuffers[isb], sourceid);
  
	  ERS_LOG("Push back spybuffer lane = " << isb);
	  vec_pairs.push_back(pair2ship);
    	}
      
      // Make SpyBuffers available to FtkEMonDataOut
      ERS_LOG("Make SpyBuffers available to FtkEMonDataOut ...");
      m_ftkemonDataOut->sendData(vec_pairs);
      ERS_LOG("SpyBuffers total number of words: " << num_words << " were made available to FtkEMonDataOut" );  

      return;
    }  // shipSpyBuffer(std::vector< std::shared_ptr< daq::ftk::AMBSpyBuffer > > & vecSpyBuffers, bool isHit)


    /**********************************/
    void AMBoard::RC_checkLinks()
    /**********************************/
    {
      ERS_LOG("checking AMB links....");      

      // set DCS server semaphore red (stop DCS monitoring)
      ERS_LOG("Stopping DCS monitoring during connect");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0xCAFE);
      
      //checking AM06 links
      if(not amchip8b10b_up_and_check(0x3, 1, 2, 3, false)){
	std::stringstream message;
	message << "Not all AM06 input links are up.";
	if(m_nAM06LinkErrors%10==0) ERS_LOG(message.str());
	if(m_nAM06LinkErrors<1) {
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::warning(ex);
	  //ers::info(ex);
	}
	m_nAM06LinkErrors++;
      }
      
      //checking HIT FPGA links
      if( not ((m_vme->read_word(HITRESETDONEGTP)==0xffffffff) && ((m_vme->read_word(HITALIGNGTP)&0xbde7)==0xbde7)) ) {
	std::stringstream message;
	message << "Not all HIT FPGA links are up.";
	if(m_nHITLinkErrors%10==0) {
	  ERS_LOG(message.str());
	  const int HIT_active_links = 0xbde7; //0xbde7 are the 12 HIT input links
	  bool HIT_GTP_OK =  check_HIT_GTP_aligned(HIT_active_links);
	}
	if(m_nHITLinkErrors<1) {
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::warning(ex);
	  //ers::info(ex);
	}	
	m_nHITLinkErrors++;
      } else {
	ERS_LOG("ALL HIT FPGA links are up.");
      }

      //checking ROAD FPGA links
      if( not ((m_vme->read_word(ROADRESETDONEGTP)==0xffffffff) && (m_vme->read_word(ROADALIGNGTP)==0xffff)) ) {
	std::stringstream message;
	message << "Not all ROAD FPGA links are up.";
	if(m_nROADLinkErrors%10==0) {
	  ERS_LOG(message.str());
	  // check the status, if they aren't ok
	  int ROAD_active_links = 0xffffffff;
	  unsigned int register_content = check_ROAD_GTP_aligned(ROAD_active_links);
	}
	if(m_nROADLinkErrors<1) {
	  daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	  ers::warning(ex);
	  //ers::info(ex);
	}	
	m_nROADLinkErrors++;
      } else {
	ERS_LOG("ALL ROAD FPGA links are up.");
      }

      // set DCS server semaphore green (restart DCS monitoring)
      ERS_LOG("Re-starting DCS monitoring after configure");
      m_vme->write_check_word(AMB_CONTROL_DCS_RUN, 0x0000);

      return;
    }  // RC_checkLinks()

  }
}
