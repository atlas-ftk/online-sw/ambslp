/* AMfeed.cpp
 
   Author: Francesco Crescioli
   Date: 11/4/08

   rev MP 26.07.2010

   Based on AMsim by
   Author: Alberto Annovi
   Date  : October 14, 2004

*/


#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"
#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


using namespace std;

int main(int argc, char **argv) 
{ 
	
	using namespace  boost::program_options ;
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("hit_file", value< vector<string> >()->required(),
         "Hit Pattern file (can be specified in any parameter position, also omitting \"--hit_file\")")
    ("slot", value< string >()->default_value("15"), "The card slot")
    ("loopfifovme", value< string >()->default_value("0"), "The option enable the loop of the VME fifo")
    ("verbose","Verbosity level, more times you add the option more verbose it gets")
    ;

	positional_options_description pd; 
	pd.add("hit_file",-1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
  {
    std::cerr << desc << std::endl;
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
  {
    std::cout << std::endl <<  desc << std::endl ;
    return 0;
  }

  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int loopfifovme = daq::ftk::string_to_int( vm["loopfifovme"].as<std::string>() );
  int verbosity = vm.count("verbose");

  if (vm.count("hit_file")==1) {
  	// single file format
  	std::string hitfname = vm["hit_file"].as<vector<string>>().front() ;
  	daq::ftk::assert_string_parameter_not_empty( hitfname, std::string("hitfname") ); // checking that is not empty

        daq::ftk::AMBoard myAMB(slot, verbosity>2 ? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::WARNING);

        myAMB.init(0); // reset FIFOs and patternX registers
        myAMB.init(2); // reset FIFOs and patternX registers  

        myAMB.feed_hit_init(); // prepare the HIT FIFO loading
        myAMB.feed_hit(hitfname.c_str());
        myAMB.feed_hit_end(loopfifovme!=0); // send the data
        return 0;
        //return daq::ftk::ambslp_feed_hit( slot, loopfifovme, hitfname.c_str(), verbosity );
  }
  else if(vm.count("hit_file")==8) {
  	return -1;
  }else return -2;
  //
  ///////////////////////////////////////////


}



