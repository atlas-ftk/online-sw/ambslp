
#include "ambslp/ambslp_card.h"
#include "ambslp/ambslp_hw.h"
#include "ambslp/ambslp_settings.h"

#include "ambslp/exceptions.h"
#include "ftkvme/VMEManager.h"
#include "ftkvme/VMEInterface.h"

#include <sstream>

using namespace daq::ftk;




ambslp_card::ambslp_card( ambslp_settings* settings ) : m_vme(0)
{

  // 1. check the settings: if invalid, throw exception
  if ( !settings ) {
    daq::ftk::ftkException issue( ERS_HERE, name_ftk(), "Input settings for ambslp are not valid");
    throw issue;
  }

  // 2. create a copy of the settings, since we do not want outside clients to change them
  m_settings.reset( new ambslp_settings() );
  m_settings->crate = settings->crate;
  m_settings->slot = settings->slot;
   

  // 2. try to connect to vme, using info in settings; 
  //      if problems are found throw exception
  try { m_vme = VMEManager::global().amb( m_settings->slot ); }
  catch ( ... ) {

    std::ostringstream errString;
    errString << "VME access via interface manager has failed for ambslp: ";
    errString << "cannot connect to AMBoard in slot " << m_settings->slot;
    errString << " in crate " << m_settings->crate;
    daq::ftk::ftkException issue( ERS_HERE, name_ftk(), errString.str() );
    throw issue;
  }


  // 2. discover hardware; if problems are found, close vme and throw exception
  try {  m_hardware.reset( new ambslp_hw()); /*discover_amb_hardware();*/ }
  catch ( ers::Issue &e ) {
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), "Hardware-discovery failed in ambslp_card constructor");
    ERS_INFO (" closing vme connection for slot" << m_settings->slot << 
        " in crate " << m_settings->crate << "!" );
    try { m_vme->close(); }
    catch ( daq::ftk::VmeError &ex ) {
      std::ostringstream errString;
      errString << "Problems in closing the vme connection for slot ";
      errString << m_settings->slot << " in crate " << m_settings->crate; 
      daq::ftk::ftkException issue_on_close( ERS_HERE, name_ftk(), errString.str() );
      throw issue_on_close;
    }
    throw issue;
  }
  
  // if here, settings, vme connection and hardware discovery are supposed to be ok
  // inform the DAQ in the LOG file
  ERS_LOG ( "Ambslp card correctly initialized, " << //
      "vme connection established and hardware configurations " << //
      "extracted for card in slot " << m_settings->slot << //
      " in crate " << m_settings->crate );
}


ambslp_card::~ambslp_card() {
}


bool ambslp_card::isConnected() {

  return m_vme->is_open();
}



void ambslp_card::setup() {

  sleep(2);
}



void ambslp_card::configure() {

  sleep(2);
}



void ambslp_card::connect() {

  sleep(2);
}



void ambslp_card::prepareForRun() {

  sleep(2);
}



void ambslp_card::cloneSetup( ambslp_settings* settings, ambslp_hw * hardware ) {

  if ( !settings ) { 
    ERS_INFO ( "ambslp_settings object is not valid.");
    daq::ftk::ftkException issue( ERS_HERE, name_ftk(), "invalid ambslp_settings object passed." );
    throw issue;
  }


  if ( !hardware ) { 
    ERS_INFO ( "ambslp_hardware object is not valid.");
    daq::ftk::ftkException issue( ERS_HERE, name_ftk(), "invalid ambslp_hardware object passed." );
    throw issue;
  }

  // if here, the two inputs should be valid
  m_hardware->cloneInto( hardware );
  m_settings->cloneInto( settings );


}
