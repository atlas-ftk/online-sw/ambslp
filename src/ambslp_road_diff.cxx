/* road_diff.cpp

   Author M. Bitossi 2004
   this program replaces with zeros a few patterns
 */

//the program takes two data files, one is a reference file, the other is the measured file

//both are in the two line fields: address in even index, layer is in uneven index

//the file are sorted increasing use the standard function qsort()

//compare one to one and in case of equal print on video road composite by Address and Layer

//else print on video an error message,the reference and measured roads 

#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

int main(int argc, char **argv) 
{ 

	using namespace  boost::program_options ;
        ///////////////////////////////////////////
        //  Parsing parameters using namespace boost::program:options
        //
        options_description desc("Allowed options");
        desc.add_options()
        ("help", "produce help message")
        ("laymap", value< std::string >()->default_value("0"), "use single word road format (i.e. no bitmap)")
        ("skip64", value< std::string >()->default_value("0"), "skip events with more than 64 roads")
        ("ignore_bits,i", value< std::string >()->default_value("0x00000"), "ignore address and layermap bits")
        ("kill_file,rw", value< std::string >()->default_value(""), "kill file name with info from RWsim")
        ("simulation_file", value< std::string >()->default_value(""), "Simulation file (can be specified in any parameter position, also omitting \"--simulation_file\". The first unspecified parameter will be considered)")
        ("amb_file", value< std::string >()->default_value(""), "Amb file (can be specified in any parameter position, also omitting \"--amb_file\". The second unspecified parameter will be considered)")
        ;

	positional_options_description pd; 
	pd.add("simulation_file", 1);
	pd.add("amb_file", -1);

        variables_map vm;
        try {
                store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
        }
        catch( ... ) // In case of errors during the parsing process, desc is printed for help 
        {
                std::cerr << desc << std::endl;
                return 1;
        }

        notify(vm);

        if( vm.count("help") ) // if help is required, then desc is printed to output
        {
                std::cout << std::endl <<  desc << std::endl ;
                return 0;
        }


    int ignore_bits = daq::ftk::string_to_int( vm["ignore_bits"].as<std::string>() );
	int laymap = daq::ftk::string_to_int( vm["laymap"].as<std::string>());
	int skip64 = daq::ftk::string_to_int( vm["skip64"].as<std::string>());
	std::string meas_fname = vm["amb_file"].as<std::string>();
	std::string rif_fname = vm["simulation_file"].as<std::string>();
	std::string kill_fname = vm["kill_file"].as<std::string>();
	daq::ftk::assert_string_parameter_not_empty( meas_fname, std::string("amb_file") ); // checking that is not empty
	daq::ftk::assert_string_parameter_not_empty( rif_fname, std::string("simulation_file") ); // checking that is not empty

 	//
	//////////////////////
	
  return daq::ftk::ambslp_road_diff( ignore_bits, laymap, skip64, rif_fname.c_str(), meas_fname.c_str(), kill_fname.c_str() );
}

