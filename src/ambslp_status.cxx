

#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

// IS includes
#include <is/info.h>
#include <is/inforeceiver.h>
#include <is/infodynany.h>
#include <ipc/core.h>

int main(int argc, char **argv) 
{ 
	using namespace  boost::program_options ;

	///////////////////////////////////////////
	//  Parsing parameters using namespace boost::program:options
	//
	options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("slot,s", value< std::string >()->default_value("15"), "The card slot. If reading from IS assumes device ID is Amb{slot}.")       	
		("verbose_level,v", value< std::string >()->default_value("1"),"0=RunControl, 1=StandAlone")
		("partition,p", value< std::string >()->default_value(""), "Partition name. If set, will try to read info from IS.")
		;

	variables_map vm;
	try {
		store(command_line_parser(argc, argv).options(desc).run(), vm);
	}
	catch( ... ) // CHecing for error during the parsing process, desc is for help 
	{
		std::cerr << desc << std::endl; 
		return 1;
	}

	notify(vm);

	if( vm.count("help") ) // if required printing help
	{	
		std::cout << std::endl <<  desc << std::endl ; 
		return 0;	
	}

	int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
	int verbose_level = daq::ftk::string_to_int( vm["verbose_level"].as<std::string>() );
	std::string is_part_name = vm["partition"].as<std::string>();

	if(!is_part_name.empty()){
		//initialize communication library (= ipc)
		IPCCore::init(argc,argv);
	}

	return daq::ftk::ambslp_status( slot, verbose_level, is_part_name);

}

