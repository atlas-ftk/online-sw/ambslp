#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

//----------------------------
//Simone, Include for VME
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ambslp/ambslp.h"
#include "ambslp/exceptions.h"

#include "ambslp/ambslp_vme_jtag_func.h"
#include "ambslp/ambslp_jtag_func.h"

//----------------------------

static int jtag_state;
static int verbose;
static int lamb;
static VMEInterface *vme = 0;

//
// JTAG state machine.
//

enum
{
	test_logic_reset, run_test_idle,

	select_dr_scan, capture_dr, shift_dr,
	exit1_dr, pause_dr, exit2_dr, update_dr,

	select_ir_scan, capture_ir, shift_ir,
	exit1_ir, pause_ir, exit2_ir, update_ir,

	num_states
};

static int jtag_step(int state, int tms)
{
	static const int next_state[num_states][2] =
	{
		[test_logic_reset] = {run_test_idle, test_logic_reset},
		[run_test_idle] = {run_test_idle, select_dr_scan},

		[select_dr_scan] = {capture_dr, select_ir_scan},
		[capture_dr] = {shift_dr, exit1_dr},
		[shift_dr] = {shift_dr, exit1_dr},
		[exit1_dr] = {pause_dr, update_dr},
		[pause_dr] = {pause_dr, exit2_dr},
		[exit2_dr] = {shift_dr, update_dr},
		[update_dr] = {run_test_idle, select_dr_scan},

		[select_ir_scan] = {capture_ir, test_logic_reset},
		[capture_ir] = {shift_ir, exit1_ir},
		[shift_ir] = {shift_ir, exit1_ir},
		[exit1_ir] = {pause_ir, update_ir},
		[pause_ir] = {pause_ir, exit2_ir},
		[exit2_ir] = {shift_ir, update_ir},
		[update_ir] = {run_test_idle, select_dr_scan}
	};

	return next_state[state][tms];
}

static int sread(int fd, void *target, int len)
{
  unsigned char *t = (unsigned char *) target;
	while (len)
	{
		int r = read(fd, t, len);
		if (r <= 0)
			return r;
		t += r;
		len -= r;
	}
	return 1;
}

//
// handle_data(fd) handles JTAG shift instructions.
//   To allow multiple programs to access the JTAG chain
//   at the same time, we only allow switching between
//   different clients only when we're in run_test_idle
//   after going test_logic_reset. This ensures that one
//   client can't disrupt the other client's IR or state.
//
int handle_data(int fd, VMEInterface *vme)
{
	int i;
	int seen_tlr = 0;

	do
	{
		char cmd[16];
                const int MAX_BUFFER = 1000000;
		unsigned char buffer[2*MAX_BUFFER], result[MAX_BUFFER];
		
		if (sread(fd, cmd, 6) != 1) {
			return 1;
		}

		if (memcmp(cmd, "shift:", 6) && memcmp(cmd, "shift;", 6))
		{
			cmd[6] = 0;
			fprintf(stderr, "invalid cmd '%s'\n", cmd);
			return 1;
		}

                bool returnTDO = true;
                // if the command is "shift;" we must not return TDO
                if (cmd[5] == ';') 
                  returnTDO = false;

		
		int len;
		if (sread(fd, &len, 4) != 1)
		{
			fprintf(stderr, "reading length failed\n");
			return 1;
		}
		
		int nr_bytes = (len + 7) / 8;
		if (nr_bytes * 2 > (int)sizeof(buffer))
		{
			fprintf(stderr, "buffer size exceeded\n");
			return 1;
		}
		
		if (sread(fd, buffer, nr_bytes * 2) != 1)
		{
			fprintf(stderr, "reading data failed\n");
			return 1;
		}
		
		memset(result, 0, nr_bytes);

		if (verbose)
		  {
		    printf("len: %d\n", len);
		    printf("tms: ");
		    for (i = 0; i < nr_bytes ; ++i) {
		      if (i==0) printf("%d\t", nr_bytes);
		      printf("%02x ", buffer[i]);
		    }
		    printf("\n");
		    printf("tdi: ");
		    for (i = nr_bytes; i < nr_bytes * 2; ++i) {
		      if (i==nr_bytes) printf("%d\t", nr_bytes);
		      printf("%02x ", buffer[i]);
		    }
		    printf("\n");
		  }


		//
		// Only allow exiting if the state is rti and the IR
		// has the default value (IDCODE) by going through test_logic_reset.
		// As soon as going through capture_dr or capture_ir no exit is
		// allowed as this will change DR/IR.
		//
		seen_tlr = (seen_tlr || jtag_state == test_logic_reset) && (jtag_state != capture_dr) && (jtag_state != capture_ir);
		
		
		//
		// Due to a weird bug(??) xilinx impacts goes through another "capture_ir"/"capture_dr" cycle after
		// reading IR/DR which unfortunately sets IR to the read-out IR value.
		// Just ignore these transactions.
		//
		
		if ((jtag_state == exit1_ir && len == 5 && buffer[0] == 0x17) || (jtag_state == exit1_dr && len == 4 && buffer[0] == 0x0b))
		{
			if (verbose)
				printf("ignoring bogus jtag state movement in jtag_state %d\n", jtag_state);
		} else
			for (i = 0; i < len; ++i)
			{
				//
				// Do the actual cycle.
				//
				
				int tms = !!(buffer[i/8] & (1<<(i&7)));
				int tdi = !!(buffer[nr_bytes + i/8] & (1<<(i&7)));
                                u_int regcont;

				if (lamb) { // access LAMBs
                                  if (returnTDO)
                                    regcont = vme->read_word(AMB_VME_TDO_LAMB);
                                  else regcont = 0;

				  result[i / 8] |= (regcont&0x1) << (i&7);
				  //				  vme->write_word(ENABLE_TCK, 0x0);
				  //				  vme->write_word(TMS_LAMB, tms);
				  //				  vme->write_word(AMB_VME_TDITMS_LAMB, tdi + (tms<<1) );
				  //				  vme->write_word(ENABLE_TCK, 0x1);
				  vme->write_word(AMB_VME_TDITMS_LAMB, tdi + (tms<<1) );
				} else { // access AMB main board
                                  if (returnTDO)
                                    regcont = vme->read_word(TDO_FPGA);
                                  else regcont = 0;
				  result[i / 8] |= (regcont&0x1) << (i&7);
				  vme->write_word(ENABLE_TCK, 0x0);
				  vme->write_word(TMS_FPGA, tms);
				  vme->write_word(ENABLE_TCK, 0x1);
				  vme->write_word(TDI_FPGA, tdi);
				}

				//
				// Track the state.
				//
				jtag_state = jtag_step(jtag_state, tms);
			}

		if (write(fd, result, nr_bytes) != nr_bytes)
		{
			perror("write");
			return 1;
		}
		
		if (verbose)
		{
			printf("jtag state %d\n", jtag_state);
			printf("tdo: ");
                        for (i = 0; i < nr_bytes; ++i) {
			  if (i==0) printf("%d\t", nr_bytes);
			  printf("%02x ", result[i]);
			}
			printf("\n\n");

		}
	} while (!(seen_tlr && jtag_state == run_test_idle));
	return 0;
}

void intHandler(int dummy) {
  // release JTAG chain. Control goes to external JTAG connector
  printf("Release control of AMB and LAMBs FPGA JTAG chains from VME FPGA\n");
  vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);	
}

int main(int argc, char **argv)
{
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("port", value<unsigned int>()->default_value(2543), "Listen to this port")
    ("slot", value<unsigned int>()->default_value(15), "Use AMB in slot")
    ("lamb,l", "If present LAMB FPGAs will be accessed instead of main board FPGAs")
    ("verbose,v", "If present verbose option will be used")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      FTK_VME_ERROR( "- ambslp_xvc.cxx : Failure in command_line_parser" );
      return 1;
    }

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      ERS_LOG( desc ); 
      return 0;	
    }

  int port = vm["port"].as<unsigned int>();
  int slot = vm["slot"].as<unsigned int>();
  verbose = vm.count("verbose");
  lamb = vm.count("lamb");

  printf("port = %d\n", port);
  printf("slot = %d\n", slot);
  printf("verbose = %d\n", verbose);
  printf("lamb = %d\n", lamb);

  if (lamb)
    printf("Accessing LAMB FPGAs\n\n");
  else
    printf("Accessing main board FPGAs\n\n");

  signal(SIGINT, intHandler);

	int i;
	int s;
	// int c;
	struct sockaddr_in address;
	
        // u_int ret;

        // try to connect to the amb via vme
        vme = VMEManager::global().amb(slot);  

        //try {  vme = VMEManager::global().amb(slot);  }
        //catch (daq::ftk::VmeError &ex) {
        //  daq::ftk::Issue_ambslp issue(ERS_HERE, "VME access via interface manager has failed for ambslp: cannot connect to AMBoard");
        //  throw issue;
        //}

                        
	printf("Taking control of AMB and LAMBs FPGA JTAG chains from VME FPGA\n");
        vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
	if (lamb) // enable TCK onle once! Here!
	  vme->write_word(ENABLE_TCK, 0x1);
	
	//
	// Listen on port.
	//
	
	s = socket(AF_INET, SOCK_STREAM, 0);
	
	if (s < 0)
	{
		perror("socket");
		return 1;
	}
	
	i = 1;
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &i, sizeof i);
	
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);
	address.sin_family = AF_INET;
	if (bind(s, (struct sockaddr*)&address, sizeof(address)) < 0)
	{
		perror("bind");
		return 1;
	}
	
	if (listen(s, 0) < 0)
	{
		perror("listen");
		return 1;
	}
	
	fd_set conn;
	int maxfd = 0;
	
	FD_ZERO(&conn);
	FD_SET(s, &conn);
	
	maxfd = s;
	
	while (1)
	{
		fd_set read = conn, except = conn;
		int fd;
		
		//
		// Look for work to do.
		//
		
		if (select(maxfd + 1, &read, 0, &except, 0) < 0)
		{
			perror("select");
			break;
		}
		
		for (fd = 0; fd <= maxfd; ++fd)
		{
			if (FD_ISSET(fd, &read))
			{
				//
				// Readable listen socket? Accept connection.
				//
				
				if (fd == s)
				{
					int newfd;
					socklen_t nsize = sizeof(address);
					
					newfd = accept(s, (struct sockaddr*)&address, &nsize);
					if (1 || verbose)
						printf("connection accepted - fd %d\n", newfd);
					if (newfd < 0)
					{
						perror("accept");
					} else
					{
						if (newfd > maxfd)
						{
							maxfd = newfd;
						}
						FD_SET(newfd, &conn);
					}
				}
				//
				// Otherwise, do work.
				//
				else if (handle_data(fd, vme))
				{
					//
					// Close connection when required.
					//
					
					if (1 || verbose)
						printf("connection closed - fd %d\n", fd);
					close(fd);
					FD_CLR(fd, &conn);
				}
			}
			//
			// Abort connection?
			//
			else if (FD_ISSET(fd, &except))
			{
				if (verbose)
					printf("connection aborted - fd %d\n", fd);
				close(fd);
				FD_CLR(fd, &conn);
				if (fd == s)
					break;
			}
		}
	}


        // call intHandler just before exiting to release the JTAG chain
        intHandler(0); 
	
	return 0;
}
