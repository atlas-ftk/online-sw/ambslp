// functions for StatusRegisterAMBCollection.h

#include "ambslp/StatusRegisterAMBCollection.h"
#include "ambslp/StatusRegisterAMBSelector.h"


namespace daq {
namespace ftk {

StatusRegisterAMBCollection::StatusRegisterAMBCollection(
	uint vmeSlot,
	uint fpgaNum,
	uint firstAddress,
	uint finalAddress,
	uint addrIncrement,
	std::string collectionName,
	std::string collectionShortName,
	uint selectorAddress,
	uint readerAddress,
	srType type,
	srAccess access
	) : StatusRegisterVMECollection (vmeSlot, fpgaNum, firstAddress, finalAddress, addrIncrement, collectionName, collectionShortName, 
		selectorAddress, readerAddress, type, access) 
{
	// Loop through addresses, creating a SRAMBSelector object if requested by access, the rest are created in StatusRegisterVMECollection constructor
	if( access == srAccess::AMB_selector ) {
		for (uint addr = firstAddress; addr <= finalAddress; addr+=addrIncrement) {
			std::stringstream buffer;
			buffer << std::hex << addr << "_" << selectorAddress << "_" << readerAddress;

			// Create a SRAMBSelector object for each address
			std::unique_ptr<StatusRegister> srVME = std::make_unique<StatusRegisterAMBSelector>(vmeSlot, fpgaNum, buffer.str(), addr, type, selectorAddress, readerAddress);
			// Add the object to the SR collection
			addStatusRegister(std::move(srVME));
		}
	}
}

StatusRegisterAMBCollection::~StatusRegisterAMBCollection() {
	// The StatusRegisterCollection destructor deletes the StatusRegisters stored in m_registers, so no need to do it here
}

} // namespace ftk
} // namespace daq
