/*****************************************************************/
/*                                                               */
/* Author: M.Franchini                                           */
/*                                                               */
/*                                                               */
/* 8 March 2011: General purpose Module to handle a CFD          */
/*                                                               */
/*****************************************************************/

#include "ambslp/ReadoutModuleAmbslp.h"

using namespace daq::ftk;
using namespace std;

/**********************/
ReadoutModule_Ambslp::ReadoutModule_Ambslp() :
  m_CanPublish(false)
  /**********************/
{ 
#include "cmt/version.ixx"
  ERS_LOG("ReadoutModule_Ambslp::constructor: Entered"       << std::endl
	  << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl
	  << cmtversion );

  m_configuration = 0;
  ERS_LOG("ReadoutModule_Ambslp::constructor: Done");
}

/***********************/
ReadoutModule_Ambslp::~ReadoutModule_Ambslp() noexcept
/***********************/
{
  ERS_LOG("ReadoutModule_Ambslp::destructor: Done");  
}

/************************************************************/
void ReadoutModule_Ambslp::setup(DFCountedPointer<ROS::Config> configuration)
/************************************************************/
{
  ERS_LOG("ReadoutModule_Ambslp::setup: Entered");
  if (daq::ftk::isLoadMode())
    { ERS_LOG("Loading mode"); }

  m_CanPublish = false;
  
  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  std::string appName = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

  // Read some info from the DB
  const ftk::dal::ReadoutModule_Ambslp * module_dal = conf->get<ftk::dal::ReadoutModule_Ambslp>(configuration->getString("UID"));
  m_name = module_dal->get_Name();
  m_slot = module_dal->get_Slot();
  m_isServerName = readOutConfig->get_ISServerName(); 
  m_dryRun = module_dal->get_DryRun();

  ERS_LOG("ReadoutModule_Ambslp::setup: SLOT = " << std::to_string(m_slot));  

  ERS_LOG("Creating the AMB object in slot " << std::to_string(m_slot));
  m_AMB=std::make_unique<AMBoard>(m_slot);

  // Calling the Setup function defined in AMBoard_procedure (reading the data member values from OKS)
  m_AMB->RC_setup(module_dal);

  if(m_dryRun)
    { 
      std::stringstream message;
      message << "ReadoutModule_Ambslp.DryRun option set in DB, IPBus calls with be skipped!";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);      
    };
  
  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try 
    { 
      //FIXME: remove this hack when migration to cmake is complete
      m_ambslpNamed=std::make_unique<ambslpNamed>( m_ipcpartition, isProvider.c_str()); 
    }
  catch( ers::Issue &ex) 
    {
      daq::ftk::ISException e(ERS_HERE, "Error while creating ambslpNamed object", ex);  // NB: append the original exception (ex) to the new one 
      ers::warning(e);  //or throw e;  
    }

  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try { 
    m_ohProvider=std::make_unique<OHRootProvider> ( m_ipcpartition , OHServer, OHName); 
    m_ohRawProvider=std::make_shared<OHRawProvider<>>( m_ipcpartition , OHServer, getDFOHNameString(m_name, "AMB", -1, m_slot) ); 
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << appName);
  }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception. 
      daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
      ers::warning(e);  //or throw e;  
    }
  m_dfReaderAmb=std::make_unique<DataFlowReaderAMB>(m_ohRawProvider, true);
  m_dfReaderAmb->init(m_name);
  
  // Construct the histograms
  // spy buffer histograms
  m_hlast_lvl1id=std::make_unique<TH1F>("AmbLastL1id:1d",        "Last LVL1ID in the AMB spy buffer",   128, -0.5, 127.5);
  m_hnroads=std::make_unique<TH1F>("AmbNRoads:1d",             "Number of roads per spy buffer", 50, -0.5, 49.5);
  m_hgeoaddress=std::make_unique<TH1F>("AmbGeoAddress:1d",         "Geographical address of roads",  64, -0.5, 63.5);
  m_hnhits=std::make_unique<TH1F>("AmbNHits:1d",             "Number of hits per spy buffer", 500, -0.5, 499.5);  

  ERS_LOG("ReadoutModule_Ambslp::setup: Done");

}

unsigned int countBits(unsigned int data) {
  int count=0;
  for (unsigned int i=0; i<sizeof(unsigned int)*8; i++) 
    if ( (data>>i)&0x1 ) count++;
  return count;
}

/********************************/
void ReadoutModule_Ambslp::configure(const daq::rc::TransitionCmd& cmd)
/********************************/
{  
  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString());
  ERS_LOG("ReadoutModule_Ambslp::configure: SLOT = " << std::to_string(m_slot));  
  // In dryRun mode bypass all the vme commands
  if(m_dryRun)
    {
      ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " [dryRun] done");
      return;
    }
  try
    {
      m_CanPublish = false;
      // High level configuration of the board
      m_AMB->RC_configure();
    }
  catch( daq::ftk::VmeError & err )
    {
      throw daq::ftk::VmeError(ERS_HERE, "ReadoutModule_Ambslp::configure: Error when initializing AmBoard at slot : " + m_slot );
    }

  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " done");

}

/**************************/
void ReadoutModule_Ambslp::unconfigure(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString());
  ERS_LOG("ReadoutModule_Ambslp::unconfigure: SLOT = " << std::to_string(m_slot));  

  if(m_dryRun)
    {
      ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " [dryRun] done");
      return;
    }
  
  try
    {
      m_CanPublish = false;      
      m_AMB->RC_unconfigure();
    }
  catch( daq::ftk::VmeError & err )
    {
      throw daq::ftk::VmeError(ERS_HERE, "ReadoutModule_Ambslp::configure: Error when initializing AmBoard at slot : " + m_slot );
    } 

  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " done");
}


/**************************/
void ReadoutModule_Ambslp::connect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString());
  ERS_LOG("ReadoutModule_Ambslp::connect: SLOT = " << std::to_string(m_slot));  
  // In dryRun mode bypass all the vme commands
  if(m_dryRun)
    {
      ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " [dryRun] done");
      return;
    }
  try
    {	 
      // prepare the connection using a common high level function
      if (m_AMB->RC_connect())
	{
	  // //Rising a Fatal in case the reset of AUX doesn't work
	  // std::stringstream message;
	  // message << "AMB link alignment failed during connect procedure - Final message!";
	  // daq::is::Exception ex(ERS_HERE, message.str());
	  // ers::fatal(ex);

	  //Rising an error in case the reset of AUX doesn't work
	  std::stringstream message;
	  message << "AMB link alignment failed during connect procedure - Final message!";
	  daq::is::Exception ex(ERS_HERE, message.str());
	  ers::error(ex);
	}
    }
  catch( daq::ftk::VmeError & err )
    {
      throw daq::ftk::VmeError(ERS_HERE, "ReadoutModule_Ambslp::configure: Error when initializing AmBoard at slot : " + m_slot );
    }
  //ML create all required StatusRegisterCollection objects here.
  m_statusRegisterAMBFactory=std::make_unique<StatusRegisterAMBFactory>(m_name, m_slot, "VHRC", m_isServerName, m_ipcpartition);
  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " done");
}


/**************************/
void ReadoutModule_Ambslp::disconnect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString());
  ERS_LOG("ReadoutModule_Ambslp::disconnect: SLOT = " << std::to_string(m_slot));  
  // Fill ME
  ERS_LOG("ReadoutModule_Ambslp: "  << cmd.toString() << " done");
}


/**********************************/    
void ReadoutModule_Ambslp::prepareForRun(const daq::rc::TransitionCmd& cmd)
/**********************************/    
{
  ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString());
  ERS_LOG("ReadoutModule_Ambslp::prepareForRun: SLOT = " << std::to_string(m_slot));  
  // Reset histograms
  m_hlast_lvl1id->Reset();
  m_hnroads->Reset();
  m_hgeoaddress->Reset();
  m_hnhits->Reset();

  // In dryRun mode bypass all the vme commands
  if(m_dryRun)
    {
      ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString() << " [dryRun] done");
      return;
    }
  try
    {	 
      m_AMB->RC_prepareForRun();
      m_CanPublish = true;
      // 2. Enable communication from upstream
      // enable sending HOLD upstream
      //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_HIT, 0x0);
      // start accepting data from upstream (stable init OFF)
      //daq::ftk::ambslp_stable_init_off(m_slot, 1);
    }
  catch( daq::ftk::VmeError & err )
    {
      throw daq::ftk::VmeError(ERS_HERE, "ReadoutModule_Ambslp::configure: Error when initializing AmBoard at slot : " + m_slot );
    }

  ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString() << " done");
}

/**************************/
void ReadoutModule_Ambslp::stopRecording(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString());
  ERS_LOG("ReadoutModule_Ambslp::stopDC: SLOT = " << std::to_string(m_slot));  

  // Fill ME
  // 1. Publish in IS/OH
  // last standard publication and, if needed, additional end of run informatoin

  // 2. Disable communication to downstream
  // prevent data fro being sent
  //ignore HOLD
  //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_AUX, 0xffff);

  // 3. disable communication from upstream
  // prevent HOLD to upstream
  //m_AMB->getVME()->write_check_word(DEBUG_DISABLE_HOLD_HIT, 0xfff);
  //ignore data

  // 4. Setup high speed links
  // so that all TX links are sending IDLE
  // Reset GTPs
  //m_AMB->reset_gtp(19, false);
  // Setup SERDES of AMchips
  //m_AMB->amchip8b10b();
  //m_AMB->amchip8b10b();
  // Reset GTPs again
  //m_AMB->reset_gtp(19, false);

  m_CanPublish = false;

  // put the board in an full IDLE condition, e.g. set to 0 the dummy-hit
  m_AMB->RC_stop();

  ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString() << " done");
}


/**************************/
void ReadoutModule_Ambslp::publish()
/**************************/
{
  ERS_LOG("Publishing ...");
  //ERS_LOG("ReadoutModule_Ambslp::publish: SLOT = " << std::to_string(m_slot));  
  if (!m_CanPublish) {
    ERS_LOG("Not in a good state yet, skipping");
    return;
  }

  //++++++++++++++++++++++++++++++++++++++
  // 2  Access the registers and publish in IS and OH (to be moved back between 1.4 and 2.1)
  //++++++++++++++++++++++++++++++++++++++
  //ML call readout function for each StatusRegisterCollection, get values and set attributes of m_ambslpNamed; checkin m_ambslpNamed
  if(m_dryRun)
    {
      // Left blank deliberately. Coded this way for consistency with the rest of the code.
    } 
  else
    {
      m_statusRegisterAMBFactory->readout();
      m_dfReaderAmb->readIS();
      m_dfReaderAmb->publishAllHistos();

    }

  ERS_LOG("Publishing Done...");

}

/**************************/
void ReadoutModule_Ambslp::publishFullStats()
/**************************/
{
  ERS_LOG("Publishing Full Stats...");
  //ERS_LOG("ReadoutModule_Ambslp::publishFullStats: SLOT = " << std::to_string(m_slot));  

  if (!m_CanPublish) {
    ERS_LOG("Not in a good state yet, skipping");
    return;
  }

  //++++++++++++++++++++++
  // Creation of the EventFragmentCollection that will be filled-up by the RC_publishFull function 
  //++++++++++++++++++++++
  std::vector<EventFragmentCollection*> eventCollectionsRoad;
  std::vector<EventFragmentCollection*> eventCollectionsHit;
  m_AMB->RC_publishFull(eventCollectionsRoad, eventCollectionsHit);
  
  //++++++++++++++++++++++
  // 2.1 Read registers and fill monitoring variable
  //++++++++++++++++++++++

  m_ambslpNamed->ExampleString = "Test";
  m_ambslpNamed->ExampleU16    = 77;
  m_ambslpNamed->ExampleS32    = rand()%100;
  
  // retriving the DCDC informatione
  vector<float> dcdcinfo = m_AMB->DCDC_read(false);
  m_ambslpNamed->DCDC0VIn = dcdcinfo[0];
  m_ambslpNamed->DCDC0VOut = dcdcinfo[1];
  m_ambslpNamed->DCDC0IOut = dcdcinfo[2];
  m_ambslpNamed->DCDC0T1 = dcdcinfo[3];
  m_ambslpNamed->DCDC0T2 = dcdcinfo[4];
  m_ambslpNamed->DCDC1VIn = dcdcinfo[5];
  m_ambslpNamed->DCDC1VOut = dcdcinfo[6];
  m_ambslpNamed->DCDC1IOut = dcdcinfo[7];
  m_ambslpNamed->DCDC1T1 = dcdcinfo[8];
  m_ambslpNamed->DCDC1T2 = dcdcinfo[9];
  m_ambslpNamed->DCDC2VIn = dcdcinfo[10];
  m_ambslpNamed->DCDC2VOut = dcdcinfo[11];
  m_ambslpNamed->DCDC2IOut = dcdcinfo[12];
  m_ambslpNamed->DCDC2T1 = dcdcinfo[13];
  m_ambslpNamed->DCDC2T2 = dcdcinfo[14];
  m_ambslpNamed->DCDC3VIn = dcdcinfo[15];
  m_ambslpNamed->DCDC3VOut = dcdcinfo[16];
  m_ambslpNamed->DCDC3IOut = dcdcinfo[17];
  m_ambslpNamed->DCDC3T1 = dcdcinfo[18];
  m_ambslpNamed->DCDC3T2 = dcdcinfo[19];

  // Fill the histogram
  uint32_t tmp_lvl1id=0;
  for (int iev=0; iev<(int)eventCollectionsRoad.size(); iev++) {
    for(uint32_t isb=0; isb<(eventCollectionsRoad.at(iev))->getNEventFragments(); isb++) {  
      EventFragmentAMBRoad* efar = (static_cast<EventFragmentAMBRoad*>((eventCollectionsRoad.at(iev))->getEventFragment(isb)));
      AMBRoadObjectEvent aroe = efar->getEventRoads();
      if(aroe.getL1ID() > tmp_lvl1id) tmp_lvl1id=aroe.getL1ID();
      m_hnroads->Fill(aroe.getNRoad());
	
      for(uint32_t ird=0; ird<aroe.getNRoad(); ird++) {
	AMBRoadObject aro = (aroe.getRoads()).at(ird);
	m_hgeoaddress->Fill(aro.getGeoAddress());	
      }
    }
  }
  m_hlast_lvl1id->Fill(tmp_lvl1id);

  for (uint32_t iev=0; iev<eventCollectionsHit.size(); iev++) {
    for(uint32_t isb=0; isb<(eventCollectionsHit.at(iev))->getNEventFragments(); isb++) {  
      EventFragmentAMBHit* efah = (static_cast<EventFragmentAMBHit*>((eventCollectionsHit.at(iev))->getEventFragment(isb)));
      AMBHitObjectEvent ahoe = efah->getEventHits();
      m_hnhits->Fill(ahoe.getNHit());	
    }
  }

  for(uint32_t i=0; i<eventCollectionsRoad.size(); i++) delete eventCollectionsRoad.at(i);
  for(uint32_t i=0; i<eventCollectionsHit.size(); i++) delete eventCollectionsHit.at(i);

  //++++++++++++++++++++++
  // 2.2 Pushish in IS  with schema (i.e.: properly)
  //++++++++++++++++++++++
  try { m_ambslpNamed->checkin();}
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception. 
      daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
      ers::warning(e);  //or throw e;  
    }
  //++++++++++++++++++++++
  // 3 Publish the histograms
  //++++++++++++++++++++++

  // Publishing Histogram
  try {
    m_ohProvider->publish( *m_hlast_lvl1id, m_hlast_lvl1id->GetName(), true ); 
    m_ohProvider->publish( *m_hnroads, m_hnroads->GetName(), true ); 
    m_ohProvider->publish( *m_hgeoaddress, m_hgeoaddress->GetName(), true ); 
    m_ohProvider->publish( *m_hnhits, m_hnhits->GetName(), true ); 
  }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception. 
      daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
      ers::warning(e);  //or throw e;  
    }
}

/**************************/
void ReadoutModule_Ambslp::resynch(const daq::rc::ResynchCmd& cmd)
/**************************/
{
  ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString());
  //ERS_LOG("ReadoutModule_Ambslp::resynch: SLOT = " << std::to_string(m_slot));  
  // Fill ME
  ERS_LOG("ReadoutModule_Ambslp: " << cmd.toString() << " done");
}
   

/**************************/
void ReadoutModule_Ambslp::clearInfo() 
/**************************/
{
  ERS_LOG("ReadoutModule_Ambslp: Clear histograms and counters");
  //ERS_LOG("ReadoutModule_Ambslp::clearInfo: SLOT = " << std::to_string(m_slot));  

  // Reset histograms
  m_hlast_lvl1id->Reset();
  m_hnroads->Reset();
  m_hgeoaddress->Reset();
  m_hnhits->Reset();


  // Reset the IS counters
  m_ambslpNamed->ExampleS32 = 0 ;
  m_ambslpNamed->ExampleU16 = 0 ;
}


//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createReadoutModule_Ambslp();
}

ROS::ReadoutModule* createReadoutModule_Ambslp()
{
  return (new ReadoutModule_Ambslp());
}


/**************************/
////void  ReadoutModule_Ambslp::dumpSpybufferOnFile(Json::Value &info)
/**************************/
/***{
 // Opening the spybuffer output file
 std::string username("unknown");
 struct passwd * pw = ::getpwuid(geteuid());
 if( pw ) { username = pw->pw_name; }

 std::string bufFile = m_dumpFolder + "/spybuffer." + username + ".dat";
 std::ofstream spyFile (  bufFile.c_str() , std::ios::trunc ) ;

 // Checking if file was correctly opened
 if ( spyFile.fail() )
 {
 throw daq::ftk::IOError( ERS_HERE,"Impossible to open spybuffer output file " +  m_dumpFolder + " for Amboard " + m_name );
 }

 try
 {
 // Here decompacting the infos
 if ( info.isMember("SpyBuffer") )
 {
 // Printing every line returned
 Json::Value lines = info["SpyBuffer"];
 for ( unsigned int i = 0; i < lines.size(); i++ )
 {
 spyFile << lines[i].asString() << std::endl;
 }
 }
 else
 {
 // Flushing and closing
 spyFile.flush();
 spyFile.close();

 throw daq::ftk::VmeError( ERS_HERE,
 "Invalid spyBuffer from AmBoard at slot " + m_slot  ) ;
 }

 // Flushing and closing
 spyFile.flush();
 spyFile.close();
 }
 catch( std::ios_base::failure & error )
 {
 throw daq::ftk::IOError( ERS_HERE,
 "ReadoutModule_Ambslp::configure: VME Error when writing spybuffer to file from AmBoard at slot : " +  m_slot );
 }
 }***/


