/* ambslp_amchip_enable_bank.cxx

   Author N.V. Biesuz nicolo.vladi.biesuz@cern.ch 2014
*/

#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"


#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv){
  using namespace  boost::program_options ;

  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("chainlength", value< std::string >()->default_value("2"), "The chain length. Default value is 2.")
    ("chipnum", value< std::string > ()->default_value("3"), "The chips to read. Default value is 3.")
    ("npatt", value< std::string > ()->default_value("-1"), "The number of patterns to be disabled. Default value is -1.")
    //    ("offset", value< std::string > ()->default_value("0"), "The address fo the first pattern. Default value is 0. NOW OBSOLETE")
    ;

  positional_options_description pd; 
  pd.add("pattern_file", 1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chainlength = daq::ftk::string_to_int( vm["chainlength"].as<std::string>() );
  int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );
  int npatt = daq::ftk::string_to_int( vm["npatt"].as<std::string>() );
  //  int offset = daq::ftk::string_to_int( vm["offset"].as<std::string>() );
  
  daq::ftk::AMBoard myAMB(slot);
  
  return  myAMB.disable_bank(chainlength, chipnum, npatt);
}

