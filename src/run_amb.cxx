#include "ambslp/run_amb.h"
#include "ambslp/ambslp_test.h"

int main(int argc, char **argv) {

  using namespace  boost::program_options ;
  
  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
      ("oks",value<std::string>()->default_value("oksconfig:daq/partitions/FTK.data.xml"), "OKS configuration file")
    ("tag"   ,value< std::string >()->default_value("Amb1"), "AMB Name in OKS Configuration")
    ("slot"  ,value< std::string >()->default_value("-1"), "The card slot.")
    ("config",bool_switch()->default_value(false), "Run the CONFIG step.")
    ("run"   ,bool_switch()->default_value(false), "Run the START step.")
    ;

  
  positional_options_description p;
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  } catch ( ... ) { std::cerr << desc << endl; return 1; }
  notify(vm);

  if (vm.count("help") ) { cout << endl <<  desc << endl ; return 0; }

  string amb_name = vm["tag"].as<std::string>();
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );

  bool doCONFIG = vm["config"].as<bool>();
  bool doRUN    = vm["run"]   .as<bool>();
  bool doAll    = !doCONFIG && !doRUN;

  string oksconf = vm["oks"].as<string>();
  cout << "Loading OKS Configuration: " << oksconf << endl;
  std::unique_ptr<::Configuration> db;
  try {
    db.reset(new ::Configuration(oksconf));
  } catch(daq::config::Generic e) {
    std::cerr << "ERROR: Cannot load OKS partition." << std::endl;
    return -1;
  }

  const daq::ftk::dal::ReadoutModule_Ambslp* settings = db->get<daq::ftk::dal::ReadoutModule_Ambslp>(amb_name.c_str());
  if(settings==0)
    {
      std::cerr << "Invalid tag " << amb_name << "! exiting..." << std::endl;
      return -1;
    }

  if (slot < 0) slot = settings->get_Slot();
  cout << "Slot         = " << slot << endl;

  daq::ftk::ambslp_test amb_test(slot);//creat amb

  amb_test.setup(settings);
  if(doCONFIG || doAll)
    {
      sleep(2);
      amb_test.configure(); //do both rx/tx resets
      sleep(2);
      amb_test.connect(); //implement when amb separates Rx/Tx resets
    }
  if(doRUN || doAll)
    {
      sleep(2);
      amb_test.prepareForRun(); //load fifos and run
    }

  // delete db; // this deletion is messed up for some reason.

  return 0;

}

