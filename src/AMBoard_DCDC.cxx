#include "ambslp/AMBoard.h"
#include "ambslp/ambslp_vme_regs.h"

//#include "ambslp/ambslp_mainboard.h"
//#include <ctime>


//using namespace std;

namespace daq {
  namespace ftk {

    /** set the Voltage limits for the DC/DC converters (now LAMB's DCDCs only)
     *
     * The scripts currently uses only constant values.
     */
    void AMBoard::DCDC_setVLimits() {
    	const struct timespec deltaT = { 0, 1000000};

    	// generic words that writes the DCDC voltage limit to the DCDC limit, usually 1.27 V
      unsigned int DCDCWord = AMB_HIT_DCDC_val_V2700 | AMB_HIT_DCDC_cmd_VLimit | AMB_HIT_DCDC_add_WRITE;
      m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB0 | DCDCWord);
      nanosleep(&deltaT,0x0);
      m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB1 | DCDCWord);
      nanosleep(&deltaT,0x0);
      m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB2 | DCDCWord);
      nanosleep(&deltaT,0x0);
      m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB3 | DCDCWord);
      nanosleep(&deltaT,0x0);
    }

    /** Set the voltage for the DC/DC converters, currently just the AM chips
     *
     * The AM chips voltage setup depends on the m_DCDCValCore.
     */
    void AMBoard::DCDC_setVOutputs() {
    	const struct timespec deltaT = { 0, 1000000};

    	// generic words that requests to set the voltage output to a given value, only
    	// the component address in the PMBUS is free, it will added later
    	unsigned int DCDCWord = m_DCDC_val_Vout | AMB_HIT_DCDC_cmd_Vout | AMB_HIT_DCDC_add_WRITE;

    	m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB0 | DCDCWord);
    	nanosleep(&deltaT,0x0);
    	m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB1 | DCDCWord);
    	nanosleep(&deltaT,0x0);
    	m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB2 | DCDCWord);
    	nanosleep(&deltaT,0x0);
    	m_vme->write_check_word(AMB_HIT_DCDC_COMMAND, AMB_HIT_DCDC_add_LAMB3 | DCDCWord);
    	nanosleep(&deltaT,0x0);
    }

    /** extracts an integer value from an n bit integer
     * encoded as complement 2*/
    int _DCDCcompl2(unsigned int x, unsigned int n) {
      unsigned int res(x);
      if (x>>(n-1)) return res -= (1<<n);
      return res;
    }

    /** Convert a register value in a floating point.
     *
     * @param val
     * @return Voltage, current or other floating point vlaue
     */
    float _DCDCValue(unsigned int val) {
      const unsigned int maskMantissa = 0x7ff; // value is related to the next
      const unsigned int mantissaLen  = 11;
      const unsigned int expLen = 5;

      // both exponent and matnissa are compl2 format
      int exp = _DCDCcompl2(val >> mantissaLen, expLen);
      int mantissa = _DCDCcompl2(val & maskMantissa, mantissaLen);

      return pow(2,exp)*mantissa;
    }

    /** Alternative int to floa conversion, used when the
     * for some registers with known scale
     *
     * @param val, registers value
     * @param exp, exponential factor
     * @return data value
     */
    float _DCDCValue2(unsigned int val, int exp) {
      // input value is assumed to be 16-bit wide
      int mantissa = _DCDCcompl2(val&0xffff, 16);

      return pow(2,exp)*mantissa;
    }



    /** The function read the PMBUS registers of the DCDC converters
     * and collect all information. Return an array of floats containing
     * the following: Lamb0 {Vin, Vout, Iout, T1, T2}, Lamb1, ...
     */
    vector<float> AMBoard::DCDC_read(bool printLog) {
      // set the delay, required to avoi collision among PMBUS requests
      const struct timespec deltaT = { 0, 1000000};

      std::stringstream out_ss;

      // create vector holding the results
      vector<float> res;

      // Hack to be fixed !!!
      bool all_V_OK = true;

      const float minVout = 1.10;
      const float maxVout = 1.20;


      for (unsigned LAMB = 0; LAMB < this->getNMaxLAMBs(); LAMB++) { // loop over the LAMBS
        unsigned which_DCDC = 0;
        switch (LAMB) {
          case 0:
            which_DCDC = AMB_HIT_DCDC_add_LAMB0;
            break;
          case 1:
            which_DCDC = AMB_HIT_DCDC_add_LAMB1;
            break;
          case 2:
            which_DCDC = AMB_HIT_DCDC_add_LAMB2;
            break;
          case 3:
            which_DCDC = AMB_HIT_DCDC_add_LAMB3;
            break;
        }

        // read the input voltage
        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_VIN);
        nanosleep(&deltaT,0x0);
        float Vin = _DCDCValue(m_vme->read_word(AMB_HIT_DCDC_DATA));

        //read Vout
        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_VOUT);
        nanosleep(&deltaT,0x0);
        float Vout = _DCDCValue2(m_vme->read_word(AMB_HIT_DCDC_DATA), -13)*AMB_HIT_DCDC_val_VoutScale;
	
        // read the out current
        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_IOUT);
        nanosleep(&deltaT,0x0);
        float IoutAD = _DCDCValue(m_vme->read_word(AMB_HIT_DCDC_DATA));

        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_IOUT_GAIN);
        nanosleep(&deltaT,0x0);
        float IoutGain = _DCDCValue(m_vme->read_word(AMB_HIT_DCDC_DATA));

        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_IOUT_OFFS);
        nanosleep(&deltaT,0x0);
        float IoutOff = _DCDCValue(m_vme->read_word(AMB_HIT_DCDC_DATA));

        float Iout = IoutGain*IoutAD+IoutOff;

        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_TMP1);
        nanosleep(&deltaT,0x0);
        float Temp1 = _DCDCValue(m_vme->read_word(AMB_HIT_DCDC_DATA));

        m_vme->write_word(AMB_HIT_DCDC_COMMAND, which_DCDC | AMB_HIT_DCDC_cmd_READ_TMP2);
        nanosleep(&deltaT,0x0);
        float Temp2 = _DCDCValue(m_vme->read_word(AMB_HIT_DCDC_DATA));

        // collect the values in the output array
        res.push_back(Vin);
        res.push_back(Vout);
        res.push_back(Iout);
        res.push_back(Temp1);
        res.push_back(Temp2);

	// Hack to be fixed !!!
	if( (Vout < minVout) || (Vout > maxVout) )  all_V_OK = false;


	// check LAMB Vout value if in standalone test mode
	if(m_DoStandaloneTest){
	  // const float minVout = 1.10, maxVout = 1.20 ;
	  if( Vout < minVout || Vout > maxVout  )
	    {
	      std::stringstream message;
	      message << "Unexpected Vout on LAMB " << LAMB << "Vout: " << Vout << " V, expected value is between " << minVout << " V and " << maxVout << " V";
	      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
	      ers::warning(ex);
	    }
	}
	
        // print the values in the logs
        out_ss << endl << "LAMB " << LAMB << endl;
        out_ss << "Vin = " << Vin << "V" << endl;
        out_ss << "Vout = " << Vout << "V" << endl;
        out_ss << "Iout =" << Iout << "A" << endl;
        out_ss << "Raw values: ADC Gain Off = " << IoutAD << " " << IoutGain  << " " << IoutOff << endl;
        out_ss << "Temperatures: " << Temp1 << " " << Temp2 << endl;

      } // end loop over the LAMBs


      // Hack to be fixed !!!
      if ( all_V_OK )  res.push_back( 5.0);
      else             res.push_back(-5.0);


      // send the log to the ERS system
      if (printLog) ERS_LOG("AMBoard::DCDC_read()" << endl << out_ss.str());

      return res;

    } // DCDC_read()

  } // namespace ftk
} // namespace daq
