/* ambslp_amchip_reg_healthcheck.cxx

   Author T. Kubota 2017 takashi.kubota@cern.ch
*/


#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv){
  using namespace  boost::program_options ;

  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("chipnum", value< std::string > ()->default_value("3"), "The chips to read. Default value is 15=0xF.")
    ("chainlength", value< std::string > ()->default_value("2"), "The length of a JTAG chain")
    ("reg_write", value< std::string > ()->default_value("0xC6"), "The address of the register to be written.")
    ("reg_read", value< std::string > ()->default_value("0xE6"), "The address of the regiser to be read back.")
    ("reg_length", value< std::string > ()->default_value("97"), "The length of the register.")
    ("n_checks", value< std::string > ()->default_value("10"), "The number of write/read checks to be performed.")
    ("verbose,v", "If present sets verbose level to maximum")
    ;
  positional_options_description pd;                                                                                                                                                               
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );
  int chainlength = daq::ftk::string_to_int( vm["chainlength"].as<std::string>() );
  int reg_write = daq::ftk::string_to_int( vm["reg_write"].as<std::string>() );
  int reg_read = daq::ftk::string_to_int( vm["reg_read"].as<std::string>() );
  int reg_length = daq::ftk::string_to_int( vm["reg_length"].as<std::string>() );
  int n_checks = daq::ftk::string_to_int( vm["n_checks"].as<std::string>() );
  bool verbose = vm.count("verbose")>0 ? true : false ;                                                                                                                                            
   
  daq::ftk::AMBoard myAMB(slot);
  if(verbose) myAMB.setVerbosity(daq::ftk::AMBoard::DEBUG);

  return myAMB.amchip_reg_healthcheck(chipnum, chainlength, reg_write, reg_read, reg_length, n_checks);

}

