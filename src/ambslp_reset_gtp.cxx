/*           Program to reset AMB GTPs                                         */
/*           Written for AMBSLP FTK collaboration                              */
/*           Starting date: 2015-07-03                                         */
/*           Authors: Annovi, Volpi                                            */


#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
using namespace std;

// Parsing input parmaeters and calling the am_init function
int main(int argc, char **argv) 
{ 
 

  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value<int>()->default_value(15), "The card slot")
    ("max_tries", value<int>()->default_value(1), "Try max N times to reset GTPs")
    //    ("amb_lamb", value< std::string >()->default_value("0"),"TO BE REDEFINED 0=Reset ONLY the AMboard, 1=Reset ONLY the LAMBs, 2=Reset ONLY the FIFOs memory inside AMBoard")
    ("mode",value<unsigned int>()->default_value(3), "Set the GTPS to reset, 0x1 input, 0x2 output, 0x3 both")
    ("force","Force the reset")
    ("verbose_level", value<int>()->default_value(1),"0=RunControl, 1=StandAlone; actually unused at the moment")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      cout << "- am_init.cxx : Failure in command_line_parser" << endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {	
      cerr <<  desc << endl;
      return 0;	
    }

  int slot = vm["slot"].as<int>();
  int max_tries = vm["max_tries"].as<int>();
  unsigned int mode = vm["mode"].as<unsigned int>();

  //  int amb_lamb = daq::ftk::string_to_int( vm["amb_lamb"].as<std::string>() );
  //  
  //  verbose_level actually unused
  // int verbose_level = daq::ftk::string_to_int( vm["verbose_level"].as<std::string>() );
  bool force = vm.count("force") > 0 ? true : false;
  //
  ///////////////////////////////////////////

  daq::ftk::AMBoard myAMB(slot); // create an AMB instance

  // calling the function and checking the result
  return  myAMB.reset_gtp(max_tries, force, mode) ? 0 : -1;
  
}


