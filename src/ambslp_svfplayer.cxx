#include <ftkcommon/svfplayer.h>
#include "ambslp/ambslp.h"
#include "ambslp/exceptions.h"

#include "ambslp/ambslp_vme_jtag_func.h"
#include "ambslp/ambslp_jtag_func.h"


/** Specialization of the SVFIntepreter class designed
 * to comunicate with the FPGAs in the AMB using VME bus
 * with single word transitions.
 */
class SVFInterpreterAMB : public SVFInterpreter {
private:
  unsigned int _slot;
  VMEInterface *_vme;

  void send_command(const JTAGCmdBuffer &);

public:
  SVFInterpreterAMB(unsigned int slot);

  virtual ~SVFInterpreterAMB();
};

/** Build the object setting the VME slot where the board is
 * and preparing the board for programming.
 */
SVFInterpreterAMB::SVFInterpreterAMB(unsigned int slot) : _slot(slot) {
  _hasCable = true;
  _vme = VMEManager::global().amb(slot);
  cout << "Begin of programming sequence for AMB" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
}

/** Destroying the object remove the flag in the AMB for
 * programming.
 */
SVFInterpreterAMB::~SVFInterpreterAMB() {
  cout << "Switching off programming for AMB" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);
}

void SVFInterpreterAMB::send_command(const JTAGCmdBuffer &cmd) {
  _totnbits += cmd.getNBits();

  bool bigCommand(cmd.getNBits()>1000000);

  std::vector<unsigned char> tdores(cmd.getBufferSize());
  for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;

  size_t istep = (cmd.getNBits()+9)/10;
  unsigned int curstep(0);
  if (bigCommand) cout << "Large command: " << 0 << flush;
  for (size_t ibit=0; ibit!=cmd.getNBits(); ++ibit) {
    unsigned int step = ibit/istep;
    if (step>curstep && bigCommand) {
      cout << step << flush;
      curstep = step;
    }

    size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
    size_t bitpos = ibit&7; // within the word the bits have a normal order
    unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
    unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
    unsigned int tdo = 0; // if not used start from 0

    if (cmd.hasTDO()) tdo = _vme->read_word(TDO_FPGA)&1;
    _vme->write_word(ENABLE_TCK, 0x0);
    _vme->write_word(TMS_FPGA, tms);
    _vme->write_word(ENABLE_TCK, 0x1);
    _vme->write_word(TDI_FPGA, tdi);


    if (_verbose>1) cout << tms << " " << tdi << " " << tdo << " (" << ibit << "," << bitpos << ")" << endl;
    tdores[bufpos] |= (tdo<<bitpos);
  }

  if (bigCommand)  cout << " Done" << endl;

  if (cmd.hasTDO()) checkTDOs(cmd, tdores.data());

}

/** Implementation of the SVFIntepreter designed to meet
 * the needs for JTAG comunnication for the FPGA
 * in the AMB, with VME block transfer .
 */
class SVFInterpreterAMBBlockTransfer : public SVFInterpreter {
private:
  unsigned int _slot;
  VMEInterface *_vme;

  size_t _nbtwrites; // total number of bytes wrote with BT

  void send_command(const JTAGCmdBuffer &);

public:
  SVFInterpreterAMBBlockTransfer(unsigned int slot);

  virtual ~SVFInterpreterAMBBlockTransfer();

  virtual void printSummary();
};

SVFInterpreterAMBBlockTransfer::SVFInterpreterAMBBlockTransfer(unsigned int slot) : _slot(slot),
                                                                                    _nbtwrites(0) {
  _hasCable = true;
  _vme = VMEManager::global().amb(slot);
  _vme->disableBlockTransfers(false); // override the disable
  cout << "Begin of programming sequence for AMBs usingn the block transfer" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
  _vme->write_word(ENABLE_TCK, 0x1);
}

SVFInterpreterAMBBlockTransfer::~SVFInterpreterAMBBlockTransfer() {
  cout << "Switching off programming for AMBs" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);
  _vme->write_word(ENABLE_TCK, 0x0);
}

/** The send command method is designed to meet the design of
 * the JTAG cotnrol through VME for the LAMB boards.
 */
void SVFInterpreterAMBBlockTransfer::send_command(const JTAGCmdBuffer &cmd) {
  _totnbits += cmd.getNBits();

  // the boolean is true if for the JTAG set the block transfer can be used
  const bool canUseBT(!cmd.hasTDO());
  if (_verbose>0) cout << "Block transfer = " << canUseBT << endl;

  bool bigCommand(cmd.getNBits()>1000000);

  std::vector<unsigned char> tdores;
  if (cmd.hasTDO()) {
    tdores = std::vector<unsigned char>(cmd.getBufferSize());
    for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;
  }

  size_t istep = (cmd.getNBits()+9)/10;
  unsigned int curstep(0);
  if (bigCommand) cout << "Large command: " << 0 << flush;

  if (canUseBT) {
    _nbtwrites += cmd.getNBits();

    // it can use the BT, collect block of TDI-TMS  data and send as vector to the VME
    const unsigned int block_size(0x1000); // max number of bits that can be sent at once
    const unsigned int nblocks((cmd.getNBits()+block_size-1)/block_size); // number of transfers

    if (_verbose>0) cout << "N blocks = " << nblocks << endl;

    vector<unsigned int> tditms_data;
    vector<unsigned int> tdo_data;
    for (unsigned int iblock=0; iblock!=nblocks; ++iblock) { // loop over the blocks
      // reset the intermediate vectors
      tditms_data.clear();
      tdo_data.clear();

      const unsigned int firstBit(iblock*block_size); // offset of the block, as number of bit
      const unsigned int lastBit(firstBit+block_size); // offset of the block, as number of bit

      unsigned int step = firstBit/istep;
      if (step>curstep && bigCommand) {
        cout << step << flush;
        curstep = step;
      }

      for (size_t ibit=firstBit; ibit!=cmd.getNBits() && ibit!=lastBit; ++ibit) { // loop over the bits
        size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
        size_t bitpos = ibit&7; // within the word the bits have a normal order

        unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
        unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
        //unsigned int tdo = 0; // if not used start from 0


        // populate the intermediate vector
        tditms_data.push_back(tdi | (tms<<1));
      } // end loop over the bits

      if (!tditms_data.empty()) _vme->write_block(AMB_MEM_TDITMS_FPGA,tditms_data);
      //TODO TDO is not implemented
    } // end loop over the blocks
  }
  else {
    // cannot use BT, perform a simpe bit-by-bit transfer, can also read TDO
    for (size_t ibit=0; ibit!=cmd.getNBits(); ++ibit) {
      unsigned int step = ibit/istep;
      if (step>curstep && bigCommand) {
        cout << step << flush;
        curstep = step;
      }

      size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
      size_t bitpos = ibit&7; // within the word the bits have a normal order
      unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
      unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
      unsigned int tdo = 0; // if not used start from 0

      if (cmd.hasTDO()) tdo = _vme->read_word(AMB_MEM_TDO_FPGA)&1;
      _vme->write_word(AMB_MEM_TDITMS_FPGA, tdi | (tms<<1) );

      if (_verbose>1) cout << tms << " " << tdi << " " << tdo << " (" << ibit << "," << bitpos << ")" << endl;
      tdores[bufpos] |= (tdo<<bitpos);
    }
  }

  if (bigCommand)  cout << " Done" << endl;

  if (cmd.hasTDO()) {
    checkTDOs(cmd, tdores.data());
  }
}

void SVFInterpreterAMBBlockTransfer::printSummary() {
  SVFInterpreter::printSummary();
  cout << "Total number of bits tranfer using BT: " << _nbtwrites << endl;
}


/** Implementation of the SVFIntepreter designed to meet
 * the needs for JTAG comunnication for the BUSCA chip
 * in the AMB, with single VME access per transition.
 */
class SVFInterpreterLAMB : public SVFInterpreter {
private:
  unsigned int _slot;
  VMEInterface *_vme;

  void send_command(const JTAGCmdBuffer &);

public:
  SVFInterpreterLAMB(unsigned int slot);

  virtual ~SVFInterpreterLAMB();
};

SVFInterpreterLAMB::SVFInterpreterLAMB(unsigned int slot) : _slot(slot) {
  _hasCable = true;
  _vme = VMEManager::global().amb(slot);
  cout << "Begin of programming sequence for LAMBs" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
  _vme->write_word(ENABLE_TCK, 0x1);
}

SVFInterpreterLAMB::~SVFInterpreterLAMB() {
  cout << "Switching off programming for LAMBs" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);
  _vme->write_word(ENABLE_TCK, 0x0);
}

/** The send command method is designed to meet the design of
 * the JTAG cotnrol through VME for the LAMB boards.
 */
void SVFInterpreterLAMB::send_command(const JTAGCmdBuffer &cmd) {
  _totnbits += cmd.getNBits();

  bool bigCommand(cmd.getNBits()>1000000);

  std::vector<unsigned char> tdores (cmd.getBufferSize());
  for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;

  size_t istep = (cmd.getNBits()+9)/10;
  unsigned int curstep(0);
  if (bigCommand) cout << "Large command: " << 0 << flush;
  for (size_t ibit=0; ibit!=cmd.getNBits(); ++ibit) {
    unsigned int step = ibit/istep;
    if (step>curstep && bigCommand) {
      cout << step << flush;
      curstep = step;
    }

    size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
    size_t bitpos = ibit&7; // within the word the bits have a normal order
    unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
    unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
    unsigned int tdo = 0; // if not used start from 0

    if (cmd.hasTDO()) tdo = _vme->read_word(AMB_VME_TDO_LAMB)&1;
    _vme->write_word(AMB_VME_TDITMS_LAMB, tdi + (tms<<1) );

    if (_verbose>1) cout << tms << " " << tdi << " " << tdo << " (" << ibit << "," << bitpos << ")" << endl;
    tdores[bufpos] |= (tdo<<bitpos);
  }

  if (bigCommand)  cout << " Done" << endl;

  if (cmd.hasTDO()) checkTDOs(cmd, tdores.data());


}


/** Implementation of the SVFIntepreter designed to meet
 * the needs for JTAG comunnication for the FPGA BUSCA
 * in the LAMB, with VME block transfer .
 */
class SVFInterpreterLAMBBlockTransfer : public SVFInterpreter {
private:
  unsigned int _slot;
  VMEInterface *_vme;

  size_t _nbtwrites;

  void send_command(const JTAGCmdBuffer &);

public:
  SVFInterpreterLAMBBlockTransfer(unsigned int slot);

  virtual ~SVFInterpreterLAMBBlockTransfer();

  virtual void printSummary();
};

SVFInterpreterLAMBBlockTransfer::SVFInterpreterLAMBBlockTransfer(unsigned int slot) : _slot(slot),
                                                                                      _nbtwrites(0) {
  _hasCable = true;
  _vme = VMEManager::global().amb(slot);
  _vme->disableBlockTransfers(false); // override the disable
  cout << "Begin of programming sequence for LAMBs using the block transfer" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 1);
  _vme->write_word(ENABLE_TCK, 0x1);
}

SVFInterpreterLAMBBlockTransfer::~SVFInterpreterLAMBBlockTransfer() {
  cout << "Switching off programming for LAMBs" << endl;
  _vme->write_check_word(AMB_VME_ENABLE_VME_FPGA_PROG, 0);
  _vme->write_word(ENABLE_TCK, 0x0);
}

/** The send command method is designed to meet the design of
 * the JTAG cotnrol through VME for the LAMB boards.
 */
void SVFInterpreterLAMBBlockTransfer::send_command(const JTAGCmdBuffer &cmd) {
  _totnbits += cmd.getNBits();

  // the boolean is true if for the JTAG set the block transfer can be used
  const bool canUseBT(!cmd.hasTDO());
  if (_verbose>0) cout << "Block transfer = " << canUseBT << endl;

  bool bigCommand(cmd.getNBits()>1000000);

  std::vector<unsigned char> tdores;
  if (cmd.hasTDO()) {
    tdores =std::vector<unsigned char>(cmd.getBufferSize());
    for (size_t pos=0; pos!=cmd.getBufferSize(); ++pos) tdores[pos] = 0;
  }

  size_t istep = (cmd.getNBits()+9)/10;
  unsigned int curstep(0);
  if (bigCommand) cout << "Large command: " << 0 << flush;

  if (canUseBT) {
    _nbtwrites += cmd.getNBits();

    // it can use the BT, collect block of TDI-TMS  data and send as vector to the VME
    const unsigned int block_size(0x1000); // max number of bits that can be sent at once
    const unsigned int nblocks((cmd.getNBits()+block_size-1)/block_size); // number of transfers

    if (_verbose>0) cout << "N blocks = " << nblocks << endl;

    vector<unsigned int> tditms_data;
    vector<unsigned int> tdo_data;
    for (unsigned int iblock=0; iblock!=nblocks; ++iblock) { // loop over the blocks
      // reset the intermediate vectors
      tditms_data.clear();
      tdo_data.clear();

      const unsigned int firstBit(iblock*block_size); // offset of the block, as number of bit
      const unsigned int lastBit(firstBit+block_size); // offset of the block, as number of bit

      unsigned int step = firstBit/istep;
      if (step>curstep && bigCommand) {
        cout << step << flush;
        curstep = step;
      }

      for (size_t ibit=firstBit; ibit!=cmd.getNBits() && ibit!=lastBit; ++ibit) { // loop over the bits
        size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
        size_t bitpos = ibit&7; // within the word the bits have a normal order

        unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
        unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
        //unsigned int tdo = 0; // if not used start from 0


        // populate the intermediate vector
        tditms_data.push_back(tdi | (tms<<1));
      } // end loop over the bits

      if (!tditms_data.empty()) _vme->write_block(AMB_MEM_TDITMS_LAMB,tditms_data);
      //TODO TDO is not implemented
    } // end loop over the blocks
  }
  else {
    // cannot use BT, perform a simpe bit-by-bit transfer, can also read TDO
    for (size_t ibit=0; ibit!=cmd.getNBits(); ++ibit) {
      unsigned int step = ibit/istep;
      if (step>curstep && bigCommand) {
        cout << step << flush;
        curstep = step;
      }

      size_t bufpos = cmd.getBufferSize()-1-ibit/8; // the first position to be sent is at the end
      size_t bitpos = ibit&7; // within the word the bits have a normal order
      unsigned int tms = (cmd.getTMS(bufpos)>>bitpos)&1;
      unsigned int tdi = (cmd.getTDI(bufpos)>>bitpos)&1;
      unsigned int tdo = 0; // if not used start from 0

      if (cmd.hasTDO()) tdo = _vme->read_word(AMB_MEM_TDO_LAMB)&1;
      _vme->write_word(AMB_MEM_TDITMS_LAMB, tdi | (tms<<1) );

      if (_verbose>1) cout << tms << " " << tdi << " " << tdo << " (" << ibit << "," << bitpos << ")" << endl;
      tdores[bufpos] |= (tdo<<bitpos);
    }
  }

  if (bigCommand)  cout << " Done" << endl;

  if (cmd.hasTDO()) {
    checkTDOs(cmd, tdores.data());
  }
}

void SVFInterpreterLAMBBlockTransfer::printSummary() {
  SVFInterpreter::printSummary();
  cout << "Total number of bits tranfer using BT: " << _nbtwrites << endl;
}

int main(int argc, char **argv)
{
  using namespace  boost::program_options ;

  int verbose(0);

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value<unsigned int>()->default_value(15), "Use AMB in slot")
    ("lamb,l", "If present LAMB FPGAs will be accessed instead of main board FPGAs")
    ("svffile,S", value<string>(), "SVF file with commands")
    ("verbose,v", value<int>(&verbose)->implicit_value(1), "Changes the verbosity level during the parsing")
    ("nocomment,N", "Suppress the print of the comments found in the SVF file")
    ("bt,B", "Uses block transfer more")
    ("test", "When used the VME interface is not opened, allowed tests of the parser")
    ;

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
    notify(vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
    {
      FTK_VME_ERROR( "- ambslp_svfplayer : Failure in command_line_parser" );
      return 1;
    }

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      ERS_LOG( desc );
      return 0;
    }

  int slot = vm["slot"].as<unsigned int>();
  string svffile_path = vm["svffile"].as<string>();
  bool lamb = vm.count("lamb")>0;
  bool printComment = vm.count("nocomment")==0;
  bool blockTransfer = vm.count("bt")>0;

  struct timeval startTime;
  gettimeofday(&startTime, 0x0);

  int res = 0;

  // implement an interpreter compatible with the options
  std::unique_ptr<SVFInterpreter> svfplayer;
  if (!vm.count("test")) {
    if (lamb and not blockTransfer) svfplayer.reset((SVFInterpreter*) new SVFInterpreterLAMB(slot) );
    else if (lamb and blockTransfer) {
      cout << "Transfer using block transfer" << endl;
      svfplayer.reset((SVFInterpreter*)new SVFInterpreterLAMBBlockTransfer(slot));
    }
    else if (not lamb and not blockTransfer) svfplayer.reset((SVFInterpreter*)new SVFInterpreterAMB(slot));
    else if (not lamb and blockTransfer) {
      cout << "Transfer using block transfer" << endl;
      svfplayer.reset((SVFInterpreter*) new SVFInterpreterAMBBlockTransfer(slot));
    }
    else {
      cerr << "The combination of options don't match any JTAG interface, check the command line" << endl;
      return -1;
    }
  }
  else {
    svfplayer.reset( new SVFInterpreter());
  }
  // setup verbosity message levels
  svfplayer->setVerbose(verbose);
  svfplayer->setPrintComment(printComment);

  // open the file and parse it
  svfplayer->openFile(svffile_path.c_str());
  svfplayer->parse();

  if (!svfplayer->getNTDOErrors()) cout << "ALL GOOD" << endl;
  else cout << "Possible issues" << endl;



  struct timeval endTime;
  gettimeofday(&endTime, 0x0);

  cout << endl << "Elapsed time: " << endTime.tv_sec-startTime.tv_sec << " seconds" << endl;

  return res;

}
