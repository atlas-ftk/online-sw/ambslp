/* ambslp_amchip_init_evt.cxx

   Author N.Biesuz 2014
*/

#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
using namespace std;

int main(int argc, char **argv) {
	using namespace  boost::program_options;

	///////////////////////////////////////////////////////////////
	// Parsing parameters using namespace boost::program:options //
	///////////////////////////////////////////////////////////////
	options_description desc("Allowed options");
	desc.add_options()
		("help", "produce help message")
		("slot", value<int>()->default_value(15), "The card slot. Default value is 15.")
		("chipnum", value<int>()->default_value(3), "The chips to read. Default value is 3.")
	;

	positional_options_description pd;
	pd.add("pattern_file", 1);

	variables_map vm;
	try {
		store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
	}
	catch( ... ) // In case of errors during the parsing process, desc is printed for help 
	{
	std::cerr << desc << std::endl;
	return 1;
	}
	notify(vm);

	if( vm.count("help") ) // if help is required, then desc is printed to output
	{
		std::cout << std::endl <<  desc << std::endl ;
		return 0;
	}

	int slot = vm["slot"].as<int>();
	int chipnum = vm["chipnum"].as<int>();

	daq::ftk::AMBoard myAMB(slot);
	return myAMB.amchip_init_evt(chipnum);
} // end of main

