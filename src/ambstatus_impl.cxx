

#include "ambstatus/ambstatus_impl.h"



void streamStateInfo( std::ostream & stream, StateInfo* state ) {

  stream << "\n\t\t is configured: " << std::boolalpha << state->isConfigured;
  stream << "\n\t\t is latest status reached: " << std::boolalpha << state->isLatestStatus;
  stream << "\n\t\t time latestConfiguration: ";
  stream << std::setfill('-') << std::setw(20) << "\n" << std::setfill(' ');
}


void setupStateInfo( StateInfo *state ) {

    state->isLatestStatus = true;
    state->isConfigured = true;
    time_t now = time(0);
    state->latestConfiguration = *localtime(&now);
}


//-----------------------------------------------
//  ConfigurationDone

struct ConfigDone {

  StateInfo state;
};

struct ConfigFailed {

  StateInfo state;
};

struct VMEConfig {

  StateInfo state;
  int slot;
  int lamb_autosense;
};



struct ConfigDone * get_ConfigurationDone( StoragePolicy * confStorage ) {

  struct * config = confStorage->get_ConfigurationDone();
  return config;
}



ConfigurationDone::ConfigurationDone( StoragePolicy *confStorage ) : AMB_state(confStorage), //
                                                                     done_data(0) //
{
  done_data = get_ConfigurationDone( confStorage );
}


ConfigurationDone::~ConfigurationDone() {}


void ConfigurationDone::show( std::ostream & stream ) {

  stream << "\n\n\tSTATE: done.";
  StateInfo *state = &(done_data->state);
  streamHeader( stream, state );
}


ConfigExitCode ConfigurationDone::configure( OptionHandle *option ) {

  // option is not needed in case of successful configuration
  StateInfo *state = &(done_data->state);

  if ( state->isLatestStatus != true ) {
    setupStateInfo( state );
  }

  ConfigExitCode eCode = SUCCESS;
  return eCode;
}
//-----------------------------------------------
//




//-----------------------------------------------
// ConfigurationFailure


ConfigurationFailure::ConfigurationFailure( AMBConfiguration *config ) : configuration(config), //
                                                                         failure_data(0) //
{  
  failure_data = get_ConfigurationFailure();
}


ConfigurationFailure::~ConfigurationFailure() { }

void ConfigurationFailure::show( std::ostream & stream ) {

  stream << "\n\n\tSTATE: failed.";
  StateInfo *state = &(failure_data->state);
  streamHeader( stream, state );

}

ConfigExitCode ConfigurationFailed::configure( OptionHandle *option ) {

  StateInfo *state = &(failure_data->state);

  if ( state->isLatestStatus != true ) {
    setupStateInfo( state );
  }

  ConfigExitCode eCode = FAILURE;
  return eCode;
}
//-----------------------------------------------



//-----------------------------------------------
// VMEReader


VMEReader::VMEReader( AMBConfiguration * config ) : AMB_state(config), //
                                                    vme_data(0) //
{

  vme_data = get_VMEConfig();
}


VMEReader::~VMEReader() {}


void VMEReader::show( std::ostream & stream ) {

  stream << "\n\n\tSTATE: Reading registers from VME.";
  StateInfo *state = &(vme_data->state);
  streamHeader( stream, state );

}

ConfigExitCode VMERegister::configure( OptionHandle *option ) {

  StateInfo *state = &(vme_data->state);

  if ( state->isLatestStatus != true ) {
    setupStateInfo( state );

    VMEOption *opt = dynamic_cast<VMEOption*>(option->option);
    
  }

  ConfigExitCode eCode = SUCCESS;
  return eCode;
}


