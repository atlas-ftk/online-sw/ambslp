/* ambslp_amchip_serdes_config.cxx

author nicolo.vladi.biesuz@cern.ch 11/2014
 
*/

// as of July, 21st 2015,
// the following standalone programs have been removed:
//
//   ambslp_amchip_8b_10b_all_buses_main
//   ambslp_amchip_8b_10b_pattin1_only_main
//   ambslp_amchip_8b_10b_pattin_pattout_main
//   ambslp_amchip_8b_10b_pattout_only_main
//   
//   ambslp_amchip_prbsgen_all_buses_main
//   ambslp_amchip_prbsgen_pattin1_only_main
//   ambslp_amchip_prbsgen_pattin_pattout_main
//   ambslp_amchip_prbsgen_pattout_only_main
//
// and substituted with the current main.
// to mimic the old behaviour of the functions above:
//
//    ambslp_amchip_8b_10b_all_buses_main  SLOT CHAINLENGTH CHIPNUM 
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM a all_buses normal 2.
//
//    ambslp_amchip_8b_10b_pattin1_only_main SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM COLUMNS pattin1 normal 2.
//
//    ambslp_amchip_8b_10b_pattin_pattout_main SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM COLUMNS patt_duplex normal 2.
//
//    ambslp_amchip_8b_10b_pattout_only_main  SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM COLUMNS pattout normal 2.
//
//
//    ambslp_amchip_prbsgen_all_buses_main SLOT CHAINLENGTH CHIPNUM
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM a all_buses prbs 2.
//
//    ambslp_amchip_prbsgen_pattin1_only_main SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM COLUMNS pattin1 prbs 2.
//
//    ambslp_amchip_prbsgen_pattin_pattout_main SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM COLUMNS patt_duplex prbs 2.
//
//    ambslp_amchip_prbsgen_pattout_only_main  SLOT CHAINLENGTH CHIPNUM COLUMNS
//         -->  ambslp_amchip_serdes_config_main  SLOT CHAINLENGTH CHIPNUM COLUMNS pattout prbs 2.
//
//
//  PLEASE NOTE: 
//   defaults values for ambslp_amchip_8b_10b_pattout_only_main 
//                       ambslp_amchip_prbsgen_pattout_only_main were:
//                         SLOT        15
//                         CHAINLENGTH  2
//                         CHIPNUM      3
//                         COLUMNS      a
//
//   for all other functions above:
//                         SLOT        15
//                         CHAINLENGTH  4
//                         CHIPNUM     15
//                         COLUMNS      a  (when present)
//
//-----------------------------------------------------------------------------




#include "ambslp/ambslp.h"
#include "ambslp/ambslp_amchip.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>




#define serdes_opt_NUM  12
#define mode_opt_NUM  3



int main(int argc, char **argv) 
{ 
	
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("chainlength", value< std::string >()->default_value("2"), "The chain length. Default value is 2.")
    ("chipnum", value< std::string > ()->default_value("3"), "The chips to read. Default value is 3.")
    ("columns", value< std::string > ()->default_value("a"), "The vme columns that must be addressed. Default value is 'a'=all. Valid values are 'a' all, 'e' even, 'o' odd.")
    ("serdes", value<std::string> ()->default_value("pattin1"), "The id of the serdes to be set up. Default value is 'pattin1'. Valid values are 'patt_duplex', 'pattin0', 'pattin1', 'pattout', 'bus0', 'bus1', 'bus2', 'bus3', 'bus4', 'bus5', 'bus6', 'bus7', 'all_buses'.")
    ("mode", value< std::string >()->default_value("normal"), "The operation mode of the serdes. Default is 'normal'=8b_10b decode/encode. Valid values are 'normal': 8b_10, 'prbs': pseudo-ranbom-bit-sequence, 'fixed_pattern': send a fixed pattern.")
    ("frequency", value <std::string>()->default_value("2.0"), "The frequency of operations, in Gbit/sec. Default is 2 Gbit/sec." )
    ;

  positional_options_description pd; 
  pd.add("pattern_file", 1);

  variables_map vm;
  try 
    {
      store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }

  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int chainlength = daq::ftk::string_to_int( vm["chainlength"].as<std::string>() );
  int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );
  const char* columns = (vm["columns"].as<std::string>()).c_str();
  const char * serdes = ( vm["serdes"].as<std::string>() ).c_str();
  const char * mode   = ( vm["mode"].as<std::string>() ).c_str();
  float frequency     = stof( vm["frequency"].as<std::string>() );


  // check input values

  // columns
  if(not(strcmp(columns, "a")==0) and not(strcmp(columns, "e")==0) and not(strcmp(columns, "e")==0) ){
    std::cerr<<"warning: function called with invalid option columns: '"<<columns<<"'. Using default value 'a'!"<<std::endl;
    columns="a";
  }

  // serdes
  const char *serdesAllowed[serdes_opt_NUM] = { //
                                  "patt_duplex", //
                                  "pattin0", //
                                  "pattin1", //
                                  "bus0", //
                                  "bus1", //
                                  "bus2", //
                                  "bus3", //
                                  "bus4", //
                                  "bus5", //
                                  "bus6", //
                                  "bus7", //
                                  "all_buses" };

  bool serdesIsValid = false;
  for ( int iser=0; iser<serdes_opt_NUM; iser++  ) {
  
    if ( strcmp( serdes, serdesAllowed[iser])==0 )
      serdesIsValid = true;
  }

  if ( !serdesIsValid ) {
    std::cerr << "warning: function called with invalid " << //
      "option serdes: '" << serdes << "'. Using default: 'pattin1'!" << std::endl;
    serdes = "pattin1";
  }
  
  // mode  
 const char* modeAllowed[mode_opt_NUM] = { //
                                           "normal", //
                                           "prbs", //
                                           "fixed_pattern" };

  bool modeIsValid = false;
  for ( int imod=0; imod<mode_opt_NUM; imod++  ) {
  
    if ( strcmp( mode, modeAllowed[imod])==0 )
      modeIsValid = true;
  }

  if ( !modeIsValid ) {
    std::cerr << "warning: function called with invalid " << //
      "option mode: '" << mode << "'. Using default: 'normal'!" << std::endl;
    mode = "normal";
  }


  // this should be changed in future, since the guard below 
  //   is ignoring the frequency option.
  if ( strcmp(serdes, "all_buses")==0 ) {

    if ( strcmp(mode, "normal")==0 )
       return daq::ftk::ambslp_amchip_8b_10b_all_buses( slot, chainlength, chipnum );

     if ( strcmp(mode, "prbs")==0 )
       return daq::ftk::ambslp_amchip_prbsgen_inout_all_buses( slot, chainlength, chipnum);

  } // if loop over all buses

  else
    return ambslp_amchip_SERDES_setup( slot, chainlength, chipnum, columns, serdes, mode, frequency );

}


