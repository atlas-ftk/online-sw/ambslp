
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"

#include "ambslp/ambslp_vme_regs.h"



int main()
{
 int slot = 15;
 u_int register_content;

 VMEInterface *vme = VMEManager::global().amb(slot);
 // printf ("\n\tinput: %d\n", IDLE_CONFIG_REG);

 // Hikmat, remember that the slot is already coded!
 int IDLE_CONFIG_REG = 0x00004140;

 register_content = vme->read_word(IDLE_CONFIG_REG);
 printf("Number of idle words before end event for slot %d : %x \n ", slot, register_content);
return 0;
}
