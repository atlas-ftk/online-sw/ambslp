//#include <algorithm>

#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  using namespace  boost::program_options ;

  std::cout << " [VALIDATION]  ambslp_inp_spy_main  " << std::endl ;


  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help",                                                    "Produce help message")
    ("slot",       value< std::string >()->default_value("15"), "The card slot")
    ("method",     value< std::string >()->default_value("0"),  "The dumping method")
    ("keepFreeze", value< std::string >()->default_value("0"),  "Keep freezing after process if this is set to 1")
    ("verbose,v",                                               "If present verbose option will be used")
    // ("filename", value< std::string >()->default_value("inp_spy"), "File name to be given as \"filename\".txt. If it is not given, default name will be given.")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) {  // In case of errors during the parsing process, desc is printed for help 
    std::cerr << desc << std::endl; 
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) {  // if help is required, then desc is printed to output
    std::cout << std::endl << desc << std::endl ; 
    return 0;	
  } 

  int  slot       = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int  method     = daq::ftk::string_to_int( vm["method"].as<std::string>() );
  int  keepFreeze = daq::ftk::string_to_int( vm["keepFreeze"].as<std::string>() );
  bool verbose    = vm.count("verbose")>0 ? true : false;


  daq::ftk::AMBoard _AM_board(slot, verbose? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::WARNING);

  return _AM_board.inputSpyBuffers(method, keepFreeze);


  // std::string filename = vm["filename"].as<std::string>();

  // return daq::ftk::ambslp_inp_spy( slot, method, keepFreeze);
}
