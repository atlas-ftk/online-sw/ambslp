#include "ambslp/AMBoard.h"
#include <ambslp/ambslp_mainboard.h>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <string>
#include <cstdlib>
using namespace boost::program_options;
using namespace std;

std::string bankPath("/afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root");
int bankType(0);
bool ssoffset(false);
unsigned int nPattPerChip(131072);

int main(int argc, char *argv[]) {

  vector<string> transitions;
  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  //  const unsigned int useDefaultDummyHit = 0xdeadbeef;
  desc.add_options()
    ("help,h", "produce help message")    
    ("bankpath,B", value<string>(&bankPath)->default_value(bankPath), "Pattern bank, value random makes a random bank")
    ("banktype,T", value<int>(&bankType)->default_value(bankType), "Pattern bank file type")
    ("ssoffset,O", value<bool>(&ssoffset)->default_value(ssoffset), "Enable the SSOffset or not")
    ("nPattPerChip,N", value<unsigned int>(&nPattPerChip)->default_value(nPattPerChip), "The number of patterns per AM06")
    ;

  positional_options_description pd;
  pd.add("transition", -1);
  
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);
  
  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }  
  
  //Pattern bank generation and configuration
  FTKPatternBank * PatternBank = new FTKPatternBank();
  PatternBank->setNLayers(NLAYERS);

  //if (ndc.size()>0)  for (int il=0;il!=NLAYERS;++il) PatternBank->setDCConfig(ndc);
  int res = PatternBank->loadPatternBankDCconfig(bankPath.c_str(), bankType); 
  std::stringstream message;
  switch(res){
  case 0:
    PRINT_LOG("Pattern bank correctly loaded in memory and DCconfig stored.");
    break;
  case -1:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBank(): Error while loading bank in memory.";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);
      break;
    }
  case -2:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBankDCconfig(): No SSMap config found in ROOT pbank file -> default bank DCconfig set [3,3,3,3,3,3,3,3].";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);
      break;
    }
  default:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBank(): Warning while loading bank in memory: unknow error code";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);
      break;
    }
  }
          
  for (int il=0;il!=NLAYERS;++il) PatternBank->setSSOffset(il,8*ssoffset);
  res = PatternBank->loadPatternBank(bankPath.c_str(), bankType, MAXCOLUMNS*MAXCHIP_PER_VMECOL*nPattPerChip);
  switch(res){
  case 0:
    PRINT_LOG("Pattern bank correctly loaded in memory.");
    break;
  case -1:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBank(): Error while loading bank in memory.";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);    
      break;
    }
  case -2:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBank(): Error while loading bank in memory: the number of requested patterns cannot be loaded";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);
      break;
    }
  case -3:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBank(): Error while loading bank in memory: the format cannot be read";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex); 
      break;
    }
  default:
    {
      std::stringstream message;
      message << "AMBoard::readPatternBank(): Warning while loading bank in memory: unknow error code";
      daq::ftk::ftkException ex(ERS_HERE, name_ftk(), message.str());
      ers::warning(ex);
      break;
    }
  }
  
  const int off = ssoffset ? 8 : 0;
  unsigned int WCSS = 0x7000;
  PatternBank->overrideSpecialPattern(WCSS-off, nPattPerChip);

  std::vector<uint32_t> data={nPattPerChip};
  PatternBank->updateChecksum(1,data);

  PRINT_LOG("checksum for bank "<<bankPath<<" is "<<std::hex<<PatternBank->getChecksum()<<std::dec<<"\n");
  
  ofstream csfile;
  csfile.open("./AM_checksum.txt");
  csfile<<"0x"<<std::hex<<PatternBank->getChecksum()<<std::dec<<std::endl;
  csfile.close();

  return 0;


}
