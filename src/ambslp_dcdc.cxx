/* ambslp_pmbus

   Author A. Annovi May 18 2016
*/

//#include "ambslp/ambslp.h"
//#include "ambslp/ambslp_amchip.h"
#include "ambslp/AMBoard.h"

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv){
  using namespace  boost::program_options ;

  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
  desc.add_options()
    // common options
    ("help,h", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot. Default value is 15.")
    ("verbose,v", "If present verbose option will be used")

    // DCDC specific options
    ("read,r", "If present reads data from DC/DC converters")
    ("VLimits", "If present sets the Voltage limits for the 4 LAMBs DC/DC converters at ~1.27V")

    // ("chipnum", value< std::string > ()->default_value("3"), "The chips to read. Default value is 3.")
    // ("npatt", value< std::string > ()->default_value("2048"), "The number of patterns to be written. Default value is 2048.")
    // ("offset", value< std::string > ()->default_value("0"), "The address fo the first pattern. Default value is 0.")
    // ("rb", "If present, then a \"random bank\" will be used")
    // ("dcbits", value<int>()->default_value(2), "number of dont'care bits for each layer, default 2")
    // ("disable_xoram", value<int>()->default_value(0), "disable xoram clock distribution, default 0")
    // ("disable_top2", value<int>()->default_value(0), "disable top2 clock distribution, default 1")
    // ("AM06_disable_mask", value< std::string > ()->default_value("0"), "Disable any of the 11 AM06 pattern blocks. If different from0, it ignores disable_xoram and disable_top2.")
    // ("thr", value< std::string > ()->default_value("8"), "threshold, default 8")
    // ("bankType", value< std::string > ()->default_value("1"), "0 for ROOT bank file. 1 for text bank file. Default value is 1.")

    // ("dump_patt_file", value< std::string >()->default_value("dump_patt.txt"), "UNUSED OPTION")
    // ("pattern_file, pf", value< std::string >()->default_value("pattern.txt"), "pattern file name, if rb is set this is the file in which patterns will be dumped. It can be an unspecified parameter at any position")
    // ("ssoff","If present the SS offset is used to avoid SS=0")
    // ("force", "If used force the writing of the bank in the board")
    // ("nojtagcfg","if used all the preliminary call to setup jtag are skipped")
    ;

  //  positional_options_description pd; 
  //  pd.add("pattern_file", 1);

  variables_map vm;
  try {
    //    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help 
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }

  // common options
  int int_verbose = vm.count("verbose")>0 ? 1 : 0;
  int slot = daq::ftk::string_to_int( vm["slot"].as<std::string>() );

  bool VLimits = vm.count("VLimits")>0 ? true : false;
  bool read = vm.count("read")>0 ? true : false;

  // int chipnum = daq::ftk::string_to_int( vm["chipnum"].as<std::string>() );
  // int npatt = daq::ftk::string_to_int( vm["npatt"].as<std::string>() );
  // int offset = daq::ftk::string_to_int( vm["offset"].as<std::string>() );
  // int dcbits = vm["dcbits"].as<int>();
  // int dis_xoram = vm["disable_xoram"].as<int>();
  // int dis_top2 = vm["disable_top2"].as<int>();
  // int AM06_disable_mask = daq::ftk::string_to_int( vm["AM06_disable_mask"].as<std::string>() );
  // int thr = daq::ftk::string_to_int( vm["thr"].as<std::string>() );
  // int use_random_bank = vm.count("rb")>0 ? 1 : 0;
  // int type = daq::ftk::string_to_int( vm["bankType"].as<std::string>() );
  // bool test_patterns = vm.count("test_patterns")>0 ? true : false;
  // bool force_write = vm.count("force")>0 ? true : false;
  // bool only_load = vm.count("nojtagcfg") ? true : false;

  bool verbose(false);
  if (int_verbose==1) verbose=true;

  //  std::string pattfname = vm["pattern_file"].as<std::string>();
  //  daq::ftk::assert_string_parameter_not_empty( pattfname, std::string("pattern_file") ); // checking that is not empty

  // create the AMB object
  daq::ftk::AMBoard myAMB(slot);
  if(verbose) myAMB.setVerbosity(daq::ftk::AMBoard::DEBUG);

  if (VLimits) myAMB.DCDC_setVLimits();
  if (read) myAMB.DCDC_read();

  return 0;
}

