/** @file PhaseTest.h 
 *  @auth Nicolo' Vladi Biesuz nicolo.vladi.biesuz@cern.ch
 *  
 *  This file contains the implementation of struct PhaseTest methods
 */

#include "ambslp/PhaseTest.h"      

namespace daq{
  namespace ftk{

    void PhaseTest::Initialize(){
      if(m_ChipConfig != nullptr){
        //TODO Warning here!!
        try{
          Delete();
        } catch (daq::ftk::FTKIssue & e){
        }
      }
      try{
        m_ChipConfig = new PhaseCut[16];
        m_ChipConfig[0 ].SetCut(  0.15,  0.19,  0.71,  0.88  );
        m_ChipConfig[1 ].SetCut(  0.09,  0.14,  0.86,  0.92  );
        m_ChipConfig[2 ].SetCut(  0.10,  0.14,  0.88,  0.93  );
        m_ChipConfig[3 ].SetCut(  0.08,  0.12,  0.89,  0.93  );
        m_ChipConfig[4 ].SetCut(  0.03,  0.28,  0.71,  0.98  );
        m_ChipConfig[5 ].SetCut(  0.16,  0.21,  0.80,  0.85  );
        m_ChipConfig[6 ].SetCut(  0.08,  0.17,  0.85,  0.94  );
        m_ChipConfig[7 ].SetCut(  0.15,  0.20,  0.84,  0.88  );
        m_ChipConfig[8 ].SetCut(  0.02,  0.16,  0.87,  1.00  );
        m_ChipConfig[9 ].SetCut(  0.01,  0.06,  0.56,  1.00  );
        m_ChipConfig[10].SetCut(  0.03,  0.10,  0.61,  0.98  );
        m_ChipConfig[11].SetCut(  0.02,  0.41,  0.56,  1.00  );
        m_ChipConfig[12].SetCut(  0.13,  0.36,  0.85,  0.90  );
        m_ChipConfig[13].SetCut(  0.08,  0.15,  0.51,  0.95  );
        m_ChipConfig[14].SetCut(  0.07,  0.15,  0.88,  0.95  );
        m_ChipConfig[15].SetCut(  0.06,  0.13,  0.88,  0.95  );
      }catch(std::exception & e ){
        throw daq::ftk::ftkException(ERS_HERE, name_ftk(), std::string("Unable to allocate m_ChipConfig. Re-throwing exception. This may cause an error during configuration!\t")+ e.what());
      }
    }

    void PhaseTest::Delete(){
      if(m_ChipConfig != nullptr){
        try{
          delete [] m_ChipConfig;
          m_ChipConfig = nullptr;
        } catch (std::exception & e){
          throw daq::ftk::ftkException(ERS_HERE, name_ftk(), std::string("Unable to de-allocate m_ChipConfig. Throwing exception.\t")+ e.what());
        }
      }else{
        throw daq::ftk::ftkException(ERS_HERE, name_ftk(), "Unable to de-allocate m_ChipConfig. m_ChipConfig is already set to nullptr.");
      }
    }

    pair<CutWindow, CutWindow> PhaseTest::GetChipConfig( unsigned int ChipNumber){
      CutWindow low = m_ChipConfig[ChipNumber].m_LowerCut;
      CutWindow up  = m_ChipConfig[ChipNumber].m_UpperCut;
      pair<CutWindow, CutWindow> config = make_pair(low, up);
      return config;
    }
  }
}






















