
#include "ambslp/monitoring_functions.h"
#include "ftkvme/VMEInterface.h"
#include "ambslp/ambslp_vme_regs.h"

using namespace daq::ftk;


TemperatureMonitor::TemperatureMonitor() {}

TemperatureMonitor::~TemperatureMonitor() {}

HWTemperature * TemperatureMonitor::readTemperature( VMEInterface *vme) {

  return read( vme );
}



unsigned int getTempFromRegister (unsigned int sensor, int lambNumber );


LambTemperatureMonitor::LambTemperatureMonitor() : TemperatureMonitor() {}

LambTemperatureMonitor::~LambTemperatureMonitor() {}

HWTemperature * LambTemperatureMonitor::read( VMEInterface * vme )  {

// uncomment the lines below when the firmware with the temperature addresses is in place
//
//  LAMB_TEMP_SENSx are defined in ambslp_vme_regs.h
//
//  whether SENS1 is on the right or on the left side of the LAMB, is yet not know.
//  therefore,  "left" actually means "sensor1"
//             "right" means "sensor2"
//
//  u_int leftSensor = vme->read_word( LAMB_TEMP_SENS1 /*0x00000030*/ );
//  u_int rightSensor = vme->read_word( LAMB_TEMP_SENS2 /*0x00000034*/ );

// place-holder values
  u_int leftSensor = 3141592653;
  u_int rightSensor = 2718281828;

  for (  int iLambNum=0; iLambNum<4; iLambNum++ ) {

    lambTemperature[iLambNum].leftSensor = getTempFromRegister( leftSensor, iLambNum);  
    lambTemperature[iLambNum].rightSensor = getTempFromRegister( rightSensor, iLambNum);  
  }

  return this->lambTemperature;
}


unsigned int getTempFromRegister (unsigned int sensor, int lambNumber ) {

  // the sensor variable is a 32-bit word, composed by 4 words of 8 bit each:
  //
  //   lamb3_sensor|lamb2_sensor|lamb1_sensor|lamb0_sensor
  //   <  31to24  > <  23to16  > <  15to 8  > <  7 to 0  >
  //   MSB                                             LSB
  //
  //   therefore: shift some word units, then AND-mask with the (wordSize) LSB bit
  //
  int wordSize = 8;
  int shiftSize = lambNumber*wordSize;
  int matchingMask = 0xff;

  return ( sensor >> shiftSize ) & matchingMask;

}
