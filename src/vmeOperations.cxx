
#include "ambslp/vmeOperations.h"


#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "ambslp/ambslp.h"
#include "ambslp/exceptions.h"
#include "ftkvme/VMEInterface.h"

#include <sstream>

VMEInterface * connectToVME_OrThrow( int slot ) {

  VMEInterface *vme = 0;

    try {
      vme = VMEManager::global().amb(slot);
    }
    catch (daq::ftk::VmeError &ex) {
      std::stringstream stream;
      stream << "VME access via interface manager has failed for ambslp: cannot connect to AMBoard in slot ";
      stream << slot;
      daq::ftk::ftkException issue(ERS_HERE, name_ftk(), stream.str() );
      throw issue;
    }

  return vme;
}


int readBoard_OrThrow( VMEInterface* vme, int address ) {

  int register_value = 0;
  try {
    register_value = vme->read_word( address );
  }
  catch(...) {
    std::stringstream stream;
    stream << "VME read_word has failed for ambslp: cannot get register ";
    stream << std::hex << address;
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), stream.str() );
    throw issue;
  }
 
  return register_value;
}


void writeToBoard_OrThrow( VMEInterface *vme, int address, int value ) {

  try {
    vme->write_word( address, value );
  }
  catch(...) {
    std::stringstream stream;
    stream << "VME write_word has failed for ambslp: cannot write register ";
    stream << std::hex << address;
    stream << " with value: ";
    stream << std::hex << value;
    daq::ftk::ftkException issue(ERS_HERE, name_ftk(), stream.str() );
    throw issue;
  }
 
}




