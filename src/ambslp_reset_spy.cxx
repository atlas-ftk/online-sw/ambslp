/*                vme_test.c               */
/*   Program to test vme in SLIM board   */

//#include <assert.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <iostream>
//#include <math.h>
//#include <unistd.h>
//#include "rcc_error/rcc_error.h"
//#include "vme_rcc/vme_rcc.h"
//#include "ambslp/ambslp.h"
#include "ambslp/ambslp_mainboard.h"
//#include "ambslp/ambslp_vme_jtag_func.h"
//#include "ambslp/ambslp_jtag_func.h"



#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>


int main(int argc, char **argv) { 
	
  using namespace  boost::program_options ;

  ///////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options
  //
  options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("slot", value< std::string >()->default_value("15"), "The card slot")
    ("road", value< std::string >()->default_value("1"),  "Enable the reset of the output spy buffer")
    ("hit",  value< std::string >()->default_value("1"),  "Enable the reset of the input  spy buffer")
    ("verbose,v",                                         "If present verbose option will be used")
    ;
        
  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).run(), vm);
  }
  catch( ... ) {    // In case of errors during the parsing process, desc is printed for help 
    std::cerr << desc << std::endl; 
    return 1;
  }

  notify(vm);

  if( vm.count("help") ) {    // if help is required, then desc is printed to output

    std::cout << std::endl <<  desc << std::endl ; 
    return 0;	
  }

  int  slot    = daq::ftk::string_to_int( vm["slot"].as<std::string>() );
  int  road    = daq::ftk::string_to_int( vm["road"].as<std::string>() );
  int  hit     = daq::ftk::string_to_int( vm["hit"].as<std::string>() );
  bool verbose = vm.count("verbose")>0? true : false;

  daq::ftk::AMBoard _AM_board(slot, verbose? daq::ftk::AMBoard::DEBUG : daq::ftk::AMBoard::WARNING);

  return _AM_board.resetSpyBuffers(hit, road);

  // return daq::ftk::ambslp_reset_spy(slot, hit, road);
}


