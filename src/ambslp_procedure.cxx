#include "ambslp/AMBoard.h"
#include <ambslp/ambslp_mainboard.h>

#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <string>
#include <cstdlib>
using namespace boost::program_options;
using namespace std;

//string pbankpath("random");
string PartName("FTK.data.xml"), RMName("Amb1");
//int bankType(1);
//bool ForceWrite(false);
bool doStandaloneTest(false);
bool testConfLoop(false);
string testFilePath("");

unsigned int npattsPerChip(1);

int runBoardCommand(daq::ftk::AMBoard &amb, string transition) {
  cout << "Executing procedure: " << transition << endl;
  if (transition=="configure" or transition=="config") {
    amb.RC_configure();
  }
  else if (transition=="connect") {
    amb.RC_connect();
  }
  else if (transition=="prepareForRun" or transition=="run" or transition=="prepare") {
  	amb.RC_prepareForRun();
  }
  else if (transition=="stop") {
    amb.RC_stop();
  }
  else if (transition=="unconfigure" or transition=="unconfig") {
    amb.RC_unconfigure();
  }
  else if (transition=="checklinks") {
    amb.RC_checkLinks();
  }
  else if (transition=="status") {
    daq::ftk::ambslp_status( amb.getSlot(), 1, "");
  }
  else {
    cerr << "** no procedure found, exiting" << endl;
    return -1;
  }
  cout << "Concluded procedure: " << transition << endl;

  return 0;
}

int main(int argc, char *argv[]) {

	//setting the FTK_LOAD enviroment variable to force the loading of the configurations
	std::cout << "ambslp_procedures: setting FTK_LOAD_MODE to true" << std::endl;
	setenv("FTK_LOAD_MODE", "1", true);

  vector<string> transitions;
  /////////////////////////////////////////////////////////////////
  //  Parsing parameters using namespace boost::program:options  //
  /////////////////////////////////////////////////////////////////
  options_description desc("Allowed options");
//  const unsigned int useDefaultDummyHit = 0xdeadbeef;
  desc.add_options()
    ("help,h", "produce help message")
    ("PartName,P", value<string>(&PartName)->default_value(PartName), "Partition name, the partition from wich load the configuration")
    ("RMName,R", value<string>(&RMName)->default_value(RMName), "ReadoutModule name, the RM in use")
    ("slot", value<int>()->default_value(15), "The card slot. Default value is 15.")
    ("transition",value< vector<string> >(&transitions)->composing(),"describe the transition")
    ("testFilePath", value<string>(&testFilePath)->default_value(testFilePath), "Path to the input hit file in the standalone test")
/* 
   ("bankpath", value<string>(&pbankpath)->default_value(pbankpath), "Pattern bank, value random makes a random bank")
    ("banktype,T", value<int>(&bankType)->default_value(bankType), "Pattern bank file type")
    ("nPattsPerChip,N", value<unsigned int>(&npattsPerChip)->default_value(npattsPerChip), "Number of patterns per chip, number tot used")
    ("ForceWrite", value<bool>(&ForceWrite)->default_value(ForceWrite), "Force to write the bank")
    ("dummyhit",value<unsigned int>()->default_value(useDefaultDummyHit), "Choose the dummyhit value. Default 0xdeadbeef indicates to use runControl default value.")
    ("doStandaloneTest", value<bool>(&doStandaloneTest)->default_value(doStandaloneTest), "Turn on the standalone test")
    ("testConfLoop", value<bool>(&testConfLoop)->default_value(testConfLoop), "Enable the loop mode in the standalone test")
    ("testFilePath", value<string>(&testFilePath)->default_value(testFilePath), "Path to the input hit file in the standalone test")
*/
    ("interactive,I","Run interactively")
    ;

  positional_options_description pd;
  pd.add("transition", -1);

  variables_map vm;
  try {
    store(command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
  }
  catch( ... ) // In case of errors during the parsing process, desc is printed for help
    {
      std::cerr << desc << std::endl;
      return 1;
    }
  notify(vm);

  if( vm.count("help") ) // if help is required, then desc is printed to output
    {
      std::cout << std::endl <<  desc << std::endl ;
      return 0;
    }


  int slot = vm["slot"].as<int>();
  bool interactive = vm.count("interactive")>0;

	std::string partition = "oksconfig:ftk/partitions/" + PartName;	
	std::cout << "Partition Name: " << partition << std::endl;
	std::cout << "RMName: " << RMName << std::endl;

  // Read some info from the DB
  std::cout << "Loading OKS Configuration." << std::endl;
  std::unique_ptr<::Configuration> db;
  try 
	{
    db.reset(new::Configuration(partition));
  } 
	catch(daq::config::Generic e) 
	{
    std::cerr << "ERROR: Cannot load OKS partition." << std::endl;
		throw e; 
    return -1;
  }
  

  const daq::ftk::dal::ReadoutModule_PU* settings = db->get<daq::ftk::dal::ReadoutModule_PU>(RMName);
  if(settings==0)
  {
    std::cerr << "Invalid tag " << RMName << "! exiting..." << std::endl;
    return -1;
  }
  
  daq::ftk::AMBoard myAMB(slot);
  myAMB.RC_setup(settings);	
  
  //if (vm.count("testFilePath")>0)  {
  if (vm["testFilePath"].as<std::string>() != "")  {
     myAMB.setTestFilePath(testFilePath);
  }

  cout << "Controlling AMB in slot: " << slot << endl;

/*
  cout << "Loading bank (" << bankType << "): " << pbankpath << " - npatt=" << npattsPerChip << endl;

  daq::ftk::AMBoard myAMB(slot);
*/
  //myAMB.setDoStandaloneTest(vm["doStandaloneTest"].as<bool>());
  //myAMB.setTestConfLoop(vm["testConfLoop"].as<bool>());
  //myAMB.setTestFilePath(vm["testFilePath"].as<string>());
/*
  // setup the dummy hit value
  if (vm["dummyhit"].as<unsigned int>()!=useDefaultDummyHit)
    myAMB.setDummyHit(vm["dummyhit"].as<unsigned int>());

  if (pbankpath == "random") {
    // prepare a random pattern bank
    if (myAMB.prepareRandomPatternBank(true)<0) {
      cerr << "Error preparing the random bank" << endl;
      return -1;
    }
  }
  else if (pbankpath.substr(0,3) == "seq") {
    unsigned int maxSS = 0xffff;
    if (pbankpath.size()==3) {
      // if the string name is just seq the maxSS remains set to the number of patterns per chip
    }
    else {
      // the rest of the value is used as max SS value: e.g. seq2048
      maxSS = stoi(pbankpath.substr(3));
    }
    cout << "Creating sequential pattern bank with " << npattsPerChip << " patt/chip and maxSS=" << maxSS << endl;

    if (myAMB.prepareSequentialBank(maxSS, true)<0) {
      cerr << "Error preparing the sequential bank" << endl;
      return -1;
    }
  }
  else {
    cout << "Loading pattern bank from " << pbankpath << endl;
    std::vector<uint32_t> out;
    if (myAMB.readPatternBank(bankType,pbankpath.c_str(), false, out)<0) {
      cerr << "Error reading the bank" << endl;
      return -1;
    }
  }
*/

  if (vm.count("transition")>0)  {
    for (string transition: vm["transition"].as< vector<string> >()) {
      if (runBoardCommand(myAMB,transition)<0) {
        cerr << "Error in procedure execution, terminating" << endl;
        return -1;
      }
    }
  }

  while (interactive) {
    cout << "prompt> " << ends;
    string transition;
    cin >> transition;
    if (!cin) break;
    else if (transition=="exit" || transition=="quit") {
      cout << "Bye!" << endl;
      return -1;
    }
    else if (runBoardCommand(myAMB,transition)<0) {
      cerr << "Error in procedure execution" << endl;
    }
  };

  return 0;
}
