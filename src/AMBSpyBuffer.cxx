// Functions used by AMB for spy buffer access using VME block transfer

#include "ambslp/AMBSpyBuffer.h"

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>

namespace daq { 
  namespace ftk {

    // just the base constructor call is sufficient so far
    AMBSpyBuffer::AMBSpyBuffer(unsigned int status_addr, 
				 unsigned int base_addr,
				 VMEInterface *vmei
				) : vme_spyBuffer(status_addr, base_addr, vmei) {
    }

    //---------------------------------------------------

    AMBSpyBuffer::~AMBSpyBuffer(){}

  } // namespace ftk
} // namespace daq
