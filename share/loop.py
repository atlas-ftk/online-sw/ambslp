
import os, sys, optparse

def parseInputArgs():

    parser = optparse.OptionParser(description='AMB job configuration.')

    parser.add_option('-s', '--slot', default="15",
                      help='Target slot', dest="slot")
    parser.add_option('--chainlength', default="4",
                      help='Length of JTAG chain in the targed AMChips', dest="chainlength")
    parser.add_option('--chipnum', default="15",
                      help='Number of AMChips in a JTAG chain', dest="chipnum")
    parser.add_option('--ambhitfile', default="gigi_hit.hit",
                      help='Hit file to be fed to HIT FIFO via VME', dest="amb_hit_file")
    parser.add_option('--auxhitfile', default="real_hit.hit",
                      help='Hit file to be fed to AUX FIFO via VME', dest="aux_hit_file")
    parser.add_option('--reconfigure', default=True,
                      help='Reconfigure everything?', type="int", dest="reconfigure")
    parser.add_option('--useaux', default=True,
                      help='Use AUX?', type="int", dest="use_aux")
    parser.add_option('--rewritehit', default=True,
                      help='Rewrite hit file?', type="int", dest="rewrite_hit")
    parser.add_option('--onlyonelamb', default=True,
                      help='Only one LAMB?', type="int", dest="only_one_lamb")

    (options, args) = parser.parse_args()
    return options

def globalInit(options):

    slt=options.slot
    
    print "## Global initialisation.."
    os.system("ambslp_init_main --slot "+slt+" >> log")
    os.system("ambslp_init_main --amb_lamb 1 --slot "+slt+" >> log")
    os.system("ambslp_config_reg_main --boardver 1 --slot "+slt+" >> log")
    os.system("ambslp_reset_spy_main --slot "+slt+" >> log")
    os.system("ambslp_status_main --slot "+slt+" >> log")

def configureAMB(options):
    
    slt=options.slot
    cn=options.chipnum
    cl=options.chainlength

    print "## Configuring AMB.."
    os.system("ambslp_amchip_8b_10b_main --lambver 1 --slot "+slt+" >> log")
    os.system("ambslp_amchip_jpatt_cfg_main --thr 15 --tmode 1 --disable_pflow 1 --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_jpatt_cfg_main --thr 15 --tmode 1 --disable_pflow 1 --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_init_evt_main --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_disable_bank_main --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_init_evt_main --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_write_test_bank_main --chainlength "+cl+" --chipnum "+cn+" --offset 0 --npatt 20 --slot "+slt+" >> log")
    os.system("ambslp_amchip_init_evt_main --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_jpatt_cfg_main --thr 1 --tmode 0 --disable_pflow 0 --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_jpatt_cfg_main --thr 1 --tmode 0 --disable_pflow 0 --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_amchip_init_evt_main --chainlength "+cl+" --chipnum "+cn+" --slot "+slt+" >> log")
    os.system("ambslp_reset_spy_main --slot "+slt+" >> log")
    os.system("ambslp_init_main --slot "+slt+" >> log")
    
    if slt=='15':
        base="78"
    elif slt=='12':
        base="60"
    else:
        print "Slot not 15 or 12"
        sys.exit(0)

    if options.only_one_lamb:
        os.system("vme_poke 0x"+base+"0040d0 f0ff >> log")
        os.system("vme_peek 0x"+base+"0040c0 >> log")
    else:
        pass

def writeHitAux(options):
    
    slt=options.slot
    hfile=options.aux_hit_file

    print "## Writing higs in AUX FIFO.."
    os.system("aux_reset_main --slot "+slt+" --fpga 1 >> log")

    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x1 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x2 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x3 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x4 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x5 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x6 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x7 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x8 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0x9 >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0xa >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0xb >> log")
    os.system("aux_load_tx_fifo_main --slot "+slt+" --fpga 1 --file "+hfile+" --buffer 0xc >> log")

    os.system("aux_write_main --slot "+slt+" --fpga 1 0x30 0xFFFF0002 >> log")

def writeHitAmb(options):

    slt=options.slot
    hfile=options.amb_hit_file

    print "## Writing higs in AMB FIFO.."
    os.system("ambslp_feed_hit_main --slot "+slt+" --loopfifovme 0 "+hfile+" >> log")

def dumpRoadAmb(options):

    slt=options.slot

    print "## Dumping AMB ROAD spy buffer"
    os.system("ambslp_out_spy_main --slot "+slt+" >  road_spy_"+slt)

## Main Routin ##
if __name__ == "__main__":

    options = parseInputArgs()
   
    if options.reconfigure:
        globalInit(options)
        configureAMB(options)
    else:
        pass

    if options.use_aux:
        writeHitAux(options)
    else:
        writeHitAmb(options)

    dumpRoadAmb(options)
