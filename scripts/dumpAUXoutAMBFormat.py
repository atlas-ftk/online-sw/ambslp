#!/usr/bin/env python

import os, sys, optparse

parser = optparse.OptionParser(description='AUX spybuffer converter.')

parser.add_option('-s', '--slot', default="15",
                    help='Target slot', type="int", dest="slot")
parser.add_option('-f', '--fpga', action='append',
                    help='AUX FPGA to query', type="int", dest="fpga")
parser.add_option('-o', '--output', default='input.txt',
                    help='Location to store merged buffers.', type="str", dest="output")

(options, args) = parser.parse_args()

if options.fpga==None: options.fpga=[2,1] # Default is all FPGAs

sbs1=[0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10]
sbs2=[0x0a,0x0b,0x0c,0x0d]

print options

# All data
streamdata=[]
for fpga in options.fpga:
    print 'Read AUX FPGA %d'%fpga
    
    if fpga==1:
        fpgastreamdata=[[],[],[],[],[],[],[],[]]
        nsb=0
        for sb in sbs1:
            fpgadata=os.popen("aux_read_buffer_main --slot %d --fpga %d %d"%(options.slot,fpga,sb)).readlines()

            # Add data to list
            started=False
            for dataline in fpgadata[8:-1]:
                parts=dataline.split()
                if not started and parts[0]!='[0001]:': continue
                started=True
            
                fpgastreamdata[nsb].append(parts[1])
            nsb+=1
        streamdata+=fpgastreamdata

    if fpga==2:
        fpgastreamdata=[[],[],[],[]]
        nsb=0
        for sb in sbs2:
            fpgadata=os.popen("aux_read_buffer_main --slot %d --fpga %d %d"%(options.slot,fpga,sb)).readlines()

            # Add data to list
            started=False
            for dataline in fpgadata[8:-1]:
                parts=dataline.split()
                if not started and parts[0]!='[0001]:': continue
                started=True
            
                fpgastreamdata[nsb].append(parts[1])
            nsb+=1

        streamdata+=fpgastreamdata

#print streamdata

print 'Dump output to %s'%(options.output)
fh=open(options.output,'w')
for fpgastreamdata in streamdata:
    event=[]
    for word in fpgastreamdata:
        fh.write('%s\n'%word)
fh.close()

