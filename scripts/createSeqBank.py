#!/usr/bin/env python

import sys

outpath = sys.argv[1]

NPATTS = 1024*32
NCHIPS = 4

outfile = open(outpath,"w")
outfile.write("%d 8\n" % (NPATTS*NCHIPS))

for ichip in xrange(NCHIPS) :
  for locpatt in xrange(NPATTS) :
    ipatt = locpatt + ichip*NPATTS
    outfile.write("%d " % ipatt)
    for i in xrange(8) :
      outfile.write("%d " % locpatt)
    outfile.write("0 0\n")

outfile.close()
