#/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'spybuffer file name must be provided'
    exit 0
fi

if [[ $# -gt 1 ]] ; then
    echo 'too many arguments'
    exit 0
fi

input_spy_buff_name=$1

cat  $input_spy_buff_name | awk '{print "0x"$2" 0x"$4" 0x"$6" 0x"$8" 0x"$10" 0x"$12" 0x"$14" 0x"$16" 0x"$18" 0x"$20" 0x"$22" 0x"$24}' > ${input_spy_buff_name}.tmp
sed 's/0xf7/0x8f7/g' ${input_spy_buff_name}.tmp > ${input_spy_buff_name}.tmp2
sed 's/0x0/0x00/g' ${input_spy_buff_name}.tmp2 > ${input_spy_buff_name}.data

rm -f ${input_spy_buff_name}.tmp