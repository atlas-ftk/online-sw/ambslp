#!/usr/bin/env tdaq_python

import random
import sys

'''
    script for the generation of AMB random testVectors.
    The BF od the testvector can be choose changing the hex value in the return of the rand function.
    By default BF=4 is generated (using 0x00ff00ff).
'''

def rand():
  return random.getrandbits(32) &  0x00ff00ff # for BF=4

for x in range (2000):
  for x in range (5):
    print "0x0%08x" %(rand()),
  print "0x400000000",
  for x in range (3):
    print "0x0%08x %s" %(rand(), "0x400000000"),
  print ""
print "0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001 0x8f7000001"

