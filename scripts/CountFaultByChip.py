#!/usr/bin/env python

import sys
import re

file = open(sys.argv[1],"r")

Conf = -1
Loop = -1
Event = 0
EventErrBits = [0 for i in range(64)]

reGood = re.compile("  [a-f0-9]{8} [a-f0-9]{8}")
reErr = re.compile("^[*+-]")
reLoop = re.compile("CONF")
reEvent = re.compile("^1 f78")

ErrLog = [[0 for i in range(64)] for j in range(50)]
PartLog = [[0 for i in range(64)] for j in range(50)]
MissLog = [[0 for i in range(64)] for j in range(50)]
ExtraLog = [[0 for i in range(64)] for j in range(50)]
GoodLog = [[0 for i in range(64)] for j in range(50)]

ErrLog1st = [[0 for i in range(64)] for j in range(50)]

ErrEvtLog = [[0 for i in range(64)] for j in range(50)]
PartEvtLog = [[0 for i in range(64)] for j in range(50)]
MissEvtLog = [[0 for i in range(64)] for j in range(50)]
ExtraEvtLog = [[0 for i in range(64)] for j in range(50)]
GoodEvtLog = [[0 for i in range(64)] for j in range(50)]


for curline in file :
  line = curline.rstrip()
  if reGood.match(line) : # good road match
    elems = line.split()
    chip = -1
    road = int(elems[0],16)
    chip = (road>>17)&0x3f
    GoodLog[Conf][chip] += 1
  elif reErr.match(line) : # matching error, any kind
    elems = line.split()
    chip = -1
    road = int(elems[1],16)
    chip = (road>>17)&0x3f
    ErrLog[Conf][chip] += 1
    if Loop==1 and Event==0 : # very first event
      ErrLog1st[Conf][chip] += 1
      
    if elems[0] == "+" :
      EventErrBits[chip] |= 1<<0
      ExtraLog[Conf][chip] += 1
    elif elems[0] == "-" :
      EventErrBits[chip] |= 1<<1
      MissLog[Conf][chip] += 1
    elif elems[0] == "*" :
      EventErrBits[chip] |= 1<<2
      PartLog[Conf][chip] += 1
  elif reLoop.match(line) : # start of a loop
    elems = line.split()
    Conf = int(elems[1][0:-1])-1
    Loop = int(elems[3][0:-1])
    Event = 0 # reset the event counter
  elif reEvent.match(line) :
    Event += 1 # increment the event counter, regardless of the real Lvl1 ID

    for chip in range(64) :
      ebits = EventErrBits[chip]
      if ebits != 0 :
        ErrEvtLog[Conf][chip] += 1
      else :
        GoodEvtLog[Conf][chip] += 1

      if ebits & 1 :
        ExtraEvtLog[Conf][chip] += 1
      if ebits & 2 :
        MissEvtLog[Conf][chip] += 1
      if ebits & 4 :
        PartEvtLog[Conf][chip] += 1
      EventErrBits[chip] = 0 # reset
      

print "Good roads summary"
for chip in range(48) :
  for conf in range(20) :
    print GoodLog[conf][chip],
  print

print "Error roads summary"
for chip in range(48) :
  for conf in range(20) :
    print ErrLog[conf][chip],
  print

print "Error roads summary (1st event)"
for chip in range(48) :
  for conf in range(20) :
    print ErrLog1st[conf][chip],
  print

print "Missing roads summary"
for chip in range(48) :
  for conf in range(20) :
    print MissLog[conf][chip],
  print

print "Extra roads summary"
for chip in range(48) :
  for conf in range(20) :
    print ExtraLog[conf][chip],
  print

print "Partial roads summary"
for chip in range(48) :
  for conf in range(20) :
    print PartLog[conf][chip],
  print

print "Good events summary"
for chip in range(48) :
  for conf in range(20) :
    print GoodEvtLog[conf][chip],
  print

print "Events with errors summary"
for chip in range(48) :
  for conf in range(20) :
    print ErrEvtLog[conf][chip],
  print

print "Events with missing roads summary"
for chip in range(48) :
  for conf in range(20) :
    print MissEvtLog[conf][chip],
  print

print "Evensts with extra roads summary"
for chip in range(48) :
  for conf in range(20) :
    print ExtraEvtLog[conf][chip],
  print

print "Events with partial roads summary"
for chip in range(48) :
  for conf in range(20) :
    print PartEvtLog[conf][chip],
  print
