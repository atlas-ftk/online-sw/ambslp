#!/bin/bash

SLOTLDC=15 # Slot that the main AUX card is plugged in

# Step 1: Just reset everything
for i in $(seq 1 2)
do
    aux_reset_main --slot ${SLOTLDC} --fpga ${i}
done
