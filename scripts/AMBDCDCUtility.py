#!/usr/bin/env tdaq_python
 
# Author: G. Volpi, control of the DCDC converters voltage and working conditions

import sys
import subprocess
import math
import argparse
import warnings
import logging

from time import sleep

verb = 0
isAMBv4 = False
VOUT_SCALE=1.6666666666666667 # value for boards with 1.000 nominal tesion
refValue = 1.15

PMBUSWrite = 0x00008000
PMBUSBarracuda = 0x00002900
PMBUSFPGA = 0x00000a00         
PMBUSLambs = [0x00001000, 0x00000e00, 0x00001100, 0x00000f00]
#VME registers that allows to access the PMBUS
#COMMAND register
PMBUSCmd = 0x00002160
#DATA read register
PMBUSData = 0x00002164
AMB_HIT_DUMMY_HIT_WORD = 0x00002168
AMB_CONTROL_DCS_RUN = 0x00006024

def VMEPeek(slot, regaddr, delay=0):
  """Do a vme_peek for a specific address, a delay can be specified, default .1"""
  vmeaddr = (slot<<27) | regaddr
  args = ["vme_peek",hex(vmeaddr)]
  if verb :
    print slot, regaddr, args
  res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  
  sleep(delay)
  return int(res,16)

def VMEPoke(slot, regaddr, value, delay=.1):
  """Do a vme_poke for a specific address and value, a delay can be specified, default .1"""
  vmeaddr = (slot<<27) | regaddr
  args = ["vme_poke",hex(vmeaddr), hex(value)]
  if verb :
   print slot, regaddr, value, args
  res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  
  sleep(delay)
  return None

def PMBusRead(slot, device, addr, delay=.1):
  """Perform a basic read of PMBus register. An integer is returned"""
  VMEPoke(slot, PMBUSCmd, device | addr, delay)
  return VMEPeek(slot, PMBUSData)

def compl2(x,n):
  """Return the complement to 2 integer"""
  if x >> n-1 == 1:
    return x - ( 1 << n)
  return x

def DCDCValueM11E5(val):
  """Convert a DCDC value from PMBUS into a float, the value is supposed to have
  11-bit mantissa, 5-bit exponent"""
  maskMantissa = 0x7ff
  mantissaLen  = 11
  expLen       = 5

  exp      = compl2(val >> mantissaLen, expLen)
  mantissa = compl2(val & maskMantissa, mantissaLen)
  
  return math.pow(2,exp)*mantissa

def DCDCValueM16EU(val, exp):
  """Return the floaing point DCDC value when the mantissa is 16-bit number
  encoded as complement 2 and the exponent value is known"""  
  mantissa=  compl2(val, 16)

  return math.pow(2,exp)*mantissa  


def DCDCValueUM16EU(val, exp): 
  """Convert an integer in a floating point. The mantissa is 16
  bits, representing unsigned number, the exponent of 2 is passed
  as parameter"""
  
  mantissa = val&0xffff
  return math.pow(2, exp)*mantissa
 
def SetOverThreshold(slot) :
  """Set the voltage threshold value for the DCDC converters""" 
  if isAMBv4 :
    print "Reset voltage to 1.00 V"
    cmdword = 0x40 | PMBUSWrite | 0x15350000
  else :
    print "Reset voltage to 1.15 V"
    cmdword = 0x40 | PMBUSWrite | 0x18f60000

  print "Reference word:", hex(cmdword)      
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)
  return None

def SetVoltage(slot) :
  """"Set the voltage provided by the DCDC converter related to the 
  LAMBs"""
  if isAMBv4 :
    print "Set correct voltage to 1.15 V"
    cmdword = 0x22 | PMBUSWrite | 0x00c40000
  else :
    if refValue == 1. : 
      print "Set correct voltage to 1 V"
      cmdword = 0x22 | PMBUSWrite | 0x00000000
    else : 
      print "Set correct voltage to 1.15 V"
      cmdword = 0x22 | PMBUSWrite | 0x02e30000
  print "Reference word:", hex(cmdword)    
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)

  return None

def CheckPower(arrOut, dummy = False):
  powerTestPassed = True
  minP = 0
  maxP = 5
  if(dummy):
    minP = 25
    maxP = 55
  for i in range(0,4) : # loop over the LAMbs
    if(i==0):
      power = arrOut['LAMB0Vout']*arrOut['LAMB0Iout']
    if(i==1):
      power = arrOut['LAMB1Vout']*arrOut['LAMB1Iout']
    if(i==2):
      power = arrOut['LAMB2Vout']*arrOut['LAMB2Iout']
    if(i==3):
      power = arrOut['LAMB3Vout']*arrOut['LAMB3Iout']
    if( (power) > maxP or (power) < minP):
      powerTestPassed = False
      print "LAMB", i, "did not pass the power consumption test (", minP, "< P <", maxP, " W)"    
  if(powerTestPassed):
    print "INFO: All LAMBs PASSED the power consumption test!"
  else:
    warnings.warn("WARNING: power consumption test not passed, details above")

  return powerTestPassed

  
def ReadDCDCInfo(slot, printSummary=False) :
  """Read the information from the DCDC converters through the PMBUS, if the the
  printSummary variable is True the method will also print a single line summary
  with the current format:
  TODO
  """
  summary = {}
  barracudaPassed = True
  lambVinPassed = True
  lambVoutPassed = True
  fpgaDCDCPassed = True

  # read the Barracuda
  status_word = PMBusRead(slot, PMBUSBarracuda, 0x79)&0xffff
  
  print "\nBarracuda, (%04x)" % status_word
  VinAD = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x88))
  VinOff = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0xd4))  
  VinGain = DCDCValueUM16EU(PMBusRead(slot, PMBUSBarracuda, 0xd3), 0)
  Vin = VinGain/8192*VinAD+VinOff
  if( Vin < 46 or Vin > 52):
    barracudaPassed = False

  VoutAD = DCDCValueUM16EU(PMBusRead(slot, PMBUSBarracuda, 0x8b), -12)
  VoutOff = DCDCValueM16EU(PMBusRead(slot, PMBUSBarracuda, 0xd2), -12)
  Vout = VoutAD+VoutOff
   
  IoutAD = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x8c))
  IoutGain = DCDCValueUM16EU(PMBusRead(slot, PMBUSBarracuda, 0xd6), 0)
  IoutOff = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0xd7))
  Iout = IoutGain/8192*IoutAD+IoutOff 
  
  # Barracuada power
  Pout = Iout*Vout
  
  Temp1 = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x8d))
  
  print "Vin =", Vin, "V"
  print "Vout =", Vout, "V", "Iout=", Iout, "A, Power =", Pout, "W"
  print "Temperature:", Temp1

  summary.update({'BarrVin': Vin, 'BarrVout': Vout, 'BarrIout': Iout, 'BarrTemp1': Temp1})
  
  # Read status of the DCDC that controls the FPGA, it should have 2 pages  
  VMEPoke(slot, PMBUSCmd, PMBUSWrite | PMBUSFPGA | 0x00000000)  
  status_word = PMBusRead(slot, PMBUSFPGA, 0x79)&0xffff
  print "\nFPGA DCDC, page 0 (%04x)" % status_word
  
  Vout = DCDCValueUM16EU(PMBusRead(slot, PMBUSFPGA, 0x8b), -9)
  if(Vout < 1.75 or Vout > 1.85):
    fpgaDCDCPassed = False
  Iout = DCDCValueM11E5(PMBusRead(slot, PMBUSFPGA, 0x8c))
  Temp1 = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x8d))
  Temp2 = DCDCValueM11E5(PMBusRead(slot, PMBUSBarracuda, 0x8e))
  
  print "Vout =", Vout, "V, Iout =", Iout, " A, Power =", Vout*Iout, " W"
  print "Temperature:", Temp1, Temp2
  
  VMEPoke(slot, PMBUSCmd, PMBUSWrite | PMBUSFPGA | 0x00010000)  
  status_word = PMBusRead(slot, PMBUSFPGA, 0x79)&0xffff
  print "\nFPGA DCDC, page 1 (%04x)" % status_word
  
  Vout = DCDCValueUM16EU(PMBusRead(slot, PMBUSFPGA, 0x8b), -9)
  if(Vout < 1.75 or Vout > 1.85):
    fpgaDCDCPassed = False
  Iout = DCDCValueM11E5(PMBusRead(slot, PMBUSFPGA, 0x8c))
  
  print "Vout =", Vout, "V, Iout =", Iout, " A, Power =", Vout*Iout, " W"
  print "Temperature:", Temp1
  
  for ilamb, laddr in enumerate(PMBUSLambs) : # loop over the LAMbs
    #use following lines for debugging
    #status = PMBusRead(slot, PMBUSBarracuda, 0x79)&0xffff
    #print "\nLAMB", ilamb, "(%04x)" % status
    #statusLAMB = PMBusRead(slot, laddr, 0x79)&0xffff
    #statusVoutLAMB = PMBusRead(slot, laddr, 0x7A)&0x00ff
    #statusIoutLAMB = PMBusRead(slot, laddr, 0x7B)&0x00ff
    #statusCMLLAMB = PMBusRead(slot, laddr, 0x7E)&0x00ff
    #print "\nstatus LAMB", ilamb, "(%04x)" % statusLAMB
    #print "Vout LAMB", ilamb, "expected (00): (%02x)" % statusVoutLAMB
    #print "Iout LAMB", ilamb, "expected (00): (%02x)" % statusIoutLAMB
    #print "CML  LAMB", ilamb, "expected (00): (%02x)" % statusCMLLAMB
    # read the input voltage
    Vin = DCDCValueM11E5(PMBusRead(slot, laddr, 0x88))
    #read Vout
    Vout = DCDCValueM16EU(PMBusRead(slot, laddr, 0x8b), -13)*VOUT_SCALE
    if(Vin < 11.5 or Vin > 12.5):
      lambVinPassed = False
    if(Vout < 1.1 or Vout > 1.2):
      lambVoutPassed = False
  
    # read the out current  
    IoutAD = DCDCValueM11E5(PMBusRead(slot, laddr, 0x8c))
    IoutGain = DCDCValueM11E5(PMBusRead(slot, laddr, 0x38))  
    IoutOff = DCDCValueM11E5(PMBusRead(slot, laddr, 0x39))
    ## Iout=((IoutGAINL0)*IoutADL0)+IoutOFFsetL0
    Iout = IoutGain*IoutAD+IoutOff

    Temp1 = DCDCValueM11E5(PMBusRead(slot, laddr, 0x8d))
    Temp2 = DCDCValueM11E5(PMBusRead(slot, laddr, 0x8e))
    
    print "Vin =", Vin, "V"
    print "Vout =", Vout, "V, ", "Iout=", Iout, "A, Power =", Vout*Iout, "W"
    print "DCDC converters temperatures: %f C, %f C" % (Temp1, Temp2)

    if(ilamb == 0):
      summary.update({'LAMB0Vin': Vin, 'LAMB0Vout': Vout, 'LAMB0Iout': Iout, 'LAMB0Temp1': Temp1, 'LAMB0Temp2': Temp2})
    if(ilamb == 1):
      summary.update({'LAMB1Vin': Vin, 'LAMB1Vout': Vout, 'LAMB1Iout': Iout, 'LAMB1Temp1': Temp1, 'LAMB1Temp2': Temp2})
    if(ilamb == 2):
      summary.update({'LAMB2Vin': Vin, 'LAMB2Vout': Vout, 'LAMB2Iout': Iout, 'LAMB2Temp1': Temp1, 'LAMB2Temp2': Temp2})
    if(ilamb == 3):
      summary.update({'LAMB3Vin': Vin, 'LAMB3Vout': Vout, 'LAMB3Iout': Iout, 'LAMB3Temp1': Temp1, 'LAMB3Temp2': Temp2})

  if not(barracudaPassed):
    warnings.warn("WARNING: Vin for barracuda is outside the expected range ( 1.75 < Vin < 1.85 V)")
  if not(fpgaDCDCPassed):
    warnings.warn("WARNING: FPGA DCDC Vout is outside the expected range (1.75 < Vout < 1.85 V)")
  if not(lambVinPassed):
    warnings.warn("WARNING: LAMB input voltage is outside the expected range (11.5 < Vin < 12.5 V)")
  if not(lambVoutPassed):
    warnings.warn("WARNING: LAMB output voltage is outside the expected range for at least one LAMB (1.10 < Vout < 1.2 V)")
  if( barracudaPassed and fpgaDCDCPassed and lambVinPassed and lambVoutPassed):
    print "\nINFO: All voltages checked are within the expected ranges! (at AMBDCDCUtility.py)"
  
  if printSummary :
    strsum = [str(v) for v in summary]
    print " ".join(strsum)
  return summary

def ReadStatusByte(slot) :
  """Reading out the Status Byte from each LAMB"""
  print "\nReading out the Status Byte:"
  print "===" 
  cmdword = 0x78 #the status byte

  #print "Reference word:", hex(cmdword)
  cnt=0
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)
    sb = VMEPeek(slot, PMBUSData)
    print "LAMB", cnt, ":"
    print "OFF: ", (sb>>6)&1, " VOUT Overvoltage: ", (sb>>5)&1, " IOUT Overcurrent: ", (sb>>4)&1, " VIN Undervoltage: ", (sb>>3)&1
    print "Temperature: ", (sb>>2)&1, " CML (Common Memory Fault): ", (sb>>1)&1, " None of the above: ", (sb)&1
    print "==="
    cnt += 1
  return None

def ReadStatusWord(slot) :
  """Reading out the Status Word from each LAMB"""
  print "\nReading out the Status Word:"
  print "===" 
  cmdword = 0x79 #the status byte

  #print "Reference word:", hex(cmdword)
  cnt=0
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)
    sw = VMEPeek(slot, PMBUSData)
    print "LAMB", cnt, ":"
    print "VOUT fault or warning: ", (sw>>15)&1, " IOUT fault: ", (sw>>14)&1, " VIN fault: ", (sw>>13)&1
    print "PowerGOOD: ", (sw>>11)&1, " Fan Fault: ", (sw>>10)&1, " Shortcircuit: ", (sw>>9)&1
    print "OFF: ", (sw>>6)&1, " VOUT Overvoltage: ", (sw>>5)&1, " IOUT Overcurrent: ", (sw>>4)&1, " VIN Undervoltage: ", (sw>>3)&1
    print "Temperature: ", (sw>>2)&1, " CML (Common Memory Fault): ", (sw>>1)&1, " None of the above: ", (sw)&1
    print "==="
    cnt += 1
  return None

def ReadStatusVOUT(slot) :
  """Reading out the Status VOUT from each LAMB"""
  print "\nReading out the Status VOUT:"
  print "==="
  cmdword = 0x7a #the status byte

  #print "Reference word:", hex(cmdword)
  cnt=0
  for laddr in PMBUSLambs :
    VMEPoke(slot, PMBUSCmd, cmdword | laddr)
    svout = VMEPeek(slot, PMBUSData)
    print "LAMB", cnt, ":"
    print "VOUT OV Fault: ", (svout>>7)&1, " VOUT UV Fault: ", (svout>>4)&1
    print "==="
    cnt += 1
  return None


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-C", "--ChangeVoltages", dest="Change", action="store_true",
                    help="Change the value, if not used the tool do nothing", default=False)
  parser.add_argument("-P", "--CheckPower", dest="Power", action="store_true", 
                    help="Check LAMBs power consumption", default=False)
  parser.add_argument("-D", "--CheckPowerDummy", dest="PowerDummy", action="store_true", 
                    help="Check LAMBs power consumption while sending dummy hits", default=False)
  parser.add_argument("-B", "--CheckPowerBalanced", dest="PowerBalanced", action="store_true", 
                    help="Check LAMBs power consumption (to be used while sending a balanced 8 bitflip file in loop)", default=False)
  parser.add_argument("-S", "--ReadStatus", dest="ReadStatus", action="store_true", 
                    help="Read out Status Byte, Status Word, Status VOUT", default=False)
  parser.add_argument("-O", "--OldAMBv4", help="The board is an AMBv4 or AMBv5 proto with 1.15 V by defeault",
                    dest="isAMBv4", action="store_true", default=False)
  parser.add_argument("--ref1V", help="Set the reference voltage to 1 volt, only for AMBv5",
                    dest='ref1V', action="store_true", default=False)
  parser.add_argument("--Summary", dest="Summary", action="store_true",
                      help="Print a single summary line at the end", default=False)
  parser.add_argument("-v", "--verbose", dest="verbose", action="count",
                      help="Verbosity level", default=0)
  parser.add_argument("slot",help="Slot number", type=int)
  opts = parser.parse_args()

  #global slot, verb, VOUT_SCALE, isAMBv4
  print "AMB Voltage setting utility\n"
  
  verb =  opts.verbose 
  slot = opts.slot

  DCS_state = VMEPeek(slot, AMB_CONTROL_DCS_RUN)
  if (DCS_state == 0xCAFE) :
    print "DCS semaphore is flagged as taken\n"
    sys.exit(0)
  else :
    print "Taking the DCS semaphore\n"
    VMEPoke(slot, AMB_CONTROL_DCS_RUN, 0xCAFE)

  print "Checking board in slot:", slot
  isAMBv4 = opts.isAMBv4
  if opts.ref1V :
    refValue = 1.
  else :
    refValue = 1.15   
  
  if not isAMBv4 :
    VOUT_SCALE=1.6666666666666667 # value for boards with 1.000 nominal tesion
  else :
    VOUT_SCALE=1.9216666666666667 # value for boards with 1.145 nominal tesion
  #print "Vout scale =", VOUT_SCALE
  
  if opts.Change :
    SetOverThreshold(slot)
    SetVoltage(slot)

  if opts.ReadStatus :
    #ReadStatusByte(slot)
    ReadStatusWord(slot)
    ReadStatusVOUT(slot)

  #send dummy hits in loop, needed to check the power consumption
  if(opts.PowerDummy):

    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0x0, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xa0005000, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xaa005500, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xa8005400, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xaaa05550, 0.5)

  arrOut = ReadDCDCInfo(slot, opts.Summary)

  #I want to read register 0x3c3c3c3c that has indirect LAMB access
  VMEPoke(slot, 0x00000038, 0x3c3c3c3c)
  logging.debug( "Fanout status: %x, expected: 1010101" % VMEPeek(slot, 0x0000003c) )
  

  #Check the power consumption of the LAMBS
  if opts.Power :
    CheckPower(arrOut, False)

  if opts.PowerDummy :
    CheckPower(arrOut, True)

  if opts.PowerBalanced :
    CheckPower(arrOut, True)

  #Stop sending dummy hits
  if(opts.PowerDummy):
    # VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0x7a007500, 0.5)
    # VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0x70007000, 0.5)

    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xaaa05550, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xaa005500, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0xa0005000, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0x00000000, 0.5)
    VMEPoke(slot, AMB_HIT_DUMMY_HIT_WORD, 0x0, 0.5)

  print "\nReleasing the DCS semaphore\n"
  VMEPoke(slot, AMB_CONTROL_DCS_RUN, 0x0000)

if __name__ == "__main__" :
  main()
