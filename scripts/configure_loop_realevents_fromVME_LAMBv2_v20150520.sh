#!/bin/bash

slt=15
thr=7

###  set AM chips in tmode to write patterns, High THR and disabled pattern flow to reduce noise during pattern writing function
ambslp_amchip_jpatt_cfg_main --disable_top2 0 --thr 15 --tmode 1 --disable_pflow 1 --chainlength 2 --chipnum 3
ambslp_amchip_jpatt_cfg_main --disable_top2 0 --thr 15 --tmode 1 --disable_pflow 1 --chainlength 2 --chipnum 3

###  INIT from JTAG to ensure previuos instrauction is activated
ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3
ambslp_amchip_disable_bank_main --chainlength 2 --chipnum 3
ambslp_amchip_jpatt_cfg_main --disable_top2 1 --thr 15 --tmode 1 --disable_pflow 1 --chainlength 2 --chipnum 3
ambslp_amchip_jpatt_cfg_main --disable_top2 1 --thr 15 --tmode 1 --disable_pflow 1 --chainlength 2 --chipnum 3
ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3

### write bank
ambslp_writepatterns_main --chainlength 2 --chipnum 3 --ssoff /afs/cern.ch/work/a/annovi/public/AMBtest/user.gvolpi.4350986._000001.all.inp0_reg45_sub0_loop0.patterns_raw_8L_15x16x36Ibl_1M_reg45_sub0.patt.bz
#ambslp_writepatterns_main --chainlength 2 --chipnum 3 --ssoff /afs/cern.ch/user/j/jgramlin/work/public/TestVectorAM/testoutbankHW2DC_flatcache.root --bankType 0
ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3

### set low THR, Tmode 0 and enable pattern flow to start to run
ambslp_amchip_jpatt_cfg_main --thr $thr --tmode 0 --disable_pflow 0 --chainlength 2 --chipnum 3
ambslp_amchip_jpatt_cfg_main --thr $thr --tmode 0 --disable_pflow 0 --chainlength 2 --chipnum 3
ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3
