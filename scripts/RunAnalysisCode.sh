#!/bin/bash

FILENAME=$1
OUTPUT="${FILENAME%.*}"
OUTPUT="$OUTPUT.pdf"
ROOTFILE="${FILENAME%.*}"
ROOTFILE="$ROOTFILE.root"

bash -c "root -b 'AnalyzeRunTest.C(\"${FILENAME}\", \"${OUTPUT}\", \"${ROOTFILE}\")'"

#evince $OUTPUT 
