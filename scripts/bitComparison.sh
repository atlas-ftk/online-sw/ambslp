#!/bin/bash

# take spybuffer with dumper, common range of events of inspy and outspy at same freeze.
ambslp_spybuffer_dumper_main --slot 15 --tag ambslp_spybuffer --method 4

# check dataflow is still going
ambslp_status_main --slot 15 | tail -n 1
ambslp_status_main --slot 15 | tail -n 1

# boardsim
## MC bank
ambslp_boardsim -N 131072 -T 0 -e -1 --pattern-file /afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root --input-file sshits_comp_ambslp_spybuffer.ss --output-file amb_validation_boardsim.out

## data-2017 bank
#ambslp_boardsim -N 131072 -T 0 -e -1 --pattern-file /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/FTK/Patterns/ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6/patterns_user.sschmitt.DataAlignment_xm05_ym05_Reb64_v2.HW2.170217-64PU_30x128x72Ibl-NB9-NE6-BM0_partitioning2_170219-215620_reg22_sub0.pbank.root --input-file sshits_comp_ambslp_spybuffer.ss --output-file amb_validation_boardsim.out

# bit comparison
./AMBCompareTestVecOutput.py outspy_comp_ambslp_spybuffer.txt amb_validation_boardsim.out --SuppressMissingEE
