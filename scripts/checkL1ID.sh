#!/bin/sh

if [ $# -ne 1 ]; then
    echo usage: checkL1ID.sh spy_dump_file_method0
    echo 
    echo checks for each column if the first and last L1IDs are compatible with the number of end_event word found 
    echo ... also checks that L1IDs in each column are consecutive.
    echo wrap around of L1IDs are NOT treated correctly;
    echo for AMB output spy, treats also spy buffers that did not overflow.
    echo for AMB input spy, DOES NOT treats spy buffers that did not overflow.
    exit -1;
fi


FILE_TYPE=$(head -n1 $1 | cut -b 1-14)
echo $FILE_TYPE

# assume OUTput spy buffer dump method0
LINK_LIST=$(echo {0..15})
EE_WORD=^f78

if [ "$FILE_TYPE" == "ambslp_inp_spy" ]; then
    # change to INput spy buffer dump method0
    LINK_LIST=$(echo {0..11})
    EE_WORD=^f700
fi

LINK_WITH_ERRORS=
echo $LINK_LIST
for qwe in $LINK_LIST; do
    TMP_FILE=/tmp/${USER}_checkL1ID_link${qwe}_$(date +%Y%m%d_%H%M%S)

#assuming it is a AMB output spy, drop lines before the start of spy buffer (i.e. in case we did not have overflow)
    #get the spy buffer status register for the relevant column
    FIRST_COLUMN=$(( 8 + 17 * ($qwe % 4) ));
    LAST_COLUMN=$(($FIRST_COLUMN+9));
    grep OSPY $1 | head -n $(( $qwe /4 + 1)) | tail -n 1 | cut -b $FIRST_COLUMN-$LAST_COLUMN > ${TMP_FILE}_SPY_REG

#    cat ${TMP_FILE}_SPY_REG
#    cut -b 6-6 ${TMP_FILE}_SPY_REG 
    SPY_POINTER=$(cat $1 | wc -l)
#    echo file length $SPY_POINTER
    if [ "$(cut -b 6-6 ${TMP_FILE}_SPY_REG)" == "-" ]; then
        SPY_POINTER=$(( 0x$(cut -b 1-4 ${TMP_FILE}_SPY_REG) + 8 ))
#        echo spy did not overflow ... $SPY_POINTER -- $(cat ${TMP_FILE}_SPY_REG )
#        SPY_POINTER=$(( $(cat $1 | wc -l) - 0x$SPY_POINTER ))
#        echo spy did not overflow ... $SPY_POINTER;
    fi

    FIRST_COLUMN=$((8+16*$qwe));
    LAST_COLUMN=$(($FIRST_COLUMN+7));
#    cut -b 1-$LAST_COLUMN $1 | wc 
#    cut -b 1-$LAST_COLUMN $1 | tail -n $SPY_POINTER | wc 
#    cut -b 1-$LAST_COLUMN $1 | tail -n $SPY_POINTER | head -n 10
    cut -b $FIRST_COLUMN-$LAST_COLUMN $1 | tail -n $SPY_POINTER | grep $EE_WORD  > $TMP_FILE
#    head $TMP_FILE

    FIRST=0x$(head -n1 $TMP_FILE)
    LAST=0x$(tail -n1 $TMP_FILE)
    NLINES=$(cat $TMP_FILE | wc -l)
    CHECK=$(($FIRST + $NLINES - $LAST -1)) 
    echo LINK $qwe -- NLINES: $NLINES -- check $CHECK "(0 is good)" -- FIRST: $FIRST -- LAST: $LAST

#    echo FIRST is $FIRST
#    for CURRENT in $(tail -n $(( $(cat $TMP_FILE | wc -l ) - 1)) $TMP_FILE ); do
#        TEST_NEXT=$(($FIRST + 1 - 0x$CURRENT));
#        if [ "$TEST_NEXT" != "0" ]; then
#            LINK_WITH_ERRORS="$LINK_WITH_ERRORS $qwe $TMP_FILE"
#            echo ERROR LINK $qwe "!!!!!!!!!!" PREV=$FIRST CURRENT=$CURRENT
#            echo ;
#        fi;
#        FIRST=0x$CURRENT
#    done 
#

    if [ "$CHECK" != "0" ]; then
        LINK_WITH_ERRORS="$LINK_WITH_ERRORS $qwe $TMP_FILE"
        echo ERROR LINK $qwe "!!!!!!!!!!"
        echo ERROR LINK $qwe "!!!!!!!!!!"
        echo ERROR LINK $qwe "!!!!!!!!!!"
        echo ;
    fi;
done

echo LINK_WITH_ERRORS: $LINK_WITH_ERRORS
