#!/bin/bash

echo
echo Loading 1 hit
echo

FILE_PREFIX=./
echo "Check if the FIFO is clean (should be 0xffff)"
vme_peek 0x780040c0

echo "Carico le TX fifo di AUX"
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0x9 | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0xa | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0xb | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0xc | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0xd | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0xe | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0xf | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 1 --file 2hits.ss --buffer 0x10 | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 2 --file 2hits.ss --buffer 0xa | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 2 --file 2hits.ss --buffer 0xb | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 2 --file 2hits.ss --buffer 0xc | tail 
sleep 1
aux_load_tx_fifo_main --slot 15 --fpga 2 --file 2hits.ss --buffer 0xd | tail 
sleep 1

echo "Resetting spy buffer"
ambslp_reset_spy_main

echo "Resetting AMB Road FIFO"
ambslp_init_main --amb_lamb 2
ambslp_init_main --amb_lamb 2
ambslp_init_main --amb_lamb 2

echo "NUMBER of events before starting the test"
ambslp_status_main | grep Number


echo "Starting TEST"
aux_write_main --slot 15 --fpga 1 0x30 0x2; aux_write_main --slot 15 --fpga 2 0x30 0x2
echo "Ending TEST"
aux_write_main --slot 15 --fpga 1 0x30 0x0; aux_write_main --slot 15 --fpga 2 0x30 0x0
ambslp_status_main | grep Number

