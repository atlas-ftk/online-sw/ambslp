#!/bin/bash
SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi

vme_poke $(printf "%x" $(( ($SLOT<<27)|0x000020e0))) 0x0  > /dev/null 2>&1

    ambslp_amchip_prbs_error_count_all_buses_main --slot $SLOT --chainlength 2  --chipnum 3  --policy disable > /dev/null 2>&1
    ambslp_amchip_prbs_error_count_all_buses_main --slot $SLOT --chainlength 2  --chipnum 3  --policy enable  > /dev/null 2>&1
    
    echo "Polling status registers"
    ambslp_amchip_polling_stat_reg_main --slot $SLOT --chainlength 2  --chipnum 3 

vme_poke $(printf "%x" $(( ($SLOT<<27)|0x000020e0))) 0x3  > /dev/null 2>&1


