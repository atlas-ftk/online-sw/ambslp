#!/bin/bash

SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi

 #test both pattin0 and pattin1 
 ambslp_PRBS_pattin_test_main --slot $SLOT  

 #resets error counters to 0, 'patt_duplex' indicates pattin 0 and pattout
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes patt_duplex  --chainlength 2  --chipnum 3  --columns a  --policy disable  > /dev/null 2>&1

 #enables error counters
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes patt_duplex  --chainlength 2  --chipnum 3  --columns a  --policy enable  > /dev/null 2>&1
 #ambslp_amchip_polling_stat_reg_pattin_main --slot $SLOT  --serdes pattin0 --chainlength 2  --chipnum 3  --columns a  

 #echo "Pattin0, pattern mode, test passed if the output is > 90"

 #resets error counters to 0, 'patt_duplex' indicates pattin 0 and pattout
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes patt_duplex  --chainlength 2  --chipnum 3  --columns a  --policy disable  > /dev/null 2>&1

 #enables error counters but set pattern mode instead of prbs mode
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes patt_duplex  --chainlength 2  --chipnum 3  --columns a  --policy pattern  > /dev/null 2>&1

 #ambslp_amchip_polling_stat_reg_pattin_main --slot $SLOT  --serdes pattin0 --chainlength 2  --chipnum 3  --columns a  

 #go back to prbs mode
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes patt_duplex  --chainlength 2  --chipnum 3  --columns a  --policy enable  > /dev/null 2>&1
	
 #echo "Pattin1, only odd chains are relevant, PRBS mode, test passed if the output is 0"

 #reset error counters to 0
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes pattin1  --chainlength 2  --chipnum 3  --columns a  --policy disable  > /dev/null 2>&1

 #enables error counters 
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes pattin1  --chainlength 2  --chipnum 3  --columns a  --policy enable  > /dev/null 2>&1

 #ambslp_amchip_polling_stat_reg_pattin_main --slot $SLOT  --serdes pattin1 --chainlength 2  --chipnum 3  --columns a

 #echo "Pattin1, only odd chains are relevant, pattern mode, test passed if the output is > 90"

 #reset error counters to 0
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes pattin1  --chainlength 2  --chipnum 3  --columns a  --policy disable  > /dev/null 2>&1

 #enables eror counters but set pattern mode 
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes pattin1  --chainlength 2  --chipnum 3  --columns a  --policy pattern  > /dev/null 2>&1

 #ambslp_amchip_polling_stat_reg_pattin_main --slot $SLOT  --serdes pattin1 --chainlength 2  --chipnum 3  --columns a

 #go back to prbs mode
 #ambslp_amchip_prbs_error_count_main --slot $SLOT --serdes pattin1  --chainlength 2  --chipnum 3  --columns a  --policy enable  > /dev/null 2>&1
