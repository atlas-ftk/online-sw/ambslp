#!/bin/bash
SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi
echo "Using SLOT=$SLOT"

##configuring AUX in Factory mode
echo "configuring AUX in Factory mode"

python reconfigureBoard.py -s $SLOT --doFactory

## turn on the dcdc converters on AMBV3
ambslp_config_reg_main --slot $SLOT

vme_poke 0x780020e8 0x3f


## Enable PRBS-7 CHECKER on ROAD FPGA
echo "Enabling PRBS-7 CHECKER on ROAD FPGA"
vme_poke $(printf "%x" $(( ($SLOT<<27)|0x000040dc))) 0x10


## Enable PRBS-7 CHECKER and GEN on HIT FPGA
echo "Enable PRBS-7 CHECKER and GEN on HIT FPGA"
vme_poke $(printf "%x" $(( ($SLOT<<27)|0x000020e0))) 0x3  > /dev/null 2>&1


## turn ON PRBS-7 GENERATOR and CHECKER on AM chips
echo "Enabling PRBS generator and Checker on AMCHIPs"

ambslp_amchip_prbsgen_main --slot $SLOT --lambver 2  > /dev/null 2>&1

ambslp_amchip_prbs_error_count_all_buses_main  --slot $SLOT --policy enable  > /dev/null 2>&1

ambslp_amchip_polling_stat_reg_main   --slot $SLOT > /dev/null 2>&1




## Enable PRBS-7 GEN on ROAD FPGA
echo "Enable PRBS-7 GEN on ROAD FPGA"

#send errors
vme_poke $(printf "%x" $(( ($SLOT<<27)|0x00004100))) 0x0    > /dev/null 2>&1
vme_poke $(printf "%x" $(( ($SLOT<<27)|0x00004100))) 0x011  > /dev/null 2>&1
vme_poke $(printf "%x" $(( ($SLOT<<27)|0x00004100))) 0x0    > /dev/null 2>&1

#send PRBS-7 w/o errors
vme_poke $(printf "%x" $(( ($SLOT<<27)|0x00004100))) 0x001  > /dev/null 2>&1

## Enable PRBS-7 GENERATOR on AUX FPGAS
echo "Enable PRBS-7 GENERATOR on AUX FPGAS"
aux_write_main --slot $SLOT --fpga 1 0x30 0x3  > /dev/null 2>&1
aux_write_main --slot $SLOT --fpga 2 0x30 0x3  > /dev/null 2>&1

#reset error counters
#echo "Reset error counters"

#for fpga in $(seq 3 6)
#do
#    aux_reset_main --fpga ${fpga} --slot ${SLOT} --err  > /dev/null 2>&1 
#done

echo "PRBS error result"
sh read_PRBS_result.sh $SLOT
ambslp_init_main --slot $SLOT
