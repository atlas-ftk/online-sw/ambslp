#!/bin/bash

HERE=`pwd`
SLOT=19
if [ $# -gt 0 ]; then
  SLOT=$1
fi

REPETITIONS=0
if [ $# -gt 1 ]; then
  REPETITIONS=$2
fi

GROUP=1
if [ $# -gt 2 ]; then
  GROUP=$3
fi

DOSA=0
if [ $# -gt 3 ]; then
  DOSA=$4
fi

#cd next_release/scripts

NOW=`date +"%Y_%m_%d_%H.%M.%S"`
NEW_FILE_NAME=TVTest_${NOW}.cmd
#sed "3s/.*/set slot ${SLOT}/" TVTest.cmd > $HERE/$NEW_FILE_NAME
sed "3s/.*/set slot ${SLOT}/" TVTestAutomatic_multiPU_p1.cmd > $HERE/$NEW_FILE_NAME

#NEW_DIR=$HERE
#NEW_DIR=/afs/cern.ch/work/n/nbiesuz/public
#NEW_DIR=/tmp/tkubota
NEW_DIR=/shared/data/tomoya
mkdir $NEW_DIR/AMBTest_${NOW}_${HOSTNAME}_${SLOT}
NEW_DIR=$NEW_DIR/AMBTest_${NOW}_${HOSTNAME}_${SLOT}

echo " " >> $HERE/$NEW_FILE_NAME

if [ $DOSA -eq "1" ]; then
    echo "set dosa 1" >> $HERE/$NEW_FILE_NAME
fi

COUNTER=0
if [ $REPETITIONS -eq "0" ]; then
	echo "will not run automatically"
else
	echo "Will run " $REPETITIONS "times"
	until [ $REPETITIONS -eq 0 ] ; do
	    if [ $(($REPETITIONS%${GROUP})) -eq 0 ]; then
		echo "set doconfigure 1" >> $HERE/$NEW_FILE_NAME
		echo "set ambhits /shared/data/tomoya/tv_flat/amb_validation_flat_"${COUNTER}".ss" >> $HERE/$NEW_FILE_NAME
		echo "set outroads /shared/data/tomoya/tv_flat/amb_validation_flat_"${COUNTER}".out" >> $HERE/$NEW_FILE_NAME
		let COUNTER+=1
	    else
		echo "set doconfigure 0" >> $HERE/$NEW_FILE_NAME
	    fi
	    echo "ambtest" >> $HERE/$NEW_FILE_NAME
	    if [ $DOSA -eq "0" ]; then
		echo "compare" >> $HERE/$NEW_FILE_NAME
	    fi
	    echo "ambtest stop" >> $HERE/$NEW_FILE_NAME
	    let REPETITIONS-=1
        done
fi

echo "ambtest stop" >> $HERE/$NEW_FILE_NAME
	    
#./AMBTest.py --auxdir $NEW_DIR $HERE/$NEW_FILE_NAME > /dev/null 2>&1 &
#./AMBTest.py --auxdir $NEW_DIR $HERE/$NEW_FILE_NAME
./AMBTest.py --batch --auxdir $NEW_DIR $HERE/$NEW_FILE_NAME

grep "- ERROR -" $NEW_DIR/ambtest.log > $NEW_DIR/error_summary.txt

cd $HERE


