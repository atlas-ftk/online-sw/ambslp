#!/bin/bash

slt=15
thr=7

###  set AM chips in tmode to write patterns, High THR and disabled pattern flow to reduce noise during pattern writing function
ambslp_amchip_jpatt_cfg_main --thr 15 --tmode 1 --disable_pflow 1 --chainlength 2 --chipnum 3

ambslp_amchip_jpatt_cfg_main --thr 15 --tmode 1 --disable_pflow 1 --chainlength 2 --chipnum 3

###  INIT from JTAG to ensure previuos instrauction is activated
ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3

ambslp_amchip_disable_bank_main --chainlength 2 --chipnum 3

ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3

### write 20 patterns from address 0
./ambslp_writepatterns_main ~/patterns_raw_8L_15x16x36Ibl_1M_reg45_sub0.patt.bz2  
#./ambslp_gen_hits_DC_main --DCmax 2 -e 10 -r  30 --pattern_file pattern.txt >hits.txt 

ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3

### set low THR, Tmode 0 and enable pattern flow to start to run
ambslp_amchip_jpatt_cfg_main --thr $thr --tmode 0 --disable_pflow 0 --chainlength 2 --chipnum 3

ambslp_amchip_jpatt_cfg_main --thr $thr --tmode 0 --disable_pflow 0 --chainlength 2 --chipnum 3

ambslp_amchip_init_evt_main --chainlength 2 --chipnum 3

