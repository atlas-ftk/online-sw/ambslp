#include <fstream>
#include <iostream>
#include <stdio.h>
#include <sstream>
#include "TCanvas.h"
#include <sstream>
#include <algorithm>
#include <iterator>
#include <string>

#include "TROOT.h"
#include "TRint.h"

using namespace std;

//#define LINES_TO_SKIP 24000
//#define LINES_TO_SKIP 23000
//#define LINES_TO_SKIP 20500
//#define LINES_TO_SKIP 3000

//#define LINES_TO_SKIP 800
#define LINES_TO_SKIP 0

//gROOT.ProcessLine(".L addVectorToROOT.C+");

vector<string> readLine(string line)
{
	vector<string> token;
	std::string::size_type n = 0;
	
	string tokenString;
	stringstream iss(line);
	while (iss >> tokenString) token.push_back(tokenString);
	return token;
}

void loadLogFile(TString filename, vector<vector<double>> &ChipID, vector<vector<double>> &MatchedRoads, vector<vector<double>> &LostRoads, vector<vector<double>> &PartialRoads, vector<vector<double>> &ExtraRoads,vector<vector<double>> &ErrorsPerEvent, vector<int> &eventsPerRun)
{
	cout<<filename<<endl;	
	ifstream in;
	in.open(filename, ios::in);

	string line;

	vector<double> data;
	
	int NumberOfRuns=0;
	int AmID,Lost,Extra,Matched,Partial,AmID2,Lost2,Extra2,Matched2,Partial2;

	const bool isRCD_message=true;

	for(int i=0;i<LINES_TO_SKIP;++i)
		getline(in, line);
	while( !in.eof() )
	{

		getline(in, line);
		/* if( line.size()==76) */
		/* if( line.size()==76 || line.size()==42) */
		/* { */

		  /* cout<<"line: "<<line<<endl; */

			if( line.find("NoFound")!=string::npos )
			{
				ChipID.push_back( data );
				MatchedRoads.push_back( data );
				LostRoads.push_back( data);
				PartialRoads.push_back( data );
				ExtraRoads.push_back( data );


				for(int i=0;i<35;++i)
				{
					getline(in, line);
					//cout<<line<<endl;
					if( line.size()> 40 )
					{
					        if(isRCD_message)
						  line.erase(0,33);

						sscanf(line.c_str(),"%d %d %d %d %d -- %d %d %d %d %d",&AmID,&Lost,&Matched,&Partial,&Extra,&AmID2,&Lost2,&Matched2,&Partial2,&Extra2);


						ChipID[NumberOfRuns].push_back(AmID);
						ChipID[NumberOfRuns].push_back(AmID2);

						MatchedRoads[NumberOfRuns].push_back(Matched);
						MatchedRoads[NumberOfRuns].push_back(Matched2);

						LostRoads[NumberOfRuns].push_back(Lost);
						LostRoads[NumberOfRuns].push_back(Lost2);

						PartialRoads[NumberOfRuns].push_back(Partial);
						PartialRoads[NumberOfRuns].push_back(Partial2);

						ExtraRoads[NumberOfRuns].push_back(Extra);
						ExtraRoads[NumberOfRuns].push_back(Extra2);
					}
				}
			}
		/* } */
		/* else if(line[55]=='[') */
		/* { */
		  //cout<<line<<endl;
			/* if( line.find("event")!=string::npos ) */
			if( line.find("# of errors by event")!=string::npos )
			{
				//cout<<line.size()<<endl;
			        if(isRCD_message)
				  line.erase(0,55);
				else
				  line.erase(0,21);

				ErrorsPerEvent.push_back(data);
				line.erase(line.begin());
				line.erase(line.end()-1);
				replace( line.begin(), line.end(), ',', ' ');
				stringstream lineStream(line);
				//cout<<lineStream.str();
				double eventErrors;
				int numberOfEvents=0;
				while(lineStream >> eventErrors)
				{
				  //cout<<eventErrors<<" ";
				  ErrorsPerEvent[NumberOfRuns].push_back(eventErrors);
				  numberOfEvents++;
				}
				while(ErrorsPerEvent[NumberOfRuns].size()<51){
				  //this while loop is to prevent the crash with truncated event loops
				  ErrorsPerEvent[NumberOfRuns].push_back(0);
				  numberOfEvents++;
				}
				eventsPerRun.push_back(numberOfEvents);			

				cout<<"Finished Parsing Run: "<< NumberOfRuns+1<<" with "<<numberOfEvents << " events"<<endl;
				NumberOfRuns++;

				for(int i=0;i<LINES_TO_SKIP;++i)
					getline(in, line);			
			}
		/* } */
	}
	cout<<"Number of Runs: "<<NumberOfRuns<<endl;
	cout<<"Number of Chips: "<< ChipID[0].size()<<endl;

}


int getMaxNumberOfErrorsPerEvent (vector<vector<double>> err)
{
	double maxErr=0;
	for(int i=0;i<err.size();++i)
		for(int j=0;j<err[0].size();++j)
			if(err[i][j]>maxErr) maxErr=err[i][j];

	return maxErr;
}

int getMaxNumberErrorsAppeared (vector<int> errInstances)
{
	int max=0;
	for(int i=0;i<errInstances.size();++i)
		if(errInstances[i] > max) max = errInstances[i];
	return max;
}

double getEfficiencyError (double fraction, double all)
{
  
  double eff = fraction/all;
  return TMath::Sqrt(eff*(1-eff)/all);

}

void analyzeErrorsPerEvent( vector<vector<double>> ErrorsPerEvent , TString outputPdf, bool avoidCatastrophic, bool doRatio)
{
	TCanvas *c1 = new TCanvas("c1","events",1800,1000);	
	c1->Divide(2,1);

	int numberOfEvents = ErrorsPerEvent[0].size(); // events per run
	int numberOfRuns = ErrorsPerEvent.size(); // times the test run
	stringstream ss;
	ss <<"Errors per event for " << numberOfRuns<<" runs";
	TString title = ss.str();
	int maxNumOfErrors = getMaxNumberOfErrorsPerEvent(ErrorsPerEvent);
	TH2F *eventErrors = new TH2F (title, title, numberOfEvents, 0.5, numberOfEvents+0.5, 100 ,0 ,maxNumOfErrors+5);
	vector<int> TimesAnEventHasErrors;
	//for(int i=0;i<numberOfEvents;++i) TimesAnEventHasErrors.push_back(0);
	std::vector<int> tot_error; tot_error.clear();
	if(avoidCatastrophic) {
	  for(int i=0;i<numberOfRuns;++i) {
	    int tmp=0;
	    for (int k=0; k<numberOfEvents; ++k) {
	      tmp=tmp+ErrorsPerEvent[i][k];
	    }
	    tot_error.push_back(tmp);
	  }
	}

	for(int i=0;i<numberOfRuns;++i)
	{
	  
	  if(avoidCatastrophic) if(tot_error.at(i)>2000) continue;
	  
	  for (int k=0; k<numberOfEvents; ++k)
	    {
	      if(ErrorsPerEvent[i][k]) 
		{
		  eventErrors->Fill(k+1,ErrorsPerEvent[i][k]);
		  TimesAnEventHasErrors.push_back(k+1);	
		}
	    }	
	}	

	// plot errors per event for all runs
	eventErrors->SetNdivisions(numberOfEvents/5, "X");
	eventErrors->GetXaxis()->SetTitle("event");	
	c1->cd(1);
	eventErrors->Draw("CANDLEX3");
	eventErrors->Write();

	// plot times an AM chip had errors
	c1->cd(2);
	ss.str("");

	int mostErrorInstances = getMaxNumberErrorsAppeared(TimesAnEventHasErrors);
	ss <<"Times an event had errors for " << numberOfRuns<<" runs";
	title = ss.str();
	TH1D *errorInstances = new TH1D (title, title, numberOfEvents, 0.5, numberOfEvents+0.5);
	for (int i=0;i<TimesAnEventHasErrors.size();++i) errorInstances->Fill(TimesAnEventHasErrors[i]);

	errorInstances->SetMarkerSize(2);
	errorInstances->GetXaxis()->SetTitle("event");	
	errorInstances->GetXaxis()->SetLabelSize(0.03);
	errorInstances->SetNdivisions(numberOfEvents/5, "X");
	errorInstances->Draw("*H");
	errorInstances->Write();
	
	c1->Print(outputPdf+"(");
}


void analyzeErrorsOnAMchips ( vector<vector<double>> ChipID, vector<vector<double>> MatchedRoads, vector<vector<double>> LostRoads, vector<vector<double>> PartialRoads, vector<vector<double>> ExtraRoads, TString outputPdf, bool avoidCatastrophic, bool doRatio)
{
	TCanvas *c2 = new TCanvas ("c2","chip errors",1800,1000);
	TCanvas *c3 = new TCanvas ("c3","counter of chip errors",1800,1000);
	TCanvas *c4 = new TCanvas ("c4","All errors",1800,1000);
	TCanvas *c5 = new TCanvas ("c5","over time plots",1800,1000);

	int numberOfRuns = ChipID.size(); // times the test run
	int numberOfChips = ChipID[0].size(); // times the test run
	cout<<numberOfRuns<<endl;
	TH1F *Matched = new TH1F("Matched LAMB0","Matched LAMB0",64,0.5,64.5);
	TH1F *Partial = new TH1F("Partially Matched LAMB0","Partially Matched LAMB0",64,0.5,64.5);
	TH1F *Lost = new TH1F("Lost LAMB0","Lost LAMB0",64,0.5,64.5);
	TH1F *Extra = new TH1F("Extra LAMB0","Extra LAMB0",64,0.5,64.5);

	TH1F *TimesMatched = new TH1F("TimesMatched LAMB0","Matched LAMB0",64,0.5,64.5);
	TH1F *TimesPartial = new TH1F("TimesPartially Matched LAMB0","Partially Matched LAMB0",64,0.5,64.5);
	TH1F *TimesLost = new TH1F("TimesLost LAMB0","Lost LAMB0",64,0.5,64.5);
	TH1F *TimesExtra = new TH1F("TimesExtra LAMB0","Extra LAMB0",64,0.5,64.5);

	TH2F * MatchedOverLoop;
	TH2F * PartialOverLoop;
	TH2F * LostOverLoop;
	TH2F * ExtraOverLoop;

	if(doRatio){
	  MatchedOverLoop = new TH2F("MatchedOverTime", "# of matched roads (ratio to the # of matched roads in the first event)", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	  PartialOverLoop = new TH2F("PartialOverTime", "# of partial roads (ratio to the # of matched roads in the first event)", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	  LostOverLoop = new TH2F("LostOverTime", "# of lost roads (ratio to the # of matched roads in the first event)", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	  ExtraOverLoop = new TH2F("ExtraOverTime", "# of extra roads (ratio to the # of matched roads in the first event)", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	} else {
	  MatchedOverLoop = new TH2F("MatchedOverTime", "# of matched roads", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	  PartialOverLoop = new TH2F("PartialOverTime", "# of partial roads", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	  LostOverLoop = new TH2F("LostOverTime", "# of lost roads", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	  ExtraOverLoop = new TH2F("ExtraOverTime", "# of extra roads", numberOfRuns, -0.5, (double)(numberOfRuns)-0.5, 64, 0.5, 64.5);
	}
	double TotalPartiallyMatched=0, TotalLost=0, TotalExtra=0, TotalRoadsForAllRuns=0;

	std::vector<int> tot_error; tot_error.clear();
	if(avoidCatastrophic) {	  
	  for(int i=0;i<numberOfRuns;++i)
	    {
	      int tmp=0;
	      for(int j=0;j<64;++j)
		{
		  tmp+=PartialRoads[i][j];
		  tmp+=LostRoads[i][j];
		  tmp+=ExtraRoads[i][j];
		}
	      tot_error.push_back(tmp);
	    }
	}

	int totalRoadsPerChip=0;	
	for(int i=0;i<numberOfRuns;++i)
	{

	  if(avoidCatastrophic) if(tot_error.at(i)>2000) continue;

		for(int j=0;j<64;++j)
		{
			totalRoadsPerChip = MatchedRoads[i][j]+LostRoads[i][j]+PartialRoads[i][j];
		//	Matched->Fill(j+1, MatchedRoads[i][j]/totalRoadsPerChip);
			Partial->Fill(j+1, PartialRoads[i][j]/totalRoadsPerChip/numberOfRuns);
			Lost->Fill(j+1, LostRoads[i][j]/totalRoadsPerChip/numberOfRuns);
			Extra->Fill(j+1, ExtraRoads[i][j]/totalRoadsPerChip/numberOfRuns);
			if( PartialRoads[i][j]) TimesPartial->Fill(j+1);
			if( LostRoads[i][j]) TimesLost->Fill(j+1);
			if( ExtraRoads[i][j]) TimesExtra->Fill(j+1);

			TotalPartiallyMatched+=PartialRoads[i][j];
			TotalLost+=LostRoads[i][j];
			TotalExtra+=ExtraRoads[i][j];
			TotalRoadsForAllRuns+=MatchedRoads[i][j]+PartialRoads[i][j]+LostRoads[i][j];
			
			if(doRatio){
			  MatchedOverLoop->Fill(i, j+1, MatchedRoads[i][j]/MatchedRoads[1][j]);
			  PartialOverLoop->Fill(i, j+1, PartialRoads[i][j]/MatchedRoads[1][j]);
			  LostOverLoop->Fill(i, j+1, LostRoads[i][j]/MatchedRoads[1][j]);
			  ExtraOverLoop->Fill(i, j+1, ExtraRoads[i][j]/MatchedRoads[1][j]);
			} else {
			  MatchedOverLoop->Fill(i, j+1, MatchedRoads[i][j]);
			  PartialOverLoop->Fill(i, j+1, PartialRoads[i][j]);
			  LostOverLoop->Fill(i, j+1, LostRoads[i][j]);
			  ExtraOverLoop->Fill(i, j+1, ExtraRoads[i][j]);
			}
		}
	}


	cout<<"TotalLost: "<<TotalLost<<endl;
	cout<<"TotalPartiallyMatched: "<<TotalPartiallyMatched<<endl;
	cout<<"TotalExtra: "<<TotalExtra<<endl;
	cout<<"TotalRoadsForAllRuns: "<<TotalRoadsForAllRuns<<endl;

	char *barLabels[3] = {"Lost Roads","Partially Matched Roads","Extra Roads"};
	TH1F *generalErrorHist = new TH1F ("AMchip Errors","Errors on all AMchips as a fraction of total roads",3,0.5,3.5);
	generalErrorHist->Fill(1, TotalLost/TotalRoadsForAllRuns);
	generalErrorHist->Fill(2, TotalPartiallyMatched/TotalRoadsForAllRuns);
	generalErrorHist->Fill(3, TotalExtra/TotalRoadsForAllRuns);
	
	generalErrorHist->SetBinError(1, getEfficiencyError(TotalLost, TotalRoadsForAllRuns));
	generalErrorHist->SetBinError(2, getEfficiencyError(TotalPartiallyMatched, TotalRoadsForAllRuns));
	generalErrorHist->SetBinError(3, getEfficiencyError(TotalExtra, TotalRoadsForAllRuns));

	cout<<"Number of runs: "<<numberOfRuns<<endl;
	cout<<"ROADs per run:"<<TotalRoadsForAllRuns/numberOfRuns;
	
	for (int i=0;i<3;++i) generalErrorHist->GetXaxis()->SetBinLabel(i+1,barLabels[i]);
	generalErrorHist->GetYaxis()->SetNoExponent(kFALSE);
	generalErrorHist->SetFillColor(kBlue);
	c4->cd();
	generalErrorHist->Draw();
	generalErrorHist->Write();

	THStack *hs1 = new THStack("hs","");
	THStack *hs2 = new THStack("hs","");
	
	Partial->SetFillColor(kGreen);
	Lost->SetFillColor(kRed);
	Extra->SetFillColor(kBlue);

	TimesPartial->SetFillColor(kGreen);
	TimesLost->SetFillColor(kRed);
	TimesExtra->SetFillColor(kBlue);
	
	hs1->Add(Lost);
	hs1->Add(Partial);
	hs1->Add(Extra);
//	hs1->GetXaxis()->SetNdivisions(numberOfChips/4);

	hs2->Add(TimesLost);
	hs2->Add(TimesPartial);
	hs2->Add(TimesExtra);

	
	c2->cd();
	hs1->Draw();	
	hs1->GetXaxis()->SetNdivisions(32);	

	TLegend *leg = new TLegend(0.68,0.72,0.98,0.92);
	leg->AddEntry(Partial,"Partially Matched Roads","f");	
	leg->AddEntry(Lost,"Lost Roads","f");	
	leg->AddEntry(Extra,"Extra Roads","f");	
	leg->Draw();


	//set title to HistStack	
	TText T; T.SetTextFont(42); T.SetTextAlign(21);
	T.DrawTextNDC(.5,.95,"Errors on each AMchip as a fraction of its total ROADs");


	c3->cd();
	hs2->Draw();
	hs2->GetXaxis()->SetNdivisions(32);	
	leg->Draw();
	
	//set title to HistStack	
	stringstream ss;
        ss <<"Times an AMchip had errors out of " << numberOfRuns<<" runs";
        TString title = ss.str();
	T.DrawTextNDC(.5,.95,title);

	c5->Divide(2,2);
	c5->cd();
	gStyle->SetOptStat(0);
	c5->cd(1);
	MatchedOverLoop->GetXaxis()->SetTitle("Loop");	
	MatchedOverLoop->GetYaxis()->SetTitle("AMchip ID");
	MatchedOverLoop->Draw("colz");

	c5->cd(2);
	PartialOverLoop->GetXaxis()->SetTitle("Loop");	
	PartialOverLoop->GetYaxis()->SetTitle("AMchip ID");
	PartialOverLoop->Draw("colz");

	c5->cd(3);
	LostOverLoop->GetXaxis()->SetTitle("Loop");	
	LostOverLoop->GetYaxis()->SetTitle("AMchip ID");
	LostOverLoop->Draw("colz");

	c5->cd(4);
	ExtraOverLoop->GetXaxis()->SetTitle("Loop");	
	ExtraOverLoop->GetYaxis()->SetTitle("AMchip ID");
	ExtraOverLoop->Draw("colz");

	Matched->Write();
	Partial->Write();
	Lost->Write();
	Extra->Write();
	TimesMatched->Write();
	TimesPartial->Write();
	TimesLost->Write();
	TimesExtra->Write();
	MatchedOverLoop->Write();
	PartialOverLoop->Write();
	LostOverLoop->Write();
	ExtraOverLoop->Write();

	// export canvases to pdf
	c2->Print(outputPdf);
	c3->Print(outputPdf);
	c4->Print(outputPdf);
	c5->Print(outputPdf+")");
}
