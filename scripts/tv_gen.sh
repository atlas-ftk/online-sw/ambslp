#!/bin/bash

for i in {1..100}; do

#    ambslp_gen_hits_DC_main -e 50 -r 60 --r7 50 --r6 20 -n 0 --rs --amb --DCmax 2 --pattern_file /afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root --bankType 0 --hit_file amb_validation_${i}.ss

    ambslp_gen_hits_DC_main -e 50 -r 230 --r7 120 --r6 20 -n 0 --rs --amb --DCmax 2 --pattern_file /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/FTK/Patterns/ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6/patterns_user.sschmitt.DataAlignment_xm05_ym05_Reb64_v2.HW2.170217-64PU_30x128x72Ibl-NB9-NE6-BM0_partitioning2_170219-215620_reg22_sub0.pbank.root --bankType 0 --hit_file amb_validation_flat_${i}.ss

    #ambslp_boardsim -N 131072 -T 0 -e -1 --pattern-file /afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root --input-file amb_validation_${i}.ss --output-file amb_validation_${i}.out

    ambslp_boardsim -N 131072 -T 0 -e -1 --pattern-file /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/FTK/Patterns/ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6/patterns_user.sschmitt.DataAlignment_xm05_ym05_Reb64_v2.HW2.170217-64PU_30x128x72Ibl-NB9-NE6-BM0_partitioning2_170219-215620_reg22_sub0.pbank.root --input-file amb_validation_flat_${i}.ss --output-file amb_validation_flat_${i}.out

done