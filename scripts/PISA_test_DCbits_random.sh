#!/bin/bash
set -x

source GLOBALS.sh
vme_poke 0x780040D0 0xF000 #disable LAMB3

if [ $# -gt 0 ]; then 
  sh reset_AUX.sh
  sh initialization_amb_v20150513Pisa.sh
  sh configure_loop_realevents_fromVME_LAMBv2_v20150519DC.sh
fi
ambslp_init_main --amb_lamb 2 
ambslp_init_main --amb_lamb 2

outfile=~/SpyBufferArchives/spyfile_$(date +%Y%m%d_%H%M)
echo $outfile

rm -f ${outfile}_m{1,2}_chips_stat.root

# 100 loops about 45 minutes
for LOOP in {1..1500}; do 
  echo "LOOP $LOOP, $(date)"
  
  tostop=0
  #sh test_loop_no0_DCrandom.sh $LOOP
  sh test_loop_no0_DCrandom.sh 1
  #sh test_no0_onepatternDC.sh
  echo "$(date)"

  

  ambslp_inp_spy_main > ${outfile}_m0.in
  ambslp_inp_spy_main --method 1 2> ${outfile}_m1.in
  ambslp_out_spy_main > ${outfile}_m0.out
  ambslp_out_spy_main --method 1 2> ${outfile}_m1.out
  #cut -b 12- ${outfile}_m1.out > ${outfile}_m1C12.out
  ambslp_out_spy_main --method 2 2> ${outfile}_m2.out

  roadfile=roads_randomtest.out

  echo "LOOP $LOOP, $(date)" >> ${outfile}_LOOP_m1.out.diff
  ./AMBCompareTestVecOutput.py --MaskLAMB 3 -A ${outfile}_m1_chips_stat.root ${outfile}_m1.out $roadfile >> ${outfile}_LOOP_m1.out.diff
  echo "LOOP $LOOP, $(date)" >> ${outfile}_LOOP_m2.out.diff 
  ./AMBCompareTestVecOutput.py --MaskLAMB 3 -A ${outfile}_m2_chips_stat.root ${outfile}_m2.out $roadfile >> ${outfile}_LOOP_m2.out.diff
  echo "$(date)"

  if [ $tostop -ne 0 ]; then
    echo "An error was found in LOOP $LOOP, stopping the loop"
    break
  fi
done

set +x
