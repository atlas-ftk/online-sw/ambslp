#!/bin/bash

echo "Carico le TX fifo di AUX"
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_4_oddhits.txt --buffer 0x9
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_4_oddhits.txt --buffer 0xa
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_5_oddhits.txt --buffer 0xb
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_5_oddhits.txt --buffer 0xc
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_6_oddhits.txt --buffer 0xd
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_6_oddhits.txt --buffer 0xe
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_7_oddhits.txt --buffer 0xf
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/banktest10patt_7_oddhits.txt --buffer 0x10
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/banktest10patt_0_oddhits.txt --buffer 0xa
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/banktest10patt_1_oddhits.txt --buffer 0xb
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/banktest10patt_2_oddhits.txt --buffer 0xc
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/banktest10patt_3_oddhits.txt --buffer 0xd
sleep 1
echo "Carico la fifo di ROAD"
#./ambslp_feed_road_main --slot 15 --loopfifovme 1 gigi_road.road
sleep 1
echo "Sparo dalla AUX"
./aux_write_main --slot 15 --fpga 1 0x30 0x2
./aux_write_main --slot 15 --fpga 2 0x30 0x2
echo "Ripristino la AUX"
./aux_write_main --slot 15 --fpga 1 0x30 0x0
./aux_write_main --slot 15 --fpga 2 0x30 0x0
