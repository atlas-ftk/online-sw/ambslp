# TV test 2016
set npatts 131072 
set slot 15 
set bankpath /afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root
set ambhits ./filesForTest/sshits_Twr22_amb.ss

set auxhits0 ./filesForTest/sshits_Twr22_L0.ss
set auxhits1 ./filesForTest/sshits_Twr22_L1.ss
set auxhits2 ./filesForTest/sshits_Twr22_L2.ss
set auxhits3 ./filesForTest/sshits_Twr22_L3.ss
set auxhits4 ./filesForTest/sshits_Twr22_L4.ss
set auxhits5 ./filesForTest/sshits_Twr22_L5.ss
set auxhits6 ./filesForTest/sshits_Twr22_L6.ss
set auxhits7 ./filesForTest/sshits_Twr22_L7.ss
set auxhits8 ./filesForTest/sshits_Twr22_L8.ss
set auxhits9 ./filesForTest/sshits_Twr22_L9.ss
set auxhits10 ./filesForTest/sshits_Twr22_L10.ss
set auxhits11 ./filesForTest/sshits_Twr22_L11.ss

set outroads ./filesForTest/outroads_Twr22.out

#set spyout /tmp/AAA
 
blocktransfer yes
#set forcebank 1

set keeptmpdir yes
set doconfigure 1
