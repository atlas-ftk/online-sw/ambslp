#!/bin/bash
set -x

source GLOBALS.sh
vme_poke 0x780040D0 0xF000 #disable LAMB3

if [ $# -gt 0 ]; then 
  sh reset_AUX.sh
  sh initialization_amb_v20150513Pisa.sh
  sh configure_loop_realevents_fromVME_LAMBv2_v20150519DC.sh
fi
ambslp_init_main --amb_lamb 2 
ambslp_init_main --amb_lamb 2

#sh test_loop_no0realevents_DWDL_DCbits.sh
sh test_infloop_no0realevents_DWDL_DCbits.sh
#sh test_no0_onepatternDC.sh

outfile=~/SpyBufferArchives/spyfile_$(date +%Y%m%d_%H%M)
echo $outfile

ambslp_inp_spy_main > ${outfile}_m0.in
ambslp_inp_spy_main --method 1 2> ${outfile}_m1.in
ambslp_out_spy_main > ${outfile}_m0.out
ambslp_out_spy_main --method 1 2> ${outfile}_m1.out
#cut -b 12- ${outfile}_m1.out > ${outfile}_m1C12.out
ambslp_out_spy_main --method 2 2> ${outfile}_m2.out

OUTFILE=filesForTest/dc_roads_no0_np128k.out

./AMBCompareTestVecOutput.py --MaskLAMB 3 ${outfile}_m1.out $OUTFILE > ${outfile}_m1.out.diff
#./AMBCompareTestVecOutput.py ${outfile}_m2.out $OUTFILE > ${outfile}_m2.out.diff

tail ${outfile}_m1.out.diff
set +x
