#!/usr/bin/env tdaq_python

import AMBDCDCUtility
import subprocess
import time
import sys
from subprocess import CalledProcessError

Point1Test = True

# array of the slots interested by the tests, all used slots should be listed here
if Point1Test :
  Slots = [2, 7]
else :
  Slots = [15]

# list of all the dummy-hit setups that will be used
if Point1Test :
  DummyHitsPrep = [0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0xaa005500, 0xaa005500, 0xaa005500, 0xaa005500]
  DummyHits     = [0x00000000, 0xa0000000, 0xa0005000, 0xaa005000, 0xaa005500, 0xaaa05500, 0xaaa05550, 0xaaaa5550, 0xaaaa5555]
else :  
  DummyHitsPrep = [0x00000000, 0x00000000]
  DummyHits     = [0x00000000, 0xa0005000] # Lab4 test


# this list represets how many times data are collected and  the time after the
# previous step to wait before the collection
if Point1Test and False :
  Delays = [0, 60, 60*5, 60*10, 60*20]
else :
  Delays = [0, 60]
InBetDelay = 0 #

# file descrciptor where the logs are saved
powerLogFile = None

def configureBoards() :
  for slot in Slots :
    try :
      subprocess.check_call(["ambslp_procedure","--slot",str(slot),"configure", "status"])
    except CalledProcessError :
      print "Error configuring board", slot
      return -1
  return 0

def retrieveData() :
  for slot in Slots :
    res = AMBDCDCUtility.ReadDCDCInfo(slot)
    
    # read the temperatures of the LAMBs from the rear and fron sensors
    # creating 2 separate arrays
    LAMBTempRear = 0x00000030
    LAMBTempFront = 0x00000034
    val = AMBDCDCUtility.VMEPeek(slot, LAMBTempRear)
    TempRear = [(val>>(8*i))&0xff for i in xrange(4)]
    val = AMBDCDCUtility.VMEPeek(slot, LAMBTempFront)
    TempFront = [(val>>(8*i))&0xff for i in xrange(4)]
    
    line = time.strftime("%Y-%m-%d %H:%M")
    line += ", 0x%08x" % AMBDCDCUtility.VMEPeek(slot, 0x2168)
    line += ", "+str(slot)
    for i in xrange(4) :
      line += ", %d, %d" % (TempFront[i], TempRear[i])
    for v in res :
      line += ", "+str(v)
    powerLogFile.write(line+"\n")
    powerLogFile.flush()
  return None

def updateDummyHit(dhval):
  for slot in Slots :
    AMBDCDCUtility.VMEPoke(slot, 0x2168, dhval)
  time.sleep(1)
  return None

if len(sys.argv[1:]) == 0 :
  powerLogFile = open("power_report.csv", "w")
else :
  powerLogFile = open(sys.argv[1], "w")
# print the header
powerLogFile.write("Time, DummyHit, Board, TempLAMB0a, TempLAMB0b, TempLAMB1a, TempLAMB1b, TempLAMB2a, TempLAMB2b, TempLAMB3a, TempLAMB3b, Vin, Vout, Iout, Temp, FPGA1Vout, FPGA1Iout, FPGA2Vout, FPGA2Iout, V0in, V0out, I0out, T0a, T0b, V1in, V1out, I1out, T1a, T1b, V2in, V2out, I2out, T2a, T2b, V3in, V3out, I3out, T3a, T3b\n")

# configure the board once if fails should interrupt the test
#if configureBoards()<0 :
#  print "Not all boards can be configured properly, exits"
#  sys.exit(-1)

retrieveData()
sys.exit(0)
   
# loop over the dummy hits and over time to read the data multiple times
for i in xrange(len(DummyHits)) :# loop over the dummy hits
  dh = DummyHits[i]
  dhp = DummyHitsPrep[i]
  # set the current dummy hit configuration 
  updateDummyHit(dhp)
  updateDummyHit(dh)
  for dt in Delays : # loop over the delays
    print "Waiting %d seconds before the next read" % dt
    time.sleep(dt) # wait
    # collect data for all the slots and store it in the table
    retrieveData() 
  updateDummyHit(0x0)
  time.sleep(InBetDelay)
  
# reset the dummy hit configuration in all slots
updateDummyHit(0x0)

powerLogFile.close()
