#!/bin/bash
set -x

source GLOBALS.sh

sh initAUXTx.sh
#sh initialization_amb_v20150513.sh
sh initialization_amb_v20150513Pisa.sh
sh initAUXRx.sh
vme_poke 0x780040d0 ff0f # only enable LAMB1
sh configure_loop_realevents_fromVME_LAMBv2_v20150520.sh
#sh configure_loop_realevents_fromVME_LAMBv2_v20150519.sh

ambslp_init_main --amb_lamb 2 
ambslp_init_main --amb_lamb 2

sh test_loop_no0realevents.sh 2>&1 | tee test_loop.log

check=true;

if [ "$check" = true ]; then 

    outfile=./SpyBufferArchives/spyfile_$(date +%Y%m%d_%H%M)
    
    ambslp_inp_spy_main > ${outfile}_m0.in
    ambslp_inp_spy_main --method 1 2> ${outfile}_m1.in
    ambslp_out_spy_main > ${outfile}_m0.out
    ambslp_out_spy_main --method 1 2> ${outfile}_m1.out
    ambslp_out_spy_main --method 2 2> ${outfile}_m2.out
    ./AMBCompareTestVecOutput.py ${outfile}_m1.out /afs/cern.ch/work/t/tkubota/public/AMBoardTest/roads_no0_np128k_skim.out > ${outfile}_m1.out.diff
    #./AMBCompareTestVecOutput.py ${outfile}_m1.out /afs/cern.ch/work/t/tkubota/public/AMBoardTest/dc_roads_no0_np128k_skim.out > ${outfile}_m1.out.diff
    #./AMBCompareTestVecOutput.py ${outfile}_m1.out /afs/cern.ch/work/t/tkubota/public/FTKTestVector/outroads_sim.out > ${outfile}_m1.out.diff
    ./AMBCompareTestVecOutput.py ${outfile}_m2.out /afs/cern.ch/work/t/tkubota/public/AMBoardTest/roads_no0_np128k_skim.out > ${outfile}_m2.out.diff
    #./AMBCompareTestVecOutput.py ${outfile}_m2.out /afs/cern.ch/work/t/tkubota/public/AMBoardTest/dc_roads_no0_np128k_skim.out > ${outfile}_m2.out.diff
    #./AMBCompareTestVecOutput.py ${outfile}_m2.out /afs/cern.ch/work/t/tkubota/public/FTKTestVector/outroads_sim.out > ${outfile}_m2.out.diff
fi
set +x

