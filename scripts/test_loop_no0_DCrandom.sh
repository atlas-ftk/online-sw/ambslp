#!/bin/bash
if [ $# -lt 1 ]; then
  SEEDOPT="--rs"
else
  SEEDOPT="--seed $1"
fi
echo "Seed: $SEED"

# generate hits
ambslp_gen_hits_DC_main -e 20 -r 20 --r7 20 --r6 20 -n 20  $SEEDOPT --DCmax 2   \
  --pattern_file /afs/cern.ch/work/a/annovi/public/AMBtest/johanna_outbank_HW2DC_newCache.root  \
  --bankType 0 --ssoff --hit_file randomtest.ss

# make simulation
ambslp_boardsim  --bankType 0 --ssoff --nkpatterns 128 \
 /afs/cern.ch/work/a/annovi/public/AMBtest/johanna_outbank_HW2DC_newCache.root \
 aux_{0..7}_randomtest.ss --output-file roads_randomtest.out

echo "Carico le TX fifo di AUX"
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_0_randomtest.ss --buffer 0x9
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_0_randomtest.ss --buffer 0xa
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_1_randomtest.ss --buffer 0xb
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_1_randomtest.ss --buffer 0xc
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_2_randomtest.ss --buffer 0xd
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_2_randomtest.ss --buffer 0xe
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_3_randomtest.ss --buffer 0xf
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 1 --file aux_3_randomtest.ss --buffer 0x10
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 2 --file aux_4_randomtest.ss --buffer 0xa
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 2 --file aux_5_randomtest.ss --buffer 0xb
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 2 --file aux_6_randomtest.ss --buffer 0xc
usleep 500000
aux_load_tx_fifo_main --slot 15 --fpga 2 --file aux_7_randomtest.ss --buffer 0xd
usleep 500000

echo "Resetting AMB Road FIFO"
ambslp_init_main --amb_lamb 2
ambslp_init_main --amb_lamb 2
ambslp_init_main --amb_lamb 2

echo "Resetting spy buffer"
ambslp_reset_spy_main

echo "Check if the FIFO is clean (should be 0xffff)"
vme_peek 0x780040c0

sleep 1

echo "Sparo dalla AUX"
aux_write_main --slot 15 --fpga 1 0x30 0x2
aux_write_main --slot 15 --fpga 2 0x30 0x2
echo "Ripristino la AUX"
aux_write_main --slot 15 --fpga 1 0x30 0x0
aux_write_main --slot 15 --fpga 2 0x30 0x0
