#! /bin/bash

#SLOTS=( 2 4 7 9 12 14 17 19 ); # used slots in P1 crates
SLOTS=( 15 );
CONF_FILE='./AMBFW.csv';
VERBOSE=false;
#arrays
declare -a FPGA;
declare -a SVF;
declare -a FW;
declare -a CURR_FW;
#functions
declare -f main
declare -f ReadConfig
declare -f PrintConfig
declare -f ReadLoaded
declare -f PrintLoaded
declare -f Program
declare -f CheckStatus
declare -f ShowHelp
declare -f GetOpts
#colours
RED='\033[0;31m'
CYAN='\033[0;36m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Colour

#========#
#  MAIN  #
#========#
function main() {
  GetOpts "$@";
  ReadConfig $CONF_FILE;
  if $VERBOSE; then PrintConfig; fi
  for slot in ${SLOTS[@]}; 
  do
    echo -e ${NC} 'Tesing SLOT: '$slot;
    file_name=./check_AMB_slot_$slot'.log';
    if [ -e $file_name ]; then rm $file_name ; fi
    unset CURR_FW ;
    ReadLoaded $slot;
    if $VERBOSE; then PrintLoaded; fi
    Program $slot;
    unset CURR_FW;
    ReadLoaded $slot;
    PrintLoaded > $file_name;
    if $VERBOSE; then PrintLoaded; fi
    #if [[ $(CheckStatus) != '0' ]] ;
    #then
    #  ./standalone_tests/check.sh $slot automatic LAMB |& tee $file_name;
    #else
    #  echo -e ${RED} 'FAILED AMB Programming on slot '$slot;
    #fi
  done

  echo -e ${NC} " ";
  echo -e ${GREEN} "42";
  echo -e ${NC} " ";

  exit 0;
}
#=========#
#  /MAIN  #
#=========#

function ReadConfig () 
{
  if [ -z $1 ]; then echo "ReadConfig() : Missing config file path!"; exit 1; fi

  FWFILE=$(readlink -f $1);
  echo -e ${NC} "choose to program slot $SLOT";
  echo -e ${NC} "Opening the collector $FWFILE . Start collecting the *.svf files..";

  while read line; do
    field=($line);
    fpga=${field[0]/ /};
    svf=${field[1]/ /};
    fw=${field[2]/ /};
#   we ignore empty lines and lines that start with '#' marks
    if [ "${fpga:0:1}" == "#" -o -z "${fpga}" ]; then continue; fi
      FPGA=( "${FPGA[@]}" "${fpga}" ); #extend array
      FW=( "${FW[@]}" "${fw}" );
      SVF=( "${SVF[@]}" "${svf}" ); #extend array
  done < ${FWFILE};
}

function PrintConfig()
{
  echo -e ${NC} "FW version that will be loaded: "
  arraylength=${#FPGA[@]};
  for (( j=0; j<${arraylength}; j++ ));
  do 
    echo -e ${NC} "${FPGA[j]} : ${FW[j]}";
  done;
}

function ReadLoaded ()
{
  tmp_file='./tmp_'$(date +%Y%m%d_%H%M%S);
  if [ -e $tmp_file ]; then rm -f $tmp_file; fi
  ambslp_status_main --slot $1 > $tmp_file;
  arraylength=${#FPGA[@]};
  for (( i=0; i<${arraylength}; i++ )); 
  do
    CURR_FW[$i]=$( cat $tmp_file | head | grep -i ${FPGA[i]} | tail -c 11 ) ;
  done
  rm -f $tmp_file ;
}

function PrintLoaded ()
{
  echo -e ${NC} "FW version that is loaded: "
  arraylength=${#FPGA[@]};
  for (( j=0; j<${arraylength}; j++ ));
  do 
    echo -e ${NC} "${FPGA[j]} : ${CURR_FW[j]}";
  done;
}

function Program ()
{
  if [ -z $1 ]; then echo "Program() : Missing slot number!"; exit 1; fi
  slot=$1;
  arraylength=${#SVF[@]};
  for (( i=0; i<${arraylength}; i++ ));
  do
    old=${CURR_FW[i]};
    niu=${FW[i]};
    if [ $old != $niu ]; 
    then 
      if [ "${FPGA[$i]}" != "LAMB" ]; then 
        ambslp_svfplayer --bt --slot $slot --svffile ${SVF[$i]};
      else
        ambslp_svfplayer --lamb --slot $slot --svffile ${SVF[$i]};
      fi
    else
      echo 'Skipping: '${FPGA[$i]};
    fi
  done
}

function CheckStatus ()
{
  arraylength=${#SVF[@]}
  for (( i=0; i<${arraylength}; i++ ));
  do
    if [ ${FW[i]} != ${CURR_FW[i]} ]; 
    then
      return 0;
    fi
  done
  return 1;
}

function ShowHelp ()
{
usage="$(basename "$0") [-h] [-c conf_file.csv] [-v] -- program to calculate the answer to life, the universe and everything -- or just to test few AMBs in a crate

where:
    -h  show this help text
    -c  *.csv configuration file for firmware programming
    -v  verbose mode"
    echo -e ${NC} "$usage";
}

function GetOpts() {
  # A POSIX variable.
  # Reset in case getopts has been used previously in the shell.
  OPTIND=1         
  while getopts "h?:c:v" opt; do
      case "$opt" in
      h|\?)
          ShowHelp
          exit 0
          ;;
      c)  
          CONF_FILE=$OPTARG
          ;;
      v)
          VERBOSE=true;
          ;;
      esac
  done
  shift $((OPTIND-1))
  [ "$1" = "--" ] && shift
}

main "$@";
