#!/bin/bash
set -x

if [ $# -lt 2 ]; then
    echo 'usage: test_loop_no0_random_seed_less_then_500.sh SLOT PATT_FILE [AUX]'
    echo '       AUX=1 sends data from AUX'
    echo '       AUX=0 sends data from AMB'
else
   
    AUX=0;
    NEVENTS=20
    MAX_HITS=4000
    if [ $# -eq 3 -a $3 -eq 1 ]; then
        AUX=1;
        NEVENTS=1
        MAX_HITS=500
    fi

    SLOT=$1
    PATT_FILE=$2

    SEEDOPT=$[ 1 + $[ RANDOM % 20000 ] ]
    regenhits=0
    echo Seed: $SEEDOPT

    # generate hits and simulate ONLY if LOOP==1
    #if [ $LOOP -eq 1 ]; then

# gen hits---------
    echo gen hit


#for all 128k patterns 15 events
#    ambslp_gen_hits_DC_main -e $NEVENTS -r 20 --r7 20 --r6 10 -n 20  -s $SEEDOPT --DCmax 2   \

#for all 128k patterns 2 events
#    ambslp_gen_hits_DC_main -e $NEVENTS -r 150 --r7 150 --r6 10 -n 20  -s $SEEDOPT --DCmax 2   \

#for all 2k patterns 20 events
    ambslp_gen_hits_DC_main -e $NEVENTS -r 200 --r7 200 --r6 10 -n 200  -s $SEEDOPT --DCmax 2   \
        --pattern_file $PATT_FILE \
        --pattmax 32768 --bankType 1 --hit_file randomtest.ss

    for i in {0..7};
    do
        filename="$(printf "aux_%d_randomtest.ss" "$i")"
        SSnum=$(wc -l $filename | cut -d \  -f 1)
        echo $SSnum
        if [ $SSnum -gt $MAX_HITS ]
        then
            regenhits=1
        fi
    done

    while [ $regenhits -eq 1 ]
    do
	regenhits=0
	SEEDOPT=$[ 1 + $[ RANDOM % 20000 ] ]
	echo Seed: $SEEDOPT
        # gen hits---------
	ambslp_gen_hits_DC_main -e $NEVENTS -r 200 --r7 200 --r6 1000 -n 200  -s $SEEDOPT --DCmax 2   \
  	    --pattern_file $PATT_FILE \
  	    --pattmax 32768 --bankType 1 --hit_file randomtest.ss

        for i in {0..7};
        do
            filename="$(printf "aux_%d_randomtest.ss" "$i")"

            SSnum=$(wc -l $filename | cut -d \  -f 1)
            if [ $SSnum -gt $MAX_HITS ]
            then
                regenhits=1
            fi
        done
    done

# make simulation
#ambslp_boardsim  --bankType 1 --nkpatterns 8192 --NPattsPerChip 131072 
echo     ambslp_boardsim  --bankType 1 --NPattsPerChip $NPATT $PATT_FILE \
        aux_{0..7}_randomtest.ss --output-file roads_randomtest.out

ambslp_boardsim  --bankType 1 --NPattsPerChip $NPATT $PATT_FILE \
        aux_{0..7}_randomtest.ss --output-file roads_randomtest.out


    echo Prepare End Event file
    grep ^1 aux_0_randomtest.ss > aux_ee_randomtest.ss
    wc aux_*_randomtest.ss*

    ./Convert_aux2amb_TV.py AMB_feed_randomtest.ss

    if [ $AUX -eq 1 ]; then
        echo "Loading AUX TX FIFOs"
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_0_randomtest.ss --buffer 0x9 > /dev/null
        usleep 500000 
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_ee_randomtest.ss --buffer 0xa > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_1_randomtest.ss --buffer 0xb > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_ee_randomtest.ss --buffer 0xc > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_2_randomtest.ss --buffer 0xd > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_ee_randomtest.ss --buffer 0xe > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_3_randomtest.ss --buffer 0xf > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_ee_randomtest.ss --buffer 0x10 > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_4_randomtest.ss --buffer 0xa > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_5_randomtest.ss --buffer 0xb > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_6_randomtest.ss --buffer 0xc > /dev/null
        usleep 500000
        aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_7_randomtest.ss --buffer 0xd > /dev/null
        usleep 500000
    fi
        
    echo "Resetting AMB Road FIFO"
    ambslp_init_main --amb_lamb 2  --slot $SLOT
    ambslp_init_main --amb_lamb 2  --slot $SLOT
    ambslp_init_main --amb_lamb 2  --slot $SLOT

    echo "Resetting spy buffer"
    ambslp_reset_spy_main  --slot $SLOT

    echo "Check if the FIFO is clean (should be 0xffff)"
    vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) |0x000040c0)))

    sleep 1

    if [ $AUX -eq 1 ]; then
        echo "Sparo dalla AUX"
        aux_write_main  --slot $SLOT --fpga 1 0x30 0x2
        aux_write_main --slot $SLOT --fpga 2 0x30 0x2
        echo "Ripristino la AUX"
        aux_write_main --slot $SLOT --fpga 1 0x30 0x0
        aux_write_main --slot $SLOT --fpga 2 0x30 0x0
    else
        #disable AUX holds if not using AUX
        vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x40F8))) 0xFFFF

        echo "Loading AMB HIT FIFOs"
        ambslp_feed_hit_main --hit_file AMB_feed_randomtest.ss --slot $SLOT

        #disable AUX holds if not using AUX
        vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x40F8))) 0xFFFF

        #sleep 1
        #ambslp_amchip_8b_10b_main --smart -v --slot $SLOT
        #added after
        #PASSED CONF 26, LOOP 20, Tue May 10 21:24:22 CEST 2016. Matched 23644. Matched incremental 11433921
    fi

fi

set +x
