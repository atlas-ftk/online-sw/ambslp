#!/bin/bash

SLOT=$1

thr=7
lambver=3


if [ $lambver -eq 1 ]; then
	chlen=4
	chipnum=15
        dis_top2=1
elif [ $lambver -eq 2 ]; then 
	chlen=2
	chipnum=3
        dis_top2=1
elif [ $lambver -ge 3 ]; then 
	chlen=2
	chipnum=3
        dis_top2=0
else 
	echo "LAMB version not supported"
	return -1
fi


###  set AM chips in tmode to write patterns, High THR and disabled pattern flow to reduce noise during pattern writing function
ambslp_amchip_jpatt_cfg_main --slot $SLOT --disable_top2 0 --thr 15 --tmode 1 --disable_pflow 1  --chipnum $chipnum
ambslp_amchip_jpatt_cfg_main --slot $SLOT --disable_top2 0 --thr 15 --tmode 1 --disable_pflow 1  --chipnum $chipnum

###  INIT from JTAG to ensure previuos instrauction is activated
ambslp_amchip_init_evt_main --slot $SLOT  --chipnum $chipnum

ambslp_amchip_disable_bank_main --slot $SLOT  --chipnum $chipnum


ambslp_amchip_jpatt_cfg_main --slot $SLOT --disable_top2 $dis_top2 --thr 15 --tmode 1 --disable_pflow 1  --chipnum $chipnum
ambslp_amchip_jpatt_cfg_main --slot $SLOT --disable_top2 $dis_top2 --thr 15 --tmode 1 --disable_pflow 1  --chipnum $chipnum

ambslp_amchip_init_evt_main --slot $SLOT  --chipnum $chipnum

### write bank, banktype 1 *.patt.bz2 ; banktype 0 *.root 

ambslp_writepatterns_main --slot $SLOT --chipnum $chipnum  --bankType 1 /afs/cern.ch/user/a/annovi/work/public/FTKtests/TEST_AM06/patterns_seq32k8M.patt.bz2 --npatt 2048 # 8M patterns

#ambslp_writepatterns_main --slot $SLOT --chainlength $chlen --chipnum $chipnum  --bankType 1 /afs/cern.ch/user/a/annovi/work/public/FTKtests/TEST_AM06/patterns_seq32k8M.patt.bz2 --npatt 131072 # 8M patterns
#ambslp_writepatterns_main --slot $SLOT --ssoff  --chainlength $chlen --chipnum $chipnum  --bankType 0 --npatt 131072 ./Bank_karol.root 

ambslp_amchip_init_evt_main --slot $SLOT  --chipnum $chipnum

### set THR, Tmode 0 and enable pattern flow to start to run
ambslp_amchip_jpatt_cfg_main --slot $SLOT --disable_top2 $dis_top2 --thr $thr --tmode 0 --disable_pflow 0  $chlen --chipnum $chipnum

ambslp_amchip_jpatt_cfg_main --slot $SLOT --disable_top2 $dis_top2 --thr $thr --tmode 0 --disable_pflow 0  --chipnum $chipnum

ambslp_amchip_init_evt_main --slot $SLOT  --chipnum $chipnum
ambslp_amchip_init_evt_main --slot $SLOT  --chipnum $chipnum
ambslp_amchip_init_evt_main --slot $SLOT  --chipnum $chipnum

