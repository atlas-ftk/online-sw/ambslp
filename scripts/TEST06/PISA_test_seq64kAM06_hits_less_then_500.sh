#!/bin/bash
set -x

if [ $# -lt 1 ]; then
    echo usage: PISA_test_seq64kAM06_hits_less_then_500.sh SLOT
    exit -1;
fi

AUX=0
SLOT=$1
COMMENT=$2
OUTDIR=~/SpyBufferArchivesAM06
SEED=4
export NPATT=2048
#export NPATT=131000 # must use 128k-1
outfile=$OUTDIR/spyfile_$(date +%Y%m%d_%H%M) 
echo $outfile 
rm -f ${outfile}_m{1,2}_chips_stat.root 
echo > ${outfile}.errors
echo $COMMENT >> ${outfile}.errors
echo >> ${outfile}.errors

export PATT_FILE=/afs/cern.ch/user/a/annovi/work/public/FTKtests/TEST_AM06/patterns_seq32k8M.patt.bz2

echo
echo START TEST $(date)
echo
./configure_bank_AM06_new.sh $SLOT $NPATT $PATT_FILE

#sh configure_16112015.sh $SLOT $AUX
#sh configure_bank_16112015.sh $SLOT

python -O -m py_compile ../AMBCompareTestVecOutput.py
NMATCHED_INCREMENTAL=0;
NERRORS=0;
NDISP_ERRORS=0;

for CONF in {1..200}; do
    echo "CONF $CONF, $(date)"
  # set dummy hits
  # vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) 0xf000f007
    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) 0xf000f001 # 17A per lamb
    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x0000216c))) 0x00000000
		
	# set half consumption
	vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002174))) 0x000000ff

    sh configure_16112015.sh $SLOT $AUX 
    ambslp_feed_hit_main --hit_file AMB_feed_2ee.ss --slot $SLOT

  for LOOP in {1..20}; do
    echo "CONF $CONF LOOP $LOOP, $(date)"
    export LOOP
  
    ./soft_configure_20160308.sh $SLOT $AUX

    ambslp_init_main --amb_lamb 2 --slot $SLOT
    ambslp_init_main --amb_lamb 2 --slot $SLOT
    ambslp_init_main --amb_lamb 2 --slot $SLOT
  
    looperr=0
    sh test_loop_no0_random_seed_less_then_500.sh $SLOT $PATT_FILE $AUX
    echo "$(date)"

  
    # Link status 
    regstatus=$( vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 )|0x000040d4))))
    regerrflag=$(vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 )|0x000040c4))))

    ambslp_inp_spy_main --slot $SLOT > ${outfile}_m0.in
    ambslp_out_spy_main --slot $SLOT > ${outfile}_m0.out
    ambslp_out_spy_main --slot $SLOT --method 1 2> ${outfile}_m1.out
   
    roadfile=roads_randomtest.out

    echo "CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}_m1.out.diff
    echo "Linkstatus: ${regstatus}, Errflag: ${regerrflag}" >> ${outfile}_m1.out.diff
    # mask out LAMBs with -L 2 -L 3 
    python ../AMBCompareTestVecOutput.pyo -L 0 -L 1 -L 2 -A ${outfile}_m1_chips_stat.root ${outfile}_m1.out $roadfile > ${outfile}_m1.out.diff || looperr=1
    NMATCHED=$(grep Fully ${outfile}_m1.out.diff | tail -n 1 | sed -e "s,Fully matched: ,,")
    NMATCHED_INCREMENTAL=$(( $NMATCHED_INCREMENTAL + $NMATCHED ))
    echo "TAIL DIFF CONF $CONF, LOOP $LOOP, $(date)"
    tail ${outfile}_m1.out.diff
    echo STATUS
    ambslp_status_main  $SLOT
    echo LENGTH
    wc -l aux_*_randomtest.ss 
    echo NEvents
    grep ^1 aux_0_randomtest.ss | wc

    #echo "CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}_m2.out.diff 
    #echo "Linkstatus: ${regstatus}, Errflag: ${regerrflag}" >> ${outfile}_m2.out.diff
    #./AMBCompareTestVecOutput.py -L 2 -L 1 -L 0 -A ${outfile}_m2_chips_stat.root ${outfile}_m2.out $roadfile >> ${outfile}_m2.out.diff
    #echo "$(date)"

    #Compare AMBout and AUXin
    #./dumpAUXinAMBFormat.py
    #./AMBCompareTestVecOutput.py output.txt $roadfile > ${outfile}_m2_aux.out.diff

    #Compare AUXout and AMBin
    ./dumpAUXoutAMBFormat.py
    sed -e 's/f70/000/g' ${outfile}_m1.in | grep 0000 > input2.txt
    nmis=`diff input.txt input2.txt | wc -l`
    echo "The number of mismatches between AUXout and AMBin: "$nmis

    # check for disparity errors
    DISP_ERROR=$(ambslp_status_main | grep disparity | grep -v 0x0\$); 
    DISP_ERROR_L3=$(ambslp_status_main | grep disparity | grep LAMB3 | grep -v 0x0\$);
    if [ "$DISP_ERROR_L3" ]; then 
        looperr=1;
        NDISP_ERRORS=$(($NDISP_ERRORS + 1));
        echo "DISPARITY ERRORS in CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}.errors
        ambslp_status_main | grep disparity | grep LAMB3 >> ${outfile}.errors
    fi

    if [ $looperr -ne 0 ]; then
      NERRORS=$(($NERRORS + 1 ))
      echo "An error was found in LOOP $LOOP, save the loop"

      echo "TAIL DIFF CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}.errors
      tail ${outfile}_m1.out.diff >> ${outfile}.errors
      echo DIFF file is ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out.diff >> ${outfile}.errors
      echo >> ${outfile}.errors
      echo >> ${outfile}.errors

      # make the loop persistent
      mv -v ${outfile}_m0.in ${outfile}_CONF${CONF}_LOOP${LOOP}_m0.in
      #mv -v ${outfile}_m1.in ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.in
      mv -v ${outfile}_m0.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m0.out
      mv -v ${outfile}_m1.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out
      #mv -v ${outfile}_m2.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m2.out
      mv -v ${outfile}_m1.out.diff ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out.diff
      #mv -v ${outfile}_m2.out.diff ${outfile}_CONF${CONF}_LOOP${LOOP}_m2.out.diff
      cp roads_randomtest.out ${outfile}_CONF${CONF}_LOOP${LOOP}_roads_randomtest.out
    else
	echo "PASSED CONF $CONF, LOOP $LOOP, $(date). Matched $NMATCHED (incr. $NMATCHED_INCREMENTAL). N errors $NERRORS. N disparity errors: $NDISP_ERRORS" >> ${outfile}.errors
    fi
  done
done
vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 )|0x00002168))) 0x00000000
vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 )|0x0000216c))) 0x00000000

set +x
