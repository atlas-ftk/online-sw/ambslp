#!/bin/bash
if [ $# -lt 1 ]; then
  SEEDOPT="--rs"
else
  SEEDOPT="--seed $1"
fi
echo "Seed: $SEEDOPT"
SLOT=$2
# generate hits
#original
#ambslp_gen_hits_DC_main -e 1 -r 200 --r7 800 --r6 200 -n 200  $SEEDOPT --DCmax 2   \
#ambslp_gen_hits_DC_main -e 5 -r 40 --r7 160 --r6 40 -n 40  $SEEDOPT --DCmax 2   \


#ok ambslp_gen_hits_DC_main -e 5 -r 40 --r7 40 --r6 160 -n 40  $SEEDOPT --DCmax 2   \

# no errora ambslp_gen_hits_DC_main -e 5 -r 40 --r7 40 --r6 200 -n 40  $SEEDOPT --DCmax 2   \

# errors at 300 hits
ambslp_gen_hits_DC_main -e 1 -r 200 --r7 200 --r6 1000 -n 200  $SEEDOPT --DCmax 2   \
  --pattern_file $PATT_FILE \
  --pattmax 32768 --bankType 1 --hit_file randomtest.ss

# make simulation
ambslp_boardsim  --bankType 1 --nkpatterns 8192 --NPattsPerChip 131072 \
 $PATT_FILE \
 aux_{0..7}_randomtest.ss --output-file roads_randomtest.out

echo Prepare End Event file
grep ^1 aux_0_randomtest.ss > aux_0_randomtest.ss.eefile
wc aux_*_randomtest.ss*


echo "Carico le TX fifo di AUX"
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_0_randomtest.ss --buffer 0x9 > /dev/null
usleep 500000 
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_0_randomtest.ss.eefile --buffer 0xa > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_1_randomtest.ss --buffer 0xb > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_0_randomtest.ss.eefile --buffer 0xc > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_2_randomtest.ss --buffer 0xd > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_0_randomtest.ss.eefile --buffer 0xe > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_3_randomtest.ss --buffer 0xf > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file aux_0_randomtest.ss.eefile --buffer 0x10 > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_4_randomtest.ss --buffer 0xa > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_5_randomtest.ss --buffer 0xb > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_6_randomtest.ss --buffer 0xc > /dev/null
usleep 500000
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file aux_7_randomtest.ss --buffer 0xd > /dev/null
usleep 500000

echo "Resetting AMB Road FIFO"
ambslp_init_main --amb_lamb 2 --slot $SLOT
ambslp_init_main --amb_lamb 2 --slot $SLOT
ambslp_init_main --amb_lamb 2 --slot $SLOT

echo "Resetting spy buffer"
ambslp_reset_spy_main  --slot $SLOT

echo "Check if the FIFO is clean (should be 0xffff)"
vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040c0)))

sleep 1

echo "Sparo dalla AUX"
aux_write_main  --slot $SLOT --fpga 1 0x30 0x2
aux_write_main  --slot $SLOT --fpga 2 0x30 0x2
echo "Ripristino la AUX"
aux_write_main  --slot $SLOT --fpga 1 0x30 0x0
aux_write_main  --slot $SLOT --fpga 2 0x30 0x0
