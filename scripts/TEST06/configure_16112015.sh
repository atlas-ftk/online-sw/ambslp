
#!/bin/bash

echo RUNNING configure_16112015.sh $@

if [ $# -lt 2 ]; then
    echo usage: configure_16112015.sh SLOT AUX	
else
	
	SLOT=$1
	AUX=$2
	if [ $AUX -eq 1 ]; then
	    sh reset_AUX.sh $SLOT
            #enable AUX holds if not using AUX
            vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x40F8))) 0x0
        else
            #disable AUX holds if not using AUX
            vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x40F8))) 0xFFFF
	fi
        echo check DISABLE of AUX HOLDs
        vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x40F8))) 

            # check status of links just before reconfiguring
                echo Checksum is: $(vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x10))) ) -- $(date)
 
		#ambslp_init_main --slot $SLOT --amb_lamb 3
		ambslp_init_main --slot $SLOT --amb_lamb 1
		ambslp_config_reg_main --slot $SLOT --fixpolarity 0 --boardver 1  

		ambslp_amchip_8b_10b_main --slot $SLOT
		ambslp_amchip_8b_10b_main --slot $SLOT
		#ambslp_amchip_8b_10b_main --smart -v --slot $SLOT
		STATUS=0
		ambslp_reset_gtp --slot $SLOT --max_tries 19 || STATUS=1

                ambslp_init_main --slot $SLOT --amb_lamb 1
                ambslp_config_reg_main --slot $SLOT --fixpolarity 0 --boardver 1

                ambslp_amchip_8b_10b_main --slot $SLOT
                ambslp_amchip_8b_10b_main --slot $SLOT
	        if [ $AUX -eq 0 ]; then
                    #disable AUX holds if not using AUX
                    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x40F8))) 0xFFFF
	        fi

                ambslp_amchip_8b_10b_main --smart -v --slot $SLOT


		ambslp_init_main --slot $SLOT
		ambslp_init_main --slot $SLOT


		ambslp_init_main --slot $SLOT --amb_lamb 2
		ambslp_init_main --slot $SLOT --amb_lamb 2

		ambslp_reset_spy_main --slot $SLOT
	

        if [ $AUX -eq 1 ]; then
		aux_write_main --slot $SLOT --fpga 1 0x40 0x10
		aux_write_main --slot $SLOT --fpga 2 0x40 0x10
		aux_write_main --slot $SLOT --fpga 3 0x40 0x10
		aux_write_main --slot $SLOT --fpga 4 0x40 0x10
		aux_write_main --slot $SLOT --fpga 5 0x40 0x10
		aux_write_main --slot $SLOT --fpga 6 0x40 0x10
	fi

	ambslp_status_main --slot $SLOT
fi
