#!/bin/bash
SLOT=$1
AUX=0

if [ $# -lt 2 ]; then
    echo usage: soft_configure_20160308.sh SLOT AUX
else
    if [ "$2" = "1" ]; then
        AUX=1;
    fi

    if [ $AUX -eq 1 ]; then
#echo "Reset fifos aux"
        aux_reset_main --txfifo --slot $SLOT --fpga 1
        sleep 1
        aux_reset_main --txfifo --slot  $SLOT --fpga 2
        sleep 1
        aux_reset_main --txfifo --slot  $SLOT --fpga 3
        sleep 1
        aux_reset_main --txfifo --slot  $SLOT --fpga 4
        sleep 1
        aux_reset_main --txfifo --slot  $SLOT --fpga 5
        sleep 1
        aux_reset_main --txfifo --slot  $SLOT --fpga 6
        sleep 1

#reset the aux freeze
        aux_write_main --slot $SLOT --fpga 1 0x40 0x10
        aux_write_main --slot $SLOT --fpga 2 0x40 0x10
        aux_write_main --slot $SLOT --fpga 3 0x40 0x10
        aux_write_main --slot $SLOT --fpga 4 0x40 0x10
        aux_write_main --slot $SLOT --fpga 5 0x40 0x10
        aux_write_main --slot $SLOT --fpga 6 0x40 0x10
    fi

    ambslp_init_main --slot $SLOT 
    ambslp_init_main --slot $SLOT 


    ambslp_init_main --amb_lamb 2 --slot $SLOT 
    ambslp_init_main --amb_lamb 2 --slot $SLOT 
    ambslp_reset_spy_main --slot $SLOT 

    if [ $AUX -eq 1 ]; then
        aux_write_main --slot $SLOT  --fpga 1 0x40 0x10
        aux_write_main --slot $SLOT  --fpga 2 0x40 0x10
        aux_write_main --slot $SLOT  --fpga 3 0x40 0x10
        aux_write_main --slot $SLOT  --fpga 4 0x40 0x10
        aux_write_main --slot $SLOT  --fpga 5 0x40 0x10
        aux_write_main --slot $SLOT  --fpga 6 0x40 0x10
    fi

    ambslp_status_main --slot $SLOT 

fi

return $STATUS
