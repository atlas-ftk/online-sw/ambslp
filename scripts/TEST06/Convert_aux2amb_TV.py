#!/usr/bin/env python

import sys

DATA = []
WORD_INDEX = []
EE_STATUS = []
OUT_LINE = []

def finished():
        for ii in range(len(WORD_INDEX)):
                if WORD_INDEX[ii]!=len(DATA[ii]):
                        return False # in case any stream is not fully read
        return True

def allEE():
        for ii in range(len(EE_STATUS)):
                if not EE_STATUS[ii]:
                        return False # don't have EndEvent on all streams
        return True

#convert string to hex
def toHex(s):
    lst = []
    for ch in s:
        hv = hex(ord(ch)).replace('0x', '')
        if len(hv) == 1:
            hv = '0'+hv
        lst.append(hv)
    
    return reduce(lambda x,y:x+y, lst)

if len(sys.argv) < 1:
        print "usage: Convert_aux2amb_TV.py outputFileName"
        exit -1;

ofname = sys.argv[1]

#remap = [ '4', '5', '6', '7', '0', 'ee', '1', 'ee', '2', 'ee', '3', 'ee' ]
remap = [ '8', '9', '10', '11', '0', '1', '2', '3', '4', '5', '6', '7' ]

print "Remapping inputs as in", remap

for filenum in range(12):
        filename = "sshits_Twr22_L%s.ss" % remap[filenum]
        print "Opening file ", filename
        f = open(filename, 'r')
        DATA.append([])
        WORD_INDEX.append(0)
        EE_STATUS.append(False)
        OUT_LINE.append("")
        for line in f:
                DATA[-1].append(line.split())

#print DATA

print "Now preparing output file"

ofile = open(ofname,"w")

while not finished():
        for ii in range(len(WORD_INDEX)):
                OUT_LINE[ii] = "0x400000000"
                if not EE_STATUS[ii] and WORD_INDEX[ii]<len(DATA[ii]):
                        THIS = DATA[ii][WORD_INDEX[ii]]
                        if THIS[0] == '1' : # end event
                                OUT_LINE[ii] = "0x8f7%06x"%int(THIS[1],16)
                        else:
                                OUT_LINE[ii] = "0x0%08x"%int(THIS[1],16)
#                        OUT_LINE[ii] = DATA[ii][WORD_INDEX[ii]][1]
#                        print THIS
                        
                        if THIS[0] == '1':
                                EE_STATUS[ii] = True
                        WORD_INDEX[ii] += 1
                #ofile.write("0x%09x\t"% int(OUT_LINE[ii][1]))
#                print OUT_LINE[ii]
                ofile.write(OUT_LINE[ii])
                if ii+1 < len(WORD_INDEX):
                        ofile.write("\t")
        ofile.write("\n")
        if allEE():
                for ii in range(len(EE_STATUS)):
                        EE_STATUS[ii] = False



#        ofile.write("0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\n" % OUT_LINE



#        OutLine = "0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\n" % (0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000))
#        ofile.write("0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\n" % (0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000))


##  
##  
##  # header of the file
##  for ir in xrange(11) :
##  	ofile.write("0x%09x\t" % 0x8f7000000)
##  ofile.write("0x%09x\n" % 0x8f7000000)
##  
##  
##  for il in xrange(nlines) :
##  	ofile.write("0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\t0x%09x\n" % (0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000, 0x0f000f0ff, 0x400000000))
##  
##  # end of the file
##  for ir in xrange(11) :
##          ofile.write("0x%09x\t" % 0x8f7000001)
##  ofile.write("0x%09x\n" % 0x8f7000001)

