#!/bin/bash

SLOT=$1

if [ 1 -eq 1 ]; then 
  for fpga in {1..6}; do
    aux_reconfigure_main --fpga $fpga --slot $SLOT --epcqaddress 0x1000000
  done
fi

echo "Reset aux"
echo -$SHELL-
aux_reset_main --slot $SLOT  --fpga 1
sleep 1
aux_reset_main --slot $SLOT  --fpga 2
sleep 1
aux_reset_main --slot $SLOT  --fpga 3
sleep 1
aux_reset_main --slot $SLOT  --fpga 4
sleep 1
aux_reset_main --slot $SLOT  --fpga 5
sleep 1
aux_reset_main --slot $SLOT  --fpga 6
sleep 1

#reset the aux freeze
aux_write_main --slot $SLOT --fpga 1 0x40 0x10
aux_write_main --slot $SLOT --fpga 2 0x40 0x10
aux_write_main --slot $SLOT --fpga 3 0x40 0x10
aux_write_main --slot $SLOT --fpga 4 0x40 0x10
aux_write_main --slot $SLOT --fpga 5 0x40 0x10
aux_write_main --slot $SLOT  --fpga 6 0x40 0x10
#exit 0
