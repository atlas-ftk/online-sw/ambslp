#!/bin/bash
set -x

OUTDIR=~/SpyBufferArchivesAM06
##mkdir -p $OUTDIR
outfile=$OUTDIR/spyfile_$(date +%Y%m%d_%H%M) 
echo $outfile 
rm -f ${outfile}_m{1,2}_chips_stat.root 
echo > ${outfile}.errors

export PATT_FILE=/afs/cern.ch/user/a/annovi/work/public/FTKtests/TEST_AM06/patterns_seq32k8M.patt.bz2

echo
echo START TEST $(date)
echo
#  sh configure_16112015.sh
#./configure_bank_16112015.sh;




for CONF in {1..1}; do
  sh configure_16112015.sh 15 0

#setto il dummy hit
if [ $CONF -lt 11 ]; then
	
	#vme_poke 0x78002168 0xff00f0f0  #33A
	 vme_poke 0x78002168 0xff00f000  #21A     

elif [ $CONF -lt 21 ]; then

	#vme_poke 0x78002168 0xff00f000  #21A

        vme_poke 0x78002168 0xf000f000 #10A

else

	vme_poke 0x78002168 0xf000f000 #10A

fi

  #disable dummy hit
  #vme_poke 0x78002168 0x00000000
  
  #disable n idle in m hits
  vme_poke 0x7800216c 0x00000000


  for LOOP in {1..1}; do
    echo "LOOP $LOOP, $(date)"
  
     #reset fifos and fsms
    ./soft_configure_20160308.sh 15 0

  ambslp_init_main --amb_lamb 2 
  ambslp_init_main --amb_lamb 2
  ambslp_init_main --amb_lamb 2
  
    looperr=0

    sh test_loop_no0_random.sh $LOOP 15  #feed AUX fifos and start  
    #sh test_loop_no0_seqmod64k_thr7.sh $LOOP
    echo "$(date)"

  
    # Link status 
    regstatus=$(vme_peek 0x780040d4)
    regerrflag=$(vme_peek 0x780040c4)

    ambslp_inp_spy_main > ${outfile}_m0.in
    #ambslp_inp_spy_main --method 1 2> ${outfile}_m1.in
    ambslp_out_spy_main > ${outfile}_m0.out
    ambslp_out_spy_main --method 1 2> ${outfile}_m1.out
    #cut -b 12- ${outfile}_m1.out > ${outfile}_m1C12.out
    #ambslp_out_spy_main --method 2 2> ${outfile}_m2.out

   
    roadfile=roads_randomtest.out
    #roadfile=seqroads.out

    echo "CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}_m1.out.diff
    echo "Linkstatus: ${regstatus}, Errflag: ${regerrflag}" >> ${outfile}_m1.out.diff
    ./AMBCompareTestVecOutput.py -L 2 -A ${outfile}_m1_chips_stat.root ${outfile}_m1.out $roadfile >> ${outfile}_m1.out.diff || looperr=1
    echo "TAIL DIFF CONF $CONF, LOOP $LOOP, $(date)"
    tail ${outfile}_m1.out.diff
    echo STATUS
    ambslp_status_main 
    echo LENGTH
    wc -l aux_*_randomtest.ss 
    echo NEvents
    grep ^1 aux_0_randomtest.ss | wc

    #echo "CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}_m2.out.diff 
    #echo "Linkstatus: ${regstatus}, Errflag: ${regerrflag}" >> ${outfile}_m2.out.diff
    #./AMBCompareTestVecOutput.py -L 2 -L 1 -L 0 -A ${outfile}_m2_chips_stat.root ${outfile}_m2.out $roadfile >> ${outfile}_m2.out.diff
    #echo "$(date)"

    #Compare AMBout and AUXin
    #./dumpAUXinAMBFormat.py
    #./AMBCompareTestVecOutput.py output.txt $roadfile > ${outfile}_m2_aux.out.diff

    #Compare AUXout and AMBin
    ./dumpAUXoutAMBFormat.py
    sed -e 's/f70/000/g' ${outfile}_m1.in | grep 0000 > input2.txt
    nmis=`diff input.txt input2.txt | wc -l`
    echo "The number of mismatches between AUXout and AMBin: "$nmis

    if [ $looperr -ne 0 ]; then
      echo "An error was found in LOOP $LOOP, save the loop"

      echo "TAIL DIFF CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}.errors
      tail ${outfile}_m1.out.diff >> ${outfile}.errors
      echo DIFF file is ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out.diff >> ${outfile}.errors
      echo >> ${outfile}.errors
      echo >> ${outfile}.errors

      # make the loop persistent
      mv -v ${outfile}_m0.in ${outfile}_CONF${CONF}_LOOP${LOOP}_m0.in
      #mv -v ${outfile}_m1.in ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.in
      mv -v ${outfile}_m0.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m0.out
      mv -v ${outfile}_m1.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out
      #mv -v ${outfile}_m2.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m2.out
      mv -v ${outfile}_m1.out.diff ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out.diff
      #mv -v ${outfile}_m2.out.diff ${outfile}_CONF${CONF}_LOOP${LOOP}_m2.out.diff
      cp roads_randomtest.out ${outfile}_CONF${CONF}_LOOP${LOOP}_roads_randomtest.out
    else
	echo "PASSED CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}.errors
    fi
  done
done

vme_poke 0x78002168 0x00000000 #disable dummy hit
vme_poke 0x7800216c 0x00000000 #disable n idle m hits
set +x
