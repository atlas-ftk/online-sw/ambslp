#!/bin/bash

if [ $# -lt 2 ]; then
    echo usage: configure_bank_AM06_new.sh SLOT NPATT PATT_FILE
else
    SLOT=$1
    NPATT=$2
    PATT_FILE=$3
    if [ $# -eq 2 ]; then
	PATT_FILE=/afs/cern.ch/user/a/annovi/work/public/FTKtests/TEST_AM06/patterns_seq32k8M.patt.bz2;
    fi	

    thr=7
    lambver=2

    if [ $lambver -eq 1 ]; then
	chipnum=15
    elif [ $lambver -ge 2 ]; then 
	chipnum=3
    else 
	echo "LAMB version not supported"
	return -1
    fi



#    PATT_FILE=/home/ftk/work_Saverio/ambslp/scripts/prova.patt.bz2
    echo Checksum is: $(vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x10))) ) -- $(date)
    ambslp_writepatterns_main -v --chipnum $chipnum --thr $thr --bankType 1 $PATT_FILE --npatt $NPATT --force --slot $SLOT 
    echo Checksum is: $(vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x10))) ) -- $(date)

fi
