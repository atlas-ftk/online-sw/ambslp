#!/usr/bin/env python

import os, sys, optparse

#stream = open('roads_no0_np128k.out','r')
stream = open('/afs/cern.ch/work/t/tkubota/public/AMBoardTest/TV80/roads_no0_np128k.out','r')

roads=[]

for road in stream:
    if road[:3]=='f78' or road[2]=='2' or road[2]=='3': # valid
        roads+=[road.rstrip()]
    else:
        pass

# Dump to file
print 'Dump output to %s'%('roads_no0_np128k_skim_highstat.out')
fh=open('roads_no0_np128k_skim_highstat.out','w')
for road in roads:
    fh.write('%s\n'%road)
fh.close()
