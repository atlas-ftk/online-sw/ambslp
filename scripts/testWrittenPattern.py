#!/usr/bin/env python

import os, sys, optparse, subprocess

parser = optparse.OptionParser(description='AM06 pattern checker.')

parser.add_option('-s', '--slot', default="15",
                    help='Target slot', type="str", dest="slot")
parser.add_option('-i', '--interval', default="128",
                    help='Interval of written pattern', type="str", dest="interval")
parser.add_option('-p', '--patt_file', default='/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/FTK/Patterns/ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6/patterns_user.sschmitt.DataAlignment_xm05_ym05_Reb64_v2.HW2.170217-64PU_30x128x72Ibl-NB9-NE6-BM0_partitioning2_170219-215620_reg22_sub0.pbank.root',
                    help='PatternBank file', type="str", dest="patt")

(options, args) = parser.parse_args()

subprocess.call(["ambslp_amchip_init_evt_main", "--slot", options.slot])
subprocess.call(["ambslp_amchip_jpatt_cfg_main", "--slot", options.slot, "--tmode", "1", "--disable_pflow", "1"])
subprocess.call(["ambslp_amchip_jpatt_cfg_main", "--slot", options.slot, "--tmode", "1", "--disable_pflow", "1"])
subprocess.call(["ambslp_amchip_init_evt_main", "--slot", options.slot])
subprocess.call(["ambslp_writepatterns_main", "--slot", options.slot, "--pattern_file", options.patt, "--write_patterns", "0", "--test_written_pattern", "1", "--check_interval_post", options.interval, "--nojtagcf"])


# PatternBank for SA test (as of 22/12/2017)
# '/afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root'

# PatternBank for commissioning (as of 22/12/2017)
# 'ftk.64tower.DataAlignment.xm05_ym05.NB9-NE6/patterns_user.sschmitt.DataAlignment_xm05_ym05_Reb64_v2.HW2.170217-64PU_30x128x72Ibl-NB9-NE6-BM0_partitioning2_170219-215620_reg22_sub0.pbank.root'
