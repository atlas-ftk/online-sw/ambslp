#!/bin/bash

STATUS=0

echo "Invio un reset"
#reset_gtp_hit.sh
ambslp_init_main --slot $AMBSLOT
sleep 1
ambslp_init_main --amb_lamb 1 --slot $AMBSLOT
sleep 1

echo "Configuro la board and turn ON DC/DC"
ambslp_config_reg_main --slot $AMBSLOT --boardver 1 
vme_poke 0x78004140 1f4 #set 500 idle words to recognize EE

sleep 1 

ambslp_amchip_8b_10b_main --slot $AMBSLOT
ambslp_amchip_8b_10b_main --slot $AMBSLOT
#ambslp_amchip_8b_10b_all_buses_main --slot $AMBSLOT --chainlength 2 --chipnum 3
#ambslp_amchip_8b_10b_pattout_only_main --slot $AMBSLOT --chainlength 2 --chipnum 3

echo "AMB GTP reset before AMchips in order to get HIT GTP in correct status"
ambslp_reset_gtp --max_tries 19

sleep 1

sleep 1

echo "AMB GTP reset after AMchips in order to get also ROAD GTPS in correct status"
ambslp_reset_gtp --max_tries 19 || STATUS=1
sleep 1

echo "Inizializzo gli spy-buffer"
ambslp_init_main --slot $AMBSLOT 
sleep 1
ambslp_init_main --slot $AMBSLOT # duplicate to ensure clean up

ambslp_reset_spy_main --slot $AMBSLOT
sleep 3

vme_poke 0x780040d0 f000 #is a force on the EE for the missing LAMB, in order to run many events. Delete when all the LAMBs are present.
echo "Leggo lo Status della board"
ambslp_status_main --slot $AMBSLOT

exit $STATUS

