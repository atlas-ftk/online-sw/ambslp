#!/bin/bash

if [ 1 -eq 1 ]; then 
  for fpga in {1..6}; do
    aux_reconfigure_main --fpga $fpga --slot 15 --epcqaddress 0x1000000
  done
fi

echo "Reset"
aux_reset_main --slot 15 --fpga 1
sleep 1
aux_reset_main --slot 15 --fpga 2
sleep 1
aux_reset_main --slot 15 --fpga 3
sleep 1
aux_reset_main --slot 15 --fpga 4
sleep 1
aux_reset_main --slot 15 --fpga 5
sleep 1
aux_reset_main --slot 15 --fpga 6

exit 0
