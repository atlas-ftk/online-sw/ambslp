#!/usr/bin/env python

import os, sys, optparse

parser = optparse.OptionParser(description='AUX spybuffer converter.')

parser.add_option('-s', '--slot', default="15",
                    help='Target slot', type="int", dest="slot")
parser.add_option('-f', '--fpga', action='append',
                    help='AUX FPGA to query', type="int", dest="fpga")
parser.add_option('-o', '--output', default='output.txt',
                    help='Location to store merged buffers.', type="str", dest="output")

(options, args) = parser.parse_args()

#if options.fpga==None: options.fpga=[3,4,5,6] # Default is all FPGAs
if options.fpga==None: options.fpga=[3,4,5] # Default is all FPGAs

sbs=[0x12,0x13,0x14,0x15]

print options

# All data
streamdata=[]
for fpga in options.fpga:
    print 'Read AUX FPGA %d'%fpga
    fpgastreamdata=[[],[],[],[]]
    nsb=0
    for sb in sbs:
        fpgadata=os.popen("aux_read_buffer_main --slot %d --fpga %d %d"%(options.slot,fpga,sb)).readlines()

        # Add data to list
        started=False
        for dataline in fpgadata[8:-1]:
            parts=dataline.split()
            if not started and parts[0]!='[0001]:': continue
            started=True
            
            fpgastreamdata[nsb].append(parts[1])
        nsb+=1
    streamdata+=fpgastreamdata

# Parse packets to determine event overlaps
print 'Merge output'
packets={}
for fpgastreamdata in streamdata:
    event=[]
    for word in fpgastreamdata:
        if word[:3]=='f78': # EE
        #if word[:3]=='f70': # EE
            l1id=int(word[3:],16)
            if l1id not in packets: packets[l1id]=[]
            packets[l1id]+=event

            event=[]
        else:
            event+=[word]

# Dump to file
print 'Dump output to %s'%(options.output)
fh=open(options.output,'w')
for l1id,event in packets.items():
    for word in event:
        if l1id==1: continue
        fh.write('%s\n'%word)
    fh.write('f78%05x\n'%l1id)
fh.close()
