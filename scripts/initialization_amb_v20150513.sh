#!/bin/bash

slt=15

echo "Invio un reset"
#reset_gtp_hit.sh
ambslp_init_main --slot $slt
sleep 1
ambslp_init_main --amb_lamb 1 --slot $slt
sleep 1

echo "Configuro la board"
ambslp_config_reg_main --slot $slt --boardver 1 --dcdc down
sleep 1
ambslp_amchip_8b_10b_main
ambslp_amchip_8b_10b_main
sleep 1

echo "Inizializzo gli spy-buffer"
ambslp_init_main --slot $slt 
sleep 1
ambslp_init_main --slot $slt # duplicate to ensure clean up

ambslp_reset_spy_main --slot $slt
sleep 3
echo "Leggo lo Status della board"
ambslp_status_main --slot $slt

