#!/usr/bin/env tdaq_python 

# Author: G. Volpi, provide consistent framework for board testing

import os
import sys
import cmd # package needed for basic CLI
import subprocess # allow to control subprocesses, where the actual tests run
import argparse # library to parse command line options
import shlex, shutil
import time
import tempfile
import signal

import logging
from logging.handlers import MemoryHandler

import AMBDCDCUtility

os.environ["FTK_LOAD_MODE"] = "1"

sessionLog = logging.getLogger("AMBTest")
# add stream handler
streamHandler = logging.StreamHandler()
sessionLog.addHandler(streamHandler)
fileHandler = None
AuxDirPath = None

def comb2Dicts(dic1, dic2):
  """Combine 2 dictiories into a new one"""
  newDic = dic1.copy()
  newDic.update(dic2)
  return newDic

def CallAndLog(cmdstr, loglevel=logging.DEBUG, cwd=None, 
               procin=None, procout=subprocess.PIPE, procerr=subprocess.STDOUT):
  """"Open a subprocess and print the stdoud in the log, the default process input, out and err streams can be redirected"""
  sessionLog.debug("$ "+cmdstr)
  proc = subprocess.Popen(shlex.split(cmdstr), bufsize=-1, stdin=procin, stdout=procout, stderr=procerr, cwd=cwd)
  
  #procout, procerr = proc.communicate()
  #sessionLog.debug(procout)
  if proc.stdout :
    for line in iter(proc.stdout.readline, b'') :
      sessionLog.log(loglevel, line.rstrip())
      if fileHandler :
        fileHandler.flush()
  else :
    proc.wait()
  return proc.returncode

def countoutroads(vars) :
  """Read the spy-buffer and count the number of road words and EE words"""

  outcmd = "ambslp_out_spy_main --slot {slot} --method 1".format(**vars)
  outproc = subprocess.Popen(shlex.split(outcmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  
  nroads = 0
  nee = 0
  
  for line in iter(outproc.stderr.readline, b'') : # loop over the stderr, because there is method 1 prints the messages
    val = int(line.rstrip(), 16)
    if (val&0xf7800000)==0xf7800000 :
      nee += 1
    else :
      nroads += 1
      
  return nroads, nee

def counthits(vars, method):
  """Count the number of SS words seen in the input spybuffers"""
  outcmd = "ambslp_spybuffer_dumper_main --inp --method 1 --slot {slot}".format(**vars)
  outproc = subprocess.Popen(shlex.split(outcmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  
  
  nhits = 0
  nee = 0
  
  for line in iter(outproc.stderr.readline, b'') : # loop over the stderr, because there is method 1 prints the messages
    elems = line.split()
   
    val = int(elems[1], 16)

    if (val&0xf7000000)==0xf7000000 :
      nee += 1
    else :
      nhits += 1
  
  return nhits, nee

def compare_outspy(vars) :
  """Compare the output spy-buffer with the <outroads> file"""

  datastream = None
  
  NEvents = AMBDCDCUtility.VMEPeek(int(vars["slot"]), 0x20FC)
  sessionLog.info("#Events checked just before taking spybuffer: " + str(NEvents))

  if vars["spyout"]=="" :
    print vars
    outcmd = "ambslp_spybuffer_dumper_main --tag ambslp_spybuffer --method 4 --dump_meth4out 1 --slot {slot}".format(**vars)
    print outcmd
    #outfile = tempfile.mkstemp("out", "outspy_m1", dir=vars["tempdir"])
    outproc = subprocess.Popen(shlex.split(outcmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    datastream = outproc.stderr
#    print datastream
  else :
    sessionLog.info("Using as data stream the spy buffers stored in: "+vars["spyout"])
    datastream = open(vars["spyout"])

  cmpcmd = "./AMBCompareTestVecOutput.py {outroads} --SuppressMissingEE".format(**vars)
  res = CallAndLog(cmpcmd, procin=datastream)

  NEvents2 = AMBDCDCUtility.VMEPeek(int(vars["slot"]), 0x20FC)
  sessionLog.info("#Events checked just after comparison: " + str(NEvents2))

  return res

def prbs_HIT2amchips(vars):
  """Control PRBS function between the HIT FPGA and AM chips"""
  return 0

def prbs_ambchips2ROAD(vars):
  """Control PRBS function between the AM chips and ROAD FPGA"""
  return 0

def generate_rndm_hits(vars, nevents=10, nroads=10, nroads7=0, nroads6=0, 
                       nnoise=10, pattaddr=None):
  """This method use the random hit generation functionality to provide 
  artificial test-vectors."""
  
  if not AuxDirPath :
    sessionLog.error("Impossible to create the random hit file, no temp dir available.")
    return -1

  hitsfile = os.path.join(AuxDirPath, "rndmhits.ss")
  curDic = comb2Dicts(vars, {"hitsfile": hitsfile, 
                             "nEvents": nevents, "nroads": nroads, "nroads7": nroads7,
                             "nroads6": nroads6, "nnoise": nnoise})  
  gencmd = "ambslp_gen_hits_DC_main --pattern_file {bankpath} --bankType {banktype} -e {nEvents} -r {nroads} --r7 {nroads7} --r6 {nroads6} -n {nnoise} --amb --hit_file {hitsfile}".format(**curDic)  
  if pattaddr :
    gencmd += " --patt_address "+pattaddr+" --dp"
  
  CallAndLog(gencmd)
  # if all is ok, the generated file becames the default input file
  sessionLog.info("Updated ambhits to "+hitsfile)
  vars["ambhits"] = hitsfile
  return 0

def aux_reset_board(vars, reconf=False):
  """The function performs resets on the AUX, used when reverting the status
  of this board is relevant for the the AMB status"""

  if reconf :
    sessionLog.info("Reconfigure AUX firmware")
    for fpga in range(1,7) :
      curVars = comb2Dicts(vars, {"fpga": str(fpga)})
      auxcmd = "aux_reconfigure_main --fpga {fpga} --slot {slot} --epcqaddress 0x1000000".format(**curVars)
      CallAndLog(auxcmd)

  sessionLog.info("Reset AUX")
  for fpga in range(1,7) :
    curVars = comb2Dicts(vars, {"fpga": str(fpga)})
    auxcmd = "aux_reset_main --slot {slot} --fpga {fpga}".format(**curVars)
    CallAndLog(auxcmd)

  return 0

def configure_amb(vars):
  sessionLog.info("Configuring the board")
  #runcmd = "ambslp_procedure --ForceWrite {forcebank} -N {npatts} -T {banktype} --slot {slot} --bankpath {bankpath} --testFilePath {ambhits} config load connect status".format(**vars)
  if vars["ambhits"]=="" :
    runcmd = "ambslp_procedure --slot {slot} -P {PartName} -R {RMName} config connect".format(**vars)
  else :
    runcmd = "ambslp_procedure --slot {slot} -P {PartName} -R {RMName} --testFilePath {ambhits} config connect".format(**vars)
  CallAndLog(runcmd)
  return None

def auxamb_test(vars, loop=False, verb=False):
  """Setup the AMB and the AUX to performe a combined standalone test"""
  
  # reset the aux
  auxresetLvl = int(vars["auxreset"])
  if auxresetLvl>0 :
    aux_reset_board(vars, auxresetLvl>1)
   
  configure_amb(vars)

  # load AUX fifo in FPGA 1
  sessionLog.info("Loading AUX FIFos")
  for iel in xrange(8) :
    tempVars = comb2Dicts(vars, {"auxhits": vars["auxhits%d" % (iel+4)], "buffer": hex(0x9+iel)})
    auxcmd = "aux_load_tx_fifo_main --slot {slot} --fpga 1 --file {auxhits} --buffer {buffer}".format(**tempVars)
    CallAndLog(auxcmd)
  for iel in xrange(4) :
    tempVars = comb2Dicts(vars, {"auxhits": vars["auxhits%d" % iel], "buffer": hex(0xa+iel)})
    auxcmd = "aux_load_tx_fifo_main --slot {slot} --fpga 2 --file {auxhits} --buffer {buffer}".format(**tempVars)
    CallAndLog(auxcmd)
  
  # send the hits
  sessionLog.info("Sending hits from AUX")
  for fpga in range(1,3) :
    tempVars = comb2Dicts(vars, {"fpga": str(fpga)})
    auxcmd = "aux_write_main --slot {slot} --fpga {fpga} 0x30 0x2".format(**tempVars)
    CallAndLog(auxcmd)
    
  # ignore hold from AUX
  AMBDCDCUtility.VMEPoke(int(vars["slot"]), 0x40f8, 0xffff)

  # restore AUX status
  sessionLog.info("Stopping AUX")
  for fpga in range(1,3) :
    tempVars = comb2Dicts(vars, {"fpga": str(fpga)})
    auxcmd = "aux_write_main --slot {slot} --fpga {fpga} 0x30 0x0".format(**tempVars)
    CallAndLog(auxcmd)

  sessionLog.info("AMB Stop transition")
  stopcmd = "ambslp_procedure --slot {slot} -P {PartName} -R {RMName} stop status".format(**vars)
  print("DIOPORCO sono a linea 208")
  CallAndLog(stopcmd)

  # restore hold from AUX
  AMBDCDCUtility.VMEPoke(int(vars["slot"]), 0x40f8, 0x0)
  
  return 0

def standalone_test(vars, loop=False, verb=False):
  """Setup the AMB and the AUX to performe a standalone test"""
  
  # reset the aux
  auxresetLvl = int(vars["auxreset"])
  if auxresetLvl>0 :
    aux_reset_board(vars, auxresetLvl>1)
  
  config = int(vars["doconfigure"])
  if config>0 :
    configure_amb(vars)

  # enable low power comsumption to mitigate disparity errors
  # AMBDCDCUtility.VMEPoke(int(vars["slot"]), 0x2174, 0xf)

  #runcmd = "ambslp_procedure --slot {slot} --testFilePath {ambhits} run status".format(**vars)
  if vars["ambhits"]=="" :
    runcmd = "ambslp_procedure --slot {slot}  -P {PartName} -R {RMName} run".format(**vars)
  else :
    runcmd = "ambslp_procedure --slot {slot}  -P {PartName} -R {RMName} --testFilePath {ambhits} run".format(**vars)
  CallAndLog(runcmd)

  time.sleep(10)
  runcmd = "ambslp_procedure --slot {slot}  -P {PartName} -R {RMName} status".format(**vars)
  CallAndLog(runcmd)

  # check dataflow is going or stops
  NEvents1 = AMBDCDCUtility.VMEPeek(int(vars["slot"]), 0x20FC)
  time.sleep(1)
  NEvents2 = AMBDCDCUtility.VMEPeek(int(vars["slot"]), 0x20FC)

  if NEvents1 == NEvents2:
    sessionLog.error("No dataflow or dataflow stops.")

  if NEvents2 < 50:
    sessionLog.error("The number of events flowing are fewer than that of expected.")

  return 0

def standalone_test_stop(vars):
  "Run the commands to interrupt a standalone test"
  sessionLog.info("Send init to stop hit loop")
  initcmd = "ambslp_init_main --slot {slot}".format(**vars)
  CallAndLog(initcmd)
  
  sessionLog.info("Stop transition")
  stopcmd = "ambslp_procedure --slot {slot} -P {PartName} -R {RMName} stop".format(**vars)
  CallAndLog(stopcmd)
  
  AMBDCDCUtility.VMEPoke(int(vars["slot"]), 0x40f8, 0x0)
  return 0

def check_outspy(vars, method=2):
  dumpcmd = "amslp"
  return res

def dump_spybuffers(vars, dumpAux=False):
  """"Dump the spy-buffers in a directory"""
  
  if not AuxDirPath :
    sessionLog.error("AUX directory doesn't exist, spy-buffers dump cannot be created")
    return -1

  # dump a clear status, similar to the normal status  
  ambstatusfile = open(os.path.join(AuxDirPath, "ambstatus.out"), "w") 
  ambstatuscmd = "ambslp_status_main --slot {slot}".format(**vars)
  CallAndLog(ambstatuscmd, procout=ambstatusfile)
  ambstatusfile.close()

  ambinfile0 =  open(os.path.join(AuxDirPath, "ambin.out"), "w")
  ambincmd = "ambslp_inp_spy_main --slot {slot}".format(**vars)
  CallAndLog(ambincmd, procout=ambinfile0)
  ambinfile0.close()
  
  amboutfile0 = open(os.path.join(AuxDirPath, "ambout.out"), "w")
  amboutcmd0 = "ambslp_out_spy_main --slot {slot}".format(**vars)
  CallAndLog(amboutcmd0, procout=amboutfile0)
  amboutfile0.close()
  
  amboutfile1 = open(os.path.join(AuxDirPath, "ambout1.out"), "w")
  amboutcmd1 = "ambslp_out_spy_main --slot {slot} --method 1".format(**vars)
  CallAndLog(amboutcmd1, procerr=amboutfile1)
  amboutfile1.close()
  
  amboutfile2 = open(os.path.join(AuxDirPath, "ambout2.out"), "w")
  amboutcmd2 = "ambslp_out_spy_main --slot {slot} --method 2".format(**vars)
  CallAndLog(amboutcmd2, procerr=amboutfile2)
  amboutfile2.close()
  
  # define the command used to dump the spybiffers
  cmd = "ambslp_spybuffer_dumper_main --slot {slot} --both --method 2 --sim --tag \"\" --sync".format(**vars)
  CallAndLog(cmd, cwd=AuxDirPath)  
  
  vars["spyout"] = os.path.join(AuxDirPath, "meth2_.txt")
  for i in xrange(12) :
    vars["auxhits%d" % i] = os.path.join(AuxDirPath, "sshits_nover__L%d.ss" % i)
  
  if dumpAux :
    auxstatusfile = open(os.path.join(AuxDirPath, "auxstatus.out"), "w")
    auxstatuscmd = "aux_status_main --slot {slot}".format(**vars)
    CallAndLog(auxstatuscmd, procout=auxstatusfile)
    auxstatusfile.close()
    
    auxoutfile0 = open(os.path.join(AuxDirPath, "auxout1.out"), "w")
    auxoutcmd0 = "aux_read_buffer_main --slot {slot} --fpga 1 0x9 0xa 0xb 0xc 0xd 0xe 0xf 0x10".format(**vars)
    CallAndLog(auxoutcmd0, procout=auxoutfile0)
    auxoutfile0.close()
    
    auxoutfile1 = open(os.path.join(AuxDirPath, "auxout2.out"), "w")
    auxoutcmd1 = "aux_read_buffer_main --slot {slot} --fpga 2 0xa 0xb 0xc 0xd".format(**vars)
    CallAndLog(auxoutcmd1, procout=auxoutfile1)
    auxoutfile1.close()
    
    #"aux_read_buffer_main --slot {slot} --fpga 3 --buffer 0x12 0x13 0x14 0x15 > auxin3.out".format(**vars)
    #"aux_read_buffer_main --slot {slot} --fpga 4 --buffer 0x12 0x13 0x14 0x15 > auxin4.out".format(**vars)
    #"aux_read_buffer_main --slot {slot} --fpga 5 --buffer 0x12 0x13 0x14 0x15 > auxin5.out".format(**vars)
    #"aux_read_buffer_main --slot {slot} --fpga 6 --buffer 0x12 0x13 0x14 0x15 > auxin6.out".format(**vars)
    
    auxlinkfile = open(os.path.join(AuxDirPath, "auxlink.out"), "w")
    auxlinkcmd = "aux_linkstatus_main --slot {slot}".format(**vars)
    CallAndLog(auxlinkcmd, procout=auxlinkfile)
    auxlinkfile.close()
    
  return None

def runemulation(vars, nevents=-1):
  
  if not AuxDirPath :
    sessionLog.error("AUX directory doesn't exist, emulation cannot run")
    return -1
  
  tempVars = comb2Dicts(vars, {"nEvents": str(nevents)})
  
  if vars["siminput"]=="aux" :
    simcmd = "ambslp_boardsim -T {banktype} -N {npatts} -e {nEvents} {bankpath} {auxhits0} {auxhits1} {auxhits2} {auxhits3} {auxhits4} {auxhits5} {auxhits6} {auxhits7} {auxhits8} {auxhits9} {auxhits10} {auxhits11}".format(**tempVars)
  elif vars["siminput"]=="amb" :
    simcmd = "ambslp_boardsim -T {banktype} -N {npatts} -e {nEvents} {bankpath} {ambhits}".format(**tempVars)
  else :
    sessionLog.error("Simulation input type unkonw, valid input types: amb, aux")
    return -1
  
  res = CallAndLog(simcmd, cwd=AuxDirPath)
   
  vars["outroads"] = os.path.join(AuxDirPath,"outroads_sim.out")
  sessionLog.info("Roads file for comparison updated to: "+vars["outroads"])
  
  return res

class AMBTestCmd(cmd.Cmd) :
  """This class provides a CLI interface to run repetitive tests"""
  
  def __init__(self):
    """"Initialize the command interface"""
    cmd.Cmd.__init__(self)
    
    # provide a dictionary with variables that are shared by the tests
    # all values are strings
    self.TestVariables = {"ambhits": "", "bankpath": "",
                          "banktype": "0", 
                          "forcebank": "0", 
                          "npatts": "131072", "slot": "15",
                          "PartName":"FTK.data.xml",  "RMName":"PU-1-15",
                          "auxhits0": "", "auxhits1": "",
                          "auxhits2": "", "auxhits3": "",
                          "auxhits4": "", "auxhits5": "",
                          "auxhits6": "", "auxhits7": "",
                          "auxhits8": "", "auxhits9": "",
                          "auxhits10": "", "auxhits11": "",
                          "auxreset": "0",
                          "siminput": "aux",
                          "outroads": "",
                          "spyout": "",
                          "doslowsct": "0" ,
                          "doconfigure": "1" ,
                          "keeptmpdir": ""}
    return None
  
  def emptyline(self):
    return None
  
  def do_loglevel(self, text):
    """" Print or set the current verbosity level for the messages printed on screen"""
    elems = text.split()
    if len(elems)==0 :
      print streamHandler.level
    elif len(elems)==1 :
      streamHandler.setLevel(text.upper())
    else :
      print "*** wrong syntax"
    return None
  
  def do_exit(self, text):
    """Provide an exit command to interrupt a test procedure. Directory used for logs 
and auxiliary files will be removed, to avoid this add "keep" or set the keeptmpdir
test variable to yes"""
    elems = text.split()
    if "keep" in elems :
      self.TestVariables["keepauxdir"] = "yes"
    
    
    toConfirm = "yes" in elems
    
    while 1:
      resp = raw_input("Do you confirm the end of the session (y,n)? ")
      if resp == "n" or resp == "no" :
        return 0
      elif resp == "y" or resp == "yes" :
        break
      
    # remind the axiliary file directory position and check if the user wants to remove it
    if AuxDirPath :
      print "Auxiliary files created in:", AuxDirPath
      
      resp = testcli.TestVariables["keeptmpdir"] 
      if  len(resp) == 0 :
        resp = raw_input("Do you want to keep the directory? (y or yes to keep it, otherwise will cancelled): ")
        resp = resp.lower()
        
      if resp != "y" and resp != "yes" :
        shutil.rmtree(AuxDirPath,True) 
        print "The directory was removed"
      else :
        print "The directory is still available" 
    
    
    return -1
  
  def do_EOF(self, text):
    """EOF requests exit"""
    print
    return self.do_exit(text)

  def do_set(self, text):
    """Set a global variable used by the tests.
  
  Usage: set varname varval
  
  The list of available variable name can be retrieved using the *set* command with no arguments, there is no check at this level that the variable value is correspondent to the expectation.   
  """
    
    # split the rest of the lines in tokens
    args = text.split()
    
    if len(args) == 2:
      if args[0] in self.TestVariables.keys() :
        self.TestVariables[args[0]] = args[1]
      else :        
        print "*** No valid variable: ", args[0]
    elif len(args) == 1:
      if args[0] in self.TestVariables.keys() :
        self.TestVariables[args[0]] = ""
      else :        
        print "*** No valid variable: ", args[0]
    elif len(args)==0 :
      print "Available arguments:"
      print ", ".join(self.TestVariables.keys())
    else :
      print "*** Invalid arguments: set key value"
      
    return None

  def do_shell(self, text):
    """Execute an arbitray command shell, without exiing the CI"""
    
    subprocess.call(shlex.split(text))
    return None
  
  def do_show(self, text):
    """Show the value of one or all the test variables setup"""
    keys = self.TestVariables.keys()
    keys.sort()
    for k in keys :
      v = self.TestVariables[k]
      print k, "=", v
    return None

  def do_ambtest(self, text):
    """Run a standalone tests of the AMB alone. The test uses npatts, 
bankpath and ambhits variable to set the main property of the test.
By default an AUX is assumed to be in the back and reset, according
the auxreset variable (1 normal reset, 2 FW refresh). The operation
can be skipped setting the variable to 0.

Syntax: ambtest [arg]

Possible arguments:
 * loop: loop over the input
 * stop: stop a previously started loop
    """
    
    args = text.split()
    
    if "stop" in args :
      # stop a previously running loop end exit
      standalone_test_stop(self.TestVariables)
      return None
      
    doLoop = "loop" in args
    
    standalone_test(self.TestVariables, doLoop)
    
    if doLoop :
      print "--- stop the loop using \"ambtest stop\""
    return None
  
  def do_auxambtest(self, text) :
    """Run a standalone test with input comning from the AUX. The input data is set 
throught the "auxihitX" files."""
    auxamb_test(self.TestVariables)
    return None
  
  
  def do_blocktransfer(self, text):
    """Check and set the environment variable used to control block transfer in
the FTK TDAQ software.
    
Usage: blocktransfer [yes|no]

By default only print the variable status, with yes or no set it to 1 (enabled)
with "yes", to 0 with "no". """
    
    if text == "yes" :
      os.environ["FTK_VME_REAL_BLOCK_WRITES"] = "1"
    elif text == "no" :
      os.environ["FTK_VME_REAL_BLOCK_WRITES"] = "0"
    
    print "FTK_VME_REAL_BLOCK_WRITES =", os.getenv("FTK_VME_REAL_BLOCK_WRITES")
    
      
    return None  
  
  def do_compare(self, text) :
    """Compare the data stream received by the AMB, form the output spy-buffer,
with a reference output, set by the outroads variable.

If the spyout variable is set, this file is used as reference, otherwise the
spy-buffers is obtained from the board, via method 1."""
    compare_outspy(self.TestVariables) 
    return None

  def do_counthits(self, text):
    """Count hits found in the input spy-buffer"""
    out = counthits(self.TestVariables)
    sessionLog.info("Number of hits = %d, EE #%d" % out)
    return None
  
  def do_countroads(self, text) :
    """This command reads the output spybuffer to count how many roads and end-event tags
    are seen in it. The spybuffer is dumped using method 1, so the EE will be replicated
    for each output stream associated with a plugged LAMB."""
    out = countoutroads(self.TestVariables)
    sessionLog.info("Number of roads = %d, EE #%d" % out)
  
  def do_genhits(self, text):
    """Run the random hits generation procedure. The file with the hits, in the format
used by ambslp_feed_hit_main, will be stored in the file used to store the logs
and eventually others auxiliary files. After the execution the *ambhits* variable
is update to point to the generated file.

Usage: genhits [nevents [nroads [nnoise [nroads7 [nroads6 [pattaddr]]]]]]

* nevents: number of events, an empty events is always added at the begin. Def 10.
* nroads: number or roads that will be extracted and disassembled as hits 8 SSs. Def 10.
* nnnoise: number of noise SSs, per layer. Def 0.
* nroads7: a nroads but only 7/8 SSs are extracted, the dropped layer is random. Def 0.
* nraods6: as nroads7 but only 6 layers are extarcted, roads shouldn't fire. Def 0.
* pattaddr: a file containing a  list of pattern address to be extracted. Not set by default."""
    
    
    # split the arguments
    elems = text.split()
    nels = len(elems)
    
    # possible arguments
    if nels>=1 :
      nevents = int(elems[0])
    else :
      nevents = 10
      
    if nels>=2 :
      nroads = int(elems[1])
    else :
      nroads = 10
    
    if nels>=3 :
      nroads7 = int(elems[2])
    else :
      nroads7 = 0
      
    if nels>=4:
      nroads6 = int(elems[3])
    else :
      nroads6 = 0
      
    if nels>=5 :
      nnoise = int(elems[4])
    else :
      nnoise = 0
    
    if nels>=6 :
      pattaddr = elems[5]
    else :
      pattaddr = None
      
    # call the function
    generate_rndm_hits(self.TestVariables, nevents, nroads, nroads7, nroads6, nnoise, pattaddr)
    return None
  
  def do_peek(self, text):
    """Print the value of a VME register in a board,

Usage: peek [slot] addr

- If the slot is not specified the global *slot* variable is used.
- Address is hex
- Return value is print as hex, decimand end bin    
    """
    
    elems = text.split()
    
    try :
      if len(elems)==2 :
        slot = int(elems[0],16)
        addr = int(elems[1],16)      
      elif len(elems)==1 :
        slot = int(self.TestVariables["slot"])
        addr = int(elems[0],16)
      else :
        print "*** Wrong number of arguments"
        return None
    except ValueError as err:
      print "***", err
      return None
          
    res = AMBDCDCUtility.VMEPeek(slot, addr)
    print ">> ", hex(res), res, bin(res)
    return None
  
  def do_poke(self, text):
    """Set a VME register for a board. 
Usage: poke [slot] addr value

- If the slot is not specified the global *slot* variable is used.
- Both register address and value are supposed to be in hex"""
 
    elems = text.split()

    try :    
      if len(elems)==3 :
        slot = int(elems[0],16)
        addr = int(elems[1],16)
        value = int(elems[2],16)
      elif len(elems)==2 :
        slot = int(self.TestVariables["slot"])
        addr = int(elems[0],16)
        value = int(elems[1],16)
      else :
        print "*** Wrong number of arguments"
        return None
    except ValueError as err:
      print "***", err
      return None

    AMBDCDCUtility.VMEPoke(slot, addr, value)
    
    return None

  def do_simulate(self, text):
    """Run the board simulation. The test variables are used to set the
board setup (bank file, number of patterns per chip...). The input can come
from the ambhits variable or the auxhitsX, this depends by the siminput
variable. The emulation input type is controlled by the siminput 
variable: if equal to amb, the ambhits file is used as source,
if equal to aux, the auxhitsX files are used instead.

The output file is created int he temporary output
directory and its path set in the outroads variable.

The additional argument is  the number of events to be emulated:

Usage: simulate [nevents]

In case the nevents argument is not set or invalid all the evnets in the
input are used."""

    try :
      nevents = int(text)
    except ValueError :
      nevents = -1
      
    runemulation(self.TestVariables, nevents)
    return None
  
  def do_spydump(self, text):
    """Dump the spy-buffer in the auxiliary directory.
    
This command dump input and output spybuffers using several standalone
programs and formats. The files created are:
 * ambin.out: legacy full dump of the input spy-buffer, form ambslp_inp_spy_main
 * ambout.out: legacy full dump of the output spy-buffer, form ambslp_out_spy_main
 * ambout1.out: legacy dump of the output spy-buffer with method 1
 * ambout1.out: as before but with method 2
 * input_spybuff_.txt: full input dump, with ambslp_spybuffer_dumper_main
 * input_spybuff_.txt: full output dump, with ambslp_spybuffer_dumper_main
 * meth2_.txt: synchronized output dump with the above standalone program
 * sshits_nover__LXX.ss: 12 independent dump of the input spy-buffers, synched with
        meth2_.txt, available to be used in new feedhit tests and as simulation input 
  
The sshits*.ss files will replace the auxhitsX file and meth2_.txt the outroads
path, this will make those files the default when compare or simulate are used,
allowing to build sequences of commands to verify the output of the board."""
    
    elems = text.split()
    
    dumpAux = "aux" in elems
    
    dump_spybuffers(self.TestVariables, dumpAux)
    return None
  
  def do_status(self, text):
    """Print the status of the board"""
    statuscmd = "ambslp_status_main --slot {slot}".format(**self.TestVariables)
    CallAndLog(statuscmd, logging.INFO)
    ambpowercmd = "./AMBDCDCUtility.py --CheckPower {slot}".format(**self.TestVariables)
    CallAndLog(ambpowercmd, logging.INFO)
    return None

  def do_pow_check(self, text):
    """Print the power consumption of the board, to be used during power check with balanced bitflip"""
    ambpowercmd = "./AMBDCDCUtility.py --CheckPowerBalanced {slot}".format(**self.TestVariables)
    CallAndLog(ambpowercmd, logging.INFO)
    return None
  
  def do_tmpdir(self, text) :
    """The command allows to print or change the path of the directory used
to place temporary and auxiliary files, created by some of the procedures.

Syntax: tmpdir [newdir]

The command suceeds if the newpath directory. Shell variables and other 
symbols are allowed. If the new directory can be created, the existing
files are moved in the new location."""
    global AuxDirPath
    
    if len(text)>0 :  
      # the text is considered the new path
      fullpath = os.path.expandvars(os.path.expanduser(text))
      if not os.path.exists(fullpath) :
        # the directory doesn't exists, this is ok
        try :
          shutil.copytree(AuxDirPath, fullpath)
          AuxDirPath = fullpath
        except Error :
          sessionLog.error("The directory cannot be copied in the new location")
      else :
        sessionLog.info("File or directory with the same name exists")
        return None
    
    sessionLog.info("Auxiliary files directory: "+AuxDirPath)
    return None
  
# read configuration  
parser = argparse.ArgumentParser()
parser.add_argument("-B","--batch",action="store_true",help="Run in batch mode", default=False)
parser.add_argument("--loglevel", help="Terminal log level: DEBUG, INFO, WARNING, ERROR",
                    default="DEBUG", dest="loglevel")
parser.add_argument("--auxdir", help="Directory for session log or temporary file, if not given local temporary dir will be created. User should delete it.",
                    default="")
parser.add_argument("cmdfile",help="Command file(s), executed before the CLI starts", nargs='*')
opts = parser.parse_args()

# create a directory, or just point to an existing one, where auxiliary
# files eventually needed during the tests will be created. The
# directory can be removed, default behaviour, at the end of the session 
if opts.auxdir != "" :
  try :
    os.mkdir(opts.auxdir)
    AuxDirPath = opts.auxdir
  except OSError, err:
    if err.errno==17 :
      sessionLog.warn("File/Directory with the same name found, files will be overwritten")
      AuxDirPath = opts.auxdir
    else :
      sessionLog.error("Requested auxiliary cannot be created, logs won't be created and some command may not be used.")
else :
  try :
    AuxDirPath = tempfile.mkdtemp(prefix="AMBTest_")
  except:
    sessionLog.error("No auxiliary directory can be created locally, some functions may not work.")

if AuxDirPath : 
  print("Auxiliary files path: "+AuxDirPath)


# try to open the history file for the user
import atexit
historyPath = os.path.expanduser("~/.AMBtest_history")
try:
  import readline
except ImportError:
  print "Module readline not available."
else:
  import rlcompleter
  readline.parse_and_bind("tab: complete")
  if os.path.exists(historyPath):
    readline.read_history_file(historyPath)

def save_history(historyPath=historyPath):
  try:
    import readline
  except ImportError:
    print "Module readline not available."
  else:
    readline.write_history_file(historyPath)

atexit.register(save_history)
# prepare the session logger
sessionLog.setLevel(logging.DEBUG)
streamHandler.setLevel(opts.loglevel)
if AuxDirPath :
  # check the file can be created, if present this also clear it
  try :
    logfname = os.path.join(AuxDirPath, "ambtest.log")
    testfile = open(logfname, "w")
    testfile.close()
    # add a file handler
    fileHandler = logging.FileHandler(logfname)
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
    memHandler = MemoryHandler(1024, logging.DEBUG, fileHandler)
    sessionLog.addHandler(memHandler)
  except :
    sessionLog.error("Log file cannot be created")

# implement the CLI 
testcli = AMBTestCmd()

for cmdpath in opts.cmdfile :
  sessionLog.debug("Executing: " + cmdpath)
  cmdfile = open(cmdpath)
  for line in cmdfile :
    if line.startswith("#") :
      continue
    testcli.onecmd(line)

if opts.batch :
  print "All done, bye!"
  sys.exit(0)

sessionLog.debug("Starting AMB test session")
hproc = subprocess.Popen(["hostname"], stdout=subprocess.PIPE)
sessionLog.debug("Hostname: "+hproc.communicate()[0])    
testcli.prompt = "AMB Test> "
testcli.cmdloop("AMB test framework")
