#!/usr/bin/env python2

import re
import os
import optparse

parser = optparse.OptionParser()
parser.add_option("-p", "--pattfile", dest="pattfile", type="string", action="store",
                  help="Pattern file to load", default="/afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root")
parser.add_option("-o", "--outfile",  dest="outfile",  type="string", action="store",
                  help="Output file for the simulation results (roads)", default="myout.out")
parser.add_option("-e", "--events",   dest="events",   type="int",    action="store",
                  help="Number of events to simulate", default=0)
opts, args = parser.parse_args()

spyfile = args[0] #"inp_after_run.txt"
pattfile = opts.pattfile
outfile = opts.outfile
noevents = opts.events

#layers = [5, 6, 8, 10, 0, 1, 2, 3]
layers = [4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3]

line_rexp_strs = []
line_rexp_objs = []
data_rexp_strs = []
data_rexp_objs = []

slines = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
datas  = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

for j in [layers[k] for k in range(12)]:
    line_rexp_strs += [""]
    data_rexp_strs += [""]
    for i in range(12):
        if j==i:
            line_rexp_strs[-1] += "\[0000] .{8} "
            data_rexp_strs[-1] += "\[.{4}\] (.{8}) "
        else:
            line_rexp_strs[-1] += "\[.{4}\] .{8} "
            data_rexp_strs[-1] += "\[.{4}\] .{8} "
    line_rexp_strs[-1] = line_rexp_strs[-1][:-1]
    data_rexp_strs[-1] = data_rexp_strs[-1][:-1]

line_rexp_objs = [re.compile(mstr) for mstr in line_rexp_strs]

data_rexp_objs += [re.compile(mstr) for mstr in data_rexp_strs]

lineno = 0
for data in open(spyfile):
    lineno += 1
    for i in range(12):
        m = line_rexp_objs[i].match(data)
        if m:
            slines[i] = lineno
            datas[i]  = data_rexp_objs[i].match(data).group(1)

hit_data = [[] for i in range(12)]
lineno = 0
for data in open(spyfile):
    lineno += 1
    for i in range(12):
        if (lineno >= slines[i]):
            m = data_rexp_objs[i].match(data)
            if m:
                hit_data[i] += [m.group(1)]


for line in slines:
    print line

for line in datas:
    print line

for i in range(12):
    f = open("hit_%d.txt" % i, 'w')
    for h in hit_data[i]:
        if re.match("^f7", h):
            f.write("1 00"+h[2:]+"\n")
        else:
            f.write("0 "+h+"\n")
    f.close()

fnamestring = ' ' + ' '.join(["hit_%d.txt" % i for i in range(12)])
events_str = "-e %d " % noevents if (noevents>0) else " "

os.system("ambslp_boardsim --bankType 0 --NPattsPerChip 131072 "+ events_str + pattfile + fnamestring + " --output-file " + outfile)
