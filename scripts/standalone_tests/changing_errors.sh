#!/bin/bash
SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi

#This script is a substitute of the command: 'watch -n 0.5 --differences=permanent './read_PRBS_result.sh $SLOT ; echo Counters in the table ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD should change;'
#It is used in the "automatic" version of the test.
#Some of the checked counters are expected to change and some other are not expected to change. Counters are checked only twice for semplicity

source vme_functions.sh

PRESENCE=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x00006000))) )

PRBS_ROAD_CHECK=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040dc))))>>4)&0x1))

PRBS_ROAD_GEN=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x00004100))))>>0)&0x1))
PRBS_ROAD_GEN_ERR=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x00004100))))>>1)&0x1))

PRBS_HIT=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000020e0))))>>0)&0x1))

LAMB0=$((( $PRESENCE>>16)&0x1))
LAMB1=$((( $PRESENCE>>17)&0x1))
LAMB2=$((( $PRESENCE>>18)&0x1))
LAMB3=$((( $PRESENCE>>19)&0x1))

ERRORS_113_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040e8))))
ERRORS_116_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040ec))))
ERRORS_213_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040f0))))
ERRORS_216_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040f4))))

MASK_LINK=0x000000ff

SHIFT_LINK0=0
SHIFT_LINK1=8
SHIFT_LINK2=16
SHIFT_LINK3=24

if [ $PRBS_ROAD_CHECK = 1 ]; then

echo ----------------------------------ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD--------------------------------------------------------

Lamb0_Link3=$((( $ERRORS_113_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
Lamb0_Link2=$((( $ERRORS_113_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
Lamb0_Link1=$((( $ERRORS_113_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb0_Link0=$((( $ERRORS_213_ROAD>>$SHIFT_LINK2)&$MASK_LINK))

Lamb1_Link3=$((( $ERRORS_216_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb1_Link2=$((( $ERRORS_216_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
Lamb1_Link1=$((( $ERRORS_213_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
Lamb1_Link0=$((( $ERRORS_213_ROAD>>$SHIFT_LINK0)&$MASK_LINK))

Lamb2_Link3=$((( $ERRORS_116_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
Lamb2_Link2=$((( $ERRORS_116_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
Lamb2_Link1=$((( $ERRORS_216_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
Lamb2_Link0=$((( $ERRORS_216_ROAD>>$SHIFT_LINK1)&$MASK_LINK))

Lamb3_Link3=$((( $ERRORS_116_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
Lamb3_Link2=$((( $ERRORS_116_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb3_Link1=$((( $ERRORS_213_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb3_Link0=$((( $ERRORS_113_ROAD>>$SHIFT_LINK1)&$MASK_LINK))

ERRORS_113_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040e8))))
ERRORS_116_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040ec))))
ERRORS_213_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040f0))))
ERRORS_216_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040f4))))

Lamb0_Link3_b=$((( $ERRORS_113_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
Lamb0_Link2_b=$((( $ERRORS_113_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
Lamb0_Link1_b=$((( $ERRORS_113_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb0_Link0_b=$((( $ERRORS_213_ROAD>>$SHIFT_LINK2)&$MASK_LINK))

Lamb1_Link3_b=$((( $ERRORS_216_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb1_Link2_b=$((( $ERRORS_216_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
Lamb1_Link1_b=$((( $ERRORS_213_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
Lamb1_Link0_b=$((( $ERRORS_213_ROAD>>$SHIFT_LINK0)&$MASK_LINK))

Lamb2_Link3_b=$((( $ERRORS_116_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
Lamb2_Link2_b=$((( $ERRORS_116_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
Lamb2_Link1_b=$((( $ERRORS_216_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
Lamb2_Link0_b=$((( $ERRORS_216_ROAD>>$SHIFT_LINK1)&$MASK_LINK))

Lamb3_Link3_b=$((( $ERRORS_116_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
Lamb3_Link2_b=$((( $ERRORS_116_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb3_Link1_b=$((( $ERRORS_213_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
Lamb3_Link0_b=$((( $ERRORS_113_ROAD>>$SHIFT_LINK1)&$MASK_LINK))

diff_Lamb3_Link3=$(expr $Lamb3_Link3_b - $Lamb3_Link3)
diff_Lamb3_Link2=$(expr $Lamb3_Link2_b - $Lamb3_Link2)
diff_Lamb3_Link1=$(expr $Lamb3_Link1_b - $Lamb3_Link1)
diff_Lamb3_Link0=$(expr $Lamb3_Link0_b - $Lamb3_Link0)

diff_Lamb2_Link3=$(expr $Lamb2_Link3_b - $Lamb2_Link3)
diff_Lamb2_Link2=$(expr $Lamb2_Link2_b - $Lamb2_Link2)
diff_Lamb2_Link1=$(expr $Lamb2_Link1_b - $Lamb2_Link1)
diff_Lamb2_Link0=$(expr $Lamb2_Link0_b - $Lamb2_Link0)

diff_Lamb1_Link3=$(expr $Lamb1_Link3_b - $Lamb1_Link3)
diff_Lamb1_Link2=$(expr $Lamb1_Link2_b - $Lamb1_Link2)
diff_Lamb1_Link1=$(expr $Lamb1_Link1_b - $Lamb1_Link1)
diff_Lamb1_Link0=$(expr $Lamb1_Link0_b - $Lamb1_Link0)

diff_Lamb0_Link3=$(expr $Lamb0_Link3_b - $Lamb0_Link3)
diff_Lamb0_Link2=$(expr $Lamb0_Link2_b - $Lamb0_Link2)
diff_Lamb0_Link1=$(expr $Lamb0_Link1_b - $Lamb0_Link1)
diff_Lamb0_Link0=$(expr $Lamb0_Link0_b - $Lamb0_Link0)

if [[ $diff_Lamb3_Link3 != 0 && $diff_Lamb3_Link2 != 0 && $diff_Lamb3_Link1 != 0 && $diff_Lamb3_Link0 != 0 ]];
then
echo "INFO: numbers changing for LAMB3 as expected, TEST PASSED (changing_errors.sh)"
else
echo "ERROR: numbers not changing for LAMB3, please repeat the test copying the line below:"
echo "source vme_functions.sh; slot_poke $SLOT 0x000040dc 0x10; ./changing_errors.sh; slot_poke $SLOT 0x000040dc 0x0;" 
fi

if [[ $diff_Lamb2_Link3 != 0 && $diff_Lamb2_Link2 != 0 && $diff_Lamb2_Link1 != 0 && $diff_Lamb2_Link0 != 0 ]];
then
echo "INFO: numbers changing for LAMB2 as expected, TEST PASSED (changing_errors.sh)"
else
echo "ERROR: numbers not changing for LAMB2, please repeat the test copying the line below:"
echo "source vme_functions.sh; slot_poke $SLOT 0x000040dc 0x10; ./changing_errors.sh; slot_poke $SLOT 0x000040dc 0x0;" 
fi

if [[ $diff_Lamb1_Link3 != 0 && $diff_Lamb1_Link2 != 0 && $diff_Lamb1_Link1 != 0 && $diff_Lamb1_Link0 != 0 ]];
then
echo "INFO: numbers changing for LAMB1 as expected, TEST PASSED (changing_errors.sh)"
else
echo "ERROR: numbers not changing for LAMB1, please repeat the test copying the line below:"
echo "source vme_functions.sh; slot_poke $SLOT 0x000040dc 0x10; ./changing_errors.sh; slot_poke $SLOT 0x000040dc 0x0;" 
fi

if [[ $diff_Lamb0_Link3 != 0 && $diff_Lamb0_Link2 != 0 && $diff_Lamb0_Link1 != 0 && $diff_Lamb0_Link0 != 0 ]];
then
echo "INFO: numbers changing for LAMB0 as expected, TEST PASSED (changing_errors.sh)"
else
echo "ERROR: numbers not changing for LAMB0, please repeat the test copying the line below:"
echo "source vme_functions.sh; slot_poke $SLOT 0x000040dc 0x10; ./changing_errors.sh; slot_poke $SLOT 0x000040dc 0x0;" 
fi


fi

