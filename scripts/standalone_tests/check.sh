#!/bin/bash

# -------------- Firmware version ----------------------------------------
hit_firmware_version=0x2d22a1e2
ctrl_firmware_version=0x00000009
vme_firmware_version=0x000005c1 
road_firmware_version=0x55a2fbdb
lamb_firmware_version=0x06060606

# Choose the slot number
SLOT=15
if [ $# -eq 3 ]; then
  SLOT=$1
fi

MODE="interactive"
#other: automatic
if [ $# -eq 3 ]; then
  MODE=$2
fi

TEST="all"
#other: LAMB
if [ $# -eq 3 ]; then
  TEST=$3
fi

export mybasedir=`echo "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"`
export LOGFILENAME=/tmp/$USER-$(date +%Y%m%d_%H%M%S)_testAMB.log
source vme_functions.sh

ERR_STATUS_NOT_READ="1"
ERR_FIRMWARE_VERSION="2"
ERR_MISALIGNED="3"
ERR_VOLTAGE="4"

DEG=$'\xc2\xb0'

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  #SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
#SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#export SCRIPTDIR=~/public/ftknightly/ambslp/scripts/

cd $mybasedir

main() {
    
    echo "Logfile is $LOGFILENAME"
    echo "Using slot $SLOT"
    echo "Checking VME communication with all 4 FPGAs" 
    echo "Only for Pisa: if you just did a power cycle please run > tsiconfig -a vmetab.sbc-tbed-ftk-01" 

    ambslp_status_main --slot $SLOT | head

    echo "INFO: the firmware version installed is printed above (at check.sh)"
    echo -n "If the firmware version printed above is not the same written in the script check.sh a warning message will be sent after the first steps (press ENTER to continue)"

    if [ $MODE != "automatic" ]; then read cont_ok; fi
    
    echo "Resetting the AUX, can take several seconds..."
    ./reset_AUX.sh --slot $SLOT > $LOGFILENAME
    
    echo "Resetting the AUX, can take several seconds..."
    ./reset_AUX.sh --slot $SLOT > $LOGFILENAME

    echo "Running preliminary procedure for configuration..."
    echo "Configure, connect, can take some minutes..."
    ambslp_init_main --slot $SLOT --amb_lamb 1 > $LOGFILENAME
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config > $LOGFILENAME
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" connect > $LOGFILENAME
    echo "Run, status..."
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" run status > $LOGFILENAME

    source check_status.sh

    status_output=`ambslp_status_main --slot $SLOT`
    
    echo "Reading AMB status..."
    check_status "$status_output"
    if [ $? = 0 ]; then
	echo "AMB status checks are all OK."
	echo
    else
	ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect
	status_output=`ambslp_status_main --slot $SLOT`
	check_status "$status_output"
	if [ ! $? = 0 ]; then
	    echo "Couldn't fix status, exiting"
	    echo "Couldn't fix status, exiting" > $LOGFILENAME
	    exit
	fi
    fi
    echo -n "Will do power checks, after an initialization. (press ENTER to continue): "
    if [ $MODE != automatic ]; then read cont_ok; fi
    #line added because power test was gigiving high values for lambs--> non initialised LAMBs?
    ambslp_init_main --slot $SLOT --amb_lamb 1 > $LOGFILENAME
    ../AMBDCDCUtility.py --CheckPower $SLOT

    echo -n "Do you wish to run the JTAG and PRBS checks? (y/n, if 'n' the script will skip this test) "
    if [ $MODE != "automatic" ]; then read answer; else answer="yes"; fi 
    if echo "$answer" | grep -iq "^y" ;then
	./do_prbs.sh $SLOT $MODE $TEST
    else
	echo "No, going on with the next step"
    fi

    echo "The following script writes the pattern bank and checks the HOLD lines."
    echo "If the bank is not already loaded writing the pattern bank requires around 10 minutes."
    echo -n "Do you wish to run the test on the hold lines? (y/n, if 'n' the script will skip this test) "
    if [ $MODE != "automatic" ]; then read answer; else answer="yes"; fi 
    if echo "$answer" | grep -iq "^y" ;then
	TESTDIR=/tmp/ftk_$USER
	mkdir -p $TESTDIR/hold_test
	ambslp_init_main --slot $SLOT --amb_lamb 1
	./run_test.sh -i 1 --slot=$SLOT --holdtest --result=$TESTDIR/hold_test
	echo "The result of the test is printed above."
    else
	echo "No, going on with the next step"
    fi

    echo "The following step is to send the bitflip_balances8 file in loop and check the power consumption in this stress configuration"
    echo "The expected power consumption is around 30 W"
    #echo "After having checked the power the idle condition is restored (Press ENTER to continue):"
    echo -n "Do you wish to run the power consumption test? (y/n, if 'n' the script will skip this test) "
    if [ $MODE != "automatic" ]; then read answer; else answer="yes"; fi 
    if echo "$answer" | grep -iq "^y" ;then
	cd ..
	pwd
        TESTDIR=/tmp/ftk_$USER/
        mkdir -p $TESTDIR
	cp ./TVTest_power_automatic.cmd $TESTDIR/TVTestNew.cmd
	sed -i '3s/.*/set slot 15/' $TESTDIR/TVTestNew.cmd
	#cat $TESTDIR/TVTestNew.cmd
	ambslp_init_main --slot $SLOT --amb_lamb 1
	./AMBTest.py -B $TESTDIR/TVTestNew.cmd
	cd standalone_tests
        #Set the register so that it sends dummy hits in loop, read the power consumption and check that it is between 25 and 55 Watt
	#../AMBDCDCUtility.py $SLOT --CheckPowerDummy
        #reset the register to go back to the idle condition
	slot_poke $SLOT 0x00002168 0x0
    else
	echo "No, going on with the next step"
    fi

    echo -n "Now reading the DNA of the HIT FPGA and of the 4 LAMBs in the logfile (press ENTER to continue):"
    if [ $MODE != "automatic" ]; then read cont_ok; fi
    echo "\nDNA of the HIT FPGA" > $LOGFILENAME
    ./DNA_registers.py $SLOT -H > $LOGFILENAME
    ./DNA_registers.py $SLOT -H
    echo "DNA registers of the LAMBs" > $LOGFILENAME
    ./DNA_registers.py $SLOT -L > $LOGFILENAME
    ./DNA_registers.py $SLOT -L
    echo "INFO: Write these serial numbers in EDMS!!!"
    echo "Now proceeding with the run test, it will take some time (press ENTER to continue):"
    echo -n "Do you wish to proceed with the run test? (y/n, if 'n' the script will skip this test) "
    if [ $MODE != "automatic" ]; then read answer; else answer="yes"; fi 
    if echo "$answer" | grep -iq "^y" ;then
	echo "Will run the script AMBTest.py, to perform a run test type ambtest and then compare."
	echo "When finished type exit"
	cd ..
	pwd
	#the script should maybe me moved to the standalone_tests folder in future
        TESTDIR=/tmp/ftk_$USER/
        mkdir -p $TESTDIR
	cp ./TVTestAutomatic.cmd $TESTDIR/TVTestNew.cmd
	sed -i '3s/.*/set slot 15/' $TESTDIR/TVTestNew.cmd
	#sed -i '3s/.*/set slot ${SLOT}/' /tmp/ftk/TVTestNew.cmd
	#cat $TESTDIR/TVTestNew.cmd
	./AMBTest.py -B $TESTDIR/TVTestNew.cmd
	cd standalone_tests
	#./run_test.sh -i 10 --sot=$SLOT --pattload --dummy --results=/home/ftk/tmp/TEST_LAMB
	echo "The result of the comparison is printed above (press ENTER to continue):"
    else
	echo "No, going on with the next step"
    fi

    echo "Starting the check of the extra lines (takes several seconds). This test checks:"
    echo "- HOLD and Freeze AUX-->AMB (not relevant for LAMB test)"
    echo "- HOLD and Freeze AMB-->AUX (not relevant for LAMB test)"
    echo "- DTest from Bousca to ROAD (relevant for LAMB test)"
    echo -n "(press ENTER to continue): "
    if [ $MODE != automatic ]; then read cont_ok; fi

    python ./reconfigureBoard.py --slot $SLOT --doFactory > $LOGFILENAME
    #running with this lines does not change the output (not passed on the aux freeze)
    aux_reset_main --slot $SLOT --freeze --fpga 3 > $LOGFILENAME
    aux_reset_main --slot $SLOT --freeze --fpga 4 > $LOGFILENAME
    aux_reset_main --slot $SLOT --freeze --fpga 5 > $LOGFILENAME
    aux_reset_main --slot $SLOT --freeze --fpga 6 > $LOGFILENAME
    ambslp_test_extra_lines_main --slot $SLOT 
    echo "Expected output: is RESULT ||    Pass||      Pass||      Pass||        Pass||    Pass||"
    python reconfigureBoard.py -s $SLOT > $LOGFILENAME

    #echo "Opening SSH connection to lxplus"
    #ssh root@pc-tbed-ftk-fw-01 "ls; ls -l"
    #echo "SSH connection terminated"

    echo logfile is $LOGFILENAME

}

main "$@"
