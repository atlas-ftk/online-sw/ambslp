#! /bin/bash

# Argument processing
PROG=`basename $0`

ARGS=`getopt --name "$PROG" --longoptions iters:,slot:,pattload,pattfile:,npatts:,hitfile:,loop:,boardsimfile:,extra:,results:,newsimfile,dummy,dummyhitvalue:,help --options i:,s:,p,e:,l:,d,h -- "$@"`

if [ $? -ne 0 ]; then
  echo "$PROG: usage error (use -h for help)" >&2
  exit 2
fi

eval set -- $ARGS
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

MYFTKRELEASE=FTK-01-00-11
HELP_ARG=no
SLOT=15
ITERS= #3 is a good choice
PATTLOAD=no
PATTFILE=/afs/cern.ch/user/f/fgravili/public/ftk-tdaq-soft/last_release/standalone_tests/setting_files/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root
NPATTS=131072
HITFILE=/afs/cern.ch/user/a/annovi/public/tmp/alberto_test_30_08/AMB_feed_randomtest.ss #/afs/cern.ch/user/f/fgravili/public/ftk-tdaq-soft/last_release/standalone_tests/setting_files/sshits_Twr22.ss
LOOP=0
GENERATE=no
BOARDSIMFILE=/afs/cern.ch/user/a/annovi/public/tmp/alberto_test_30_08/roads_randomtest.out #/afs/cern.ch/user/f/fgravili/public/ftk-tdaq-soft/last_release/standalone_tests/setting_files/outroads_sim.out
DUMMY=no
DUMMYHIT=0x7a0075a0 #Other option 0xfa00f0a0 --- #Original Christos' choice 0xf000f001
RESDIR=/afs/cern.ch/user/f/fgravili/public/ftk-tdaq-soft/last_release/standalone_tests
EXTRATESTDESC=

while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)      HELP_ARG=yes;;
        -i | --iters)     ITERS=$2; echo "iterations set to $ITERS"; shift;;
	-s | --slot)      SLOT=$2; echo "Slot set to $SLOT"; shift;;
	-p | --pattload)  PATTLOAD=yes; echo "Pattern bank will be loaded";;
        --newsimfile)     GENERATE=yes; echo "New simulation file will be generated";;
	--pattfile)       PATTFILE=$2; echo "Pattern file set to $PATTFILE"; shift;;
	--results)        RESDIR=$2; echo "Results directory set to $RESDIR"; shift;;
	--npatts)         NPATTS=$2; echo "Number of patterns set to $NPATTS"; shift;;
	--hitfile)        HITFILE=$2; echo "Hit file set to $HITFILE"; shift;;
        -l | --loop)      LOOP=$2; echo "Enabling feed_hit loop"; shift;;
	-d | --dummy)     DUMMY="yes"; echo "Dummy hits are enabled";;
        --dummyhitvalue)  DUMMYHIT=$2; echo "Dummy hit set to $DUMMYHIT"; shift;;
	--boardsimfile)   BOARDSIMFILE=$2; echo "Board simulation output set to $BOARDSIMFILE"; shift;;
	-e | --extra)     EXTRATESTDESC=$2; echo "Extra description to append to output files set to $EXTRATESTDESC"; shift;;
        --)               shift; break;; #end of options
    esac
    shift
done

printcrate() {
V_NODENAME=$(uname -n)
if [ $V_NODENAME = sbc-tbed-ftk-01 ]; then
    MYCRATE=Crate16
    elif [ $V_NODENAME = sbc-tbed-ftk-02 ]; then
        MYCRATE=Crate17
        elif [ $V_NODENAME = sbc-ftk-rcc-02 ]; then
            MYCRATE=CaenCrate
        else
        echo "Invalid crate number, exiting"
        exit
fi
}

# Setting date and crate number for files
MYDATE=$(date '+%m-%d-%H-%M')
printcrate

resetAUX() {
if [ 1 -eq 1 ]; then 
  for fpga in {1..6}; do
    aux_reconfigure_main --fpga $fpga --slot $1 --epcqaddress 0x1000000
  done
fi

echo -$SHELL-
aux_reset_main --slot $1 --fpga 1
sleep 1
aux_reset_main --slot $1 --fpga 2
sleep 1
aux_reset_main --slot $1 --fpga 3
sleep 1
aux_reset_main --slot $1 --fpga 4
sleep 1
aux_reset_main --slot $1 --fpga 5
sleep 1
aux_reset_main --slot $1 --fpga 6
sleep 1

#reset the aux freeze
aux_write_main --fpga 1 0x40 0x10
aux_write_main --fpga 2 0x40 0x10
aux_write_main --fpga 3 0x40 0x10
aux_write_main --fpga 4 0x40 0x10
aux_write_main --fpga 5 0x40 0x10
aux_write_main --fpga 6 0x40 0x10
}

main() {
    if [ "$HELP_ARG" == "yes" ]; then
        echo "NOTE: THIS TOOL IS VERY SIMILAR TO THE ONE CREATED BY CHRISTOS (run_test.sh)...FEEL FREE TO MODIFY IT"
	echo "Usage:"
	echo "  $PROG [--slot=N] [--iters M] [--pattload] [--pattfile=pattern_Filepath] [--npatts Npatts] [--hitfile=Hit_File] [--loop 0 or 1] [--boardsimfile=board_sim_output_file] [--newsimfile=if_its_recreation_is_needed] [--extra=extra_string_to_result_directory] [--dummy] [--dummyhitvalue=0x????????] [--help]"
	echo "OR"
	echo "  $PROG [-s N] [-i M] [-p] [-l 0/1] [-d] [-e string_to_append_to_test_files] [-h]"
	echo "  The script looks for an FTK release environment and, if it doesn't find one, it loads one."
	echo "  The flag --slot set the slot which the AMBoard is plugged in."
	echo "  The flag --pattload enable the loading of the pattern bank, stored in --pattfile, with --npatts patterns."
        echo "  For a numbers of iterations, set with --iters, it sends a hit file with --hitfile"
        echo "  even with the loop-option (--loop) enabled, then stores (input and output) spybuffers"
	echo "  and compares the output (both methods, 1 and 2) with the board simulation file set with --boardsimfile."
        echo "  Optionally, the board simulation file may be recreated with --newsimfile flag."
        echo "  The flag --dummy enables the dummy hit function and --dummyhitvalue set it. "
        echo "  The flag --results lets you set your output directory, where files will be stored,"
        echo "  and the flag --extra adds an extra string to that folder."
	echo "  Of course, result directory must be somewhere you have write access to). Enjoy it!"
	return 0
    fi

#Checking for a FTK release
    if [ -z "$FTK_INST_PATH" ]; then
	echo "FTK release environment not found, setting up release $MYFTKRELEASE"
	source /afs/cern.ch/user/f/ftk/public/bin/setup_ftk.sh $MYFTKRELEASE
    fi

#Creating directories
    mkdir -p ${RESDIR}/${MYCRATE}-slot$SLOT-tests-${MYDATE}${EXTRATESTDESC}/
    RESDIR2=${RESDIR}/${MYCRATE}-slot$SLOT-tests-${MYDATE}${EXTRATESTDESC}

#Initializing AMBoard
    echo "Running ambslp_procedure..."
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect run stop > ${RESDIR2}/init_procedure_log.txt
    sleep 1

#Checking board status after initial procedure 
    echo "Now checking status..."
    ambslp_status_main --slot $SLOT > ${RESDIR2}/init_status_log.txt
    STATUS=${RESDIR2}/init_status_log.txt
    DISP_ERROR=$(grep disparity $STATUS | grep -v 0x0\$); 
    if [ "$DISP_ERROR" ]; then
        NDISP_ERRORS=$(($NDISP_ERRORS + 1));
        echo "Found $NDISP_ERRORS disparity errors"
        tail -6 $STATUS
        exit
    else
       echo "No disparity error found...Going on with the tests"
    fi
    
#Resetting AUX
    echo "Resetting AUX..."
    resetAUX $SLOT
    sleep 1

    if [ "$PATTLOAD" = "yes" ]; then
	echo "Loading patterns..."
	ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" -T 0 -N $NPATTS --bankpath $PATTFILE config load connect run # > ${RESDIR}/tests-${MYDATE}${EXTRATESTDESC}/test-${fname_prefix}-patternbank-load_log.txt
    fi
    sleep 1

# Starting iteration loop
    for i in `seq 1 $ITERS`; do
    export fname_prefix=iteration$i

#Creating summary file for the number of iteration chosen
    if [ ! -e ${RESDIR2}/summary-${MYDATE}-meth1.txt ]; then
        touch ${RESDIR2}/summary-${MYDATE}-meth1.txt
        summary=${RESDIR2}/summary-${MYDATE}-meth1.txt
    fi

#Configuring board and checking status 
    echo "Iteration $i of $ITERS currently running..."
    echo "Iteration $i of $ITERS: Initializing AMBoard"
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect run stop > ${RESDIR2}/test-${fname_prefix}-procedure_log.txt
    echo "Iteration $i of $ITERS: Now checking status"
    ambslp_status_main --slot $SLOT > ${RESDIR2}/test-${fname_prefix}-status_log.txt
    STATUS=${RESDIR2}/test-${fname_prefix}-status_log.txt
    DISP_ERROR=$(grep disparity $STATUS | grep -v 0x0\$); 
    if [ "$DISP_ERROR" ]; then
        NDISP_ERRORS=$(($NDISP_ERRORS + 1));
        echo "Found $NDISP_ERRORS disparity errors"
        tail -6 $STATUS
        exit
    else
       echo "No disparity error found...Going on with iteration $i"
    fi
    sleep 1
     
#Enabling dummy hit (if requested)
    if [ "$DUMMY" = "yes" ]; then
        echo "Enabling dummy hits in iteration $i"
        vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) $DUMMYHIT    #try also 0xfa00f0a0
        vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x0000216c))) 0x00000000
        echo "Checking value..."
        CHECK=$(vme_peek $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))))
        if [ $CHECK = $DUMMYHIT ]; then
        echo "Checked value is $CHECK - Dummy Hit set correctly"
        else
           echo "Checked value is $CHECK - Dummy Hit set uncorrectly, disabling it and exiting"
           vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) 0x00000000
           vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x0000216c))) 0x00000000
           exit
        fi
        sleep 5
    fi

#Feeding hit file
    echo "Running ambslp_feed_hit in iteration $i"
    ambslp_feed_hit_main --slot $SLOT --hit_file $HITFILE --loopfifovme $LOOP
    sleep 1    

#Generating new simulation *.out file in order to replace the previous one
    if [ "$GENERATE" = "yes" ]; then
        echo "New simulation file will be created in iteration $i"
 	echo "Generating *.ss files for simulation..."
        cd ${RESDIR2}/
        ambslp_spybuffer_dumper_main --slot $SLOT --tag ${fname_prefix}
        sleep 1
        echo "Running simulation..."
        ambslp_boardsim -T 0 -N $NPATTS $PATTFILE ${RESDIR2}/sshits_Twr22_${fname_prefix}_L{0..11}.ss --output-file ${RESDIR2}/outroads_sim_${fname_prefix}_${MYDATE}.out
        BOARDSIMFILE=${RESDIR2}/outroads_sim_${fname_prefix}_${MYDATE}.out
        sleep 1
    fi

#Storing spybuffers for comparison with simulation
    echo "Storing Output SpyBuffers using method 0 and 1 as well as Input ones..."
    cd ${RESDIR2}/
    ambslp_spybuffer_dumper_main --slot $SLOT --tag ${fname_prefix} --method 1 #2> ${RESDIR2}/test-${fname_prefix}-outspy_method1.txt
#    ambslp_spybuffer_dumper_main --slot $SLOT --tag ${fname_prefix} --method 2 2> ${RESDIR2}/test-${fname_prefix}-outspy_method2.txt
    sleep 1

#Running the comparison: it'll be done if the mehod2-output-file has more rows than the number of events in simulation
    echo "Comparing output spybuffer results with simulation"
    count=$(more ${RESDIR2}/meth1_${fname_prefix}.txt | wc -l)
    events=$(echo "obase=10; $(( 0x$(echo "$(echo "$(tail -1 $BOARDSIMFILE)")")&0xff ))" | bc)
    if [ $count -lt $events ]; then
        echo "Unable to make a comparison. Output file doesn't match minimal requirements"
        exit
    else
       AMBCompareTestVecOutput.py ${RESDIR2}/meth1_${fname_prefix}.txt $BOARDSIMFILE > ${RESDIR2}/test-${fname_prefix}-compres.txt
       echo " "
       echo "Printing comparison with METHOD 1"
       tail -49 ${RESDIR2}/test-${fname_prefix}-compres.txt
       if [ $i = 1 ]; then
           echo " " > $summary
       fi
       echo "------- ITERATION $i -------" >> $summary
       echo "Printing comparison with Method 1" >> $summary
       tail -49 ${RESDIR2}/test-${fname_prefix}-compres.txt >> $summary
       echo >> ${RESDIR2}/test-${fname_prefix}-compres.txt
       echo >> ${RESDIR2}/test-${fname_prefix}-compres.txt
       echo >> ${RESDIR2}/test-${fname_prefix}-compres.txt
#       echo >> $summary
#       echo "Now trying comparison with Method 2" >> ${RESDIR2}/test-${fname_prefix}-compres.txt
#       AMBCompareTestVecOutput.py ${RESDIR2}/test-${fname_prefix}-outspy_method2.txt $BOARDSIMFILE >> ${RESDIR2}/test-${fname_prefix}-compres.txt
#       echo " "
#       echo "Printing comparison with METHOD 2"
#       tail -49 ${RESDIR2}/test-${fname_prefix}-compres.txt
#       echo "Printing comparison with Method 2" >> $summary
#       tail -49 ${RESDIR2}/test-${fname_prefix}-compres.txt >> $summary
       echo >> $summary
       echo >> $summary
    fi
    sleep 1

#Disabling dummy hit (if previously enabled)
    if [ "$DUMMY" = "yes" ]; then
        echo "Disabling dummy hits in iteration $i"
        vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) 0x00000000
        vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x0000216c))) 0x00000000
    fi

#Checking board status at the end of one iteration
    echo "Checking board status at the end of iteration $i"
    ambslp_status_main --slot $SLOT > ${RESDIR2}/test-${fname_prefix}-status_finalog.txt
    STATUS=${RESDIR2}/test-${fname_prefix}-status_finalog.txt
    DISP_ERROR=$(grep disparity $STATUS | grep -v 0x0\$); 
    if [ "$DISP_ERROR" ]; then
        NDISP_ERRORS=$(($NDISP_ERRORS + 1));
        echo "Found $NDISP_ERRORS disparity errors"
        tail -6 $STATUS
        exit
    else
        echo "No disparity errors found in iteration $i"    
    fi
    sleep 5
    done 

    echo "Standalone test complete...Results are stored in ${RESDIR2}"    
}

main "$@"
