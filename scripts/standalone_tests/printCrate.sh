#!/bin/bash

V_NODENAME=`uname -n`

main() {
    if [ $V_NODENAME = sbc-tbed-ftk-01 ]; then
	echo "Crate16"
	return 0
    fi
    
    if [ $V_NODENAME = sbc-tbed-ftk-02 ]; then
	echo "Crate17"
	return 0
    fi

    echo "NoCrate"
    return 0
}

main "$@"
