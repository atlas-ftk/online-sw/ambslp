#/h/hmarez/public/python/ambslp_checkoutput.py
#Processes, sorts, and plots output data from AMBoard

import sys
import ROOT as rt

#isolates the bb from the chip bytes
def bb1(s):
	return s[:-7]
def bb2(s):
	return s[1:-6]
def remove_bb(s):
	return s[2:]
#function that opens the selected file and reads in the first entry of bitmask data
def firstbitmask(filename):
	with open(filename) as f:
		mylist = [n for n in f.read().splitlines() if not n.startswith('f78')]
		bb1list = [bb1(s) for s in mylist]			
		bb1int = [int(x, 16) for x in bb1list]
		finalbb1 = []
		for line in bb1int:
			if line==14:
				finalbb1.append(0)
			elif line==13:
				finalbb1.append(1)
			elif line==11:
				finalbb1.append(2)
			elif line==7:
				finalbb1.append(3)
			else:
				finalbb1.append(8)
	return finalbb1

#function that opens the selected file and reads in the second entry of bitmask data
def secondbitmask(filename):
	with open(filename) as f:
		mylist = [n for n in f.read().splitlines() if not n.startswith('f78')]
		bb2list = [bb1(s) for s in mylist]			
		bb2int = [int(x, 16) for x in bb2list]
		finalbb2 = []		
		for line in bb2int:
			if line==14:
				finalbb2.append(4)
			elif line==13:
				finalbb2.append(5)
			elif line==11:
				finalbb2.append(6)
			elif line==7:
				finalbb2.append(7)
			else:
				finalbb2.append(9)
	return finalbb2

#select the file that you would like to view
if (len(sys.argv)!=3):
	sys.exit('\n Please list the outroadsim.out file & spy output file you would like to use.\n Example: python ambslp_checkouput.py outroadsim.out spyoutput.txt\n \n')
else:
	simoutput = sys.argv[1]
	spyoutput = sys.argv[2]

#function that opens the selected file and read in data
def chip_outroads(filename):
	with open(filename) as f:
		mylist = [n for n in f.read().splitlines() if not n.startswith('f78')]
		chiplist = [remove_bb(s) for s in mylist]			
		chipad = [int(x, 16)//131072 for x in chiplist]
	return chipad

simresults = chip_outroads(simoutput)
#print "simresults", simresults
spyresults = chip_outroads(spyoutput)
#print "spyresults", spyresults
c1 = rt.TCanvas("c1","Board Output Status", 800, 600)
simhist = rt.TH1F("simhist","Expected Output", 65, 0, 64)
spyhist = rt.TH1F("spyhist","Board Output", 65, 0, 64)

for line in simresults:
	simhist.Fill(line)
simhist.GetXaxis().SetTitle("Chip Address")
simhist.GetYaxis().SetTitle("Number of Output Roads")
simhist.SetStats(0)
simhist.GetXaxis().SetNdivisions(13)
simhist.SetTitle("Expected Output vs. Board Output")
simhist.SetBarWidth(0.45)
simhist.SetBarOffset(0.1)
simhist.SetFillColor(8)

for line in spyresults:
	spyhist.Fill(line)
spyhist.SetStats(0)
spyhist.SetBarWidth(0.45)
spyhist.SetBarOffset(0.55)
spyhist.SetFillColor(4)
c1.cd(1)
simhist.Draw("B")
spyhist.Draw("B,same")

legend = rt.TLegend(0.66,0.70,0.90,0.90)
legend.AddEntry(simhist,"Simulated Expected Output","f")
legend.AddEntry(spyhist,"AMBoard Output","f")
legend.Draw("same")
c1.Update()
c1.SaveAs("RoadBoardOutput.png")
#c1.SaveAs("RoadBoardOutput.pdf","pdf")

simbb1 = firstbitmask(simoutput)
simbb2 = secondbitmask(simoutput)

spybb1 = firstbitmask(spyoutput)
spybb2 = secondbitmask(spyoutput)

bbc = rt.TCanvas("bbc","Bitmask Board Output Status", 800, 600)
simbbhist = rt.TH1F("simbbhist","Expected Output Panels 0-7", 10, 0, 10)
spybbhist = rt.TH1F("spybbhist","Board Output Panels 0-7", 10, 0, 10)

for line in simbb1:
	simbbhist.Fill(line)
for line in simbb2:
	simbbhist.Fill(line)
simbbhist.GetXaxis().SetTitle("Panels Hit")
simbbhist.GetXaxis().SetTitleOffset(1.3)
simbbhist.GetXaxis().SetBinLabel(1, "No Bus Layer 0")
simbbhist.GetXaxis().SetBinLabel(2, "No Bus Layer 1")
simbbhist.GetXaxis().SetBinLabel(3, "No Bus Layer 2")
simbbhist.GetXaxis().SetBinLabel(4, "No Bus Layer 3")
simbbhist.GetXaxis().SetBinLabel(5, "No Bus Layer 4")
simbbhist.GetXaxis().SetBinLabel(6, "No Bus Layer 5")
simbbhist.GetXaxis().SetBinLabel(7, "No Bus Layer 6")
simbbhist.GetXaxis().SetBinLabel(8, "No Bus Layer 7")
simbbhist.GetXaxis().SetBinLabel(9, "All Layers 0-3")
simbbhist.GetXaxis().SetBinLabel(10, "All Layers 4-7")
simbbhist.GetYaxis().SetTitle("Number of Output Roads")
simbbhist.GetYaxis().SetLabelSize(0.02)
simbbhist.SetStats(0)
simbbhist.SetTitle("Expected Output vs. Board Output for Panels 0-7")
simbbhist.SetBarWidth(0.25)
simbbhist.SetFillColor(8)

for line in spybb1:
	spybbhist.Fill(line)
for line in spybb2:
	spybbhist.Fill(line)

spybbhist.SetStats(0)
spybbhist.SetBarWidth(0.25)
spybbhist.SetBarOffset(0.25)
spybbhist.SetFillColor(4)
simbbhist.Draw("B")
spybbhist.Draw("B,same")

legend1 = rt.TLegend(0.10,0.70,0.35,0.90)
legend1.AddEntry(simbbhist,"Sim Expected Output","f")
legend1.AddEntry(spybbhist,"AMBoard Output","f")
legend1.SetBorderSize(1)
legend1.Draw("same")

bbc.Update()
bbc.SaveAs("HitBoardOutput.png")
#bbc.SaveAs("HitBoardOutput.pdf","pdf")













