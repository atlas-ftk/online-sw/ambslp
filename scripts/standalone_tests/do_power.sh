#!/bin/bash

check_core () {
    if [ "$2" = "12" ]; then
	echo "Checking 12V rail..."
	vins="`echo "$1" | grep -i \"Vin\" | sed 's/[Vin =]*//g'`"
	while read -r vin; do
	    if (( $(echo $vin '>' 12.2 | bc) + $(echo $vin '<' 11.8 | bc))); then
		echo $vin is out of the expected voltage range for 12V
		return 255
	    else
		echo $vin is within the expected voltage range for 12V, continuing...
	    fi
	done <<< "$vins"
    else
	echo "Checking LAMB rails at $2V..."
	vouts="`echo "$1" | grep -i \"Vout =\" | sed 's/[Vout =]*//g'`"
	llim=`echo $2-0.05 | bc`
	ulim=`echo $2+0.05 | bc`
	while read -r vout; do
	    if (( $(echo $vout '>' $ulim | bc) + $(echo $vout '<' $llim | bc))); then
		echo $vout is out of the expected voltage range for ${2}V
		return 255
	    else
		echo $vout is within the expected voltage range for ${2}V, continuing...
	    fi
	done <<< "$vouts"
    fi
}

check_barracuda() {
    vin="`echo "$1" | grep -i \"Vin\" | sed 's/[Vin =]*//g'`"
    echo "Checking 50V rail..."
    if (( $(echo $vin '>' 52 | bc) + $(echo $vin '<' 48 | bc))); then
	echo $vin is out of the expected voltage range for 50V
	return 255
    else
	echo $vin is within the expected voltage range for 50V, continuing...
    fi

    vin="`echo "$1" | grep -i \"Vout\" | sed 's/[Vout =]*//g'`"
    echo "Checking 12V rail..."
    if (( $(echo $vin '>' 12.2 | bc) + $(echo $vin '<' 11.8 | bc))); then
	echo $vin is out of the expected voltage range for 12V
	return 255
    else
	echo $vin is within the expected voltage range for 12V, continuing...
    fi
}

check_serdes() {
    vin="`echo "$1" | grep -i \"Vout\_0\" | sed 's/Vout\_0//' | sed 's/[Vout \_ =]*//g'`"
    echo "Checking 1.8V rail #1..."
    if (( $(echo $vin '>' 1.85 | bc) + $(echo $vin '<' 1.75 | bc))); then
	echo $vin is out of the expected voltage range for 1.8V
	return 255
    else
	echo $vin is within the expected voltage range for 1.8V, continuing...
    fi
    
    vin="`echo "$1" | grep -i \"Vout\_1\" | sed 's/Vout\_1//' | sed 's/[Vout \_ =]*//g'`"
    echo "Checking 1.8V rail #2..."
    if (( $(echo $vin '>' 1.85 | bc) + $(echo $vin '<' 1.75 | bc))); then
	echo $vin is out of the expected voltage range for 1.8V
	return 255
    else
	echo $vin is within the expected voltage range for 1.8V, continuing...
    fi
}


check_all_voltages () {
    barracuda_output=`./PMB_barracuda.sh`
    echo "Checking barracuda voltages..."
    check_barracuda "$barracuda_output"
    if [ $? = 255 ]; then
	echo "Some barracuda voltage is wrong, exiting."
	return $ERR_VOLTAGE
    else
	echo "Barracuda voltage tests OK"
	echo
    fi
    
    serdes_output=`./PMB_SerDes.sh`
    echo "Checking Serdes voltages..."
    check_serdes "$serdes_output"
    if [ $? = 255 ]; then
	echo "Some serdes voltage is wrong, exiting."
	return $ERR_VOLTAGE
    else
	echo "Serdes voltage tests OK"
	echo
    fi
    
    core_output=`./PMB_Core.sh`
    check_core "$core_output" 12
    if [ $? = 255 ]; then
	echo "12V core voltage is wrong, exiting."
	return $ERR_VOLTAGE
    else
	echo "Voltage tests at 12V OK"
	echo
    fi
    
    check_core "$core_output" 1
    if [ $? = 255 ]; then
	echo "LAMB core voltage is not close to 1V, let's check if it's already at 1.15V."
	echo
    else
	echo "Voltage tests at 1V OK"
	echo "Setting LAMB voltage to 1.15V..."
	./Set_Th_OverV.sh
	./Set_Vout_1_15V.sh
	sleep 0.5
    fi
    
    core_output=`./PMB_Core.sh`
    check_core "$core_output" 1.15
    if [ $? = 255 ]; then
	echo "LAMB voltage is outside normal range, exiting."
	return $ERR_VOLTAGE
    else
	echo "Voltage tests at 1.15V OK"
	echo
    fi
}

cd $SCRIPTDIR/../PMBUS

   check_all_voltages 
