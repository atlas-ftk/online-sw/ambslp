#!/hmarez/public/python/ambslp_boardstats

import sys

def intlist(list):
	newlist=[]
	for line in list:
		newlist.append(int(line))
	return newlist

def median(list):
    sortedlist = sorted(list)
    listlength = len(list)
    index = (listlength - 1) // 2

    if (listlength % 2):
        return sortedlist[index]
    else:
        return (sortedlist[index] + sortedlist[index + 1])/2

#select the file that you would like to view
if(len(sys.argv)!=2):
	sys.exit('Please list the file you would like to use in the command-line argument.\n Example: python ambslp_boardstats.py outsim.out')
else:
	filename = sys.argv[1]
#open the selected file and read in data
with open(filename) as f:
	full=[]
	part=[]
	lost=[]
	extra=[]
	for line in f.readlines():
		line = line.strip()
		columns = line.split()
		full.append(columns[0])
		part.append(columns[1])
		lost.append(columns[2])
		extra.append(columns[3])

print("Number of Trials: ",len(full),"\n")		
fullmatch = intlist(full)
partialmatch = intlist(part)
lostroads = intlist(lost)
totalroads = [sum(x) for x in zip(fullmatch, partialmatch, lostroads)]
#print("Total Number of Roads: ",totalroads)

fullpercent = [((x)/(y))*100 for x,y in zip(fullmatch, totalroads)]
avgfull = sum(fullpercent)/len(fullpercent)
maxfull = max(fullpercent)
medfull = median(fullpercent)
minfull = min(fullpercent)
print("Fully Matched Roads:\n""Average % Fully Matched: ",avgfull,"\nMax % Fully Matched: ",maxfull,"\nMedian % Fully Matched: ",medfull,"\nMin % Fully Matched: ",minfull,"\nRange: ",(max(fullmatch)-min(fullmatch))," roads")

partpercent = [((x)/(y))*100 for x,y in zip(partialmatch, totalroads)]
avgpart = sum(partpercent)/len(partpercent)
maxpart = max(partpercent)
medpart = median(partpercent)
minpart = min(partpercent)
print("\nPartially Matched Roads:\n""Average % Partially Matched: ",avgpart,"\nMax % Partially Matched: ",maxpart,"\nMedian % Fully Matched: ",medpart,"\nMin % Partially Matched: ",minpart,"\nRange: ",(max(partialmatch)-min(partialmatch))," roads")

lostpercent = [((x)/(y))*100 for x,y in zip(lostroads, totalroads)]
avglost = sum(lostroads)/len(lostroads)
maxlost = max(lostpercent)
medlost = median(lostpercent)
minlost = min(lostpercent)
print("\nLost Roads:\n""Average % Lost Roads: ",avglost,"\nMax % Lost Roads: ",maxlost,"\nMedian % Lost Roads: ",medlost,"\nMin % Lost Roads: ",minpart,"\nRange: ",(max(lostroads)-min(lostroads))," roads")

extraroads = intlist(extra)
extrapercent = [((x)/(y))*100 for x,y in zip(extraroads, totalroads)]
avgextra = sum(extraroads)/len(extraroads)
maxextra = max(extrapercent)
minextra = min(extrapercent)
medextra = median(extrapercent)
print("\nExtra Roads:\n""Average % Extra Roads: ",avgextra,"\nMax % Extra Roads: ",maxextra,"\nMedian % Extra Roads: ",medextra,"\nMin % Extra Roads: ",minextra,"\nRange: ",(max(extraroads)-min(extraroads))," roads")