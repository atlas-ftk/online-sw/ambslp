#!/usr/bin/env python

import optparse
import sys

parser = optparse.OptionParser()
parser.add_option("-L", "--MaskLAMB", dest="MaskedLAMBs", action="append",
                  help="ID of the LAMBs to be ignored in the difference", type=int)
parser.add_option("-G", "--GlobalMatch", help="Just check if an expected road is found, ignoring the EE ",
                  dest="Global", action='store_true', default=False)
parser.add_option("-q", dest="quiet", help="Quite mode, very minimal messaging level",
                  action="store_true", default=False)
parser.add_option("-A", "--AppendChipStat", help="If set update the per chip statistic cointained in a specific file",
                  dest="ChipStat", default="")
parser.add_option("-V", "--VetoRoadsAbove", help="Ignore patterns with address >= VetoRoadsAbove",
                  dest="VetoRoadsAbove", type=int, default="1000000000")
parser.add_option("-C", "--SelectChip", help="Ignore all patterns from other chips",
                  dest="SelectedChip", type=int, default="-1")
parser.add_option("-0", "--IgnorePattern0", help="Ignore errors from pattern0",
                  dest="IgnorePattern0", action="store_true", default=False)
opts, args = parser.parse_args()

import ROOT
ROOT.gROOT.SetBatch()
# this variable reports the general exit status: 0 ok, !=0 errors, TBD
status = 0

class AMBTestLogger :
  """The class handles the logger file used to register the chip working status.
  The logger is assumed to use a file shard among different sessions.""" 
  def __init__(self, filename, isValid=True):
    self.valid = isValid
    if not isValid :
      return None
    # open the ROOT file, update mode
    self.file = ROOT.TFile(filename,"update")
    if not self.file.Get("h_chip_lostroads") :
      # in this case the file is newly created, all the relevant histograms are created
      self.h_chip_lostroads = ROOT.TH1F("h_chip_lostroads","Lost roads",64,0,64)
      self.h_chip_extraroads = ROOT.TH1F("h_chip_extraroads","Extra roads",64,0,64)
      self.h_chip_partroads = ROOT.TH1F("h_chip_partroads","Partially matched roads",64,0,64)
      self.h_chip_goodroads = ROOT.TH1F("h_chip_goodroads","Exact roads",64,0,64)
      self.h_chip_goodroadsFF = ROOT.TH1F("h_chip_goodroadsFF","Exact roads",64,0,64)
      self.h_eetags_matched = ROOT.TH1F("h_eetags_matched","EE matched tags",16,0,16)
      self.h_eetags_extra = ROOT.TH1F("h_eetags_extra","EE extra tags",16,0,16)
      self.h_eetags_lost = ROOT.TH1F("h_eetags_lost","EE lost tags",16,0,16)
    else :
      # in this case the file exist and need to be updated
      self.h_chip_lostroads = self.file.Get("h_chip_lostroads") 
      self.h_chip_extraroads = self.file.Get("h_chip_extraroads")
      self.h_chip_partroads = self.file.Get("h_chip_partroads")
      self.h_chip_goodroads = self.file.Get("h_chip_goodroads")
      self.h_chip_goodroadsFF = self.file.Get("h_chip_goodroadsFF")
      self.h_eetags_matched = self.file.Get("h_eetags_matched")
      self.h_eetags_extra = self.file.Get("h_eetags_extra")
      self.h_eetags_lost = self.file.Get("h_eetags_lost")
    return None
  
  def AddLostRoad(self, chipnum):
    """Add a lost road"""
    if self.valid :
      self.h_chip_lostroads.Fill(chipnum)
    return None
  
  def AddExtraRoad(self, chipnum):
    """Add an extra road"""
    if self.valid :
      self.h_chip_extraroads.Fill(chipnum)
    return None
  
  def AddPartialRoad(self, chipnum):
    """Add a partially matched road"""
    if self.valid :
      self.h_chip_partroads.Fill(chipnum)
    return None
  
  def AddGoodRoad(self, chipnum):
    """Add a peferctly matched road"""
    if self.valid :
      self.h_chip_goodroads.Fill(chipnum)
    return None
  
  def AddGoodRoadFull(self, chipnum):
    """Add a peferctly matched road"""
    if self.valid :
      self.h_chip_goodroadsFF.Fill(chipnum)
    return None
  
  def AddEECountMatched(self, count):
    """Add the number of EE tags found for each specific matched EE"""
    if self.valid :
      self.h_eetags_matched.Fill(count)
    return None
  
  def AddEECountExtra(self, count):
    """Add the number of EE tags found for a each specific extra EE"""
    if self.valid :
      self.h_eetags_extra.Fill(count)
    return None
  
  def AddEECountLost(self, count):
    """Add the number of EE tags found for a each specific lost EE"""
    if self.valid :
      self.h_eetags_lost.Fill(count)
    return None
  
  def Close(self):
    """Update and close the file"""
    if self.valid :
      self.file.Write("",ROOT.TObject.kOverwrite)
      self.file.Close()
    return None
  
if not opts.quiet :
  print "Ignoring LAMBs:", opts.MaskedLAMBs

# check if a log file to collect static on chips has to be used
testLogger = AMBTestLogger(opts.ChipStat, opts.ChipStat!="")
    
def AddrToChip(pattaddr) :
  """This function convert a pattern address in the chip number"""
  # remove pattern ID
  chipID = (pattaddr>>17) & 0x3
  chainID = (pattaddr>>19) & 0x3
  lambID = (pattaddr>>21) & 0x3
  chipNum =  chipID+4*chainID+16*lambID
  #print "***", hex(pattaddr), chipID, chainID, lambID, chipNum
  return chipNum

def AddrInfo(pattaddr) :
  """This function expand the pattern address to extract the global AM
  chip ID, the chip ID within the output chain, the chain ID within 
  the LAMB, and the LAMB ID."""
  # remove pattern ID
  chipID = (pattaddr>>17) & 0x3
  chainID = (pattaddr>>19) & 0x3
  lambID = (pattaddr>>21) & 0x3
  chipNum =  chipID+4*chainID+16*lambID
  #print "***", hex(pattaddr), chipID, chainID, lambID, chipNum
  return chipNum, chipID, chainID, lambID


# open the input files
leftfile  = open(args[0],"r") # represents the AMB streams
rightfile = open(args[1],"r") # represents the simulation reference

def CollectSpyBufferData(myfile) :
  """ This function fully read a spy-buffer or road reference file. The data
  are globally collected and also organized by event, using the EE tag as
  dictionary key. The EE tag themselve are counted to spot eventual problems"""
  # global lists of roads, bitmasks and end event tags
  roads = []
  bitmasks = []
  tags = []
  # dictionaris with roads and bitmasks organized by EE tag, EE counter
  roadsByEE = {}
  bitmasksByEE = {}
  counterEE = {}
  
  # variables used to collect the event-by-event values, assumed to preceed the EE tag
  curroads = []
  curbitmasks = []
  for data in myfile:
    word = int(data.rstrip(),16) # assume the words is an hexadecimal value
    if (word & (0xf78<<20)) == (0xf78<<20) : # marks an end event
      tags.append(word) # append the tag the list of EE tags
      if word in counterEE :
        if not opts.quiet :
          print "EE", hex(word), "already found, append data"
        counterEE[word] += 1
        roadsByEE[word] += curroads
        bitmasksByEE[word] += curbitmasks
        curroads = []
        curbitmasks = []
      else :
        counterEE[word] = 1
        roadsByEE[word] = curroads
        bitmasksByEE[word] = curbitmasks
        curroads = []
        curbitmasks = []
      
    else :
      # Under given conditions a road can be ignored in the comparison
      toIgnore = False
      if opts.MaskedLAMBs :
        for l in opts.MaskedLAMBs :
          # a road can be ignored because belonging to a vetoed LAMB
          if (word>>21) & 3 == l :
            toIgnore = True
            break
      
      # road word, extract the bitmask and accumulate it
      road = word & ~((0xff)<<24) # blind the bitmask
      if ( road & 0x1ffff ) >= opts.VetoRoadsAbove :
        toIgnore = True

      chipNum = (road>>17) & 0x3f
      if ( chipNum != opts.SelectedChip and opts.SelectedChip>=0 ) :
        toIgnore = True

      if ( opts.IgnorePattern0 and (( road & 0x1ffff ) == 0) ):
        toIgnore = True

      if toIgnore :
        continue

      bitmask = word>>24 & 0xff
      roads.append(road) 
      bitmasks.append(bitmask)
      curroads.append(road)
      curbitmasks.append(bitmask)
  return roads, bitmasks, tags, roadsByEE, bitmasksByEE, counterEE

# global list of roads, bitmasks and EE tags
leftroads, leftbitmasks, lefttags, leftroadsByEE, leftbitmasksByEE, leftcounterEE = CollectSpyBufferData(leftfile)
#print leftroadsByEE, leftbitmasksByEE

# this is usually assumed to be the reference output
rightroads, rightbitmasks, righttags, rightroadsByEE, rightbitmasksByEE, rightcounterEE = CollectSpyBufferData(rightfile)

# It is possible to compare the output EE by EE or in general for all the roads
DoGlobalMatch = opts.Global # when true the *roads and *bitmasks data are used, otherwise the *byEE elements

# the match result, for any situation, is accumulated into the outdata array, here
# each line is the result of a test to be printed out
outdata = []

if DoGlobalMatch :
  print "Collect global match information"
  found = []
  for iroad in xrange(len(leftroads)) :
    if leftroads[iroad] in rightroads : # match
      idr = rightroads.index(leftroads[iroad])
      if leftbitmasks[iroad] == rightbitmasks[idr] :
        outdata.append((' ', leftroads[iroad] | (leftbitmasks[iroad]<<24), rightroads[idr] | (rightbitmasks[idr]<<24)))
      else :
        outdata.append(('*', leftroads[iroad] | (leftbitmasks[iroad]<<24), rightroads[idr] | (rightbitmasks[idr]<<24)))
      found.append(idr)
    else :
      outdata.append(('+', leftroads[iroad] | (leftbitmasks[iroad]<<24), None))
  
  for iroad in xrange(len(rightroads)) :
    if not iroad in found :
      outdata.append(('-',None, rightroads[iroad] | (rightbitmasks[iroad]<<24)))
else :
  # do the EE by EE check
  if not opts.quiet :
    print "Collect event-by-event information"
  
  foundTags = []
  for tag in leftcounterEE.keys() : # loop over EE from
    if not tag in rightcounterEE :
      testLogger.AddEECountExtra(leftcounterEE[tag])
      # add all the roads as extar not matched
      for road, bitmask in zip(leftroadsByEE[tag], leftbitmasksByEE[tag]) :
        roadInfo = AddrInfo(road)
        outdata.append(("+", road | bitmask<<24))
        testLogger.AddExtraRoad(roadInfo[0])
      outdata.append((None,tag,None))      
      continue 
    
    # check that this EE tag has been found
    foundTags.append(tag)
    # update the EE checker histogram
    testLogger.AddEECountMatched(leftcounterEE[tag])
    
    # retrieve all object related to this tag
    roadsA = leftroadsByEE[tag]
    bitmasksA = leftbitmasksByEE[tag]
    roadsB = rightroadsByEE[tag]
    bitmasksB = rightbitmasksByEE[tag]
    #print roadsA, bitmasksA, roadsB, bitmasksB
    
    found = []
    for iroad in xrange(len(roadsA)) :
      mroad = roadsA[iroad]
      roadInfo = AddrInfo(mroad)
      if mroad in roadsB: # match
        # get the idnex of the road
        idr = roadsB.index(mroad)        
        if idr in found :
          outdata.append(('+', mroad | (bitmasksA[iroad]<<24), None))
          testLogger.AddExtraRoad(roadInfo[0])
        elif bitmasksA[iroad] == bitmasksB[idr] :
          outdata.append((' ', mroad | (bitmasksA[iroad]<<24), roadsB[idr] | (bitmasksB[idr]<<24)))
          testLogger.AddGoodRoad(roadInfo[0])
          if bitmasksA[iroad] == 0xff :
            testLogger.AddGoodRoadFull(roadInfo[0])
        else :
          outdata.append(('*', mroad | (bitmasksA[iroad]<<24), roadsB[idr] | (bitmasksB[idr]<<24)))
          testLogger.AddPartialRoad(roadInfo[0])
        found.append(idr)
      else :
        outdata.append(('+', mroad | (bitmasksA[iroad]<<24), None))
        testLogger.AddExtraRoad(roadInfo[0])
    
    for iroad in xrange(len(roadsB)) :
      if not iroad in found :
        roadInfo = AddrInfo(roadsB[iroad])
        outdata.append(('-',None, roadsB[iroad] | (bitmasksB[iroad]<<24)))
        testLogger.AddLostRoad(roadInfo[0])
    outdata.append((None,tag,tag))
    
  for tag in rightcounterEE.keys() :
    if not tag in foundTags :
      # add all the roads as not matched
      for road, bitmask in zip(rightroadsByEE[tag], rightbitmasksByEE[tag]) :
        roadInfo = AddrInfo(road)
        outdata.append(("-", None, road | bitmask<<24))
        testLogger.AddLostRoad(roadInfo[0])
      outdata.append((None,None,tag))
      testLogger.AddEECountLost(rightcounterEE[tag])

# prepare the per chip statistic
histo_addroads = [0 for i in xrange(64)]
histo_matched = [0 for i in xrange(64)]
histo_unmatched = [0 for i in xrange(64)]
histo_nofound = [0 for i in xrange(64)]
histo_XORAMblock = [0 for i in xrange(32)]
histo_extraLayer = [0 for i in xrange(8)]
histo_missingLayer = [0 for i in xrange(8)]
histo_events = [0]

for data in outdata : # loop over the test data ([:-2058] to ignore first event)
  try :
    if not data[0] : # end event
      histo_events.append(0)
      if not data[1] :
        print "1          %08x" % data[2]
        status |= 1<<3 # mark missing event
      elif not data[2] :
        print "1 %08x         " % data[1]
        status |= 1<<4 # mark extra event
      else :
        print "1 %08x %08x" % (data[1], data[2])
    elif data[0]=="-" : # missing road
      roadInfo = AddrInfo(data[2])
      vals = (data[2],)+roadInfo[1:]
      print "-          %08x (%d, %d, %d)" % vals
      status |= 1 # mark missing road
      histo_nofound[AddrToChip(data[2])] += 1
      #histo_XORAMblock[ ((data[2]&0x1FFFF)>>6) ] += 1
      histo_events[-1] += 1
    elif data[0]=="+" : # additional road
      roadInfo = AddrInfo(data[1])        
      vals = (data[1],)+roadInfo[1:]
      print "+ %08x          (%d, %d, %d)" % vals
      status |= 1<<1
      histo_addroads[AddrToChip(data[1])] += 1
      histo_events[-1] += 1
    else  : # common road, will check the bitmask
      roadInfo = AddrInfo(data[2])
      if not opts.quiet :        
        vals = data+roadInfo[1:]
        print "%s %08x %08x (%d, %d, %d)" % vals 
      if data[0] == '*' :
        status |= 1<<2 # mark a partial match
        histo_unmatched[AddrToChip(data[1])] += 1
        histo_events[-1] += 1
        # save statistics of errors by layer
        leftbitmap = (vals[1]>>24) & 0xff
        rightbitmap = (vals[2]>>24) & 0xff
        extraLayers = leftbitmap & (~rightbitmap)
        missingLayers = rightbitmap & (~leftbitmap)
        for i in xrange(8):
          if (missingLayers>>i) & 0x1:
            histo_missingLayer[i] += 1
          if (extraLayers>>i) & 0x1:
            histo_extraLayer[i] += 1

      else :
        histo_matched[AddrToChip(data[1])] += 1
  except :
    print "Invalid format:", data
histo_events.pop()
# end test over output data

if not opts.quiet :
  print
  print "Event tag verification (empty line is good)"
  for data in righttags :
    if not data in lefttags :
      print "Not found:", hex(data)
  
  print
  print "Roads match report"
  print "ChipID #NoFound Matched Partial Additional"

  chipsWithErrors = []
  
  for i in xrange(64) :
    print "%2d %6d %6d %6d %6d" % (i, histo_nofound[i], histo_matched[i], histo_unmatched[i], histo_addroads[i]),
    if i%2==1 :
      print
    else:
      print "  --  ",
    if histo_nofound[i]!=0 or histo_unmatched[i] !=0 or histo_addroads[i]!=0 :
      chipsWithErrors+=[i];
  
    if i%16 == 15 :
        print

  #print
  #print "XORAM block# #NoFound"  
  #for i in xrange(32) :
  #  print "%2d %6d" % (i, histo_XORAMblock[i])
  
  print    
  print "# of missing Layers (0-->7)", histo_missingLayer
  print "# of extra   Layers (0-->7)", histo_extraLayer
  print
  print "# of errors by event", histo_events
  print
  print "AM chips with errors", chipsWithErrors
  print
  
print "Errors summary"
print "Fully matched:", sum(histo_matched)
print "Partially matched:", sum(histo_unmatched), "- %.4f%%" % (100.*sum(histo_unmatched)/sum(histo_matched)) 
print "Lost roads:", sum(histo_nofound), "- %6.4f%%" % (100.*sum(histo_nofound)/sum(histo_matched))
print "Extra roads:", sum(histo_addroads), "- %6.4f%%" % (100.*sum(histo_addroads)/sum(histo_matched))

# in case the chip status logger object exists the status is saved
testLogger.Close()

sys.exit(status)
