#!/usr/bin/env tdaq_python
 
# Author: G. Volpi, control of the DCDC converters voltage and working conditions

import sys
import subprocess
import math
import argparse
import warnings
import logging

from time import sleep

verb = 0
isAMBv4 = False
VOUT_SCALE=1.6666666666666667 # value for boards with 1.000 nominal tesion
refValue = 1.15

PMBUSWrite = 0x00008000
PMBUSBarracuda = 0x00002900
PMBUSFPGA = 0x00000a00         
PMBUSLambs = [0x00001000, 0x00000e00, 0x00001100, 0x00000f00]
#VME registers that allows to access the PMBUS
#COMMAND register
PMBUSCmd = 0x00002160
#DATA read register
PMBUSData = 0x00002164
AMB_HIT_DUMMY_HIT_WORD = 0x00002168


def VMEPeek(slot, regaddr, delay=0):
  """Do a vme_peek for a specific address, a delay can be specified, default .1"""
  vmeaddr = (slot<<27) | regaddr
  args = ["vme_peek",hex(vmeaddr)]
  if verb :
    print slot, regaddr, args
  res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  
  sleep(delay)
  return int(res,16)

def VMEPoke(slot, regaddr, value, delay=.1):
  """Do a vme_poke for a specific address and value, a delay can be specified, default .1"""
  vmeaddr = (slot<<27) | regaddr
  args = ["vme_poke",hex(vmeaddr), hex(value)]
  if verb :
   print slot, regaddr, value, args
  res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
  
  sleep(delay)
  return None

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-H", "--ReadHitDNA", dest="Hit", action="store_true",
                    help="Read Hit DNA registers", default=False)
  parser.add_argument("-L", "--ReadLAMBsDNA", dest="LAMB", action="store_true",
                    help="Read LAMBs registers", default=False)
  parser.add_argument("slot",help="Slot number", type=int)
  opts = parser.parse_args()

  slot = opts.slot

  if opts.Hit : 
    print "HIT DNA register: %x%x" % ( VMEPeek(slot, 0x000021F8), VMEPeek(slot, 0x000021FC) )

  if opts.LAMB :   
    VMEPoke(slot, 0x00000038, 0x44444444)
    DNA0 = VMEPeek(slot, 0x0000003c)
    VMEPoke(slot, 0x00000038, 0x43434343)
    DNA1 = VMEPeek(slot, 0x0000003c)
    VMEPoke(slot, 0x00000038, 0x42424242)
    DNA2 = VMEPeek(slot, 0x0000003c)
    VMEPoke(slot, 0x00000038, 0x41414141)
    DNA3 = VMEPeek(slot, 0x0000003c)
    VMEPoke(slot, 0x00000038, 0x40404040)
    DNA4 = VMEPeek(slot, 0x0000003c)
    VMEPoke(slot, 0x00000038, 0x3f3f3f3f)
    DNA5 = VMEPeek(slot, 0x0000003c)
    VMEPoke(slot, 0x00000038, 0x3e3e3e3e)
    DNA6 = VMEPeek(slot, 0x0000003c)
    # print "%02x" % DNA0
    # print "%02x" % DNA1
    # print "%02x" % DNA2
    # print "%02x" % DNA3
    # print "%02x" % DNA4
    # print "%02x" % DNA5
    # print "%02x" % DNA6

    mask = 0xff
    numb = 1
    print "LAMB0 DNA: %x%x%x%x%x%x%x" % ( (DNA0&mask) // numb, (DNA1&mask) // numb, (DNA2&mask) // numb, (DNA3&mask) // numb, (DNA4&mask) // numb, (DNA5&mask) // numb, (DNA6&mask) // numb )
    mask = 0xff00
    numb = 16*16
    print "LAMB1 DNA: %x%x%x%x%x%x%x" % ( (DNA0&mask) // numb, (DNA1&mask) // numb, (DNA2&mask) // numb, (DNA3&mask) // numb, (DNA4&mask) // numb, (DNA5&mask) // numb, (DNA6&mask) // numb )
    mask = 0xff0000
    numb = 16*16*16*16
    print "LAMB2 DNA: %x%x%x%x%x%x%x" % ( (DNA0&mask) // numb, (DNA1&mask) // numb, (DNA2&mask) // numb, (DNA3&mask) // numb, (DNA4&mask) // numb, (DNA5&mask) // numb, (DNA6&mask) // numb )
    mask = 0xff000000
    numb = 16*16*16*16*16*16
    print "LAMB3 DNA: %x%x%x%x%x%x%x" % ( (DNA0&mask) // numb, (DNA1&mask) // numb, (DNA2&mask) // numb, (DNA3&mask) // numb, (DNA4&mask) // numb, (DNA5&mask) // numb, (DNA6&mask) // numb )

if __name__ == "__main__" :
  main()
