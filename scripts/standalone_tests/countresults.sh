#!/bin/zsh -f

lost_tosum=0`grep "Lost roads" $@ | awk -F'[\t| ]*' '{printf "+%d",$3;}'`
part_tosum=0`grep "Partially matched" $@ | awk -F'[\t| ]*' '{printf "+%d",$3;}'`
extra_tosum=0`grep "Extra roads" $@ | awk -F'[\t| ]*' '{printf "+%d",$3;}'`
matched_tosum=0`grep "Fully matched" $@ | awk -F'[\t| ]*' '{printf "+%d",$3;}'`

lost_sum=`bc <<< $lost_tosum`
part_sum=`bc <<< $part_tosum`
extra_sum=`bc <<< $extra_tosum`
matched_sum=`bc <<< $matched_tosum`

echo Total Matched $matched_sum
echo Total Lost $lost_sum
echo Total Partially Matched $part_sum
echo Total Extra $extra_sum
echo Lost / Found percentage: `dc <<< "5 k $lost_sum 100 * $matched_sum / p"`\%
