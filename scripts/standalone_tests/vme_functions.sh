#!/bin/bash 

#Read word in register, inputs are SLOT number and register numeber
slot_poke(){
   vme_poke $(printf "%x" $(( ($1<<27)|$2))) $3 > /dev/null 2>&1
} 

#Write word in register, inputs are SLOT number, register number and word to write
slot_peek(){
   vme_peek $(printf "%x" $(( ($1<<27)|$2)))  
}  

