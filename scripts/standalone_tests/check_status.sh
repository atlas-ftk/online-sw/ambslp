check_status() {
    check_status_is_there "$1"
    if [ $? = 255 ]; then
	echo "INFO: AMB status not read OK, exiting."
	return $ERR_STATUS_NOT_READ
    fi

    check_firmware_versions "$1"
    if [ $? = 255 ]; then
	echo "WARNING: Firmware version mismatch! (at /ambslp/scripts/standalone_tests/check_status.sh)"
	echo $ERR_FIRMWARE_VERSION
	#return $ERR_FIRMWARE_VERSION
    else
	echo "INFO: all firmware checks OK."
	echo
    fi

    check_temperature "$1"
    if [ $? = 255 ]; then
	echo "WARNING: Temperature warnings!!! Be careful! (at /ambslp/scripts/standalone_tests/check_status.sh)"
	echo
    else
	echo "INFO: Temperature checks OK."
	echo
    fi

    check_gtp "$1"
    if [ $? = 255 ]; then
	echo "ERROR: GTP misconfigured, exiting (at /ambslp/scripts/standalone_tests/check_status.sh)"
	echo $ERR_MISALIGNED
	#return $ERR_MISALIGNED
    else
	echo "INFO: GTP checks OK."
	echo
    fi

    check_fifos "$1"
    if [ $? = 255 ]; then
	echo "WARNING: Initial FIFO flags wrong (at /ambslp/scripts/standalone_tests/check_status.sh)"
    else
	echo "INFO: Initial FIFO flags OK."
	echo
    fi
}

check_status_is_there() {
    if [ -z "`echo "$1" | grep -i ambslp_status\(\)`" ]; then
	echo "ERROR: Couldn't read AMB status (at /ambslp/scripts/standalone_tests/check_status.sh)"
	return 255
    fi
}


check_firmware_versions() {
    result=0
    if [ "`echo \"$1\" | grep -i \"hit firmware version\" | awk -F'[: ]*' '{print $4}'`" != $hit_firmware_version ]; then
	echo -n "HIT firmware version isn't $hit_firmware_version, it is"
	echo "`echo \"$1\" | grep -i \"hit firmware version\" | awk -F'[: ]*' '{print $4}'`"
	result=255
    else
	echo "HIT firmware version is $hit_firmware_version, check OK"
    fi
    
    if [ "`echo \"$1\" | grep -i \"control firmware version\" | awk -F'[: ]*' '{print $4}'`" != $ctrl_firmware_version ]; then
	echo -n "CTRL firmware version isn't $ctrl_firmware_version, it is"
	echo "`echo \"$1\" | grep -i \"ctrl firmware version\" | awk -F'[: ]*' '{print $4}'`"
	result=255
    else
	echo "CTRL firmware version is $ctrl_firmware_version, check OK"
    fi

    if [ "`echo \"$1\" | grep -i \"vme firmware version\" | awk -F'[: ]*' '{print $4}'`" != $vme_firmware_version ]; then
	echo -n "VME firmware version isn't $vme_firmware_version, it is"
	echo "`echo \"$1\" | grep -i \"vme firmware version\" | awk -F'[: ]*' '{print $4}'`"
	result=255
    else
	echo "VME firmware version is $vme_firmware_version, check OK"
    fi

    if [ "`echo \"$1\" | grep -i \"road firmware version\" | awk -F'[: ]*' '{print $4}'`" != $road_firmware_version ]; then
	echo -n "ROAD firmware version isn't $road_firmware_version, it is"
	echo "`echo \"$1\" | grep -i \"road firmware version\" | awk -F'[: ]*' '{print $4}'`"
	result=255
    else
	echo "ROAD firmware version is $road_firmware_version, check OK"
    fi

    if [ "`echo \"$1\" | grep -i \"lamb firmware version\" | awk -F'[: ]*' '{print $8}'`" != $lamb_firmware_version ]; then
	echo -n "LAMBs firmware version isn't $lamb_firmware_version, it is"
	echo "`echo \"$1\" | grep -i \"lamb firmware version\" | awk -F'[: ]*' '{print $8}'`"
	result=255
    else
	echo "LAMBs firmware version is $lamb_firmware_version, check OK"
    fi
    
    return $result
}


check_temperature() {
    echo "Checking LAMB temperatures..."
    
    temps=$(echo "$1" | awk -F'[\t| ]*' '{
if ($0 ~ "Temperature measurements") {
gottempline=1
templine=NR;
}
if (gottempline && ((NR == templine+3)||(NR == templine+4))) {
print $2
print $3
print $4
print $5
}
}')
    while read -r temp; do
	if [ $temp -gt 50 ]; then
	    echo "WARNING: Temp $temp${DEG}C is too big"
	    return 255
	else
	    echo "Temp $temp${DEG}C seems fine"
	fi
    done <<< "$temps"
}


check_gtp() {
    result=0
    echo "Checking GTP status..."
    
    alignments=$(echo "$1" | awk -F"[\t :-]+" '{
if ($0 ~ "GTP align") {
print $3" "$4
print $3" "$5
print $3" "$6
}
}')
    while read -r alignment; do
	if [ "`echo $alignment | cut -d' ' -f 2`" != "1" ]; then
	    echo "`echo $alignment | cut -d' ' -f 1` is not aligned"
	    result=255
	fi
    done <<< "$alignments"

    resets_s=$(echo "$1" | awk -F"[\t :-]+" '{
if ($0 ~ "GTP reset done") {
print $5" "$7$9
print $5" "$7$9
print $5" "$7$9
}
}')
    while read -r reset_s; do
	if [ "`echo $reset_s | cut -d' ' -f 2`" != "ff" ]; then
	    echo "`echo $reset_s | cut -d' ' -f 1` is not reset"
	    result=255
	fi
    done <<< "$resets_s"

    return $result
}

check_fifos() {
    result=0
    echo "Checking FIFO empty/prog full flag status..."
    
    empty_fifos=$(echo "$1" | awk -F"[\t :-]+" '{
if ($0 ~ "Empty Fifo") {
print $3" "$5
}
}')
    while read -r empty_fifo; do
	if [ "`echo $empty_fifo | cut -d' ' -f 2`" != "f" ]; then
	    echo "`echo $empty_fifo | cut -d' ' -f 1` input fifo is not empty, it should be"
	    result=255
	fi
    done <<< "$empty_fifos"

    prog_full_fifos=$(echo "$1" | awk -F"[\t :-]+" '{
if ($0 ~ "Prog Full Fifo") {
print $4" "$6
}
}')
    while read -r prog_full_fifo; do
	if [ "`echo $prog_full_fifo | cut -d' ' -f 2`" != "0" ]; then
	    echo "`echo $prog_full_fifo | cut -d' ' -f 1` input fifo has prog_full up, it shouldn't have"
	    result=255
	fi
    done <<< "$prog_full_fifos"

    return $result
}
