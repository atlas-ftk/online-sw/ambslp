#! /bin/bash

# Process slot argument
PROG=`basename $0`

ARGS=`getopt --name "$PROG" --longoptions slot:,pattfile:,hitfile:,boardsimfile:,results:,mydate:,dummy,holdtest --options s: -- "$@"`

if [ $? -ne 0 ]; then
  echo "$PROG: usage error (use -h for help)" >&2
  exit 2
fi

eval set -- $ARGS

source vme_functions.sh

SLOT=15
patt_file=
hit_file=
boardsim_file=
dummy=
holdtest=yes
RESDIR=./
MYDATE=$(date -d "$D" '+%d-%m')
while [ $# -gt 0 ]; do
    case "$1" in
	-s | --slot)     SLOT=$2; shift;;
	--pattfile)      patt_file=$2; echo "pattern file set to $patt_file"; shift;;
	--results)       RESDIR=$2; echo "results directory set to $RESDIR"; shift;;
	--hitfile)       hit_file=$2; echo "hit file set to $hit_file"; shift;;
	--boardsimfile)  boardsim_file=$2; echo "board simulation output set to $boardsim_file"; shift;;
        --mydate)        MYDATE=$2; shift;;
	--dummy)         dummy=yes;;
	--holdtest)      holdtest=yes;;
        --)              shift; break;; # end of options
    esac
    shift
done

echo "test run descriptor: $fname_descriptor"

#boardsim_file=./outroads_Twr22.out
#hit_file=./sshits_Twr22.ss
spyin_file=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_inp.txt
spyout_file=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_out.txt
spyrawout_file=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_rawout.txt
compare_result_file=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_compres.txt
test_desc_file=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_desc.txt
test_hold_file=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_hold.txt

if [ ! -d tests-${MYDATE} ]; then
    mkdir -p ${RESDIR}/tests-${MYDATE}
fi
if [ ! -e $test_desc_file ]; then
    touch $test_desc_file
fi

echo "pattern file used: $patt_file" > $test_desc_file
echo "board simulation output used: $boardsim_file" >> $test_desc_file
echo "hit input file used: $hit_file" >> $test_desc_file
echo "spybuffer input file: $spyin_file" >> $test_desc_file
echo "spybuffer output file: $spyout_file" >> $test_desc_file
echo "spybuffer raw output file: $spyrawout_file" >> $test_desc_file
echo "comparison with simulation results: $compare_result_file" >> $test_desc_file
echo >> $test_desc_file
echo >> $test_desc_file

if [ -n "$dummy" ]; then
    echo "Dummy hits are enabled" >> $test_desc_file
else
    echo "Dummy hits are disabled" >> $test_desc_file
fi
echo >> $test_desc_file
echo >> $test_desc_file

echo "status before test" >> $test_desc_file
ambslp_status_main --slot $SLOT >> $test_desc_file
echo >> $test_desc_file
echo >> $test_desc_file

#echo "Resetting AMB"
ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect run > $test_desc_file

#    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect > $test_desc_file
#    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect > $test_desc_file

#ambslp_init_main --slot $SLOT
#ambslp_reset_spy_main --slot $SLOT

# enable dummy hits, maybe non necessary any more?
if [ -n "$dummy" ]; then
    echo "Enabling dummy hits"
    echo "Enabling dummy hits" >> $test_desc_file
    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) 0xf000f001
    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x0000216c))) 0x00000000
    sleep 1
fi

# FIXME: slot 1 will generate one digit less, but it's actually the SBC there so OK
SLOTTOHEX=`echo "obase=16; ${SLOT}*8" | bc`
PSREGADDRESS=${SLOTTOHEX}002174

# query the low power function, if it's on it returns 0xf
echo "vme_peek 0x${PSREGADDRESS} returns" >> $test_desc_file

vme_peek 0x${PSREGADDRESS} >> $test_desc_file
echo >> $test_desc_file
echo >> $test_desc_file

echo "status after AMB reset" >> $test_desc_file
ambslp_status_main --slot $SLOT >> $test_desc_file
echo >> $test_desc_file
echo >> $test_desc_file

#slot_poke $SLOT 0x00002098 0x00010000
#useless?

echo "Feeding hits"
ambslp_feed_hit_main --slot $SLOT --hit_file ./empty_event.hit > $test_desc_file
sleep 0.2

echo "Enable the reset of the input and output spy buffer"
ambslp_reset_spy_main --slot $SLOT > $test_desc_file
sleep 0.2

# Force HOLD to LAMB
if [ "$holdtest" == "yes" ]; then
    echo "forcing HOLD to LAMB"
    slot_poke $SLOT 0x0000414c 0xffff
    slot_peek $SLOT 0x0000414c 0xffff
    echo "expected output 0xffff"
fi

echo "saving amb status in log before sending real events" > $test_desc_file
ambslp_status_main --slot $SLOT > $test_desc_file

# added to check the alignment
# ambslp_reset_gtp --mode 0x2 --max_tries 10  

ambslp_feed_hit_main --slot $SLOT --hit_file $hit_file > $test_desc_file
sleep 0.1
echo "status after test" >> $test_desc_file
echo FTK_INST_PATH=$FTK_INST_PATH >> $test_desc_file
echo FTK_RELEASE=$FTK_RELEASE >> $test_desc_file
ambslp_status_main --slot $SLOT >> $test_desc_file
echo $PATH >> $test_desc_file

#Spy before and after removing hold
if [ "$holdtest" == "yes" ]; then
    echo " "
    echo "Status before removing hold"
    echo "status before removing hold" >> $test_desc_file
    ambslp_out_spy_main --slot $SLOT | tail -8
    ambslp_out_spy_main --slot $SLOT | tail -8 >> $test_hold_file
    sleep 0.5
    slot_poke $SLOT 0x0000414c 0x0000

    echo " "
    echo "Status after removing hold"
    echo "status after removing hold" >> $test_desc_file

    ambslp_out_spy_main --slot $SLOT | tail -8 
    ambslp_out_spy_main --slot $SLOT | tail -8  >> $test_hold_file
fi

ambslp_status_main --slot $SLOT >> $test_desc_file
#this sleep is necessary (or at least it seems to be)

sleep 1

echo "Getting input spybuffer"
ambslp_inp_spy_main --slot $SLOT > $spyin_file
sleep 0.1

echo "Getting output spybuffer"
ambslp_out_spy_main --slot $SLOT --method 1 2> $spyout_file
ambslp_out_spy_main --slot $SLOT > $spyrawout_file
sleep 0.1

echo "Comparing output spybuffer results with simulation"
#wc --> word count, wc -l line count
count=$(wc -l < "$spyout_file")
events=`echo "obase=10; $(( 0x$(echo "$(echo "$(tail -1 $boardsim_file)")")&0xff ))" | bc`
#lt --> less than 
if [ $count -lt $events ]; then
    echo "Unable to make a comparison. Output file doesn't match minimal requirements"
else
    if [ "$holdtest" != "yes" ]; then
	./AMBCompareTestVecOutput.py $spyout_file $boardsim_file > $compare_result_file
	tail $compare_result_file 
    fi
fi

if [ "$holdtest" == "yes" ]; then
    result=$(diff $test_hold_file hold_compare_output.txt)
    if [ $? -eq 0 ]
    then
	echo
        echo "INFO: Hold output test is the same as expected, TEST PASSED! (at run_one_iteration.sh)"
	echo
    else
	echo
        echo "ERROR: output is different from expected output, diff printed below: (at run_one_iteration.sh)"
        echo "$result"
    fi
fi

# Disable dummy hits after each run
if [ -n "$dummy" ]; then
    echo "Disabling dummy hits again"
    echo "Disabling dummy hits again" >> $test_desc_file
    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x00002168))) 0x00000000
    vme_poke $(printf "%x" $(( ($SLOT * 128 * 1024 * 1024 ) | 0x0000216c))) 0x00000000
fi