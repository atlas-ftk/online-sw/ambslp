#! /bin/bash

# Argument processing
PROG=`basename $0`

ARGS=`getopt --name "$PROG" --longoptions iters:,slot:,pattload,holdtest,pattfile:,npatts:,hitfile:,boardsimfile:,extra:,results:,dummy,help --options i:,s:,p,e:,d,h -- "$@"`

if [ $? -ne 0 ]; then
  echo "$PROG: usage error (use -h for help)" >&2
  exit 2
fi

eval set -- $ARGS
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

MYFTKRELEASE=FTK-01-00-11
HELP_ARG=no
ITERS= #3 is a good choice
PATTLOAD=no
HOLDTEST=yes
SLOT=15
EXTRATESTDESC=
PATTFILE=/afs/cern.ch/user/a/annovi/work/public/AMB_run_test_FTK/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root
NPATTS=131072
HITFILE=$SCRIPTDIR/AMB_feed_randomtest.ss
BOARDSIMFILE=$SCRIPTDIR/roads_randomtest.out
#following files need debugging
#HITFILE=$SCRIPTDIR/sshits_Twr22.ss
#BOARDSIMFILE=$SCRIPTDIR/outroads_Twr22.out
DUMMY=
RESDIR=$SCRIPTDIR
while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)     HELP_ARG=yes;;
        -i | --iters)    ITERS=$2; echo "iterations set to $ITERS"; shift;;
	-s | --slot)     SLOT=$2; echo "slot set to $SLOT"; shift;;
	-p | --pattload) PATTLOAD=yes; echo "will load a pattern bank";;
	--holdtest)      HOLDTEST=yes; echo "will do an hold test";;
	--pattfile)      PATTFILE=$2; echo "pattern file set to $PATTFILE"; shift;;
	--results)       RESDIR=$2; echo "results directory set to $RESDIR"; shift;;
	--npatts)        NPATTS=$2; echo "number of patterns set to $NPATTS"; shift;;
	--hitfile)       HITFILE=$2; echo "hit file set to $HITFILE"; shift;;
	-d | --dummy)    DUMMY="--dummy"; echo "dummy hits are enabled";;
	--boardsimfile)  BOARDSIMFILE=$2; echo "board simulation output set to $BOARDSIMFILE"; shift;;
	-e | --extra)    EXTRATESTDESC=$2; echo "extra description to append to output files set to $EXTRATESTDESC"; shift;;
        --)              shift; break;; # end of options
    esac
    shift
done

# Setting some variables
MYDATE=$(date '+%m-%d-%H-%M')
MYCRATE=$(source $SCRIPTDIR/printCrate.sh)

if [ "$HOLDTEST" = "yes" ]; then
    HITFILE=/afs/cern.ch/user/a/annovi/work/public/AMB_run_test_FTK/Hits_HOLDtest.ss
    BOARDSIMFILE=/afs/cern.ch/user/a/annovi/work/public/AMB_run_test_FTK/Boardsim_HOLDtest.out
    PATTFILE=/afs/cern.ch/user/a/annovi/work/public/AMB_run_test_FTK/Bank_HOLDtest_64chips_128kpatt.patt.bz2
fi


main() {
    if [ "$HELP_ARG" == "yes" ]; then
	echo "Usage:"
	echo "  $PROG [--slot=N] [--iters M] [--pattload] [--holdtest] [--pattfile=pattern_file] [--npatts Npatts] [--hitfile=Hit_File] [--boardsimfile=board_sim_output_file] [--extra=string_to_append_to_test_files] [--dummy] [--help]"
	echo "or"
	echo "  $PROG [-s N] [-i M] [-p] [-e string_to_append_to_test_files] [-h]"
	echo
	echo "  The script looks for an FTK release environment, if it doesn't find one it"
	echo "  loads one. Then if passed the flag --pattload, it optionally loads the"
	echo "  --npatts patterns specified with --pattfile. Finally, for a number of"
	echo "  repetitions set with --iters it sends a hit file set with --hitfile, stores"
	echo "  input and output spybuffers, and compares the output with the board simulation"
	echo "  file set with --boardsimfile. The results are stored in files in a directory"
	echo "  test-mm-dd, each test file gets the string defined in --extra appended to it."
	echo "  --dummy enables the dummy hits function. You can the program from your space,"
	echo "  as long as you set the --results option to the directory you want the output"
	echo "  to be stored (must be somewhere you have write access to)."
	echo "  --holdtest is used to test the hold check the hold lines."
	return 0
    fi

    if [ -z "$FTK_INST_PATH" ]; then
	echo "FTK release environment not found, setting up release $MYFTKRELEASE"
	source /afs/cern.ch/user/f/ftk/public/bin/setup_ftk.sh $MYFTKRELEASE
    fi
    
    if [ "$PATTLOAD" = "yes" ]; then
	echo "Loading patterns..."
	if [ "$HOLDTEST" != "yes" ]; then
	    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" --bankpath $PATTFILE -N $NPATTS -T 0 load
	# else
	#     #ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" --bankpath $PATTFILE -N $NPATTS -T 1 load
	fi
    fi

    mkdir -p ${RESDIR}/tests-${MYDATE}${EXTRATESTDESC}/
    echo created directory ${RESDIR}/tests-${MYDATE}${EXTRATESTDESC}/

    if [ -z $EXTRATESTDESC ]; then
	fname_prefix=${MYDATE}-${MYCRATE}_Slot${SLOT}
    else
	fname_prefix=${MYDATE}-${MYCRATE}_Slot${SLOT}-${EXTRATESTDESC}
    fi
    
    echo "running config, connect, takes several seconds"
    source ./reset_AUX.sh --slot $SLOT > ${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-config_log.txt
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config > ${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-config_log.txt
    ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" connect > ${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-config_log.txt

    rm ${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-[0-9]*_*
    rm ${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-compare_summary.txt
    
    SUMMARY_FILE=${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-compare_summary.txt
    NDISP_ERRORS=0
    echo "Check for disparity errors" > $SUMMARY_FILE

    #iteration
    for i in `seq 1 $ITERS`; do
	export fname_descriptor=${fname_prefix}-$i
	echo $fname_descriptor
	if [ "$HOLDTEST" = "yes" ]; then
	    echo "run_one_iteration.sh"
	    echo ""
	    $SCRIPTDIR/run_one_iteration.sh --slot $SLOT --pattfile $PATTFILE --hitfile $HITFILE --boardsimfile $BOARDSIMFILE --results "$RESDIR" --mydate "$MYDATE" $DUMMY --holdtest
	else
	    echo $SCRIPTDIR/run_one_iteration.sh
	    $SCRIPTDIR/run_one_iteration.sh --slot $SLOT --pattfile $PATTFILE --hitfile $HITFILE --boardsimfile $BOARDSIMFILE --results "$RESDIR" --mydate "$MYDATE" $DUMMY
	fi
        STATUS_FILE=${RESDIR}/tests-${MYDATE}/test-${fname_descriptor}_status-after-test.log
        ambslp_status_main > $STATUS_FILE
        DISP_ERROR=$(grep disparity $STATUS_FILE | grep -v 0x0\$); 
        if [ "$DISP_ERROR" ]; then 
            NDISP_ERRORS=$(($NDISP_ERRORS + 1));
            echo "DISPARITY ERRORS in iteration $i $(date)" >> $SUMMARY_FILE
        fi
    done

    if [ "$HOLDTEST" != "yes" ]; then
	echo >> $SUMMARY_FILE
	echo Found $NDISP_ERRORS disparity errors >> $SUMMARY_FILE
	echo >> $SUMMARY_FILE
	echo >> $SUMMARY_FILE
	echo "Number of iterations: $ITERS" >> $SUMMARY_FILE
	$SCRIPTDIR/countresults.sh ${RESDIR}/tests-${MYDATE}/test-${fname_prefix}-[0-9]*_compres.txt >> $SUMMARY_FILE
	
	echo
	echo OUTPUT DIR is ${RESDIR}/tests-${MYDATE}/
	echo cat $SUMMARY_FILE
	echo
	cat $SUMMARY_FILE
    fi
}

main "$@"
