#!/bin/bash
SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi

#This script resets the GTP counters (so the initial randomly sent errors are not counted anymore) and shows the errors in each of the 16 links (4 links for each of the 4 LAMBs). If there are no real errors, the counters are all “0”.

source vme_functions.sh

PRESENCE=$( slot_peek $SLOT 0x00006000 )

PRBS_ROAD_CHECK=$((($( slot_peek $SLOT 0x000040dc )>>4)&0x1))

PRBS_ROAD_GEN=$((($(  slot_peek $SLOT 0x00004100 )>>0)&0x1))

PRBS_ROAD_GEN_ERR=$((($(  slot_peek $SLOT 0x00004100 )>>1)&0x1))

LAMB0=$((( $PRESENCE>>16)&0x1))
LAMB1=$((( $PRESENCE>>17)&0x1))
LAMB2=$((( $PRESENCE>>18)&0x1))
LAMB3=$((( $PRESENCE>>19)&0x1))

ERRORS_113_ROAD=$( slot_peek $SLOT 0x000040e8 )
ERRORS_116_ROAD=$( slot_peek $SLOT 0x000040ec )
ERRORS_213_ROAD=$( slot_peek $SLOT 0x000040f0 )
ERRORS_216_ROAD=$( slot_peek $SLOT 0x000040f4 )

MASK_LINK=0x000000ff

SHIFT_LINK0=0
SHIFT_LINK1=8
SHIFT_LINK2=16
SHIFT_LINK3=24

MASK_Missing_Link0=0xffffff00
MASK_Missing_Link1=0xffff00ff
MASK_Missing_Link2=0xff00ffff
MASK_Missing_Link3=0x00ffffff

ERRORS_113_ROAD_Masked=$(( $ERRORS_113_ROAD))
ERRORS_116_ROAD_Masked=$(( $ERRORS_116_ROAD))
ERRORS_213_ROAD_Masked=$(( $ERRORS_213_ROAD))
ERRORS_216_ROAD_Masked=$(( $ERRORS_216_ROAD))






if [ $PRBS_ROAD_CHECK = 1 ]; then
        if [ $LAMB0  = 0 ]; then
					
		ERRORS_113_ROAD_Masked=$(( $ERRORS_113_ROAD_Masked&$MASK_Missing_Link3))
        	ERRORS_113_ROAD_Masked=$(( $ERRORS_113_ROAD_Masked&$MASK_Missing_Link2))
        	ERRORS_113_ROAD_Masked=$(( $ERRORS_113_ROAD_Masked&$MASK_Missing_Link0))
        	ERRORS_213_ROAD_Masked=$(( $ERRORS_213_ROAD_Masked&$MASK_Missing_Link2))
		                
        fi

        if [ $LAMB1  = 0 ]; then

	        ERRORS_216_ROAD_Masked=$(( $ERRORS_216_ROAD_Masked&$MASK_Missing_Link3))
	        ERRORS_216_ROAD_Masked=$(( $ERRORS_216_ROAD_Masked&$MASK_Missing_Link2))
                ERRORS_213_ROAD_Masked=$(( $ERRORS_213_ROAD_Masked&$MASK_Missing_Link1))
                ERRORS_213_ROAD_Masked=$(( $ERRORS_213_ROAD_Masked&$MASK_Missing_Link0))
        fi

        if [ $LAMB2  = 0 ]; then
                ERRORS_116_ROAD_Masked=$(( $ERRORS_116_ROAD_Masked&$MASK_Missing_Link1))
                ERRORS_116_ROAD_Masked=$(( $ERRORS_116_ROAD_Masked&$MASK_Missing_Link0))
                ERRORS_216_ROAD_Masked=$(( $ERRORS_216_ROAD_Masked&$MASK_Missing_Link0))
                ERRORS_216_ROAD_Masked=$(( $ERRORS_216_ROAD_Masked&$MASK_Missing_Link1))

        fi

        if [ $LAMB3  = 0 ]; then
                ERRORS_116_ROAD_Masked=$(( $ERRORS_116_ROAD_Masked&$MASK_Missing_Link2))
                ERRORS_116_ROAD_Masked=$(( $ERRORS_116_ROAD_Masked&$MASK_Missing_Link3))
                ERRORS_213_ROAD_Masked=$(( $ERRORS_213_ROAD_Masked&$MASK_Missing_Link3))
                ERRORS_113_ROAD_Masked=$(( $ERRORS_113_ROAD_Masked&$MASK_Missing_Link1))
        fi

fi



if [ $ERRORS_113_ROAD_Masked -ne 0 ]; then
    echo reset quad 113
    slot_poke $SLOT 0x00004104 1
    slot_poke $SLOT 0x00004104 0
fi
if [ $ERRORS_116_ROAD_Masked -ne 0 ]; then
    echo reset quad 116 
    slot_poke $SLOT 0x00004104 2
    slot_poke $SLOT 0x00004104 0
fi
if [ $ERRORS_213_ROAD_Masked -ne 0 ]; then
    echo reset quad 213 
    slot_poke $SLOT 0x00004104 4
    slot_poke $SLOT 0x00004104 0
fi
if [ $ERRORS_216_ROAD_Masked -ne 0 ]; then
    echo reset quad 216 
    slot_poke $SLOT 0x00004104 8
    slot_poke $SLOT 0x00004104 0
fi



ambslp_init_main --slot $SLOT  > /dev/null 2>&1


if [ $PRBS_ROAD_CHECK = 1 ]; then
	if [ $LAMB0 = 1 ] || [ $LAMB1 = 1 ] || [ $LAMB2 = 1 ] || [ $LAMB3 = 1 ]; then
		echo ----------------------------------ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD--------------------------------------------------------
	fi

	if [ $LAMB0  = 1 ]; then
		echo -e "Errors on LAMB0:" ' \t ' ' \t ' "Link3:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK2)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK0)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK3)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
	fi

	if [ $LAMB1  = 1 ]; then
		echo -e "Errors on LAMB1:"  ' \t '' \t ' "Link3:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK3)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK2)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK1)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
	fi

	if [ $LAMB2  = 1 ]; then
		echo -e "Errors on LAMB2:"  ' \t '' \t ' "Link3:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK1)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK0)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK0)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
	fi

	if [ $LAMB3  = 1 ]; then
		echo -e "Errors on LAMB3:"  ' \t '' \t ' "Link3:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK2)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK3)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK3)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
	fi

        if [ $LAMB0 = 1 ] || [ $LAMB1 = 1 ] || [ $LAMB2 = 1 ] || [ $LAMB3 = 1 ]; then
         	echo ------------------------------------------------------------------------------------------------------------------------------------------
        fi
fi



if [ $LAMB0  = 1 ]; then
    Link3=$((( $ERRORS_113_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
    Link2=$((( $ERRORS_113_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
    Link1=$((( $ERRORS_113_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
    Link0=$((( $ERRORS_213_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
    if [[ $Link3 != 0 || $Link2 != 0 || $Link1 != 0 || $Link0 != 0 ]]; then
	echo "ERROR: test on the output links not passed for LAMB 0 (at read_AM_out.sh)"
    else
	echo "INFO: test on the output links passed for LAMB 0 (at read_AM_out.sh)"	
    fi
fi

if [ $LAMB1  = 1 ]; then
    Link3=$((( $ERRORS_216_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
    Link2=$((( $ERRORS_216_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
    Link1=$((( $ERRORS_213_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
    Link0=$((( $ERRORS_213_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
    if [[ $Link3 != 0 || $Link2 != 0 || $Link1 != 0 || $Link0 != 0 ]]; then
	echo "ERROR: test on the output links not passed for LAMB 1 (at read_AM_out.sh)"
    else
	echo "INFO: test on the output links passed for LAMB 1 (at read_AM_out.sh)"	
    fi
fi

if [ $LAMB2  = 1 ]; then
    Link3=$((( $ERRORS_116_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
    Link2=$((( $ERRORS_116_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
    Link1=$((( $ERRORS_216_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
    Link0=$((( $ERRORS_216_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
    if [[ $Link3 != 0 || $Link2 != 0 || $Link1 != 0 || $Link0 != 0 ]]; then
	echo "ERROR: test on the output links not passed for LAMB 2 (at read_AM_out.sh)"
    else
	echo "INFO: test on the output links passed for LAMB 2 (at read_AM_out.sh)"	
    fi
fi

if [ $LAMB3  = 1 ]; then
    Link3=$((( $ERRORS_116_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
    Link2=$((( $ERRORS_116_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
    Link1=$((( $ERRORS_213_ROAD>>$SHIFT_LINK3)&$MASK_LINK))
    Link0=$((( $ERRORS_113_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
    if [[ $Link3 != 0 || $Link2 != 0 || $Link1 != 0 || $Link0 != 0 ]]; then
	echo "ERROR: test on the output links not passed for LAMB 3 (at read_AM_out.sh)"
    else
	echo "INFO: test on the output links passed for LAMB 3 (at read_AM_out.sh)"	
    fi
fi
