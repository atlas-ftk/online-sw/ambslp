#!/bin/zsh -f

SLOT=15
if [ $# -eq 3 ]; then
  SLOT=$1
fi

MODE="interactive"
if [ $# -eq 3 ]; then
  MODE=$2
fi

TEST="all"
#other: LAMB
if [ $# -eq 3 ]; then
  TEST=$3
fi

source vme_functions.sh

# We want to trap the Ctrl-C signal to be able to quit the JTAG servers while keeping the script running
control_c()
{
  echo -en "\n*** Got the Ctrl-C ***\n"
}

export PRBSLOGFILENAME=/tmp/$USER-$(date +%Y%m%d_%H%M%S)_testAMB_prbs.log
echo prbs logfile is $PRBSLOGFILENAME

# Reset the aux and setup everything
echo "running reset_AUX, it takes some time..."
./reset_AUX.sh --slot $SLOT > $PRBSLOGFILENAME
echo "running ambslp_procedure config connect, it takes some time..."
ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect > $PRBSLOGFILENAME
    
# VME to the AUX
echo -n "Next step is to check VME to the AUX, will run aux_vmetest_main (takes some time). "
echo -n "Do you wish to run aux_vmetest_main (y/n, if n the script will skip this test)? "
if [ $MODE != automatic ]; then read answer; else answer="yes"; fi
if [ $TEST != all ]; then answer="no"; fi

if echo "$answer" | grep -iq "^y" ;then
    aux_vmetest_main --slot $SLOT --n 1000
    echo -n "Correct output should be all zeros on the right column, is the output OK? (press ENTER to continue):"
    if [ $MODE != automatic ]; then read cont_ok; fi
else
    echo "No, going on with the next step"
fi

echo "Will run block transfer test, this test is only available for release FTK-01-00-16 and following."
echo -n "Do you want to run this test (y/n)?"
if [ $MODE != automatic ]; then read answer; else answer="yes"; fi
if [ $TEST != all ]; then answer="no"; fi
if echo "$answer" | grep -iq "^y" ;then
    ./reset_AUX.sh --slot $SLOT
    python ./reconfigureBoard.py --slot $SLOT
    aux_reset_main --slot $SLOT --freeze --fpga 3
    aux_reset_main --slot $SLOT --freeze --fpga 4
    aux_reset_main --slot $SLOT --freeze --fpga 5
    aux_reset_main --slot $SLOT --freeze --fpga 6
    aux_ammap_load_main -a /afs/cern.ch/work/a/annovi/public/AMBtest/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root --loadAllSectors -n 8388608 -s $SLOT -f 3
    echo -n "Is the output OK? (press ENTER to continue)"
    if [ $MODE != automatic ]; then read cont_ok; fi
    echo "if the test is not working it could be that the aux_firmware is outdated, if so follow the instruction below and try again"
    echo "This will load new firmware to the application firmware location on the epcqs and reconfigure the fpgas to the new firmware"
    echo "cp -r /afs/cern.ch/user/p/pbryant/public/aux_firmware ."
    echo "cd aux_firmware"
    echo "python upgradeFirmware.py -f 160 -s [slot]"
    echo "python reconfigureBoard.py --slot [slot]"
else
    echo "No, going on with the next step"
fi

# VME to the LAMBs
echo "Check VME to the LAMBs, will run ambslp_amchip_idcode_main:"
ambslp_amchip_idcode_main --slot $SLOT
echo "Output should look like that: Idcode chain 0 LAMB 0 60003071  60003071        0       0"
echo -n "just ran ambslp_amchip_idcode_main, a summary is printed at the end (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi
echo

if [ 1 -eq 0 ]; then 
echo "Running JTAG server for AMB FPGAs, fire up Impact and"
echo "see if everything is as should be, disconnect the cable,"
echo "(you should see a connection closed message here) then"
echo "exit the server with Ctrl+C"

echo "the address of the server for crate 16, if within cern is"
echo "xilinx_xvc host=sbc-tbed-ftk-01:2543 disableversioncheck=true"
trap control_c SIGINT
ambslp_xvc --slot $SLOT -v

echo "Running JTAG server for LAMB FPGAs, do as you did before"
ambslp_xvc --slot $SLOT -l -v
trap SIGINT
fi

echo "enabling PRBS check on the HIT FPGA, take some seconds..."
./reset_AUX.sh --slot $SLOT > $PRBSLOGFILENAME
ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config connect > $PRBSLOGFILENAME
slot_poke $SLOT 0x000020d8 ffff
slot_poke $SLOT 0x000020e0 0x0
slot_poke $SLOT 0x000020e0 0x3

echo "sending an init to the AM board"
ambslp_init_main --slot $SLOT

#cd ${FTK_RELEASE_PATH}/FTK-nightly-head/ambslp/scripts/PRBS_Test/

PRBS_HIT=$(slot_peek $SLOT 0x000020e0)
if [ $PRBS_HIT != 0x00000003 ]; 
then echo "WARNING: number in register 0x000020e0 is unexpected, please check the previous steps!"
fi

echo "Reading the prbs checked packets and error packets counters"
./read_PRBS_result.sh $SLOT "standard"

echo "everything should be zero at this point."
echo "will send two hits and read again (not relevant for LAMB testing), type"
echo -n "something : "
if [ $MODE != automatic ]; then read cont_ok; fi

if [ $TEST != LAMB ]; then
    ./send_2hits.sh $SLOT > $PRBSLOGFILENAME
    ./read_PRBS_result.sh $SLOT "standard"
    echo "Checked packets should be 2, errors should be 1, is everything OK? "
fi

echo -n "Enable the PRBS generator on the AUX board. (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi
python reconfigureBoard.py --slot $SLOT > $PRBSLOGFILENAME

aux_write_main --slot $SLOT --fpga 1 0x30 0x3
aux_write_main --slot $SLOT --fpga 2 0x30 0x3
#register name: DEBUG_DISABLE_HOLD_HIT
slot_poke $SLOT 0x000020d8 ffff

PRBS_HIT=$(slot_peek $SLOT 0x000020e0)
if [ $PRBS_HIT != 0x00000003 ]; 
then echo "WARNING: number in register 0x000020e0 is unexpected, please check the previous steps!"
fi

echo -n "Following test will last 6s, CHECKED PACKETS values should change, upper table, ERROR LINKS should remain the same, lower table (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi
if [ $MODE != automatic ]; then
    ( watch -n 0.5 --differences=permanent "./read_PRBS_result.sh $SLOT 'standard'; echo CHECKED PACKETS values should change, upper table; echo ERROR LINKS should remain the same, lower table;" ) & pid=$!
    ( sleep 6s && kill -HUP $pid ) 
else
    ./changing_counters.sh $SLOT
fi

echo "Well, hopefully, they are changing."

echo -n "Now I will prepare some stuff for the next step. (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi

#added this reset to avoid crash in the config connect
echo "Resetting the AUX... "
./reset_AUX.sh --slot $SLOT > $PRBSLOGFILENAME
echo "Config connect procedure, takes some seconds... "
ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" config > $PRBSLOGFILENAME
ambslp_procedure --slot $SLOT -P "FTK.data.xml" -R "PU-1-15" connect > $PRBSLOGFILENAME


# Output links
echo " Next step is to check the output links"
echo -n "Do you wish to check the output links (y/n, if n the script will skip this test)? "
if [ $MODE != automatic ]; then read answer; else answer="yes"; fi
if echo "$answer" | grep -iq "^y" ;then

#Configure AMB, LAMBs and AUX to enable PRBS
    ./configure_PRBS_test.sh $SLOT
    echo "Running after 10 minutes"
    echo "This script generates random errors at the beginning and it check that all the links in the table ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD do have errors."
    echo -n "The test result is printed after the table (press ENTER to continue)."
    if [ $MODE != automatic ]; then read cont_ok; fi
    echo "Check the output links, the script is sent 3 times, because the reset of some of the counters might fail at first."
    ./read_AM_out.sh $SLOT
    ./read_AM_out.sh $SLOT
    ./read_AM_out.sh $SLOT

    echo "This script resets the GTP counters and shows the errors in each of the 16 links (4 links for each of the 4 LAMBs). If there are no real errors, the counters are all “0”. You see a table with 4 rows (one for each LAMB) and 4 columns (one for each link). If one or more LAMBs are missing, you will see less rows."
    echo "A summary with the test results is printed after the table."
    echo -n "Do you wish to skip the 10 minutes sleep (y/n, if y the script will only stop for 10 seconds)? "
    if [ $MODE != automatic ]; then read answer;
    else 
	answer="no"; 
	echo "no"
    fi
    for i in {1..10}
    do
	if echo "$answer" | grep -iq "^y" ;then
	    sleep 1
	    echo $i "seconds"
	else
	    sleep 60
	    echo $i "minutes"
	fi
    done
    ./read_AM_out.sh $SLOT
    echo "If there are no real errors the counters are all 0. A summary with the test result is printed after the table."
else
    echo "No, going on with the next step"
fi

# Input links
echo " Next step is to check the input links"
#./read_PRBS_IN_AMchip.sh $SLOT
echo -n "Do you wish to check the input links (y/n, if n the script will skip this test)? "
if [ $MODE != automatic ]; then read answer; else answer="yes"; fi
if echo "$answer" | grep -iq "^y" ;then
    echo -n "Do you wish to run the quick version of the test (y/n, if y the script will skip the 10 minutes break)? "
    if [ $MODE != automatic ]; then read answer; else answer="no"; fi
    if echo "$answer" | grep -iq "^y" ;then
	ambslp_PRBS_test_main --slot $SLOT --quick
    else
	ambslp_PRBS_test_main --slot $SLOT
    fi
else
    echo "No, going on with the next step"
fi

echo "Next step is to check the links between the AM chips in each group of 4:"
#./read_PRBS_between_AMchip.sh $SLOT
echo -n "Do you wish to check the links between the AM chips (y/n, if n the script will skip this test)? "
if [ $MODE != automatic ]; then read answer; else answer="yes"; fi
if echo "$answer" | grep -iq "^n" ;then
    echo "No, going on with the next step"
else
    echo -n "Do you wish to run the quick version of the test (y/n, if y the script will skip the 10 minutes break)? "
    if [ $MODE != automatic ]; then read answer; else answer="no"; fi
    if echo "$answer" | grep -iq "^y"
    then
        ambslp_PRBS_pattin_test_main --slot $SLOT --quick
    else
	ambslp_PRBS_pattin_test_main --slot $SLOT
    fi
fi

echo -n "doing some more stuff. (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi

#change the register configuration to avoid printing useless table when doing read_PRBS_result
slot_poke $SLOT 0x000020e0 0x0

#Name of the register: CONFIG_ROAD, enable PRBS checker from AM06 chips to road
slot_poke $SLOT 0x000040dc 0x10

sleep 0.2

ambslp_amchip_prbsgen_main --slot $SLOT --lambver 2 > /dev/null 2>&1

sleep 0.2

ambslp_init_main --slot $SLOT > /dev/null 2>&1

sleep 0.2

#put the 10 minutes as an option
./read_PRBS_result.sh $SLOT "zeroErrors"

echo "Counters in the table ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD should be zero."
echo -n "A summary is printed after the table (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi

ambslp_amchip_8b_10b_main --slot $SLOT

sleep 0.2

# echo -n -e "\nFollowing test will last 30s, Counters in the table ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD should change (press ENTER to continue): "
echo -n -e "\nFollowing test will check the counters for ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD (press ENTER to continue): "
if [ $MODE != automatic ]; then read cont_ok; fi
./changing_errors.sh $SLOT

#Name of the register: CONFIG_ROAD, now disable PRBS checker from AM06 chips to road
slot_poke $SLOT 0x000040dc 0x0

echo "PRBS tests complete"