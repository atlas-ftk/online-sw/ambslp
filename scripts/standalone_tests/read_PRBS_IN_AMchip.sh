#!/bin/bash

SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi

#ambslp_amchip_prbs_error_count_all_buses_main --slot $SLOT --chainlength 2  --chipnum 3  --policy disable > /dev/null 2>&1
#ambslp_amchip_prbs_error_count_all_buses_main --slot $SLOT --chainlength 2  --chipnum 3  --policy enable  > /dev/null 2>&1
  
#echo "Polling status registers"
#ambslp_amchip_polling_stat_reg_main --slot $SLOT --chainlength 2  --chipnum 3 
echo "Testing the input links in a few steps:"
echo "1)Enable PRBS generator 2)Input links are tested 3)Disable PRBS 4)Retest"
echo "5)Enable PRBS generator 6)Retest 7)Stop for 10 seconds 8)Retest 9)Stop for 10 minutes 10)Retest"
echo "If an error is found at any step the test stops and a detailed output is printed"
echo "  "

ambslp_PRBS_test_main --slot $SLOT