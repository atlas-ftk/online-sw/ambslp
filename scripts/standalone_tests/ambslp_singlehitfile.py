#/hmarez/public/python/ambslp_singlehitfile.py

import sys

#select the file that you would like to view
if(len(sys.argv)!=9):
	sys.exit('Please list the hits files you would like to use in the command-line argument.\n Example: python ambslp_singlefile.py aux_{0..7}_hits.txt')
else:
	aux0 = sys.argv[1]
	aux1 = sys.argv[2]
	aux2 = sys.argv[3]
	aux3 = sys.argv[4]
	aux4 = sys.argv[5]
	aux5 = sys.argv[6]
	aux6 = sys.argv[7]
	aux7 = sys.argv[8]

#open the selected file and read in data into a list
def hitlist(filename):
	with open(filename) as f:
		hitslist = []
		for line in f.readlines():
			if line.startswith('1'):
				hitslist.append("0X8"+line[4:-1])
			elif line.startswith('0'):
				hitslist.append("0X0"+line[4:-1])
			else:
				continue
	return hitslist

#creates dummy lists so you can have 12 columns
def dummycolumn(filename):
	with open(filename) as f:
		dummylist = []
		for line in f.readlines():
			if line.startswith('0'):
				dummylist.append("0X400000000")
			else:
				dummylist.append("0X8"+line[4:-1])
	return dummylist
	
#create a list for every hits file from gen hits
hit0 = hitlist(aux0)
hit1 = hitlist(aux1)
hit2 = hitlist(aux2)
hit3 = hitlist(aux3)
hit4 = hitlist(aux4)
dummy4 = dummycolumn(aux4)
hit5 = hitlist(aux5)
dummy5 = dummycolumn(aux5)
hit6 = hitlist(aux6)
dummy6 = dummycolumn(aux6)
hit7 = hitlist(aux7)
dummy7 = dummycolumn(aux7)

#write them out to a single file that can be used for board testing
with open('singlehitsfile.txt','w') as f:
		lis=[hit0,hit1,hit2,hit3,hit4,dummy4,hit5,dummy5,hit6,dummy6,hit7,dummy7]
		for x in zip(*lis):
			f.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\n".format(*x))
