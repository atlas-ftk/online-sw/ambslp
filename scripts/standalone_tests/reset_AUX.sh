#!/bin/bash

PROG=`basename $0`

ARGS=`getopt --name "$PROG" --longoptions slot:,help --options s:,h -- "$@"`

if [ $? -ne 0 ]; then
  echo "$PROG: usage error (use -h for help)" >&2
  exit 2
fi

eval set -- $ARGS

HELP_ARG=
HAS_SLOT_ARG=0
while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)     HELP_ARG=yes;;
        -s | --slot)     HAS_SLOT_ARG=1; USE_SLOT="$2"; shift;;
        --)              shift; break;; # end of options
    esac
    shift
done
main() {
    if [ -n "$HELP_ARG" ]; then
	echo "Usage:"
	echo "reset_AUX --slot N"
	echo "or"
	echo "reset_AUX -s N"
	return 0
    fi

    if [ $HAS_SLOT_ARG == 1 ]; then
	echo "slot defined: $USE_SLOT"
    else
	echo "no slot defined"
	USE_SLOT=15
    fi

    case $USE_SLOT in
	''|*[!0-9]*) echo "Invalid slot $USE_SLOT, should be a number"; return 1;;
	*) ;;
    esac

    if [ $USE_SLOT -gt 21 ]; then
	echo "Invalid slot $USE_SLOT"
	return 1
    fi

    if [ $USE_SLOT -lt 2 ]; then
	echo "Invalid slot $USE_SLOT"
	return 1
    fi

    for fpga in {1..6}; do
	aux_reconfigure_main --slot $USE_SLOT --fpga $fpga "$@" --epcqaddress 0x1000000
    done

    echo "Reset aux using options=$@"
    echo -$SHELL-
    aux_reset_main --slot $USE_SLOT --fpga 1 "$@"
    sleep 1
    aux_reset_main --slot $USE_SLOT --fpga 2 "$@"
    sleep 1
    aux_reset_main --slot $USE_SLOT --fpga 3 "$@"
    sleep 1
    aux_reset_main --slot $USE_SLOT --fpga 4 "$@"
    sleep 1
    aux_reset_main --slot $USE_SLOT --fpga 5 "$@"
    sleep 1
    aux_reset_main --slot $USE_SLOT --fpga 6 "$@"
    sleep 1
    
    #reset the aux freeze
    aux_write_main --slot $USE_SLOT --fpga 1 "$@" 0x40 0x10
    aux_write_main --slot $USE_SLOT --fpga 2 "$@" 0x40 0x10
    aux_write_main --slot $USE_SLOT --fpga 3 "$@" 0x40 0x10
    aux_write_main --slot $USE_SLOT --fpga 4 "$@" 0x40 0x10
    aux_write_main --slot $USE_SLOT --fpga 5 "$@" 0x40 0x10
    aux_write_main --slot $USE_SLOT --fpga 6 "$@" 0x40 0x10
}

main "$@"
