#!/bin/bash

SLOT=15
if [ $# -eq 2 ]; then
    SLOT=$1
fi
echo "Using slot $SLOT"

READ="standard"
if [ $# -eq 2 ]; then
  READ=$2
fi

PRESENCE=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x00006000))) )

PRBS_ROAD_CHECK=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040dc))))>>4)&0x1))

PRBS_ROAD_GEN=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x00004100))))>>0)&0x1))
PRBS_ROAD_GEN_ERR=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x00004100))))>>1)&0x1))

PRBS_HIT=$((($(  vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000020e0))))>>0)&0x1))

LAMB0=$((( $PRESENCE>>16)&0x1))
LAMB1=$((( $PRESENCE>>17)&0x1))
LAMB2=$((( $PRESENCE>>18)&0x1))
LAMB3=$((( $PRESENCE>>19)&0x1))

ERRORS_113_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040e8))))
ERRORS_116_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040ec))))
ERRORS_213_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040f0))))
ERRORS_216_ROAD=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000040f4))))

ERRORS_113_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000020C8))))
ERRORS_116_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000020CC))))
ERRORS_213_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000020D0))))
ERRORS_216_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000020D4))))

CHECKED_113_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000021C8))))
CHECKED_116_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000021CC))))
CHECKED_213_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000021D0))))
CHECKED_216_HIT=$( vme_peek $(printf "%x" $(( ($SLOT<<27)|0x000021D4))))

MASK_LINK=0x000000ff

SHIFT_LINK0=0
SHIFT_LINK1=8
SHIFT_LINK2=16
SHIFT_LINK3=24

if [ $PRBS_HIT = 1 ]; then

	echo ------------------------------CHECKED PACKETS FROM AUXCARD TO HIT FPGA ON AMBOARD ------------------------------------------------------------
	echo -e "Errors on SCT links:"' \t ' ' \t ' "Link3:"  $((( $CHECKED_213_HIT>>$SHIFT_LINK1)&$MASK_LINK))' \t ' ' \t ' \
						"Link2:"  $((( $CHECKED_213_HIT>>$SHIFT_LINK2)&$MASK_LINK))' \t ' ' \t ' \
						"Link1:"  $((( $CHECKED_213_HIT>>$SHIFT_LINK0)&$MASK_LINK)) ' \t ' ' \t ' \
						"Link0:"  $((( $CHECKED_113_HIT>>$SHIFT_LINK2)&$MASK_LINK)) 
 	echo -e "Errors on PIX links:"' \t ' ' \t ' "Link7:"  $((( $CHECKED_216_HIT>>$SHIFT_LINK0)&$MASK_LINK))' \t ' ' \t ' \
						"Link6:"  $((( $CHECKED_216_HIT>>$SHIFT_LINK1)&$MASK_LINK))' \t ' ' \t ' \
						"Link5:"  $((( $CHECKED_116_HIT>>$SHIFT_LINK1)&$MASK_LINK)) ' \t ' ' \t ' \
						"Link4:"  $((( $CHECKED_116_HIT>>$SHIFT_LINK2)&$MASK_LINK))
	echo -e "                    "' \t ' ' \t ' "Link3:"  $((( $CHECKED_216_HIT>>$SHIFT_LINK2)&$MASK_LINK))' \t ' ' \t ' \
						"Link2:"  $((( $CHECKED_116_HIT>>$SHIFT_LINK0)&$MASK_LINK))' \t ' '  \t ' \
						"Link1:"  $((( $CHECKED_113_HIT>>$SHIFT_LINK0)&$MASK_LINK)) ' \t ' ' \t ' \
						"Link0:"  $((( $CHECKED_113_HIT>>$SHIFT_LINK1)&$MASK_LINK))

	echo ------------------------------------------------------------------------------------------------------------------------------------------
	echo ------------------------------ERROR LINKS FROM AUXCARD TO HIT FPGA ON AMBOARD ------------------------------------------------------------
	echo -e "Errors on SCT links:"' \t ' ' \t ' "Link3:"  $((( $ERRORS_213_HIT>>$SHIFT_LINK1)&$MASK_LINK))' \t ' ' \t ' \
						"Link2:"  $((( $ERRORS_213_HIT>>$SHIFT_LINK2)&$MASK_LINK))' \t ' ' \t ' \
						"Link1:"  $((( $ERRORS_213_HIT>>$SHIFT_LINK0)&$MASK_LINK)) ' \t ' ' \t ' \
						"Link0:"  $((( $ERRORS_113_HIT>>$SHIFT_LINK2)&$MASK_LINK)) 
 	echo -e "Errors on PIX links:"' \t ' ' \t ' "Link7:"  $((( $ERRORS_216_HIT>>$SHIFT_LINK0)&$MASK_LINK))' \t ' ' \t ' \
						"Link6:"  $((( $ERRORS_216_HIT>>$SHIFT_LINK1)&$MASK_LINK))' \t ' ' \t ' \
						"Link5:"  $((( $ERRORS_116_HIT>>$SHIFT_LINK1)&$MASK_LINK)) ' \t ' ' \t ' \
						"Link4:"  $((( $ERRORS_116_HIT>>$SHIFT_LINK2)&$MASK_LINK))
	echo -e "                    "' \t ' ' \t ' "Link3:"  $((( $ERRORS_216_HIT>>$SHIFT_LINK2)&$MASK_LINK))' \t ' ' \t ' \
						"Link2:"  $((( $ERRORS_116_HIT>>$SHIFT_LINK0)&$MASK_LINK))' \t ' '  \t ' \
						"Link1:"  $((( $ERRORS_113_HIT>>$SHIFT_LINK0)&$MASK_LINK)) ' \t ' ' \t ' \
						"Link0:"  $((( $ERRORS_113_HIT>>$SHIFT_LINK1)&$MASK_LINK))

	echo ------------------------------------------------------------------------------------------------------------------------------------------
fi

if [ $PRBS_ROAD_CHECK = 1 ]; then
	if [ $LAMB0 = 1 ] || [ $LAMB1 = 1 ] || [ $LAMB2 = 1 ] || [ $LAMB3 = 1 ]; then
		echo ----------------------------------ERROR LINKS FROM AMCHIPs TO ROAD FPGA ON AMBOARD--------------------------------------------------------
	fi

	if [ $LAMB0  = 1 ]; then
		echo -e "Errors on LAMB0:" ' \t ' ' \t ' "Link3:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK2)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK0)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK3)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK2)&$MASK_LINK))
	fi

	if [ $LAMB1  = 1 ]; then
		echo -e "Errors on LAMB1:"  ' \t '' \t ' "Link3:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK3)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK2)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK1)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK0)&$MASK_LINK))
	fi

	if [ $LAMB2  = 1 ]; then
		echo -e "Errors on LAMB2:"  ' \t '' \t ' "Link3:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK1)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK0)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK0)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_216_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
	fi

	if [ $LAMB3  = 1 ]; then
		echo -e "Errors on LAMB3:"  ' \t '' \t ' "Link3:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK2)&$MASK_LINK))   ' \t '' \t ' \
						"Link2:" $((( $ERRORS_116_ROAD>>$SHIFT_LINK3)&$MASK_LINK))  ' \t '' \t ' \
						"Link1:" $((( $ERRORS_213_ROAD>>$SHIFT_LINK3)&$MASK_LINK))  ' \t '' \t ' \
						"Link0:" $((( $ERRORS_113_ROAD>>$SHIFT_LINK1)&$MASK_LINK))
	fi

        if [ $LAMB0 = 1 ] || [ $LAMB1 = 1 ] || [ $LAMB2 = 1 ] || [ $LAMB3 = 1 ]; then
         	echo ------------------------------------------------------------------------------------------------------------------------------------------
        fi
fi
	
if [ $PRBS_ROAD_GEN = 1 ] || [ $PRBS_ROAD_GEN_ERR = 1 ]; then
	 echo -------------------ERROR LINKS FROM ROAD FPGA ON AMBOARD TO PROCESSORS FPGAs ON AUX--------------------------------------------------------

	for fpga in $(seq 3 6)
	do
	echo -e   "Errors on FPGA $fpga LINK 3:" $( aux_read_main --fpga ${fpga} --slot ${SLOT} 0x150 | cut -c25-32) ' \t '' \t ' \
				   	"Link2:" $( aux_read_main --fpga ${fpga} --slot ${SLOT} 0x160 | cut -c25-32) ' \t '' \t ' \
					"Link1:" $( aux_read_main --fpga ${fpga} --slot ${SLOT} 0x170 | cut -c25-32) ' \t '' \t ' \
					"Link0:" $( aux_read_main --fpga ${fpga} --slot ${SLOT} 0x180 | cut -c25-32) 
	done
fi


FILE=./temp

Lamb0_Link3=$((( $ERRORS_113_ROAD>>$SHIFT_LINK2)&$MASK_LINK))                
Lamb0_Link2=$((( $ERRORS_113_ROAD>>$SHIFT_LINK0)&$MASK_LINK))            
Lamb0_Link1=$((( $ERRORS_113_ROAD>>$SHIFT_LINK3)&$MASK_LINK))      
Lamb0_Link0=$((( $ERRORS_213_ROAD>>$SHIFT_LINK2)&$MASK_LINK))                           
                                                                                                 
Lamb1_Link3=$((( $ERRORS_216_ROAD>>$SHIFT_LINK3)&$MASK_LINK))                              
Lamb1_Link2=$((( $ERRORS_216_ROAD>>$SHIFT_LINK2)&$MASK_LINK))       
Lamb1_Link1=$((( $ERRORS_213_ROAD>>$SHIFT_LINK1)&$MASK_LINK))                           
Lamb1_Link0=$((( $ERRORS_213_ROAD>>$SHIFT_LINK0)&$MASK_LINK))                                    
                                                                                                                 
Lamb2_Link3=$((( $ERRORS_116_ROAD>>$SHIFT_LINK1)&$MASK_LINK))                              
Lamb2_Link2=$((( $ERRORS_116_ROAD>>$SHIFT_LINK0)&$MASK_LINK))                          
Lamb2_Link1=$((( $ERRORS_216_ROAD>>$SHIFT_LINK0)&$MASK_LINK))                                      
Lamb2_Link0=$((( $ERRORS_216_ROAD>>$SHIFT_LINK1)&$MASK_LINK))   
                                                                                                              
Lamb3_Link3=$((( $ERRORS_116_ROAD>>$SHIFT_LINK2)&$MASK_LINK))  
Lamb3_Link2=$((( $ERRORS_116_ROAD>>$SHIFT_LINK3)&$MASK_LINK))                                      
Lamb3_Link1=$((( $ERRORS_213_ROAD>>$SHIFT_LINK3)&$MASK_LINK))                                                                            
Lamb3_Link0=$((( $ERRORS_113_ROAD>>$SHIFT_LINK1)&$MASK_LINK))

if [ $READ == "readErrors" ]; then
   if [[ $Lamb0_Link3 != 0 && $Lamb0_Link2 != 0 && $Lamb0_Link1 != 0 && $Lamb0_Link0 != 0 && $Lamb1_Link3 != 0 && $Lamb1_Link2 != 0 && $Lamb1_Link1 != 0 && $Lamb1_Link0 != 0 && $Lamb2_Link3 != 0 && $Lamb2_Link2 != 0 && $Lamb2_Link1 != 0 && $Lamb2_Link0 != 0 && $Lamb3_Link3 != 0 && $Lamb3_Link2 != 0 && $Lamb3_Link1 != 0 && $Lamb3_Link0 != 0 ]]
   then
   printf "\nINFO: errors on all LAMBS as expected, TEST PASSED (read_PRBS_result.sh)\n\n"
   else
   printf "\nERROR: 0 found on one or more of the error counters, please check the detailed output above (read_PRBS_result.sh)\n\n"
   fi
fi

if [ $READ == "zeroErrors" ]; then
   if [[ $Lamb0_Link3 != 0 || $Lamb0_Link2 != 0 || $Lamb0_Link1 != 0 || $Lamb0_Link0 != 0 || $Lamb1_Link3 != 0 || $Lamb1_Link2 != 0 || $Lamb1_Link1 != 0 || $Lamb1_Link0 != 0 || $Lamb2_Link3 != 0 || $Lamb2_Link2 != 0 || $Lamb2_Link1 != 0 || $Lamb2_Link0 != 0 || $Lamb3_Link3 != 0 || $Lamb3_Link2 != 0 || $Lamb3_Link1 != 0 || $Lamb3_Link0 != 0 ]]
   then
   printf "\nERROR: found on one or more of the errors, please check the detailed output above (read_PRBS_result.sh)\n\n"
   else
   printf "\nINFO: 0 on all error counters as expected, TEST PASSED (read_PRBS_result.sh)\n\n"
   fi
fi


 #   ambslp_amchip_prbs_error_count_all_buses_main --chainlength 2  --chipnum 3  --policy disable > /dev/null 2>&1
 #   ambslp_amchip_prbs_error_count_all_buses_main --chainlength 2  --chipnum 3  --policy enable  > /dev/null 2>&1

 #   echo "Polling status registers"
 #   ambslp_amchip_polling_stat_reg_main --chainlength 2  --chipnum 3 >temp


#while read line; do

#line_offeset+$line + 2

# if [ $LAMB0  = 1 ]; then

#if [ $line_offeset  ]

# fi

# if [ $LAMB1  = 1 ]; then

# fi

# if [ $LAMB2  = 1 ]; then

# fi

# if [ $LAMB3  = 1 ]; then

# fi


#     echo "This is a line : $line"
#done < $FILE


