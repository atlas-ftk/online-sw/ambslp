#!/bin/bash
SLOT=15
if [ $# -eq 1 ]; then
  SLOT=$1
fi
source vme_functions.sh

echo
echo Loading 1 hit
echo

FILE_PREFIX=./
echo "Check if the FIFO is clean"
echo "The following number should be 0xffff, (Fifo’s HF(MS 16 b) & EF(LS 16 b))"
slot_peek $SLOT 0x000040c0

echo "Carico le TX fifo di AUX"
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0x9 | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0xa | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0xb | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0xc | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0xd | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0xe | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0xf | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 1 --file 2hits.ss --buffer 0x10 | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file 2hits.ss --buffer 0xa | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file 2hits.ss --buffer 0xb | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file 2hits.ss --buffer 0xc | tail 
sleep 1
aux_load_tx_fifo_main --slot $SLOT --fpga 2 --file 2hits.ss --buffer 0xd | tail 
sleep 1

echo "Resetting spy buffer"
ambslp_reset_spy_main --slot $SLOT

echo "Resetting AMB Road FIFO"
#amb_lamb 2=Reset ONLY the FIFOs memory inside AMBoard
ambslp_init_main --slot $SLOT --amb_lamb 2
ambslp_init_main --slot $SLOT --amb_lamb 2
ambslp_init_main --slot $SLOT --amb_lamb 2

echo "NUMBER of events before starting the test"
ambslp_status_main | grep Number

#the 4 pairs of input links from AUX FPGA input 1 are called pixel, the 4 single input links from AUX FPGA input 2 are called SCT

echo "Starting TEST"
aux_write_main --slot $SLOT --fpga 1 0x30 0x2; aux_write_main --slot $SLOT --fpga 2 0x30 0x2

echo "Ending TEST"
aux_write_main --slot $SLOT --fpga 1 0x30 0x0; aux_write_main --slot $SLOT --fpga 2 0x30 0x0
ambslp_status_main | grep Number
