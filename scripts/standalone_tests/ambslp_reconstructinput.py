import sys


def joinidpatt(idlist,inplist):
	joint=[]
	for i in range(len(idlist)):
		joint.append('%s %r' % (idlist[i], inplist[i]))
	return joint


def reconstrinput(jlist):
	input=[]
	for line in jlist:
		if line[8:-1].startswith('00000000'):
			input.append("0X4" + line[8:-1])
		elif line[8:-1].startswith('f70000'):
			input.append("0X8" + line[8:-1])
		else:
			input.append("0X0" + line[8:-1])
	return input


def boardsim(jointlist):
	binput=[]
	for line in jointlist:
		if line[8:-1].startswith('00000000'):
			continue
		elif line[8:-1].startswith('f70000'):
			binput.append("0X8" + line[8:-1])
		else:
			binput.append("0X0" + line[8:-1])
	return binput


def eventdef(bjointlist):
	eventinput = []
	for line in bjointlist:
		if line.startswith('0X8'):
			eventinput.append("1 "+line)
		else:
			eventinput.append("0 "+line)
	return eventinput


#select the input file that you would like to reconstruct
if (len(sys.argv)!=3):
	sys.exit('\n Please list the input spy file & output type you would like to use.\n Example: python ambslp_reconstructinput.py input.out outputoption\n \n Please select a valid output file(s) argument:\n 0 = single file output for ambslp_diff\n 1 = 12 file output for ambslp_boardsim/gen_hits\n 2 = create both types of output files')
elif (int(sys.argv[2])>=3):
	sys.exit('Please select a valid output file(s) argument:\n 0 = single file output for ambslp_diff\n 1 = 12 file output for ambslp_boardsim/gen_hits\n 2 = create both types of output files')
else:
	filename1 = sys.argv[1]
	outputopt = sys.argv[2]
	print('You have selected output option: ', int(sys.argv[2]))


#open the selected input spy file and read in data
with open(filename1) as f:
	lid1=[]
	linput1=[]
	lid2=[]
	linput2=[]
	lid3=[]
	linput3=[]
	lid4=[]
	linput4=[]
	lid5=[]
	linput5=[]
	lid6=[]
	linput6=[]
	lid7=[]
	linput7=[]
	lid8=[]
	linput8=[]
	lid9=[]
	linput9=[]
	lid10=[]
	linput10=[]
	lid11=[]
	linput11=[]
	lid12=[]
	linput12=[]	
	for line in f.readlines():
		if line.startswith('['):
			line = line.strip()
			columns = line.split()
			lid1.append(columns[0])
			linput1.append(columns[1])
			lid2.append(columns[2])
			linput2.append(columns[3])
			lid3.append(columns[4])
			linput3.append(columns[5])
			lid4.append(columns[6])
			linput4.append(columns[7])
			lid5.append(columns[8])
			linput5.append(columns[9])
			lid6.append(columns[10])
			linput6.append(columns[11])
			lid7.append(columns[12])
			linput7.append(columns[13])
			lid8.append(columns[14])
			linput8.append(columns[15])
			lid9.append(columns[16])
			linput9.append(columns[17])
			lid10.append(columns[18])
			linput10.append(columns[19])
			lid11.append(columns[20])
			linput11.append(columns[21])
			lid12.append(columns[22])
			linput12.append(columns[23])
		else:
			continue

joint1 = reconstrinput(sorted(joinidpatt(lid1,linput1), key=lambda x: x[:-11]))
bjoint1 = boardsim(sorted(joinidpatt(lid1,linput1), key=lambda x: x[:-11]))
joint2 = reconstrinput(sorted(joinidpatt(lid2,linput2), key=lambda x: x[:-11]))
bjoint2 = boardsim(sorted(joinidpatt(lid2,linput2), key=lambda x: x[:-11]))
joint3 = reconstrinput(sorted(joinidpatt(lid3,linput3), key=lambda x: x[:-11]))
bjoint3 = boardsim(sorted(joinidpatt(lid3,linput3), key=lambda x: x[:-11]))
joint4 = reconstrinput(sorted(joinidpatt(lid4,linput4), key=lambda x: x[:-11]))
bjoint4 = boardsim(sorted(joinidpatt(lid4,linput4), key=lambda x: x[:-11]))
joint5 = reconstrinput(sorted(joinidpatt(lid5,linput5), key=lambda x: x[:-11]))
bjoint5 = boardsim(sorted(joinidpatt(lid5,linput5), key=lambda x: x[:-11]))
joint6 = reconstrinput(sorted(joinidpatt(lid6,linput6), key=lambda x: x[:-11]))
bjoint6 = boardsim(sorted(joinidpatt(lid6,linput6), key=lambda x: x[:-11]))
joint7 = reconstrinput(sorted(joinidpatt(lid7,linput7), key=lambda x: x[:-11]))
bjoint7 = boardsim(sorted(joinidpatt(lid7,linput7), key=lambda x: x[:-11]))
joint8 = reconstrinput(sorted(joinidpatt(lid8,linput8), key=lambda x: x[:-11]))
bjoint8 = boardsim(sorted(joinidpatt(lid8,linput8), key=lambda x: x[:-11]))
joint9 = reconstrinput(sorted(joinidpatt(lid9,linput9), key=lambda x: x[:-11]))
bjoint9 = boardsim(sorted(joinidpatt(lid9,linput9), key=lambda x: x[:-11]))
joint10 = reconstrinput(sorted(joinidpatt(lid10,linput10), key=lambda x: x[:-11]))
bjoint10 = boardsim(sorted(joinidpatt(lid10,linput10), key=lambda x: x[:-11]))
joint11 = reconstrinput(sorted(joinidpatt(lid11,linput11), key=lambda x: x[:-11]))
bjoint11 = boardsim(sorted(joinidpatt(lid11,linput11), key=lambda x: x[:-11]))
joint12 = reconstrinput(sorted(joinidpatt(lid12,linput12), key=lambda x: x[:-11]))
bjoint12 = boardsim(sorted(joinidpatt(lid12,linput12), key=lambda x: x[:-11]))

if int(outputopt)==0:
	with open('inpspydata2.txt','w') as f:
		lis=[joint1,joint2,joint3,joint4,joint5,joint6,joint7,joint8,joint9,joint10,joint11,joint12]
		for x in zip(*lis):
			f.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\n".format(*x))
elif int(outputopt)==1:
	with open('input_0_spy.txt','w') as f0:
		lis0=[eventdef(bjoint1)]
		for x in zip(*lis0):
			f0.write("{0}\n".format(*x))
	with open('input_1_spy.txt','w') as f1:
		lis1=[eventdef(bjoint2)]
		for x in zip(*lis1):
			f1.write("{0}\n".format(*x))
	with open('input_2_spy.txt','w') as f2:
		lis2=[eventdef(bjoint3)]
		for x in zip(*lis2):
			f2.write("{0}\n".format(*x))
	with open('input_3_spy.txt','w') as f3:
		lis=[eventdef(bjoint4)]
		for x in zip(*lis):
			f3.write("{0}\n".format(*x))
	with open('input_4_spy.txt','w') as f4:
		lis=[eventdef(bjoint5)]
		for x in zip(*lis):
			f4.write("{0}\n".format(*x))	
	with open('input_5_spy.txt','w') as f5:
		lis=[eventdef(bjoint6)]
		for x in zip(*lis):
			f5.write("{0}\n".format(*x))
	with open('input_6_spy.txt','w') as f6:
		lis=[eventdef(bjoint7)]
		for x in zip(*lis):
			f6.write("{0}\n".format(*x))
	with open('input_7_spy.txt','w') as f7:
		lis=[eventdef(bjoint8)]
		for x in zip(*lis):
			f7.write("{0}\n".format(*x))
	with open('input_8_spy.txt','w') as f8:
		lis=[eventdef(bjoint9)]
		for x in zip(*lis):
			f8.write("{0}\n".format(*x))
	with open('input_9_spy.txt','w') as f9:
		lis=[eventdef(bjoint10)]
		for x in zip(*lis):
			f9.write("{0}\n".format(*x))
	with open('input_10_spy.txt','w') as f10:
		lis=[eventdef(bjoint11)]
		for x in zip(*lis):
			f10.write("{0}\n".format(*x))
	with open('input_11_spy.txt','w') as f11:
		lis=[eventdef(bjoint12)]
		for x in zip(*lis):
			f11.write("{0}\n".format(*x))
elif int(outputopt)==2:
	with open('inpspydata.txt','w') as f:
		lis=[joint1,joint2,joint3,joint4,joint5,joint6,joint7,joint8,joint9,joint10,joint11,joint12]
		for x in zip(*lis):
			f.write("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\n".format(*x))
	with open('input_0_spy.txt','w') as f0:
		lis0=[eventdef(bjoint1)]
		for x in zip(*lis0):
			f0.write("{0}\n".format(*x))
	with open('input_1_spy.txt','w') as f1:
		lis1=[eventdef(bjoint2)]
		for x in zip(*lis1):
			f1.write("{0}\n".format(*x))
	with open('input_2_spy.txt','w') as f2:
		lis2=[eventdef(bjoint3)]
		for x in zip(*lis2):
			f2.write("{0}\n".format(*x))
	with open('input_3_spy.txt','w') as f3:
		lis=[eventdef(bjoint4)]
		for x in zip(*lis):
			f3.write("{0}\n".format(*x))
	with open('input_4_spy.txt','w') as f4:
		lis=[eventdef(bjoint5)]
		for x in zip(*lis):
			f4.write("{0}\n".format(*x))	
	with open('input_5_spy.txt','w') as f5:
		lis=[eventdef(bjoint6)]
		for x in zip(*lis):
			f5.write("{0}\n".format(*x))
	with open('input_6_spy.txt','w') as f6:
		lis=[eventdef(bjoint7)]
		for x in zip(*lis):
			f6.write("{0}\n".format(*x))
	with open('input_7_spy.txt','w') as f7:
		lis=[eventdef(bjoint8)]
		for x in zip(*lis):
			f7.write("{0}\n".format(*x))
	with open('input_8_spy.txt','w') as f8:
		lis=[eventdef(bjoint9)]
		for x in zip(*lis):
			f8.write("{0}\n".format(*x))
	with open('input_9_spy.txt','w') as f9:
		lis=[eventdef(bjoint10)]
		for x in zip(*lis):
			f9.write("{0}\n".format(*x))
	with open('input_10_spy.txt','w') as f10:
		lis=[eventdef(bjoint11)]
		for x in zip(*lis):
			f10.write("{0}\n".format(*x))
	with open('input_11_spy.txt','w') as f11:
		lis=[eventdef(bjoint12)]
		for x in zip(*lis):
			f11.write("{0}\n".format(*x))
else:
	print('not a valid output option') #this is a backup in case the program accepts an input greater than 2
