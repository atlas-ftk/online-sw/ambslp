#!/bin/bash
./ambslp_reset_spy_main
echo "Carico le TX fifo di AUX"
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L4_np128k.ss --buffer 0x9
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L4_np128k.ss --buffer 0xa
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L5_np128k.ss --buffer 0xb
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L5_np128k.ss --buffer 0xc
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L6_np128k.ss --buffer 0xd
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L6_np128k.ss --buffer 0xe
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L7_np128k.ss --buffer 0xf
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 1 --file ./filesForTest/sshits_L7_np128k.ss --buffer 0x10
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/sshits_L0_np128k.ss --buffer 0xa
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/sshits_L1_np128k.ss --buffer 0xb
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/sshits_L2_np128k.ss --buffer 0xc
sleep 1
./aux_load_tx_fifo_main --slot 15 --fpga 2 --file ./filesForTest/sshits_L3_np128k.ss --buffer 0xd
sleep 1
echo "Carico la fifo di ROAD"
#./ambslp_feed_road_main --slot 15 --loopfifovme 1 gigi_road.road
sleep 1
echo "Sparo dalla AUX"
./aux_write_main --slot 15 --fpga 1 0x30 0x2
./aux_write_main --slot 15 --fpga 2 0x30 0x2
echo "Ripristino la AUX"
./aux_write_main --slot 15 --fpga 1 0x30 0x0
./aux_write_main --slot 15 --fpga 2 0x30 0x0
