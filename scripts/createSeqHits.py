#!/usr/bin/env python

import sys

totpatts = 32*1024 # total number of patterns

startpatt = int(sys.argv[1])%totpatts
npatts = int(sys.argv[2]) # has to be even
nevents = int(sys.argv[3])

SSoffset = 8


outfile = open("seqhits.ss", "w")
emptyfile = open("seqhits_empty.ss","w")
roadsfile = open("seqroads.out","w")

outfile.write("1 0x00000000\n")

for ievt in xrange(nevents) :
  for iw in xrange(npatts/2) :
    base = iw*2+npatts*ievt+startpatt+SSoffset
    outword = base
    outword |= (base+1)<<16
    outfile.write("0 0x%08x\n" % outword)
  outfile.write("1 0x%08x\n" % (ievt+1))

outfile.close()


for ievt in xrange(nevents+1) :
  emptyfile.write("1 0x%08x\n" % ievt)
emptyfile.close()

roadsfile.write("f7800000\n")
for ievt in xrange(nevents) :
  for ir in xrange(npatts) :
    for il in xrange(4) : # repetitions 
      roadid = ir+ievt*npatts+startpatt+il*totpatts
      geoaddr = roadid/2048
      pattid = roadid%2048
      roadsfile.write("%08x\n" % ((pattid|(geoaddr<<17)|(0xff<<24))))
  roadsfile.write("%08x\n" % ((ievt+1) | 0xf7800000))
