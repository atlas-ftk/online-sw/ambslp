import sys
import math

def compl2(x,n):
        if x >> n-1 == 1:
                return x - ( 1 << n)
        return x

def main():

	a     = sys.argv[1]
	number      = int(a,16)
	exp       = int( sys.argv[2])
	mantissa=  compl2(number, 16)

	print  `float(math.pow(2,exp)*mantissa)`

if __name__ == "__main__":
	main()
