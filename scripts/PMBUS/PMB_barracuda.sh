#!/bin/bash

################################################################################
vme_poke 0x78002160 2988
VinAD=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 29d4
VinOFFset=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 29d3
VinGAIN=$( python  mantissa_unsigned.py $(  vme_peek 0x78002164) 0)

## Vin=((VinGAIN/8192)*VinAD)+VinOFFSET
Vin=$( echo $( echo $( echo $VinGAIN/8192 |bc)*$VinAD |bc) + $VinOFFset |bc)

echo "Vin =" $Vin  "V"

#################################################################################

vme_poke 0x78002160 298b
VoutAD=$( python  mantissa_unsigned.py $(  vme_peek 0x78002164) -12)

vme_poke 0x78002160 29d2
VoutOFFset=$( python  mantissa_2comp.py $(  vme_peek 0x78002164) -12)

## Vout=(VinAD+VinOFFSET)
Vout=$( echo $VoutOFFset + $VoutAD |bc)


echo "Vout ="  $Vout "V"

###############################################################################

vme_poke 0x78002160 298c
IoutAD=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 29d6
IoutGAIN=$( python  mantissa_unsigned.py $(  vme_peek 0x78002164) 0)

vme_poke 0x78002160 29d7
IoutOFFset=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=((IoutGAIN/8192)*IoutAD)+IoutOFFSET
Iout=$( echo $( echo $( echo $IoutGAIN/8192 |bc -l)*$IoutAD |bc -l) + $IoutOFFset |bc)

echo "Iout =" $Iout  "A"
#################################################################################

Pout=$( echo $Iout*$Vout |bc)
echo "Pout =" $Pout  "W"

#################################################################################


vme_poke 0x78002160 298d
Temp1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp ="  $Temp1 "°C"

#################################################################################


