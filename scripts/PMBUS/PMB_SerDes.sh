#!/bin/bash


vme_poke 0x78002160 08a00

#################################################################################

vme_poke 0x78002160 a8b
Vout0=$( python mantissa_2comp.py $(  vme_peek 0x78002164) -9)


echo "Vout_0 ="  $Vout0 "V"

###############################################################################

vme_poke 0x78002160 a8c
IoutAD0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "IoutAD_0 =" $IoutAD0  "A"


vme_poke 0x78002160 a38
IoutGAIN0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 a39
IoutOFFset0=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=(IoutGAIN*IoutAD)+IoutOFFSET
Iout0=$( echo $( echo $IoutGAIN0*$IoutAD0 |bc) + $IoutOFFset0 |bc)

echo "Iout_0 =" $Iout0  "A"

#################################################################################

vme_poke 0x78002160 a8e
Temp1_0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp_0 ="  $Temp1_0 "°C"

#################################################################################
echo " "

vme_poke 0x78002160 18a00
#################################################################################

vme_poke 0x78002160 a8b
Vout_1=$( python mantissa_2comp.py $(  vme_peek 0x78002164) -9)


echo "Vout_1 ="  $Vout_1 "V"

###############################################################################

vme_poke 0x78002160 a8c
IoutAD_1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "IoutAD_1 =" $IoutAD_1  "A"


vme_poke 0x78002160 a38
IoutGAIN_1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 a39
IoutOFFset_1=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=(IoutGAIN*IoutAD)+IoutOFFSET
Iout_1=$( echo $( echo $IoutGAIN_1*$IoutAD_1 |bc) + $IoutOFFset_1 |bc)

echo "Iout_1 =" $Iout_1  "A"
echo "IoutGAIN_1 =" $IoutGAIN_1
echo "IoutOFFset_1 =" $IoutOFFset_1  "A"
#################################################################################

vme_poke 0x78002160 a8e
Temp1_1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp_1 ="  $Temp1_1 "°C"



