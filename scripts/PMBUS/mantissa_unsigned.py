import sys
import math


def main():

	a     = sys.argv[1]
	mantissa      = int(a,16)
	exp       = int(sys.argv[2])

	print  `float(math.pow(2,exp)*mantissa)`

if __name__ == "__main__":
	main()
