#!/bin/bash

echo "current @ 12V (A)"
vme_poke 0x78002160 298c
python mantissaexp.py $(  vme_peek 0x78002164)

echo "current @ 1V Lamb 0 (A)"
vme_poke 0x78002160 108c
python mantissaexp.py $(  vme_peek 0x78002164)

echo "current @ 1V Lamb 1 (A)"
vme_poke 0x78002160 e8c        
python mantissaexp.py $(  vme_peek 0x78002164)

echo "current @ 1V Lamb 2 (A)"
vme_poke 0x78002160 118c        
python mantissaexp.py $(  vme_peek 0x78002164)

echo "current @ 1V Lamb 3 (A)"
vme_poke 0x78002160 f8c        
python mantissaexp.py $(  vme_peek 0x78002164)



