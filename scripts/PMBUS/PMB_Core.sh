#!/bin/bash

#This value depens by the trim ressitor mounted
#The AMBv4 and the first AMBv5 need to use 1.9216666666666667
#The second AMBv5 and then following 20 AMBv5 need to use 1.6666666666666667
VOUT_SCALE=$( echo 1.9216666666666667) # value for boards with 1.145 nominal tesion 
VOUT_SCALE=$( echo 1.6666666666666667) # value for boards with 1.000 nominal tesion  

echo 
echo WARNING: VOUT_SCALE is $VOUT_SCALE
echo          VOUT_SCALE depends on the TRIMMING restistor.
echo          Please read the source code to cross-check that the correct VOUT_SCALE is used
echo

echo " "
echo "LAMB 0"
################################################################################
vme_poke 0x78002160 1088
VinL0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Vin =" $VinL0  "V"

#################################################################################

vme_poke 0x78002160 108b
VoutL0=$( python  mantissa_2comp.py $(  vme_peek 0x78002164) -13)

echo "Vout ="  $( echo $VoutL0*$VOUT_SCALE |bc) "V"

###############################################################################

vme_poke 0x78002160 108c
IoutADL0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 1038
IoutGAINL0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 1039
IoutOFFsetL0=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=((IoutGAINL0)*IoutADL0)+IoutOFFsetL0
IoutL0=$( echo $( echo $IoutGAINL0*$IoutADL0 |bc) + $IoutOFFsetL0 |bc)

echo "Iout =" $IoutL0  "A" "  (This is the value corrected with offset and gain. Be aware than eng. samples might have wrong calibartion!!!!)"
echo "IoutAD =" $IoutADL0  "A" "  (This is the raw value read from the ADC)"
echo "IoutGAINL0 =" $IoutGAINL0
echo "IoutOFFsetL0 =" $IoutOFFsetL0  "A"
#################################################################################

vme_poke 0x78002160 108d
Temp1L0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp1 ="  $Temp1L0 "°C"

#################################################################################


vme_poke 0x78002160 108e
Temp2L0=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp2 ="  $Temp2L0 "°C"

#################################################################################
echo " "

echo "LAMB 1"
################################################################################
vme_poke 0x78002160 e88
VinL1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Vin =" $VinL1  "V"

#################################################################################

vme_poke 0x78002160 e8b
VoutL1=$( python  mantissa_2comp.py $(  vme_peek 0x78002164) -13)

echo "Vout ="  $( echo $VoutL1*$VOUT_SCALE |bc) "V"

###############################################################################

vme_poke 0x78002160 e8c
IoutADL1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 e38
IoutGAINL1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 e39
IoutOFFsetL1=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=((IoutGAIN)*IoutAD)+IoutOFFSET
IoutL1=$( echo $( echo $IoutGAINL1*$IoutADL1 |bc) + $IoutOFFsetL1 |bc)

echo "Iout =" $IoutL1  "A"
echo "IoutAD =" $IoutADL1  "A"

#################################################################################

vme_poke 0x78002160 e8d
Temp1L1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp1 ="  $Temp1L1 "°C"

#################################################################################


vme_poke 0x78002160 e8e
Temp2L1=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp2 ="  $Temp2L1 "°C"

#################################################################################
echo " "

echo "LAMB 2"
################################################################################
vme_poke 0x78002160 1188
VinL2=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Vin =" $VinL2  "V"

#################################################################################

vme_poke 0x78002160 118b
VoutL2=$( python  mantissa_2comp.py $(  vme_peek 0x78002164) -13)

echo "Vout ="  $( echo $VoutL2*$VOUT_SCALE |bc) "V"

###############################################################################

vme_poke 0x78002160 118c
IoutADL2=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 1138
IoutGAINL2=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 1139
IoutOFFsetL2=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=((IoutGAIN)*IoutAD)+IoutOFFSET
IoutL2=$( echo $( echo $IoutGAINL2*$IoutADL2 |bc) + $IoutOFFsetL2 |bc)

echo "Iout =" $IoutL2  "A"
echo "IoutAD =" $IoutADL2  "A"


#################################################################################

vme_poke 0x78002160 118d
Temp1L2=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp1 ="  $Temp1L2 "°C"

#################################################################################


vme_poke 0x78002160 118e
Temp2L2=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp2 ="  $Temp2L2 "°C"

#################################################################################
echo " "

echo "LAMB 3"
################################################################################
vme_poke 0x78002160 f88
VinL3=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Vin =" $VinL3  "V"

#################################################################################

vme_poke 0x78002160 f8b

VoutL3=$( python  mantissa_2comp.py $(  vme_peek 0x78002164) -13)

echo "Vout ="  $( echo $VoutL3*$VOUT_SCALE |bc) "V"

###############################################################################

vme_poke 0x78002160 f8c
IoutADL3=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 f38
IoutGAINL3=$( python  mantissaexp.py $(  vme_peek 0x78002164))

vme_poke 0x78002160 f39
IoutOFFsetL3=$( python  mantissaexp.py $(  vme_peek 0x78002164))


## Iout=(IoutGAIN*IoutAD)+IoutOFFSET

IoutL3=$( echo $( echo $IoutGAINL3*$IoutADL3 |bc) + $IoutOFFsetL3 |bc)

echo "Iout =" $IoutL3  "A" "  (This is the value corrected with offset and gain. Be aware than eng. samples might have wrong calibartion!!!!)"
echo "IoutAD =" $IoutADL3  "A" "  (This is the raw value read from the ADC)"

#################################################################################

vme_poke 0x78002160 f8d
Temp1L3=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp1 ="  $Temp1L3 "°C"

#################################################################################


vme_poke 0x78002160 f8e
Temp2L3=$( python  mantissaexp.py $(  vme_peek 0x78002164))

echo "Temp2 ="  $Temp2L3 "°C"

#################################################################################

