import sys
import math

# 0x0000f088
#
# mantissa 136
# exp -2
# 34

def compl2(x,n):
	if x >> n-1 == 1:
		return x - ( 1 << n)
	return x

def main():

	a            = sys.argv[1]
	number       = int(a,16)
	maskMantissa = 2047
	mantissaLen  = 11
	expLen       = 5

	exp      = compl2(number >> mantissaLen, expLen)
	mantissa = compl2(number & maskMantissa, mantissaLen)

#	print "num: "      + `number`
#	print "exp: "      + `exp`
#	print "mantissa: " + `mantissa`
	print  `float(math.pow(2,exp)*mantissa)`

if __name__ == "__main__":
	main()
