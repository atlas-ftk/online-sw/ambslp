#!/bin/bash

SLOTLDC=15 # Slot that the main AUX card is plugged in

# Step 1: Enable RSLB
for i in $(seq 3 6)
do
    aux_write_main --slot ${SLOTLDC} --fpga ${i} 0x40 0x1
done

# Step 3: Redo sync
for i in $(seq 1 6)
do
    aux_reset_main --slot ${SLOTLDC} --fpga ${i} --sync
    aux_reset_main --slot ${SLOTLDC} --fpga ${i} --spybuffer
done

aux_status_main --slot ${SLOTLDC}
