#!/bin/bash
set -x

source GLOBALS.sh
vme_poke 0x780040D0 0xF000 #disable LAMB3

outfile=~/SpyBufferArchives/spyfile_$(date +%Y%m%d_%H%M)
echo $outfile
rm -f ${outfile}_m{1,2}_chips_stat.root

  

for CONF in {1..20}; do
  setuperr=0
  sh reset_AUX.sh
  sh initialization_amb_v20150513Pisa.sh || setuperr=1
  if [ $setuperr -ne 0 ]; then
    echo "Error during initialize, skipping the test"
    continue
  fi
  sh configure_loop_realevents_fromVME_LAMBv2_v20150914_seqmod32k.sh

  ambslp_init_main --amb_lamb 2 
  ambslp_init_main --amb_lamb 2

  # 100 loops about 45 minutes
  for LOOP in {1..192}; do 
    echo "LOOP $LOOP, $(date)"
  
    looperr=0
    sh test_loop_no0_seqmod32k.sh $LOOP
    #sh test_no0_onepatternDC.sh
    echo "$(date)"

  

    ambslp_inp_spy_main > ${outfile}_m0.in
    ambslp_inp_spy_main --method 1 2> ${outfile}_m1.in
    ambslp_out_spy_main > ${outfile}_m0.out
    ambslp_out_spy_main --method 1 2> ${outfile}_m1.out
    #cut -b 12- ${outfile}_m1.out > ${outfile}_m1C12.out
    ambslp_out_spy_main --method 2 2> ${outfile}_m2.out

    #roadfile=roads_randomtest.out
    roadfile=seqroads.out

    echo "CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}_LOOP_m1.out.diff
    ./AMBCompareTestVecOutput.py --MaskLAMB 3 -A ${outfile}_m1_chips_stat.root ${outfile}_m1.out $roadfile >> ${outfile}_LOOP_m1.out.diff || looperr=1
    echo "CONF $CONF, LOOP $LOOP, $(date)" >> ${outfile}_LOOP_m2.out.diff 
    ./AMBCompareTestVecOutput.py --MaskLAMB 3 -A ${outfile}_m2_chips_stat.root ${outfile}_m2.out $roadfile >> ${outfile}_LOOP_m2.out.diff
    echo "$(date)"

    if [ $looperr -ne 0 ]; then
      echo "An error was found in LOOP $LOOP, save the loop"
      
      # make the loop persistent
      mv -v ${outfile}_m0.in ${outfile}_CONF${CONF}_LOOP${LOOP}_m0.in
      mv -v ${outfile}_m1.in ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.in
      mv -v ${outfile}_m0.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m0.out
      mv -v ${outfile}_m1.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m1.out
      mv -v ${outfile}_m2.out ${outfile}_CONF${CONF}_LOOP${LOOP}_m2.out
    fi
  done
done

set +x
