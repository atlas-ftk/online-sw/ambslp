# TV test 2016
set npatts 131072 
set slot 19
set bankpath /shared/data/tkubota/user.sschmitt.patterns_raw_8L_15x16x36Ibl_30x64x72Ibl_1000M_am8M_nlamb4_reg22_sub0.pbank.root

set ambhits /shared/data/tkubota/sshits_Twr22_amb.ss
#set ambhits standalone_tests/AMB_feed_randomtest.ss
#set ambhits /home/ftk/tmp/bitFlip.ss
#set ambhits /home/ftk/tmp/bitFlip_corto.ss
#set ambhits /home/ftk/tmp/bitFlip_balanced4.ss
#set ambhits /home/ftk/tmp/bitFlip_balanced5.ss
#set ambhits /home/ftk/tmp/bitFlip_balanced8.ss
#set ambhits standalone_tests/bitFlip_balanced8.ss
#set ambhits standalone_tests/bitFlip_corto.ss

set auxhits0 /shared/data/tkubota/sshits_Twr22_L0.ss
set auxhits1 /shared/data/tkubota/sshits_Twr22_L1.ss
set auxhits2 /shared/data/tkubota/sshits_Twr22_L2.ss
set auxhits3 /shared/data/tkubota/sshits_Twr22_L3.ss
set auxhits4 /shared/data/tkubota/sshits_Twr22_L4.ss
set auxhits5 /shared/data/tkubota/sshits_Twr22_L5.ss
set auxhits6 /shared/data/tkubota/sshits_Twr22_L6.ss
set auxhits7 /shared/data/tkubota/sshits_Twr22_L7.ss
set auxhits8 /shared/data/tkubota/sshits_Twr22_L8.ss
set auxhits9 /shared/data/tkubota/sshits_Twr22_L9.ss
set auxhits10 /shared/data/tkubota/sshits_Twr22_L10.ss
set auxhits11 /shared/data/tkubota/sshits_Twr22_L11.ss

blocktransfer yes
#set forcebank 1

set outroads /shared/data/tkubota/outroads_Twr22.out
#set outroads standalone_tests/roads_randomtest.out

set keeptmpdir yes
