/** \file ambslp_doc.h Documentation header file */

/**
 *  \mainpage ambslp - library and standalone programs
 *  \brief The ambslp package provides general utilities to access the AM board 
 *         and set it up
 *
 *  Some of the ambslp utilities are:
 *    - access to the AM board internals
 *    - set up and tear down of the board
 *    - hardware discovery (by quering the board via vme)
 *    - standalone programs to test the board behaviour
 *    - test scripts
 *    - Readout Module schema and object
 *    - standalone possibility to link the OKS settings
 *
 *
 *
 *  \section Dependencies
 *
 *      The package depends on the following other packages:
 *        - ftkcommon
 *        - ftkvme
 *        - aux
 *
 *      The package also depends on the CMT and TDAQ releases;
 *      more details can be found at
 *
 *      https://twiki.cern.ch/twiki/bin/view/Atlas/FastTrackerTdaqRelease
 *
 *
 *
 *  \section CompileInstall Obtain Code, Compile and Install
 *
 *      obtain the source from the FTK repository:
 *        - svn+ssh://svn.cern.ch/reps/atlastdaq/FTK/ambslp/trunk
 *
 *      compile via cmt:
 *        - cd ambslp/cmt
 *        - cmt make
 *
 *      install:
 *        - cmt make inst
 *
 *      This will compile and install the trunk version of the ambslp package.
 *
 *      For nightly, nightly-head and release versions, please refer to 
 *
 *      https://twiki.cern.ch/twiki/bin/view/Atlas/FastTrackerTdaqRelease
 *
 *      you can also retrieve the compile version of a tagged release.
 *      The above link also explains how to export environment variables and 
 *      allow run the standalones from any directory, or link correctly the libraries.
 *
 *
 *
 *  \section StandaloneUsage Standalone Usage
 *
 *      After installation, all the standalone are provided with a <em>--help</em>
 *      option, which explains the other possible configuration from command-line 
 *      for each executable.
 *
 *
 *
 *  \section GenBehaviour General Behaviour of the Library
 *
 *      The library tries t isolate internal aspects of the AM board and present 
 *      more abstract interfaces. The messages to the user/client are managed via 
 *      the ERS logging mechanism.
 *
 *      In case of failure, the library throws exceptions (common, defined in \a ftkcommon, 
 *      vme-related, defined in \a ftkvme, or specific, defined in the \a ambslp package).
 *      The client can catch all of them referring to a base type daq::ftk::FTKIssue.
 * 
 *      Useful variables/histograms are published via the IS mechanism.
 *
 *  \section GenTodo General To-Be-Done Issues
 *
 *     \todo Some pending issues
 *      - complete passage to ERS logging system
 *      - complete passage to ERS exception handling
 *      - start using IS publishing
 *      - clean code, remove duplicates
 *      - write documentation
 *      - fix standalone problems with OKS
 *
 *
 *
*/



/**
 * \defgroup libambslp libambslp
 * \brief ambslp library functions and objects
 *
 *     This module collects the classes and functions defined in the
 *     ambslp library.
 * 
*/

/**
 * \defgroup lib_chip amchip
 * \ingroup libambslp
 *
 *     Library methods to set up and extract information from the amchips.
 *
*/

/**
 * \defgroup lib_board amboard
 * \ingroup libambslp
 *  
 *     Library methods to obtain information from the board.
 *
*/

/**
 * \defgroup lib_ROM Readout Modules
 * \ingroup libambslp
 *
 *     Library functions and classes to interact with the Readout Module for Run Control.
 *
*/

/**
 * \defgroup lib_misc Miscellanea
 * \ingroup libambslp
 *
 *     Other utilities/classes/methods that do not fit (yet) the above categories
 *
*/


/**
 * \defgroup standalone ambslp standalone programs
 * \brief standalone programs using the ambslp library
 *
 *     This module describes the different standalone programs 
 *     used to test/access the board in stand-alone mode.
 *     They expose the library functionalities to the user, and 
 *     are used in the test-suite and to reconfigure the AM board.
 *
*/


/**
 * \defgroup stl_chip amchip
 * \ingroup standalone
 *
 *     Library methods to set up and extract information from the amchips.
 *
*/

/**
 * \defgroup stl_board amboard
 * \ingroup standalone
 *  
 *     Library methods to obtain information from the board.
 *
*/

/**
 * \defgroup stl_ROM Readout Modules
 * \ingroup standalone
 *
 *     Library functions and classes to interact with the Readout Module for Run Control.
 *
*/

/**
 * \defgroup stl_misc Miscellanea
 * \ingroup standalone
 *
 *     Other utilities/classes/methods that do not fit (yet) the above categories
 *
*/


